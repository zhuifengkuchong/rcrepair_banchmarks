/* Utilities for RA GlobalSite and CountrySites
Modified 07-06-14 by BM = added LANGUAGE SELECTOR
Modified 09-10-14 by BM = added EU COOKIE BANNER
Modified 12-11-14 by SB = added new toggle open/close with class activation
*/

//BEGIN EU COOKIE BANNER
function grabCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i=0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}
var thisPage = window.parent.location.href;
var urlSite = thisPage.replace(/https?:\/\/[^\/]+\/?([^\/]*).*/,"$1");
var cookieSites = [ "de_AT","fr_BE","nl_BE","de_CH","fr_CH","cs_CZ","de_DE","da_DK","es_ES","fr_FR","en_UK","en_IE","en_IL","it_IT","en_MDE","nl_NL","pl_PL","pt_PT","ru_RU","sv_SE","tr_TR","en_ZA" ];
if ( urlSite != "" && cookieSites.toString().indexOf(urlSite) != -1 ) {
	var hideCookieBanner = grabCookie('hide_cookie_banner') ? true : false;
	if (!hideCookieBanner) {
		var cookieMsg="";
		var cookieBtn="";
		switch (urlSite) {
			case "cs_CZ": //Czech
				cookieMsg="This website uses cookies to improve functionality and performance. If you continue browsing the site, you are giving implied consent to the use of cookies on this website. See our <a href='http://www.rockwellautomation.com/rockwellautomation/legal-notices/overview.page?#/tab2' target='_blank'>Cookie Policy</a> for details.";
				cookieBtn="Don't show this message again";
				break;
/*			case "da_DK": //Danish
				cookieMsg="This website uses cookies to improve functionality and performance. If you continue browsing the site, you are giving implied consent to the use of cookies on this website. See our <a href='http://www.rockwellautomation.com/rockwellautomation/legal-notices/overview.page?#/tab2' target='_blank'>Cookie Policy</a> for details.";
				cookieBtn="Don't show this message again";
				break;
			case "nl_BE": case "nl_NL": //Dutch
				cookieMsg="This website uses cookies to improve functionality and performance. If you continue browsing the site, you are giving implied consent to the use of cookies on this website. See our <a href='http://www.rockwellautomation.com/rockwellautomation/legal-notices/overview.page?#/tab2' target='_blank'>Cookie Policy</a> for details.";
				cookieBtn="Don't show this message again";
				break;
			case "fr_BE": case "fr_CH": case "fr_FR": //French
				cookieMsg="This website uses cookies to improve functionality and performance. If you continue browsing the site, you are giving implied consent to the use of cookies on this website. See our <a href='http://www.rockwellautomation.com/rockwellautomation/legal-notices/overview.page?#/tab2' target='_blank'>Cookie Policy</a> for details.";
				cookieBtn="Don't show this message again";
				break;
			case "de_AT": case "de_CH": case "de_DE": //German
				cookieMsg="This website uses cookies to improve functionality and performance. If you continue browsing the site, you are giving implied consent to the use of cookies on this website. See our <a href='http://www.rockwellautomation.com/rockwellautomation/legal-notices/overview.page?#/tab2' target='_blank'>Cookie Policy</a> for details.";
				cookieBtn="Don't show this message again";
				break;
			case "it_IT": //Italian
				cookieMsg="This website uses cookies to improve functionality and performance. If you continue browsing the site, you are giving implied consent to the use of cookies on this website. See our <a href='http://www.rockwellautomation.com/rockwellautomation/legal-notices/overview.page?#/tab2' target='_blank'>Cookie Policy</a> for details.";
				cookieBtn="Don't show this message again";
				break;
			case "pl_PL": //Polish
				cookieMsg="This website uses cookies to improve functionality and performance. If you continue browsing the site, you are giving implied consent to the use of cookies on this website. See our <a href='http://www.rockwellautomation.com/rockwellautomation/legal-notices/overview.page?#/tab2' target='_blank'>Cookie Policy</a> for details.";
				cookieBtn="Don't show this message again";
				break;
			case "pt_PT": //Portuguese
				cookieMsg="This website uses cookies to improve functionality and performance. If you continue browsing the site, you are giving implied consent to the use of cookies on this website. See our <a href='http://www.rockwellautomation.com/rockwellautomation/legal-notices/overview.page?#/tab2' target='_blank'>Cookie Policy</a> for details.";
				cookieBtn="Don't show this message again";
				break;
			case "ru_RU": //Russian
				cookieMsg="This website uses cookies to improve functionality and performance. If you continue browsing the site, you are giving implied consent to the use of cookies on this website. See our <a href='http://www.rockwellautomation.com/rockwellautomation/legal-notices/overview.page?#/tab2' target='_blank'>Cookie Policy</a> for details.";
				cookieBtn="Don't show this message again";
				break;
			case "es_ES": //Spanish
				cookieMsg="This website uses cookies to improve functionality and performance. If you continue browsing the site, you are giving implied consent to the use of cookies on this website. See our <a href='http://www.rockwellautomation.com/rockwellautomation/legal-notices/overview.page?#/tab2' target='_blank'>Cookie Policy</a> for details.";
				cookieBtn="Don't show this message again";
				break;
			case "sv_SE": //Swedish
				cookieMsg="This website uses cookies to improve functionality and performance. If you continue browsing the site, you are giving implied consent to the use of cookies on this website. See our <a href='http://www.rockwellautomation.com/rockwellautomation/legal-notices/overview.page?#/tab2' target='_blank'>Cookie Policy</a> for details.";
				cookieBtn="Don't show this message again";
				break;
			case "tr_TR": //Turkish
				cookieMsg="This website uses cookies to improve functionality and performance. If you continue browsing the site, you are giving implied consent to the use of cookies on this website. See our <a href='http://www.rockwellautomation.com/rockwellautomation/legal-notices/overview.page?#/tab2' target='_blank'>Cookie Policy</a> for details.";
				cookieBtn="Don't show this message again";
				break;
*/			default: //English
				cookieMsg="This website uses cookies to improve functionality and performance. If you continue browsing the site, you are giving implied consent to the use of cookies on this website. See our <a href='http://www.rockwellautomation.com/rockwellautomation/legal-notices/overview.page?#/tab2' target='_blank'>Cookie Policy</a> for details.";
				cookieBtn="Don't show this message again";
		}
		document.write('<div id="cookieNotice"><div class="cookie-notice-dialog"><p>'+cookieMsg+'</p><p><a class="cta" href="javascript:void(0);" onclick="setEuCookie();">'+cookieBtn+'</a></p></div></div>');
	}
}
function setEuCookie() {
	document.getElementById('cookieNotice').style.display = 'none'; // hide banner
	Set_Cookie('hide_cookie_banner','1','365','/','.rockwellautomation.com'); // store cookie to prevent showing banner in future
}
//END EU COOKIE BANNER


//BEGIN LANGUAGE SELECTOR
function changeLanguage(selectedOption) {
	if ( selectedOption == "" ) return false;
	var currPage = window.parent.location.href;
	var urlRoot = currPage.replace(/https?:\/\/[^\/]+\/?([^\/]*).*/,"$1");
	var siteNames = [ "fr_BE","nl_BE","en_CAR","es_CAR","de_CH","fr_CH" ];
	if ( currPage.match(/.+\/sites\//) ) {
		var newPage = currPage.replace(/(.+\/sites\/)[^\/]+(\/.+)/,"$1"+selectedOption+"$2"); //Preview with full Site name (replace sitename)
	} else if ( urlRoot != "" && siteNames.toString().indexOf(urlRoot) != -1 ) { 
		var newPage = currPage.replace(/https?:\/\/[^\/]+\/[^\/]+(\/?.*)/,"/"+selectedOption+"$1"); //Runtime with full Site name (replace sitename)
	} else {
		var newPage = currPage.replace(/(https?:\/\/[^\/]+)(\/*.*)/,"$1/"+selectedOption+"$2"); //Anything else (insert sitename)
	}
	window.parent.location.href = newPage;
}
//END LANGUAGE SELECTOR


//BEGIN ALTERNATE TABLE ROW HIGHLIGHT
$(document).ready(function(){
	$("table.data tr:nth-child(even)").addClass("even");
	$("#alltabs table tr:nth-child(even)").addClass("even");
	$("#C11 #content table table.data tr:nth-child(even)").addClass("even");
});
//END ALTERNATE TABLE ROW HIGHLIGHT


//BEGIN BAD LINK
function reportlink(code) {
	var to = 'http://www.rockwellautomation.com/resources/scripts/rockwellautomation/RAWebContact@ra.rockwell.com';
	var subject = null;
	switch (code) {
		case 401: subject = '401-Web Page Authorization Required'; break;
		case 403: subject = '403-Forbidden Web Page or Directory'; break;
		case 404: subject = '404-Web Page Not Found'; break;
		case 405: subject = '405-Website Method Not Allowed'; break;
		case 408: subject = '408-Website Request Timeout'; break;
		case 500: subject = '500-Website Experiencing Internal Server Error'; break;
		case 501: subject = '501-Request Not Implemented'; break;
		case 502: subject = '502-Server Experiencing Bad Gateway'; break;
		case 503: subject = '503-Website Service unavailable'; break;
		default: subject = 'Unexpected Error';
	}
	var badURL = window.location.href;
	var from = document.referrer;
	var message1 = 'I would like to report a ' + code + ' error on this page:  ' + badURL;
	var message2 = 'I linked to this page from:  ' + from;
	var message3 = 'Thank you for addressing this matter in a timely fashion.';
	if (from != "") {
		var ad = 'mailto:' + to + '?subject=' + subject + '&body=Dear Webmaster:%0a' + message1 + '.%0a' + message2 + '.%0a' + message3;
	} else {
		var ad = 'mailto:' + to + '?subject=' + subject + '&body=Dear Webmaster:%0a' + message1 + '.%0a' + message3;
	}
	document.location = ad ; 
}
//END BAD LINK


//HIDE AND SHOW TOGGLE - NEW DECEMBER 2014 - SUE
$(document).ready(function() {
	$(".toggle").hide()     
	$("a.togglelink").click(function(event) {
		$(this).siblings().removeClass('active'); 
		$(this).addClass('active');
		event.preventDefault();
		$(".toggle").hide();
		var relatedDivID = $(this).attr('href');
		$("" + relatedDivID).show('fast'); 
	});
	$('a.active').trigger('click');
});

//BEGIN IN-PAGE TOGGLE
function toggle(el) {
	myEl = document.getElementById(el);
	myEl.style.display = (myEl.style.display == 'block') ? 'none' : 'block';
}

// showSection function. designed to be run at body.onload. toggles div(s) referenced by # in URL. if multiple divs targeted, separate by -s. eg. #div1-div2-div3
function opentoggle() {
var loc = window.location.href;
	if (loc.lastIndexOf('#') != -1) {
		var section = loc.substring(loc.lastIndexOf('#')+1);
		var secs = section.split('-');
		if (secs.length > 1) {
			for (var i = 0; i< secs.length; i++ ){ toggle(secs[i]); }
		} else {
			toggle(section);
		}
	}
	window.attachEvent('onload', toggle);
}
//END IN-PAGE TOGGLE


//BEGIN C5 NAV TOGGLE
function navtoggle(el) {
	myEl = document.getElementById(el);
	myEl.style.display = (myEl.style.display == 'block') ? 'none' : 'block';
	myElHdg = document.getElementById(el+'head');
	myElHdg.className = (myElHdg.className == 'open') ? 'closed' : 'open';
}
//END C5 NAV TOGGLE

//*****************IMPORTANT!**************
//WOULD THE OWNER OF SEARCH AS YOU TYPE CODE PLEASE COME FORTH?  NEITHER BLAIR NOR SUE KNOW ANYTHING ABOUT THIS CODE.
//BEGIN SEARCH-AS-YOU-TYPE
/**
 * HTML element names for the search form, the spellchecking suggestion, and the
 * cluster suggestions. The search form must have the following input elements:
 * "q" (for search box), "site", "client".
 * @type {string}
 */
var ss_form_element = 'suggestion_form'; // search form
/**
 * Name of search suggestion drop down.
 * @type {string}
 */
var ss_popup_element = 'search_suggest'; // search suggestion drop-down
/**
 * Types of suggestions to include.  Just one options now, but reserving the
 * code for more types
 *   g - suggest server
 * Array sequence determines how different suggestion types are shown.
 * Empty array would effectively turn off suggestions.
 * @type {object}
 */
var ss_seq = [ 'g' ];
/**
 * Suggestion type name to display when there is only one suggestion.
 * @type {string}
 */
var ss_g_one_name_to_display =
    "Suggestion";
/**
 * Suggestion type name to display when there are more than one suggestions.
 * @type {string}
 */
var ss_g_more_names_to_display =
    "Suggestions";
/**
 * The max suggestions to display for different suggestion types.
 * No-positive values are equivalent to unlimited.
 * For key matches, -1 means using GSA default (not tagging numgm parameter),
 * 0 means unlimited.
 * Be aware that GSA has a published max limit of 10 for key matches.
 * @type {number}
 */
var ss_g_max_to_display = 10;
/**
 * The max suggestions to display for all suggestion types.
 * No-positive values are equivalent to unlimited.
 * @type {number}
 */
var ss_max_to_display = 12;
/**
 * Idling interval for fast typers.
 * @type {number}
 */
var ss_wait_millisec = 300;
/**
 * Delay time to avoid contention when drawing the suggestion box by various
 * parallel processes.
 * @type {number}
 */
var ss_delay_millisec = 30;
/**
 * Host name or IP address of GSA.
 * Null value can be used if the JS code loads from the GSA.
 * For local test, use null if there is a <base> tag pointing to the GSA,
 * otherwise use the full GSA host name
 * @type {string}
 */
var ss_gsa_host = "origin-ab.rockwellautomation.com/RockwellServletProxy"; 
var ss_gsa_port = "";
/**
 * Constant that represents legacy output format.
 * @type {string}
 */
var SS_OUTPUT_FORMAT_LEGACY = 'legacy';
/**
 * Constant that represents OpenSearch output format.
 * @type {string}
 */
var SS_OUTPUT_FORMAT_OPEN_SEARCH = 'os';
/**
 * Constant that represents rich output format.
 * @type {string}
 */
var SS_OUTPUT_FORMAT_RICH = 'rich';
/**
 * What suggest request API to use.
 *   legacy - use current protocol in 6.0
 *            Request: /suggest?token=<query>&max_matches=<num>&use_similar=0
 *            Response: [ "<term 1>", "<term 2>", ..., "<term n>" ]
 *                   or
 *                      [] (if no result)
 *   os -     use OpenSearch protocol
 *            Request: /suggest?q=<query>&max=<num>&site=<collection>&client=<frontend>&access=p&format=os
 *            Response: [
 *                        "<query>",
 *                        [ "<term 1>", "<term 2>", ... "<term n>" ],
 *                        [ "<content 1>", "<content 2>", ..., "<content n>" ],
 *                        [ "<url 1>", "<url 2>", ..., "<url n>" ]
 *                      ] (where the last two elements content and url are optional)
 *                   or
 *                      [ <query>, [] ] (if no result)
 *   rich -   use rich protocol from search-as-you-type
 *            Request: /suggest?q=<query>&max=<num>&site=<collection>&client=<frontend>&access=p&format=rich
 *            Response: {
 *                        "query": "<query>",
 *                        "results": [
 *                          { "name": "<term 1>", "type": "suggest", "content": "<content 1>", "style": "<style 1>", "moreDetailsUrl": "<url 1>" },
 *                          { "name": "<term 2>", "type": "suggest", "content": "<content 2>", "style": "<style 2>", "moreDetailsUrl": "<url 2>" },
 *                          ...,
 *                          { "name": "<term n>", "type": "suggest", "content": "<content n>", "style": "<style n>", "moreDetailsUrl": "<url n>" }
 *                        ]
 *                      } (where type, content, style, moreDetailsUrl are optional)
 *                   or
 *                      { "query": <query>, "results": [] } (if no result)
 * If unspecified or null, using legacy protocol.
 * @type {string}
 */
var ss_protocol = SS_OUTPUT_FORMAT_RICH;
/**
 * Whether to allow non-query suggestion items.
 * Setting it to false can bring results from "os" and "rich" responses into
 * backward compatible with "legacy".
 * @type {boolean}
 */
var ss_allow_non_query = true;
/**
 * Default title text when the non-query suggestion item does not have a useful
 * title.
 * The default display text should be internalionalized.
 * @type {string}
 */
var ss_non_query_empty_title =
    "No Title";
/**
 * Default title text when the non-query suggestion item does not have a useful
 * title.
 * The default display text should be internalionalized.
 * @type {string}
 */
var ss_remote = false;
/**
 * Whether debugging is allowed.  If so, toggle with F2 key.
 * @type {boolean}
 */
var ss_allow_debug = true;
//END SEARCH-AS-YOU-TYPE


//BEGIN PRINT THIS PAGE
(function(jQuery) {
    jQuery.fn.printPage = function() {
       return this.each(function() {
            // Wrap each element in a <a href="#">...</a> tag
            var $current = jQuery(this);
            $current.wrapInner('<a href="#"></a>');
            
            jQuery('span.print > a').click(function() {
                window.print(); 
                return false;    
            });
       });
    }
})(jQuery);
/*
addPrintLink function by Roger Johansson, www.456bereastreet.com
*/
var addPrintLink = {
	init:function(sTargetEl,sLinkText) {
		if (!document.getElementById || !document.createTextNode) {return;} // Check for DOM support
		if (!document.getElementById(sTargetEl)) {return;} // Check that the target element actually exists
		if (!window.print) {return;} // Check that the browser supports window.print
		var oTarget = document.getElementById(sTargetEl);
		var oLink = document.createElement('a');
		oLink.id = 'print-link'; // Give the link an id to allow styling
		oLink.href = '#'; // Make the link focusable for keyboard users
		oLink.appendChild(document.createTextNode(sLinkText));
		oLink.onclick = function() {window.print(); return false;} // Return false prevents the browser from following the link and jumping to the top of the page after printing
		oTarget.appendChild(oLink);
	},
	/*
	addEvent function included here for portability. Replace with your own addEvent function if you use one.
	addEvent function from http://www.quirksmode.org/blog/archives/2005/10/_and_the_winner_1.html
	*/
	addEvent:function(obj, type, fn) {
		if (obj.addEventListener)
			obj.addEventListener(type, fn, false);
		else if (obj.attachEvent) {
			obj["e"+type+fn] = fn;
			obj[type+fn] = function() {obj["e"+type+fn](window.event);}
			obj.attachEvent("on"+type, obj[type+fn]);
		}
	}
};
addPrintLink.addEvent(window, 'load', function(){addPrintLink.init('article','');});
//END PRINT THIS PAGE


$(document).ready(function(){
	$(".dropdown-menu").hide();
	$("#share-drop").show();
	$('#share-drop').click(function(){
		$(".dropdown-menu").toggle();
	});

	var selector = '.share-list li.dropdown';

//	$('#share-drop').on('click', function(){
//		$(selector).toggleClass('active');
//	});

	$("html").click(function(event){
		var selector = '.share-list li';
		var otarget = $(event.target);
		if (!otarget.parents('.dropdown-menu').length && otarget.attr('class')!="dropdown-menu" && !otarget.parents('#share-drop').length) {
			$('.dropdown-menu').hide();
			$(selector).removeClass('active');
		}
	});
});
