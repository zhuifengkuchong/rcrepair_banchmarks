function performSearch() {
    searchHTML = "/search.html?" + $("input:text[name=username]").val();  //search terms
    window.location.href = searchHTML;
}
function processInput() {
    //Handler for carriage return (simulate form submit)
    document.onkeyup = function(e){     
        if (e == null) { // ie
            keycode = event.keyCode;
        } else { // mozilla
            keycode = e.which;
        }
        
        //Keycode 13 = return key
        if(keycode == 13){ // close
            performSearch();
        }    
    };
}
function clearInput() {
    //Clear ONLY default text on keypress (leave text if not default)
    if ($("input:text[name=username]").val() == "How can we help today?") {
        $("input:text[name=username]").val("");
    }
}