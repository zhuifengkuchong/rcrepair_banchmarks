
var items = new Array("chiclet-mask.png"/*tpa=http://www.mattel.com/files/nav/chiclet-mask.png*/,
						 "chiclet_barbie3.png"/*tpa=http://www.mattel.com/files/nav/chiclet_barbie3.png*/,
						 "chiclet_ag3.png"/*tpa=http://www.mattel.com/files/nav/chiclet_ag3.png*/,
						 "chiclet_teamHW3.png"/*tpa=http://www.mattel.com/files/nav/chiclet_teamHW3.png*/,
						 "chiclet_fijit3.png"/*tpa=http://www.mattel.com/files/nav/chiclet_fijit3.png*/,
						 "chiclet_iCanBe3.png"/*tpa=http://www.mattel.com/files/nav/chiclet_iCanBe3.png*/,
						 "chiclet_bf53.png"/*tpa=http://www.mattel.com/files/nav/chiclet_bf53.png*/,
						 "chiclet_hw3.png"/*tpa=http://www.mattel.com/files/nav/chiclet_hw3.png*/,
						 "chiclet_mh3.png"/*tpa=http://www.mattel.com/files/nav/chiclet_mh3.png*/,
						 "chiclet_polly3.png"/*tpa=http://www.mattel.com/files/nav/chiclet_polly3.png*/,
						 "chiclet_maxsteel3.png"/*tpa=http://www.mattel.com/files/nav/chiclet_maxsteel3.png*/,
						 "chiclet_fisherPrice3.png"/*tpa=http://www.mattel.com/files/nav/chiclet_fisherPrice3.png*/,
						 "chiclet_matchbox3.png"/*tpa=http://www.mattel.com/files/nav/chiclet_matchbox3.png*/,
						 "chiclet_polly3.png"/*tpa=http://www.mattel.com/files/nav/chiclet_polly3.png*/);

for (var i = 0; i < items.length; i++) {
  img = new Image();
  img.src = items[i];
}



jQuery(document).ready(function(){



$(".jcarousellite").jCarouselLite({
      btnNext: ".next",
      btnPrev: ".prev",
      visible: 8,
      beforeStart: function(){
    	  $('.chiclet').css('backgroundPosition', '0 -50px');
        $('#chiclet-mask').css('z-index', 999);  
        $('#chiclet-mask').css('display', 'block');  
        
        $('#globalmycarousel li').css('background', '#AAB0BC'); 
        
       
      },
      afterEnd: function(){
    	$('#globalmycarousel li').css('background', 'none'); 
    	$('.chiclet').css('backgroundPosition', '0 0');
    	 $('#chiclet-mask').css('display', 'none');  
    	$('#chiclet-mask').css('z-index', 4); 
       
     },
  //   easing: "backinout",
     speed: 500
});



$("#globalmycarousel li").hover(
function(){



	var p = $(this);
	var position = p.offset();

	var spanelem = document.getElementById("spanelem"); 
	spanelem.style.marginLeft = 42*1+position.left*1+"px";
	spanelem.style.marginTop = position.top+"px";
	spanelem.innerHTML = "<span>"+ $(this).find('span').html() +"</span>";
	spanelem.style.display = 'block';
}, 
function(){
	
	
	//alert("image1: "+origenverified);

	//alert("image2: "+origenverified);
	var spanelem = document.getElementById("spanelem"); 
	spanelem.style.display = 'none';
		
	//$(this).find("span").animate({opacity: "hide", left: "40"}, "fast");	
	
});


$("#leftside span").hover(
function(){
	var iconName = $(this).find("img").attr("src");
	//alert(iconName);
	var origen = iconName.split(".")[2];
	//alert(origen);
	var origenFinal = origen.split("/")[4];
	//alert(origenFinal);
	$(this).find("img").attr({src: "http://corporate.mattel.com/mdn/mattel-top-nav-test/images/" + origenFinal + "R.png"});
}, 
function(){
	var iconName = $(this).find("img").attr("src");
	//alert(iconName);
	var origen = iconName.split("R.")[0];
	//alert(origen);
	var origenFinal = origen.split("/")[6];
	//alert(origenFinal);
	$(this).find("img").attr({src: "http://corporate.mattel.com/mdn/mattel-top-nav-test/images/" + origenFinal + ".png"});
});


$("#rightside span").hover(
function(){
	var iconName = $(this).find("img").attr("src");
		//alert(iconName);
	var origen = iconName.split(".")[2];
//	alert(origen);
	var origenFinal = origen.split("/")[4];
//	alert(origenFinal);
	$(this).find("img").attr({src: "http://corporate.mattel.com/mdn/mattel-top-nav-test/images/" + origenFinal + "R.png"});
}, 
function(){
	var iconName = $(this).find("img").attr("src");
//	alert(iconName);
	var origen = iconName.split("R.")[0];
//	alert(origen);
	var origenFinal = origen.split("/")[6];
//	alert(origenFinal);
	$(this).find("img").attr({src: "http://corporate.mattel.com/mdn/mattel-top-nav-test/images/" + origenFinal + ".png"});
});

$(".jcarousel-prev, .jcarousel-next").disableTextSelect();

});



/**
 * .disableTextSelect - Disable Text Select Plugin
 *
 * Version: 1.1
 * Updated: 2007-11-28
 *
 * Used to stop users from selecting text
 *
 * Copyright (c) 2007 James Dempster (letssurf@gmail.com, http://www.jdempster.com/category/jquery/disabletextselect/)
 *
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 **/

/**
 * Requirements:
 * - jQuery (John Resig, http://www.jquery.com/)
 **/
(function($) {
    if ($.browser.mozilla) {
        $.fn.disableTextSelect = function() {
            return this.each(function() {
                $(this).css({
                    'MozUserSelect' : 'none'
                });
            });
        };
        $.fn.enableTextSelect = function() {
            return this.each(function() {
                $(this).css({
                    'MozUserSelect' : ''
                });
            });
        };
    } else if ($.browser.msie) {
        $.fn.disableTextSelect = function() {
            return this.each(function() {
                $(this).bind('selectstart.disableTextSelect', function() {
                    return false;
                });
            });
        };
        $.fn.enableTextSelect = function() {
            return this.each(function() {
                $(this).unbind('selectstart.disableTextSelect');
            });
        };
    } else {
        $.fn.disableTextSelect = function() {
            return this.each(function() {
                $(this).bind('mousedown.disableTextSelect', function() {
                    return false;
                });
            });
        };
        $.fn.enableTextSelect = function() {
            return this.each(function() {
                $(this).unbind('mousedown.disableTextSelect');
            });
        };
    }
})(jQuery);