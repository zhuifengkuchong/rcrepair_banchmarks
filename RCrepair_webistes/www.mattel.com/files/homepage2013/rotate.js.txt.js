jQuery.noConflict();
var numCover = 1;
var rand = 0;

jQuery(document).ready(function(){
		numCover = jQuery(".m_cover").length;
		setCover();
});

function setCover() {
	
	if(numCover != 1) {
		var rand = Math.floor(Math.random()*numCover);
		select_cover = rand + 1;
		swapCover(select_cover);
		jQuery('#cover-control').show();
	}
	else {
		jQuery('#cover-control').hide();
		jQuery('#feature_div').show();
		jQuery('#fp-cover-1').show();
	}

}

function swapCover(number) {
	 
		var coverX = '#fp-cover-';
		var tabX = '#tab-';
		var textX = '#cdaText-';
		var cdaText = '.cda-thumbnail-text';
		var coverDiv =  coverX + number ;
		var tabDiv = tabX + number;
		var textDiv = textX + number;
		var cdaTextActicve = cdaText + number;

		for (i=1;i<= numCover;i++) {
				jQuery(tabX + i).removeClass("selected");
				jQuery(tabX + i).show();
				jQuery(tabX + i).css('background', 'url("../homepage2012/bg/cda-thumb-gradient.png"/*tpa=http://www.mattel.com/files/homepage2012/bg/cda-thumb-gradient.png*/)');
				jQuery(tabX + i).css('background-color', '#FFF');
				jQuery(cdaText + i).css('color', '#FFF');
				jQuery(coverX + i).hide();
				jQuery(textX + i).hide();
		}
		jQuery(tabDiv).addClass("selected");
		jQuery(tabDiv).css('background', 'none');
		jQuery(tabDiv).css('background-color', '#FFF');
		jQuery(cdaTextActicve).css('color', '#333333');
		jQuery(coverDiv).show();
		jQuery(textDiv).show();
}

function rotateCDAImage(pos) {

	swapCover(pos);
	pos = pos + 1;
	if(pos == 6)    pos = 1;
	t = setTimeout("rotateCDAImage("+pos+")", 10000);
	
}
