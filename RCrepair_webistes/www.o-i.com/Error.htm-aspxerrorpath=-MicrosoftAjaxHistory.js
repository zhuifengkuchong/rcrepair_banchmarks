﻿<!DOCTYPE html>
<!--[if IE 6 ]> <html xml:lang="en" lang="en" class="ie6" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 7 ]> <html xml:lang="en" lang="en" class="ie7" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 8 ]> <html xml:lang="en" lang="en" class="ie8" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if !(IE 8)]> <html lang="en"><![endif]-->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=100; chrome=1" />
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />
    <title>Error</title>
    <link media="screen" href="/uploadedfiles/_Assets/css/global.css" rel="stylesheet" />
    <link media="print" href="/uploadedfiles/_Assets/css/print.css" rel="stylesheet" />
</head>
<body>
    <form id="form1">
    <div id="page-bkg" style='background-image: url("/uploadedImages/_Assets/images/page_backgrounds/o-i_background_images_default.jpg")'>
    </div>
    <div id="page">
        <div id="nav-main">
            <a title="Home" href="/" id="logo-oi"><span class="image-rep">Home</span></a>
            <ul>
                <li class="mainnav-off"><a title="About O-I" href="/About-O-I/" class="mainnav-off-parent">
                    <span>About O-I</span></a><ul>
                        <li><a title="Our Story" href="/About-O-I/Our-Story/"><span>Our Story</span></a></li><li>
                            <a title="Company Facts" href="/About-O-I/Company-Facts/"><span>Company Facts</span></a></li><li>
                                <a title="Capabilities" href="/About-O-I/Capabilities/"><span>Capabilities</span></a></li><li>
                                    <a title="Leadership" href="/About-O-I/Leadership/"><span>Leadership</span></a></li></ul>
                </li>
                <li class="mainnav-off"><a title="Why Glass" href="/Why-Glass/" class="mainnav-off-parent">
                    <span>Why Glass</span></a><ul>
                        <li><a title="Glass Is Life" href="/Why-Glass/Glass-Is-Life/"><span>Glass Is Life</span></a></li><li>
                            <a title="How Glass Is Made" href="/Why-Glass/How-Glass-Is-Made/"><span>How Glass Is
                                Made</span></a></li></ul>
                </li>
                <li class="mainnav-off"><a title="Product Solutions" href="/Product-Solutions/" class="mainnav-off-parent">
                    <span>Product Solutions</span></a><ul>
                        <li><a title="Beer" href="/Product-Solutions/Beer/"><span>Beer</span></a></li><li><a
                            title="Wine" href="/Product-Solutions/Wine/"><span>Wine</span></a></li><li><a title="Spirits"
                                href="/Product-Solutions/Spirits/"><span>Spirits</span></a></li><li><a title="Non-Alcoholic Beverages"
                                    href="/Product-Solutions/Non-Alcoholic/"><span>Non-Alcoholic Beverages</span></a></li><li>
                                        <a title="Pharmaceutical &amp; Chemical" href="/Product-Solutions/Pharmaceutical---Chemical/">
                                            <span>Pharmaceutical &amp; Chemical</span></a></li><li><a title="Food" href="/Product-Solutions/Food/">
                                                <span>Food</span></a></li><li><a title="Tableware" href="/Product-Solutions/Tableware/">
                                                    <span>Tableware</span></a></li><li><a title="Specialty &amp; Limited" href="/Product-Solutions/Specialty---Limited/">
                                                        <span>Specialty &amp; Limited</span></a></li></ul>
                </li>
                <li class="mainnav-off"><a title="Sustainability" href="/Sustainability/" class="mainnav-off-parent">
                    <span>Sustainability</span></a><ul>
                        <li><a title="Life Cycle Assessment" href="/Sustainability/Life-Cycle-Assessment/"><span>
                            Life Cycle Assessment</span></a></li><li><a title="Sustainability Goals" href="/Sustainability/Sustainability-Goals/">
                                <span>Sustainability Goals</span></a></li></ul>
                </li>
                <li class="mainnav-off"><a title="Investors" href="/Investors/" class="mainnav-off-parent">
                    <span>Investors</span></a><ul>
                        <li><a title="Company News" href="http://investors.o-i.com/phoenix.zhtml?c=88324&amp;p=irol-news&amp;nyo=0">
                            <span>Company News</span></a></li><li><a title="SEC Filings" href="http://investors.o-i.com/phoenix.zhtml?c=88324&amp;p=irol-sec">
                                <span>SEC Filings</span></a></li><li><a title="Annual Reports" href="http://investors.o-i.com/phoenix.zhtml?c=88324&amp;p=irol-reportsAnnual">
                                    <span>Annual Reports</span></a></li><li><a title="Webcasts and Presentations" href="http://investors.o-i.com/phoenix.zhtml?c=88324&amp;p=irol-calendar">
                                        <span>Webcasts and Presentations</span></a></li><li><a title="Stock Information"
                                            href="http://investors.o-i.com/phoenix.zhtml?c=88324&amp;p=irol-stockQuote"><span>Stock
                                                Information</span></a></li><li><a title="Analysts" href="http://investors.o-i.com/phoenix.zhtml?c=88324&amp;p=irol-analysts">
                                                    <span>Analysts</span></a></li><li><a title="Corporate Governance" href="http://investors.o-i.com/phoenix.zhtml?c=88324&amp;p=irol-govHighlights">
                                                        <span>Corporate Governance</span></a></li><li><a title="Investor FAQs" href="http://investors.o-i.com/phoenix.zhtml?c=88324&amp;p=irol-faq">
                                                            <span>Investor FAQs</span></a></li></ul>
                </li>
                <li class="mainnav-off"><a title="Newsroom" href="/Newsroom/" class="mainnav-off-parent">
                    <span>Newsroom</span></a><ul>
                        <li><a title="News Release Archives" href="/News-Archives/"><span>News Release Archives</span></a></li></ul>
                </li>
                <li class="mainnav-off"><a title="Careers" href="/Careers/" class="mainnav-off-parent">
                    <span>Careers</span></a><ul style="display: none; opacity: 0; width: 0px;">
                        <li><a title="Job Openings" href="/Careers/Job-Openings/"><span>Job Openings</span></a></li></ul>
                </li>
                <li class="mainnav-off"><a title="Contacts &amp; Locations" href="/Contacts-and-Locations/"
                    class="mainnav-off-parent"><span>Contacts &amp; Locations</span></a><ul style="display: none;
                        opacity: 0; width: 0px;">
                        <li><a title="Corporate Contacts" href="/Contacts-and-Locations/Corporate-Contacts/">
                            <span>Corporate Contacts</span></a></li><li><a title="North America" href="/Contacts-and-Locations/North-America/">
                                <span>North America</span></a></li><li><a title="Latin America" href="/Contacts-and-Locations/Latin-America/">
                                    <span>Latin America</span></a></li><li><a title="Europe" href="/Contacts-and-Locations/Europe/">
                                        <span>Europe</span></a></li><li><a title="Asia Pacific" href="/Contacts-and-Locations/Asia-Pacific/">
                                            <span>Asia Pacific</span></a></li></ul>
                </li>
            </ul>
        </div>
        <div class="" id="page-content">
            <script type="text/javascript">
                //&lt;![CDATA[
                Sys.WebForms.PageRequestManager._initialize('ctl00$body$apScriptManager', 'aspnetForm', [], [], [], 90, 'ctl00');
                //]]&gt;
</script>
            <div id="subpage">
                <div id="subpage-header">
                    <h1>
                        O-I.com Error</h1>
                </div>
                <div id="subpage-content" style="height: 429px;">
                    <div class="single-content">
                        There has been an error with our site. Thank you for your patience as we fix it.
                    </div>
                </div>
            </div>
        </div>
        <div class="" id="footer">
            <div id="footer-content">
                <ul id="footer-left" class="clearfix">
                    <li class="footer-copyright">&copy; 2012 O-I</li>
                    <li><a href="/Privacy"><span>Privacy</span></a></li>
                    <li><a href="/Legal"><span>Legal</span></a></li>
                    <li><a class="last" href="/Contacts-and-Locations"><span>Contacts &amp; Locations</span></a></li>
                </ul>
                <ul id="footer-right" class="clearfix">
                    <li><a title="Language" href="#" id="anchor-language">English</a></li>
                    <li><span>NYSE:</span> <span class="white" id="stockQuote"><strong>OI</strong></span>
                    </li>
                    <li><a title="Site Map" href="#" id="anchor-sitemap">Site Map</a></li>
                </ul>
            </div>
            <div id="footer-sitemap" class="clearfix">
                <div class="clearfix">
                    <ul>
                        <li><a title="About O-I" href="/About-O-I/"><strong>About O-I</strong></a></li><li><a
                            title="Our Story" href="/About-O-I/Our-Story/">Our Story</a></li><li><a title="Company Facts"
                                href="/About-O-I/Company-Facts/">Company Facts</a></li><li><a title="Capabilities"
                                    href="/About-O-I/Capabilities/">Capabilities</a></li><li><a title="Leadership" href="/About-O-I/Leadership/">
                                        Leadership</a></li></ul>
                    <ul>
                        <li><a title="Why Glass" href="/Why-Glass/"><strong>Why Glass</strong></a></li><li><a
                            title="Glass Is Life" href="/Why-Glass/Glass-Is-Life/">Glass Is Life</a></li><li><a
                                title="How Glass Is Made" href="/Why-Glass/How-Glass-Is-Made/">How Glass Is Made</a></li></ul>
                    <ul>
                        <li><a title="Product Solutions" href="/Product-Solutions/"><strong>Product Solutions</strong></a></li><li>
                            <a title="Beer" href="/Product-Solutions/Beer/">Beer</a></li><li><a title="Wine"
                                href="/Product-Solutions/Wine/">Wine</a></li><li><a title="Spirits" href="/Product-Solutions/Spirits/">
                                    Spirits</a></li><li><a title="Non-Alcoholic Beverages" href="/Product-Solutions/Non-Alcoholic/">
                                        Non-Alcoholic Beverages</a></li><li><a title="Pharmaceutical &amp; Chemical" href="/Product-Solutions/Pharmaceutical---Chemical/">
                                            Pharmaceutical &amp; Chemical</a></li><li><a title="Food" href="/Product-Solutions/Food/">
                                                Food</a></li><li><a title="Tableware" href="/Product-Solutions/Tableware/">Tableware</a></li><li>
                                                    <a title="Specialty &amp; Limited" href="/Product-Solutions/Specialty---Limited/">Specialty
                                                        &amp; Limited</a></li></ul>
                    <ul>
                        <li><a title="Sustainability" href="/Sustainability/"><strong>Sustainability</strong></a></li><li>
                            <a title="Life Cycle Assessment" href="/Sustainability/Life-Cycle-Assessment/">Life
                                Cycle Assessment</a></li><li><a title="Sustainability Goals" href="/Sustainability/Sustainability-Goals/">
                                    Sustainability Goals</a></li></ul>
                </div>
                <div class="clearfix">
                    <ul>
                        <li><a title="Investors" href="/Investors/"><strong>Investors</strong></a></li><li><a
                            title="Company News" href="http://investors.o-i.com/phoenix.zhtml?c=88324&amp;p=irol-news&amp;nyo=0">
                            Company News</a></li><li><a title="SEC Filings" href="http://investors.o-i.com/phoenix.zhtml?c=88324&amp;p=irol-sec">
                                SEC Filings</a></li><li><a title="Annual Reports" href="http://investors.o-i.com/phoenix.zhtml?c=88324&amp;p=irol-reportsAnnual">
                                    Annual Reports</a></li><li><a title="Webcasts and Presentations" href="http://investors.o-i.com/phoenix.zhtml?c=88324&amp;p=irol-calendar">
                                        Webcasts and Presentations</a></li><li><a title="Stock Information" href="http://investors.o-i.com/phoenix.zhtml?c=88324&amp;p=irol-stockQuote">
                                            Stock Information</a></li><li><a title="Analysts" href="http://investors.o-i.com/phoenix.zhtml?c=88324&amp;p=irol-analysts">
                                                Analysts</a></li><li><a title="Corporate Governance" href="http://investors.o-i.com/phoenix.zhtml?c=88324&amp;p=irol-govHighlights">
                                                    Corporate Governance</a></li><li><a title="Investor FAQs" href="http://investors.o-i.com/phoenix.zhtml?c=88324&amp;p=irol-faq">
                                                        Investor FAQs</a></li></ul>
                    <ul>
                        <li><a title="Newsroom" href="/Newsroom/"><strong>Newsroom</strong></a></li><li><a
                            title="News Release Archives" href="/News-Archives/">News Release Archives</a></li></ul>
                    <ul>
                        <li><a title="Careers" href="/Careers/"><strong>Careers</strong></a></li><li><a title="Job Openings"
                            href="/Careers/Job-Openings/">Job Openings</a></li></ul>
                    <ul>
                        <li><a title="Contacts &amp; Locations" href="/Contacts-and-Locations/"><strong>Contacts
                            &amp; Locations</strong></a></li><li><a title="Corporate Contacts" href="/Contacts-and-Locations/Corporate-Contacts/">
                                Corporate Contacts</a></li><li><a title="North America" href="/Contacts-and-Locations/North-America/">
                                    North America</a></li><li><a title="Latin America" href="/Contacts-and-Locations/Latin-America/">
                                        Latin America</a></li><li><a title="Europe" href="/Contacts-and-Locations/Europe/">Europe</a></li><li>
                                            <a title="Asia Pacific" href="/Contacts-and-Locations/Asia-Pacific/">Asia Pacific</a></li></ul>
                </div>
                <div id="footer-viewall">
                    <a href="/Site-Map/"><strong>View full site map »</strong></a>
                </div>
            </div>
            <div id="footer-language" class="clearfix">
                <div id="region-select">
                    <h4>
                        Select a Region and Language</h4>
                    <div id="select-region-dd">
                        <div title="Region" id="select-region-btn">
                            Region</div>
                        <div style="display: none;" id="select-region-list">
                            <span class="soft">Region</span><ul>
                                <li title="North America">North America</li><li title="Europe">Europe</li><li title="Latin America">
                                    Latin America</li><li title="Asia Pacific">Asia Pacific</li></ul>
                        </div>
                    </div>
                    <div id="select-language-dd">
                        <div title="Language" class="off" id="select-language-btn">
                            Language</div>
                        <div style="display: none;" id="select-language-list">
                            <span class="soft">Language</span><ul id="North America">
                                <li langid="1033" title="English">English</li><li langid="3084" title="French (Canada)">
                                    French (Canada)</li></ul>
                            <ul id="Europe">
                                <li langid="2057" title="English">English</li><li langid="1029" title="Czech">Czech</li><li
                                    langid="1043" title="Dutch">Dutch</li><li langid="1061" title="Estonian">Estonian</li><li
                                        langid="1036" title="French (France)">French (France)</li><li langid="1031" title="German">
                                            German</li><li langid="1038" title="Hungarian">Hungarian</li><li langid="1040" title="Italian">
                                                Italian</li><li langid="1045" title="Polish">Polish</li><li langid="2070" title="Portuguese (Portugal)">
                                                    Portuguese (Portugal)</li><li langid="1034" title="Spanish (Spain)">Spanish (Spain)</li></ul>
                            <ul id="Latin America">
                                <li langid="11274" title="Spanish (South America)">Spanish (South America)</li><li
                                    langid="1046" title="Portuguese (Brazil)">Portuguese (Brazil)</li></ul>
                            <ul id="Asia Pacific">
                                <li langid="3081" title="English">English</li><li langid="1057" title="Bahasan (Indonesia)">
                                    Bahasan (Indonesia)</li><li langid="2052" title="Simplified Chinese">Simplified Chinese</li></ul>
                        </div>
                    </div>
                    <div class="off" id="btn-go">
                        <a title="Go" href="javascript:;"><span>Go</span></a></div>
                </div>
            </div>
        </div>
    </div>
    <!--SCRIPTS-->
    <script type="text/javascript" src="/uploadedfiles/_Assets/js/plugins.js"></script>
    <script type="text/javascript" src="/uploadedfiles/_Assets/js/global.js"></script>
    <!--END SCRIPTS-->
    </form>
</body>
</html> 