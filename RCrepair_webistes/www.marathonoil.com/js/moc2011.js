function log(a){
try{
  //console.log(a);
  }catch(er){
   try{
     window.opera.postError(a);
     }catch(er){
     //no console avaliable. put
     //alert(a) here or write to a document node or just ignore
     }
  }
}

var timeOuts = [];
var dipHome = 0;
var activeSecondary = '';
var myHeight = 0;
var totalHeight = 0;
var resizeTimer;

String.prototype.parseURL = function () {
  return this.replace(/[A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&\?\/.=]+/, function (url) {
    return url.link(url);
  });
};

String.prototype.parseDate = function () {
  var s = this.split(/\D/);
  return new Date(Date.UTC(s[0], --s[1], s[2], s[3], s[4], s[5], s[6]))
}

String.prototype.formatDate = function() {
  var v = this.split(' '); var dte = new Date(Date.parse(v[1] + " " + v[2] + ", " + v[5] + " " + v[3] + " UTC"));
  return (dte.getMonth() + 1) + '/' + dte.getDate() + '/' + dte.getFullYear();
}

var resizeText = function (size) {
  var myCookie = jQuery.cookie('fontSize', size);
  redrawFonts(size);
}

var redrawFonts = function(size) {
  $body = jQuery("body").removeClass('resize0');
  $body.removeClass('resize1');
  $body.removeClass('resize2');
  $body.addClass('resize' + String(size));
}

var tw = function (data) {
  jQuery(".sideBarTwitterFeed").append('<img src="../../c3212742.r42.cf0.rackcdn.com/twitterTop.png"/*tpa=http://c3212742.r42.cf0.rackcdn.com/twitterTop.png*/ style="display:block;" /><div id="tweets" />');
  //jQuery(".sideBarTwitterFeed").append('<img src="Unknown_83_filename"/*tpa=http://marathon.brandextract.com/images/obfT.jpg*/ /><div id="tweets" />');
  data = data.slice(0,3);
  jQuery.each(data, function (i, item) {
    jQuery("<div/>").html('<div class="tweet"><p>' + String(item.text).parseURL() + '<br/>' + String(item.created_at).formatDate() + ' | <a href="http://twitter.com/intent/tweet?in_reply_to='+item.id_str+'">reply</a> | <a href="http://twitter.com/intent/retweet?tweet_id='+item.id_str+'">retweet</a> | <a href="http://twitter.com/intent/favorite?tweet_id='+item.id_str+'">favorite</p></div>').appendTo("#tweets");
  });
}

var emailPage = function() {
  temploc='http://www.marathonoil.com/About_Us/Contacts/Email_Page/?myURL=';
  temploc = temploc + window.location.href.replace(/\&/, '%26').replace(/\?/, '%3F');
  window.location.href = temploc;
}

var printPage = function() {
  window.print();
}

var showAccordionByID = function(thisAccordionID) {
  jQuery('#accordionTab'+thisAccordionID).addClass('active');
  jQuery('#accordionContent'+thisAccordionID).slideDown(200);
}

var hideAccordionByID = function(thisAccordionID) {
  jQuery('#accordionTab'+thisAccordionID).removeClass('active');
  jQuery('#accordionContent'+thisAccordionID).slideUp(200);
}

var accordionsOpen = function() {
  result = 0;
  jQuery("#inlineAccordion > div.inlineAccordionTab").each(function (i) {
    temp = jQuery(this).css("background-image");
    if (temp.indexOf('up') != -1) {
      result++;
    }
  });
  return result;
}

var fitList = function(id, height) {
  var $ul = jQuery(id).find('ul');
  while ($ul.outerHeight() > height) {
    $ul.find("li:last").remove();
  }
}

var updateCollapser = function() {
  if (jQuery("#inlineAccordion > div.inlineAccordionTab").length/2 >= accordionsOpen()) {
    jQuery("#expand").attr('src', '../images/spacer.gif'/*tpa=http://www.marathonoil.com/images/spacer.gif*/);
  } else {
    jQuery("#expand").attr('src', '../images/spacer.gif'/*tpa=http://www.marathonoil.com/images/spacer.gif*/);
  }
  if (accordionsOpen() == 1 || accordionsOpen() == 0) {
    jQuery("#accSpacer").height(myHeight);
  } else {
    jQuery("#accSpacer").height(totalHeight);
  }
}

var apikey='26d7b275ba37b30e29b36623f18fac85',userid='35458411@N04';

function getSets(){
  jQuery.getJSON('https://api.flickr.com/services/rest/?jsoncallback=?',{method:'flickr.photosets.getList',api_key:apikey,user_id:userid,format:'json'},function(obJ){
    var photoSets = obJ.photosets.photoset;
    jQuery('.galleryShowMore>.target').live('click', function() {
      jQuery(this).hide().next().show();
    });
    jQuery('.galleryThumb').live('click', function() {

    });
    jQuery(".flickerGallery").each( function() {
      var thisGallery, galleryName;
      $this = jQuery(this);
      galleryName = $this.attr('rel');
      for(i=0, j=photoSets.length;i<j;i++) {
        if(photoSets[i].title._content == galleryName) thisGallery = photoSets[i];
      }
      if (typeof(thisGallery) != "undefined") {
        $this.append('<h2>'+galleryName+'</h2><div class="galleryThumbs" rel="'+thisGallery.id+'"><img src="../images/loading.gif"/*tpa=http://www.marathonoil.com/images/loading.gif*/ /></div><div class="clear" />');
        jQuery.getJSON('https://api.flickr.com/services/rest/?jsoncallback=?',{method:'flickr.photosets.getPhotos',api_key:apikey,photoset_id:thisGallery.id,format:'json'},function(obj){
          $galleryDiv = jQuery('div[rel='+thisGallery.id+']');
          $galleryDiv.empty();
          var photoset = obj.photoset;
          for(i=0, mylength = photoset.photo.length;i<6 && i<mylength;i++) {
            thisImg = photoset.photo[i];
            $galleryDiv.append('<div class="galleryThumb"><a title="'+thisImg.title+'" href="http://farm'+thisImg.farm+'.static.flickr.com/'+thisImg.server+'/'+thisImg.id+'_'+thisImg.secret+'.jpg'+'" class="overlay"><img alt="'+thisImg.title+'" src="'+'http://farm'+thisImg.farm+'.static.flickr.com/'+thisImg.server+'/'+thisImg.id+'_'+thisImg.secret+'_s.jpg'+'" class="galleryThumb" /></a></div>');
          }
          if (photoset.photo.length > 6) {
            $galleryDiv.append('<div class="galleryShowMore clear"><div class="target">Show All</div><div class="moreImages"></div></div>');
            for(i=6, mylength = photoset.photo.length;i<mylength;i++) {
            thisImg = photoset.photo[i];
            $galleryDiv.find('.moreImages').append('<div class="galleryThumb"><a title="'+thisImg.title+'" href="'+'http://farm'+thisImg.farm+'.static.flickr.com/'+thisImg.server+'/'+thisImg.id+'_'+thisImg.secret+'.jpg'+'" class="overlay"><img src="'+'http://farm'+thisImg.farm+'.static.flickr.com/'+thisImg.server+'/'+thisImg.id+'_'+thisImg.secret+'_s.jpg'+'" class="galleryThumb" /></a></div>');
          }
          }
        });
      }
    });
  });
}

var parseYtId = function (url) {
  var regexp = /.*(?:youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/;
  var match = url.match(regexp);
    if (match&&match[1].length==11){
        return match[1];
    }
    return null;
}

var MONTHS = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
var YOUTUBE_PATH = "http://www.youtube.com/v/";

/* Callback from SPAS */
var JSONCallback = function (data) {
  yt(data.youtube.result);
  ytHome(data.youtube.result);
  tw(data.tweets.result);
}

function trim(str, maxLength) {
  maxLength -= 3; // for the three dots
  //trim the string to the maximum length
  var trimmedString = str.substr(0, maxLength);
  
  //re-trim if we are in the middle of a word
  return trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" "))) + "...";
}

/* YouTube Data Call Back */
var yt = function (data) {
  var myLength = data.length;
  jQuery(".embedYouTubeVideo").each( function () {
    var $this = jQuery(this);
    var keyword = $this.attr('rel');
    var hits = 0;
    for (i=0;i<myLength;i++) {
      var item = data[i];
      var vidSrc = YOUTUBE_PATH + item.id + '?autoplay=1&rel=0&modestbranding=1&autohide=1&theme=light';data
      
      if (!item.tags) {
        item.tags = [];
      }

      var categoriesLength = item.tags.length;
      for (j=0;j<categoriesLength;j++) {
        if(item.tags[j].indexOf(keyword) > -1 || item.tags[j].indexOf(keyword.toLowerCase()) > -1 || (item.title === keyword && j===0)) {
          hits++;
          MYID = 'vid'+keyword+'Vid'+String(i);
          if ($this.hasClass('multi')) {
            var myclass="youtubeVideoMulti";
            if ((hits - 1) % 3 === 0) myclass += ' clear';

            $this.append('<div class="'+myclass+'"><a href="' + vidSrc + '" data-title="' + item.title + '" class="overlay playButtonContainer"><img src="' + item.thumbnails.medium.url + '" class="videoThumb" /><div class="playButton"></div></a><div class="youTubeVideoDescription"><h2>'+item.title+'</h2><p>'+trim(item.description, 190)+'</p></div></div>');

            //$this.append('<div class="'+myclass+'"><div style="display: none;" id="X'+MYID+'"><div id="'+MYID+'"></div></div><div><a href="#TB_inline?height=380&width=425&inlineId=X'+MYID+'" class="thickbox playButtonContainer"><img src="' + item.media$group.media$thumbnail[0].url + '" /><div class="playButton"></div></a></div><div class="youTubeVideoDescription"><h2>'+item.title+'</h2><p>'+item.description+'</p><div class="clear"></div></div>');
            //swfobject.embedSWF(YOUTUBE_PATH + item.id+'&rel=0&autoplay=1', MYID, "425", "380", "9.0.0","Unknown_83_filename"/*tpa=http://www.marathonoil.com/js/expressInstall.swf*/, {}, {wmode:"transparent"}, {});
          } else {
            if ($this.hasClass('homeVideo')) {
              var vidSrc = YOUTUBE_PATH + item.id + '?autoplay=1&rel=0&modestbranding=1&theme=light&autohide=1&enablejsapi=1&origin=http://marathonoil.com';
              var thumbSrc = item.thumbnails.medium.url;
              $this.append('<a href="' + vidSrc + '" data-title="' + item.title + '" target="_blank" class="inlineVideo playButtonContainer playButtonContainerXLarge"><img class="videoThumb" src="' + thumbSrc + '"><div class="playButton playButtonXLarge"></div></a>');
            } else if ($this.hasClass("careersVideo")) {

              var id = item.id;
              var $container = jQuery('<div class="youtubeVideoCareers"><a href="' + vidSrc + '" class="inlineVideo playButtonContainer playButtonContainerXLarge"><img src="' + item.thumbnails.medium.url + '" class="videoThumb" /><div class="playButton playButtonXLarge"></div></a></div>');
              // var $container = jQuery('<div class="youtubeVideoCareers" id="'+MYID+'"></div>');
              var $playBtn = jQuery('<div class="ytPlayBtn"></div>');
              var $thumb = jQuery('<div class="marqueeDot"><img width="45" height="25" src="Unknown_83_filename"/*tpa=http://img.youtube.com/vi/%27+id+%27/default.jpg*/ /></div>');

              $this.append($container);

              //swfobject.embedSWF(YOUTUBE_PATH + item.id+'&rel=0&showinfo=1', MYID, "272", "200", "9.0.0","Unknown_83_filename"/*tpa=http://www.marathonoil.com/js/expressInstall.swf*/, {}, {wmode:"window"}, {});

              jQuery(".marqueeIndicator").append($thumb);
              // Since we don't know order, have to keep adding active class until the last one
              jQuery(".marqueeDot").removeClass("active");
              $thumb.addClass("active");

              $thumb.click(function() {
                if (window.player && window.player.getPlayerState() === 1) {
                  window.player.stopVideo();
                }
                jQuery(".marqueeDot").removeClass("active");
                $thumb.addClass("active");
                jQuery(".marqueeImage").hide();
                $this.parents(".marqueeImage").show();
              });
            } else if ($this.hasClass("two-column")) {
              var id = item.id;
              var $container = jQuery('<div class="youtubeVideoColumn"><a href="' + vidSrc + '" class="inlineVideo playButtonContainer"><img src="' + item.thumbnails.medium.url + '" class="videoThumb" /><div class="playButton"></div></a></div>');
              var $playBtn = jQuery('<div class="ytPlayBtn"></div>');
              var $thumb = jQuery('<div class="marqueeDot"><img width="45" height="25" src="Unknown_83_filename"/*tpa=http://img.youtube.com/vi/%27+id+%27/default.jpg*/ /></div>');

              $this.append($container);
              var uploaded = String(item.publishedAt).parseDate();
              var fancyDate = MONTHS[uploaded.getMonth()] + ' ' + uploaded.getDate() + ", " + uploaded.getFullYear();
              $container.append('<div><h2>'+item.title+'</h2><p class="date">' + fancyDate + '</p></div>');

            } else {
              // $this.append('<div class="youtubeVideo" id="'+MYID+'"></div><div class="youTubeVideoDescription"><h2>'+item.title+'</h2><p>'+item.description+'</p><div class="clear"></div>');
              // i=myLength;
              // swfobject.embedSWF(YOUTUBE_PATH + item.id+'&rel=0', MYID, "360", "270", "9.0.0","Unknown_83_filename"/*tpa=http://www.marathonoil.com/js/expressInstall.swf*/, {}, {wmode:"transparent", autoplay: true}, {});
              var vidSrc = YOUTUBE_PATH + item.id + '?autoplay=1&rel=0&modestbranding=1&autohide=1&theme=light&enablejsapi=1&origin=http://marathonoil.com';
              var thumbSrc = item.thumbnails.medium.url;
              $this.append('<a href="' + vidSrc + '" data-title="' + item.title + '" target="_blank" class="inlineVideo playButtonContainer playButtonContainer2XLarge"><img class="videoThumb" src="' + thumbSrc + '"><div class="playButton playButton2XLarge"></div></a>');
              $this.append('<div class="youTubeVideoDescription"><h2>'+item.title+'</h2><p>'+trim(item.description, 190)+'</p></div>');
            }
          }

        }
      }
    }
  });
  /*var $vidPage = jQuery("#videoPage");
  if ($vidPage.length > 0) {
    for(i=0;i<myLength;i++) {
      var item = data[i];
      var myKeywords = item.media$group.media$keywords.$t;

    }
  }*/
}

var ytHome = function(data) {
  jQuery(function(){
    if (jQuery("#tabContentVideos").length > 0) {
      var $container = jQuery('<ul id="homeNews" />').prependTo("#tabContentVideos");
      data.sort(function(a,b){
        var ad = String(a.publishedAt).parseDate();
        var bd = String(b.publishedAt).parseDate();
        return (ad>bd) ? -1 : ((ad<bd) ? 1 : 0);
      });

      var homeVideos = data.slice(0,5);

      jQuery.each(homeVideos,function(idx,item) {
        var uploaded = String(item.publishedAt).parseDate();
        var fancyDate = MONTHS[uploaded.getMonth()] + ' ' + uploaded.getDate() + ", " + uploaded.getFullYear();
        var vidSrc = YOUTUBE_PATH + item.id + '?autoplay=1&rel=0&modestbranding=1&autohide=1&theme=light&enablejsapi=1&origin=http://marathonoil.com';
        var thumbSrc = item.thumbnails.medium.url;
        var $li = jQuery("<li>").appendTo($container);
        $li.append('<a href="' + vidSrc + '" data-title="' + item.title + '" target="_blank" class="overlay playButtonContainer playButtonContainerSmall"><img class="videoThumb" src="' + thumbSrc + '"><div class="playButton playButtonSmall"></div></a>');
        // $li.append('<a href="' + vidSrc + '" target="_blank" class="overlay playButtonContainer"><div class="vidThumbBack" style="background-image: url(' + thumbSrc + ');"></div><div class="playButton playButtonSmall"></div></a>');
        $li.append('<div class="newsDate">' + fancyDate + '</div>');
        $li.append('<a href="' + vidSrc +'" data-title="' + item.title + '" class="overlay">'+item.title+'</a>');
        $li.append('<div class="clear"></div>');
      });
    }

    jQuery(".careers-spotlight").each(function(){
      var $container = jQuery(this);
      var matched = 0;
      var $indicator = jQuery('<div/>').appendTo($container).addClass('slide-indicator');
      var $slider = jQuery('<div/>').appendTo($container).addClass('spotlight-slider');

      jQuery.each(data,function(idx,item) {
        if (item.tags.indexOf('home-careers-spotlight') > -1) {
        //if (/home-careers-spotlight/ig.test(item.media$group.media$keywords.$t)) {
          matched++;
          $indicator.append('<img src="../images/marqueeDot.png"/*tpa=http://www.marathonoil.com/images/marqueeDot.png*/ data-idx="' + matched + '" />');
          var $item = jQuery('<div class="spotlight-item"  data-idx="' + matched + '"/>').appendTo($slider);
          var vidSrc = YOUTUBE_PATH + item.id + '?autoplay=1&rel=0';
          var thumbSrc = item.thumbnails.medium.url;
          var vidTitle = item.title.replace(/(a day in the life \-(\-)? )|(Job Profile of a(n)?)/ig,'');

          $item.append('<a href="' + vidSrc + '" data-title="' + item.title + '" target="_blank" class="overlay playButtonContainer playButtonContainerLarge"><img class="videoThumb" src="' + thumbSrc + '"><div class="playButton playButtonLarge"></div></a>');
          var $vidText = jQuery('<div class="video-text"></div>').appendTo($item);
          $vidText.append('<h4>' + vidTitle + '</h4>');
          $vidText.append('<p>' + trim(item.description, 190) + '</p>');
        }
      });

      $indicator.find("img:first").attr('src','../images/marqueeDotActive.png'/*tpa=http://www.marathonoil.com/images/marqueeDotActive.png*/);

      var $first = $slider.find(".spotlight-item:first").addClass('active');
      $container.find(".left-arrow").addClass('arrow-disabled');

      var itemWidth = $first.outerWidth();
      var fullWidth = matched * itemWidth;

      $slider.css('width',fullWidth);

      var active = 1;

      var changeSpotlight = function(event) {
        var $clicked = jQuery(event.target);
        var direction,next;
        if ($clicked.hasClass("left-arrow") || $clicked.hasClass("right-arrow")) {
          direction = $clicked.attr('data-direction');
          next = parseInt(active) + parseInt(direction);
        } else {
          direction = 1;
          next = parseInt($clicked.attr("data-idx"),10);
        }

        if (active !== next) {
          active = next;

          jQuery(".right-arrow, .left-arrow").removeClass("arrow-disabled");
          if (active === 1) {
            jQuery(".left-arrow").addClass("arrow-disabled");
          }

          if (active === matched) {
            jQuery(".right-arrow").addClass("arrow-disabled");
          }


          next--;
          $indicator.find("img").attr('src','../images/marqueeDot.png'/*tpa=http://www.marathonoil.com/images/marqueeDot.png*/);
          $indicator.find("img").eq(next).attr('src','../images/marqueeDotActive.png'/*tpa=http://www.marathonoil.com/images/marqueeDotActive.png*/);

          if (Modernizr.csstransitions) {
            $slider.css({
              'left' : (next * itemWidth) * -1
            });
          } else {
            $slider.stop().animate({
              'left' : (next * itemWidth) * -1
            },{
              'duration' : 300,
              'easing' : 'easeInOutQuart'
            });
          }
        }
      };

      $container.find(".right-arrow:not(.arrow-disabled)").live('click',changeSpotlight);
      $container.find(".left-arrow:not(.arrow-disabled)").live('click',changeSpotlight);
      $container.find(".slide-indicator img").live('click',changeSpotlight);
    });
  });
};

var homeImageClasses = ['homeImage1','homeImage2','homeImage3','homeImage4','homeImage5'], homeImage = 0;

var rotateHomeImage = function() {
  homeImage+1 == homeImageClasses.length ? homeImage = 0 : homeImage++;
  $this = jQuery('.rotatingHomeImage');
  $this.wrap('<div class="rotatingHomeImage '+homeImageClasses[homeImage]+'"></div>');
  $this.fadeOut(1000, function() {jQuery(this).remove();});
  setTimeout("rotateHomeImage()",5000);
}

var homeMarquee = function () {
  sizmarquee.init(jQuery, gParams);
  // Listens for clicks on Marquee "dots"
  jQuery('.marqueeIndicator .marqueeDot').click(function() {
      sizmarquee.jumpTo(jQuery('.marqueeDot').index(this));
  });
}


jQuery(function () {
  jQuery.noConflict();
  setTimeout("jQuery('#inline').css('min-height', jQuery('#leftNav').height()+40)", 100);
  try {
    jQuery("#stockTicker").load('/content/includes/AJAXtwister.asp?type=2011homequote');
  }
  catch(ex) {
    var script   = document.createElement("script");
    script.type  = "text/javascript";
    script.src   = "../content/includes/AJAXtwister.asp-type=2011homequoteinjection"/*tpa=https://www.marathonoil.com/content/includes/AJAXtwister.asp?type=2011homequoteinjection*/;
    document.body.appendChild(script);
  }

  if (jQuery.cookie('fontSize') === null) {
    resizeText(1);
  } else {
    redrawFonts(jQuery.cookie('fontSize'));
  }

  jQuery("a.inlineVideo[href]").live("click",function(event){
    event.preventDefault();
    var $this = jQuery(this);

    var href = $this.attr("href");
    var videoID = (/v\/((.)+)\?/ig.exec(href))[1];
    var videoTitle = $this.attr("data-title");
    var autohide = href.indexOf('autohide=') >= 0 ? (/autohide=(.)/ig.exec(href))[1] : 1;
    var theme = (/theme=([a-z]+)/ig.exec(href))[1];
    
    $this.html('<div id="ytInlinePlayer'+videoID+'"></div>');

    window.player = new YT.Player('ytInlinePlayer'+videoID, {
      height: '100%',
      width: '100%',
      videoId: videoID,
      playerVars: {
        'autoplay': 1,
        'rel':0,
        'modestbranding':0,
        'autohide':autohide,
        'theme':theme,
        'showinfo':0
      },
      events: {
        'onStateChange':  function(event){
          var states = ['Watch to End','Play','Pause','Buffering','','Cued'];
         _gaq.push(['_trackEvent', 'Video' , states[event.data] , videoTitle + '[' + videoID + ']']);
        }
      }
    });
  });
  
  window.onYouTubeIframeAPIReady = function() {
    jQuery('iframe[src*="youtube.com/embed"]').each(function() {
      var $iframe = jQuery(this);
      
      var a           = document.createElement( 'a' );
          a.href      = this.src;
          a.hostname  = 'http://www.marathonoil.com/js/www.youtube.com';
          a.protocol  = document.location.protocol;
      var tmpPathname = a.pathname.charAt( 0 ) === '/' ? a.pathname : '/' + a.pathname;  // IE10 shim
      
      // For security reasons, YouTube wants an origin parameter set that matches our hostname
      var origin = window.location.protocol + '%2F%2F' + window.location.hostname + ':' + window.location.port;
  
      if( a.search.indexOf( 'enablejsapi' ) === -1 ) {
  
        a.search = a.search.length > 0 ? a.search + '&enablejsapi=1' : a.search = 'enablejsapi=1&origin=' + origin;
  
      }
  
      if( a.search.indexOf( 'origin' ) === -1 ) {
  
        a.search = a.search + '&origin=' + origin;
  
      }
      
      a.pathname       = tmpPathname;
      this.src = a.href + a.hash;
      
      new YT.Player(this, {
        events: {
          'onStateChange':  function(event){
            var targetVideoUrl  = event.target.getVideoUrl();
            var targetVideoId   = targetVideoUrl.match( /[?&]v=([^&#]*)/ )[ 1 ];  // Extract the ID
            
            var states = ['Watch to End','Play','Pause','Buffering','','Cued'];
            if (states[event.data]) {
              _gaq.push(['_trackEvent', 'Video' , states[event.data] , targetVideoId]);
            }
          }
        }
      });
    });
  }
  
  
  //Prevents style from being applied to the privacy statement if on the privacy page, otherwise wrapped in lightbox
  jQuery("body.p2920 #privacy-overlay").each(function() {
    jQuery(this).removeAttr("id");
  })
  
  jQuery("a[rel='#privacy-overlay']").overlay({
    mask: '#fff'
  });
  
  
  jQuery(document).ready(function(){
  /*
    jQuery(".thickboxcust").live("click", function(e){
      e.preventDefault();
      var href = jQuery(this).attr("href");

      jQuery(this).overlay({
        top: "15%",
        target: "#moc-overlay",
        mask: {
          color: '#fff',
          loadSpeed: 200,
          opacity: 0.8
        },
        load: true,
        onBeforeLoad: function() {
          var wrap = this.getOverlay().find(".content-wrap");
          wrap.load(href);
          // Kind of a hack to ensure SAM knows you haven't actually
          // navigated to the page the content was pulled from
          if (jQuery("body").hasClass("samadmin")) {
              $.ajax('/Default.asp?pageid=' + myPageID);
          }
        },
        onClose: function() {
          this.getOverlay().find(".content-wrap").empty();
        }
      });
  //    tb_show('',url+'?KeepThis=true&amp;TB_iframe=true&amp;height=580&amp;width=570');
    });

    jQuery(".thickboxAR").bind("click", function(e){
      e.preventDefault();
      var url = jQuery(this).attr("href");
      tb_show('',url+'?KeepThis=true&amp;TB_iframe=true&amp;height=470&amp;width=670');
    });
  */
    
  
  
  });

  jQuery("a.overlay[href$='.jpg'],a.overlay[href$='.gif'],a.overlay[href$='.png']").each(function(){
    var url = jQuery(this).attr('href');
    var image = new Image();
    image.src = url;
  });

  jQuery("a.overlay[href],.thickboxcust,.thickboxAR").live("click",function(event){
    event.preventDefault();
    jQuery(this).overlay({
      top: "15%",
      target: "#moc-overlay",
      mask: {
        color: '#fff',
        loadSpeed: 200,
        opacity: 0.8
      },
      load: true,
      onBeforeLoad: function() {
        var myOverlay = this.getOverlay();
        var $trigger = this.getTrigger();
        var href = $trigger.attr("href");
        if (href.substr(11,7) === 'youtube') {
          var videoTitle = $trigger.attr("data-title");
          var videoID = (/v\/((.)+)\?/ig.exec(href))[1]
          var autohide = href.indexOf('autohide=') >= 0 ? (/autohide=(.)/ig.exec(href))[1] : 1
          var theme = href.indexOf('theme=') >= 0 ? (/theme=([a-z]+)/ig.exec(href))[1] : 'light';

          this.getOverlay().find(".content-wrap").html('<div id="ytOverlayPlayer"></div>');
          // <iframe id="ytPlayer" src="' + $trigger.attr('href') + '" frameborder="0" width="610" height="373"></iframe>');
          // this.getOverlay().find(".content-wrap").html('<iframe id="ytPlayer" src="' + $trigger.attr('href') + '" frameborder="0" width="610" height="373"></iframe>');
          window.player = new YT.Player('ytOverlayPlayer', {
            height: '343',
            width: '610',
            videoId: videoID,
            playerVars: {
              'autoplay': 1,
              'rel':0,
              'modestbranding':0,
              'autohide':autohide,
              'theme':theme,
              'showinfo':0
            },
            events: {
              'onStateChange':  function(event){
                var states = ['Watch to End','Play','Pause','Buffering','Cued'];
                _gaq.push(['_trackEvent', 'Video' , states[event.data] , videoTitle + '[' + videoID + ']']);
              }
            }
          });

        } else if ($trigger.attr("href").match(/\.jpg|.gif|.png/)) {
          var $contentWrap = myOverlay.find(".content-wrap");
          $contentWrap.css("text-align","center");
          var image = new Image();
          image.src = $trigger.attr("href");
          image.onload = function() {
            // image should already be preloaded but doing this for edge case where caching disabled
            var ow = myOverlay.width();
            var ww = jQuery(window).width();
            var mleft = (ww - ow) / 2;
            myOverlay.css("left",mleft);
          };

          $contentWrap.append(image);
          if ($trigger.attr("title")) {
            $contentWrap.append("<p>" + $trigger.attr("title") + "</p>");
          }
        } else if ($trigger.attr("href").match(/\.html|.htm/)) {
          var wrap = this.getOverlay().find(".content-wrap");
          wrap.load(this.getTrigger().attr("href"),function(){
            var ow = myOverlay.width();
            var ww = jQuery(window).width();
            var mleft = (ww - ow) / 2;
            myOverlay.css("left",mleft);
          });
        } else {
          var wrap = this.getOverlay().find(".content-wrap");
          wrap.load(this.getTrigger().attr("href") + " main");
          // Kind of a hack to ensure SAM knows you haven't actually
          // navigated to the page the content was pulled from
          if (jQuery("body").hasClass("samadmin")) {
              $.ajax('/Default.asp?pageid=' + myPageID);
          }
        }
      },
      onClose: function() {
        this.getOverlay().find(".content-wrap").empty();
      }
    });
    
    if (jQuery("body").hasClass("p3040")) {
      jQuery("#braeForm").validate();
    }
  });

    initializePage = function (id, primaryid) {
        /* World Map Code */
        if (jQuery("#countryList").length > 0) {
            /* Drop Down */
            jQuery("#countryList").bind('change', function () {
                if (jQuery(this).find("option:selected").val() != '') {
                    window.location.href = jQuery(this).find("option:selected").val();
                }
            });
        }
        //if (id == 1908 || id == 2968) {
            /* Map Hotspots */
            jQuery(".wmHotSpot").each(function () {
                var $this = jQuery(this);
                $this.find('img').css('height', $this.innerHeight()).css('width', $this.innerWidth());
            }).click(function () {
                jQuery('.wmHotSpot').show();
                var $this = jQuery(this).hide();
                jQuery('.popup').hide();
                jQuery('#pu' + $this.attr('id')).fadeIn(250);
            });

            /* Popup Close Buttons */
            jQuery(".popupCloser").click(function () {
                var $this = jQuery(this);
                jQuery("#" + $this.find('img').attr('rel')).fadeIn(125);
                $this.parents('.popup').hide();
            });
        //}

        /* Image Gallery specific code */
        if (id == 2043) {
          getSets();
        }

        /* Home Page Specific Code */
        if (id == 1907 || id == 2939) {
          jQuery.easing.easeInOutQuart = function (x, t, b, c, d) {
            if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
            return -c/2 * ((t-=2)*t*t*t - 2) + b;
          };

          jQuery("#tabContentPR").load('/content/includes/AJAXtwister.asp?type=2011NewsReleasesHome', function () {
            // fitList("#tabContentPR", 380);
          });

          jQuery("#newsTicker").load('/content/includes/AJAXtwister.asp?type=2011NewsTickerHome', function () {
            // fitList("#tabContentPR", 380);
            var $ticker = jQuery(this);
            var displayDuration = 8000;
            var transitionDuration = 1000;

            var changeTicker = function(action) {
              var $current = $ticker.find("li.active:first");

              if (action === "prev") {
                var $prev = $current.is(":first-child") ? $ticker.find("li:last") : $current.prev();
                $prev.addClass("notrans past");
                if (!Modernizr.csstransitions) {
                  $current.stop().animate({"left":"850"},transitionDuration,"easeInOutQuart",function(){
                    $current.removeClass("active").css("left","");
                  });

                  $prev.stop().animate({"left":"0"},transitionDuration,"easeInOutQuart",function(){
                    $prev.removeClass("past").addClass("active").css("left","");
                  });
                } else {
                  $current.removeClass("active");
                  setTimeout(function(){
                    $prev.removeClass("notrans past");
                    $prev.addClass("active");
                  },50);
                }
              } else {
                var $next = $current.is(":last-child") ? $ticker.find("li:first") : $current.next();

                if (!Modernizr.csstransitions) {
                  $current.stop().animate({"left":"-850"},transitionDuration,"easeInOutQuart",function(){
                    $current.removeClass("active").addClass("past").css("left","");
                  });

                  $next.removeClass("past").stop().animate({"left":"0"},transitionDuration,"easeInOutQuart",function(){
                    $next.removeClass("past").addClass("active").css("left","");
                  });
                } else {
                  $next.removeClass("notrans").addClass("active");
                  $current.removeClass("active").addClass("past");
                  setTimeout(function(){
                    $current.addClass("notrans").removeClass("past");
                  },transitionDuration);
                }
              }
            };

            var tickerInterval = setInterval(changeTicker,displayDuration);

            jQuery(".tickerButton").click(function(event){
              var action = jQuery(this).attr("data-action");
              clearInterval(tickerInterval);
              changeTicker(action);
              tickerInterval = setInterval(changeTicker,displayDuration);
            });
          });

          jQuery("#tabContentEvents").load('/content/includes/AJAXtwister.asp?type=2011EventsHome');

          jQuery("#tabEvents").live("click", function() {
            // setTimeout("fitList('#tabContentEvents', 380)", 10);
          });

          /* Set timer */
          setTimeout("rotateHomeImage()", 5000);
          homeMarquee();
        }

        /* News Page Specific Code */
        if (id == 1910) {
          jQuery("#tabContent3828").load('/content/includes/AJAXtwister.asp?type=2011NewsReleasesHome', function () {
          // fitList("#tabContent3828", 345);
          });
        }

        /* Press Releases Specific Code */
        if (id == 2039) {
          jQuery(function () { jQuery('#pressReleases').load('http://www.marathonoil.com/content/includes/AJAXtwister.asp?type=2011NewsReleases'); });
        }

        /* Press release specific code */
        if (id == 2563) {
          jQuery("#newPressRelease img").each( function() {
            if (jQuery(this).width() > 593) {
              jQuery("#sidebar").hide();
              jQuery("#inline").width(800);
            }
          });
        };

        /* Investor Center Home Code */
        if (primaryid == 1909) {
          try {
            jQuery("#newsEventsSidebar").load('http://www.marathonoil.com/content/includes/AJAXtwister.asp?type=2011IRSidebarNews', function () {
              // fitList("#newsEventsSidebar", 300 );
              jQuery("#eventsContent").load('http://www.marathonoil.com/content/includes/AJAXtwister.asp?type=2011IRSidebarEvents', function() {
                if (id == 1909) {
                  jQuery("#eventsTab").swapWith("#pressReleasesTab").hide();
                }
                jQuery("#eventsTab").live( "click", function() {
                  // setTimeout("fitList('#eventsContent', 300 )", 10);
                });
                if (id == 1909) {
                  jQuery("#eventsTab").trigger("click");
                }
              });
            });
          }
          catch(ex) {
            var script   = document.createElement("script");
            script.type  = "text/javascript";
            script.src   = "../content/includes/AJAXtwister.asp-type=2011IRSidebarNewsinjection"/*tpa=http://www.marathonoil.com/content/includes/AJAXtwister.asp?type=2011IRSidebarNewsinjection*/;
            document.body.appendChild(script);
          }
        }

        /* Spotlight Series Specific Code */
        if (id == 2045) {
          //jQuery("#featureArea").load('/content/includes/AJAXtwister.asp?type=spotlightImageRotator2011');
          jQuery("#featureArea .featureImage:first").show().addClass("active");
          var rotateSpotlight = function() {
            var $current = jQuery("#featureArea .featureImage.active");
            var $last = jQuery("#featureArea .featureImage:last");

            if ($current.attr('id') === $last.attr('id')) {
              var $next = jQuery("#featureArea .featureImage:first");
            } else  {
              var $next = $current.next();
            }

            $current.fadeOut(1000).removeClass("active");
            $next.fadeIn(1000).addClass("active");
          }

          setInterval(rotateSpotlight,5000);
        }

        /* We Work with Care Specific Code */
        if (id == 2118) {
            jQuery("#featureArea").load('/content/includes/AJAXtwister.asp?type=weWorkWithCare2011');
        }

        // CSR Report Homepage
        if (id == 2957 || id == 3094) {
          homeMarquee();
        }

        /* Shade Button Code */
        /* Shade Button Configurator */
        jQuery(".shadebutton").each(function () {
          var $this = jQuery(this), myTarget = $this.parent().attr('href'), myLabel = $this.parent().attr('title') || $this.attr('title'), myDescription = $this.attr('alt'), myWidth = $this.width(), myHeight = $this.height();
          $this.parent().wrap('<div class="shadebuttonwrapper" />').before('<div class="shadebuttonlabel">' + myLabel + '</div>', '<div class="shadeclipper"><div class="shadebuttontab sprite"><div class="shadebuttondesc">' + myDescription + '</div></div></div>', '<a href="' + myTarget + '" class="shadebuttontrigger" title=""><img src="../../www.marathon.com/images/spacer.gif"/*tpa=http://www.marathon.com/images/spacer.gif*/ alt="" /></a>');
          $this.parent().siblings('.shadeclipper, .shadebuttontrigger').css('width', String(myWidth + 'px')).css('height', String(myHeight + 'px')).find('.shadebuttontab').css('width', String(myWidth + 'px'));
          $this.parent().siblings('.shadebuttontrigger').find('img').css('width', String(myWidth + 'px')).css('height', String(myHeight + 'px'));
        });

        /* Shadebutton Watcher */
        jQuery('.shadebuttontrigger').hover(function () {
          var $this = jQuery(this);
          $this.siblings('.shadeclipper').find('.shadebuttontab').animate({ marginTop: 0 }, 125, "swing");
        }, function () {
          var $this = jQuery(this);
          var marginTop = jQuery("body").hasClass("CSR") ? -125 : -99
          $this.siblings('.shadeclipper').find('.shadebuttontab').animate({ marginTop: marginTop }, 500, "swing");
        });

        /* Alternate (Bottom Up) Shade Button Configurator */
        jQuery(".BUshadebutton").each(function () {
          var $this = jQuery(this), myTarget = $this.parent().attr('href'), myLabel = $this.parent().attr('title'), myDescription = $this.attr('alt'), myWidth = $this.width(), myHeight = $this.height();
          $this.parent().wrap('<div class="BUshadebuttonwrapper" />').before('<div class="BUshadebuttonlabel">' + myLabel + '</div>', '<div class="BUshadeclipper"><div class="BUshadebuttoncorner"></div><div class="BUshadebuttontab"><div class="BUshadebuttondesc">' + myDescription + '</div></div></div>', '<a href="' + myTarget + '" class="BUshadebuttontrigger" title=""><img src="../../www.marathon.com/images/spacer.gif"/*tpa=http://www.marathon.com/images/spacer.gif*/ alt="" /></a>');
          $this.parent().siblings('.BUshadeclipper, .BUshadebuttontrigger').css('width', String(myWidth + 'px')).css('height', String(myHeight + 10 + 'px')).find('.BUshadebuttontab').css('width', String(myWidth + 'px'));
          $this.parent().siblings('.BUshadebuttontrigger').find('img').css('width', String(myWidth + 'px')).css('height', String(myHeight + 'px'));
        });

        /* Alternate (Bottom Up) Shadebutton Watcher */
        jQuery('.BUshadebuttontrigger').hover(function () {
          var $this = jQuery(this);
          $this.siblings('.BUshadeclipper').find('.BUshadebuttontab').animate({ marginTop: -1 }, 125, "swing");
        }, function () {
          var $this = jQuery(this);
          $this.siblings('.BUshadeclipper').find('.BUshadebuttontab').animate({ marginTop: 98 }, 500, "swing");
        });

        /* WavyImage Code */
        jQuery(".wavyImage").each(function () {
          var $this = jQuery(this), myDescription = $this.attr('alt'), myWidth = $this.width(), myHeight = $this.height();
          $this.wrap('<div class="wavyImageWrapper" />').before('<div class="wavyImageWaves sprite" />');
          $this.siblings('.wavyImageWaves').css('width', String(myWidth + 'px')).css('height', String(myHeight + 'px'));
        });

        jQuery(".annualReview img").each(function () {
          var myLabel = jQuery(this).attr('title');
          jQuery(this).wrap('<div class="arImg" />').before('<div class="arImgLabel">' + myLabel + '</div>');
        });

        /* Search Box Watcher */
        jQuery("#q").focus(
          function () {
            if (this.value == 'Search this site...') this.value = '';
          }
        ).blur(
          function () {
            if (this.value == '') this.value = 'Search this site...';
          }
        );

        /* Press Release Search Box Watcher */
        jQuery("#prq").live( 'focus',
          function () {
            if (this.value == 'Search press releases...') this.value = '';
          }
        ).live('blur',
          function () {
            if (this.value == '') this.value = 'Search press releases...';
          }
        );

        /* Overlapping Tab Code */
        /* Code needs to be tweaked if we are going to have multiple tab groups on a page */
        jQuery(".oTab.target").live("click", function () {
          var $this = jQuery(this);
          var $that = jQuery("#" + $this.attr('rel'));
          $this.closest(".oTabGroup").find(".oTabContent").removeClass("active");
          $this.closest(".oTabGroup").find(".oTab").removeClass("active").removeClass("nextActive");
          $this.addClass("active").prev().addClass("nextActive").next().next().addClass("active");
          $that.addClass("active");
        });

        jQuery(".tabbedArea .tabs li").live("click", function () {
          var $this = jQuery(this);
          var $that = jQuery("#" + $this.attr('data-tab'));
          $this.closest(".tabbedArea").find(".tabContent").removeClass("active");
          $this.closest(".tabbedArea").find(".tabs li").removeClass("active");
          $this.addClass("active");
          $that.addClass("active");
        });

        /* Inline Accordion Code */
        if (jQuery("#inlineAccordion").length > 0) {
            jQuery("#inlineAccordion > div.inlineAccordionContent").each(function (i) {
              // Find the ideal height to display the tallest single accordion
              if (jQuery(this).height() > myHeight) myHeight = jQuery(this).height();
              // Calculate the ideal height to display all accordions
              totalHeight += jQuery(this).height() + 44;
            });

            myHeight += 208;

            jQuery("#inlineAccordion > div.inlineAccordionTab").each(function (i) {
              // Add in the height of each tab
              myHeight += jQuery(this).height();
              totalHeight += jQuery(this).height();
            });

            jQuery(".inlineAccordionTab").bind("click", function (e) {
              $this = jQuery(this);
              if ($this.hasClass('active') == false) {
                setTimeout('showAccordionByID(' + $this.attr('id').substring(12) + ')', 250)
              }
              if (accordionsOpen() > 1) {
                hideAccordionByID($this.attr('id').substring(12));
              } else {
                jQuery(".inlineAccordionContent").slideUp(200);
                jQuery(".inlineAccordionTab").removeClass('active');
              }
              updateCollapser();
            });
            
            function openAccordionByHash() {
              var hash = window.location.hash;
              if (hash && hash != "#") {
                hash = hash.substring(1); // Get rid of the #
                jQuery(".inlineAccordionTab:contains('"+hash+"')").each(function() {
                  var $this = jQuery(this);
                  jQuery('html, body').animate({
                    scrollTop: $this.offset().top - (jQuery('#SAMAdminTools').height() || 0)
                  }, 1000, function() {
                    $this.click();
                  });
                });
              }
            }
            
            jQuery(window).bind( 'hashchange', openAccordionByHash);
            
            jQuery(function() {
              openAccordionByHash();
            });

            jQuery("#accSpacer").height(totalHeight);

            jQuery("#expand").bind("click", function (e) {
              if (jQuery("#inlineAccordion > div.inlineAccordionTab").length / 2 >= accordionsOpen()) {
                jQuery("#inlineAccordion > div.inlineAccordionContent").each(function (i) { jQuery(this).slideDown(200); });
                jQuery("#inlineAccordion > div.inlineAccordionTab").each(function (i) { jQuery(this).addClass('active') });
              } else {
                jQuery("#inlineAccordion > div.inlineAccordionContent").each(function (i) { jQuery(this).slideUp(200); });
                jQuery("#inlineAccordion > div.inlineAccordionTab").each(function (i) { jQuery(this).removeClass('active') });
              }
              updateCollapser();
            });
        }

        jQuery(".resizer").hover(function () { jQuery("#resizeTool").css("background-position", "0px -840px"); }, function () { jQuery("#resizeTool").css("background-position", "0px -860px"); });

        jQuery(".lovTable tr:odd").addClass("blue");

        /* Set up border radius for button, mostly for IE */
      if (typeof jQuery.fn.corner === 'function') {
        jQuery(".button").each(function() {
          var $button = jQuery(this), radius = $button.css("border-radius");
          $button.corner(radius);
        });
      }

    }

    activateRollovers = function () {
      // This nonsense is here because some browsers try and run this bit before elements have their offset top and left defined
      if (
      jQuery("#primaryNav>div>ul>li:eq(0)").offset().left != jQuery("#primaryNav>div>ul>li:eq(1)").offset().left
      ) {
          $activePrimary = jQuery("#primaryNav li.active");
          dipHome = $activePrimary.length > 0 ? $activePrimary.offset().left : 0;
          jQuery("#ulNav1908").pacNav({ "type": "primary", "dd": "#globalDD" });
          jQuery("#ulNav1909").pacNav({ "type": "primary", "dd": "#investorDD" });
          jQuery("#ulNav1910").pacNav({ "type": "primary", "dd": "#newsDD" });
          jQuery("#ulNav1911").pacNav({ "type": "primary", "dd": "#socialDD" });
          jQuery("#ulNav2916").pacNav({ "type": "primary", "dd": "#careersDD" });
          jQuery("#ulNav1913").pacNav({ "type": "primary", "dd": "#aboutDD" });
          jQuery("#ulNav2112").pacNav({ "type": "primary", "dd": "#technologyDD" });
          // CSR Primary Nav
          jQuery.each([2960, 2961, 2962, 2963, 2964, 2965, 2966, 2967, 3095, 3103, 3110, 3117, 3124, 3133, 3138, 3139], function(idx, CSRId) {
            jQuery("#ulNav"+CSRId).pacNav({"type": "primary", "dd": "#CSR-DD"+CSRId});
          });
          jQuery(".dropDownContainer").pacNav({ type: 'dropdown' });
        } else {
          setTimeout("activateRollovers()", 100);
        }
    };

    jQuery(window).resize(function () {
      clearTimeout(timeOuts['resize']);
      timeOuts['resize'] = setTimeout('positionDropdowns();', 250);

    });

    positionDropdowns = function () {
      jQuery("#ulNav1908").each(function () { positionDD(jQuery(this), "#globalDD") });
      jQuery("#ulNav1909").each(function () { positionDD(jQuery(this), "#investorDD") });
      jQuery("#ulNav1910").each(function () { positionDD(jQuery(this), "#newsDD") });
      jQuery("#ulNav1911").each(function () { positionDD(jQuery(this), "#socialDD") });
      //jQuery("#ulNav2916").each(function () { positionDD(jQuery(this), "#careersDD") });
      jQuery("#ulNav1913").each(function () { positionDD(jQuery(this), "#aboutDD") });
      jQuery("#ulNav2112").each(function () { positionDD(jQuery(this), "#technologyDD") });

      jQuery.each([2960, 2961, 2962, 2963, 2964, 2965, /*2966,*/ 2967], function(idx, CSRId) {
        positionDD(jQuery("#ulNav"+CSRId), "#CSR-DD"+CSRId);
      });
    }

    secondaryOff = function () {
      jQuery("#primaryDip").stop().animate({ "width": dipHome }, 200, "swing");
      jQuery(".dropDownContainer").stop().fadeOut("Fast");
      //jQuery("#"+activeSecondary).fadeOut("fast");
    }

    secondaryOn = function (left, dd) {
      jQuery("#primaryDip").stop().animate({ "width": left }, 200, "swing");
      activeSecondary = dd;
      jQuery(dd).fadeIn("fast");
    }

    stopTimeOut = function () {
      for (key in timeOuts) {
        clearTimeout(timeOuts[key]);
      }
    }

    positionDD = function ($this, dd) {
      var ddLeft = $this.offset().left - 40;
      var ddWidth = jQuery(dd).width();
      var maxLeft = jQuery('.pageWidth:eq(0)').width() + jQuery('.pageWidth:eq(0)').offset().left - ddWidth;
      if (ddLeft > maxLeft) ddLeft = maxLeft;
      // jQuery(dd).css({ 'top': jQuery("#primaryIndicator").offset().top + 1, 'left': ddLeft });
      // jQuery(dd).css({ 'top': jQuery("#primaryIndicator").offset().top, 'left': '50%' });
      jQuery(dd).css({'left': '50%' });
    }

    jQuery.fn.pacNav = function (params) {
      this.each(function () {
        var $this = jQuery(this);
        var offset = ($this.outerWidth() - parseInt($this.css("paddingRight")))/2;
        positionDD($this, params.dd);
        var dipLeft = $this.offset().left + 140 + offset;

        if (jQuery($this).hasClass("active")) {
          jQuery("#primaryDip").stop().css({ "width": dipLeft }).show();
          dipHome = dipLeft;
        }

        jQuery(this).bind("mouseover", function () {
          var $this = jQuery(this);
          switch (params.type) {
            /*case 'primary':
            jQuery(this).attr("src",params.on);
            break;*/
            case 'dropdown':
              stopTimeOut();
              break;
            default:
              stopTimeOut();
              left = $this.offset().left + 140 + offset;
              timeOuts["mouseIn"] = setTimeout("secondaryOn(" + left + ", '" + params.dd + "')", 125);
              if (activeSecondary != params.dd) {
                  jQuery(activeSecondary).fadeOut("fast");
              }
          }
        });
        jQuery(this).bind("mouseout", function () {
          switch (params.type) {
            /*case 'primary':
            jQuery(this).attr("src",params.off);
            break;*/
            default:
              stopTimeOut();
              timeOuts["mouseOut"] = setTimeout("secondaryOff()", 300);
          }
        });
      });
    };

    activateRollovers();
});

/*jslint browser: true */ /*global jQuery: true */

/**
 * jQuery Cookie plugin
 *
 * Copyright (c) 2010 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */

// TODO JsDoc

/**
 * Create a cookie with the given key and value and other optional parameters.
 *
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Set the value of a cookie.
 * @example $.cookie('the_cookie', 'the_value', { expires: 7, path: '/', domain: 'http://www.marathonoil.com/js/jquery.com', secure: true });
 * @desc Create a cookie with all available options.
 * @example $.cookie('the_cookie', 'the_value');
 * @desc Create a session cookie.
 * @example $.cookie('the_cookie', null);
 * @desc Delete a cookie by passing null as value. Keep in mind that you have to use the same path and domain
 *       used when the cookie was set.
 *
 * @param String key The key of the cookie.
 * @param String value The value of the cookie.
 * @param Object options An object literal containing key/value pairs to provide optional cookie attributes.
 * @option Number|Date expires Either an integer specifying the expiration date from now on in days or a Date object.
 *                             If a negative value is specified (e.g. a date in the past), the cookie will be deleted.
 *                             If set to null or omitted, the cookie will be a session cookie and will not be retained
 *                             when the the browser exits.
 * @option String path The value of the path atribute of the cookie (default: path of page that created the cookie).
 * @option String domain The value of the domain attribute of the cookie (default: domain of page that created the cookie).
 * @option Boolean secure If true, the secure attribute of the cookie will be set and the cookie transmission will
 *                        require a secure protocol (like HTTPS).
 * @type undefined
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */

/**
 * Get the value of a cookie with the given key.
 *
 * @example $.cookie('the_cookie');
 * @desc Get the value of a cookie.
 *
 * @param String key The key of the cookie.
 * @return The value of the cookie.
 * @type String
 *
 * @name $.cookie
 * @cat Plugins/Cookie
 * @author Klaus Hartl/klaus.hartl@stilbuero.de
 */
jQuery.cookie = function (key, value, options) {
    // key and at least value given, set cookie...
    if (arguments.length > 1 && String(value) !== "[object Object]") {
        options = jQuery.extend({}, options);

        if (value === null || value === undefined) {
            options.expires = -1;
        }

        if (typeof options.expires === 'number') {
            var days = options.expires, t = options.expires = new Date();
            t.setDate(t.getDate() + days);
        }

        value = String(value);

        return (document.cookie = [
            encodeURIComponent(key), '=',
            options.raw ? value : encodeURIComponent(value),
            options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
            options.path ? '; path=' + options.path : '',
            options.domain ? '; domain=' + options.domain : '',
            options.secure ? '; secure' : ''
        ].join(''));
    }

    // key and possibly options given, get cookie...
    options = value || {};
    var result, decode = options.raw ? function (s) { return s; } : decodeURIComponent;
    return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? decode(result[1]) : null;
};


jQuery.fn.swapWith = function(to) {
    return this.each(function() {
        var copy_to = jQuery(to).clone(true);
        var copy_from = jQuery(this).clone(true);
        jQuery(to).replaceWith(copy_from);
        jQuery(this).replaceWith(copy_to);
    });
};


/*
  activateRollovers = function () {
    // This nonsense is here because sometimes browsers try and run this bit before elements have their offset top and left defined
    if (jQuery("#secondaryNav>ul>li:eq(0)").offset().left != jQuery("#secondaryNav>ul>li:eq(1)").offset().left) {
      jQuery("#primaryLab").pacNav({ off: 'Unknown_83_filename'/*tpa=http://www.marathonoil.com/images/lab_instruments.png*/, on: 'Unknown_83_filename'/*tpa=http://www.marathonoil.com/images/lab_instruments_over.png*/, type: 'primary' });
      jQuery("#primaryProcess").pacNav({ off: 'Unknown_83_filename'/*tpa=http://www.marathonoil.com/images/process_analytics.png*/, on: 'Unknown_83_filename'/*tpa=http://www.marathonoil.com/images/process_analytics_over.png*/, type: 'primary' });
      jQuery("#primaryParts").pacNav({ off: 'Unknown_83_filename'/*tpa=http://www.marathonoil.com/images/parts_store.png*/, on: 'Unknown_83_filename'/*tpa=http://www.marathonoil.com/images/parts_store_over.png*/, type: 'primary' });
      jQuery("#primarySupport").pacNav({ off: 'Unknown_83_filename'/*tpa=http://www.marathonoil.com/images/customer_support.png*/, on: 'Unknown_83_filename'/*tpa=http://www.marathonoil.com/images/customer_support_over.png*/, type: 'primary' });
      jQuery("#primaryNews").pacNav({ off: 'Unknown_83_filename'/*tpa=http://www.marathonoil.com/images/news_and_events.png*/, on: 'Unknown_83_filename'/*tpa=http://www.marathonoil.com/images/news_and_events_over.png*/, type: 'primary' });
      jQuery("#primaryAbout").pacNav({ off: 'Unknown_83_filename'/*tpa=http://www.marathonoil.com/images/about_pac.png*/, on: 'Unknown_83_filename'/*tpa=http://www.marathonoil.com/images/about_pac_over.png*/, type: 'primary' });
      jQuery("#secondaryNav>ul>li").pacNav({type: 'secondary' });
      jQuery(".dropDownContainer").pacNav({type: 'dropdown' });
      jQuery("#zoomableImage").each( function () {
        myLeft = jQuery(this).offset().left + (jQuery(this).width());
        myTop = jQuery(this).offset().top + (jQuery(this).height());
        jQuery("#lightboxArrow").offset({ top: myTop, left: myLeft });
      });
    } else {
      setTimeout("activateRollovers()", 100);
    }
  };

  activateRollovers();
  mytabs = jQuery("#tabbedContent > div.tabs").tabs("#tabbedContent > div.panes > div");
  jQuery("#footerSlideshow > div.tabs").tabs("#footerSlideshow > div.panes > div", { rotate: true}).slideshow({autoplay:true, autopause:true });
  jQuery(".dropDownParent").tabs(".dropDownChild > div", {initialIndex: 1000, event:'mouseover', onBeforeClick:function () { stopTimeOut() }});
  jQuery(".timeOutKiller").bind("mouseover", function () {
    stopTimeOut();
  });
*/