/**
 * Design Inspiration Gallery
 **/

// Encapsulate to make jquery happy.
(function ($, Drupal) {

  // Add to the Drupal.behaviors namespace.
  Drupal.behaviors.designInspirationGallery = {
    attach: function(context, settings) {
      // Add our behaviors to our wrapping div, only once
      // Note that we are not using context, because our top level selector is the one that gets replaced on ajax reload
      $('.view-id-crown_inspiration_gallery').once("designInspirationGallery", function() {
        $.each($(this), function(index, elem){
          var gallery = new designInspirationGallery();
          gallery.init(elem, settings, index);
        });
      });
    },
    // Detach is called when elements are removed from the DOM.
    detach: function(context, settings, trigger) {
      if (trigger == 'unload' && context.length) {
        if(context.hasClass('view-id-crown_inspiration_gallery')) {
          // Clean up any event handlers outside the context of our top-level class.
          $(document).off("click.designInspirationGallery");
          $(window).off("resize.designInspirationGallery");
        }
      }
    }
  }
  /**
   * Create an object to hold all our methods.
   */
  var designInspirationGallery = function(){
    // Create an object to hold out public functions.
    var pub = {};

    /**
     * The initi function is used to instantiate these behaviors
     */
    pub.init = function($elem, settings, nth) {
      /*
      $('.masonry', $elem).masonry("reload");
      // On resize, use our poll function so that masonry reloads on window resize, but only every 30 miliseconds
      $(window).on("resize.designInspirationGallery", function(){
        poll(function(){
          $('.masonry', $elem).masonry("reload");
        }, 30);
      });
      */
      // Add the view all button to the exosed filters
      addViewAll($elem);
      // Add our hover event on the view-content of the $elem, delegated to gallerycard divs.
      //$(".gallerycard--front", $elem).on("hover", galleryCardHover);
      // Add our click events
      $(".gallerycard--front", $elem).on("click", galleryCardClick);
      // Add a document-level event for closing the lightbox
      $(document).on("click.designInspirationGallery touchstart.designInspirationGallery", galleryOutsideClickClose);
      // Add a click handler on the close button
      $('.gallerycard--back--button-close').on('click', galleryCloseClickClose); 
    };

    /**
     * Add the view all item.
     * Note we are doing it this way instead of the "select all/none" that bef gives us.
     */
    function addViewAll($elem) {
      var highlight_class = "highlight";
      var $views_exposed_form = $(".view-filters form", $elem);
      var $wrapper = $(".bef-checkboxes", $elem);
      // Add the "View All" item as the first button
      var $view_all_item = $('<div/>', {
        'class': 'form-item form-type-bef-checkbox galleryViewAll',
        'html': '<label class="option" for="crown_view_all">View All</label>'
      }).prependTo($wrapper);
      // Add the click event
      $view_all_item.on("click", function(e) {
        // Uncheck checkboxes and remove highlight classes
        var $inputs = $("input", $wrapper);
        $inputs.prop('checked', false);
        // 
        // Add the highlight class to this button
        $(this).addClass(highlight_class);
        // Adding the class before ajax reload seemed weird, so leaving that out for now.
        //$inputs.closest(".form-item").removeClass(highlight_class);
        // Trigger ajax submission
        $inputs.change();
      });
      // Highlight "View All" if none are selected
      if ($("input:checked", $wrapper).length == 0) {
        $view_all_item.addClass(highlight_class);
      }
    }

    /**
     * On hover, show the title
     */
    function galleryCardHover(e) {
      $(this).find('.gallerycard--front--title').toggleClass('gallerycard--front--title-hover');
    }
    /**
     * When the card is clicked, set classes to invoke the lightbox
     */
    function galleryCardClick(e) {
      $('body').addClass('gallerymode');
      $(this).closest(".gallerycard").addClass('active');
      e.stopPropagation();
    }

    /**
     * When the document is clicked outside the gallerycard, close the lightbox
     */
    function galleryOutsideClickClose(e) {
      var gallery_back = $('.gallerycard--back');
      if($('.gallerycard').hasClass('active') && $('.gallerycard--back').is(":visible") && !gallery_back.is(e.target) && gallery_back.has(e.target).length === 0) {
        close_all_cards();
      }
    }

    /**
     * Handles when a user clicks the close button
     */
    function galleryCloseClickClose(e) {
      close_all_cards();
    }

    /**
     * Does the actual closing of the cards
     */
    function close_all_cards() {
      $('.gallerycard').removeClass('active');
      $('body').removeClass('gallerymode');
    }

    /**
     * Create a poll function to lessen the load of resize events.
     */
    var poll = (function(){
      var timer = 0;
      return function(callback, ms){
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
      };
    })();

    /**
     * Debug wrapper
     */
    function debug(msg, obj){
      if (typeof console !== 'undefined' && 'log' in console) {
        //console.log(msg, obj);
      }
    };

    // Return our public function
    return pub;
  };

})(jQuery, Drupal);