

/* ============== DO NOT ALTER ANYTHING ABOVE THIS LINE ! ============ */
/* AppMeasurement code version: JS.1.4.3
LAST UPDATED: 03-27-2015 */

/************************** CONFIG SECTION **************************/
/* You may add or alter any code config here. */

var currentDomain = "https://www.verizon.com";
var vzRetailHomeHost = removeHttps("../../home/verizonglobalhome/ghp_landing.aspx.htm"/*tpa=http://www.verizon.com/*/);
var vziobiHost = removeHttps("https://www36.verizon.com");
var vzForums = removeHttps("http://forums.verizon.com/");
var vzFiOSVoice = removeHttps("https://www36.verizon.com");
var vzMessageCenter = removeHttps("http://webmail.verizon.com/");
var vzSignIn = removeHttps("https://signin.verizon.com");
var vzMyVZ = removeHttps("http://my.verizon.com/");
var vzEntertainment = removeHttps("http://entertainment.verizon.com/");
var vzFiOSTV = removeHttps("../../home/verizonglobalhome/ghp_landing.aspx.htm"/*tpa=http://www.verizon.com/*/);
var vzSmallBiz = removeHttps("http://smallbusiness.verizon.com/");
var vzRegistrationSEC = removeHttps("https://registration.verizon.com");
var vzShop = removeHttps("http://shop.verizon.com/");
var vzCustomLt = false;

function removeHttps(strUrl){
	var tmpStrUrl = strUrl.replace("https://","");
	tmpStrUrl = tmpStrUrl.replace("http://","");
	return tmpStrUrl;
}
function scTrim(str){return str.replace(/^\s+|\s+$/g,'');}

function scPageView(){
	if(typeof(arguments[0]) != 'undefined' && arguments[0].length > 0){
		var arrArgs=arguments[0].split('^');
		for(i=0;i<arrArgs.length;i++){
			var arrArgTemp=arrArgs[i].split('=');
			if(typeof(arrArgTemp[0]) != 'undefined' && arrArgTemp[0] != '' && typeof(arrArgTemp[1]) != 'undefined' && arrArgTemp[1] != ''){
				eval('s_837.'+scTrim(arrArgTemp[0])+'="'+scTrim(arrArgTemp[1])+'"');
				if(scTrim(arrArgTemp[0])=='detailpageName'){s_837.prop1=s_837.pfxID+"| "+scTrim(arrArgTemp[1]);}
			}
		}
	}
	s_837.prop11='';
	s_837.prop27='';
	s_837.prop37='';
	vzCustomLt = true;
	var s_code=s_837.t();
}
function scLinkTrack(){
	var strID='[linkIDunset]';
	if(typeof(arguments[0]) != 'undefined' && arguments[0].length > 0){
		var arrArgs=arguments[0].split('^');
		for(i=0;i<arrArgs.length;i++){
			var arrArgTemp=arrArgs[i].split('=');
			if(typeof(arrArgTemp[0]) != 'undefined' && arrArgTemp[0] != '' && typeof(arrArgTemp[1]) != 'undefined' && arrArgTemp[1] != ''){
				eval('s_837.'+scTrim(arrArgTemp[0])+'="'+scTrim(arrArgTemp[1])+'"');
				if(scTrim(arrArgTemp[0])=='prop27'){strID=scTrim(arrArgTemp[1]);}//set linkID
			}
		}
	}
	vzCustomLt=true;
	s_837.hbx_lt="manual";
	if(document.cookie.toLowerCase().indexOf('sc_link')>-1){
		document.cookie='SC_LINK=;path=/;domain=.verizon.com;expires=Thu, 01-Jan-1970 00:00:01 GMT;';
	}
	s_837.tl(true,'o',strID);
	vzCustomLt=true;
	s_837.hbx_lt="manual";
	s_837.prop11='';
	s_837.prop27='';
	s_837.prop37='';
}
function scLinkTrackID(){
	if(typeof(arguments[0]) != 'undefined' && arguments[0].length > 0){
		var strID=scTrim(arguments[0]);
		s_837.prop11=s_837.pfxID+'| '+s_837.detailpageName+'| '+strID;
		s_837.prop27=strID;
		s_837.prop37=s_837.pfxID+'| '+s_837.detailpageName;
		vzCustomLt=true;
		s_837.hbx_lt="manual";
		if(document.cookie.toLowerCase().indexOf('sc_link')>-1){
			document.cookie='SC_LINK=;path=/;domain=.verizon.com;expires=Thu, 01-Jan-1970 00:00:01 GMT;';
		}
		s_837.tl(true,'o',strID);
		vzCustomLt=true;
		s_837.hbx_lt="manual";
		s_837.prop11='';
		s_837.prop27='';
		s_837.prop37='';
	}
}

function scfObj(n,d){
	var p,i,x;if(!d)d=document;if((p=n.indexOf('?'))>0&&parent.frames.length){
	d=parent.frames[n.substring(p+1)].document;n=n.substring(0,p);}
	if(!(x=d[n])&&d.all)x=d.all[n];for(i=0;!x&&i<d.forms.length;i++)x=d.forms[i][n];
	for(i=0;!x&&d.layers&&i<d.layers.length;i++)x=scfObj(n,d.layers[i].document);
	if(!x&&d.getElementById)x=d.getElementById(n);return x;
}
function scStrip(a){
	a=a.split('Verizon | ').join('');
	a=a.split('verizon | ').join('');
	a=a.split('|').join('');
	a=a.split('(').join('');
	a=a.split(')').join('');
	a=a.split('&amp;').join('and');
	a=a.split('&reg;').join('');
	a=a.split('&trade;').join('');
	a=a.split('&#36;').join('');
	a=a.split('&#8482;').join('');
	a=a.split("'").join("");
	a=a.split('©').join('');
	a=a.split('\u00A9').join('');
	a=a.split('®').join('');
	a=a.split('\u00AE').join('');
	a=a.split('\u2122').join('');
	a=a.split('™').join('');
	a=a.split('\u2013').join('');
	a=a.split('–').join('');
	a=a.split('\u2014').join('');
	a=a.split('—').join('');
	a=a.split('\u2022').join('');
	a=a.split('•').join('');
	a=a.split('\u00F1').join('');
	a=a.split('ñ').join('');
	a=a.split(' & ').join(' and ');
	a=a.split('&').join('');
	a=a.split('#').join('');
	a=a.split('$').join('');
	a=a.split('%').join('');
	a=a.split('^').join('');
	a=a.split('*').join('');
	a=a.split(':').join('');
	a=a.split('!').join('');
	a=a.split('~').join('');
	a=a.split('<').join('');
	a=a.split('>').join('');
	a=a.split(';').join('');
	return a;
}

/******   custom globals start   ******/
var strSCapp='[noapp]';
var strSCappname='[noname]';
var strSCbizunit='[nobizunit]';
var strSClineofbiz='[nolineofbusiness]';
if(typeof(scType)=='undefined'){scType='[notype]';}
if(scType=='res'){strSCapp='res';strSCappname='residential';strSClineofbiz='residential';strSCbizunit='residential';}
else if(scType=='corp'){strSCapp='crp';strSCappname='corporate';strSClineofbiz='corporate';}
else if(scType=='smb'){strSCapp='smb';strSCappname='business';strSClineofbiz='business';strSCbizunit='small business';}
else if(scType=='myvz20'){strSCapp='myvz';strSCappname='myverizon';strSClineofbiz='myverizon';strSCbizunit='res myverizon';}
else if(scType=='rord'){strSCapp='res';strSCappname='ordering';strSClineofbiz='residential';strSCbizunit='res ordering';}
else if(scType=='myvz20ord'){strSCapp='myvz';strSCappname='ordering';strSClineofbiz='myverizon';strSCbizunit='res ordering';}

var scLHR=window.location.href;
var scLH=window.location.hostname;
var scLP=window.location.pathname;
var scLHRnogoto=scLHR.toLowerCase().substring(0,(scLHR.search(/goto=/)>-1)?scLHR.search(/goto=/):999);
var scIsPrd=false;
var scPS=new Array('http://www.verizon.com/includes/javascript/www.verizon.com','http://www.verizon.com/includes/javascript/www22.verizon.com','http://www.verizon.com/includes/javascript/smallbiz.verizonmarketing.com','http://www.verizon.com/includes/javascript/smallbusiness.verizon.com','http://www.verizon.com/includes/javascript/mediumbusiness.verizon.com','http://www.verizon.com/includes/javascript/www.verizonbusiness.com','http://www.verizon.com/includes/javascript/www.verizonmarketing.com','http://www.verizon.com/includes/javascript/search.verizon.com','http://www.verizon.com/includes/javascript/espanol.verizon.com','http://www.verizon.com/includes/javascript/promo.verizon.com','http://www.verizon.com/includes/javascript/promo.verizon.koreansite.us.com','http://www.verizon.com/includes/javascript/promo.verizon.chinesesite.us.com','http://www.verizon.com/includes/javascript/promopage.verizon.com','http://www.verizon.com/includes/javascript/verizon.koreansite.us.com','http://www.verizon.com/includes/javascript/verizon.chinesesite.us.com','http://www.verizon.com/includes/javascript/vas.verizon.com','http://www.verizon.com/includes/javascript/forums.verizon.com','http://www.verizon.com/includes/javascript/wirelesssupport.verizon.com','http://www.verizon.com/includes/javascript/newscenter.verizon.com','http://www.verizon.com/includes/javascript/investor.verizon.com','http://www.verizon.com/includes/javascript/responsibility.verizon.com','http://www.verizon.com/includes/javascript/foundation.verizon.com','http://www.verizon.com/includes/javascript/community.verizon.com','http://www.verizon.com/includes/javascript/communities.verizon.com','http://www.verizon.com/includes/javascript/referafriend.verizon.com','http://www.verizon.com/includes/javascript/multimedia.verizon.com','http://www.verizon.com/includes/javascript/newsfeed.verizon.com','http://www.verizon.com/includes/javascript/www36.verizon.com','http://www.verizon.com/includes/javascript/gogreen.verizon.com','http://www.verizon.com/includes/javascript/myoffer.verizon.com','http://www.verizon.com/includes/javascript/offer.verizon.com','http://www.verizon.com/includes/javascript/signin.verizon.com','http://www.verizon.com/includes/javascript/my.verizon.com','http://www.verizon.com/includes/javascript/searchservices.verizon.com','http://www.verizon.com/includes/javascript/media.verizon.com','http://www.verizon.com/includes/javascript/webmail.verizon.com','http://www.verizon.com/includes/javascript/newsroom.verizon.com','http://www.verizon.com/includes/javascript/surround.verizon.com','http://www.verizon.com/includes/javascript/games.verizon.com','http://www.verizon.com/includes/javascript/essentialsandextras.verizon.com','http://www.verizon.com/includes/javascript/auth.verizon.com','http://www.verizon.com/includes/javascript/registration.verizon.com','http://www.verizon.com/includes/javascript/m.verizon.com','http://www.verizon.com/includes/javascript/learningcenter.verizon.com','http://www.verizon.com/includes/javascript/perks.verizon.com','http://www.verizon.com/includes/javascript/lookup.verizon.com','http://www.verizon.com/includes/javascript/touchdownchallenge.verizon.com','http://www.verizon.com/includes/javascript/youdeservebetter.verizon.com','http://www.verizon.com/includes/javascript/business.verizon.com','http://www.verizon.com/includes/javascript/mail.verizon.com','http://www.verizon.com/includes/javascript/shop.verizon.com','http://www.verizon.com/includes/javascript/disneychannelpromo.verizon.com','http://www.verizon.com/includes/javascript/entertainment.verizon.com','http://www.verizon.com/includes/javascript/paperfree.verizon.com','http://www.verizon.com/includes/javascript/searchresults.verizon.com','http://www.verizon.com/includes/javascript/musthavefios.verizon.com');
for(i=0;i<scPS.length;i++){
	var sDom=scPS[i];
	sDom=sDom.toLowerCase();
	if((scLH.toLowerCase()).indexOf(sDom)>-1){
		scIsPrd=true;
		break;
	}
}
var s_accountglob='verizontelecomglobal';
var s_accountapp='';
if(strSCapp!='[noapp]'){s_accountapp='verizontelecom'+strSCapp+'';}

/* array=[0] URL patterns, [1] saccountID, [2] .prop6 (line of business), [3] pfxID (assigned prefix), [4] .prop2 (business unit), [5] .prop48 (name of application) */
var scURL=new Array(
	vzSmallBiz+'/default.aspx|verizontelecomsmb|business|ghp|small business|global homepage'
	,'/home/verizonglobalhome/ghp_business.aspx|verizontelecomsmb|business|ghp|business|global homepage'
	,'/home/verizonglobalhome/ghp_business|verizontelecomsmb|business|smb|small business|small business'
	,'/foryourhome/myaccount/unprotected/usermanagement/login/login.aspx|verizontelecommyvz|myverizon|ghp|res myverizon|global homepage'
	,'/foryourhome/myaccount|verizontelecommyvz|myverizon|mvz|res myverizon|misc'
	,vzRetailHomeHost+'/foryoursmallbiz/goflow|verizontelecomsmb|business|smb|small business|ordering'
	,'mail.verizon.com,'+vzMessageCenter+'|verizontelecommsgctr|myverizon|msg|res myverizon|msgcenter'
	,vzEntertainment+'|verizontelecoment|myverizon|ent|res myverizon|entertainment'
	,'foryourhome/myverizon|verizontelecommyvz|myverizon|urc|res myverizon|urc'
	,'business.verizon.net|verizontelecommas|business|smp|small business|smbportal'
	,'_pagelabel=smbportal_page_newsandresources|verizontelecommas|business|nws|small business|smbportal'
	,'_pagelabel=smbportal_page_main_profnetworking|verizontelecommas|business|pnt|small business|smbportal'
	,'_pagelabel=smbportal_page_main_marketplace|verizontelecommas|business|mkt|small business|smbportal'
	,'_pagelabel=smbportal_page_main_support|verizontelecommas|business|spp|small business|smbportal'
	,'_pagelabel=gb_mycommunity|verizontelecomcfr|myverizon|cfr|smb myverizon|community'
	,vzRetailHomeHost+'/ondemand/,www.verizon.com/ondemand/|verizontelecomflx|myverizon|flx|res myverizon|flexview'
	,vzRetailHomeHost+'/support/residential,'+vzRetailHomeHost+'/residentialhelp/,www.verizon.com/support/residential,www.verizon.com/residentialhelp/|verizontelecomesp|myverizon|esp|residential|esupport'
	,vzRetailHomeHost+'/customerhelp/,'+vzRetailHomeHost+'/smallbusiness/help/|verizontelecomesp|myverizon|esp|small business|esupport'
	,vzSignIn+'/ssogb/|verizontelecomsmb|business|sso|small business|reg_sso'
	,vzSignIn+','+vzRegistrationSEC+'|verizontelecomres|myverizon|sso|residential|reg_sso'
	,vzRetailHomeHost+'/fiostv/myaccount/,'+vzRetailHomeHost+'/fiostv/myaccount/members,'+vzRetailHomeHost+'/fiostv/web/,'+vzRetailHomeHost+'/fiostv/web/members,'+vzFiOSTV+'/fiostv/myaccount/,'+vzFiOSTV+'/fiostv/myaccount/members,'+vzFiOSTV+'/fiostv/web/,'+vzFiOSTV+'/fiostv/web/members,www.verizon.com/fiostv/myaccount/,www.verizon.com/fiostv/myaccount/members,www.verizon.com/fiostv/web/,www.verizon.com/fiostv/web/members|verizontelecomftc|myverizon|ftc|res myverizon|fiostvcentral'
	,'_pagelabel=pws,_pagelabel=vzcp_mat_billinghistory,_pagelabel=vzcp_wifi_manage,'+vzMyVZ+'/central/portlets/broadbandwifi/hotspotsearch.jsp,'+vzMyVZ+'/central/portlets/wifi/submitwifi.jsp?mode=download|verizontelecomvzc|myverizon|vzc|res myverizon|verizoncentral'
	,vzShop+'|verizontelecomshp|myverizon|shp|res myverizon|shop'
	,vzMyVZ+'/whatsnext|verizontelecomwhn|myverizon|wnr|residential|whatsnext'
	,'business.verizon.com/whatsnext|verizontelecomwhn|myverizon|wns|small business|whatsnext'
	,vzFiOSTV+'/products/mediamanager,www.verizon.com/products/mediamanager|verizontelecommdm|myverizon|mdm|res myverizon|mediamanager'
	,vzFiOSVoice+'/fiosvoice,'+vzFiOSVoice+'/products/fiosvoice|verizontelecomfdv|myverizon|fdv|res myverizon|fiosdigvoice'
	,vziobiHost+'/callassistant|verizontelecomvca|myverizon|vca|res myverizon|vzcallassist'
	,vzForums+',/content/sso/community.aspx|verizontelecomcfr|myverizon|cfr|res myverizon|community'
	,vzRetailHomeHost+'/home,www.verizon.com/home|verizontelecomres|residential|res|residential|residential'
	,'business.verizon.com/mybusinessaccount|verizontelecommas|business|vzb|small business|mybusiness: myaccount'
	,'type=myvz20|verizontelecommyvz|myverizon|msv|res myverizon|varies'
	,'type=rord,'+vzRetailHomeHost+'/foryourhome/goflow,www.verizon.com/foryourhome/goflow|verizontelecomres|residential|res|res ordering|ordering'
);

for(i=0;i<scURL.length;i++){
	var strboolFound=false;
	var tmpArr=scURL[i].split('|');
	var arrURL=tmpArr[0].split(',');
	for(j=0;j<arrURL.length;j++){
		if(scLHRnogoto.indexOf(arrURL[j])>0 ||
			((arrURL[j].toLowerCase()).indexOf('type=')==0 && arrURL[j].toLowerCase().substring(5)==scType)){
			strboolFound=true;
		}
		if(strboolFound){
			s_accountapp=tmpArr[1];
			strSClineofbiz=tmpArr[2];
			strSCapp=tmpArr[3];
			strSCbizunit=tmpArr[4];
			strSCappname=tmpArr[5];
			break;
		}
	}
	if(strboolFound){
		break;
	}
}

if (!s_account){
	var s_account=s_accountglob+(s_accountapp!=''?','+s_accountapp:'');
}

var arSCValid=new Array('verizontelecomglobal','verizontelecomres','verizontelecomcfr','verizontelecomcrp','verizontelecoment','verizontelecomesp','verizontelecomfdv','verizontelecomflx','verizontelecomftc','verizontelecommas','verizontelecommdd','verizontelecommdm','verizontelecommsgctr','verizontelecommsm','verizontelecommyvz','verizontelecomoas','verizontelecomrap','verizontelecomshp','verizontelecomsmb','verizontelecomvca','verizontelecomvzc','verizontelecomwel','verizontelecomwhn','verizontelecomsso','verizontelecomslf','verizontelecomfioshub');

s_account=s_account.replace(/\s/g,'');
var arSAcc=s_account.split(',');
var scbErrType='';
var scbErrAccount=s_account;
for(x=0;x<arSAcc.length;x++){
	var scBoolAccOK=false;
	for(i=0;i<arSCValid.length;i++){
		if(arSCValid[i].indexOf(arSAcc[x])>-1){scBoolAccOK=true;break;}
	}
	if(scBoolAccOK==false){
		if(typeof(sc_84406DEV)!='undefined' && sc_84406DEV){
			// temp for LR -- do nothing in PROD
		}else{
			s_account='verizontelecomcleanup';
			scbErrType='1';
			break;
		}
	}
}
if(s_account=='verizontelecomglobal'){
	s_account+=',verizontelecomcleanup';
	scbErrType='2';
}
var s_837=s_gi(s_account)

if(scbErrType=='1'){
	s_837.prop20=document.URL;
	s_837.pfxID='[invalidRSID]';
	s_837.detailpageName=scbErrAccount;
}
if(scbErrType=='2'){
	s_837.prop20=document.URL;
	s_837.pfxID='[globalonly]';
	s_837.detailpageName=scbErrAccount;
}

if(!s_837.pfxID){s_837.pfxID=strSCapp.toLowerCase();}
if(!s_837.simplepageName){s_837.simplepageName=scLP.toLowerCase();}
s_837.simplepageName=(s_837.simplepageName).split('+').join(' ');
if(!s_837.detailpageName){s_837.detailpageName=s_837.simplepageName;}
s_837.detailpageName=(s_837.detailpageName).split('+').join(' ');
//if(!s_837.prop4){s_837.prop4=scStrip(scLP.substring(0,scLP.lastIndexOf('/')));}

if(!s_837.prop2){s_837.prop2=strSCbizunit.toLowerCase();}
if(!s_837.prop6){s_837.prop6=strSClineofbiz.toLowerCase();}
if(!s_837.prop48){s_837.prop48=strSCappname.toLowerCase();}

/******   custom globals end   ******/

s_837.charSet="ISO-8859-1"
/* Conversion Config */
s_837.currencyCode="USD"
/* Link Tracking Config */
s_837.trackDownloadLinks=true
s_837.trackExternalLinks=true
s_837.trackInlineStats=false
s_837.linkDownloadFileTypes="exe,zip,wav,mp3,mov,mpg,avi,wmv,doc,pdf,xls,xlsx,docx"
s_837.linkInternalFilters="javascript:,verizon.com"
s_837.linkLeaveQueryString=false
s_837.linkTrackVars="None"
s_837.linkTrackEvents="None"
var _lc=true;

/* Plugin Config */
s_837.usePlugins=true
function s_doPlugins(s) {

	//Raw marketing campaign tracking
	if(!s_837.campaign){
		s_837.campaign=s_837.Util.getQueryParam('CMP').toLowerCase();
	}
	if(!s_837.campaign){
		s_837.campaign=s_837.Util.getQueryParam('PromoTCode').toLowerCase();
	}
	s_837.eVar31="D=v0"
	s_837.eVar32="D=v0"
	s_837.eVar33="D=v0"
	s_837.prop31="D=v0"
	if(!s_837.eVar37){
		s_837.eVar37=s_837.Util.getQueryParam('incid');
	}

	var scAddressID=document.getElementById('address_id');
	if(scAddressID!=null && scAddressID.value!=''){
		s_837.prop43=scAddressID.value.toLowerCase();
	}
	s_837.eVar43="D=c43";

	if(!s_837.eVar44){
		s_837.eVar44 = s_837.Util.getQueryParam('txid');
	}
	if(!s_837.eVar76){
		s_837.eVar76 = s_837.Util.getQueryParam('flcid');
	}

	if(!s_837.pageLanguage){
		if(window.location.hostname.indexOf('espanol')>-1){s_837.pageLanguage='es';}
		else{s_837.pageLanguage='en';}
	}
	s_837.pageLanguage=s_837.pageLanguage.toLowerCase();

	if(!s_837.prop38){s_837.prop38="vz"}
	s_837.prop38=s_837.prop38.toLowerCase();

	//validate prefix
	if(!s_837.pfxID){s_837.pfxID="[unset]"}
	else{s_837.pfxID=s_837.pfxID.toLowerCase()
	}
	if(!s_837.simplepageName){s_837.simplepageName="[unset]"}
	else{s_837.simplepageName=s_837.simplepageName.toLowerCase()
	}
	if(!s_837.detailpageName){s_837.detailpageName=document.URL.split("?")[0].toLowerCase()}
	else{s_837.detailpageName=s_837.detailpageName.toLowerCase()
	}
	s_837.pageName=s_837.pfxID + "| " + s_837.simplepageName
	s_837.eVar30="D=pageName"
	s_837.prop1=s_837.pfxID  + "| " + s_837.detailpageName
	s_837.eVar1="D=c1"

	//Check contents of c2 and c3, set for validation & set v2 and v3
	if(!s_837.prop2){s_837.prop2="[unset]"}
	else{s_837.prop2=s_837.prop2.toLowerCase()
	}
	s_837.eVar2="D=c2"
	if(!s_837.prop3){s_837.prop3="[unset]"}
	else{s_837.prop3=s_837.prop3.toLowerCase()
	}
	s_837.eVar3="D=c3"

	//Channel Tracking
	s_837.channel=s_837.prop48+"| "+s_837.prop3+"- "+s_837.pageLanguage
	s_837.channel=s_837.channel.toLowerCase();

	//Set with current content channel value from HBX
	if(!s_837.prop4){s_837.prop4="[novconvalueinhbx]"}
	else{if((s_837.prop4).charAt(0)!='/'){s_837.prop4='/'+s_837.prop4;}}
	s_837.prop4=s_837.prop4.toLowerCase();
	s_837.eVar4="D=c4"

	//Get Profile ID
	if(!s_837.prop5){s_837.prop5="not available"}
	else{s_837.prop5=s_837.prop5.toLowerCase()
	}
	s_837.eVar5="D=c5"
	s_837.eVar25="D=c5"
	s_837.eVar26="D=c5"

	//Set Line of Business (LOB) change residential to [unset] when moved to GHF
	if(!s_837.prop6){s_837.prop6="residential"}
	s_837.prop6=s_837.prop6.toLowerCase();
	s_837.eVar6="D=c6"

	//Get Offer ID
	if(!s_837.prop7){}
	else{s_837.prop7=s_837.prop7.toLowerCase()
	}
	s_837.eVar7="D=c7"
	s_837.eVar38="D=c7"
	s_837.eVar39="D=c7"

	//Get New vs. Repeat
	s_837.prop9=s_837.getNewRepeat(365)
	s_837.eVar9="D=c9"

	//Get Registration Status
	if(!s_837.prop10){
		if(typeof(bIsLogin)!='undefined'&&bIsLogin&&typeof(bLogBiz)!='undefined'&&!bLogBiz){s_837.prop10="res logged in";}
		else if(typeof(bIsLogin)!='undefined'&&!bIsLogin&&typeof(bLogBiz)!='undefined'&&bLogBiz){s_837.prop10="smb logged in";}
		else if(typeof(bIsLogin)!='undefined'&&bIsLogin&&typeof(bLogBiz)!='undefined'&&bLogBiz){s_837.prop10="res/smb logged in";}
		else{s_837.prop10="Anonymous";}
	}
	if(s_837.prop10&&s_837.prop10!="Anonymous"){s_837.prop10=s_837.prop10.toLowerCase()}
	s_837.eVar10="D=c10"

	//Get Days Since Last Visit (fix)
	s_837.prop13=s_837.getDaysSinceLastVisit('s_lv')
	s_837.eVar13="D=c13"

	// Get TimeParting, Hourly
	s_837.prop14=s_837.getTimeParting('h','-5')
	s_837.eVar14="D=c14"

	// Get TimeParting, Daily
	s_837.prop15=s_837.getTimeParting('d','-5')
	s_837.eVar15="D=c15"

	//Get Visit Number
	s_837.prop16=s_837.getVisitNum(365)
	s_837.eVar16="D=c16"

	//Get Previous Page
	s_837.prop17=s_837.getPreviousValue(s_837.pageName,'gpv_p17','')
	s_837.eVar17="D=c17"

	//Get Creative ID
	if(!s_837.prop19){}
	else{s_837.prop19=s_837.prop19.toLowerCase()
	}
	s_837.eVar19="D=c19"

	//Get URL plus page identifiable qs params and not user identifiable qs params
	//ADD Util.getQueryParam
	if(!s_837.prop20){
		s_837.prop20=document.URL.split("?")[0].toLowerCase();
		s_837.prop20=s_837.prop20.split("#")[0].toLowerCase();
		s_837.prop20=s_837.prop20.split("://")[1].toLowerCase();
	}
	else{s_837.prop20=s_837.prop20.toLowerCase()
	}
	s_837.eVar20="D=c20"

	//Get Domain Name
	if(!s_837.prop21){s_837.prop21=document.location.host.toLowerCase()}
	else{s_837.prop21=s_837.prop21.toLowerCase()
	}
	s_837.eVar21="D=c21"

	//Get DMA Value
	if(!s_837.prop22){s_837.prop22="no dma value"}
	else{s_837.prop22=s_837.prop22.toLowerCase()
	}
	s_837.eVar22="D=c22"

	// Get Coupon Code
	if(s_837.prop24){s_837.prop24=(s_837.prop24).toLowerCase();}
	s_837.eVar24="D=c24"

	// Get Country
	if(s_837.prop26){s_837.prop26=(s_837.prop26).toLowerCase();}

	//Alternate Campaign Stacking
	s_837.eVar29=s_837.crossVisitParticipation(s_837.campaign,'s_ev29','30','10','>','purchase','1')

	// Get Navigation Used
	if(s_837.prop35){s_837.prop35=(s_837.prop35).toLowerCase();}
	s_837.eVar35="D=c35"

	// Get Internal Search Terms
	if(!s_837.prop36 && document.cookie.indexOf('SCSEARCH=')>-1){
		var scbTerm=document.cookie.substring(document.cookie.indexOf('SCSEARCH=')+9);
		if(scbTerm.indexOf(';')>-1){scbTerm=scbTerm.substring(0,scbTerm.indexOf(';'));}
		s_837.prop36=scbTerm;
		if(typeof(scsearchset)=='undefined'){
			document.cookie='SCSEARCH=; path=/; domain=.verizon.com; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
		}
	}
	if(s_837.prop36){s_837.prop36=s_837.prop36.toLowerCase();}
	s_837.eVar36="D=c36"

	//Flow Name
	if(s_837.prop45){
		if(s_837.prop45.indexOf('|')==-1){
			s_837.prop45=s_837.pfxID+"| "+s_837.prop45.toLowerCase();
		}
	}
	s_837.eVar45="D=c45"


	// Get Qual Attempt
	if(s_837.prop46){s_837.prop46=(s_837.prop46).toLowerCase();}
	s_837.eVar46="D=c46"

	// Get Name of Application
	if(s_837.prop48){s_837.prop48=(s_837.prop48).toLowerCase();}

	//Get Personalization/Channel New or Repeat Visitor
	if(s_837.prop74==1){s_837.prop74="repeat"}
	else if(s_837.prop74==0){s_837.prop74="new"}
	if(s_837.prop74)s_837.prop74=(s_837.prop74).toLowerCase()
	if (!s_837.prop74)s_837.prop74="not available"
	s_837.eVar74="D=c74"

	//Get Verizon User ID
	if(!s_837.prop75){s_837.prop75="no verizon uid"}
	s_837.prop75=(s_837.prop75).toLowerCase();
	s_837.eVar75="D=c75"

	if(!s_837.events){s_837.events="event1"}
	else{s_837.events+=",event1"}

	// Get Qual Response
	if(typeof(cv)!='undefined'&&typeof(cv.c5)!='undefined'&&(cv.c5).length > 0){
		var sTemp='';
		var sTempArr=(cv.c5).split('|');
		sTemp=sTempArr[0];
		//var sTempArr=(sTemp).split('-');
		if(sTempArr.length > 1){s_837.prop47=sTemp;}
	}
	if(s_837.prop47&&(s_837.events).indexOf('event90')==-1){s_837.events+=',event90,event99';}
	if(s_837.prop47){
		s_837.prop47=(s_837.prop47).toLowerCase();
	}
	s_837.eVar47="D=c47";

	// Get Customer select bundle
	if(typeof(cv)!='undefined'&&typeof(cv.c6)!='undefined'&&(cv.c6).length > 0){
		var sTemp='';
		var sTemp2='';
		var sTempArr=(cv.c6).split('|');
		sTemp=sTempArr[0];
		if(sTempArr.length > 1){sTemp2=sTempArr[1];}
		if(sTemp != ''){s_837.prop23=sTemp;}
	}
	if(s_837.prop23){
		s_837.prop23=(s_837.prop23).toLowerCase();
	}
	s_837.eVar23="D=c23";

	if(!s_837.prop50 && document.cookie.indexOf('AkaSTrackingID=')>-1){
		var scbAka=document.cookie.substring(document.cookie.indexOf('AkaSTrackingID=')+15);
		if(scbAka.indexOf(';')>-1){
			scbAka=scbAka.substring(0,scbAka.indexOf(';'));
		}
		s_837.prop50=scbAka;
	}
	s_837.eVar50="D=c50";

	// check domain
	var scbGSIDname='GlobalSessionID';
	if(currentDomain.indexOf('25')>-1){scbGSIDname+='_25';}
	else if(currentDomain.indexOf('26')>-1){scbGSIDname+='_26';}
	else if(currentDomain.indexOf('98')>-1){scbGSIDname+='_98';}
	// check Sess + Ord ids
	if(!s_837.prop8 && document.cookie.indexOf(scbGSIDname)>-1){
		var scbGSID=document.cookie.substring(document.cookie.indexOf(scbGSIDname)+(scbGSIDname.length+1));
		if(scbGSID.indexOf(';')>-1){scbGSID=scbGSID.substring(0,scbGSID.indexOf(';'));}
		if(scbGSID.length>0){s_837.prop8=scbGSID;}
		var scbEOrd=document.getElementById('hdn_Encses');
		if(scbEOrd!=null && scbEOrd.value!=''){
			if(s_837.prop8){s_837.prop8+='| '+scbEOrd.value;}
		}
		s_837.eVar8="D=c8";
	}else if(!s_837.prop8){
		s_837.prop8='no cookie';
		s_837.eVar8="D=c8";
	}

	// Get Bundle and session ID
	if(typeof(order)!='undefined'&&typeof(order.attr1)!='undefined'&&(order.attr1).length > 0){
		var sTemp='';
		var sTempArr=(order.attr1).split('|');
		sTemp=sTempArr[0];
		s_837.prop44=sTemp;
	}
	if(s_837.prop44){
		s_837.prop44=(s_837.prop44).toLowerCase();
	}
	s_837.eVar34="D=c44";

	// remove duplicate event reference
	var arrSorted=(s_837.events).split(',');
	arrSorted.sort();
	var strTemp='';
	for(var i=0;i<arrSorted.length;i++){
		if(arrSorted[i]!=arrSorted[i+1]){
			if(strTemp!=''){strTemp+=','}
			strTemp+=arrSorted[i];
		}
	}
	s_837.events=strTemp;

	//Populate workstream custom conversion variables
	//s_837.pfxID=(scType!='res'?s_837.pfxID+="| ":'')

	if(s_837.prop51){
		if(!s_837.eVar51)s_837.eVar51="D=c51"}

	if(s_837.prop52){
		if(!s_837.eVar52)s_837.eVar52="D=c52"}

	if(s_837.prop53){
		if(!s_837.eVar53)s_837.eVar53="D=c53"}

	if(s_837.prop54){
		if(!s_837.eVar54)s_837.eVar54="D=c54"}

	if(s_837.prop55){
		if(!s_837.eVar55)s_837.eVar55="D=c55"}

	if(s_837.prop56){
		if(!s_837.eVar56)s_837.eVar56="D=c56"}

	if(s_837.prop57){
		if(!s_837.eVar57)s_837.eVar57="D=c57"}

	if(s_837.prop58){
		if(!s_837.eVar58)s_837.eVar58="D=c58"}

	if(s_837.prop59){
		if(!s_837.eVar59)s_837.eVar59="D=c59"}

	if(s_837.prop60){
		if(!s_837.eVar60)s_837.eVar60="D=c60"}

	if(s_837.prop61){
		if(!s_837.eVar61)s_837.eVar61="D=c61"}

	if(s_837.prop62){
		if(!s_837.eVar62)s_837.eVar62="D=c62"}

	if(s_837.prop63){
		if(!s_837.eVar63)s_837.eVar63="D=c63"}

	if(s_837.prop64){
		if(!s_837.eVar64)s_837.eVar64="D=c64"}

	if(s_837.prop65){
		if(!s_837.eVar65)s_837.eVar65="D=c65"}

	if(s_837.prop66){
		if(!s_837.eVar66)s_837.eVar66="D=c66"}

	if(s_837.prop67){
		if(!s_837.eVar67)s_837.eVar67="D=c67"}

	if(s_837.prop68){
		if(!s_837.eVar68)s_837.eVar68="D=c68"}

	if(s_837.prop69){
		if(!s_837.eVar69)s_837.eVar69="D=c69"}

	if(s_837.prop70){
		if(!s_837.eVar70)s_837.eVar70="D=c70"}

	if(s_837.prop71){
		if(!s_837.eVar71)s_837.eVar71="D=c71"}

	if(s_837.prop72){
		if(!s_837.eVar72)s_837.eVar72="D=c72"}

	if(s_837.prop73){
		if(!s_837.eVar73)s_837.eVar73="D=c73"}


	if(!vzCustomLt){
		s_837.linkTrackVars="prop37,prop27,prop11,prop12";
		s_837.hbx_lt="auto";
		s_837.setupLinkTrack("prop37,prop27,prop11,prop12","SC_LINK");
		s_837.linkTrackVars="";
	}else{
		vzCustomLt = false;
	}

	s_837.hier1='D=c38'+'||'+'D=c6'+'||'+'D=c2'+'||'+'D=c48'+'||'+'D=c3'+'||'+'D=c20';

	//Set the listVars appropriately
	var SCDMP='';
	if(document.getElementById('inputString2AK_ff') != null){SCDMP=document.getElementById('inputString2AK_ff').value;}
	if(SCDMP != ''){
		var listSplit=SCDMP.split(',');
		s_837.list1='';
		for(var l=0;l<listSplit.length;l++){
			if(listSplit[l].indexOf('-')!=-1){
				s_837.list1+=listSplit[l] + '^';
			}
		}
		if(s_837.list1.length>0)s_837.list1=s_837.list1.slice(0, s_837.list1.length - 1);
	}
	//Set the listVars2 appropriately
	var SCXP1Resp=document.getElementById('Xp1Response');
	var SCXP1QS=document.getElementById('xp1QueryString');
	var arrSSV=new Array();
	if(SCXP1Resp!=null && SCXP1Resp.value!='' && SCXP1Resp.value.indexOf('xp1QString')>-1){
		// try new xp1QString (in JSON) first
		var SCXP1json=SCXP1Resp.value;
		SCXP1json=SCXP1json.replace(/\s/g,'');
		var SCXP1JQS=SCXP1json.substring(SCXP1json.indexOf('xp1QString'),SCXP1json.indexOf('}',SCXP1json.indexOf('xp1QString')));
		SCXP1JQS=SCXP1JQS.substring(SCXP1JQS.indexOf(':')+1);
		arrSSV=SCXP1JQS.split('&');
	}else if(SCXP1QS!=null && SCXP1QS.value!=''){
		// else try xp1QueryString (string value)
		arrSSV=SCXP1QS.value.split('?')[1].split('&');
	}
	if(arrSSV.length>0){
		s_837.list2='';
		for(var z=0;z<arrSSV.length;z++){
			if(arrSSV[z].substr(0,3)=='ssv' && arrSSV[z].split('=')[1]!=''){
				s_837.list2+=arrSSV[z].substring(arrSSV[z].indexOf('_')+1) + '^';
			}
		}
		if(s_837.list2.length>0)s_837.list2=s_837.list2.slice(0, s_837.list2.length - 1);
		var SCXP1pid=document.getElementById('xp1ProfileId');
		if(SCXP1pid!=null && SCXP1pid.value!=''){
			var listSplit=SCXP1pid.value.split('-');
			for(var l=0;l<listSplit.length;l++){
				if(listSplit[l].charAt(0)=='v'){
					s_837.list2 += '^'+listSplit[l];
				}
			}
		}
	}

	// ord_dcenter check
	var scOrdDcenter=document.getElementById('ord_dcenter');
	if(scOrdDcenter!=null && scOrdDcenter.value!=''){
		s_837.server=scOrdDcenter.value.toLowerCase();
	}
}

s_837.doPlugins=s_doPlugins


/*
 * Plugin: YouTube plugin SC14.9/15 v1.4
 */
var s_YTO={};s_YTO.v=new Object;s_YTO.ya=s_YTisa()?2:0;s_YTO.ut=s_YTO.uf=0;s_YTO.vp='YouTube Player';if(document.loaded){s_YTp()}else if(window.addEventListener){window.addEventListener('load',s_YTp,false)}else if(window.attachEvent){window.attachEvent('onload',s_YTp)}else{s_YTp()}function onYouTubePlayerReady(id){if(id&&typeof id=='string'){var p=document.getElementById(id);if(p&&!s_YTO.v[id])s_YTO.v[id]=new s_YTv(id,1)}}function s_YTp(){try{var f=document.getElementsByTagName('iframe');if(s_YTisa())s_YTO.ya=2;for(var i=0;i<f.length;i++){var k=s_YTgk(f[i].src),id=f[i].id;if(k){if(!s_YTO.ya){s_YTO.ya=1;var t=document.createElement('script'),f;t.src='../../../www.youtube.com/player_api.js'/*tpa=http://www.youtube.com/player_api*/;f=document.getElementsByTagName('script')[0];f.parentNode.insertBefore(t,f)}else if(s_YTO.ya==2&&!s_YTO.v[id]){s_YTO.v[id]=new s_YTv(id)}}}}catch(e){};s_YTO.ut=setTimeout('s_YTp()',1000)}function s_YTisa(){return typeof window.YT=='object'&&typeof YT.Player}function s_YTism(){return typeof window.s_837=='object'&&typeof s_837.Media=='object'&&s_837.Media.open}function s_YTgk(u){var r='';try{var a,b,c,d;if(u.indexOf('//www.youtube.com/watch')>-1){a=u.indexOf('?');if(a>-1){b='&'+u.substring(a+1);c=b.indexOf('&v=');if(c>-1){r=b.substring(c+3);d=r.indexOf('&');if(d>-1)r=r.substring(0,d)}}}if(u.indexOf('//www.youtube.com/embed/')>-1){a=u.indexOf('/embed/')+7;r=u.substring(a+7);d=r.indexOf('?');if(d>-1)r=r.substring(0,d)}}catch(e){};return r}function onYouTubePlayerAPIReady(){try{s_YTO.ya=2;if(s_YTO.ut)clearTimeout(s_YTO.ut);s_YTp()}catch(e){}}function s_YTdi(){try{if(!s_YTism())return;if(typeof s_837.Media.trackWhilePlaying!='undefined'){s_YTO.twp=s_837.Media.trackWhilePlaying;s_837.Media.trackWhilePlaying=false}if(typeof s_837.Media.trackSeconds!='undefined'){s_YTO.ts=s_837.Media.trackSeconds;delete s_837.Media.trackSeconds}}catch(e){}}function s_YTei(){try{if(!s_YTism())return;if(typeof s_YTO.twp!='undefined'){s_837.Media.trackWhilePlaying=s_YTO.twp;delete s_YTO.twp}if(typeof s_YTO.ts!='undefined'){s_837.Media.trackSeconds=s_YTO.ts;delete s_YTO.ts}}catch(e){}}function s_YTut(){try{s_YTO.uf=0;s_YTei()}catch(e){}}function s_YTdv(id){try{if(!id)return;var v=s_YTO.v[id]||0;if(v){if(v.ss){if(s_837.Media)s_837.Media.close(v.sv);v.ss=0}}v.vc()}catch(e){}}function s_YTv(id){try{var t=this;t.vc=function(){var t=this;t.id=t.st=t.sv=t.sl='';t.yt=t.yp=t.ys=t.ss=t.ts=t.ql=t.qs=0};t.vg=function(yp){try{var t=this,F='function',O='object',N='number',S='string',u='',x=u,y=u,pt=typeof yp;if(pt==O||pt==F){if(typeof yp.getVideoUrl==F)u=yp.getVideoUrl();if(typeof yp.getVideoData==F){x=yp.getVideoData();if(typeof x==O){if(typeof x.video_id==S)y=x.video_id;if(typeof x.title==S)s_837.st=x.title}}if(!y&&u)y=s_YTgk(u);t.sv='YouTube';t.sv+='|'+(y?y:t.id);if(t.st)t.sv+='|'+t.st;if((typeof yp.getPlayerState)==F){x=yp.getPlayerState();if(typeof x==N)t.ys=x}t.qs=0;if((typeof yp.getCurrentTime)==F){x=yp.getCurrentTime();t.qs=(typeof x==N)?Math.round(x):0}t.ts=0;if((typeof yp.getDuration)==F){x=yp.getDuration();t.ts=(typeof x==N)?Math.round(x):0}}}catch(e){}};t.ve=function(){try{var t=this;if(!s_YTism()||!t.sv)return;t.vg(t.yp);if(t.sv!=t.sl&&t.ss){if(t.ss==2){s_837.Media.stop(t.sl,t.ql);t.ss=1}s_837.Media.close(t.sl);t.sl=t.sv;t.ss=t.ql=0}switch(t.ys){case 1:if(t.ss==2){if(t.qs>=t.ql&&Math.abs(t.qs-t.ql)<1.0)return;s_837.Media.stop(t.sl,t.ql)}if(!t.ss){s_837.Media.open(t.sv,t.ts,s_YTO.vp);t.qs=t.ql=0;t.sl=t.sv;t.ss=1}s_837.Media.play(t.sv,t.qs);t.ql=t.qs;t.ss=2;break;case 0:if(t.ss){if(t.ss!=1){if(Math.abs(t.qs-t.ts)<=1)t.qs=t.ts;s_837.Media.stop(t.sv,t.qs);t.ql=t.qs;t.ss=1}s_837.Media.close(t.sv);t.ss=t.qs=t.ql=0;t.sv=t.sl=''}break;case 2:if(!t.ss){s_837.Media.open(t.sv,t.ts,s_YTO.vp);t.ss=1;t.sl=t.sv}if(t.ss!=1){s_837.Media.stop(t.sv,t.qs);t.ql=t.qs;t.ss=1}break;case 3:if(s_YTO.uf){clearTimeout(s_YTO.uf)}else{s_YTdi()}s_YTO.uf=setTimeout('s_YTut()',3000);break;case 5:break;case-1:s_837.Media.open(t.sv,t.ts,s_YTO.vp);t.ss=1;t.sl=t.sv;break;default:break}}catch(e){}};t.fsc=function(ye){t.ys=ye;t.vg(t.yp);setTimeout('s_YTO.v["'+t.id+'"].ve()',10)};t.isc=function(ye){t.ys=ye.data;t.vg(ye.target);setTimeout('s_YTO.v["'+t.id+'"].ve()',10)};var o=id&&typeof id=='string'?document.getElementById(id):'';if(!o)return null;t.vc();t.id=id;var ar=arguments;if(ar.length>1&&ar[1]==1){t.yt=1;t.yp=o;if(window.addEventListener){t.yp.addEventListener('onStateChange','s_YTO.v.'+id+'.fsc',false)}else if(window.attachEvent){window.attachEvent('onStateChange','s_YTO.v.'+id+'.fsc')}}else{t.yt=2;var a=new Object();if(ar.length>1)a.videoId=ar[1];if(ar.length>3){a.width=w;a.height=h}a.events=new Object();a.events.onStateChange=t.isc;try{t.yp=new YT.Player(id,a)}catch(e){s_YTdv(id);t=null}}return t}catch(e){return null}}

/*
 * channelManager v2.5 - Tracking External Traffic
 */
s_837.channelManager=new Function("a","b","c","d","e","f",""
+"var s=this,A,B,g,l,m,M,p,q,P,h,k,u,S,i,O,T,j,r,t,D,E,F,G,H,N,U,v=0,"
+"X,Y,W,n=new Date;n.setTime(n.getTime()+1800000);if(e){v=1;if(s_837.c_r("
+"e))v=0;if(!s_837.c_w(e,1,n))s_837.c_w(e,1,0);if(!s_837.c_r(e))v=0;}g=s_837.referrer"
+"?s_837.referrer:document.referrer;g=g.toLowerCase();if(!g)h=1;i=g.index"
+"Of('?')>-1?g.indexOf('?'):g.length;j=g.substring(0,i);k=s_837.linkInter"
+"nalFilters.toLowerCase();k=s_837.split(k,',');for(m=0;m<k.length;m++){B"
+"=j.indexOf(k[m])==-1?'':g;if(B)O=B;}if(!O&&!h){p=g;U=g.indexOf('//'"
+");q=U>-1?U+2:0;Y=g.indexOf('/',q);r=Y>-1?Y:i;u=t=g.substring(q,r).t"
+"oLowerCase();P='Other Natural Referrers';S=s_837.seList+'>'+s_837._extraSea"
+"rchEngines;if(d==1){j=s_837.repl(j,'oogle','%');j=s_837.repl(j,'ahoo','^');"
+"g=s_837.repl(g,'as_q','*')}A=s_837.split(S,'>');for(i=0;i<A.length;i++){D=A"
+"[i];D=s_837.split(D,'|');E=s_837.split(D[0],',');for(G=0;G<E.length;G++){H="
+"j.indexOf(E[G]);if(H>-1){i=s_837.split(D[1],',');for(k=0;k<i.length;k++"
+"){l=s_837.Util.getQueryParam(i[k],'',g).toLowerCase();if(l){M=l;if(D[2])N=u="
+"D[2];else N=t;if(d==1){N=s_837.repl(N,'#','-');g=s_837.repl(g,'*','as_q');N"
+"=s_837.repl(N,'^','ahoo');N=s_837.repl(N,'%','oogle');}}}}}}}if(!O||f!='1')"
+"{O=s_837.Util.getQueryParam(a,b);if(O){u=O;if(M)P='Paid Search';else P='Unkn"
+"own Paid Channel';}if(!O&&M){u=N;P='Natural Search';}}if(h==1&&!O&&"
+"v==1)u=P=t=p='Typed/Bookmarked';g=s_837._channelDomain;if(g){k=s_837.split("
+"g,'>');;for(m=0;m<k.length;m++){q=s_837.split(k[m],'|');r=s_837.split(q[1],"
+"',');S=r.length;for(T=0;T<S;T++){Y=r[T];Y=Y.toLowerCase();i=j.index"
+"Of(Y);if(i>-1)P=q[0];}}}g=s_837._channelParameter;if(g){k=s_837.split(g,'>'"
+");h;for(m=0;m<k.length;m++){q=s_837.split(k[m],'|');r=s_837.split(q[1],',')"
+";S=r.length;for(T=0;T<S;T++){U=s_837.Util.getQueryParam(r[T]);if(U)P=q[0];}}"
+"}g=s_837._channelPattern;if(g){k=s_837.split(g,'>');for(m=0;m<k.length;m++)"
+"{q=s_837.split(k[m],'|');r=s_837.split(q[1],',');S=r.length;for(T=0;T<S;T++"
+"){Y=r[T];Y=Y.toLowerCase();i=O.toLowerCase();H=i.indexOf(Y);if(H==0"
+")P=q[0];}}}X=P+M+t;c=c?c:'c_m';if(c!='0')X=s_837.getValOnce(X,c,0);if(X"
+"){s_837._referrer=p?p:'n/a';s_837._referringDomain=t?t:'n/a';s_837._partner=N?N"
+":'n/a';s_837._campaignID=O?O:'n/a';s_837._campaign=u?u:'n/a';s_837._keywords=M?"
+"M:'n/a';s_837._channel=P?P:'n/a';}");
/* Top 130 - Grouped */
s_837.seList="altavista.co,altavista.de|q,r|AltaVista>.aol.,suche.aolsvc"
+".de|q,query|AOL>ask.jp,ask.co|q,ask|Ask>www.baidu.com|wd|Baidu>daum"
+".net,search.daum.net|q|Daum>google.,googlesyndication.com|q,as_q|Go"
+"ogle>icqit.com|q|icq>bing.com|q|Microsoft Bing>myway.com|searchfor|"
+"MyWay.com>naver.com,search.naver.com|query|Naver>netscape.com|query"
+",search|Netscape Search>reference.com|q|Reference.com>seznam|w|Sezn"
+"am.cz>abcsok.no|q|Startsiden>tiscali.it,www.tiscali.co.uk|key,query"
+"|Tiscali>virgilio.it|qs|Virgilio>yahoo.com,yahoo.co.jp|p,va|Yahoo!>"
+"yandex|text|Yandex.ru>search.cnn.com|query|CNN Web Search>search.ea"
+"rthlink.net|q|Earthlink Search>search.comcast.net|q|Comcast Search>"
+"search.rr.com|qs|RoadRunner Search>optimum.net|q|Optimum Search";
/*
 * Plugin: getNewRepeat 1.2 - Returns whether user is new or repeat
 */
s_837.getNewRepeat=new Function("d","cn",""
+"var s=this,e=new Date(),cval,sval,ct=e.getTime();d=d?d:30;cn=cn?cn:"
+"'s_nr';e.setTime(ct+d*24*60*60*1000);cval=s_837.c_r(cn);if(cval.length="
+"=0){s_837.c_w(cn,ct+'-New',e);return'New';}sval=s_837.split(cval,'-');if(ct"
+"-sval[0]<30*60*1000&&sval[1]=='New'){s_837.c_w(cn,ct+'-New',e);return'N"
+"ew';}else{s_837.c_w(cn,ct+'-Repeat',e);return'Repeat';}");
/*
 * Plugin Utility: apl v1.1
 */
s_837.apl=new Function("l","v","d","u",""
+"var s=this,m=0;if(!l)l='';if(u){var i,n,a=s_837.split(l,d);for(i=0;i<a."
+"length;i++){n=a[i];m=m||(u==1?(n==v):(n.toLowerCase()==v.toLowerCas"
+"e()));}}if(!m)l=l?l+d+v:v;return l");
/*
 * Utility Function: split v1.5 (JS 1.0 compatible)
 */
s_837.split=new Function("l","d",""
+"var i,x=0,a=new Array;while(l){i=l.indexOf(d);i=i>-1?i:l.length;a[x"
+"++]=l.substring(0,i);l=l.substring(i+d.length);}return a");
/*
 * Plugin: Days since last Visit 1.0.H - capture time from last visit
 */
s_837.getDaysSinceLastVisit=new Function(""
+"var s=this,e=new Date(),cval,ct=e.getTime(),c='s_lastvisit',day=24*"
+"60*60*1000;e.setTime(ct+3*365*day);cval=s_837.c_r(c);if(!cval){s_837.c_w(c,"
+"ct,e);return 'First page view or cookies not supported';}else{var d"
+"=ct-cval;if(d>30*60*1000){if(d>30*day){s_837.c_w(c,ct,e);return 'More t"
+"han 30 days';}if(d<30*day+1 && d>7*day){s_837.c_w(c,ct,e);return 'More "
+"than 7 days';}if(d<7*day+1 && d>day){s_837.c_w(c,ct,e);return 'Less tha"
+"n 7 days';}if(d<day+1){s_837.c_w(c,ct,e);return 'Less than 1 day';}}els"
+"e return '';}"
);
/*
* Plugin: getVisitNum - version 3.0
*/
s_837.getVisitNum=new Function("tp","c","c2",""
+"var s=this,e=new Date,cval,cvisit,ct=e.getTime(),d;if(!tp){tp='m';}"
+"if(tp=='m'||tp=='w'||tp=='d'){eo=s_837.endof(tp),y=eo.getTime();e.setTi"
+"me(y);}else{d=tp*86400000;e.setTime(ct+d);}if(!c){c='s_vnum';}if(!"
+"c2){c2='s_invisit';}cval=s_837.c_r(c);if(cval){var i=cval.indexOf('&vn="
+"'),str=cval.substring(i+4,cval.length),k;}cvisit=s_837.c_r(c2);if(cvisi"
+"t){if(str){e.setTime(ct+1800000);s_837.c_w(c2,'true',e);return str;}els"
+"e {return 'unknown visit number';}}else{if(str){str++;k=cval.substri"
+"ng(0,i);e.setTime(k);s_837.c_w(c,k+'&vn='+str,e);e.setTime(ct+1800000);"
+"s_837.c_w(c2,'true',e);return str;}else{s_837.c_w(c,e.getTime()+'&vn=1',e)"
+";e.setTime(ct+1800000);s_837.c_w(c2,'true',e);return 1;}}");
s_837.dimo=new Function("m","y",""
+"var d=new Date(y,m+1,0);return d.getDate();");
s_837.endof=new Function("x",""
+"var t=new Date;t.setHours(0);t.setMinutes(0);t.setSeconds(0);if(x=="
+"'m'){d=s_837.dimo(t.getMonth(),t.getFullYear())-t.getDate()+1;}else if("
+"x=='w'){d=7-t.getDay();}else{d=1;}t.setDate(t.getDate()+d);return "
+"t;");
/*
 * Plugin: getPreviousValue_v1.0 - return previous value of designated
 *  variable (requires split utility)
 */
s_837.getPreviousValue=new Function("v","c","el",""
+"var s=this,t=new Date,i,j,r='';t.setTime(t.getTime()+1800000);if(el"
+"){if(s_837.events){i=s_837.split(el,',');j=s_837.split(s_837.events,',');for(x in i"
+"){for(y in j){if(i[x]==j[y]){if(s_837.c_r(c)) r=s_837.c_r(c);v?s_837.c_w(c,v,t)"
+":s_837.c_w(c,'no value',t);return r}}}}}else{if(s_837.c_r(c)) r=s_837.c_r(c);v?"
+"s_837.c_w(c,v,t):s_837.c_w(c,'no value',t);return r}");
/*
 * Plugin: getValOnce_v1.1
 */
s_837.getValOnce=new Function("v","c","e","t",""
+"var s=this,a=new Date,v=v?v:'',c=c?c:'s_gvo',e=e?e:0,i=t=='m'?6000"
+"0:86400000;k=s_837.c_r(c);if(v){a.setTime(a.getTime()+e*i);s_837.c_w(c,v,e"
+"==0?0:a);}return v==k?'':v");
/*
 * Plugin: getTimeParting 2.0
 */
s_837.getTimeParting=new Function("t","z","y","l",""
+"var s=this,d,A,U,X,Z,W,B,C,D,Y;d=new Date();A=d.getFullYear();Y=U=S"
+"tring(A);if(s_837.dstStart&&s_837.dstEnd){B=s_837.dstStart;C=s_837.dstEnd}else{;U=U"
+".substring(2,4);X='090801|101407|111306|121104|131003|140902|150801"
+"|161306|171205|181104|191003';X=s_837.split(X,'|');for(W=0;W<=10;W++){Z"
+"=X[W].substring(0,2);if(U==Z){B=X[W].substring(2,4);C=X[W].substrin"
+"g(4,6)}}if(!B||!C){B='08';C='01'}B='03/'+B+'/'+A;C='11/'+C+'/'+A;}D"
+"=new Date('1/1/2000');if(D.getDay()!=6||D.getMonth()!=0){return'Dat"
+"a Not Available'}else{z=z?z:'0';z=parseFloat(z);B=new Date(B);C=new"
+" Date(C);W=new Date();if(W>B&&W<C&&l!='0'){z=z+1}W=W.getTime()+(W.g"
+"etTimezoneOffset()*60000);W=new Date(W+(3600000*z));X=['Sunday','Mo"
+"nday','Tuesday','Wednesday','Thursday','Friday','Saturday'];B=W.get"
+"Hours();C=W.getMinutes();D=W.getDay();Z=X[D];U='AM';A='Weekday';X='"
+"00';if(C>30){X='30'}if(B>=12){U='PM';B=B-12};if(B==0){B=12};if(D==6"
+"||D==0){A='Weekend'}W=B+':'+X+U;if(y&&y!=Y){return'Data Not Availab"
+"le'}else{if(t){if(t=='h'){return W}if(t=='d'){return Z}if(t=='w'){r"
+"eturn A}}else{return Z+', '+W}}}");
/*
 *	Plug-in: crossVisitParticipation v1.6 - stacks values from
 *	specified variable in cookie and returns value
 */
s_837.crossVisitParticipation=new Function("v","cn","ex","ct","dl","ev","dv",""
+"var s=this,ce;if(typeof(dv)==='undefined')dv=0;if(s_837.events&&ev){var"
+" ay=s_837.split(ev,',');var ea=s_837.split(s_837.events,',');for(var u=0;u<ay.l"
+"ength;u++){for(var x=0;x<ea.length;x++){if(ay[u]==ea[x]){ce=1;}}}}i"
+"f(!v||v==''){if(ce){s_837.c_w(cn,'');return'';}else return'';}v=escape("
+"v);var arry=new Array(),a=new Array(),c=s_837.c_r(cn),g=0,h=new Array()"
+";if(c&&c!='')arry=eval(c);var e=new Date();e.setFullYear(e.getFullY"
+"ear()+5);if(dv==0&&arry.length>0&&arry[arry.length-1][0]==v)arry[ar"
+"ry.length-1]=[v,new Date().getTime()];else arry[arry.length]=[v,new"
+" Date().getTime()];var start=arry.length-ct<0?0:arry.length-ct;var "
+"td=new Date();for(var x=start;x<arry.length;x++){var diff=Math.roun"
+"d((td.getTime()-arry[x][1])/86400000);if(diff<ex){h[g]=unescape(arr"
+"y[x][0]);a[g]=[arry[x][0],arry[x][1]];g++;}}var data=s_837.join(a,{deli"
+"m:',',front:'[',back:']',wrap:\"'\"});s_837.c_w(cn,data,e);var r=s_837.join"
+"(h,{delim:dl});if(ce)s_837.c_w(cn,'');return r;");
/* s_837.join: 1.0 - Joins an array into a string */
s_837.join=new Function("v","p",""
+"var s=this;var f,b,d,w;if(p){f=p.front?p.front:'';b=p.back?p.back"
+":'';d=p.delim?p.delim:'';w=p.wrap?p.wrap:'';}var str='';for(var x=0"
+";x<v.length;x++){if(typeof(v[x])=='object' )str+=s_837.join( v[x],p);el"
+"se str+=w+v[x]+w;if(x<v.length-1)str+=d;}return f+str+b;");
/* Plugin: setupLinkTrack v3.15AM */
s_837.setupLinkTrack=new Function("vl","c","e",""
+"var s=this;var cv=s.c_r(c);if(vl){var vla=vl.split(',');}if(cv!='')"
+"{var cva=s.split(cv,'^^');if(cva[1]!=''){for(x in vla){s[vla[x]]=cv"
+"a[x];if(e){s.events=s.apl(s.events,e,',',2);}}}}s.c_w(c,'',0);if(ty"
+"peof s.linkObject!='undefined'&&s.hbx_lt!='manual'){s.lta=[];if(typ"
+"eof s.pageName!='undefined')s.lta[0]=s.pageName;if(typeof s.linkObj"
+"ect!=null){slo=s.linkObject;if(s.linkObject!=0){if(s.linkObject.get"
+"Attribute('name')!=null){var b=s.linkObject.getAttribute('name');if"
+"(b.indexOf('&lpos=')>-1){s.lta[3]=b.match('\&lpos\=([^\&]*)')[1];}i"
+"f(b.indexOf('&lid=')>-1){s.lta[1]=b.match('\&lid\=([^\&]*)')[1];}}}"
+"if(typeof s.lta[1]=='undefined'){if(s.linkName!=0){s.lta[1]=s.linkN"
+"ame;}else if(s.linkObject!=0){if(s.cleanStr(s.linkObject.innerHTML)"
+".length>0){s.lta[1]=s.cleanStr(s.linkObject.innerHTML);}else if(s.l"
+"inkObject.innerHTML.indexOf('<img')>-1){s.lta[1]=s.linkObject.inner"
+"HTML.match('alt=\"([^\"]*)')[1];if(!s.lta[1]){s.lta[1]=s.linkObject.i"
+"nnerHTML.match('src=\"([^\"]*)')[1]}}else{s.lta[1]=s.linkObject.inner"
+"HTML;}}}if(s.lta[1]!=null&&typeof s.lta[1]!='undefined'){if(typeof "
+"s.pageName!='undefined')s.lta[0]=s.pageName;s.lta[2]=s.pageName+' |"
+" '+s.lta[1];}}if(s.linkType!=0){for(var x=0;x<vla.length;x++){s[vla"
+"[x]]=s.cleanStr(s.lta[x]);if(e){s.events=s.apl(s.events,e,',',2);s."
+"linkTrackVars=s.apl(s.linkTrackVars,'events',',',2);}}s.linkTrackVa"
+"rs=s.apl(s.linkTrackVars,vl,',',2);}else{if(s.lta[1]){var tcv='';fo"
+"r(var x=0;x<s.lta.length;x++){tcv+=s.cleanStr(s.lta[x])+'^^'}s.c_w("
+"c,tcv)}}s.lta=null;}");
s_837.cleanStr = function(a){
	if(typeof a != 'undefined'){
		a = a.replace(/<\/?[^>]+(>|$)/g, '');
		a = a.replace(/^\s+|\s+$/g,'');
		return a;
	}
}
/* Utility: AppMeasurement Compatibility */
s_837.wd=window;
s_837.fl=new Function("x","l",""
+"return x?(''+x).substring(0,l):x");
s_837.pt=new Function("x","d","f","a",""
+"var s=this,t=x,z=0,y,r,l='length';while(t){y=t.indexOf(d);y=y<0?t[l"
+"]:y;t=t.substring(0,y);r=s[f](t,a);if(r)return r;z+=y+d[l];t=x.subs"
+"tring(z,x[l]);t=z<x[l]?t:''}return''");
s_837.rep=new Function("x","o","n",""
+"var a=new Array,i=0,j;if(x){if(x.split)a=x.split(o);else if(!o)for("
+"i=0;i<x.length;i++)a[a.length]=x.substring(i,i+1);else while(i>=0){"
+"j=x.indexOf(o,i);a[a.length]=x.substring(i,j<0?x.length:j);i=j;if(i"
+">=0)i+=o.length}}x='';j=a.length;if(a&&j>0){x=a[0];if(j>1){if(a.joi"
+"n)x=a.join(n);else for(i=1;i<j;i++)x+=n+a[i]}}return x");
s_837.ape=new Function("x",""
+"var s=this,h='0123456789ABCDEF',f='+~!*()\\'',i,c=s.charSet,n,l,e,y"
+"='';c=c?c.toUpperCase():'';if(x){x=''+x;if(s.em==3){x=encodeURIComp"
+"onent(x);for(i=0;i<f.length;i++){n=f.substring(i,i+1);if(x.indexOf("
+"n)>=0)x=s.rep(x,n,'%'+n.charCodeAt(0).toString(16).toUpperCase())}}"
+"else if(c=='AUTO'&&('').charCodeAt){for(i=0;i<x.length;i++){c=x.sub"
+"string(i,i+1);n=x.charCodeAt(i);if(n>127){l=0;e='';while(n||l<4){e="
+"h.substring(n%16,n%16+1)+e;n=(n-n%16)/16;l++}y+='%u'+e}else if(c=='"
+"+')y+='%2B';else y+=escape(c)}x=y}else x=s.rep(escape(''+x),'+','%2"
+"B');if(c&&c!='AUTO'&&s.em==1&&x.indexOf('%u')<0&&x.indexOf('%U')<0)"
+"{i=x.indexOf('%');while(i>=0){i++;if(h.substring(8).indexOf(x.subst"
+"ring(i,i+1).toUpperCase())>=0)return x.substring(0,i)+'u00'+x.subst"
+"ring(i);i=x.indexOf('%',i)}}}return x");
s_837.epa=new Function("x",""
+"var s=this,y,tcf;if(x){x=s.rep(''+x,'+',' ');if(s.em==3){tcf=new Fu"
+"nction('x','var y,e;try{y=decodeURIComponent(x)}catch(e){y=unescape"
+"(x)}return y');return tcf(x)}else return unescape(x)}return y");

/* Media Module Config */
s_837.loadModule('Media');
s_837.Media.autoTrack=true;
s_837.Media.playerName='Embedded YouTube Player V070913A';
s_837.Media.segmentByMilestones=true;
s_837.Media.trackMilestones='25,50,75,99';
s_837.Media.trackUsingContextData=true;
s_837.Media.contextDataMapping = {
	'a.contentType':'eVar28',
	'http://www.verizon.com/includes/javascript/a.media.name':'eVar8,prop18',
	'a.media.segment':'eVar27',
	'http://www.verizon.com/includes/javascript/a.media.view':'event16',
	'a.media.segmentView':'event17',
	'a.media.timePlayed':'event61',
	'a.media.milestones':{
		99:'event18'
	}
}
s_837.Media.trackVars='events,eVar8,eVar27,eVar28,prop18';
s_837.Media.trackEvents='event16,event17,event18,event61';

/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
s_837.visitorNamespace="verizontelecom"
s_837.trackingServer = "http://www.verizon.com/includes/javascript/analytics.verizon.com";
s_837.trackingServerSecure = "http://www.verizon.com/includes/javascript/sanalytics.verizon.com";

/****************************** MODULES *****************************/
/* Module: Media */
function AppMeasurement_Module_Media(q){var b=this;b.s=q;q=window;q.s_c_in||(q.s_c_il=[],q.s_c_in=0);b._il=q.s_c_il;b._in=q.s_c_in;b._il[b._in]=b;q.s_c_in++;b._c="s_m";b.list=[];b.open=function(d,c,e,k){var f={},a=new Date,l="",g;c||(c=-1);if(d&&e){b.list||(b.list={});b.list[d]&&b.close(d);k&&k.id&&(l=k.id);if(l)for(g in b.list)!Object.prototype[g]&&b.list[g]&&b.list[g].R==l&&b.close(b.list[g].name);f.name=d;f.length=c;f.offset=0;f.e=0;f.playerName=b.playerName?b.playerName:e;f.R=l;f.C=0;f.a=0;f.timestamp=
Math.floor(a.getTime()/1E3);f.k=0;f.u=f.timestamp;f.c=-1;f.n="";f.g=-1;f.D=0;f.I={};f.G=0;f.m=0;f.f="";f.B=0;f.L=0;f.A=0;f.F=0;f.l=!1;f.v="";f.J="";f.K=0;f.r=!1;f.H="";f.complete=0;f.Q=0;f.p=0;f.q=0;b.list[d]=f}};b.openAd=function(d,c,e,k,f,a,l,g){var h={};b.open(d,c,e,g);if(h=b.list[d])h.l=!0,h.v=k,h.J=f,h.K=a,h.H=l};b.M=function(d){var c=b.list[d];b.list[d]=0;c&&c.monitor&&clearTimeout(c.monitor.interval)};b.close=function(d){b.i(d,0,-1)};b.play=function(d,c,e,k){var f=b.i(d,1,c,e,k);f&&!f.monitor&&
(f.monitor={},f.monitor.update=function(){1==f.k&&b.i(f.name,3,-1);f.monitor.interval=setTimeout(f.monitor.update,1E3)},f.monitor.update())};b.click=function(d,c){b.i(d,7,c)};b.complete=function(d,c){b.i(d,5,c)};b.stop=function(d,c){b.i(d,2,c)};b.track=function(d){b.i(d,4,-1)};b.P=function(d,c){var e="a.media.",k=d.linkTrackVars,f=d.linkTrackEvents,a="m_i",l,g=d.contextData,h;c.l&&(e+="ad.",c.v&&(g["http://www.verizon.com/includes/javascript/a.media.name"]=c.v,g[e+"pod"]=c.J,g[e+"podPosition"]=c.K),c.G||(g[e+"CPM"]=c.H));c.r&&(g[e+"clicked"]=
!0,c.r=!1);g["a.contentType"]="video"+(c.l?"Ad":"");g["a.media.channel"]=b.channel;g[e+"name"]=c.name;g[e+"playerName"]=c.playerName;0<c.length&&(g[e+"length"]=c.length);g[e+"timePlayed"]=Math.floor(c.a);0<Math.floor(c.a)&&(g[e+"timePlayed"]=Math.floor(c.a));c.G||(g[e+"view"]=!0,a="m_s",b.Heartbeat&&b.Heartbeat.enabled&&(a=c.l?b.__primetime?"mspa_s":"msa_s":b.__primetime?"msp_s":"ms_s"),c.G=1);c.f&&(g[e+"segmentNum"]=c.m,g[e+"segment"]=c.f,0<c.B&&(g[e+"segmentLength"]=c.B),c.A&&0<c.a&&(g[e+"segmentView"]=
!0));!c.Q&&c.complete&&(g[e+"complete"]=!0,c.S=1);0<c.p&&(g[e+"milestone"]=c.p);0<c.q&&(g[e+"offsetMilestone"]=c.q);if(k)for(h in g)Object.prototype[h]||(k+=",contextData."+h);l=g["a.contentType"];d.pe=a;d.pev3=l;var q,s;if(b.contextDataMapping)for(h in d.events2||(d.events2=""),k&&(k+=",events"),b.contextDataMapping)if(!Object.prototype[h]){a=h.length>e.length&&h.substring(0,e.length)==e?h.substring(e.length):"";l=b.contextDataMapping[h];if("string"==typeof l)for(q=l.split(","),s=0;s<q.length;s++)l=
q[s],"a.contentType"==h?(k&&(k+=","+l),d[l]=g[h]):"view"==a||"segmentView"==a||"clicked"==a||"complete"==a||"timePlayed"==a||"CPM"==a?(f&&(f+=","+l),"timePlayed"==a||"CPM"==a?g[h]&&(d.events2+=(d.events2?",":"")+l+"="+g[h]):g[h]&&(d.events2+=(d.events2?",":"")+l)):"segment"==a&&g[h+"Num"]?(k&&(k+=","+l),d[l]=g[h+"Num"]+":"+g[h]):(k&&(k+=","+l),d[l]=g[h]);else if("milestones"==a||"offsetMilestones"==a)h=h.substring(0,h.length-1),g[h]&&b.contextDataMapping[h+"s"][g[h]]&&(f&&(f+=","+b.contextDataMapping[h+
"s"][g[h]]),d.events2+=(d.events2?",":"")+b.contextDataMapping[h+"s"][g[h]]);g[h]&&(g[h]=0);"segment"==a&&g[h+"Num"]&&(g[h+"Num"]=0)}d.linkTrackVars=k;d.linkTrackEvents=f};b.i=function(d,c,e,k,f){var a={},l=(new Date).getTime()/1E3,g,h,q=b.trackVars,s=b.trackEvents,t=b.trackSeconds,u=b.trackMilestones,v=b.trackOffsetMilestones,w=b.segmentByMilestones,x=b.segmentByOffsetMilestones,p,n,r=1,m={},y;b.channel||(b.channel=b.s.w.location.hostname);if(a=d&&b.list&&b.list[d]?b.list[d]:0)if(a.l&&(t=b.adTrackSeconds,
u=b.adTrackMilestones,v=b.adTrackOffsetMilestones,w=b.adSegmentByMilestones,x=b.adSegmentByOffsetMilestones),0>e&&(e=1==a.k&&0<a.u?l-a.u+a.c:a.c),0<a.length&&(e=e<a.length?e:a.length),0>e&&(e=0),a.offset=e,0<a.length&&(a.e=a.offset/a.length*100,a.e=100<a.e?100:a.e),0>a.c&&(a.c=e),y=a.D,m.name=d,m.ad=a.l,m.length=a.length,m.openTime=new Date,m.openTime.setTime(1E3*a.timestamp),m.offset=a.offset,m.percent=a.e,m.playerName=a.playerName,m.mediaEvent=0>a.g?"OPEN":1==c?"PLAY":2==c?"STOP":3==c?"MONITOR":
4==c?"TRACK":5==c?"COMPLETE":7==c?"CLICK":"CLOSE",2<c||c!=a.k&&(2!=c||1==a.k)){f||(k=a.m,f=a.f);if(c){1==c&&(a.c=e);if((3>=c||5<=c)&&0<=a.g&&(r=!1,q=s="None",a.g!=e)){h=a.g;h>e&&(h=a.c,h>e&&(h=e));p=u?u.split(","):0;if(0<a.length&&p&&e>=h)for(n=0;n<p.length;n++)(g=p[n]?parseFloat(""+p[n]):0)&&h/a.length*100<g&&a.e>=g&&(r=!0,n=p.length,m.mediaEvent="MILESTONE",a.p=m.milestone=g);if((p=v?v.split(","):0)&&e>=h)for(n=0;n<p.length;n++)(g=p[n]?parseFloat(""+p[n]):0)&&h<g&&e>=g&&(r=!0,n=p.length,m.mediaEvent=
"OFFSET_MILESTONE",a.q=m.offsetMilestone=g)}if(a.L||!f){if(w&&u&&0<a.length){if(p=u.split(","))for(p.push("100"),n=h=0;n<p.length;n++)if(g=p[n]?parseFloat(""+p[n]):0)a.e<g&&(k=n+1,f="M:"+h+"-"+g,n=p.length),h=g}else if(x&&v&&(p=v.split(",")))for(p.push(""+(0<a.length?a.length:"E")),n=h=0;n<p.length;n++)if((g=p[n]?parseFloat(""+p[n]):0)||"E"==p[n]){if(e<g||"E"==p[n])k=n+1,f="O:"+h+"-"+g,n=p.length;h=g}f&&(a.L=!0)}(f||a.f)&&f!=a.f&&(a.F=!0,a.f||(a.m=k,a.f=f),0<=a.g&&(r=!0));(2<=c||100<=a.e)&&a.c<e&&
(a.C+=e-a.c,a.a+=e-a.c);if(2>=c||3==c&&!a.k)a.n+=(1==c||3==c?"S":"E")+Math.floor(e),a.k=3==c?1:c;!r&&0<=a.g&&3>=c&&(t=t?t:0)&&a.a>=t&&(r=!0,m.mediaEvent="SECONDS");a.u=l;a.c=e}if(!c||3>=c&&100<=a.e)2!=a.k&&(a.n+="E"+Math.floor(e)),c=0,q=s="None",m.mediaEvent="CLOSE";7==c&&(r=m.clicked=a.r=!0);if(5==c||b.completeByCloseOffset&&(!c||100<=a.e)&&0<a.length&&e>=a.length-b.completeCloseOffsetThreshold)r=m.complete=a.complete=!0;l=m.mediaEvent;"MILESTONE"==l?l+="_"+m.milestone:"OFFSET_MILESTONE"==l&&(l+=
"_"+m.offsetMilestone);a.I[l]?m.eventFirstTime=!1:(m.eventFirstTime=!0,a.I[l]=1);m.event=m.mediaEvent;m.timePlayed=a.C;m.segmentNum=a.m;m.segment=a.f;m.segmentLength=a.B;b.monitor&&4!=c&&b.monitor(b.s,m);b.Heartbeat&&b.Heartbeat.enabled&&0<=a.g&&(r=!1);0==c&&b.M(d);r&&a.D==y&&(d={contextData:{}},d.linkTrackVars=q,d.linkTrackEvents=s,d.linkTrackVars||(d.linkTrackVars=""),d.linkTrackEvents||(d.linkTrackEvents=""),b.P(d,a),d.linkTrackVars||(d["!linkTrackVars"]=1),d.linkTrackEvents||(d["!linkTrackEvents"]=
1),b.s.track(d),a.F?(a.m=k,a.f=f,a.A=!0,a.F=!1):0<a.a&&(a.A=!1),a.n="",a.p=a.q=0,a.a-=Math.floor(a.a),a.g=e,a.D++)}return a};b.O=function(d,c,e,k,f){var a=0;if(d&&(!b.autoTrackMediaLengthRequired||c&&0<c)){if(b.list&&b.list[d])a=1;else if(1==e||3==e)b.open(d,c,"HTML5 Video",f),a=1;a&&b.i(d,e,k,-1,0)}};b.attach=function(d){var c,e,k;d&&d.tagName&&"VIDEO"==d.tagName.toUpperCase()&&(b.o||(b.o=function(c,a,d){var e,h;b.autoTrack&&(e=c.currentSrc,(h=c.duration)||(h=-1),0>d&&(d=c.currentTime),b.O(e,h,a,
d,c))}),c=function(){b.o(d,1,-1)},e=function(){b.o(d,1,-1)},b.j(d,"play",c),b.j(d,"pause",e),b.j(d,"seeking",e),b.j(d,"seeked",c),b.j(d,"ended",function(){b.o(d,0,-1)}),b.j(d,"timeupdate",c),k=function(){d.paused||d.ended||d.seeking||b.o(d,3,-1);setTimeout(k,1E3)},k())};b.j=function(b,c,e){b.attachEvent?b.attachEvent("on"+c,e):b.addEventListener&&b.addEventListener(c,e,!1)};void 0==b.completeByCloseOffset&&(b.completeByCloseOffset=1);void 0==b.completeCloseOffsetThreshold&&(b.completeCloseOffsetThreshold=
1);b.Heartbeat={};b.N=function(){var d,c;if(b.autoTrack&&(d=b.s.d.getElementsByTagName("VIDEO")))for(c=0;c<d.length;c++)b.attach(d[c])};b.j(q,"load",b.N)}

/*
 ============== DO NOT ALTER ANYTHING BELOW THIS LINE ! ===============

AppMeasurement for JavaScript version: 1.4.3
Copyright 1996-2013 Adobe, Inc. All Rights Reserved
More info available at http://www.omniture.com
*/
function AppMeasurement(){var a=this;a.version="1.4.3";var k=window;k.s_c_in||(k.s_c_il=[],k.s_c_in=0);a._il=k.s_c_il;a._in=k.s_c_in;a._il[a._in]=a;k.s_c_in++;a._c="s_c";var q=k.yb;q||(q=null);var r=k,n,t;try{for(n=r.parent,t=r.location;n&&n.location&&t&&""+n.location!=""+t&&r.location&&""+n.location!=""+r.location&&n.location.host==t.host;)r=n,n=r.parent}catch(u){}a.nb=function(a){try{console.log(a)}catch(b){}};a.za=function(a){return""+parseInt(a)==""+a};a.replace=function(a,b,d){return!a||0>a.indexOf(b)?
a:a.split(b).join(d)};a.escape=function(c){var b,d;if(!c)return c;c=encodeURIComponent(c);for(b=0;7>b;b++)d="+~!*()'".substring(b,b+1),0<=c.indexOf(d)&&(c=a.replace(c,d,"%"+d.charCodeAt(0).toString(16).toUpperCase()));return c};a.unescape=function(c){if(!c)return c;c=0<=c.indexOf("+")?a.replace(c,"+"," "):c;try{return decodeURIComponent(c)}catch(b){}return unescape(c)};a.eb=function(){var c=k.location.hostname,b=a.fpCookieDomainPeriods,d;b||(b=a.cookieDomainPeriods);if(c&&!a.cookieDomain&&!/^[0-9.]+$/.test(c)&&
(b=b?parseInt(b):2,b=2<b?b:2,d=c.lastIndexOf("."),0<=d)){for(;0<=d&&1<b;)d=c.lastIndexOf(".",d-1),b--;a.cookieDomain=0<d?c.substring(d):c}return a.cookieDomain};a.c_r=a.cookieRead=function(c){c=a.escape(c);var b=" "+a.d.cookie,d=b.indexOf(" "+c+"="),f=0>d?d:b.indexOf(";",d);c=0>d?"":a.unescape(b.substring(d+2+c.length,0>f?b.length:f));return"[[B]]"!=c?c:""};a.c_w=a.cookieWrite=function(c,b,d){var f=a.eb(),e=a.cookieLifetime,g;b=""+b;e=e?(""+e).toUpperCase():"";d&&"SESSION"!=e&&"NONE"!=e&&((g=""!=
b?parseInt(e?e:0):-60)?(d=new Date,d.setTime(d.getTime()+1E3*g)):1==d&&(d=new Date,g=d.getYear(),d.setYear(g+5+(1900>g?1900:0))));return c&&"NONE"!=e?(a.d.cookie=c+"="+a.escape(""!=b?b:"[[B]]")+"; path=/;"+(d&&"SESSION"!=e?" expires="+d.toGMTString()+";":"")+(f?" domain="+f+";":""),a.cookieRead(c)==b):0};a.F=[];a.ba=function(c,b,d){if(a.ta)return 0;a.maxDelay||(a.maxDelay=250);var f=0,e=(new Date).getTime()+a.maxDelay,g=a.d.visibilityState,m=["webkitvisibilitychange","visibilitychange"];g||(g=a.d.webkitVisibilityState);
if(g&&"prerender"==g){if(!a.ca)for(a.ca=1,d=0;d<m.length;d++)a.d.addEventListener(m[d],function(){var b=a.d.visibilityState;b||(b=a.d.webkitVisibilityState);"visible"==b&&(a.ca=0,a.delayReady())});f=1;e=0}else d||a.l("_d")&&(f=1);f&&(a.F.push({m:c,a:b,t:e}),a.ca||setTimeout(a.delayReady,a.maxDelay));return f};a.delayReady=function(){var c=(new Date).getTime(),b=0,d;for(a.l("_d")?b=1:a.na();0<a.F.length;){d=a.F.shift();if(b&&!d.t&&d.t>c){a.F.unshift(d);setTimeout(a.delayReady,parseInt(a.maxDelay/2));
break}a.ta=1;a[d.m].apply(a,d.a);a.ta=0}};a.setAccount=a.sa=function(c){var b,d;if(!a.ba("setAccount",arguments))if(a.account=c,a.allAccounts)for(b=a.allAccounts.concat(c.split(",")),a.allAccounts=[],b.sort(),d=0;d<b.length;d++)0!=d&&b[d-1]==b[d]||a.allAccounts.push(b[d]);else a.allAccounts=c.split(",")};a.foreachVar=function(c,b){var d,f,e,g,m="";e=f="";if(a.lightProfileID)d=a.J,(m=a.lightTrackVars)&&(m=","+m+","+a.ga.join(",")+",");else{d=a.c;if(a.pe||a.linkType)m=a.linkTrackVars,f=a.linkTrackEvents,
a.pe&&(e=a.pe.substring(0,1).toUpperCase()+a.pe.substring(1),a[e]&&(m=a[e].xb,f=a[e].wb));m&&(m=","+m+","+a.A.join(",")+",");f&&m&&(m+=",events,")}b&&(b=","+b+",");for(f=0;f<d.length;f++)e=d[f],(g=a[e])&&(!m||0<=m.indexOf(","+e+","))&&(!b||0<=b.indexOf(","+e+","))&&c(e,g)};a.L=function(c,b,d,f,e){var g="",m,p,k,w,n=0;"contextData"==c&&(c="c");if(b){for(m in b)if(!(Object.prototype[m]||e&&m.substring(0,e.length)!=e)&&b[m]&&(!d||0<=d.indexOf(","+(f?f+".":"")+m+","))){k=!1;if(n)for(p=0;p<n.length;p++)m.substring(0,
n[p].length)==n[p]&&(k=!0);if(!k&&(""==g&&(g+="&"+c+"."),p=b[m],e&&(m=m.substring(e.length)),0<m.length))if(k=m.indexOf("."),0<k)p=m.substring(0,k),k=(e?e:"")+p+".",n||(n=[]),n.push(k),g+=a.L(p,b,d,f,k);else if("boolean"==typeof p&&(p=p?"true":"false"),p){if("retrieveLightData"==f&&0>e.indexOf(".contextData."))switch(k=m.substring(0,4),w=m.substring(4),m){case "transactionID":m="xact";break;case "channel":m="ch";break;case "campaign":m="v0";break;default:a.za(w)&&("prop"==k?m="c"+w:"eVar"==k?m="v"+
w:"list"==k?m="l"+w:"hier"==k&&(m="h"+w,p=p.substring(0,255)))}g+="&"+a.escape(m)+"="+a.escape(p)}}""!=g&&(g+="&."+c)}return g};a.gb=function(){var c="",b,d,f,e,g,m,p,k,n="",q="",r=d="";if(a.lightProfileID)b=a.J,(n=a.lightTrackVars)&&(n=","+n+","+a.ga.join(",")+",");else{b=a.c;if(a.pe||a.linkType)n=a.linkTrackVars,q=a.linkTrackEvents,a.pe&&(d=a.pe.substring(0,1).toUpperCase()+a.pe.substring(1),a[d]&&(n=a[d].xb,q=a[d].wb));n&&(n=","+n+","+a.A.join(",")+",");q&&(q=","+q+",",n&&(n+=",events,"));a.events2&&
(r+=(""!=r?",":"")+a.events2)}a.AudienceManagement&&a.AudienceManagement.isReady()&&(c+=a.L("d",a.AudienceManagement.getEventCallConfigParams()));for(d=0;d<b.length;d++){e=b[d];g=a[e];f=e.substring(0,4);m=e.substring(4);!g&&"events"==e&&r&&(g=r,r="");if(g&&(!n||0<=n.indexOf(","+e+","))){switch(e){case "supplementalDataID":e="sdid";break;case "timestamp":e="ts";break;case "dynamicVariablePrefix":e="D";break;case "visitorID":e="vid";break;case "marketingCloudVisitorID":e="mid";break;case "analyticsVisitorID":e=
"aid";break;case "audienceManagerLocationHint":e="aamlh";break;case "audienceManagerBlob":e="aamb";break;case "authState":e="as";break;case "pageURL":e="g";255<g.length&&(a.pageURLRest=g.substring(255),g=g.substring(0,255));break;case "pageURLRest":e="-g";break;case "referrer":e="r";break;case "vmk":case "visitorMigrationKey":e="vmt";break;case "visitorMigrationServer":e="vmf";a.ssl&&a.visitorMigrationServerSecure&&(g="");break;case "visitorMigrationServerSecure":e="vmf";!a.ssl&&a.visitorMigrationServer&&
(g="");break;case "charSet":e="ce";break;case "visitorNamespace":e="ns";break;case "cookieDomainPeriods":e="cdp";break;case "cookieLifetime":e="cl";break;case "variableProvider":e="vvp";break;case "currencyCode":e="cc";break;case "channel":e="ch";break;case "transactionID":e="xact";break;case "campaign":e="v0";break;case "latitude":e="lat";break;case "longitude":e="lon";break;case "resolution":e="s";break;case "colorDepth":e="c";break;case "javascriptVersion":e="j";break;case "javaEnabled":e="v";
break;case "cookiesEnabled":e="k";break;case "browserWidth":e="bw";break;case "browserHeight":e="bh";break;case "connectionType":e="ct";break;case "homepage":e="hp";break;case "events":r&&(g+=(""!=g?",":"")+r);if(q)for(m=g.split(","),g="",f=0;f<m.length;f++)p=m[f],k=p.indexOf("="),0<=k&&(p=p.substring(0,k)),k=p.indexOf(":"),0<=k&&(p=p.substring(0,k)),0<=q.indexOf(","+p+",")&&(g+=(g?",":"")+m[f]);break;case "events2":g="";break;case "contextData":c+=a.L("c",a[e],n,e);g="";break;case "lightProfileID":e=
"mtp";break;case "lightStoreForSeconds":e="mtss";a.lightProfileID||(g="");break;case "lightIncrementBy":e="mti";a.lightProfileID||(g="");break;case "retrieveLightProfiles":e="mtsr";break;case "deleteLightProfiles":e="mtsd";break;case "retrieveLightData":a.retrieveLightProfiles&&(c+=a.L("mts",a[e],n,e));g="";break;default:a.za(m)&&("prop"==f?e="c"+m:"eVar"==f?e="v"+m:"list"==f?e="l"+m:"hier"==f&&(e="h"+m,g=g.substring(0,255)))}g&&(c+="&"+e+"="+("pev"!=e.substring(0,3)?a.escape(g):g))}"pev3"==e&&a.e&&
(c+=a.e)}return c};a.u=function(a){var b=a.tagName;if("undefined"!=""+a.Bb||"undefined"!=""+a.rb&&"HTML"!=(""+a.rb).toUpperCase())return"";b=b&&b.toUpperCase?b.toUpperCase():"";"SHAPE"==b&&(b="");b&&(("INPUT"==b||"BUTTON"==b)&&a.type&&a.type.toUpperCase?b=a.type.toUpperCase():!b&&a.href&&(b="A"));return b};a.va=function(a){var b=a.href?a.href:"",d,f,e;d=b.indexOf(":");f=b.indexOf("?");e=b.indexOf("/");b&&(0>d||0<=f&&d>f||0<=e&&d>e)&&(f=a.protocol&&1<a.protocol.length?a.protocol:l.protocol?l.protocol:
"",d=l.pathname.lastIndexOf("/"),b=(f?f+"//":"")+(a.host?a.host:l.host?l.host:"")+("/"!=h.substring(0,1)?l.pathname.substring(0,0>d?0:d)+"/":"")+b);return b};a.G=function(c){var b=a.u(c),d,f,e="",g=0;return b&&(d=c.protocol,f=c.onclick,!c.href||"A"!=b&&"AREA"!=b||f&&d&&!(0>d.toLowerCase().indexOf("javascript"))?f?(e=a.replace(a.replace(a.replace(a.replace(""+f,"\r",""),"\n",""),"\t","")," ",""),g=2):"INPUT"==b||"SUBMIT"==b?(c.value?e=c.value:c.innerText?e=c.innerText:c.textContent&&(e=c.textContent),
g=3):c.src&&"IMAGE"==b&&(e=c.src):e=a.va(c),e)?{id:e.substring(0,100),type:g}:0};a.zb=function(c){for(var b=a.u(c),d=a.G(c);c&&!d&&"BODY"!=b;)if(c=c.parentElement?c.parentElement:c.parentNode)b=a.u(c),d=a.G(c);d&&"BODY"!=b||(c=0);c&&(b=c.onclick?""+c.onclick:"",0<=b.indexOf(".tl(")||0<=b.indexOf(".trackLink("))&&(c=0);return c};a.qb=function(){var c,b,d=a.linkObject,f=a.linkType,e=a.linkURL,g,m;a.ha=1;d||(a.ha=0,d=a.clickObject);if(d){c=a.u(d);for(b=a.G(d);d&&!b&&"BODY"!=c;)if(d=d.parentElement?d.parentElement:
d.parentNode)c=a.u(d),b=a.G(d);b&&"BODY"!=c||(d=0);if(d){var p=d.onclick?""+d.onclick:"";if(0<=p.indexOf(".tl(")||0<=p.indexOf(".trackLink("))d=0}}else a.ha=1;!e&&d&&(e=a.va(d));e&&!a.linkLeaveQueryString&&(g=e.indexOf("?"),0<=g&&(e=e.substring(0,g)));if(!f&&e){var n=0,q=0,r;if(a.trackDownloadLinks&&a.linkDownloadFileTypes)for(p=e.toLowerCase(),g=p.indexOf("?"),m=p.indexOf("#"),0<=g?0<=m&&m<g&&(g=m):g=m,0<=g&&(p=p.substring(0,g)),g=a.linkDownloadFileTypes.toLowerCase().split(","),m=0;m<g.length;m++)(r=
g[m])&&p.substring(p.length-(r.length+1))=="."+r&&(f="d");if(a.trackExternalLinks&&!f&&(p=e.toLowerCase(),a.ya(p)&&(a.linkInternalFilters||(a.linkInternalFilters=k.location.hostname),g=0,a.linkExternalFilters?(g=a.linkExternalFilters.toLowerCase().split(","),n=1):a.linkInternalFilters&&(g=a.linkInternalFilters.toLowerCase().split(",")),g))){for(m=0;m<g.length;m++)r=g[m],0<=p.indexOf(r)&&(q=1);q?n&&(f="e"):n||(f="e")}}a.linkObject=d;a.linkURL=e;a.linkType=f;if(a.trackClickMap||a.trackInlineStats)a.e=
"",d&&(f=a.pageName,e=1,d=d.sourceIndex,f||(f=a.pageURL,e=0),k.s_objectID&&(b.id=k.s_objectID,d=b.type=1),f&&b&&b.id&&c&&(a.e="&pid="+a.escape(f.substring(0,255))+(e?"&pidt="+e:"")+"&oid="+a.escape(b.id.substring(0,100))+(b.type?"&oidt="+b.type:"")+"&ot="+c+(d?"&oi="+d:"")))};a.hb=function(){var c=a.ha,b=a.linkType,d=a.linkURL,f=a.linkName;b&&(d||f)&&(b=b.toLowerCase(),"d"!=b&&"e"!=b&&(b="o"),a.pe="lnk_"+b,a.pev1=d?a.escape(d):"",a.pev2=f?a.escape(f):"",c=1);a.abort&&(c=0);if(a.trackClickMap||a.trackInlineStats){var b=
{},d=0,e=a.cookieRead("s_sq"),g=e?e.split("&"):0,m,p,k,e=0;if(g)for(m=0;m<g.length;m++)p=g[m].split("="),f=a.unescape(p[0]).split(","),p=a.unescape(p[1]),b[p]=f;f=a.account.split(",");if(c||a.e){c&&!a.e&&(e=1);for(p in b)if(!Object.prototype[p])for(m=0;m<f.length;m++)for(e&&(k=b[p].join(","),k==a.account&&(a.e+=("&"!=p.charAt(0)?"&":"")+p,b[p]=[],d=1)),g=0;g<b[p].length;g++)k=b[p][g],k==f[m]&&(e&&(a.e+="&u="+a.escape(k)+("&"!=p.charAt(0)?"&":"")+p+"&u=0"),b[p].splice(g,1),d=1);c||(d=1);if(d){e="";
m=2;!c&&a.e&&(e=a.escape(f.join(","))+"="+a.escape(a.e),m=1);for(p in b)!Object.prototype[p]&&0<m&&0<b[p].length&&(e+=(e?"&":"")+a.escape(b[p].join(","))+"="+a.escape(p),m--);a.cookieWrite("s_sq",e)}}}return c};a.ib=function(){if(!a.vb){var c=new Date,b=r.location,d,f,e=f=d="",g="",m="",k="1.2",n=a.cookieWrite("s_cc","true",0)?"Y":"N",q="",s="";if(c.setUTCDate&&(k="1.3",(0).toPrecision&&(k="1.5",c=[],c.forEach))){k="1.6";f=0;d={};try{f=new Iterator(d),f.next&&(k="1.7",c.reduce&&(k="1.8",k.trim&&(k=
"1.8.1",Date.parse&&(k="1.8.2",Object.create&&(k="1.8.5")))))}catch(t){}}d=screen.width+"x"+screen.height;e=navigator.javaEnabled()?"Y":"N";f=screen.pixelDepth?screen.pixelDepth:screen.colorDepth;g=a.w.innerWidth?a.w.innerWidth:a.d.documentElement.offsetWidth;m=a.w.innerHeight?a.w.innerHeight:a.d.documentElement.offsetHeight;try{a.b.addBehavior("#default#homePage"),q=a.b.Ab(b)?"Y":"N"}catch(u){}try{a.b.addBehavior("#default#clientCaps"),s=a.b.connectionType}catch(x){}a.resolution=d;a.colorDepth=f;
a.javascriptVersion=k;a.javaEnabled=e;a.cookiesEnabled=n;a.browserWidth=g;a.browserHeight=m;a.connectionType=s;a.homepage=q;a.vb=1}};a.K={};a.loadModule=function(c,b){var d=a.K[c];if(!d){d=k["AppMeasurement_Module_"+c]?new k["AppMeasurement_Module_"+c](a):{};a.K[c]=a[c]=d;d.Na=function(){return d.Ra};d.Sa=function(b){if(d.Ra=b)a[c+"_onLoad"]=b,a.ba(c+"_onLoad",[a,d],1)||b(a,d)};try{Object.defineProperty?Object.defineProperty(d,"onLoad",{get:d.Na,set:d.Sa}):d._olc=1}catch(f){d._olc=1}}b&&(a[c+"_onLoad"]=
b,a.ba(c+"_onLoad",[a,d],1)||b(a,d))};a.l=function(c){var b,d;for(b in a.K)if(!Object.prototype[b]&&(d=a.K[b])&&(d._olc&&d.onLoad&&(d._olc=0,d.onLoad(a,d)),d[c]&&d[c]()))return 1;return 0};a.lb=function(){var c=Math.floor(1E13*Math.random()),b=a.visitorSampling,d=a.visitorSamplingGroup,d="s_vsn_"+(a.visitorNamespace?a.visitorNamespace:a.account)+(d?"_"+d:""),f=a.cookieRead(d);if(b){f&&(f=parseInt(f));if(!f){if(!a.cookieWrite(d,c))return 0;f=c}if(f%1E4>v)return 0}return 1};a.M=function(c,b){var d,
f,e,g,m,k;for(d=0;2>d;d++)for(f=0<d?a.oa:a.c,e=0;e<f.length;e++)if(g=f[e],(m=c[g])||c["!"+g]){if(!b&&("contextData"==g||"retrieveLightData"==g)&&a[g])for(k in a[g])m[k]||(m[k]=a[g][k]);a[g]=m}};a.Ga=function(c,b){var d,f,e,g;for(d=0;2>d;d++)for(f=0<d?a.oa:a.c,e=0;e<f.length;e++)g=f[e],c[g]=a[g],b||c[g]||(c["!"+g]=1)};a.cb=function(a){var b,d,f,e,g,m=0,k,n="",q="";if(a&&255<a.length&&(b=""+a,d=b.indexOf("?"),0<d&&(k=b.substring(d+1),b=b.substring(0,d),e=b.toLowerCase(),f=0,"http://"==e.substring(0,
7)?f+=7:"https://"==e.substring(0,8)&&(f+=8),d=e.indexOf("/",f),0<d&&(e=e.substring(f,d),g=b.substring(d),b=b.substring(0,d),0<=e.indexOf("google")?m=",q,ie,start,search_key,word,kw,cd,":0<=e.indexOf("http://www.verizon.com/includes/javascript/yahoo.co")&&(m=",p,ei,"),m&&k)))){if((a=k.split("&"))&&1<a.length){for(f=0;f<a.length;f++)e=a[f],d=e.indexOf("="),0<d&&0<=m.indexOf(","+e.substring(0,d)+",")?n+=(n?"&":"")+e:q+=(q?"&":"")+e;n&&q?k=n+"&"+q:q=""}d=253-(k.length-q.length)-b.length;a=b+(0<d?g.substring(0,d):"")+"?"+k}return a};a.Ma=function(c){var b=
a.d.visibilityState,d=["webkitvisibilitychange","visibilitychange"];b||(b=a.d.webkitVisibilityState);if(b&&"prerender"==b){if(c)for(b=0;b<d.length;b++)a.d.addEventListener(d[b],function(){var b=a.d.visibilityState;b||(b=a.d.webkitVisibilityState);"visible"==b&&c()});return!1}return!0};a.Y=!1;a.C=!1;a.Ta=function(){a.C=!0;a.i()};a.W=!1;a.Q=!1;a.Qa=function(c){a.marketingCloudVisitorID=c;a.Q=!0;a.i()};a.T=!1;a.N=!1;a.Ia=function(c){a.analyticsVisitorID=c;a.N=!0;a.i()};a.V=!1;a.P=!1;a.Ka=function(c){a.audienceManagerLocationHint=
c;a.P=!0;a.i()};a.U=!1;a.O=!1;a.Ja=function(c){a.audienceManagerBlob=c;a.O=!0;a.i()};a.La=function(c){a.maxDelay||(a.maxDelay=250);return a.l("_d")?(c&&setTimeout(function(){c()},a.maxDelay),!1):!0};a.X=!1;a.B=!1;a.na=function(){a.B=!0;a.i()};a.isReadyToTrack=function(){var c=!0,b=a.visitor;a.Y||a.C||(a.Ma(a.Ta)?a.C=!0:a.Y=!0);if(a.Y&&!a.C)return!1;b&&b.isAllowed()&&(a.W||a.marketingCloudVisitorID||!b.getMarketingCloudVisitorID||(a.W=!0,a.marketingCloudVisitorID=b.getMarketingCloudVisitorID([a,a.Qa]),
a.marketingCloudVisitorID&&(a.Q=!0)),a.T||a.analyticsVisitorID||!b.getAnalyticsVisitorID||(a.T=!0,a.analyticsVisitorID=b.getAnalyticsVisitorID([a,a.Ia]),a.analyticsVisitorID&&(a.N=!0)),a.V||a.audienceManagerLocationHint||!b.getAudienceManagerLocationHint||(a.V=!0,a.audienceManagerLocationHint=b.getAudienceManagerLocationHint([a,a.Ka]),a.audienceManagerLocationHint&&(a.P=!0)),a.U||a.audienceManagerBlob||!b.getAudienceManagerBlob||(a.U=!0,a.audienceManagerBlob=b.getAudienceManagerBlob([a,a.Ja]),a.audienceManagerBlob&&
(a.O=!0)),a.W&&!a.Q&&!a.marketingCloudVisitorID||a.T&&!a.N&&!a.analyticsVisitorID||a.V&&!a.P&&!a.audienceManagerLocationHint||a.U&&!a.O&&!a.audienceManagerBlob)&&(c=!1);a.X||a.B||(a.La(a.na)?a.B=!0:a.X=!0);a.X&&!a.B&&(c=!1);return c};a.k=q;a.o=0;a.callbackWhenReadyToTrack=function(c,b,d){var f;f={};f.Xa=c;f.Wa=b;f.Ua=d;a.k==q&&(a.k=[]);a.k.push(f);0==a.o&&(a.o=setInterval(a.i,100))};a.i=function(){var c;if(a.isReadyToTrack()&&(a.o&&(clearInterval(a.o),a.o=0),a.k!=q))for(;0<a.k.length;)c=a.k.shift(),
c.Wa.apply(c.Xa,c.Ua)};a.Oa=function(c){var b,d,f=q,e=q;if(!a.isReadyToTrack()){b=[];if(c!=q)for(d in f={},c)f[d]=c[d];e={};a.Ga(e,!0);b.push(f);b.push(e);a.callbackWhenReadyToTrack(a,a.track,b);return!0}return!1};a.fb=function(){var c=a.cookieRead("s_fid"),b="",d="",f;f=8;var e=4;if(!c||0>c.indexOf("-")){for(c=0;16>c;c++)f=Math.floor(Math.random()*f),b+="0123456789ABCDEF".substring(f,f+1),f=Math.floor(Math.random()*e),d+="0123456789ABCDEF".substring(f,f+1),f=e=16;c=b+"-"+d}a.cookieWrite("s_fid",
c,1)||(c=0);return c};a.t=a.track=function(c,b){var d,f=new Date,e="s"+Math.floor(f.getTime()/108E5)%10+Math.floor(1E13*Math.random()),g=f.getYear(),g="t="+a.escape(f.getDate()+"/"+f.getMonth()+"/"+(1900>g?g+1900:g)+" "+f.getHours()+":"+f.getMinutes()+":"+f.getSeconds()+" "+f.getDay()+" "+f.getTimezoneOffset());a.visitor&&(a.visitor.getAuthState&&(a.authState=a.visitor.getAuthState()),!a.supplementalDataID&&a.visitor.getSupplementalDataID&&(a.supplementalDataID=a.visitor.getSupplementalDataID("AppMeasurement:"+
a._in,a.expectSupplementalData?!1:!0)));a.l("_s");a.Oa(c)||(b&&a.M(b),c&&(d={},a.Ga(d,0),a.M(c)),a.lb()&&(a.analyticsVisitorID||a.marketingCloudVisitorID||(a.fid=a.fb()),a.qb(),a.usePlugins&&a.doPlugins&&a.doPlugins(a),a.account&&(a.abort||(a.trackOffline&&!a.timestamp&&(a.timestamp=Math.floor(f.getTime()/1E3)),f=k.location,a.pageURL||(a.pageURL=f.href?f.href:f),a.referrer||a.Ha||(a.referrer=r.document.referrer),a.Ha=1,a.referrer=a.cb(a.referrer),a.l("_g")),a.hb()&&!a.abort&&(a.ib(),g+=a.gb(),a.pb(e,
g),a.l("_t"),a.referrer=""))),c&&a.M(d,1));a.abort=a.supplementalDataID=a.timestamp=a.pageURLRest=a.linkObject=a.clickObject=a.linkURL=a.linkName=a.linkType=k.s_objectID=a.pe=a.pev1=a.pev2=a.pev3=a.e=0};a.tl=a.trackLink=function(c,b,d,f,e){a.linkObject=c;a.linkType=b;a.linkName=d;e&&(a.j=c,a.q=e);return a.track(f)};a.trackLight=function(c,b,d,f){a.lightProfileID=c;a.lightStoreForSeconds=b;a.lightIncrementBy=d;return a.track(f)};a.clearVars=function(){var c,b;for(c=0;c<a.c.length;c++)if(b=a.c[c],"prop"==
b.substring(0,4)||"eVar"==b.substring(0,4)||"hier"==b.substring(0,4)||"list"==b.substring(0,4)||"channel"==b||"events"==b||"eventList"==b||"products"==b||"productList"==b||"purchaseID"==b||"transactionID"==b||"state"==b||"zip"==b||"campaign"==b)a[b]=void 0};a.tagContainerMarker="";a.pb=function(c,b){var d,f=a.trackingServer;d="";var e=a.dc,g="sc.",k=a.visitorNamespace;f?a.trackingServerSecure&&a.ssl&&(f=a.trackingServerSecure):(k||(k=a.account,f=k.indexOf(","),0<=f&&(k=k.substring(0,f)),k=k.replace(/[^A-Za-z0-9]/g,
"")),d||(d="http://www.verizon.com/includes/javascript/2o7.net"),e=e?(""+e).toLowerCase():"d1","http://www.verizon.com/includes/javascript/2o7.net"==d&&("d1"==e?e="112":"d2"==e&&(e="122"),g=""),f=k+"."+e+"."+g+d);d=a.ssl?"https://":"http://";e=a.AudienceManagement&&a.AudienceManagement.isReady();d+=f+"/b/ss/"+a.account+"/"+(a.mobile?"5.":"")+(e?"10":"1")+"/JS-"+a.version+(a.ub?"T":"")+(a.tagContainerMarker?"-"+a.tagContainerMarker:"")+"/"+c+"?AQB=1&ndh=1&pf=1&"+(e?"callback=s_c_il["+a._in+"].AudienceManagement.passData&":"")+b+"&AQE=1";a.ab(d);a.da()};a.ab=function(c){a.g||a.jb();
a.g.push(c);a.fa=a.r();a.Fa()};a.jb=function(){a.g=a.mb();a.g||(a.g=[])};a.mb=function(){var c,b;if(a.ka()){try{(b=k.localStorage.getItem(a.ia()))&&(c=k.JSON.parse(b))}catch(d){}return c}};a.ka=function(){var c=!0;a.trackOffline&&a.offlineFilename&&k.localStorage&&k.JSON||(c=!1);return c};a.wa=function(){var c=0;a.g&&(c=a.g.length);a.v&&c++;return c};a.da=function(){if(!a.v)if(a.xa=q,a.ja)a.fa>a.I&&a.Da(a.g),a.ma(500);else{var c=a.Va();if(0<c)a.ma(c);else if(c=a.ua())a.v=1,a.ob(c),a.sb(c)}};a.ma=
function(c){a.xa||(c||(c=0),a.xa=setTimeout(a.da,c))};a.Va=function(){var c;if(!a.trackOffline||0>=a.offlineThrottleDelay)return 0;c=a.r()-a.Ca;return a.offlineThrottleDelay<c?0:a.offlineThrottleDelay-c};a.ua=function(){if(0<a.g.length)return a.g.shift()};a.ob=function(c){if(a.debugTracking){var b="AppMeasurement Debug: "+c;c=c.split("&");var d;for(d=0;d<c.length;d++)b+="\n\t"+a.unescape(c[d]);a.nb(b)}};a.Pa=function(){return a.marketingCloudVisitorID||a.analyticsVisitorID};a.S=!1;var s;try{s=JSON.parse('{"x":"y"}')}catch(x){s=
null}s&&"y"==s.x?(a.S=!0,a.R=function(a){return JSON.parse(a)}):k.$&&k.$.parseJSON?(a.R=function(a){return k.$.parseJSON(a)},a.S=!0):a.R=function(){return null};a.sb=function(c){var b,d,f;a.Pa()&&2047<c.length&&("undefined"!=typeof XMLHttpRequest&&(b=new XMLHttpRequest,"withCredentials"in b?d=1:b=0),b||"undefined"==typeof XDomainRequest||(b=new XDomainRequest,d=2),b&&a.AudienceManagement&&a.AudienceManagement.isReady()&&(a.S?b.pa=!0:b=0));!b&&a.kb&&(c=c.substring(0,2047));!b&&a.d.createElement&&a.AudienceManagement&&
a.AudienceManagement.isReady()&&(b=a.d.createElement("SCRIPT"))&&"async"in b&&((f=(f=a.d.getElementsByTagName("HEAD"))&&f[0]?f[0]:a.d.body)?(b.type="text/javascript",b.setAttribute("async","async"),d=3):b=0);b||(b=new Image,b.alt="");b.ra=function(){try{a.la&&(clearTimeout(a.la),a.la=0),b.timeout&&(clearTimeout(b.timeout),b.timeout=0)}catch(c){}};b.onload=b.tb=function(){b.ra();a.$a();a.Z();a.v=0;a.da();if(b.pa){b.pa=!1;try{var c=a.R(b.responseText);AudienceManagement.passData(c)}catch(d){}}};b.onabort=
b.onerror=b.bb=function(){b.ra();(a.trackOffline||a.ja)&&a.v&&a.g.unshift(a.Za);a.v=0;a.fa>a.I&&a.Da(a.g);a.Z();a.ma(500)};b.onreadystatechange=function(){4==b.readyState&&(200==b.status?b.tb():b.bb())};a.Ca=a.r();if(1==d||2==d){var e=c.indexOf("?");f=c.substring(0,e);e=c.substring(e+1);e=e.replace(/&callback=[a-zA-Z0-9_.\[\]]+/,"");1==d?(b.open("POST",f,!0),b.send(e)):2==d&&(b.open("POST",f),b.send(e))}else if(b.src=c,3==d){if(a.Aa)try{f.removeChild(a.Aa)}catch(g){}f.firstChild?f.insertBefore(b,
f.firstChild):f.appendChild(b);a.Aa=a.Ya}b.abort&&(a.la=setTimeout(b.abort,5E3));a.Za=c;a.Ya=k["s_i_"+a.replace(a.account,",","_")]=b;if(a.useForcedLinkTracking&&a.D||a.q)a.forcedLinkTrackingTimeout||(a.forcedLinkTrackingTimeout=250),a.aa=setTimeout(a.Z,a.forcedLinkTrackingTimeout)};a.$a=function(){if(a.ka()&&!(a.Ba>a.I))try{k.localStorage.removeItem(a.ia()),a.Ba=a.r()}catch(c){}};a.Da=function(c){if(a.ka()){a.Fa();try{k.localStorage.setItem(a.ia(),k.JSON.stringify(c)),a.I=a.r()}catch(b){}}};a.Fa=
function(){if(a.trackOffline){if(!a.offlineLimit||0>=a.offlineLimit)a.offlineLimit=10;for(;a.g.length>a.offlineLimit;)a.ua()}};a.forceOffline=function(){a.ja=!0};a.forceOnline=function(){a.ja=!1};a.ia=function(){return a.offlineFilename+"-"+a.visitorNamespace+a.account};a.r=function(){return(new Date).getTime()};a.ya=function(a){a=a.toLowerCase();return 0!=a.indexOf("#")&&0!=a.indexOf("about:")&&0!=a.indexOf("opera:")&&0!=a.indexOf("javascript:")?!0:!1};a.setTagContainer=function(c){var b,d,f;a.ub=
c;for(b=0;b<a._il.length;b++)if((d=a._il[b])&&"s_l"==d._c&&d.tagContainerName==c){a.M(d);if(d.lmq)for(b=0;b<d.lmq.length;b++)f=d.lmq[b],a.loadModule(f.n);if(d.ml)for(f in d.ml)if(a[f])for(b in c=a[f],f=d.ml[f],f)!Object.prototype[b]&&("function"!=typeof f[b]||0>(""+f[b]).indexOf("s_c_il"))&&(c[b]=f[b]);if(d.mmq)for(b=0;b<d.mmq.length;b++)f=d.mmq[b],a[f.m]&&(c=a[f.m],c[f.f]&&"function"==typeof c[f.f]&&(f.a?c[f.f].apply(c,f.a):c[f.f].apply(c)));if(d.tq)for(b=0;b<d.tq.length;b++)a.track(d.tq[b]);d.s=
a;break}};a.Util={urlEncode:a.escape,urlDecode:a.unescape,cookieRead:a.cookieRead,cookieWrite:a.cookieWrite,getQueryParam:function(c,b,d){var f;b||(b=a.pageURL?a.pageURL:k.location);d||(d="&");return c&&b&&(b=""+b,f=b.indexOf("?"),0<=f&&(b=d+b.substring(f+1)+d,f=b.indexOf(d+c+"="),0<=f&&(b=b.substring(f+d.length+c.length+1),f=b.indexOf(d),0<=f&&(b=b.substring(0,f)),0<b.length)))?a.unescape(b):""}};a.A="supplementalDataID timestamp dynamicVariablePrefix visitorID marketingCloudVisitorID analyticsVisitorID audienceManagerLocationHint authState fid vmk visitorMigrationKey visitorMigrationServer visitorMigrationServerSecure charSet visitorNamespace cookieDomainPeriods fpCookieDomainPeriods cookieLifetime pageName pageURL referrer contextData currencyCode lightProfileID lightStoreForSeconds lightIncrementBy retrieveLightProfiles deleteLightProfiles retrieveLightData pe pev1 pev2 pev3 pageURLRest".split(" ");
a.c=a.A.concat("purchaseID variableProvider channel server pageType transactionID campaign state zip events events2 products audienceManagerBlob tnt".split(" "));a.ga="timestamp charSet visitorNamespace cookieDomainPeriods cookieLifetime contextData lightProfileID lightStoreForSeconds lightIncrementBy".split(" ");a.J=a.ga.slice(0);a.oa="account allAccounts debugTracking visitor trackOffline offlineLimit offlineThrottleDelay offlineFilename usePlugins doPlugins configURL visitorSampling visitorSamplingGroup linkObject clickObject linkURL linkName linkType trackDownloadLinks trackExternalLinks trackClickMap trackInlineStats linkLeaveQueryString linkTrackVars linkTrackEvents linkDownloadFileTypes linkExternalFilters linkInternalFilters useForcedLinkTracking forcedLinkTrackingTimeout trackingServer trackingServerSecure ssl abort mobile dc lightTrackVars maxDelay expectSupplementalData AudienceManagement".split(" ");
for(n=0;250>=n;n++)76>n&&(a.c.push("prop"+n),a.J.push("prop"+n)),a.c.push("eVar"+n),a.J.push("eVar"+n),6>n&&a.c.push("hier"+n),4>n&&a.c.push("list"+n);n="latitude longitude resolution colorDepth javascriptVersion javaEnabled cookiesEnabled browserWidth browserHeight connectionType homepage".split(" ");a.c=a.c.concat(n);a.A=a.A.concat(n);a.ssl=0<=k.location.protocol.toLowerCase().indexOf("https");a.charSet="UTF-8";a.contextData={};a.offlineThrottleDelay=0;a.offlineFilename="AppMeasurement.offline";
a.Ca=0;a.fa=0;a.I=0;a.Ba=0;a.linkDownloadFileTypes="exe,zip,wav,mp3,mov,mpg,avi,wmv,pdf,doc,docx,xls,xlsx,ppt,pptx";a.w=k;a.d=k.document;try{a.kb="Microsoft Internet Explorer"==navigator.appName}catch(y){}a.Z=function(){a.aa&&(k.clearTimeout(a.aa),a.aa=q);a.j&&a.D&&a.j.dispatchEvent(a.D);a.q&&("function"==typeof a.q?a.q():a.j&&a.j.href&&(a.d.location=a.j.href));a.j=a.D=a.q=0};a.Ea=function(){a.b=a.d.body;a.b?(a.p=function(c){var b,d,f,e,g;if(!(a.d&&a.d.getElementById("cppXYctnr")||c&&c["s_fe_"+a._in])){if(a.qa)if(a.useForcedLinkTracking)a.b.removeEventListener("click",
a.p,!1);else{a.b.removeEventListener("click",a.p,!0);a.qa=a.useForcedLinkTracking=0;return}else a.useForcedLinkTracking=0;a.clickObject=c.srcElement?c.srcElement:c.target;try{if(!a.clickObject||a.H&&a.H==a.clickObject||!(a.clickObject.tagName||a.clickObject.parentElement||a.clickObject.parentNode))a.clickObject=0;else{var m=a.H=a.clickObject;a.ea&&(clearTimeout(a.ea),a.ea=0);a.ea=setTimeout(function(){a.H==m&&(a.H=0)},1E4);f=a.wa();a.track();if(f<a.wa()&&a.useForcedLinkTracking&&c.target){for(e=c.target;e&&
e!=a.b&&"A"!=e.tagName.toUpperCase()&&"AREA"!=e.tagName.toUpperCase();)e=e.parentNode;if(e&&(g=e.href,a.ya(g)||(g=0),d=e.target,c.target.dispatchEvent&&g&&(!d||"_self"==d||"_top"==d||"_parent"==d||k.name&&d==k.name))){try{b=a.d.createEvent("MouseEvents")}catch(n){b=new k.MouseEvent}if(b){try{b.initMouseEvent("click",c.bubbles,c.cancelable,c.view,c.detail,c.screenX,c.screenY,c.clientX,c.clientY,c.ctrlKey,c.altKey,c.shiftKey,c.metaKey,c.button,c.relatedTarget)}catch(q){b=0}b&&(b["s_fe_"+a._in]=b.s_fe=
1,c.stopPropagation(),c.stopImmediatePropagation&&c.stopImmediatePropagation(),c.preventDefault(),a.j=c.target,a.D=b)}}}}}catch(r){a.clickObject=0}}},a.b&&a.b.attachEvent?a.b.attachEvent("onclick",a.p):a.b&&a.b.addEventListener&&(navigator&&(0<=navigator.userAgent.indexOf("WebKit")&&a.d.createEvent||0<=navigator.userAgent.indexOf("Firefox/2")&&k.MouseEvent)&&(a.qa=1,a.useForcedLinkTracking=1,a.b.addEventListener("click",a.p,!0)),a.b.addEventListener("click",a.p,!1))):setTimeout(a.Ea,30)};a.Ea()}
function s_gi(a){var k,q=window.s_c_il,r,n,t=a.split(","),u,s,x=0;if(q)for(r=0;!x&&r<q.length;){k=q[r];if("s_c"==k._c&&(k.account||k.oun))if(k.account&&k.account==a)x=1;else for(n=k.account?k.account:k.oun,n=k.allAccounts?k.allAccounts:n.split(","),u=0;u<t.length;u++)for(s=0;s<n.length;s++)t[u]==n[s]&&(x=1);r++}x||(k=new AppMeasurement);k.setAccount?k.setAccount(a):k.sa&&k.sa(a);return k}AppMeasurement.getInstance=s_gi;window.s_objectID||(window.s_objectID=0);
function s_pgicq(){var a=window,k=a.s_giq,q,r,n;if(k)for(q=0;q<k.length;q++)r=k[q],n=s_gi(r.oun),n.setAccount(r.un),n.setTagContainer(r.tagContainerName);a.s_giq=0}s_pgicq();

