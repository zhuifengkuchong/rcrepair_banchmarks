(function ($) {

/**
 * Fix for IE.
 * To make buttons inside a tags work.
 */
Drupal.behaviors.aButtonFixIE = {
  attach: function (context, settings) {
    if ($.browser.msie) {
      $('body').once('aButtonFixIE', function () {
        $('p > a > input').bind('click', function() {
          location.href = $(this).closest('a').attr('href');
        });
      });
    }
  }
};


})(jQuery);
