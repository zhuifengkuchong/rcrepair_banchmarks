(function ($) {

Drupal.behaviors.featuredStoriesSlider = {
  attach: function (context, settings) {

  	var slides = $('#slides', context);

  	slides.once('featuredStoriesSlider', function () {

			$('#slides').slides({
				generatePagination: true,
				container: 'slides_container',
				paginationClass: 'pagination',
				preload: true,
				preloadImage: 'Unknown_83_filename'/*tpa=http://www.pfizer.com/sites/default/files/advagg_js/sites/default/files/img/loading.gif*/,
				play: 5000,
				pause: 2500,
				hoverPause: true,
				animationStart: function(current){
					$('.caption').animate({
						bottom: -35,
						height: 0,
					}, 100);
				},
				animationComplete: function(current){
					$('.caption').animate({
						bottom: 0,
						height: 100,
					}, 200);
				},
				slidesLoaded: function() {
					$('.caption').animate({
						bottom: 0,
						height: 100,
					}, 200);
				}
			});

	  });

  }
};

})(jQuery);


;/**/
