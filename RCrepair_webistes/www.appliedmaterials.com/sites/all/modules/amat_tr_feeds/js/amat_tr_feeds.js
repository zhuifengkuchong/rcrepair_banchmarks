//console.log('tr feeds js: 1');
(function ($) {

	//  Document Loaded
	//$(window).load() 

// BTC: This just can't be good.
//	$(function() {
//		if($('body').hasClass('section-investor-relations')) {
//			
//			$('#block-amat_tr_feeds-0').find('h2.title').append('<a href="http://investors.appliedmaterials.com/corporate.rss?c=112059&Rule=Cat=sec~subcat=ALL"><img src="../../../../../misc/feed.png"/*tpa=http://www.appliedmaterials.com/misc/feed.png*/ class="feed-icon"/></a>');
//			
//			$('#block-amat_tr_feeds-1').after('<div class="clear-block"></div>');
//			
//			$('#block-amat_tr_feeds-12').before('<div class="clear-block"></div>');
//			
//			$('#block-amat_tr_feeds-2').find('h2.title').append('<a href="http://investors.appliedmaterials.com/corporate.rss?c=112059&Rule=Cat=events~subcat=ALL"><img src="../../../../../misc/feed.png"/*tpa=http://www.appliedmaterials.com/misc/feed.png*/ class="feed-icon"/></a>');
//			
//			$('#block-views-view_ir_news_landing-block_4 h2.title').append($('#block-views-view_ir_news_landing-block_4 .feed-icon'));
//			
//			
//			var content = $('#block-amat_tr_feeds-5');
//			var signup = $('#block-amat_tr_feeds-6');
//			var twitter = $('#block-amat_twitter-0');
//			var irKit = $('#block-amat_tr_feeds-14');
//			$('#rail-left #secondary-nav').append(irKit).append(content).append(twitter).append(signup);
//	
//			var left11 = $('#block-amat_tr_feeds-11');
//			$('.region-rail-left').after(left11);
//			
//			if($('.menu-mlid-16893').hasClass('active-trail')) {
//				$('.menu-mlid-16893').append('<ul class="menu">	<li class="leaf first"><a href="http://www.appliedmaterials.com/newsroom/semiconductor" title="Semiconductor">Semiconductor</a></li><li class="leaf"><a href="http://www.appliedmaterials.com/newsroom/display" title="Display">Display</a></li><li class="leaf"><a href="http://www.appliedmaterials.com/newsroom/solar" title="Solar">Solar</a></li>		<li class="leaf last"><a href="http://www.appliedmaterials.com/newsroom/automation-software" title="Automation Software">Automation Software</a></li></ul>');
//			}
//			$('#marketing_pane_carousel_nav:first').css('display','none');
//			
//		Drupal.attachBehaviors(); 
//		} //section-investor-relations
//	}); //doc.ready

	//$().ajaxComplete(function(e, xhr, settings) {
	//	if($('body').hasClass('section-investor-relations') {
	//		Drupal.attachBehaviors(); 
	//	}	
	//}); // ajaxComplete

	
	function killFormAction(form) {
		return false;
	}
	function addAjaxLoader(loadDiv) {
		$.loaderDiv = loadDiv;
		$.loaderDiv.append('<div class="ajax-loader"></div><div class="ajax-loader-icon"></div>');
		return false;
	}
	
	function removeAjaxLoader() {
		$('body').find('.ajax-loader').remove();
		$('body').find('.ajax-loader-icon').remove();
		return false;
	}
	
	/***************************************** Drupal.behaviors.updateFullQuote **/
	Drupal.behaviors.updateFullQuote = {
	// Bind an AJAX callback to our links				
	//console.log('Drupal.behaviors.updateFullQuote: 1');
	
		attach: function (context, settings) {
			$('.refresh-quote-link:not(.refresh-quote-link-processed)').addClass('refresh-quote-link-processed').each(function() {
				
				$(this).bind('click', function(event) {
					event.preventDefault();	


					var container = $(this).parents('.pane-content');
					
					if (container.length == 0) {
				      container = $(this).parents('.content');
					}
					
					if (container.length == 0) {
					  container = $(this).parent().parent();
					}

					container.css('position', 'relative');
		
					addAjaxLoader(container);

					ajaxURL ='/investor-relations/1/updateFullQuote';
					$.ajax({
						type: "POST",
						url: ajaxURL,
						beforeSend: function() {
						},
						success: function(data) {
							$('.amat-ir-stock-quotes').replaceWith(data);
						},
						error: function(xmlhttp) {
							removeAjaxLoader();
						},
						complete: function() {					
							$('.refresh-quote-link-processed').removeClass('refresh-quote-link-processed');
							removeAjaxLoader();
							Drupal.attachBehaviors('.amat-ir-stock-quotes'); 
						}
					}); // $.ajax	
				
				return false;
				}); //click	
			}); //each		
		}
		

	}; //Drupal.behaviors.updateFullQuote
	
	/***************************************** Drupal.behaviors.updateChartQuote **/
	Drupal.behaviors.updateChartQuote = {
	// Bind an AJAX callback to our links				
	//console.log('Drupal.behaviors.updateChartQuote: 2');
		attach: function (context, settings) {
			$('.refresh-quote-chart-link:not(.refresh-quote-chart-link-processed)').addClass('refresh-quote-chart-link-processed').each(function() {
				
				$(this).bind('click', function(event) {
					event.preventDefault();	
					addAjaxLoader($('#block-amat_tr_feeds-3'));
					ajaxURL ='/investor-relations/1/updateChartQuote';
					$.ajax({
						type: "POST",
						url: ajaxURL,
						beforeSend: function() {
						},
						success: function(data) {	
							$('#amat-ir-stock-table').remove();
							$('#amat-ir-stock-quotes').replaceWith(data);
						},
						error: function(xmlhttp) {
							removeAjaxLoader();
						},
						complete: function() {					
							$('.refresh-quote-chart-link-processed').removeClass('refresh-quote-chart-link-processed');
							
							removeAjaxLoader();
							Drupal.attachBehaviors('#amat-ir-stock-quotes'); 
						}
					}); // $.ajax	
				
				return false;
				}); //click	
			}); //each
		}
	}; //Drupal.behaviors.updateChartQuote
	
	
	/***************************************** Drupal.behaviors.updateChartImage **/
	Drupal.behaviors.updateChartimage = {
	//console.log('Drupal.behaviors.updateChartimage: 1');
        attach: function (context, settings) {
			$imgSRC = "http://charts.edgar-online.com/ext/charts.dll?";
			
			$addAChart = "";
			
			$newIMG = null;
			
			$('#btnSubmit:not(.btnSubmit-processed)').addClass('btnSubmit-processed').each(function() {
			
				$(this).bind('click', function(event) {
    				event.preventDefault();	
    				addAjaxLoader($('#chart-image-td'));
    				
    				// get form vals
    				var tickerComp = 		$("#control_TickerCompare").val();
    				var timeFrame = 		$('#control_time').val();
    				var frequency = 		$('#control_freq').val();
    				var compTo = 			$('#control_compidx').val();
    				var chartType = 		$('#control_type').val();
    				var displayEvents = 	$('#control_display_events').val();
    				var scale = 				$('#control_scale').val();
    				
    				
    				// test display vals
    			if(false) {	
    				$('#chart-image-td #info').html(
    					'tickerComp: '+tickerComp+'<br />'+
    					'timeFrame: '+timeFrame+'<br />'+
    					'frequency: '+frequency+'<br />'+
    					'compTo: '+compTo+'<br />'+
    					'chartType: '+chartType+'<br />'+
    					'displayEvents: '+displayEvents+'<br />'+
    					'scale: '+scale+'<br />'
    				);
    			}
    				//build URL string
    				(parseInt(timeFrame) == 61)?timeFrame="2-11-2-0-0-009301600":timeFrame="2-2-18-0-0-"+timeFrame;
    				$imgSRC += timeFrame + "-03NA000000AMAT-"; //
    				
    				if(tickerComp != "") {
    					comps = tickerComp.split(',');
    					$.each(comps, function(index, value) {
    						$imgSRC += "03NA000000"+value+"-";
    						($addAChart == "|2")?$addAChart="|2|3":$addAChart="|2";
    					});			
    				}
    				if((compTo != "") && ($addAChart!="|2|3")) {
    					$imgSRC += compTo+"-";
    					($addAChart == "|2")?$addAChart="|2|3":$addAChart="|2";
    				}
    				
    				$imgSRC += "&SF:"+chartType+$addAChart;
    				
    				if(displayEvents != "") {
    					$imgSRC += "|"+displayEvents;
    				}
    				
    				$imgSRC += "-HT=300-WD=600-";
    				$imgSRC += "FREQ="+frequency;
    				$imgSRC += "-BG=FFFFFF-FTS:A17=0-FC:2=000000-FC:3=009900-FF:1=A3DBE8-FB:1=FFFFFF-FL:1=000000-AT:"+scale;
    				
    				//replace image
    				$newIMG = document.createElement('img');
    	 
    				$newIMG.src= $imgSRC;
    				$('#chart-image').replaceWith($newIMG)
    				$('.ccbnBgChart img').attr('id','chart-image').attr("alt","Stock chart for: 03NA000000AMAT");
    				
    			removeAjaxLoader();
    			
    			Drupal.attachBehaviors('#form-td'); 
    			return false;
    			}); //click	
    		}); //each
    	} //Drupal.behaviors.updateAnnualReports
    }
    
})(jQuery);