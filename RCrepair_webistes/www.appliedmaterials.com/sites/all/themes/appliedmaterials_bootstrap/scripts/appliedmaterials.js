
function getUrlParams() {
    var result = {};
    var params = (window.location.search.split('?')[1] || '').split('&');
    for(var param in params) {
        if (params.hasOwnProperty(param)) {
            paramParts = params[param].split('=');
            result[paramParts[0]] = decodeURIComponent(paramParts[1] || "");
        }
    }
    return result;
}


jQuery(function($) {
  //Homepage carousel
  $(function() {
    $('#hero-banner-carousel').carousel({interval: 5000});
    
    setTimeout(function() {
      // Do something after 5 seconds
      var bannerImgHeight = $('#hero-banner-carousel img').height();
      if(bannerImgHeight > 0){
        $('#hero-banner-carousel .hero-slide').css('min-height', bannerImgHeight);
      }
      //Adjust banner css on resize
      $(window).resize(function(){
        $('.carousel').carousel('pause');
        var bannerImgHeight = $('#hero-banner-carousel img').height();
        if(bannerImgHeight > 0){
          $('#hero-banner-carousel .hero-slide').css('min-height', bannerImgHeight);
        }      
      });

    }, 250);

  });
  //unbind JS from webforms
  $('.webform-client-form').unbind();
  //Add video.js to FF
    var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
    if(is_firefox)
    {
          var ss = document.createElement("link");
          ss.type = "text/css";
          ss.rel = "stylesheet";
          ss.href = "../../../../../../vjs.zencdn.net/4.3/video-js.css"/*tpa=http://vjs.zencdn.net/4.3/video-js.css*/;
          $("body").append(ss);

          var s = document.createElement("script");
          s.type = "text/javascript";
          s.src = "../../../../../../vjs.zencdn.net/4.3/video.js"/*tpa=http://vjs.zencdn.net/4.3/video.js*/;
          $("body").append(s);

          if($('#pilot_careers_video').length){
            setTimeout(function() {
              $('#pilot_careers_video').removeClass('vjs-controls-disabled');
              $('#pilot_careers_video .vjs-poster').show();
            }, 1500);           
            $('#pilot_careers_video').click(function(){
              $('#pilot_careers_video .vjs-poster').hide();
            });
          }
    }
  //IE Specific JS
  if($.browser.msie && parseInt($.browser.version, 10) <= 8){
          if($('#pilot_careers_video').length){
            setTimeout(function() {
              $('#pilot_careers_video').removeClass('vjs-controls-disabled');
              $('#pilot_careers_video .vjs-poster').show();
            }, 1500);           
            $('#pilot_careers_video').click(function(){
              $('#pilot_careers_video .vjs-poster').hide();
            });
          }
  }

  //Check device
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    //disable feedback modal and open in new page
    $('.feedback-container a').removeAttr('data-toggle');
    $('.feedback-container a').attr('href','/feedback');

    //update captcha img height and width for mobile devices
    setTimeout(function() {
        $('.captcha img').attr('height', 60);
        $('.captcha img').attr('width', 180);
      }, 2500);


    //Add new class to videos on iPad
    if(/iPad/i.test(navigator.userAgent)){
      $('.video video').addClass('ipad');
      $('.video-play-button').remove();
    }

    //Add video poster as image for iphone
    if(/iPhone/i.test(navigator.userAgent)){
      $('.video-play-button').remove();
      // $('.video video').wrap('<div class="video-controls-wrap"></div>');
      // $('video').each(function(){
      //     var poster = $(this).attr('poster');
      //     var output = '<img class="visible-phone iphone-poster" src="'+poster+'" />';

      //     $(this).before(output);
      //     $(this).after('<a class="video-play-button">Play</a>');          
      // });
    }
    $('.mobile .products-technologies').attr('href','javascript:;');
  }

  //Feedback Form action. Set to reload current page
  var feedbackURL = window.location.href;
  $('#webform-client-form-3332019').attr('action', feedbackURL);

  // Override Bootstrap behavior on Company Menu (and other dropdowns)
  $('ul.menu.nav .dropdown-menu a.dropdown-toggle').click(function() {
    window.location.href = $(this).attr('href');
  });

  // Hover state for Products
  if (window.innerWidth > 1024) {
    $('.prod-wrap').on('mouseover', function() {
      $(this).children('.prod-description').fadeIn();
    });
    $('.prod-wrap').on('mouseleave', function() {
      $(this).children('.prod-description').fadeOut();
    });
  }

  // Vertical tabs
  $('.vertical-tabs .panel-col-right .panel-pane').first().addClass('current-tab');
  $('.vertical-tabs .panel-col-left li').first().addClass('current');
  $('.vertical-tabs .panel-col-left li').click(function(e) {
    if ($('a', this).hasClass('request-information-cta')) {
      return true;
    }
    $('.current-tab').removeClass('current-tab');
    $('.current').removeClass('current');
    $('.panel-col-right .panel-pane').eq($(this).index()).addClass('current-tab');
    $(this).addClass('current');
    e.preventDefault();
  });

  // Vertical tabs -- Locations
  // Conflicts with the panel-pane Vertical Tabs above, but didn't want to fragment CSS.
  var tabsInterior = $('.vertical-tabs .panel-col-right .tab-content');
  if (tabsInterior.length > 0) {
    var verticalTabs = $('.vertical-tabs .panel-col-left .field-content');
    $('.vertical-tabs .panel-col-right .panel-pane').css('display', 'block');
    $('.current-tab').removeClass('current-tab');
    tabsInterior.first().addClass('current-tab');
    verticalTabs.first().addClass('current');
    verticalTabs.click(function(e) {
      $('.current-tab').removeClass('current-tab');
      $('.current').removeClass('current');
      tabsInterior.eq($(this).parent().parent().index()).addClass('current-tab');
      $(this).addClass('current');
      e.preventDefault();
    });
  }

  // Vertical tabs for AGS L3 software pages that don't use panels
  var agsTabs = $('.node-software .ags-content .ags-tab');
  if (agsTabs.length > 0){
    agsTabs.first().addClass('current-tab');
    $('.node-software .vertical-menu-list li').first().addClass('current');
    $('.node-software .vertical-menu-list li').click(function (e) {
      $('.current-tab').removeClass('current-tab');
      $('.current').removeClass('current');
      agsTabs.eq($(this).index()).addClass('current-tab');
      $(this).addClass('current');
      e.preventDefault();
    });
  }

  //Vertical tabs for AGS Semiconductor Service pages
  var agsServiceTabs = $('.node-service .ags-content .ags-tab');
  if (agsServiceTabs.length > 0){
    agsServiceTabs.first().addClass('current-tab');
    $('.node-service .vertical-menu-list li').first().addClass('current');
    $('.node-service .vertical-menu-list li').click(function (e) {
      $('.current-tab').removeClass('current-tab');
      $('.current').removeClass('current');
      agsServiceTabs.eq($(this).index()).addClass('current-tab');
      $(this).addClass('current');
      e.preventDefault();
    });
  }

  //Update page to the correct location if hash is present in URL
  var hash = window.location.hash;
  if(hash){
    var clean_text = hash.substring(1);
    var text = clean_text.split('+');

    if(text[0] == 'loc'){
      $('.view-amat2-locations .tab-content').removeClass('current-tab');
      $('.view-amat2-locations .tab-content').each(function(){
         var getH3 = $(this).children('h3').text();
         getH3 = getH3.toLowerCase();
         getH3 = getH3.replace(' ', '-');
         if(getH3 == text[1]){
          $(this).addClass('current-tab');
         }
      });

      $('.view-amat2-locations .views-field-field-country-country-name .field-content').removeClass('current');
      $('.view-amat2-locations .views-field-field-country-country-name .field-content').each(function(){
        var getFieldName = $(this).children('.btn').text();
        getFieldName = getFieldName.toLowerCase();
        getFieldName = getFieldName.replace(' ', '-');

        if(getFieldName == text[1]){
          $(this).addClass('current');
        }
      });
    }
  }

  var verticalTabsExist = $('body').hasClass("vertical-tabs");
  //Vertical Tab hash matching functionality.
  //If there is a hash in the URL, show the appropriate tab.
  if(verticalTabsExist){
    var urlhash;

    //Get URL Hash
    urlhash = window.location.hash;

    var matchFound = false;
    //Detect first level tab if one exists
    var countTab = 0;
    $('.vertical-menu-list li a').each(function(){
      var linkHash = $(this).attr('href');
      //Make sure url hash matches href
      if(urlhash == linkHash){
        matchFound = true;
        return false;
      }
      countTab++;
    });

    if(matchFound){
        $('.current-tab').removeClass('current-tab');
        $('.current').removeClass('current');
        $('.panel-col-right .panel-pane').eq(countTab).addClass('current-tab');
        $('.vertical-menu-list li').eq(countTab).addClass('current');
    }
  }

  // Investor Relations tabs
  $('.investor-relations-block .ir-main-tab li').click(function(e) {
    $('.investor-relations-block > .current-tab').removeClass('current-tab');
    $('.ir-main-tab .current').removeClass('current');
    $('.investor-tab').eq($(this).index()).addClass('current-tab');
    $(this).addClass('current');
    e.preventDefault();
  });

  $('.investor-relations-block .ir-sub-tab li').click(function(e) {
    $('.quarterly-earnings-feed > .current-tab').removeClass('current-tab');
    $('.ir-sub-tab .current').removeClass('current');
    $('.quarterly-earnings-tab').eq($(this).index()).addClass('current-tab');
    $(this).addClass('current');
    e.preventDefault();
  });

  $('.investor-relations-block .ir-sub-tab-reports li').click(function(e) {
    $('.amat-ir-reports-years > .current-tab').removeClass('current-tab');
    $('.ir-sub-tab-reports .current').removeClass('current');
    $('.annual-reports-tab').eq($(this).index()).addClass('current-tab');
    $(this).addClass('current');
    e.preventDefault();
  });

  //Awards Page Button filtering
  $('#awards-landing-filter .btn').live('click',function(event){
    var selectedBtn = $(this).attr('value');
    //Initiate loading function
    loadingSpinner($('.view-id-view_awards_landing .view-content'), $('.view-id-view_awards_landing'));

    $('#edit-field-award-category-value').val(selectedBtn);
    $('#edit-submit-view-awards-landing').click();

    $('#awards-landing-filter a').each(function(){
      if($(this).hasClass('btn-current')){
        $(this).removeClass('btn-current');
      }
    });

    $(this).addClass('btn-current');

  });

  var pathname = window.location.pathname;

  var path_split = pathname.split('/');

  //Active state for sub navigation
  var section_nav = $('.section-navigation').length;
  var main_landing = $('.hero-slides').length;

  if(section_nav){
    $('.section-navigation .nav-links a').each(function(){
      var subnav_links = $(this).text().toLowerCase();
      var match_found = $.inArray(subnav_links, path_split);



      if(match_found > -1){
        if(path_split[1] == 'zh-hans' || path_split[1] == 'zh-hant' || path_split[1] == 'ja' || path_split[1] == 'ko' || path_split[1] == 'en-in' || path_split[1] == 'en-sg'){
          var i = 2;
        }
        else{
          var i = 1;
        }
        if(path_split[i] == 'global-services'){
          $(this).each(function(){
            if($(this).text().toLowerCase() == path_split[i+1]){
              $(this).addClass('active');
            }
          });
        }
        else{
          $(this).addClass('active');
        }
      }
    });

    $(window).resize(fixedHeader).trigger("resize");
  }

function fixedHeader(){
    var window_width = window.innerWidth;
    //Fixed header
    if(window_width < 980){
      //Mobile & Tablet Width
      $(window).unbind('scroll.fixedHeader');
      mobileHeader();
    }

    if(window_width > 979){
      //Desktop Width
      if(!main_landing){
        $(window).unbind('scroll.fixedHeader');
        childLandingHeader();
      }

      if (main_landing) {
        $(window).unbind('scroll.fixedHeader');
        mainLandingHeader();
      }
    }
}
function mainLandingHeader(){

  $('body').removeClass('fixed-header');
  $('.section-navigation').addClass('fixed');

  var mainLandingHeaderScroll = function () {
    var scroll = $(window).scrollTop();
    var elementOffset = $('.hero-slides .nav-links').offset().top;
    var distance      = ((elementOffset + 90) - scroll);
    var window_width = window.innerWidth;

    if (scroll >= distance && window_width > 979) {
      $('.section-navigation').addClass('fixed');
      $('.section-navigation').show();
      $('#hero-banner-carousel .nav-links').addClass('hidden');
    }
    if(scroll <= distance && window_width > 979) {
      $('.section-navigation').hide();
      $('.section-navigation').removeClass('fixed');
      $('#hero-banner-carousel .nav-links').removeClass('hidden');
    }
  }

  $(window).bind('scroll.fixedHeader', mainLandingHeaderScroll);

  return mainLandingHeaderScroll;
}
function childLandingHeader(){
  checkScrollDistance();
  function checkScrollDistance(){
    var scroll = $(window).scrollTop();
    if(scroll > 135){
      $('.section-navigation').addClass('fixed');
    }
  }

  $('body').removeClass('fixed-header');
  var childLandingHeaderScroll = function() {
    var scroll = $(window).scrollTop();
    var elementOffset = $('.section-navigation').offset().top;
    var distance      = ((elementOffset + 104) - scroll);

    if (scroll >= distance) {
      $('.section-navigation').addClass('fixed');
      $('body').addClass('fixed-header-desktop');
    } else {
      $('.section-navigation').removeClass('fixed');
      $('body').removeClass('fixed-header-desktop');
    }
  }
  $(window).bind('scroll.fixedHeader', childLandingHeaderScroll);
}

function mobileHeader(){
  var previousScroll = 0;
  $('.section-navigation').removeClass('fixed');
  var mobileHeaderScroll = function (){
     var currentScroll = $(this).scrollTop();
     //Detect scroll direction and add/remove fixed navigation
     if (currentScroll > previousScroll && currentScroll > 75){
         $('body').removeClass('fixed-header');
         $('.header-nav-wrap').removeClass('fixed');
     }
     if(currentScroll < 5){
      $('body').removeClass('fixed-header');
      $('.header-nav-wrap').removeClass('fixed');
     }
     if(currentScroll < previousScroll ){
        $('body').addClass('fixed-header');
        $('.header-nav-wrap').addClass('fixed');
     }
     previousScroll = currentScroll;
  }

  $(window).bind('scroll.fixedHeader', mobileHeaderScroll);

  return mobileHeaderScroll;
}

  var products_page = false;
  var anyparts_page = false;
  var main_parts = false;
  var req_quote = false;
  var product_library = false;
  var product_listing = false;
  var media_center = false;
  var locations_ww = false;
  var request_info_form = false;
  var main_search = false;
  var ir_news_search = false;
  var av_portfolio_search = false;
  var safe_harbor_page = false;
  var subscribe_page = false;
  var endura_ventura_pvd = false;

  //Check the URL and only run code that pertains to the page
  if(($.inArray( 'products', path_split ) > -1) && ($.inArray( 'endura-ventura-pvd', path_split ) > -1)){
    var endura_ventura_pvd = true;
  }
  if($.inArray( 'products', path_split ) > -1){
    var products_page = true;
  }
  if($.inArray( 'product-library', path_split ) > -1){
    var product_library = true;
  }
  if($.inArray( 'listing', path_split ) > -1){
    var product_listing = true;
  }
  if(($.inArray( 'parts', path_split ) > -1) || ($.inArray( 'parts-center', path_split ) > -1)){
    var any_parts = true;
  }
  if($.inArray( 'parts-center', path_split ) > -1){
    var main_parts = true;
  }
  if($.inArray( 'parts', path_split ) > -1){
    var prod_parts = true;
  }
  if($.inArray( 'request-quote', path_split ) > -1){
    var req_quote = true;
  }
  if($.inArray( 'media-center', path_split ) > -1){
    var media_center = true;
  }
  if($.inArray( 'request-information', path_split ) > -1){
    var request_info_form = true;
  }
  if($.inArray( 'search', path_split ) > -1){
    var main_search = true;
  }
  if(($.inArray( 'investor-relations', path_split ) > -1) && ($.inArray( 'news', path_split ) > -1)){
    var ir_news_search = true;
  }
  if(($.inArray( 'applied-ventures', path_split ) > -1) && ($.inArray( 'portfolio', path_split ) > -1)){
    var av_portfolio_search = true;
  }
  if(($.inArray( 'news', path_split ) > -1) && ($.inArray( 'events', path_split ) > -1)){
    var safe_harbor_page = true;
  }
  if(($.inArray( 'contact', path_split ) > -1) && ($.inArray( 'subscribe', path_split ) > -1)){
    var subscribe_page = true;
  }

  //Superscript R in title
  if(endura_ventura_pvd){
    $(".page-title h1").html("Endura<sup>®</sup> Ventura™ PVD");
  }
  //Subscribe page
  if(subscribe_page){
      if($.browser.msie && parseInt($.browser.version, 10) <= 8){
        //Force IE to submit form when Enter key is used
        $("#amat-utilities-nanochip-tech-form #edit-email--2").keydown(function (e) { 
          if(e.which == 13) {
            e.preventDefault();
            $('#amat-utilities-nanochip-tech-form button.form-submit').mousedown();
          }
        });
        //Force all other browsers to submit form when Enter key is used
        $("#amat-utilities-nanochip-fab-form #edit-email").keydown(function (e) { 
          if(e.which == 13) {
            e.preventDefault();
            $('#amat-utilities-nanochip-fab-form button.form-submit').mousedown();
          }
        });
      }
      else{
        $('#amat-utilities-nanochip-tech-form').change(function(){
          $('#amat-utilities-nanochip-tech-form button.form-submit').mousedown();
        });
        $('#amat-utilities-nanochip-fab-form').change(function(){
          $('#amat-utilities-nanochip-fab-form .form-submit').mousedown();
        });
      }
  }

  //Remove section navigation active states from events and news links
  if(safe_harbor_page){
    $('.section-navigation a').removeClass('active');
  }

  //AV Portfolio Search
  if(av_portfolio_search){
    //Add Search markup and functionality to Applied Ventures Portfolio page on load
    var searchIconMarkup = '<a class="parts-search-icon" href="javascript:;">&nbsp;</a>';
    $('.company-applied-ventures-portfolio .form-type-textfield .controls input').attr('placeholder', 'Search');
    $('.company-applied-ventures-portfolio .form-type-textfield .controls').append(searchIconMarkup);
    
    $('.company-applied-ventures-portfolio .form-type-textfield .controls input').change(function(){
      if($.browser.msie && parseInt($.browser.version, 10) <= 8){
        var inputVal = $("#edit-title").val();
        if(inputVal == 'Search'){
          $("#edit-title").val('');
        }
      }
      $('.company-applied-ventures-portfolio .views-submit-button button').click();

      //Initiate Loading function
      loadingSpinner($('.view-id-amat2_ventures_portfolio .view-content'), $('.view-id-amat2_ventures_portfolio'));
    });

    $('.parts-search-icon').click(function(){
      //Initiate Loading function
      loadingSpinner($('.view-id-amat2_ventures_portfolio .view-content'), $('.view-id-amat2_ventures_portfolio'));
     if($.browser.msie && parseInt($.browser.version, 10) <= 8){
        var inputVal = $("#edit-title").val();
        if(inputVal == 'Search'){
          $("#edit-title").val('');
        }
      }

      $('.company-applied-ventures-portfolio .views-submit-button button').click();
    });

    //Re-add Search markup and functionality on AV Portfolio page after drupal AJAX search functionality is completed
    Drupal.behaviors.updateAVPortfolioSearch = {
      attach: function (context, settings) {
        var searchIconMarkup = '<a class="parts-search-icon" href="javascript:;">&nbsp;</a>';
        $('.company-applied-ventures-portfolio .form-type-textfield .controls input').attr('placeholder', 'Search');
        $('.company-applied-ventures-portfolio .form-type-textfield .controls').append(searchIconMarkup);
        $('.parts-search-icon:gt(0)').remove();

        $('.company-applied-ventures-portfolio .form-type-textfield .controls input').change(function(){
          $('.company-applied-ventures-portfolio .views-submit-button button').click();

          //Initiate Loading function
          loadingSpinner($('.view-id-amat2_ventures_portfolio .view-content'), $('.view-id-amat2_ventures_portfolio'));
        });

        $('.parts-search-icon').click(function(){
           if($.browser.msie && parseInt($.browser.version, 10) <= 8){
              var inputVal = $("#edit-title").val();
              if(inputVal == 'Search'){
                $("#edit-title").val('');
              }
            }
            $('.company-applied-ventures-portfolio .views-submit-button button').click();

          //Initiate Loading function
          loadingSpinner($('.view-id-amat2_ventures_portfolio .view-content'), $('.view-id-amat2_ventures_portfolio'));
        });
      }
    };
  }

  //IR News Search
  if(ir_news_search){
    var searchIconMarkup = '<a class="parts-search-icon" href="javascript:;">&nbsp;</a>';
    $('.company-investor-relations-news .form-type-textfield .controls input').attr('placeholder', 'Search');
    $('.company-investor-relations-news .form-type-textfield .controls').append(searchIconMarkup);
    
    $('.company-investor-relations-news .form-type-textfield .controls input').change(function(){
      $('.company-investor-relations-news .views-submit-button button').click();

      //Initiate Loading spinner function
      loadingSpinner($('.view-id-amat_news_block .view-content'), $('.view-id-amat_news_block'));
    });

    $('.parts-search-icon').click(function(){
        $('.company-investor-relations-news .views-submit-button button').click();
    });

    Drupal.behaviors.updateIRNewsSearch = {
      attach: function (context, settings) {
        var searchIconMarkup = '<a class="parts-search-icon" href="javascript:;">&nbsp;</a>';
        $('.company-investor-relations-news .form-type-textfield .controls input').attr('placeholder', 'Search');
        $('.company-investor-relations-news .form-type-textfield .controls').append(searchIconMarkup);
        
        $('.company-investor-relations-news .form-type-textfield .controls input').change(function(){
          $('.company-investor-relations-news .views-submit-button button').click();

          //Initiate Loading spinner function
          loadingSpinner($('.view-id-amat_news_block .view-content'), $('.view-id-amat_news_block'));
        });

        $('.parts-search-icon').click(function(){
            $('.company-investor-relations-news .views-submit-button button').click();
        });
      }
    };
  }

  //Main search page - adding search icon to input
  if(main_search){
    var searchIconMarkup = '<a class="parts-search-icon" href="javascript:;">&nbsp;</a>';
    $('.page-search .form-type-textfield .controls input').attr('placeholder', 'Search');
    $('.page-search .form-type-textfield .controls').append(searchIconMarkup);
    
    $('.page-search .form-type-textfield .controls input').change(function(){
      $('.page-search .form-wrapper button').click();
    });

    $('.parts-search-icon').click(function(){
        $('.page-search .form-wrapper button').click();
    });
  }

  //Request information submit button
  if(request_info_form){
    $('.form-wrapper input').addClass('btn btn-primary');
  }

  //Media Center Filtering
  if(media_center){
    $('#media-center-menu a').click(function(){
      //Add active class to appropriate menu item
      $('#media-center-menu ul li').removeClass('active');
      $(this).parent().addClass('active');

      $('.form-type-textfield input').val('');

      var term_id = $(this).attr('termid');
      $('#edit-field-media-category-tid option[value="'+term_id+'"]').attr('selected', true);

      //Initialize loading overlay and spinner function
      loadingSpinner($('.view-id-amat2_media_browser .view-content'), $('.view-id-amat2_media_browser'));

      $('#views-exposed-form-amat2-media-browser-image-list .form-submit').click();
      $('#views-exposed-form-amat2-media-browser-video-list .form-submit').click();
      $('#views-exposed-form-amat2-media-browser-document-tile-list .form-submit').click();
    });

    $('#media-center-search').change(function(){
      $('#edit-field-media-category-tid option[value="All"]').attr('selected', true);

      $('.form-type-textfield input').val($(this).val());

      //Initialize loading overlay and spinner function
      loadingSpinner($('.view-id-amat2_media_browser .view-content'), $('.view-id-amat2_media_browser'));

      $('#views-exposed-form-amat2-media-browser-image-list .form-submit').click();
      $('#views-exposed-form-amat2-media-browser-video-list .form-submit').click();
      $('#views-exposed-form-amat2-media-browser-document-tile-list .form-submit').click();
    });
  }

  //START - Product Library Filtering
  if(product_library){
    $('.taxonomy-menu a.term').click(function(e){
      e.preventDefault;
      if($.browser.msie && parseInt($.browser.version, 10) <= 8){
        var parentCatText = $(this).parent().parent().parent().parent().parent().parent().parent().siblings('.accordion-heading').children('a').text();
        var subCatText = $(this).text();
      }
      else{
        var parentCatText = $(this).parent().parent().parent().parent().parent().parent().parent().siblings('.accordion-heading').children('a').text().trim();
        var subCatText = $(this).text().trim();
      }

      $('#prod_scat_name').html(parentCatText+' | '+subCatText);
    });

    $('.accordion-heading a').click(function(){
      if($.browser.msie && parseInt($.browser.version, 10) <= 8){
          var headingText = $(this).text();
      }
      else{
        var headingText = $(this).text().trim();
      }
          $('#prod_scat_name').html(headingText);
    });

    //Handle Alphabetical filtering
    $('.alpha-index a').click(function(e){
      var index = $(this).attr('data-id');
      e.preventDefault();

      //If Alpha filter is clicked, set hidden select menu to ALL - product categories,
      //update hidden alpha index field with appropriate value, and submit form.
      $('#edit-taxonomy-vocabulary-2-tid option').first().text('- Any -').attr('selected', true);
      $('#edit-field-alpha-index-value').val(index);

      //Initialize loading overlay and spinner function
      loadingSpinner($('.view-display-id-product_library_tile_view_2 .view-content'), $('.view-display-id-product_library_tile_view_2'));

      $('#edit-submit-amat2-product-selector').click();
    });

    //Load first subcateogory of clicked accordion title
    $('#accordion-products-menu .accordion-heading a').click(function(){
      if($.browser.msie && parseInt($.browser.version, 10) <= 8){
        var headingText = $(this).text();
      }
      else{
        var headingText = $(this).text().trim();
      }
      if(headingText == 'Alphabetical'){
        //If Alphabetical heading is clicked, set hidden select menu to ALL product categories,
        //add AJAX loading spinners, and submit form
        $('#edit-taxonomy-vocabulary-2-tid option').first().text('- Any -').attr('selected', true);
        $('#edit-field-alpha-index-value').val('');

        //Initialize loading overlay and spinner function
        loadingSpinner($('.view-display-id-product_library_tile_view_2 .view-content'), $('.view-display-id-product_library_tile_view_2'));

        $('#edit-submit-amat2-product-selector').click();
      }
      else if(headingText == 'Roll to Roll WEB Coating' || headingText == 'Roll to Roll WEB Coating '){
        //Do nothing if Roll to Roll is clicked, since that is handled elsewhere (around line 695)
        console.log('roll-to-roll');
      }
      else{
        $('#edit-field-alpha-index-value').val('');

        //Load first category on product library when accordion heading is clicked
        if($.browser.msie && parseInt($.browser.version, 10) <= 8){
          var firstCat = $(this).parent().siblings('.accordion-body').find('.taxonomy_menu_wrapper').children('.taxonomy_menu').first().children('.taxonomy-menu').find('.term').first().text();
          var parent_str = '-'+$(this).parent().siblings('.accordion-body').find('.taxonomy_menu_wrapper').children('.taxonomy_menu').first().children('h3').text();
        }
        else{
          var firstCat = $(this).parent().siblings('.accordion-body').find('.taxonomy_menu_wrapper').children('.taxonomy_menu').first().children('.taxonomy-menu').find('.term').first().text().trim();
          var parent_str = '-'+$(this).parent().siblings('.accordion-body').find('.taxonomy_menu_wrapper').children('.taxonomy_menu').first().children('h3').text().trim();
        }
        
         var menu_str = '--'+firstCat;
         var parent = 0;

        //match menu string to select menu value
        $('#edit-taxonomy-vocabulary-2-tid option').each(function(){
          var select_str = $(this).text();
          //Match the select option to the proper parent
          if(select_str == parent_str){
            parent ++;
          }
          if((select_str == menu_str) && (parent > 0)){
            $(this).attr('selected',true);
            parent = 0;
          }
        });

        //Initialize loading overlay and spinner function
        loadingSpinner($('.view-display-id-product_library_tile_view_2 .view-content'), $('.view-display-id-product_library_tile_view_2'));

        $('#edit-submit-amat2-product-selector').click();
      }
    });

    //Handle active state for Table or Tile view buttons
    if(product_listing){
      $('.view-table-btn').addClass('active');
    }
    else{
      $('.view-tile-btn').addClass('active');
    }

    //Handle roll-to-roll link heading in accordion menu
    $('.roll-to-roll').click(function(){
      //Minimize all other accordion sections
      $('.collapse.in').collapse('hide');

      //Clear all other hidden filter fields
      $('#edit-keys').val('');
      $('#edit-title').val('');
      $('#product-library-search').val('');
      $('#edit-field-alpha-index-value').val('');

      //Select roll to roll option in hidden menu filter, and submit form
      $('#edit-taxonomy-vocabulary-2-tid option').val('2000').attr('selected', true);
      $('#edit-submit-amat2-product-selector').click();

      //Add loading spinner
      if(product_listing){
        //Loading spinner markup for LIST view
        loadingSpinner($('.view-display-id-product_library_list_view_2 .view-content'), $('.view-display-id-product_library_list_view_2'));
      }
      else{
        //Loading spinner markup for TILE view
        loadingSpinner($('.view-display-id-product_library_tile_view_2 .view-content'), $('.view-display-id-product_library_tile_view_2'));
      }
    });

    //Reset form when clicking View All Products button
    $('.view-all-products-btn').click(function(){
      //Add loading spinner
      if(product_listing){
        loadingSpinner($('.view-display-id-product_library_list_view_2 .view-content'), $('.view-display-id-product_library_list_view_2'));
      }
      else{
        loadingSpinner($('.view-display-id-product_library_tile_view_2 .view-content'), $('.view-display-id-product_library_tile_view_2'));
      }

      $('#edit-keys').val('');
      $('#edit-title').val('');
      $('#product-library-search').val('');
      $('#edit-field-alpha-index-value').val('');

      $('#edit-taxonomy-vocabulary-2-tid option').first().text('- Any -').attr('selected', true);
      $('#edit-submit-amat2-product-selector').click();
    });

    //Handle custom search input
    $('#product-library-search').change(function(){
      //Add loading spinner
      if(product_listing) {
        loadingSpinner($('.view-display-id-product_library_list_view_2 .view-content'), $('.view-display-id-product_library_list_view_2'));
      }
      else{
        loadingSpinner($('.view-display-id-product_library_tile_view_2 .view-content'), $('.view-display-id-product_library_tile_view_2'));
      }
      $('#edit-field-alpha-index-value').val('');
      $('#edit-taxonomy-vocabulary-2-tid option').first().text('- Any -').attr('selected', true);
      $('#edit-title').val($(this).val());

      $('#edit-submit-amat2-product-selector').click();

      $('.parts-search-icon').click(function(){
        $('#edit-submit-amat2-product-selector').click();
      });
    });
    //Handle click event for accordion menu headings
    $('.taxonomy_menu_wrapper h3').click(function(){
      if($.browser.msie && parseInt($.browser.version, 10) <= 8){
        var clickedText = $(this).text();
      }
      else{
        var clickedText = $(this).text().trim();
      }

      if(clickedText == 'Technical Glossary'){
        //Do not submit form for Technical Glossary heading, redirect to glossary page instead
        console.log('redirecting to glossary page');
      }
      else{
        //Clear all existing filter values
        $('#edit-keys').val('');
        $('#edit-title').val('');
        $('#product-library-search').val('');
        $('#edit-field-alpha-index-value').val('');

        //Add loading spinner
        if(product_listing){
          loadingSpinner($('.view-display-id-product_library_list_view_2 .view-content'), $('.view-display-id-product_library_list_view_2'));
        }
        else{
          loadingSpinner($('.view-display-id-product_library_tile_view_2 .view-content'), $('.view-display-id-product_library_tile_view_2'));
        }

        var menu_list_str = '-'+$(this).text();
        var parent_list_str = $(this).parentsUntil('accordion-body').siblings('.accordion-heading').children().text().trim();
        var parent_list = 0;

        //match menu string to select menu value
        $('#edit-taxonomy-vocabulary-2-tid option').each(function(){
          var select_list_str = $(this).text();

          //Match the select option to the proper parent
          if(select_list_str == parent_list_str){
            parent_list ++;
          }
          if((select_list_str == menu_list_str) && (parent_list > 0)){
            $(this).attr('selected',true);
            parent_list = 0;
          }
          //Submit Search
          $('#edit-submit-amat2-product-selector').click();
        });
      }
    });

    //Swap out accordion icons using bootstrap events
      $('.accordion-group').on('show hide', function (n) {
          $(n.target).siblings('.accordion-heading').find('.accordion-toggle img').toggleClass('show hide');
      });

    //Handle accordion menu click
    $('http://www.appliedmaterials.com/sites//all//themes//appliedmaterials_bootstrap//scripts//ul.taxonomy-menu a.term').click(function(event){
      event.preventDefault(); //Prevent links from redirecting page
      $('#edit-keys').val('');
      $('#edit-title').val('');
      $('#product-library-search').val('');
      $('#edit-field-alpha-index-value').val('');

      //Add loading spinner
      if(product_listing){
        loadingSpinner($('.view-display-id-product_library_list_view_2 .view-content'), $('.view-display-id-product_library_list_view_2'));
      }
      else{
        loadingSpinner($('.view-display-id-product_library_tile_view_2 .view-content'), $('.view-display-id-product_library_tile_view_2'));
      }

      var menu_str = '--'+$(this).text();
      var parent_str = '-'+$(this).parent().parent().siblings('h3').text();
      var parent = 0;

      //match menu string to select menu value
      $('#edit-taxonomy-vocabulary-2-tid option').each(function(){
        var select_str = $(this).text();
        //Match the select option to the proper parent
        if(select_str == parent_str){
          parent ++;
        }
        if((select_str == menu_str) && (parent > 0)){
          $(this).attr('selected',true);
          parent = 0;
        }
      });
      //Submit Search
      $('#edit-submit-amat2-product-selector').click();
    });
  }

  //Enable request quote form fields on webform page
  if(req_quote){
    $('#edit-submitted-part-name').removeAttr('readonly');
    $('#edit-submitted-rquote-part-id').removeAttr('readonly');
    $('#edit-submitted-rquote-category').removeAttr('readonly');
    $('#edit-submitted-sub-category').removeAttr('readonly');
  }

  //Add Search placeholder to products exposed filter
  if(products_page){
    var urlparams = getUrlParams();
    var searchIconMarkup = '<a class="parts-search-icon" href="javascript:;">&nbsp;</a>';
    $('.products .form-type-textfield .controls input').attr('placeholder', 'Search');
    $('.products .form-type-textfield .controls').append(searchIconMarkup);

    if (urlparams.title) $('.edit-keys').val(urlparams.title);
    $('.parts-search-icon').click(function(){
      $('#edit-submit-amat2-product-selector').click();
    });
  }
  //END Products Filtering

  //START PARTS IF STATEMENT
  if(any_parts){
    //Change table opacity when sorting parts or clicking table pagination
    $('.views-table .views-field a').not('.rquote').click(function(){
      $('.views-table tbody').addClass('loading');
    });
    $('.pagination a').click(function(){
      $('.views-table').addClass('loading');
    });
    //Re-attach JS functionality for opacity after drupal AJAX call
    Drupal.behaviors.updateTableOpacity = {
      attach: function (context, settings) {
        $('.views-table .views-field a').not('.rquote').click(function(){
          $('.views-table tbody').addClass('loading');
        });

        $('.pagination a').click(function(){
          $('.views-table').addClass('loading');
        });
      }
    };
    if(main_parts){
      var parts_firstCat = $('#accordion-parts-menu .accordion-heading #cat-1').text();
      var parts_firstSubCat = $('#accordion-parts-menu #scat-1 .accordion-inner').first().children().text();

      $('.view-amat-parts #edit-category').val(parts_firstCat);
      $('.view-amat-parts #edit-subcategory').val(parts_firstSubCat);

      $('.view-amat-parts #edit-submit-amat-parts').click();
      
      setTimeout(function() {
        $('.pane-amat-parts').show();
      }, 1500);

      $('.accordion-heading a').click(function() {
        var $this = $(this);
        var $accordion_body = $this.parents('.accordion-group').find('.accordion-body');
        if (!$accordion_body.hasClass('in')) {
          $accordion_body.find('div').first().click();
        }
      });

      //Parts Accordion Menu
      $('.accordion-body div').click(function() {
        //Insert AJAX Spinner overlay and change table opacity
        $('.views-table').before($('<div class="parts-ajax-overlay"><div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div></div>'));
        $('.views-table').addClass('loading');
        //Clear all fields except Category and subcategory when clicking side menu item
        $('#parts-search').val('');
        $('#edit-partid, #edit-name, #edit-cip-number').val('');
        var li_val = $(this).attr('data');
        li_val = li_val.split('@@');

        $("#edit-category").val(li_val[0]);
        $("#edit-subcategory").val(li_val[1]);
        $("#edit-category").trigger('change');
        $('#edit-subcategory').trigger('change');

        $("#scat_name").html(li_val[0] + " / " + li_val[1]);

        $('#edit-submit-amat-parts').click();
        $('#scat_name').fadeIn('slow');
      });

      //Swap out accordion icon using bootstrap events
      $('.accordion-group').on('show hide', function (n) {
          $(n.target).siblings('.accordion-heading').find('.accordion-toggle img').toggleClass('show hide');
      });

      //Add search term to input for pagination items
      var sPageURL = window.location.search.substring(1);

      if(sPageURL){
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)
        {
            var sParameterName = sURLVariables[i].split('=');
            if(sParameterName[0] == 'name'){
              $('#parts-search').val(sParameterName[1]);
            }
        }
      }

      //Parts Center Request Quote Modal
      $(document).on("click", ".rquote", function () {
        var quotes = $(this).data('id');
        var quote_data = quotes.split("@@");

        $("#edit-submitted-part-name").val(quote_data[0]);
        $("#edit-submitted-rquote-part-id").val(quote_data[1]);
        $("#edit-submitted-rquote-category").val(quote_data[2]);
        $("#edit-submitted-sub-category").val(quote_data[3]);

        $("#edit-submitted-rquote-name").attr("placeholder", "Enter your name");
        $("#edit-submitted-rquote-email").attr("placeholder", "Enter your email address");

        $('.webform-component-textfield input').click(function(){
          $(this).removeAttr('readonly');
        });
      });
    }

    //Product Specific Parts pages
    if(prod_parts){
      var mainCat = $('#parts-menu .menu-item').attr('data');
      mainCat = mainCat.split('@@');
      $("#edit-category").val(mainCat[0]);
      $('#edit-submit-amat-parts').click();

      //Side Menu Click
      $('.menu-item a').click(function(){
        //Insert AJAX Spinner overlay and change table opacity
        $('.views-table').before($('<div class="parts-ajax-overlay"><div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div></div>'));
        $('.views-table').addClass('loading');
        //Clear all fields except Category and subcategory when clicking side menu item
        $('#parts-search').val('');
        $('#edit-partid').val('');
        $('#edit-name').val('');
        $("#edit-cip-number").val('');

        var subCat = $(this).parent().attr('data');
        subCat = subCat.split('@@');

        $("#edit-subcategory").val(subCat[1]);
        $('#edit-submit-amat-parts').click();
        $("#scat_name").html(subCat[1].toUpperCase());
        $('#scat_name').fadeIn('slow');
      });
      //Request quote modal
      $(document).on("click", ".rquote", function () {
        var quotes = $(this).data('id');
        var quote_data = quotes.split("@@");
        $("#edit-submitted-part-name").val(quote_data[0]);
        $("#edit-submitted-rquote-part-id").val(quote_data[1]);
        $("#edit-submitted-rquote-category").val(quote_data[2]);
        $("#edit-submitted-sub-category").val(quote_data[3]);

        $("#edit-submitted-rquote-name").attr("placeholder", "Enter your name");
        $("#edit-submitted-rquote-email").attr("placeholder", "Enter your email address");
        $('.webform-component-textfield input').click(function(){
          $(this).removeAttr('readonly');
        });
      });
    }

    //Parts Center Search
    // Drupal.behaviors.updateCustomSearch = {
    //   attach: function (context, settings) {
    //     $('#parts-search').hasClass('search-processed');
    //     if($('#parts-search').hasClass('search-processed')){
    //       $('.pagination a').click(function(e){
    //         e.preventDefault();
    //         window.alert = function() {};
    //         $('.views-table').before($('<div class="parts-ajax-overlay"><div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div></div>'));
    //         window.location = $(this).attr('href');
    //       });
    //     }
    //   }
    // };

    $('#parts-search').change(function(){
      $(this).addClass('search-processed');
      $('.views-table').before($('<div class="parts-ajax-overlay"><div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div></div>'));
      $('.views-table').addClass('loading');
      if(main_parts){
        $('#edit-category').val('');
        $('#edit-subcategory').val('');
      }
      if(prod_parts){
        $('#edit-subcategory').val('');
      }
      $('#edit-partid, #edit-name, #edit-cip-number').val($(this).val());
      $('#edit-submit-amat-parts').click();
      $('#scat_name').fadeOut('slow');
    });

    $('.parts-search-icon').click(function(){
      $('#edit-submit-amat-parts').click();
    });
  }
  //END PARTS IF STATEMENT

  // Add placeholders to fields
  $('#node-3332209 .email, #node-3332210 .email, #node-3332423 .email').prop('placeholder', 'Enter email address');

  // Media Center form - display "Other" text box when option selected
  $('#edit-submitted-asset-use-4').change(function() {
    if (this.checked) {
      $('#node-3332423 #edit-submitted-other').fadeIn('fast');
    }
    else {
      $('#node-3332423 #edit-submitted-other').fadeOut('fast').val('');
    }
  });

  // Add overlay button to videos except for IE 8 and lower (uses VIDEO.js)
   if($.browser.msie && parseInt($.browser.version, 10) > 8){
      var videoStyle = function () {
        $('.video video').wrap('<div class="video-controls-wrap"></div>');
        $('.video video').each(function(idx) {
          var videoClass = 'video-play' + idx,
              videoPlay = '<a class="' + videoClass + ' video-play-button">Play</a>',
              $this = $(this),
              currentVid = $(this).get(0);
          currentVid.removeAttribute('controls');
          $('.' + videoClass).live('click', function() {
            currentVid.play();
            currentVid.volume = 0.5;
            $(this).fadeOut();
            currentVid.setAttribute('controls');
          });
          $this.after(videoPlay);
        })
      };
      videoStyle();

      $('.video video').live('click', function() {
        currentVid = $(this).get(0);
        currentVid.volume = 0.5;
      });
   }
  if(!$.browser.msie && !is_firefox && !navigator.userAgent.match(/(iPod|iPhone|iPad)/)){
      var videoStyle = function () {
        $('.video video').wrap('<div class="video-controls-wrap"></div>');
        $('.video video').each(function(idx) {
          var videoClass = 'video-play' + idx,
              videoPlay = '<a class="' + videoClass + ' video-play-button">Play</a>',
              $this = $(this),
              currentVid = $(this).get(0);
          currentVid.removeAttribute('controls');
          $('.' + videoClass).live('click', function() {
            currentVid.play();
            currentVid.volume = 0.5;
            $(this).fadeOut();
            currentVid.setAttribute('controls','true');
          });
          $this.after(videoPlay);
        })
      };
      videoStyle();

      $('.video video').live('click', function() {
        currentVid = $(this).get(0);
        currentVid.volume = 0.5;
      });
   }

  // Change - All - text in dropdowns on Portfolio page
  $('#edit-field-portfolio-status-value option[value="All"]').text('Status');
  $('#edit-field-portfolio-location-value option[value="All"]').text('Location');
  $('#edit-field-portfolio-sector-value option[value="All"]').text('Sector');

  $('.vertical-menu-list').closest('.panel-col-left').next('.panel-col-right').addClass('vertical-menu-list-right');

  // Add Take Snapshot button to video fields
  // function addTakeSnapshotButtons() {
  //   var video_fields = [];
  //   // var $video_fields = ;
  //   jQuery('.field-name-field-video').each(function(item) {
  //     $video = jQuery(this);
  //     video_fields.push({
  //       id: $video.attr('id'),
  //       element: $video,
  //       filename: $video.find('.media-item').attr('title')
  //     });
  //   });
  //   // console.dir(video_fields);
  //   // var html = "<a href=\"#\" class=\"button snapshot\" id=\"edit-field-video-collection-und-0-field-video-und-0-remove\">Take Snapshot</a>";
  // }
  // addTakeSnapshotButtons();
});

//Awards Landing page Filtering updates after AJAX call
Drupal.behaviors.updateExposedFilterAwards = {
  attach: function(context) {
    var findIt = $('#edit-field-award-category-value:not(.rewrite-processed)', context);
    if (findIt.length > 0) {
      findIt.addClass('.rewrite-processed');
      $('#edit-field-award-category-value option').each(function() {
        if ($(this).attr('selected')) {
          var selectedOption = $(this).val();
          $('#awards-landing-filter a[value="'+selectedOption+'"]').addClass('btn-current');
        }
      });
    }
  }
};

//Add hidden label to global search for SEO
Drupal.behaviors.updateSearchLabel = {
  attach: function(context) {
    $('.form-search.content-search .input-append', context).prepend('<label class="hide">Search</label>');
  }
};

//Loading overlay and AJAX Spinner markup
function loadingSpinner(overlay, spinner){
  overlay.addClass('loading');
  spinner.append('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>');
}