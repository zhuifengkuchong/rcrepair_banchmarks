/* ===================================================
 * bootstrap-transition.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#transitions
 * ===================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


  /* CSS TRANSITION SUPPORT (http://www.modernizr.com/)
   * ======================================================= */

  $(function () {

    $.support.transition = (function () {

      var transitionEnd = (function () {

        var el = document.createElement('bootstrap')
          , transEndEventNames = {
               'WebkitTransition' : 'webkitTransitionEnd'
            ,  'MozTransition'    : 'transitionend'
            ,  'OTransition'      : 'oTransitionEnd otransitionend'
            ,  'transition'       : 'transitionend'
            }
          , name

        for (name in transEndEventNames){
          if (el.style[name] !== undefined) {
            return transEndEventNames[name]
          }
        }

      }())

      return transitionEnd && {
        end: transitionEnd
      }

    })()

  })

}(window.jQuery);
/* =========================================================
 * bootstrap-modal.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#modals
 * =========================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================= */


!function ($) {

  "use strict"; // jshint ;_;


 /* MODAL CLASS DEFINITION
  * ====================== */

  var Modal = function (element, options) {
    this.options = options
    this.$element = $(element)
      .delegate('[data-dismiss="modal"]', 'click.dismiss.modal', $.proxy(this.hide, this))
    this.options.remote && this.$element.find('.modal-body').load(this.options.remote)
  }

  Modal.prototype = {

      constructor: Modal

    , toggle: function () {
        return this[!this.isShown ? 'show' : 'hide']()
      }

    , show: function () {
        var that = this
          , e = $.Event('show')

        this.$element.trigger(e)

        if (this.isShown || e.isDefaultPrevented()) return

        this.isShown = true

        this.escape()

        this.backdrop(function () {
          var transition = $.support.transition && that.$element.hasClass('fade')

          if (!that.$element.parent().length) {
            that.$element.appendTo(document.body) //don't move modals dom position
          }

          that.$element.show()

          if (transition) {
            that.$element[0].offsetWidth // force reflow
          }

          that.$element
            .addClass('in')
            .attr('aria-hidden', false)

          that.enforceFocus()

          transition ?
            that.$element.one($.support.transition.end, function () { that.$element.focus().trigger('shown') }) :
            that.$element.focus().trigger('shown')

        })
      }

    , hide: function (e) {
        e && e.preventDefault()

        var that = this

        e = $.Event('hide')

        this.$element.trigger(e)

        if (!this.isShown || e.isDefaultPrevented()) return

        this.isShown = false

        this.escape()

        $(document).off('focusin.modal')

        this.$element
          .removeClass('in')
          .attr('aria-hidden', true)

        $.support.transition && this.$element.hasClass('fade') ?
          this.hideWithTransition() :
          this.hideModal()
      }

    , enforceFocus: function () {
        var that = this
        $(document).on('focusin.modal', function (e) {
          if (that.$element[0] !== e.target && !that.$element.has(e.target).length) {
            that.$element.focus()
          }
        })
      }

    , escape: function () {
        var that = this
        if (this.isShown && this.options.keyboard) {
          this.$element.on('keyup.dismiss.modal', function ( e ) {
            e.which == 27 && that.hide()
          })
        } else if (!this.isShown) {
          this.$element.off('keyup.dismiss.modal')
        }
      }

    , hideWithTransition: function () {
        var that = this
          , timeout = setTimeout(function () {
              that.$element.off($.support.transition.end)
              that.hideModal()
            }, 500)

        this.$element.one($.support.transition.end, function () {
          clearTimeout(timeout)
          that.hideModal()
        })
      }

    , hideModal: function () {
        var that = this
        this.$element.hide()
        this.backdrop(function () {
          that.removeBackdrop()
          that.$element.trigger('hidden')
        })
      }

    , removeBackdrop: function () {
        this.$backdrop && this.$backdrop.remove()
        this.$backdrop = null
      }

    , backdrop: function (callback) {
        var that = this
          , animate = this.$element.hasClass('fade') ? 'fade' : ''

        if (this.isShown && this.options.backdrop) {
          var doAnimate = $.support.transition && animate

          this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />')
            .appendTo(document.body)

          this.$backdrop.click(
            this.options.backdrop == 'static' ?
              $.proxy(this.$element[0].focus, this.$element[0])
            : $.proxy(this.hide, this)
          )

          if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

          this.$backdrop.addClass('in')

          if (!callback) return

          doAnimate ?
            this.$backdrop.one($.support.transition.end, callback) :
            callback()

        } else if (!this.isShown && this.$backdrop) {
          this.$backdrop.removeClass('in')

          $.support.transition && this.$element.hasClass('fade')?
            this.$backdrop.one($.support.transition.end, callback) :
            callback()

        } else if (callback) {
          callback()
        }
      }
  }


 /* MODAL PLUGIN DEFINITION
  * ======================= */

  var old = $.fn.modal

  $.fn.modal = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('modal')
        , options = $.extend({}, $.fn.modal.defaults, $this.data(), typeof option == 'object' && option)
      if (!data) $this.data('modal', (data = new Modal(this, options)))
      if (typeof option == 'string') data[option]()
      else if (options.show) data.show()
    })
  }

  $.fn.modal.defaults = {
      backdrop: true
    , keyboard: true
    , show: true
  }

  $.fn.modal.Constructor = Modal


 /* MODAL NO CONFLICT
  * ================= */

  $.fn.modal.noConflict = function () {
    $.fn.modal = old
    return this
  }


 /* MODAL DATA-API
  * ============== */

  $(document).on('click.modal.data-api', '[data-toggle="modal"]', function (e) {
    var $this = $(this)
      , href = $this.attr('href')
      , $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) //strip for ie7
      , option = $target.data('modal') ? 'toggle' : $.extend({ remote:!/#/.test(href) && href }, $target.data(), $this.data())

    e.preventDefault()

    $target
      .modal(option)
      .one('hide', function () {
        $this.focus()
      })
  })

}(window.jQuery);

/* ============================================================
 * bootstrap-dropdown.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#dropdowns
 * ============================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function ($) {

  "use strict"; // jshint ;_;


 /* DROPDOWN CLASS DEFINITION
  * ========================= */

  var toggle = '[data-toggle=dropdown]'
    , Dropdown = function (element) {
        var $el = $(element).on('click.dropdown.data-api', this.toggle)
        $('html').on('click.dropdown.data-api', function () {
          $el.parent().removeClass('open')
        })
      }

  Dropdown.prototype = {

    constructor: Dropdown

  , toggle: function (e) {
      var $this = $(this)
        , $parent
        , isActive

      if ($this.is('.disabled, :disabled')) return

      $parent = getParent($this)

      isActive = $parent.hasClass('open')

      clearMenus()

      if (!isActive) {
        if ('ontouchstart' in document.documentElement) {
          // if mobile we we use a backdrop because click events don't delegate
          $('<div class="dropdown-backdrop"/>').insertBefore($(this)).on('click', clearMenus)
        }
        $parent.toggleClass('open')
      }

      $this.focus()

      return false
    }

  , keydown: function (e) {
      var $this
        , $items
        , $active
        , $parent
        , isActive
        , index

      if (!/(38|40|27)/.test(e.keyCode)) return

      $this = $(this)

      e.preventDefault()
      e.stopPropagation()

      if ($this.is('.disabled, :disabled')) return

      $parent = getParent($this)

      isActive = $parent.hasClass('open')

      if (!isActive || (isActive && e.keyCode == 27)) {
        if (e.which == 27) $parent.find(toggle).focus()
        return $this.click()
      }

      $items = $('[role=menu] li:not(.divider):visible a', $parent)

      if (!$items.length) return

      index = $items.index($items.filter(':focus'))

      if (e.keyCode == 38 && index > 0) index--                                        // up
      if (e.keyCode == 40 && index < $items.length - 1) index++                        // down
      if (!~index) index = 0

      $items
        .eq(index)
        .focus()
    }

  }

  function clearMenus() {
    $('.dropdown-backdrop').remove()
    $(toggle).each(function () {
      getParent($(this)).removeClass('open')
    })
  }

  function getParent($this) {
    var selector = $this.attr('data-target')
      , $parent

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && /#/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
    }

    $parent = selector && $(selector)

    if (!$parent || !$parent.length) $parent = $this.parent()

    return $parent
  }


  /* DROPDOWN PLUGIN DEFINITION
   * ========================== */

  var old = $.fn.dropdown

  $.fn.dropdown = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('dropdown')
      if (!data) $this.data('dropdown', (data = new Dropdown(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  $.fn.dropdown.Constructor = Dropdown


 /* DROPDOWN NO CONFLICT
  * ==================== */

  $.fn.dropdown.noConflict = function () {
    $.fn.dropdown = old
    return this
  }


  /* APPLY TO STANDARD DROPDOWN ELEMENTS
   * =================================== */

  $(document)
    .on('click.dropdown.data-api', clearMenus)
    .on('click.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
    .on('click.dropdown.data-api'  , toggle, Dropdown.prototype.toggle)
    .on('keydown.dropdown.data-api', toggle + ', [role=menu]' , Dropdown.prototype.keydown)

}(window.jQuery);

/* =============================================================
 * bootstrap-scrollspy.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#scrollspy
 * =============================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* SCROLLSPY CLASS DEFINITION
  * ========================== */

  function ScrollSpy(element, options) {
    var process = $.proxy(this.process, this)
      , $element = $(element).is('body') ? $(window) : $(element)
      , href
    this.options = $.extend({}, $.fn.scrollspy.defaults, options)
    this.$scrollElement = $element.on('scroll.scroll-spy.data-api', process)
    this.selector = (this.options.target
      || ((href = $(element).attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) //strip for ie7
      || '') + ' .nav li > a'
    this.$body = $('body')
    this.refresh()
    this.process()
  }

  ScrollSpy.prototype = {

      constructor: ScrollSpy

    , refresh: function () {
        var self = this
          , $targets

        this.offsets = $([])
        this.targets = $([])

        $targets = this.$body
          .find(this.selector)
          .map(function () {
            var $el = $(this)
              , href = $el.data('target') || $el.attr('href')
              , $href = /^#\w/.test(href) && $(href)
            return ( $href
              && $href.length
              && [[ $href.position().top + (!$.isWindow(self.$scrollElement.get(0)) && self.$scrollElement.scrollTop()), href ]] ) || null
          })
          .sort(function (a, b) { return a[0] - b[0] })
          .each(function () {
            self.offsets.push(this[0])
            self.targets.push(this[1])
          })
      }

    , process: function () {
        var scrollTop = this.$scrollElement.scrollTop() + this.options.offset
          , scrollHeight = this.$scrollElement[0].scrollHeight || this.$body[0].scrollHeight
          , maxScroll = scrollHeight - this.$scrollElement.height()
          , offsets = this.offsets
          , targets = this.targets
          , activeTarget = this.activeTarget
          , i

        if (scrollTop >= maxScroll) {
          return activeTarget != (i = targets.last()[0])
            && this.activate ( i )
        }

        for (i = offsets.length; i--;) {
          activeTarget != targets[i]
            && scrollTop >= offsets[i]
            && (!offsets[i + 1] || scrollTop <= offsets[i + 1])
            && this.activate( targets[i] )
        }
      }

    , activate: function (target) {
        var active
          , selector

        this.activeTarget = target

        $(this.selector)
          .parent('.active')
          .removeClass('active')

        selector = this.selector
          + '[data-target="' + target + '"],'
          + this.selector + '[href="' + target + '"]'

        active = $(selector)
          .parent('li')
          .addClass('active')

        if (active.parent('.dropdown-menu').length)  {
          active = active.closest('li.dropdown').addClass('active')
        }

        active.trigger('activate')
      }

  }


 /* SCROLLSPY PLUGIN DEFINITION
  * =========================== */

  var old = $.fn.scrollspy

  $.fn.scrollspy = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('scrollspy')
        , options = typeof option == 'object' && option
      if (!data) $this.data('scrollspy', (data = new ScrollSpy(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.scrollspy.Constructor = ScrollSpy

  $.fn.scrollspy.defaults = {
    offset: 10
  }


 /* SCROLLSPY NO CONFLICT
  * ===================== */

  $.fn.scrollspy.noConflict = function () {
    $.fn.scrollspy = old
    return this
  }


 /* SCROLLSPY DATA-API
  * ================== */

  $(window).on('load', function () {
    $('[data-spy="scroll"]').each(function () {
      var $spy = $(this)
      $spy.scrollspy($spy.data())
    })
  })

}(window.jQuery);
/* ========================================================
 * bootstrap-tab.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#tabs
 * ========================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ======================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* TAB CLASS DEFINITION
  * ==================== */

  var Tab = function (element) {
    this.element = $(element)
  }

  Tab.prototype = {

    constructor: Tab

  , show: function () {
      var $this = this.element
        , $ul = $this.closest('ul:not(.dropdown-menu)')
        , selector = $this.attr('data-target')
        , previous
        , $target
        , e

      if (!selector) {
        selector = $this.attr('href')
        selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
      }

      if ( $this.parent('li').hasClass('active') ) return

      previous = $ul.find('.active:last a')[0]

      e = $.Event('show', {
        relatedTarget: previous
      })

      $this.trigger(e)

      if (e.isDefaultPrevented()) return

      $target = $(selector)

      this.activate($this.parent('li'), $ul)
      this.activate($target, $target.parent(), function () {
        $this.trigger({
          type: 'shown'
        , relatedTarget: previous
        })
      })
    }

  , activate: function ( element, container, callback) {
      var $active = container.find('> .active')
        , transition = callback
            && $.support.transition
            && $active.hasClass('fade')

      function next() {
        $active
          .removeClass('active')
          .find('> .dropdown-menu > .active')
          .removeClass('active')

        element.addClass('active')

        if (transition) {
          element[0].offsetWidth // reflow for transition
          element.addClass('in')
        } else {
          element.removeClass('fade')
        }

        if ( element.parent('.dropdown-menu') ) {
          element.closest('li.dropdown').addClass('active')
        }

        callback && callback()
      }

      transition ?
        $active.one($.support.transition.end, next) :
        next()

      $active.removeClass('in')
    }
  }


 /* TAB PLUGIN DEFINITION
  * ===================== */

  var old = $.fn.tab

  $.fn.tab = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('tab')
      if (!data) $this.data('tab', (data = new Tab(this)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.tab.Constructor = Tab


 /* TAB NO CONFLICT
  * =============== */

  $.fn.tab.noConflict = function () {
    $.fn.tab = old
    return this
  }


 /* TAB DATA-API
  * ============ */

  $(document).on('click.tab.data-api', '[data-toggle="tab"], [data-toggle="pill"]', function (e) {
    e.preventDefault()
    $(this).tab('show')
  })

}(window.jQuery);
/* ===========================================================
 * bootstrap-tooltip.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#tooltips
 * Inspired by the original jQuery.tipsy by Jason Frame
 * ===========================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* TOOLTIP PUBLIC CLASS DEFINITION
  * =============================== */

  var Tooltip = function (element, options) {
    this.init('tooltip', element, options)
  }

  Tooltip.prototype = {

    constructor: Tooltip

  , init: function (type, element, options) {
      var eventIn
        , eventOut
        , triggers
        , trigger
        , i

      this.type = type
      this.$element = $(element)
      this.options = this.getOptions(options)
      this.enabled = true

      triggers = this.options.trigger.split(' ')

      for (i = triggers.length; i--;) {
        trigger = triggers[i]
        if (trigger == 'click') {
          this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
        } else if (trigger != 'manual') {
          eventIn = trigger == 'hover' ? 'mouseenter' : 'focus'
          eventOut = trigger == 'hover' ? 'mouseleave' : 'blur'
          this.$element.on(eventIn + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
          this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
        }
      }

      this.options.selector ?
        (this._options = $.extend({}, this.options, { trigger: 'manual', selector: '' })) :
        this.fixTitle()
    }

  , getOptions: function (options) {
      options = $.extend({}, $.fn[this.type].defaults, this.$element.data(), options)

      if (options.delay && typeof options.delay == 'number') {
        options.delay = {
          show: options.delay
        , hide: options.delay
        }
      }

      return options
    }

  , enter: function (e) {
      var defaults = $.fn[this.type].defaults
        , options = {}
        , self

      this._options && $.each(this._options, function (key, value) {
        if (defaults[key] != value) options[key] = value
      }, this)

      self = $(e.currentTarget)[this.type](options).data(this.type)

      if (!self.options.delay || !self.options.delay.show) return self.show()

      clearTimeout(this.timeout)
      self.hoverState = 'in'
      this.timeout = setTimeout(function() {
        if (self.hoverState == 'in') self.show()
      }, self.options.delay.show)
    }

  , leave: function (e) {
      var self = $(e.currentTarget)[this.type](this._options).data(this.type)

      if (this.timeout) clearTimeout(this.timeout)
      if (!self.options.delay || !self.options.delay.hide) return self.hide()

      self.hoverState = 'out'
      this.timeout = setTimeout(function() {
        if (self.hoverState == 'out') self.hide()
      }, self.options.delay.hide)
    }

  , show: function () {
      var $tip
        , pos
        , actualWidth
        , actualHeight
        , placement
        , tp
        , e = $.Event('show')

      if (this.hasContent() && this.enabled) {
        this.$element.trigger(e)
        if (e.isDefaultPrevented()) return
        $tip = this.tip()
        this.setContent()

        if (this.options.animation) {
          $tip.addClass('fade')
        }

        placement = typeof this.options.placement == 'function' ?
          this.options.placement.call(this, $tip[0], this.$element[0]) :
          this.options.placement

        $tip
          .detach()
          .css({ top: 0, left: 0, display: 'block' })

        this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element)

        pos = this.getPosition()

        actualWidth = $tip[0].offsetWidth
        actualHeight = $tip[0].offsetHeight

        switch (placement) {
          case 'bottom':
            tp = {top: pos.top + pos.height, left: pos.left + pos.width / 2 - actualWidth / 2}
            break
          case 'top':
            tp = {top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2}
            break
          case 'left':
            tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth}
            break
          case 'right':
            tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width}
            break
        }

        this.applyPlacement(tp, placement)
        this.$element.trigger('shown')
      }
    }

  , applyPlacement: function(offset, placement){
      var $tip = this.tip()
        , width = $tip[0].offsetWidth
        , height = $tip[0].offsetHeight
        , actualWidth
        , actualHeight
        , delta
        , replace

      $tip
        .offset(offset)
        .addClass(placement)
        .addClass('in')

      actualWidth = $tip[0].offsetWidth
      actualHeight = $tip[0].offsetHeight

      if (placement == 'top' && actualHeight != height) {
        offset.top = offset.top + height - actualHeight
        replace = true
      }

      if (placement == 'bottom' || placement == 'top') {
        delta = 0

        if (offset.left < 0){
          delta = offset.left * -2
          offset.left = 0
          $tip.offset(offset)
          actualWidth = $tip[0].offsetWidth
          actualHeight = $tip[0].offsetHeight
        }

        this.replaceArrow(delta - width + actualWidth, actualWidth, 'left')
      } else {
        this.replaceArrow(actualHeight - height, actualHeight, 'top')
      }

      if (replace) $tip.offset(offset)
    }

  , replaceArrow: function(delta, dimension, position){
      this
        .arrow()
        .css(position, delta ? (50 * (1 - delta / dimension) + "%") : '')
    }

  , setContent: function () {
      var $tip = this.tip()
        , title = this.getTitle()

      $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
      $tip.removeClass('fade in top bottom left right')
    }

  , hide: function () {
      var that = this
        , $tip = this.tip()
        , e = $.Event('hide')

      this.$element.trigger(e)
      if (e.isDefaultPrevented()) return

      $tip.removeClass('in')

      function removeWithAnimation() {
        var timeout = setTimeout(function () {
          $tip.off($.support.transition.end).detach()
        }, 500)

        $tip.one($.support.transition.end, function () {
          clearTimeout(timeout)
          $tip.detach()
        })
      }

      $.support.transition && this.$tip.hasClass('fade') ?
        removeWithAnimation() :
        $tip.detach()

      this.$element.trigger('hidden')

      return this
    }

  , fixTitle: function () {
      var $e = this.$element
      if ($e.attr('title') || typeof($e.attr('data-original-title')) != 'string') {
        $e.attr('data-original-title', $e.attr('title') || '').attr('title', '')
      }
    }

  , hasContent: function () {
      return this.getTitle()
    }

  , getPosition: function () {
      var el = this.$element[0]
      return $.extend({}, (typeof el.getBoundingClientRect == 'function') ? el.getBoundingClientRect() : {
        width: el.offsetWidth
      , height: el.offsetHeight
      }, this.$element.offset())
    }

  , getTitle: function () {
      var title
        , $e = this.$element
        , o = this.options

      title = $e.attr('data-original-title')
        || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title)

      return title
    }

  , tip: function () {
      return this.$tip = this.$tip || $(this.options.template)
    }

  , arrow: function(){
      return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
    }

  , validate: function () {
      if (!this.$element[0].parentNode) {
        this.hide()
        this.$element = null
        this.options = null
      }
    }

  , enable: function () {
      this.enabled = true
    }

  , disable: function () {
      this.enabled = false
    }

  , toggleEnabled: function () {
      this.enabled = !this.enabled
    }

  , toggle: function (e) {
      var self = e ? $(e.currentTarget)[this.type](this._options).data(this.type) : this
      self.tip().hasClass('in') ? self.hide() : self.show()
    }

  , destroy: function () {
      this.hide().$element.off('.' + this.type).removeData(this.type)
    }

  }


 /* TOOLTIP PLUGIN DEFINITION
  * ========================= */

  var old = $.fn.tooltip

  $.fn.tooltip = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('tooltip')
        , options = typeof option == 'object' && option
      if (!data) $this.data('tooltip', (data = new Tooltip(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.tooltip.Constructor = Tooltip

  $.fn.tooltip.defaults = {
    animation: true
  , placement: 'top'
  , selector: false
  , template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
  , trigger: 'hover focus'
  , title: ''
  , delay: 0
  , html: false
  , container: false
  }


 /* TOOLTIP NO CONFLICT
  * =================== */

  $.fn.tooltip.noConflict = function () {
    $.fn.tooltip = old
    return this
  }

}(window.jQuery);

/* ===========================================================
 * bootstrap-popover.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#popovers
 * ===========================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* POPOVER PUBLIC CLASS DEFINITION
  * =============================== */

  var Popover = function (element, options) {
    this.init('popover', element, options)
  }


  /* NOTE: POPOVER EXTENDS BOOTSTRAP-TOOLTIP.js
     ========================================== */

  Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype, {

    constructor: Popover

  , setContent: function () {
      var $tip = this.tip()
        , title = this.getTitle()
        , content = this.getContent()

      $tip.find('.popover-title')[this.options.html ? 'html' : 'text'](title)
      $tip.find('.popover-content')[this.options.html ? 'html' : 'text'](content)

      $tip.removeClass('fade top bottom left right in')
    }

  , hasContent: function () {
      return this.getTitle() || this.getContent()
    }

  , getContent: function () {
      var content
        , $e = this.$element
        , o = this.options

      content = (typeof o.content == 'function' ? o.content.call($e[0]) :  o.content)
        || $e.attr('data-content')

      return content
    }

  , tip: function () {
      if (!this.$tip) {
        this.$tip = $(this.options.template)
      }
      return this.$tip
    }

  , destroy: function () {
      this.hide().$element.off('.' + this.type).removeData(this.type)
    }

  })


 /* POPOVER PLUGIN DEFINITION
  * ======================= */

  var old = $.fn.popover

  $.fn.popover = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('popover')
        , options = typeof option == 'object' && option
      if (!data) $this.data('popover', (data = new Popover(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.popover.Constructor = Popover

  $.fn.popover.defaults = $.extend({} , $.fn.tooltip.defaults, {
    placement: 'right'
  , trigger: 'click'
  , content: ''
  , template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
  })


 /* POPOVER NO CONFLICT
  * =================== */

  $.fn.popover.noConflict = function () {
    $.fn.popover = old
    return this
  }

}(window.jQuery);

/* ==========================================================
 * bootstrap-affix.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#affix
 * ==========================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* AFFIX CLASS DEFINITION
  * ====================== */

  var Affix = function (element, options) {
    this.options = $.extend({}, $.fn.affix.defaults, options)
    this.$window = $(window)
      .on('scroll.affix.data-api', $.proxy(this.checkPosition, this))
      .on('click.affix.data-api',  $.proxy(function () { setTimeout($.proxy(this.checkPosition, this), 1) }, this))
    this.$element = $(element)
    this.checkPosition()
  }

  Affix.prototype.checkPosition = function () {
    if (!this.$element.is(':visible')) return

    var scrollHeight = $(document).height()
      , scrollTop = this.$window.scrollTop()
      , position = this.$element.offset()
      , offset = this.options.offset
      , offsetBottom = offset.bottom
      , offsetTop = offset.top
      , reset = 'affix affix-top affix-bottom'
      , affix

    if (typeof offset != 'object') offsetBottom = offsetTop = offset
    if (typeof offsetTop == 'function') offsetTop = offset.top()
    if (typeof offsetBottom == 'function') offsetBottom = offset.bottom()

    affix = this.unpin != null && (scrollTop + this.unpin <= position.top) ?
      false    : offsetBottom != null && (position.top + this.$element.height() >= scrollHeight - offsetBottom) ?
      'bottom' : offsetTop != null && scrollTop <= offsetTop ?
      'top'    : false

    if (this.affixed === affix) return

    this.affixed = affix
    this.unpin = affix == 'bottom' ? position.top - scrollTop : null

    this.$element.removeClass(reset).addClass('affix' + (affix ? '-' + affix : ''))
  }


 /* AFFIX PLUGIN DEFINITION
  * ======================= */

  var old = $.fn.affix

  $.fn.affix = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('affix')
        , options = typeof option == 'object' && option
      if (!data) $this.data('affix', (data = new Affix(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.affix.Constructor = Affix

  $.fn.affix.defaults = {
    offset: 0
  }


 /* AFFIX NO CONFLICT
  * ================= */

  $.fn.affix.noConflict = function () {
    $.fn.affix = old
    return this
  }


 /* AFFIX DATA-API
  * ============== */

  $(window).on('load', function () {
    $('[data-spy="affix"]').each(function () {
      var $spy = $(this)
        , data = $spy.data()

      data.offset = data.offset || {}

      data.offsetBottom && (data.offset.bottom = data.offsetBottom)
      data.offsetTop && (data.offset.top = data.offsetTop)

      $spy.affix(data)
    })
  })


}(window.jQuery);
/* ==========================================================
 * bootstrap-alert.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#alerts
 * ==========================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* ALERT CLASS DEFINITION
  * ====================== */

  var dismiss = '[data-dismiss="alert"]'
    , Alert = function (el) {
        $(el).on('click', dismiss, this.close)
      }

  Alert.prototype.close = function (e) {
    var $this = $(this)
      , selector = $this.attr('data-target')
      , $parent

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
    }

    $parent = $(selector)

    e && e.preventDefault()

    $parent.length || ($parent = $this.hasClass('alert') ? $this : $this.parent())

    $parent.trigger(e = $.Event('close'))

    if (e.isDefaultPrevented()) return

    $parent.removeClass('in')

    function removeElement() {
      $parent
        .trigger('closed')
        .remove()
    }

    $.support.transition && $parent.hasClass('fade') ?
      $parent.on($.support.transition.end, removeElement) :
      removeElement()
  }


 /* ALERT PLUGIN DEFINITION
  * ======================= */

  var old = $.fn.alert

  $.fn.alert = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('alert')
      if (!data) $this.data('alert', (data = new Alert(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  $.fn.alert.Constructor = Alert


 /* ALERT NO CONFLICT
  * ================= */

  $.fn.alert.noConflict = function () {
    $.fn.alert = old
    return this
  }


 /* ALERT DATA-API
  * ============== */

  $(document).on('click.alert.data-api', dismiss, Alert.prototype.close)

}(window.jQuery);
/* ============================================================
 * bootstrap-button.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#buttons
 * ============================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function ($) {

  "use strict"; // jshint ;_;


 /* BUTTON PUBLIC CLASS DEFINITION
  * ============================== */

  var Button = function (element, options) {
    this.$element = $(element)
    this.options = $.extend({}, $.fn.button.defaults, options)
  }

  Button.prototype.setState = function (state) {
    var d = 'disabled'
      , $el = this.$element
      , data = $el.data()
      , val = $el.is('input') ? 'val' : 'html'

    state = state + 'Text'
    data.resetText || $el.data('resetText', $el[val]())

    $el[val](data[state] || this.options[state])

    // push to event loop to allow forms to submit
    setTimeout(function () {
      state == 'loadingText' ?
        $el.addClass(d).attr(d, d) :
        $el.removeClass(d).removeAttr(d)
    }, 0)
  }

  Button.prototype.toggle = function () {
    var $parent = this.$element.closest('[data-toggle="buttons-radio"]')

    $parent && $parent
      .find('.active')
      .removeClass('active')

    this.$element.toggleClass('active')
  }


 /* BUTTON PLUGIN DEFINITION
  * ======================== */

  var old = $.fn.button

  $.fn.button = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('button')
        , options = typeof option == 'object' && option
      if (!data) $this.data('button', (data = new Button(this, options)))
      if (option == 'toggle') data.toggle()
      else if (option) data.setState(option)
    })
  }

  $.fn.button.defaults = {
    loadingText: 'loading...'
  }

  $.fn.button.Constructor = Button


 /* BUTTON NO CONFLICT
  * ================== */

  $.fn.button.noConflict = function () {
    $.fn.button = old
    return this
  }


 /* BUTTON DATA-API
  * =============== */

  $(document).on('click.button.data-api', '[data-toggle^=button]', function (e) {
    var $btn = $(e.target)
    if (!$btn.hasClass('btn')) $btn = $btn.closest('.btn')
    $btn.button('toggle')
  })

}(window.jQuery);
/* =============================================================
 * bootstrap-collapse.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#collapse
 * =============================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function ($) {

  "use strict"; // jshint ;_;


 /* COLLAPSE PUBLIC CLASS DEFINITION
  * ================================ */

  var Collapse = function (element, options) {
    this.$element = $(element)
    this.options = $.extend({}, $.fn.collapse.defaults, options)

    if (this.options.parent) {
      this.$parent = $(this.options.parent)
    }

    this.options.toggle && this.toggle()
  }

  Collapse.prototype = {

    constructor: Collapse

  , dimension: function () {
      var hasWidth = this.$element.hasClass('width')
      return hasWidth ? 'width' : 'height'
    }

  , show: function () {
      var dimension
        , scroll
        , actives
        , hasData

      if (this.transitioning || this.$element.hasClass('in')) return

      dimension = this.dimension()
      scroll = $.camelCase(['scroll', dimension].join('-'))
      actives = this.$parent && this.$parent.find('> .accordion-group > .in')

      if (actives && actives.length) {
        hasData = actives.data('collapse')
        if (hasData && hasData.transitioning) return
        actives.collapse('hide')
        hasData || actives.data('collapse', null)
      }

      this.$element[dimension](0)
      this.transition('addClass', $.Event('show'), 'shown')
      $.support.transition && this.$element[dimension](this.$element[0][scroll])
    }

  , hide: function () {
      var dimension
      if (this.transitioning || !this.$element.hasClass('in')) return
      dimension = this.dimension()
      this.reset(this.$element[dimension]())
      this.transition('removeClass', $.Event('hide'), 'hidden')
      this.$element[dimension](0)
    }

  , reset: function (size) {
      var dimension = this.dimension()

      this.$element
        .removeClass('collapse')
        [dimension](size || 'auto')
        [0].offsetWidth

      this.$element[size !== null ? 'addClass' : 'removeClass']('collapse')

      return this
    }

  , transition: function (method, startEvent, completeEvent) {
      var that = this
        , complete = function () {
            if (startEvent.type == 'show') that.reset()
            that.transitioning = 0
            that.$element.trigger(completeEvent)
          }

      this.$element.trigger(startEvent)

      if (startEvent.isDefaultPrevented()) return

      this.transitioning = 1

      this.$element[method]('in')

      $.support.transition && this.$element.hasClass('collapse') ?
        this.$element.one($.support.transition.end, complete) :
        complete()
    }

  , toggle: function () {
      this[this.$element.hasClass('in') ? 'hide' : 'show']()
    }

  }


 /* COLLAPSE PLUGIN DEFINITION
  * ========================== */

  var old = $.fn.collapse

  $.fn.collapse = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('collapse')
        , options = $.extend({}, $.fn.collapse.defaults, $this.data(), typeof option == 'object' && option)
      if (!data) $this.data('collapse', (data = new Collapse(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.collapse.defaults = {
    toggle: true
  }

  $.fn.collapse.Constructor = Collapse


 /* COLLAPSE NO CONFLICT
  * ==================== */

  $.fn.collapse.noConflict = function () {
    $.fn.collapse = old
    return this
  }


 /* COLLAPSE DATA-API
  * ================= */

  $(document).on('click.collapse.data-api', '[data-toggle=collapse]', function (e) {
    var $this = $(this), href
      , target = $this.attr('data-target')
        || e.preventDefault()
        || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') //strip for ie7
      , option = $(target).data('collapse') ? 'toggle' : $this.data()
    $this[$(target).hasClass('in') ? 'addClass' : 'removeClass']('collapsed')
    $(target).collapse(option)
  })

}(window.jQuery);
/* ==========================================================
 * bootstrap-carousel.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#carousel
 * ==========================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function ($) {

  "use strict"; // jshint ;_;


 /* CAROUSEL CLASS DEFINITION
  * ========================= */

  var Carousel = function (element, options) {
    this.$element = $(element)
    this.$indicators = this.$element.find('.carousel-indicators')
    this.options = options
    this.options.pause == 'hover' && this.$element
      .on('mouseenter', $.proxy(this.pause, this))
      .on('mouseleave', $.proxy(this.cycle, this))
  }

  Carousel.prototype = {

    cycle: function (e) {
      if (!e) this.paused = false
      if (this.interval) clearInterval(this.interval);
      this.options.interval
        && !this.paused
        && (this.interval = setInterval($.proxy(this.next, this), this.options.interval))
      return this
    }

  , getActiveIndex: function () {
      this.$active = this.$element.find('.item.active')
      this.$items = this.$active.parent().children()
      return this.$items.index(this.$active)
    }

  , to: function (pos) {
      var activeIndex = this.getActiveIndex()
        , that = this

      if (pos > (this.$items.length - 1) || pos < 0) return

      if (this.sliding) {
        return this.$element.one('slid', function () {
          that.to(pos)
        })
      }

      if (activeIndex == pos) {
        return this.pause().cycle()
      }

      return this.slide(pos > activeIndex ? 'next' : 'prev', $(this.$items[pos]))
    }

  , pause: function (e) {
      if (!e) this.paused = true
      if (this.$element.find('.next, .prev').length && $.support.transition.end) {
        this.$element.trigger($.support.transition.end)
        this.cycle(true)
      }
      clearInterval(this.interval)
      this.interval = null
      return this
    }

  , next: function () {
      if (this.sliding) return
      return this.slide('next')
    }

  , prev: function () {
      if (this.sliding) return
      return this.slide('prev')
    }

  , slide: function (type, next) {
      var $active = this.$element.find('.item.active')
        , $next = next || $active[type]()
        , isCycling = this.interval
        , direction = type == 'next' ? 'left' : 'right'
        , fallback  = type == 'next' ? 'first' : 'last'
        , that = this
        , e

      this.sliding = true

      isCycling && this.pause()

      $next = $next.length ? $next : this.$element.find('.item')[fallback]()

      e = $.Event('slide', {
        relatedTarget: $next[0]
      , direction: direction
      })

      if ($next.hasClass('active')) return

      if (this.$indicators.length) {
        this.$indicators.find('.active').removeClass('active')
        this.$element.one('slid', function () {
          var $nextIndicator = $(that.$indicators.children()[that.getActiveIndex()])
          $nextIndicator && $nextIndicator.addClass('active')
        })
      }

      if ($.support.transition && this.$element.hasClass('slide')) {
        this.$element.trigger(e)
        if (e.isDefaultPrevented()) return
        $next.addClass(type)
        $next[0].offsetWidth // force reflow
        $active.addClass(direction)
        $next.addClass(direction)
        this.$element.one($.support.transition.end, function () {
          $next.removeClass([type, direction].join(' ')).addClass('active')
          $active.removeClass(['active', direction].join(' '))
          that.sliding = false
          setTimeout(function () { that.$element.trigger('slid') }, 0)
        })
      } else {
        this.$element.trigger(e)
        if (e.isDefaultPrevented()) return
        $active.removeClass('active')
        $next.addClass('active')
        this.sliding = false
        this.$element.trigger('slid')
      }

      isCycling && this.cycle()

      return this
    }

  }


 /* CAROUSEL PLUGIN DEFINITION
  * ========================== */

  var old = $.fn.carousel

  $.fn.carousel = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('carousel')
        , options = $.extend({}, $.fn.carousel.defaults, typeof option == 'object' && option)
        , action = typeof option == 'string' ? option : options.slide
      if (!data) $this.data('carousel', (data = new Carousel(this, options)))
      if (typeof option == 'number') data.to(option)
      else if (action) data[action]()
      else if (options.interval) data.pause().cycle()
    })
  }

  $.fn.carousel.defaults = {
    interval: 5000
  , pause: 'hover'
  }

  $.fn.carousel.Constructor = Carousel


 /* CAROUSEL NO CONFLICT
  * ==================== */

  $.fn.carousel.noConflict = function () {
    $.fn.carousel = old
    return this
  }

 /* CAROUSEL DATA-API
  * ================= */

  $(document).on('click.carousel.data-api', '[data-slide], [data-slide-to]', function (e) {
    var $this = $(this), href
      , $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) //strip for ie7
      , options = $.extend({}, $target.data(), $this.data())
      , slideIndex

    $target.carousel(options)

    if (slideIndex = $this.attr('data-slide-to')) {
      $target.data('carousel').pause().to(slideIndex).cycle()
    }

    e.preventDefault()
  })

}(window.jQuery);
/* =============================================================
 * bootstrap-typeahead.js v2.3.2
 * http://getbootstrap.com/2.3.2/javascript.html#typeahead
 * =============================================================
 * Copyright 2013 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function($){

  "use strict"; // jshint ;_;


 /* TYPEAHEAD PUBLIC CLASS DEFINITION
  * ================================= */

  var Typeahead = function (element, options) {
    this.$element = $(element)
    this.options = $.extend({}, $.fn.typeahead.defaults, options)
    this.matcher = this.options.matcher || this.matcher
    this.sorter = this.options.sorter || this.sorter
    this.highlighter = this.options.highlighter || this.highlighter
    this.updater = this.options.updater || this.updater
    this.source = this.options.source
    this.$menu = $(this.options.menu)
    this.shown = false
    this.listen()
  }

  Typeahead.prototype = {

    constructor: Typeahead

  , select: function () {
      var val = this.$menu.find('.active').attr('data-value')
      this.$element
        .val(this.updater(val))
        .change()
      return this.hide()
    }

  , updater: function (item) {
      return item
    }

  , show: function () {
      var pos = $.extend({}, this.$element.position(), {
        height: this.$element[0].offsetHeight
      })

      this.$menu
        .insertAfter(this.$element)
        .css({
          top: pos.top + pos.height
        , left: pos.left
        })
        .show()

      this.shown = true
      return this
    }

  , hide: function () {
      this.$menu.hide()
      this.shown = false
      return this
    }

  , lookup: function (event) {
      var items

      this.query = this.$element.val()

      if (!this.query || this.query.length < this.options.minLength) {
        return this.shown ? this.hide() : this
      }

      items = $.isFunction(this.source) ? this.source(this.query, $.proxy(this.process, this)) : this.source

      return items ? this.process(items) : this
    }

  , process: function (items) {
      var that = this

      items = $.grep(items, function (item) {
        return that.matcher(item)
      })

      items = this.sorter(items)

      if (!items.length) {
        return this.shown ? this.hide() : this
      }

      return this.render(items.slice(0, this.options.items)).show()
    }

  , matcher: function (item) {
      return ~item.toLowerCase().indexOf(this.query.toLowerCase())
    }

  , sorter: function (items) {
      var beginswith = []
        , caseSensitive = []
        , caseInsensitive = []
        , item

      while (item = items.shift()) {
        if (!item.toLowerCase().indexOf(this.query.toLowerCase())) beginswith.push(item)
        else if (~item.indexOf(this.query)) caseSensitive.push(item)
        else caseInsensitive.push(item)
      }

      return beginswith.concat(caseSensitive, caseInsensitive)
    }

  , highlighter: function (item) {
      var query = this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
      return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
        return '<strong>' + match + '</strong>'
      })
    }

  , render: function (items) {
      var that = this

      items = $(items).map(function (i, item) {
        i = $(that.options.item).attr('data-value', item)
        i.find('a').html(that.highlighter(item))
        return i[0]
      })

      items.first().addClass('active')
      this.$menu.html(items)
      return this
    }

  , next: function (event) {
      var active = this.$menu.find('.active').removeClass('active')
        , next = active.next()

      if (!next.length) {
        next = $(this.$menu.find('li')[0])
      }

      next.addClass('active')
    }

  , prev: function (event) {
      var active = this.$menu.find('.active').removeClass('active')
        , prev = active.prev()

      if (!prev.length) {
        prev = this.$menu.find('li').last()
      }

      prev.addClass('active')
    }

  , listen: function () {
      this.$element
        .on('focus',    $.proxy(this.focus, this))
        .on('blur',     $.proxy(this.blur, this))
        .on('keypress', $.proxy(this.keypress, this))
        .on('keyup',    $.proxy(this.keyup, this))

      if (this.eventSupported('keydown')) {
        this.$element.on('keydown', $.proxy(this.keydown, this))
      }

      this.$menu
        .on('click', $.proxy(this.click, this))
        .on('mouseenter', 'li', $.proxy(this.mouseenter, this))
        .on('mouseleave', 'li', $.proxy(this.mouseleave, this))
    }

  , eventSupported: function(eventName) {
      var isSupported = eventName in this.$element
      if (!isSupported) {
        this.$element.setAttribute(eventName, 'return;')
        isSupported = typeof this.$element[eventName] === 'function'
      }
      return isSupported
    }

  , move: function (e) {
      if (!this.shown) return

      switch(e.keyCode) {
        case 9: // tab
        case 13: // enter
        case 27: // escape
          e.preventDefault()
          break

        case 38: // up arrow
          e.preventDefault()
          this.prev()
          break

        case 40: // down arrow
          e.preventDefault()
          this.next()
          break
      }

      e.stopPropagation()
    }

  , keydown: function (e) {
      this.suppressKeyPressRepeat = ~$.inArray(e.keyCode, [40,38,9,13,27])
      this.move(e)
    }

  , keypress: function (e) {
      if (this.suppressKeyPressRepeat) return
      this.move(e)
    }

  , keyup: function (e) {
      switch(e.keyCode) {
        case 40: // down arrow
        case 38: // up arrow
        case 16: // shift
        case 17: // ctrl
        case 18: // alt
          break

        case 9: // tab
        case 13: // enter
          if (!this.shown) return
          this.select()
          break

        case 27: // escape
          if (!this.shown) return
          this.hide()
          break

        default:
          this.lookup()
      }

      e.stopPropagation()
      e.preventDefault()
  }

  , focus: function (e) {
      this.focused = true
    }

  , blur: function (e) {
      this.focused = false
      if (!this.mousedover && this.shown) this.hide()
    }

  , click: function (e) {
      e.stopPropagation()
      e.preventDefault()
      this.select()
      this.$element.focus()
    }

  , mouseenter: function (e) {
      this.mousedover = true
      this.$menu.find('.active').removeClass('active')
      $(e.currentTarget).addClass('active')
    }

  , mouseleave: function (e) {
      this.mousedover = false
      if (!this.focused && this.shown) this.hide()
    }

  }


  /* TYPEAHEAD PLUGIN DEFINITION
   * =========================== */

  var old = $.fn.typeahead

  $.fn.typeahead = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('typeahead')
        , options = typeof option == 'object' && option
      if (!data) $this.data('typeahead', (data = new Typeahead(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.typeahead.defaults = {
    source: []
  , items: 8
  , menu: '<ul class="typeahead dropdown-menu"></ul>'
  , item: '<li><a href="#"></a></li>'
  , minLength: 1
  }

  $.fn.typeahead.Constructor = Typeahead


 /* TYPEAHEAD NO CONFLICT
  * =================== */

  $.fn.typeahead.noConflict = function () {
    $.fn.typeahead = old
    return this
  }


 /* TYPEAHEAD DATA-API
  * ================== */

  $(document).on('focus.typeahead.data-api', '[data-provide="typeahead"]', function (e) {
    var $this = $(this)
    if ($this.data('typeahead')) return
    $this.typeahead($this.data())
  })

}(window.jQuery);
;/**/
var $ = jQuery;
(function($){
	
	//closeDOMWindow
	$.fn.closeDOMWindow = function(settings){
		
		if(!settings){settings={};}
		
		var run = function(passingThis){
			
			if(settings.anchoredClassName){
				var $anchorClassName = $('.'+settings.anchoredClassName);
				$anchorClassName.fadeOut('fast',function(){
					if($.fn.draggable){
						$anchorClassName.draggable('destory').trigger("unload").remove();	
					}else{
						$anchorClassName.trigger("unload").remove();
					}
				});
				if(settings.functionCallOnClose){settings.functionCallAfterClose();}
			}else{
				var $DOMWindowOverlay = $('#DOMWindowOverlay');
				var $DOMWindow = $('#DOMWindow');
				$DOMWindowOverlay.fadeOut('fast',function(){
					$DOMWindowOverlay.trigger('unload').unbind().remove();																	  
				});
				$DOMWindow.fadeOut('fast',function(){
					if($.fn.draggable){
						$DOMWindow.draggable("destroy").trigger("unload").remove();
					}else{
						$DOMWindow.trigger("unload").remove();
					}
				});
			
				$(window).unbind('scroll.DOMWindow');
				$(window).unbind('resize.DOMWindow');
				
				if($.fn.openDOMWindow.isIE6){$('#DOMWindowIE6FixIframe').remove();}
				if(settings.functionCallOnClose){settings.functionCallAfterClose();}
			}	
		};
		
		if(settings.eventType){//if used with $().
			return this.each(function(index){
				$(this).bind(settings.eventType, function(){
					run(this);
					return false;
				});
			});
		}else{//else called as $.function
			run();
		}
		
	};
	
	//allow for public call, pass settings
	$.closeDOMWindow = function(s){$.fn.closeDOMWindow(s);};
	
	//openDOMWindow
	$.fn.openDOMWindow = function(instanceSettings){	
		
		var shortcut =  $.fn.openDOMWindow;
	
		//default settings combined with callerSettings////////////////////////////////////////////////////////////////////////
		
		shortcut.defaultsSettings = {
			anchoredClassName:'',
			anchoredSelector:'',
			borderColor:'#ccc',
			borderSize:'4',
			draggable:0,
			eventType:null, //click, blur, change, dblclick, error, focus, load, mousedown, mouseout, mouseup etc...
			fixedWindowY:100,
			functionCallOnOpen:null,
			functionCallOnClose:null,
			height:500,
			loader:0,
			loaderHeight:0,
			loaderImagePath:'',
			loaderWidth:0,
			modal:0,
			overlay:1,
			overlayColor:'#000',
			overlayOpacity:'85',
			positionLeft:0,
			positionTop:0,
			positionType:'centered', // centered, anchored, absolute, fixed
			width:500, 
			windowBGColor:'#fff',
			windowBGImage:null, // http path
			windowHTTPType:'get',
			windowPadding:10,
			windowSource:'inline', //inline, ajax, iframe
			windowSourceID:'',
			windowSourceURL:'',
			windowSourceAttrURL:'href'
		};
		
		var settings = $.extend({}, $.fn.openDOMWindow.defaultsSettings , instanceSettings || {});
		
		//Public functions
		
		shortcut.viewPortHeight = function(){ return self.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;};
		shortcut.viewPortWidth = function(){ return self.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;};
		shortcut.scrollOffsetHeight = function(){ return self.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;};
		shortcut.scrollOffsetWidth = function(){ return self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft;};
		shortcut.isIE6 = typeof document.body.style.maxHeight === "undefined";
		
		//Private Functions/////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		var sizeOverlay = function(){
			var $DOMWindowOverlay = $('#DOMWindowOverlay');
			if(shortcut.isIE6){//if IE 6
				var overlayViewportHeight = document.documentElement.offsetHeight + document.documentElement.scrollTop - 4;
				var overlayViewportWidth = document.documentElement.offsetWidth - 21;
				$DOMWindowOverlay.css({'height':overlayViewportHeight +'px','width':overlayViewportWidth+'px'});
			}else{//else Firefox, safari, opera, IE 7+
				$DOMWindowOverlay.css({'height':'100%','width':'100%','position':'fixed'});
			}	
		};
		
		var sizeIE6Iframe = function(){
			var overlayViewportHeight = document.documentElement.offsetHeight + document.documentElement.scrollTop - 4;
			var overlayViewportWidth = document.documentElement.offsetWidth - 21;
			$('#DOMWindowIE6FixIframe').css({'height':overlayViewportHeight +'px','width':overlayViewportWidth+'px'});
		};
		
		var centerDOMWindow = function() {
			var $DOMWindow = $('#DOMWindow');
			if(settings.height + 50 > shortcut.viewPortHeight()){//added 50 to be safe
				$DOMWindow.css('left',Math.round(shortcut.viewPortWidth()/2) + shortcut.scrollOffsetWidth() - Math.round(($DOMWindow.outerWidth())/2));
			}else{
				$DOMWindow.css('left',Math.round(shortcut.viewPortWidth()/2) + shortcut.scrollOffsetWidth() - Math.round(($DOMWindow.outerWidth())/2));
				$DOMWindow.css('top',Math.round(shortcut.viewPortHeight()/2) + shortcut.scrollOffsetHeight() - Math.round(($DOMWindow.outerHeight())/2));
			}
		};
		
		var centerLoader = function() {
			var $DOMWindowLoader = $('#DOMWindowLoader');
			if(shortcut.isIE6){//if IE 6
				$DOMWindowLoader.css({'left':Math.round(shortcut.viewPortWidth()/2) + shortcut.scrollOffsetWidth() - Math.round(($DOMWindowLoader.innerWidth())/2),'position':'absolute'});
				$DOMWindowLoader.css({'top':Math.round(shortcut.viewPortHeight()/2) + shortcut.scrollOffsetHeight() - Math.round(($DOMWindowLoader.innerHeight())/2),'position':'absolute'});
			}else{
				$DOMWindowLoader.css({'left':'50%','top':'50%','position':'fixed'});
			}
			
		};
		
		var fixedDOMWindow = function(){
			var $DOMWindow = $('#DOMWindow');
			$DOMWindow.css('left', settings.positionLeft + shortcut.scrollOffsetWidth());
			$DOMWindow.css('top', + settings.positionTop + shortcut.scrollOffsetHeight());
		};
		
		var showDOMWindow = function(instance){
			if(arguments[0]){
				$('.'+instance+' #DOMWindowLoader').remove();
				$('.'+instance+' #DOMWindowContent').fadeIn('fast',function(){if(settings.functionCallOnOpen){settings.functionCallOnOpen();}});
				$('.'+instance+ '.closeDOMWindow').click(function(){
					$.closeDOMWindow();	
					return false;
				});
			}else{
				$('#DOMWindowLoader').remove();
				$('#DOMWindow').fadeIn('fast',function(){if(settings.functionCallOnOpen){settings.functionCallOnOpen();}});
				$('#DOMWindow .closeDOMWindow').click(function(){						
					$.closeDOMWindow();
					return false;
				});
			}
			
		};
		
		var urlQueryToObject = function(s){
			  var query = {};
			  s.replace(/b([^&=]*)=([^&=]*)b/g, function (m, a, d) {
				if (typeof query[a] != 'undefined') {
				  query[a] += ',' + d;
				} else {
				  query[a] = d;
				}
			  });
			  return query;
		};
			
		//Run Routine ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		var run = function(passingThis){
			
			//get values from element clicked, or assume its passed as an option
			settings.windowSourceID = $(passingThis).attr('href') || settings.windowSourceID;
			settings.windowSourceURL = $(passingThis).attr(settings.windowSourceAttrURL) || settings.windowSourceURL;
			settings.windowBGImage = settings.windowBGImage ? 'background-image:url('+settings.windowBGImage+')' : '';
			var urlOnly, urlQueryObject;
			
			if(settings.positionType == 'anchored'){//anchored DOM window
				
				var anchoredPositions = $(settings.anchoredSelector).position();
				var anchoredPositionX = anchoredPositions.left + settings.positionLeft;
				var anchoredPositionY = anchoredPositions.top + settings.positionTop;
				
				$('body').append('<div class="'+settings.anchoredClassName+'" style="'+settings.windowBGImage+';background-repeat:no-repeat;padding:'+settings.windowPadding+'px;overflow:auto;position:absolute;top:'+anchoredPositionY+'px;left:'+anchoredPositionX+'px;height:'+settings.height+'px;width:'+settings.width+'px;background-color:'+settings.windowBGColor+';border:'+settings.borderSize+'px solid '+settings.borderColor+';z-index:10001"><div id="DOMWindowContent" style="display:none"></div></div>');		
				//loader
				if(settings.loader && settings.loaderImagePath !== ''){
					$('.'+settings.anchoredClassName).append('<div id="DOMWindowLoader" style="width:'+settings.loaderWidth+'px;height:'+settings.loaderHeight+'px;"><img src="'+settings.loaderImagePath+'" /></div>');
					
				}

				if($.fn.draggable){
					if(settings.draggable){$('.' + settings.anchoredClassName).draggable({cursor:'move'});}
				}
				
				switch(settings.windowSource){
					case 'inline'://////////////////////////////// inline //////////////////////////////////////////
						$('.' + settings.anchoredClassName+" #DOMWindowContent").append($(settings.windowSourceID).children());
						$('.' + settings.anchoredClassName).unload(function(){// move elements back when you're finished
							$('.' + settings.windowSourceID).append( $('.' + settings.anchoredClassName+" #DOMWindowContent").children());				
						});
						showDOMWindow(settings.anchoredClassName);
					break;
					case 'iframe'://////////////////////////////// iframe //////////////////////////////////////////
						$('.' + settings.anchoredClassName+" #DOMWindowContent").append('<iframe frameborder="0" hspace="0" wspace="0" src="'+settings.windowSourceURL+'" name="DOMWindowIframe'+Math.round(Math.random()*1000)+'" style="width:100%;height:100%;border:none;background-color:#fff;" class="'+settings.anchoredClassName+'Iframe" ></iframe>');
						$('.'+settings.anchoredClassName+'Iframe').load(showDOMWindow(settings.anchoredClassName));
					break;
					case 'ajax'://////////////////////////////// ajax //////////////////////////////////////////	
						if(settings.windowHTTPType == 'post'){
							
							if(settings.windowSourceURL.indexOf("?") !== -1){//has a query string
								urlOnly = settings.windowSourceURL.substr(0, settings.windowSourceURL.indexOf("?"));
								urlQueryObject = urlQueryToObject(settings.windowSourceURL);
							}else{
								urlOnly = settings.windowSourceURL;
								urlQueryObject = {};
							}
							$('.' + settings.anchoredClassName+" #DOMWindowContent").load(urlOnly,urlQueryObject,function(){
								showDOMWindow(settings.anchoredClassName);
							});
						}else{
							if(settings.windowSourceURL.indexOf("?") == -1){ //no query string, so add one
								settings.windowSourceURL += '?';
							}
							$('.' + settings.anchoredClassName+" #DOMWindowContent").load(
								settings.windowSourceURL + '&random=' + (new Date().getTime()),function(){
								showDOMWindow(settings.anchoredClassName);
							});
						}
					break;
				}
				
			}else{//centered, fixed, absolute DOM window
				
				//overlay & modal
				if(settings.overlay){
					$('body').append('<div id="DOMWindowOverlay" style="z-index:10000;display:none;position:absolute;top:0;left:0;background-color:'+settings.overlayColor+';filter:alpha(opacity='+settings.overlayOpacity+');-moz-opacity: 0.'+settings.overlayOpacity+';opacity: 0.'+settings.overlayOpacity+';"></div>');
					if(shortcut.isIE6){//if IE 6
						$('body').append('<iframe id="DOMWindowIE6FixIframe"  src="http://www.appliedmaterials.com/files/advagg_js/blank.html"  style="width:100%;height:100%;z-index:9999;position:absolute;top:0;left:0;filter:alpha(opacity=0);"></iframe>');
						sizeIE6Iframe();
					}
					sizeOverlay();
					var $DOMWindowOverlay = $('#DOMWindowOverlay');
					$DOMWindowOverlay.fadeIn('fast');
					if(!settings.modal){$DOMWindowOverlay.click(function(){$.closeDOMWindow();});}
				}
				
				//loader
				if(settings.loader && settings.loaderImagePath !== ''){
					$('body').append('<div id="DOMWindowLoader" style="z-index:10002;width:'+settings.loaderWidth+'px;height:'+settings.loaderHeight+'px;"><img src="'+settings.loaderImagePath+'" /></div>');
					centerLoader();
				}

				//add DOMwindow
				$('body').append('<div id="DOMWindow" style="background-repeat:no-repeat;'+settings.windowBGImage+';overflow:auto;padding:'+settings.windowPadding+'px;display:none;height:'+settings.height+'px;width:'+settings.width+'px;background-color:'+settings.windowBGColor+';border:'+settings.borderSize+'px solid '+settings.borderColor+'; position:absolute;z-index:10001"></div>');
				
				var $DOMWindow = $('#DOMWindow');
				//centered, absolute, or fixed
				switch(settings.positionType){
					case 'centered':
						centerDOMWindow();
						if(settings.height + 50 > shortcut.viewPortHeight()){//added 50 to be safe
							$DOMWindow.css('top', (settings.fixedWindowY + shortcut.scrollOffsetHeight()) + 'px');
						}
					break;
					case 'absolute':
						$DOMWindow.css({'top':(settings.positionTop+shortcut.scrollOffsetHeight())+'px','left':(settings.positionLeft+shortcut.scrollOffsetWidth())+'px'});
						if($.fn.draggable){
							if(settings.draggable){$DOMWindow.draggable({cursor:'move'});}
						}
					break;
					case 'fixed':
						fixedDOMWindow();
					break;
					case 'anchoredSingleWindow':
						var anchoredPositions = $(settings.anchoredSelector).position();
						var anchoredPositionX = anchoredPositions.left + settings.positionLeft;
						var anchoredPositionY = anchoredPositions.top + settings.positionTop;
						$DOMWindow.css({'top':anchoredPositionY + 'px','left':anchoredPositionX+'px'});
								
					break;
				}
				
				$(window).bind('scroll.DOMWindow',function(){
					if(settings.overlay){sizeOverlay();}
					if(shortcut.isIE6){sizeIE6Iframe();}
					if(settings.positionType == 'centered'){centerDOMWindow();}
					if(settings.positionType == 'fixed'){fixedDOMWindow();}
				});

				$(window).bind('resize.DOMWindow',function(){
					if(shortcut.isIE6){sizeIE6Iframe();}
					if(settings.overlay){sizeOverlay();}
					if(settings.positionType == 'centered'){centerDOMWindow();}
				});
				
				switch(settings.windowSource){
					case 'inline'://////////////////////////////// inline //////////////////////////////////////////
						$DOMWindow.append($(settings.windowSourceID).children());
						$DOMWindow.unload(function(){// move elements back when you're finished
							$(settings.windowSourceID).append($DOMWindow.children());				
						});
						showDOMWindow();
					break;
					case 'iframe'://////////////////////////////// iframe //////////////////////////////////////////
						$DOMWindow.append('<iframe frameborder="0" hspace="0" wspace="0" src="'+settings.windowSourceURL+'" name="DOMWindowIframe'+Math.round(Math.random()*1000)+'" style="width:100%;height:100%;border:none;background-color:#fff;" id="DOMWindowIframe" ></iframe>');
						$('#DOMWindowIframe').load(showDOMWindow());
					break;
					case 'ajax'://////////////////////////////// ajax //////////////////////////////////////////
						if(settings.windowHTTPType == 'post'){
							
							if(settings.windowSourceURL.indexOf("?") !== -1){//has a query string
								urlOnly = settings.windowSourceURL.substr(0, settings.windowSourceURL.indexOf("?"));
								urlQueryObject = urlQueryToObject(settings.windowSourceURL);
							}else{
								urlOnly = settings.windowSourceURL;
								urlQueryObject = {};
							}
							$DOMWindow.load(urlOnly,urlQueryObject,function(){
								showDOMWindow();
							});
						}else{
							if(settings.windowSourceURL.indexOf("?") == -1){ //no query string, so add one
								settings.windowSourceURL += '?';
							}
							$DOMWindow.load(
								settings.windowSourceURL + '&random=' + (new Date().getTime()),function(){
								showDOMWindow();
							});
						}
					break;
				}
				
			}//end if anchored, or absolute, fixed, centered
			
		};//end run()
		
		if(settings.eventType){//if used with $().
			return this.each(function(index){				  
				$(this).bind(settings.eventType,function(){
					run(this);
					return false;
				});
			});	
		}else{//else called as $.function
			run();
		}
		
	};//end function openDOMWindow
	
	//allow for public call, pass settings
	$.openDOMWindow = function(s){$.fn.openDOMWindow(s);};
	
})(jQuery);
;/**/
/* Modernizr 2.6.2 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-video-cssclasses-hasevent-domprefixes
 */
;window.Modernizr=function(a,b,c){function y(a){j.cssText=a}function z(a,b){return y(prefixes.join(a+";")+(b||""))}function A(a,b){return typeof a===b}function B(a,b){return!!~(""+a).indexOf(b)}function C(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:A(f,"function")?f.bind(d||b):f}return!1}var d="2.6.2",e={},f=!0,g=b.documentElement,h="modernizr",i=b.createElement(h),j=i.style,k,l={}.toString,m="Webkit Moz O ms",n=m.split(" "),o=m.toLowerCase().split(" "),p={},q={},r={},s=[],t=s.slice,u,v=function(){function d(d,e){e=e||b.createElement(a[d]||"div"),d="on"+d;var f=d in e;return f||(e.setAttribute||(e=b.createElement("div")),e.setAttribute&&e.removeAttribute&&(e.setAttribute(d,""),f=A(e[d],"function"),A(e[d],"undefined")||(e[d]=c),e.removeAttribute(d))),e=null,f}var a={select:"input",change:"input",submit:"form",reset:"form",error:"img",load:"img",abort:"img"};return d}(),w={}.hasOwnProperty,x;!A(w,"undefined")&&!A(w.call,"undefined")?x=function(a,b){return w.call(a,b)}:x=function(a,b){return b in a&&A(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=t.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(t.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(t.call(arguments)))};return e}),p.video=function(){var a=b.createElement("video"),c=!1;try{if(c=!!a.canPlayType)c=new Boolean(c),c.ogg=a.canPlayType('video/ogg; codecs="theora"').replace(/^no$/,""),c.h264=a.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/,""),c.webm=a.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/,"")}catch(d){}return c};for(var D in p)x(p,D)&&(u=D.toLowerCase(),e[u]=p[D](),s.push((e[u]?"":"no-")+u));return e.addTest=function(a,b){if(typeof a=="object")for(var d in a)x(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof f!="undefined"&&f&&(g.className+=" "+(b?"":"no-")+a),e[a]=b}return e},y(""),i=k=null,e._version=d,e._domPrefixes=o,e._cssomPrefixes=n,e.hasEvent=v,g.className=g.className.replace(/(^|\s)no-js(\s|$)/,"$1$2")+(f?" js "+s.join(" "):""),e}(this,this.document);;/**/
/* Placeholders.js v2.1.1 */
(function(t){"use strict";function e(t,e,r){return t.addEventListener?t.addEventListener(e,r,!1):t.attachEvent?t.attachEvent("on"+e,r):void 0}function r(t,e){var r,n;for(r=0,n=t.length;n>r;r++)if(t[r]===e)return!0;return!1}function n(t,e){var r;t.createTextRange?(r=t.createTextRange(),r.move("character",e),r.select()):t.selectionStart&&(t.focus(),t.setSelectionRange(e,e))}function a(t,e){try{return t.type=e,!0}catch(r){return!1}}t.Placeholders={Utils:{addEventListener:e,inArray:r,moveCaret:n,changeType:a}}})(this),function(t){"use strict";function e(){}function r(t,e){var r,n,a=!!e&&t.value!==e,u=t.value===t.getAttribute(V);return(a||u)&&"true"===t.getAttribute(D)?(t.setAttribute(D,"false"),t.value=t.value.replace(t.getAttribute(V),""),t.className=t.className.replace(R,""),n=t.getAttribute(z),n&&(t.setAttribute("maxLength",n),t.removeAttribute(z)),r=t.getAttribute(I),r&&(t.type=r),!0):!1}function n(t){var e,r,n=t.getAttribute(V);return""===t.value&&n?(t.setAttribute(D,"true"),t.value=n,t.className+=" "+k,r=t.getAttribute(z),r||(t.setAttribute(z,t.maxLength),t.removeAttribute("maxLength")),e=t.getAttribute(I),e?t.type="text":"password"===t.type&&K.changeType(t,"text")&&t.setAttribute(I,"password"),!0):!1}function a(t,e){var r,n,a,u,i;if(t&&t.getAttribute(V))e(t);else for(r=t?t.getElementsByTagName("input"):p,n=t?t.getElementsByTagName("textarea"):h,i=0,u=r.length+n.length;u>i;i++)a=r.length>i?r[i]:n[i-r.length],e(a)}function u(t){a(t,r)}function i(t){a(t,n)}function l(t){return function(){b&&t.value===t.getAttribute(V)&&"true"===t.getAttribute(D)?K.moveCaret(t,0):r(t)}}function o(t){return function(){n(t)}}function c(t){return function(e){return m=t.value,"true"===t.getAttribute(D)&&m===t.getAttribute(V)&&K.inArray(C,e.keyCode)?(e.preventDefault&&e.preventDefault(),!1):void 0}}function s(t){return function(){r(t,m),""===t.value&&(t.blur(),K.moveCaret(t,0))}}function d(t){return function(){t===document.activeElement&&t.value===t.getAttribute(V)&&"true"===t.getAttribute(D)&&K.moveCaret(t,0)}}function g(t){return function(){u(t)}}function v(t){t.form&&(L=t.form,L.getAttribute(P)||(K.addEventListener(L,"submit",g(L)),L.setAttribute(P,"true"))),K.addEventListener(t,"focus",l(t)),K.addEventListener(t,"blur",o(t)),b&&(K.addEventListener(t,"keydown",c(t)),K.addEventListener(t,"keyup",s(t)),K.addEventListener(t,"click",d(t))),t.setAttribute(U,"true"),t.setAttribute(V,E),n(t)}var p,h,b,f,m,A,y,E,x,L,T,N,S,w=["text","search","url","tel","email","password","number","textarea"],C=[27,33,34,35,36,37,38,39,40,8,46],B="#ccc",k="placeholdersjs",R=RegExp("(?:^|\\s)"+k+"(?!\\S)"),V="data-placeholder-value",D="data-placeholder-active",I="data-placeholder-type",P="data-placeholder-submit",U="data-placeholder-bound",j="data-placeholder-focus",q="data-placeholder-live",z="data-placeholder-maxlength",F=document.createElement("input"),G=document.getElementsByTagName("head")[0],H=document.documentElement,J=t.Placeholders,K=J.Utils;if(J.nativeSupport=void 0!==F.placeholder,!J.nativeSupport){for(p=document.getElementsByTagName("input"),h=document.getElementsByTagName("textarea"),b="false"===H.getAttribute(j),f="false"!==H.getAttribute(q),A=document.createElement("style"),A.type="text/css",y=document.createTextNode("."+k+" { color:"+B+"; }"),A.styleSheet?A.styleSheet.cssText=y.nodeValue:A.appendChild(y),G.insertBefore(A,G.firstChild),S=0,N=p.length+h.length;N>S;S++)T=p.length>S?p[S]:h[S-p.length],E=T.attributes.placeholder,E&&(E=E.nodeValue,E&&K.inArray(w,T.type)&&v(T));x=setInterval(function(){for(S=0,N=p.length+h.length;N>S;S++)T=p.length>S?p[S]:h[S-p.length],E=T.attributes.placeholder,E&&(E=E.nodeValue,E&&K.inArray(w,T.type)&&(T.getAttribute(U)||v(T),(E!==T.getAttribute(V)||"password"===T.type&&!T.getAttribute(I))&&("password"===T.type&&!T.getAttribute(I)&&K.changeType(T,"text")&&T.setAttribute(I,"password"),T.value===T.getAttribute(V)&&(T.value=E),T.setAttribute(V,E))));f||clearInterval(x)},100)}J.disable=J.nativeSupport?e:u,J.enable=J.nativeSupport?e:i}(this);;/**/
/*!
 * Retina.js v1.3.0
 *
 * Copyright 2014 Imulus, LLC
 * Released under the MIT license
 *
 * Retina.js is an open source script that makes it easy to serve
 * high-resolution images to devices with retina displays.
 */
!function(){function a(){}function b(a){return f.retinaImageSuffix+a}function c(a,c){if(this.path=a||"","undefined"!=typeof c&&null!==c)this.at_2x_path=c,this.perform_check=!1;else{if(void 0!==document.createElement){var d=document.createElement("a");d.href=this.path,d.pathname=d.pathname.replace(g,b),this.at_2x_path=d.href}else{var e=this.path.split("?");e[0]=e[0].replace(g,b),this.at_2x_path=e.join("?")}this.perform_check=!0}}function d(a){this.el=a,this.path=new c(this.el.getAttribute("src"),this.el.getAttribute("data-at2x"));var b=this;this.path.check_2x_variant(function(a){a&&b.swap()})}var e="undefined"==typeof exports?window:exports,f={retinaImageSuffix:"@2x",check_mime_type:!0,force_original_dimensions:!0};e.Retina=a,a.configure=function(a){null===a&&(a={});for(var b in a)a.hasOwnProperty(b)&&(f[b]=a[b])},a.init=function(a){null===a&&(a=e);var b=a.onload||function(){};a.onload=function(){var a,c,e=document.getElementsByTagName("img"),f=[];for(a=0;a<e.length;a+=1)c=e[a],c.getAttributeNode("data-no-retina")||f.push(new d(c));b()}},a.isRetina=function(){var a="(-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx)";return e.devicePixelRatio>1?!0:e.matchMedia&&e.matchMedia(a).matches?!0:!1};var g=/\.\w+$/;e.RetinaImagePath=c,c.confirmed_paths=[],c.prototype.is_external=function(){return!(!this.path.match(/^https?\:/i)||this.path.match("//"+document.domain))},c.prototype.check_2x_variant=function(a){var b,d=this;return this.is_external()?a(!1):this.perform_check||"undefined"==typeof this.at_2x_path||null===this.at_2x_path?this.at_2x_path in c.confirmed_paths?a(!0):(b=new XMLHttpRequest,b.open("HEAD",this.at_2x_path),b.onreadystatechange=function(){if(4!==b.readyState)return a(!1);if(b.status>=200&&b.status<=399){if(f.check_mime_type){var e=b.getResponseHeader("Content-Type");if(null===e||!e.match(/^image/i))return a(!1)}return c.confirmed_paths.push(d.at_2x_path),a(!0)}return a(!1)},b.send(),void 0):a(!0)},e.RetinaImage=d,d.prototype.swap=function(a){function b(){c.el.complete?(f.force_original_dimensions&&(c.el.setAttribute("width",c.el.offsetWidth),c.el.setAttribute("height",c.el.offsetHeight)),c.el.setAttribute("src",a)):setTimeout(b,5)}"undefined"==typeof a&&(a=this.path.at_2x_path);var c=this;b()},a.isRetina()&&a.init(e)}();;/**/
this.console=this.console||{info:function(){},log:function(){},dir:function(){},debug:function(){},warn:function(){},error:function(){}}
;/**/

function getUrlParams() {
    var result = {};
    var params = (window.location.search.split('?')[1] || '').split('&');
    for(var param in params) {
        if (params.hasOwnProperty(param)) {
            paramParts = params[param].split('=');
            result[paramParts[0]] = decodeURIComponent(paramParts[1] || "");
        }
    }
    return result;
}


jQuery(function($) {
  //Homepage carousel
  $(function() {
    $('#hero-banner-carousel').carousel({interval: 5000});
    
    setTimeout(function() {
      // Do something after 5 seconds
      var bannerImgHeight = $('#hero-banner-carousel img').height();
      if(bannerImgHeight > 0){
        $('#hero-banner-carousel .hero-slide').css('min-height', bannerImgHeight);
      }
      //Adjust banner css on resize
      $(window).resize(function(){
        $('.carousel').carousel('pause');
        var bannerImgHeight = $('#hero-banner-carousel img').height();
        if(bannerImgHeight > 0){
          $('#hero-banner-carousel .hero-slide').css('min-height', bannerImgHeight);
        }      
      });

    }, 250);

  });
  //unbind JS from webforms
  $('.webform-client-form').unbind();
  //Add video.js to FF
    var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
    if(is_firefox)
    {
          var ss = document.createElement("link");
          ss.type = "text/css";
          ss.rel = "stylesheet";
          ss.href = "../../../vjs.zencdn.net/4.3/video-js.css"/*tpa=http://vjs.zencdn.net/4.3/video-js.css*/;
          $("body").append(ss);

          var s = document.createElement("script");
          s.type = "text/javascript";
          s.src = "../../../vjs.zencdn.net/4.3/video.js"/*tpa=http://vjs.zencdn.net/4.3/video.js*/;
          $("body").append(s);

          if($('#pilot_careers_video').length){
            setTimeout(function() {
              $('#pilot_careers_video').removeClass('vjs-controls-disabled');
              $('#pilot_careers_video .vjs-poster').show();
            }, 1500);           
            $('#pilot_careers_video').click(function(){
              $('#pilot_careers_video .vjs-poster').hide();
            });
          }
    }
  //IE Specific JS
  if($.browser.msie && parseInt($.browser.version, 10) <= 8){
          if($('#pilot_careers_video').length){
            setTimeout(function() {
              $('#pilot_careers_video').removeClass('vjs-controls-disabled');
              $('#pilot_careers_video .vjs-poster').show();
            }, 1500);           
            $('#pilot_careers_video').click(function(){
              $('#pilot_careers_video .vjs-poster').hide();
            });
          }
  }

  //Check device
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    //disable feedback modal and open in new page
    $('.feedback-container a').removeAttr('data-toggle');
    $('.feedback-container a').attr('href','/feedback');

    //update captcha img height and width for mobile devices
    setTimeout(function() {
        $('.captcha img').attr('height', 60);
        $('.captcha img').attr('width', 180);
      }, 2500);


    //Add new class to videos on iPad
    if(/iPad/i.test(navigator.userAgent)){
      $('.video video').addClass('ipad');
      $('.video-play-button').remove();
    }

    //Add video poster as image for iphone
    if(/iPhone/i.test(navigator.userAgent)){
      $('.video-play-button').remove();
      // $('.video video').wrap('<div class="video-controls-wrap"></div>');
      // $('video').each(function(){
      //     var poster = $(this).attr('poster');
      //     var output = '<img class="visible-phone iphone-poster" src="'+poster+'" />';

      //     $(this).before(output);
      //     $(this).after('<a class="video-play-button">Play</a>');          
      // });
    }
    $('.mobile .products-technologies').attr('href','javascript:;');
  }

  //Feedback Form action. Set to reload current page
  var feedbackURL = window.location.href;
  $('#webform-client-form-3332019').attr('action', feedbackURL);

  // Override Bootstrap behavior on Company Menu (and other dropdowns)
  $('ul.menu.nav .dropdown-menu a.dropdown-toggle').click(function() {
    window.location.href = $(this).attr('href');
  });

  // Hover state for Products
  if (window.innerWidth > 1024) {
    $('.prod-wrap').on('mouseover', function() {
      $(this).children('.prod-description').fadeIn();
    });
    $('.prod-wrap').on('mouseleave', function() {
      $(this).children('.prod-description').fadeOut();
    });
  }

  // Vertical tabs
  $('.vertical-tabs .panel-col-right .panel-pane').first().addClass('current-tab');
  $('.vertical-tabs .panel-col-left li').first().addClass('current');
  $('.vertical-tabs .panel-col-left li').click(function(e) {
    if ($('a', this).hasClass('request-information-cta')) {
      return true;
    }
    $('.current-tab').removeClass('current-tab');
    $('.current').removeClass('current');
    $('.panel-col-right .panel-pane').eq($(this).index()).addClass('current-tab');
    $(this).addClass('current');
    e.preventDefault();
  });

  // Vertical tabs -- Locations
  // Conflicts with the panel-pane Vertical Tabs above, but didn't want to fragment CSS.
  var tabsInterior = $('.vertical-tabs .panel-col-right .tab-content');
  if (tabsInterior.length > 0) {
    var verticalTabs = $('.vertical-tabs .panel-col-left .field-content');
    $('.vertical-tabs .panel-col-right .panel-pane').css('display', 'block');
    $('.current-tab').removeClass('current-tab');
    tabsInterior.first().addClass('current-tab');
    verticalTabs.first().addClass('current');
    verticalTabs.click(function(e) {
      $('.current-tab').removeClass('current-tab');
      $('.current').removeClass('current');
      tabsInterior.eq($(this).parent().parent().index()).addClass('current-tab');
      $(this).addClass('current');
      e.preventDefault();
    });
  }

  // Vertical tabs for AGS L3 software pages that don't use panels
  var agsTabs = $('.node-software .ags-content .ags-tab');
  if (agsTabs.length > 0){
    agsTabs.first().addClass('current-tab');
    $('.node-software .vertical-menu-list li').first().addClass('current');
    $('.node-software .vertical-menu-list li').click(function (e) {
      $('.current-tab').removeClass('current-tab');
      $('.current').removeClass('current');
      agsTabs.eq($(this).index()).addClass('current-tab');
      $(this).addClass('current');
      e.preventDefault();
    });
  }

  //Vertical tabs for AGS Semiconductor Service pages
  var agsServiceTabs = $('.node-service .ags-content .ags-tab');
  if (agsServiceTabs.length > 0){
    agsServiceTabs.first().addClass('current-tab');
    $('.node-service .vertical-menu-list li').first().addClass('current');
    $('.node-service .vertical-menu-list li').click(function (e) {
      $('.current-tab').removeClass('current-tab');
      $('.current').removeClass('current');
      agsServiceTabs.eq($(this).index()).addClass('current-tab');
      $(this).addClass('current');
      e.preventDefault();
    });
  }

  //Update page to the correct location if hash is present in URL
  var hash = window.location.hash;
  if(hash){
    var clean_text = hash.substring(1);
    var text = clean_text.split('+');

    if(text[0] == 'loc'){
      $('.view-amat2-locations .tab-content').removeClass('current-tab');
      $('.view-amat2-locations .tab-content').each(function(){
         var getH3 = $(this).children('h3').text();
         getH3 = getH3.toLowerCase();
         getH3 = getH3.replace(' ', '-');
         if(getH3 == text[1]){
          $(this).addClass('current-tab');
         }
      });

      $('.view-amat2-locations .views-field-field-country-country-name .field-content').removeClass('current');
      $('.view-amat2-locations .views-field-field-country-country-name .field-content').each(function(){
        var getFieldName = $(this).children('.btn').text();
        getFieldName = getFieldName.toLowerCase();
        getFieldName = getFieldName.replace(' ', '-');

        if(getFieldName == text[1]){
          $(this).addClass('current');
        }
      });
    }
  }

  var verticalTabsExist = $('body').hasClass("vertical-tabs");
  //Vertical Tab hash matching functionality.
  //If there is a hash in the URL, show the appropriate tab.
  if(verticalTabsExist){
    var urlhash;

    //Get URL Hash
    urlhash = window.location.hash;

    var matchFound = false;
    //Detect first level tab if one exists
    var countTab = 0;
    $('.vertical-menu-list li a').each(function(){
      var linkHash = $(this).attr('href');
      //Make sure url hash matches href
      if(urlhash == linkHash){
        matchFound = true;
        return false;
      }
      countTab++;
    });

    if(matchFound){
        $('.current-tab').removeClass('current-tab');
        $('.current').removeClass('current');
        $('.panel-col-right .panel-pane').eq(countTab).addClass('current-tab');
        $('.vertical-menu-list li').eq(countTab).addClass('current');
    }
  }

  // Investor Relations tabs
  $('.investor-relations-block .ir-main-tab li').click(function(e) {
    $('.investor-relations-block > .current-tab').removeClass('current-tab');
    $('.ir-main-tab .current').removeClass('current');
    $('.investor-tab').eq($(this).index()).addClass('current-tab');
    $(this).addClass('current');
    e.preventDefault();
  });

  $('.investor-relations-block .ir-sub-tab li').click(function(e) {
    $('.quarterly-earnings-feed > .current-tab').removeClass('current-tab');
    $('.ir-sub-tab .current').removeClass('current');
    $('.quarterly-earnings-tab').eq($(this).index()).addClass('current-tab');
    $(this).addClass('current');
    e.preventDefault();
  });

  $('.investor-relations-block .ir-sub-tab-reports li').click(function(e) {
    $('.amat-ir-reports-years > .current-tab').removeClass('current-tab');
    $('.ir-sub-tab-reports .current').removeClass('current');
    $('.annual-reports-tab').eq($(this).index()).addClass('current-tab');
    $(this).addClass('current');
    e.preventDefault();
  });

  //Awards Page Button filtering
  $('#awards-landing-filter .btn').live('click',function(event){
    var selectedBtn = $(this).attr('value');
    //Initiate loading function
    loadingSpinner($('.view-id-view_awards_landing .view-content'), $('.view-id-view_awards_landing'));

    $('#edit-field-award-category-value').val(selectedBtn);
    $('#edit-submit-view-awards-landing').click();

    $('#awards-landing-filter a').each(function(){
      if($(this).hasClass('btn-current')){
        $(this).removeClass('btn-current');
      }
    });

    $(this).addClass('btn-current');

  });

  var pathname = window.location.pathname;

  var path_split = pathname.split('/');

  //Active state for sub navigation
  var section_nav = $('.section-navigation').length;
  var main_landing = $('.hero-slides').length;

  if(section_nav){
    $('.section-navigation .nav-links a').each(function(){
      var subnav_links = $(this).text().toLowerCase();
      var match_found = $.inArray(subnav_links, path_split);



      if(match_found > -1){
        if(path_split[1] == 'zh-hans' || path_split[1] == 'zh-hant' || path_split[1] == 'ja' || path_split[1] == 'ko' || path_split[1] == 'en-in' || path_split[1] == 'en-sg'){
          var i = 2;
        }
        else{
          var i = 1;
        }
        if(path_split[i] == 'global-services'){
          $(this).each(function(){
            if($(this).text().toLowerCase() == path_split[i+1]){
              $(this).addClass('active');
            }
          });
        }
        else{
          $(this).addClass('active');
        }
      }
    });

    $(window).resize(fixedHeader).trigger("resize");
  }

function fixedHeader(){
    var window_width = window.innerWidth;
    //Fixed header
    if(window_width < 980){
      //Mobile & Tablet Width
      $(window).unbind('scroll.fixedHeader');
      mobileHeader();
    }

    if(window_width > 979){
      //Desktop Width
      if(!main_landing){
        $(window).unbind('scroll.fixedHeader');
        childLandingHeader();
      }

      if (main_landing) {
        $(window).unbind('scroll.fixedHeader');
        mainLandingHeader();
      }
    }
}
function mainLandingHeader(){

  $('body').removeClass('fixed-header');
  $('.section-navigation').addClass('fixed');

  var mainLandingHeaderScroll = function () {
    var scroll = $(window).scrollTop();
    var elementOffset = $('.hero-slides .nav-links').offset().top;
    var distance      = ((elementOffset + 90) - scroll);
    var window_width = window.innerWidth;

    if (scroll >= distance && window_width > 979) {
      $('.section-navigation').addClass('fixed');
      $('.section-navigation').show();
      $('#hero-banner-carousel .nav-links').addClass('hidden');
    }
    if(scroll <= distance && window_width > 979) {
      $('.section-navigation').hide();
      $('.section-navigation').removeClass('fixed');
      $('#hero-banner-carousel .nav-links').removeClass('hidden');
    }
  }

  $(window).bind('scroll.fixedHeader', mainLandingHeaderScroll);

  return mainLandingHeaderScroll;
}
function childLandingHeader(){
  checkScrollDistance();
  function checkScrollDistance(){
    var scroll = $(window).scrollTop();
    if(scroll > 135){
      $('.section-navigation').addClass('fixed');
    }
  }

  $('body').removeClass('fixed-header');
  var childLandingHeaderScroll = function() {
    var scroll = $(window).scrollTop();
    var elementOffset = $('.section-navigation').offset().top;
    var distance      = ((elementOffset + 104) - scroll);

    if (scroll >= distance) {
      $('.section-navigation').addClass('fixed');
      $('body').addClass('fixed-header-desktop');
    } else {
      $('.section-navigation').removeClass('fixed');
      $('body').removeClass('fixed-header-desktop');
    }
  }
  $(window).bind('scroll.fixedHeader', childLandingHeaderScroll);
}

function mobileHeader(){
  var previousScroll = 0;
  $('.section-navigation').removeClass('fixed');
  var mobileHeaderScroll = function (){
     var currentScroll = $(this).scrollTop();
     //Detect scroll direction and add/remove fixed navigation
     if (currentScroll > previousScroll && currentScroll > 75){
         $('body').removeClass('fixed-header');
         $('.header-nav-wrap').removeClass('fixed');
     }
     if(currentScroll < 5){
      $('body').removeClass('fixed-header');
      $('.header-nav-wrap').removeClass('fixed');
     }
     if(currentScroll < previousScroll ){
        $('body').addClass('fixed-header');
        $('.header-nav-wrap').addClass('fixed');
     }
     previousScroll = currentScroll;
  }

  $(window).bind('scroll.fixedHeader', mobileHeaderScroll);

  return mobileHeaderScroll;
}

  var products_page = false;
  var anyparts_page = false;
  var main_parts = false;
  var req_quote = false;
  var product_library = false;
  var product_listing = false;
  var media_center = false;
  var locations_ww = false;
  var request_info_form = false;
  var main_search = false;
  var ir_news_search = false;
  var av_portfolio_search = false;
  var safe_harbor_page = false;
  var subscribe_page = false;
  var endura_ventura_pvd = false;

  //Check the URL and only run code that pertains to the page
  if(($.inArray( 'products', path_split ) > -1) && ($.inArray( 'endura-ventura-pvd', path_split ) > -1)){
    var endura_ventura_pvd = true;
  }
  if($.inArray( 'products', path_split ) > -1){
    var products_page = true;
  }
  if($.inArray( 'product-library', path_split ) > -1){
    var product_library = true;
  }
  if($.inArray( 'listing', path_split ) > -1){
    var product_listing = true;
  }
  if(($.inArray( 'parts', path_split ) > -1) || ($.inArray( 'parts-center', path_split ) > -1)){
    var any_parts = true;
  }
  if($.inArray( 'parts-center', path_split ) > -1){
    var main_parts = true;
  }
  if($.inArray( 'parts', path_split ) > -1){
    var prod_parts = true;
  }
  if($.inArray( 'request-quote', path_split ) > -1){
    var req_quote = true;
  }
  if($.inArray( 'media-center', path_split ) > -1){
    var media_center = true;
  }
  if($.inArray( 'request-information', path_split ) > -1){
    var request_info_form = true;
  }
  if($.inArray( 'search', path_split ) > -1){
    var main_search = true;
  }
  if(($.inArray( 'investor-relations', path_split ) > -1) && ($.inArray( 'news', path_split ) > -1)){
    var ir_news_search = true;
  }
  if(($.inArray( 'applied-ventures', path_split ) > -1) && ($.inArray( 'portfolio', path_split ) > -1)){
    var av_portfolio_search = true;
  }
  if(($.inArray( 'news', path_split ) > -1) && ($.inArray( 'events', path_split ) > -1)){
    var safe_harbor_page = true;
  }
  if(($.inArray( 'contact', path_split ) > -1) && ($.inArray( 'subscribe', path_split ) > -1)){
    var subscribe_page = true;
  }

  //Superscript R in title
  if(endura_ventura_pvd){
    $(".page-title h1").html("Endura<sup>®</sup> Ventura™ PVD");
  }
  //Subscribe page
  if(subscribe_page){
      if($.browser.msie && parseInt($.browser.version, 10) <= 8){
        //Force IE to submit form when Enter key is used
        $("#amat-utilities-nanochip-tech-form #edit-email--2").keydown(function (e) { 
          if(e.which == 13) {
            e.preventDefault();
            $('#amat-utilities-nanochip-tech-form button.form-submit').mousedown();
          }
        });
        //Force all other browsers to submit form when Enter key is used
        $("#amat-utilities-nanochip-fab-form #edit-email").keydown(function (e) { 
          if(e.which == 13) {
            e.preventDefault();
            $('#amat-utilities-nanochip-fab-form button.form-submit').mousedown();
          }
        });
      }
      else{
        $('#amat-utilities-nanochip-tech-form').change(function(){
          $('#amat-utilities-nanochip-tech-form button.form-submit').mousedown();
        });
        $('#amat-utilities-nanochip-fab-form').change(function(){
          $('#amat-utilities-nanochip-fab-form .form-submit').mousedown();
        });
      }
  }

  //Remove section navigation active states from events and news links
  if(safe_harbor_page){
    $('.section-navigation a').removeClass('active');
  }

  //AV Portfolio Search
  if(av_portfolio_search){
    //Add Search markup and functionality to Applied Ventures Portfolio page on load
    var searchIconMarkup = '<a class="parts-search-icon" href="javascript:;">&nbsp;</a>';
    $('.company-applied-ventures-portfolio .form-type-textfield .controls input').attr('placeholder', 'Search');
    $('.company-applied-ventures-portfolio .form-type-textfield .controls').append(searchIconMarkup);
    
    $('.company-applied-ventures-portfolio .form-type-textfield .controls input').change(function(){
      if($.browser.msie && parseInt($.browser.version, 10) <= 8){
        var inputVal = $("#edit-title").val();
        if(inputVal == 'Search'){
          $("#edit-title").val('');
        }
      }
      $('.company-applied-ventures-portfolio .views-submit-button button').click();

      //Initiate Loading function
      loadingSpinner($('.view-id-amat2_ventures_portfolio .view-content'), $('.view-id-amat2_ventures_portfolio'));
    });

    $('.parts-search-icon').click(function(){
      //Initiate Loading function
      loadingSpinner($('.view-id-amat2_ventures_portfolio .view-content'), $('.view-id-amat2_ventures_portfolio'));
     if($.browser.msie && parseInt($.browser.version, 10) <= 8){
        var inputVal = $("#edit-title").val();
        if(inputVal == 'Search'){
          $("#edit-title").val('');
        }
      }

      $('.company-applied-ventures-portfolio .views-submit-button button').click();
    });

    //Re-add Search markup and functionality on AV Portfolio page after drupal AJAX search functionality is completed
    Drupal.behaviors.updateAVPortfolioSearch = {
      attach: function (context, settings) {
        var searchIconMarkup = '<a class="parts-search-icon" href="javascript:;">&nbsp;</a>';
        $('.company-applied-ventures-portfolio .form-type-textfield .controls input').attr('placeholder', 'Search');
        $('.company-applied-ventures-portfolio .form-type-textfield .controls').append(searchIconMarkup);
        $('.parts-search-icon:gt(0)').remove();

        $('.company-applied-ventures-portfolio .form-type-textfield .controls input').change(function(){
          $('.company-applied-ventures-portfolio .views-submit-button button').click();

          //Initiate Loading function
          loadingSpinner($('.view-id-amat2_ventures_portfolio .view-content'), $('.view-id-amat2_ventures_portfolio'));
        });

        $('.parts-search-icon').click(function(){
           if($.browser.msie && parseInt($.browser.version, 10) <= 8){
              var inputVal = $("#edit-title").val();
              if(inputVal == 'Search'){
                $("#edit-title").val('');
              }
            }
            $('.company-applied-ventures-portfolio .views-submit-button button').click();

          //Initiate Loading function
          loadingSpinner($('.view-id-amat2_ventures_portfolio .view-content'), $('.view-id-amat2_ventures_portfolio'));
        });
      }
    };
  }

  //IR News Search
  if(ir_news_search){
    var searchIconMarkup = '<a class="parts-search-icon" href="javascript:;">&nbsp;</a>';
    $('.company-investor-relations-news .form-type-textfield .controls input').attr('placeholder', 'Search');
    $('.company-investor-relations-news .form-type-textfield .controls').append(searchIconMarkup);
    
    $('.company-investor-relations-news .form-type-textfield .controls input').change(function(){
      $('.company-investor-relations-news .views-submit-button button').click();

      //Initiate Loading spinner function
      loadingSpinner($('.view-id-amat_news_block .view-content'), $('.view-id-amat_news_block'));
    });

    $('.parts-search-icon').click(function(){
        $('.company-investor-relations-news .views-submit-button button').click();
    });

    Drupal.behaviors.updateIRNewsSearch = {
      attach: function (context, settings) {
        var searchIconMarkup = '<a class="parts-search-icon" href="javascript:;">&nbsp;</a>';
        $('.company-investor-relations-news .form-type-textfield .controls input').attr('placeholder', 'Search');
        $('.company-investor-relations-news .form-type-textfield .controls').append(searchIconMarkup);
        
        $('.company-investor-relations-news .form-type-textfield .controls input').change(function(){
          $('.company-investor-relations-news .views-submit-button button').click();

          //Initiate Loading spinner function
          loadingSpinner($('.view-id-amat_news_block .view-content'), $('.view-id-amat_news_block'));
        });

        $('.parts-search-icon').click(function(){
            $('.company-investor-relations-news .views-submit-button button').click();
        });
      }
    };
  }

  //Main search page - adding search icon to input
  if(main_search){
    var searchIconMarkup = '<a class="parts-search-icon" href="javascript:;">&nbsp;</a>';
    $('.page-search .form-type-textfield .controls input').attr('placeholder', 'Search');
    $('.page-search .form-type-textfield .controls').append(searchIconMarkup);
    
    $('.page-search .form-type-textfield .controls input').change(function(){
      $('.page-search .form-wrapper button').click();
    });

    $('.parts-search-icon').click(function(){
        $('.page-search .form-wrapper button').click();
    });
  }

  //Request information submit button
  if(request_info_form){
    $('.form-wrapper input').addClass('btn btn-primary');
  }

  //Media Center Filtering
  if(media_center){
    $('#media-center-menu a').click(function(){
      //Add active class to appropriate menu item
      $('#media-center-menu ul li').removeClass('active');
      $(this).parent().addClass('active');

      $('.form-type-textfield input').val('');

      var term_id = $(this).attr('termid');
      $('#edit-field-media-category-tid option[value="'+term_id+'"]').attr('selected', true);

      //Initialize loading overlay and spinner function
      loadingSpinner($('.view-id-amat2_media_browser .view-content'), $('.view-id-amat2_media_browser'));

      $('#views-exposed-form-amat2-media-browser-image-list .form-submit').click();
      $('#views-exposed-form-amat2-media-browser-video-list .form-submit').click();
      $('#views-exposed-form-amat2-media-browser-document-tile-list .form-submit').click();
    });

    $('#media-center-search').change(function(){
      $('#edit-field-media-category-tid option[value="All"]').attr('selected', true);

      $('.form-type-textfield input').val($(this).val());

      //Initialize loading overlay and spinner function
      loadingSpinner($('.view-id-amat2_media_browser .view-content'), $('.view-id-amat2_media_browser'));

      $('#views-exposed-form-amat2-media-browser-image-list .form-submit').click();
      $('#views-exposed-form-amat2-media-browser-video-list .form-submit').click();
      $('#views-exposed-form-amat2-media-browser-document-tile-list .form-submit').click();
    });
  }

  //START - Product Library Filtering
  if(product_library){
    $('.taxonomy-menu a.term').click(function(e){
      e.preventDefault;
      if($.browser.msie && parseInt($.browser.version, 10) <= 8){
        var parentCatText = $(this).parent().parent().parent().parent().parent().parent().parent().siblings('.accordion-heading').children('a').text();
        var subCatText = $(this).text();
      }
      else{
        var parentCatText = $(this).parent().parent().parent().parent().parent().parent().parent().siblings('.accordion-heading').children('a').text().trim();
        var subCatText = $(this).text().trim();
      }

      $('#prod_scat_name').html(parentCatText+' | '+subCatText);
    });

    $('.accordion-heading a').click(function(){
      if($.browser.msie && parseInt($.browser.version, 10) <= 8){
          var headingText = $(this).text();
      }
      else{
        var headingText = $(this).text().trim();
      }
          $('#prod_scat_name').html(headingText);
    });

    //Handle Alphabetical filtering
    $('.alpha-index a').click(function(e){
      var index = $(this).attr('data-id');
      e.preventDefault();

      //If Alpha filter is clicked, set hidden select menu to ALL - product categories,
      //update hidden alpha index field with appropriate value, and submit form.
      $('#edit-taxonomy-vocabulary-2-tid option').first().text('- Any -').attr('selected', true);
      $('#edit-field-alpha-index-value').val(index);

      //Initialize loading overlay and spinner function
      loadingSpinner($('.view-display-id-product_library_tile_view_2 .view-content'), $('.view-display-id-product_library_tile_view_2'));

      $('#edit-submit-amat2-product-selector').click();
    });

    //Load first subcateogory of clicked accordion title
    $('#accordion-products-menu .accordion-heading a').click(function(){
      if($.browser.msie && parseInt($.browser.version, 10) <= 8){
        var headingText = $(this).text();
      }
      else{
        var headingText = $(this).text().trim();
      }
      if(headingText == 'Alphabetical'){
        //If Alphabetical heading is clicked, set hidden select menu to ALL product categories,
        //add AJAX loading spinners, and submit form
        $('#edit-taxonomy-vocabulary-2-tid option').first().text('- Any -').attr('selected', true);
        $('#edit-field-alpha-index-value').val('');

        //Initialize loading overlay and spinner function
        loadingSpinner($('.view-display-id-product_library_tile_view_2 .view-content'), $('.view-display-id-product_library_tile_view_2'));

        $('#edit-submit-amat2-product-selector').click();
      }
      else if(headingText == 'Roll to Roll WEB Coating' || headingText == 'Roll to Roll WEB Coating '){
        //Do nothing if Roll to Roll is clicked, since that is handled elsewhere (around line 695)
        console.log('roll-to-roll');
      }
      else{
        $('#edit-field-alpha-index-value').val('');

        //Load first category on product library when accordion heading is clicked
        if($.browser.msie && parseInt($.browser.version, 10) <= 8){
          var firstCat = $(this).parent().siblings('.accordion-body').find('.taxonomy_menu_wrapper').children('.taxonomy_menu').first().children('.taxonomy-menu').find('.term').first().text();
          var parent_str = '-'+$(this).parent().siblings('.accordion-body').find('.taxonomy_menu_wrapper').children('.taxonomy_menu').first().children('h3').text();
        }
        else{
          var firstCat = $(this).parent().siblings('.accordion-body').find('.taxonomy_menu_wrapper').children('.taxonomy_menu').first().children('.taxonomy-menu').find('.term').first().text().trim();
          var parent_str = '-'+$(this).parent().siblings('.accordion-body').find('.taxonomy_menu_wrapper').children('.taxonomy_menu').first().children('h3').text().trim();
        }
        
         var menu_str = '--'+firstCat;
         var parent = 0;

        //match menu string to select menu value
        $('#edit-taxonomy-vocabulary-2-tid option').each(function(){
          var select_str = $(this).text();
          //Match the select option to the proper parent
          if(select_str == parent_str){
            parent ++;
          }
          if((select_str == menu_str) && (parent > 0)){
            $(this).attr('selected',true);
            parent = 0;
          }
        });

        //Initialize loading overlay and spinner function
        loadingSpinner($('.view-display-id-product_library_tile_view_2 .view-content'), $('.view-display-id-product_library_tile_view_2'));

        $('#edit-submit-amat2-product-selector').click();
      }
    });

    //Handle active state for Table or Tile view buttons
    if(product_listing){
      $('.view-table-btn').addClass('active');
    }
    else{
      $('.view-tile-btn').addClass('active');
    }

    //Handle roll-to-roll link heading in accordion menu
    $('.roll-to-roll').click(function(){
      //Minimize all other accordion sections
      $('.collapse.in').collapse('hide');

      //Clear all other hidden filter fields
      $('#edit-keys').val('');
      $('#edit-title').val('');
      $('#product-library-search').val('');
      $('#edit-field-alpha-index-value').val('');

      //Select roll to roll option in hidden menu filter, and submit form
      $('#edit-taxonomy-vocabulary-2-tid option').val('2000').attr('selected', true);
      $('#edit-submit-amat2-product-selector').click();

      //Add loading spinner
      if(product_listing){
        //Loading spinner markup for LIST view
        loadingSpinner($('.view-display-id-product_library_list_view_2 .view-content'), $('.view-display-id-product_library_list_view_2'));
      }
      else{
        //Loading spinner markup for TILE view
        loadingSpinner($('.view-display-id-product_library_tile_view_2 .view-content'), $('.view-display-id-product_library_tile_view_2'));
      }
    });

    //Reset form when clicking View All Products button
    $('.view-all-products-btn').click(function(){
      //Add loading spinner
      if(product_listing){
        loadingSpinner($('.view-display-id-product_library_list_view_2 .view-content'), $('.view-display-id-product_library_list_view_2'));
      }
      else{
        loadingSpinner($('.view-display-id-product_library_tile_view_2 .view-content'), $('.view-display-id-product_library_tile_view_2'));
      }

      $('#edit-keys').val('');
      $('#edit-title').val('');
      $('#product-library-search').val('');
      $('#edit-field-alpha-index-value').val('');

      $('#edit-taxonomy-vocabulary-2-tid option').first().text('- Any -').attr('selected', true);
      $('#edit-submit-amat2-product-selector').click();
    });

    //Handle custom search input
    $('#product-library-search').change(function(){
      //Add loading spinner
      if(product_listing) {
        loadingSpinner($('.view-display-id-product_library_list_view_2 .view-content'), $('.view-display-id-product_library_list_view_2'));
      }
      else{
        loadingSpinner($('.view-display-id-product_library_tile_view_2 .view-content'), $('.view-display-id-product_library_tile_view_2'));
      }
      $('#edit-field-alpha-index-value').val('');
      $('#edit-taxonomy-vocabulary-2-tid option').first().text('- Any -').attr('selected', true);
      $('#edit-title').val($(this).val());

      $('#edit-submit-amat2-product-selector').click();

      $('.parts-search-icon').click(function(){
        $('#edit-submit-amat2-product-selector').click();
      });
    });
    //Handle click event for accordion menu headings
    $('.taxonomy_menu_wrapper h3').click(function(){
      if($.browser.msie && parseInt($.browser.version, 10) <= 8){
        var clickedText = $(this).text();
      }
      else{
        var clickedText = $(this).text().trim();
      }

      if(clickedText == 'Technical Glossary'){
        //Do not submit form for Technical Glossary heading, redirect to glossary page instead
        console.log('redirecting to glossary page');
      }
      else{
        //Clear all existing filter values
        $('#edit-keys').val('');
        $('#edit-title').val('');
        $('#product-library-search').val('');
        $('#edit-field-alpha-index-value').val('');

        //Add loading spinner
        if(product_listing){
          loadingSpinner($('.view-display-id-product_library_list_view_2 .view-content'), $('.view-display-id-product_library_list_view_2'));
        }
        else{
          loadingSpinner($('.view-display-id-product_library_tile_view_2 .view-content'), $('.view-display-id-product_library_tile_view_2'));
        }

        var menu_list_str = '-'+$(this).text();
        var parent_list_str = $(this).parentsUntil('accordion-body').siblings('.accordion-heading').children().text().trim();
        var parent_list = 0;

        //match menu string to select menu value
        $('#edit-taxonomy-vocabulary-2-tid option').each(function(){
          var select_list_str = $(this).text();

          //Match the select option to the proper parent
          if(select_list_str == parent_list_str){
            parent_list ++;
          }
          if((select_list_str == menu_list_str) && (parent_list > 0)){
            $(this).attr('selected',true);
            parent_list = 0;
          }
          //Submit Search
          $('#edit-submit-amat2-product-selector').click();
        });
      }
    });

    //Swap out accordion icons using bootstrap events
      $('.accordion-group').on('show hide', function (n) {
          $(n.target).siblings('.accordion-heading').find('.accordion-toggle img').toggleClass('show hide');
      });

    //Handle accordion menu click
    $('http://www.appliedmaterials.com/files/advagg_js/ul.taxonomy-menu a.term').click(function(event){
      event.preventDefault(); //Prevent links from redirecting page
      $('#edit-keys').val('');
      $('#edit-title').val('');
      $('#product-library-search').val('');
      $('#edit-field-alpha-index-value').val('');

      //Add loading spinner
      if(product_listing){
        loadingSpinner($('.view-display-id-product_library_list_view_2 .view-content'), $('.view-display-id-product_library_list_view_2'));
      }
      else{
        loadingSpinner($('.view-display-id-product_library_tile_view_2 .view-content'), $('.view-display-id-product_library_tile_view_2'));
      }

      var menu_str = '--'+$(this).text();
      var parent_str = '-'+$(this).parent().parent().siblings('h3').text();
      var parent = 0;

      //match menu string to select menu value
      $('#edit-taxonomy-vocabulary-2-tid option').each(function(){
        var select_str = $(this).text();
        //Match the select option to the proper parent
        if(select_str == parent_str){
          parent ++;
        }
        if((select_str == menu_str) && (parent > 0)){
          $(this).attr('selected',true);
          parent = 0;
        }
      });
      //Submit Search
      $('#edit-submit-amat2-product-selector').click();
    });
  }

  //Enable request quote form fields on webform page
  if(req_quote){
    $('#edit-submitted-part-name').removeAttr('readonly');
    $('#edit-submitted-rquote-part-id').removeAttr('readonly');
    $('#edit-submitted-rquote-category').removeAttr('readonly');
    $('#edit-submitted-sub-category').removeAttr('readonly');
  }

  //Add Search placeholder to products exposed filter
  if(products_page){
    var urlparams = getUrlParams();
    var searchIconMarkup = '<a class="parts-search-icon" href="javascript:;">&nbsp;</a>';
    $('.products .form-type-textfield .controls input').attr('placeholder', 'Search');
    $('.products .form-type-textfield .controls').append(searchIconMarkup);

    if (urlparams.title) $('.edit-keys').val(urlparams.title);
    $('.parts-search-icon').click(function(){
      $('#edit-submit-amat2-product-selector').click();
    });
  }
  //END Products Filtering

  //START PARTS IF STATEMENT
  if(any_parts){
    //Change table opacity when sorting parts or clicking table pagination
    $('.views-table .views-field a').not('.rquote').click(function(){
      $('.views-table tbody').addClass('loading');
    });
    $('.pagination a').click(function(){
      $('.views-table').addClass('loading');
    });
    //Re-attach JS functionality for opacity after drupal AJAX call
    Drupal.behaviors.updateTableOpacity = {
      attach: function (context, settings) {
        $('.views-table .views-field a').not('.rquote').click(function(){
          $('.views-table tbody').addClass('loading');
        });

        $('.pagination a').click(function(){
          $('.views-table').addClass('loading');
        });
      }
    };
    if(main_parts){
      var parts_firstCat = $('#accordion-parts-menu .accordion-heading #cat-1').text();
      var parts_firstSubCat = $('#accordion-parts-menu #scat-1 .accordion-inner').first().children().text();

      $('.view-amat-parts #edit-category').val(parts_firstCat);
      $('.view-amat-parts #edit-subcategory').val(parts_firstSubCat);

      $('.view-amat-parts #edit-submit-amat-parts').click();
      
      setTimeout(function() {
        $('.pane-amat-parts').show();
      }, 1500);

      $('.accordion-heading a').click(function() {
        var $this = $(this);
        var $accordion_body = $this.parents('.accordion-group').find('.accordion-body');
        if (!$accordion_body.hasClass('in')) {
          $accordion_body.find('div').first().click();
        }
      });

      //Parts Accordion Menu
      $('.accordion-body div').click(function() {
        //Insert AJAX Spinner overlay and change table opacity
        $('.views-table').before($('<div class="parts-ajax-overlay"><div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div></div>'));
        $('.views-table').addClass('loading');
        //Clear all fields except Category and subcategory when clicking side menu item
        $('#parts-search').val('');
        $('#edit-partid, #edit-name, #edit-cip-number').val('');
        var li_val = $(this).attr('data');
        li_val = li_val.split('@@');

        $("#edit-category").val(li_val[0]);
        $("#edit-subcategory").val(li_val[1]);
        $("#edit-category").trigger('change');
        $('#edit-subcategory').trigger('change');

        $("#scat_name").html(li_val[0] + " / " + li_val[1]);

        $('#edit-submit-amat-parts').click();
        $('#scat_name').fadeIn('slow');
      });

      //Swap out accordion icon using bootstrap events
      $('.accordion-group').on('show hide', function (n) {
          $(n.target).siblings('.accordion-heading').find('.accordion-toggle img').toggleClass('show hide');
      });

      //Add search term to input for pagination items
      var sPageURL = window.location.search.substring(1);

      if(sPageURL){
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)
        {
            var sParameterName = sURLVariables[i].split('=');
            if(sParameterName[0] == 'name'){
              $('#parts-search').val(sParameterName[1]);
            }
        }
      }

      //Parts Center Request Quote Modal
      $(document).on("click", ".rquote", function () {
        var quotes = $(this).data('id');
        var quote_data = quotes.split("@@");

        $("#edit-submitted-part-name").val(quote_data[0]);
        $("#edit-submitted-rquote-part-id").val(quote_data[1]);
        $("#edit-submitted-rquote-category").val(quote_data[2]);
        $("#edit-submitted-sub-category").val(quote_data[3]);

        $("#edit-submitted-rquote-name").attr("placeholder", "Enter your name");
        $("#edit-submitted-rquote-email").attr("placeholder", "Enter your email address");

        $('.webform-component-textfield input').click(function(){
          $(this).removeAttr('readonly');
        });
      });
    }

    //Product Specific Parts pages
    if(prod_parts){
      var mainCat = $('#parts-menu .menu-item').attr('data');
      mainCat = mainCat.split('@@');
      $("#edit-category").val(mainCat[0]);
      $('#edit-submit-amat-parts').click();

      //Side Menu Click
      $('.menu-item a').click(function(){
        //Insert AJAX Spinner overlay and change table opacity
        $('.views-table').before($('<div class="parts-ajax-overlay"><div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div></div>'));
        $('.views-table').addClass('loading');
        //Clear all fields except Category and subcategory when clicking side menu item
        $('#parts-search').val('');
        $('#edit-partid').val('');
        $('#edit-name').val('');
        $("#edit-cip-number").val('');

        var subCat = $(this).parent().attr('data');
        subCat = subCat.split('@@');

        $("#edit-subcategory").val(subCat[1]);
        $('#edit-submit-amat-parts').click();
        $("#scat_name").html(subCat[1].toUpperCase());
        $('#scat_name').fadeIn('slow');
      });
      //Request quote modal
      $(document).on("click", ".rquote", function () {
        var quotes = $(this).data('id');
        var quote_data = quotes.split("@@");
        $("#edit-submitted-part-name").val(quote_data[0]);
        $("#edit-submitted-rquote-part-id").val(quote_data[1]);
        $("#edit-submitted-rquote-category").val(quote_data[2]);
        $("#edit-submitted-sub-category").val(quote_data[3]);

        $("#edit-submitted-rquote-name").attr("placeholder", "Enter your name");
        $("#edit-submitted-rquote-email").attr("placeholder", "Enter your email address");
        $('.webform-component-textfield input').click(function(){
          $(this).removeAttr('readonly');
        });
      });
    }

    //Parts Center Search
    // Drupal.behaviors.updateCustomSearch = {
    //   attach: function (context, settings) {
    //     $('#parts-search').hasClass('search-processed');
    //     if($('#parts-search').hasClass('search-processed')){
    //       $('.pagination a').click(function(e){
    //         e.preventDefault();
    //         window.alert = function() {};
    //         $('.views-table').before($('<div class="parts-ajax-overlay"><div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div></div>'));
    //         window.location = $(this).attr('href');
    //       });
    //     }
    //   }
    // };

    $('#parts-search').change(function(){
      $(this).addClass('search-processed');
      $('.views-table').before($('<div class="parts-ajax-overlay"><div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div></div>'));
      $('.views-table').addClass('loading');
      if(main_parts){
        $('#edit-category').val('');
        $('#edit-subcategory').val('');
      }
      if(prod_parts){
        $('#edit-subcategory').val('');
      }
      $('#edit-partid, #edit-name, #edit-cip-number').val($(this).val());
      $('#edit-submit-amat-parts').click();
      $('#scat_name').fadeOut('slow');
    });

    $('.parts-search-icon').click(function(){
      $('#edit-submit-amat-parts').click();
    });
  }
  //END PARTS IF STATEMENT

  // Add placeholders to fields
  $('#node-3332209 .email, #node-3332210 .email, #node-3332423 .email').prop('placeholder', 'Enter email address');

  // Media Center form - display "Other" text box when option selected
  $('#edit-submitted-asset-use-4').change(function() {
    if (this.checked) {
      $('#node-3332423 #edit-submitted-other').fadeIn('fast');
    }
    else {
      $('#node-3332423 #edit-submitted-other').fadeOut('fast').val('');
    }
  });

  // Add overlay button to videos except for IE 8 and lower (uses VIDEO.js)
   if($.browser.msie && parseInt($.browser.version, 10) > 8){
      var videoStyle = function () {
        $('.video video').wrap('<div class="video-controls-wrap"></div>');
        $('.video video').each(function(idx) {
          var videoClass = 'video-play' + idx,
              videoPlay = '<a class="' + videoClass + ' video-play-button">Play</a>',
              $this = $(this),
              currentVid = $(this).get(0);
          currentVid.removeAttribute('controls');
          $('.' + videoClass).live('click', function() {
            currentVid.play();
            currentVid.volume = 0.5;
            $(this).fadeOut();
            currentVid.setAttribute('controls');
          });
          $this.after(videoPlay);
        })
      };
      videoStyle();

      $('.video video').live('click', function() {
        currentVid = $(this).get(0);
        currentVid.volume = 0.5;
      });
   }
  if(!$.browser.msie && !is_firefox && !navigator.userAgent.match(/(iPod|iPhone|iPad)/)){
      var videoStyle = function () {
        $('.video video').wrap('<div class="video-controls-wrap"></div>');
        $('.video video').each(function(idx) {
          var videoClass = 'video-play' + idx,
              videoPlay = '<a class="' + videoClass + ' video-play-button">Play</a>',
              $this = $(this),
              currentVid = $(this).get(0);
          currentVid.removeAttribute('controls');
          $('.' + videoClass).live('click', function() {
            currentVid.play();
            currentVid.volume = 0.5;
            $(this).fadeOut();
            currentVid.setAttribute('controls','true');
          });
          $this.after(videoPlay);
        })
      };
      videoStyle();

      $('.video video').live('click', function() {
        currentVid = $(this).get(0);
        currentVid.volume = 0.5;
      });
   }

  // Change - All - text in dropdowns on Portfolio page
  $('#edit-field-portfolio-status-value option[value="All"]').text('Status');
  $('#edit-field-portfolio-location-value option[value="All"]').text('Location');
  $('#edit-field-portfolio-sector-value option[value="All"]').text('Sector');

  $('.vertical-menu-list').closest('.panel-col-left').next('.panel-col-right').addClass('vertical-menu-list-right');

  // Add Take Snapshot button to video fields
  // function addTakeSnapshotButtons() {
  //   var video_fields = [];
  //   // var $video_fields = ;
  //   jQuery('.field-name-field-video').each(function(item) {
  //     $video = jQuery(this);
  //     video_fields.push({
  //       id: $video.attr('id'),
  //       element: $video,
  //       filename: $video.find('.media-item').attr('title')
  //     });
  //   });
  //   // console.dir(video_fields);
  //   // var html = "<a href=\"#\" class=\"button snapshot\" id=\"edit-field-video-collection-und-0-field-video-und-0-remove\">Take Snapshot</a>";
  // }
  // addTakeSnapshotButtons();
});

//Awards Landing page Filtering updates after AJAX call
Drupal.behaviors.updateExposedFilterAwards = {
  attach: function(context) {
    var findIt = $('#edit-field-award-category-value:not(.rewrite-processed)', context);
    if (findIt.length > 0) {
      findIt.addClass('.rewrite-processed');
      $('#edit-field-award-category-value option').each(function() {
        if ($(this).attr('selected')) {
          var selectedOption = $(this).val();
          $('#awards-landing-filter a[value="'+selectedOption+'"]').addClass('btn-current');
        }
      });
    }
  }
};

//Add hidden label to global search for SEO
Drupal.behaviors.updateSearchLabel = {
  attach: function(context) {
    $('.form-search.content-search .input-append', context).prepend('<label class="hide">Search</label>');
  }
};

//Loading overlay and AJAX Spinner markup
function loadingSpinner(overlay, spinner){
  overlay.addClass('loading');
  spinner.append('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>');
};/**/
