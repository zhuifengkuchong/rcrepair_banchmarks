
var hideUtilBox, showUtilBox;
var curToolbox;
var curBtnWidget;
var cur_carousel = 0;
var cur_carousel_secondary = 0;
var carousel_num;
var carousel_num_secondary;
var next_carousel = 0;
var next_carousel_secondary = 0;
var rollover_feature_call, rollover_feature_hide;

$(function () {
    init();
    //GoogleEventTracking();

    $(window).load(function () {
        GoogleEventTrackingUniversal();
    });

    $(".btn-widget").on('mouseover', function () {
        var shoverClass = $(this).attr("id") + "-shover";
        $(this).addClass(shoverClass).children(".util-widget-box").show();
        curBtnWidget = shoverClass;
    }).on('mouseout', function () {
        $(this).removeClass(curBtnWidget).children(".util-widget-box").hide();
    });

    $('#icon-print').on('click', function () {
        window.print();
    });


    /**** image carousel - previous button ****/
    $(".hero-image-carousel-container #carousel-controls .prev").on('click', function () {
        if (cur_carousel <= 0) {
            next_carousel = carousel_num - 1;
        } else {
            next_carousel = cur_carousel - 1;
        }
        carousel_next(cur_carousel, next_carousel);
        cur_carousel = next_carousel;
    }).on('mouseover', function () {
        $(this).addClass("prev-shover");
    }).on('mouseout', function () {
        $(this).removeClass("prev-shover");
    });

    /**** image carousel - next button ****/
    $(".hero-image-carousel-container #carousel-controls .next").on('click', function () {
        if (cur_carousel >= (carousel_num - 1)) {
            next_carousel = 0;
        } else {
            next_carousel = cur_carousel + 1;
        }
        carousel_next(cur_carousel, next_carousel);
        cur_carousel = next_carousel;
    }).on('mouseover', function () {
        $(this).addClass("next-shover");
    }).on('mouseout', function () {
        $(this).removeClass("next-shover");
    });

    /**** secondary image carousel - previous button ****/
    /*    $(".hero-image-carousel-secondary-container #carousel-controls .prev").on('click', function () {
    if (cur_carousel_secondary <= 0) {
    next_carousel_secondary = carousel_num - 1;
    } else {
    next_carousel_secondary = cur_carousel_secondary - 1;
    }
    secondary_carousel_next(cur_carousel_secondary, next_carousel_secondary);
    cur_carousel_secondary = next_carousel_secondary;
    }).on('mouseover', function () {
    $(this).addClass("prev-shover");
    }).on('mouseout', function () {
    $(this).removeClass("prev-shover");
    });*/

    /**** secondary image carousel - next button ****/
    /*  $(".hero-image-carousel-secondary-container #carousel-controls .next").on('click', function () {
    if (cur_carousel_secondary >= (carousel_num_secondary - 1)) {
    next_carousel_secondary = 0;
    } else {
    next_carousel_secondary = cur_carousel_secondary + 1;
    }
    secondary_carousel_next(cur_carousel_secondary, next_carousel_secondary);
    cur_carousel_secondary = next_carousel_secondary;
    }).on('mouseover', function () {
    $(this).addClass("next-shover");
    }).on('mouseout', function () {
    $(this).removeClass("next-shover");
    });
    */


    var slider = $('.hero-image-carousel-container .wrppr');
    var containerWidth = slider.width();
    var sliderWidth = containerWidth * carousel_num;
    var carouselInnerLeft = -1 * containerWidth * cur_carousel;
    slider.width(containerWidth);

    /* var sliderSecondary = $('.hero-image-carousel-secondary-container .wrppr');
    var containerWidthSecondary = sliderSecondary.width();
    var sliderWidthSecondary = containerWidthSecondary * carousel_num_secondary;
    var carouselInnerLeftSecondary = -1 * containerWidthSecondary * cur_carousel_secondary;
    sliderSecondary.width(containerWidthSecondary);*/


    // Update the widths if the browser is resized
    $(window).bind("resize", function () {
        containerWidth = $('.hero-image-carousel-container .hero-image-carousel ').width();
        sliderWidth = containerWidth * carousel_num;

        $('.hero-image-carousel-container .hero-image-carousel  .hero-image-block').width(containerWidth);
        slider.width(sliderWidth);

        /*     containerWidthSecondary = $('.hero-image-carousel-secondary .hero-image-carousel ').width();
        sliderWidthSecondary = containerWidthSecondary * carousel_num_secondary;

        $('.hero-image-carousel-secondary .hero-image-carousel  .hero-image-block').width(containerWidthSecondary);
        slider.width(sliderWidthSecondary); */

        // New Margin-Left wei 
        carouselInnerLeft = -1 * containerWidth * cur_carousel;


        $('.hero-image-carousel  .hero-image-block').css('width', containerWidth + 'px');
        $('.hero-image-carousel .wrppr').css('width', sliderWidth + 'px');
        $('.hero-image-carousel .wrppr').css('left', carouselInnerLeft + 'px');


        var slide_container_width = (carousel_num * $(".hero-image-carousel-container .hero-image-carousel").width());

        $(".hero-image-carousel-container .hero-image-carousel .wrppr").width(slide_container_width + 'px');
        $(".hero-image-carousel-container .hero-image-carousel").height($(".hero-image-carousel-container .hero-image-carousel .hero-image-block").eq(0).height());



        // Set Carousel to Vertical Center
        captiontopmargin = ($('.hero-image-block').height()) / 2 - 20;
        $('#carousel-controls').css('top', captiontopmargin + 'px');

        // $(".hero-image-carousel-secondary-container .hero-image-caption-area").height($(".hero-image-carousel .hero-image-block").height());

        // img modal box re-position
        if ($(window).width() > 768) {

            $('#img-modal').css('margin-top', $(window).scrollTop() + 30 + 'px');
        } else {
            $('#img-modal').css('margin-top', $(window).scrollTop() + 'px');
        }

        // Set the size for the investors stock iframe
        if ($('.investor-teaser').length > 0) {
            $('#stock-frame').height($('.investor-teaser').height() - 2);
        }
        else {

            $('#stock-frame').height($('#stock-frame').width() * .66);
        }

    });


    $(".img-enlarge").on('click', function () {
        //Get the screen height and width
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
        var curImg = $(this).siblings("img").attr("src");


        $('body').append('<div id="mask"></div>');
        $('body').append('<div id="img-modal"></div>');
        $('#img-modal').append('<a href="#" class="close-btn">Close</a>')
        .append('<img src="' + curImg + '" />');

        if ($(this).siblings(".img-caption").length > 0) {
            var curCaption = $(this).siblings(".img-caption").html();
            $('#img-modal').append('<div class="img-caption">' + curCaption + '</div>');
        }

        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();
        var imgH = winH - 80;
        //Set the popup window to center

        // $('#img-modal').css('left', winW / 2 - $('#img-modal').width() / 2);
        // Added for Chrome and IE
        $('#img-modal').css('top', 0);
        $('#img-modal').css('left', 0);

        // img modal box re-position
        if ($(window).width() > 768) {

            $('#img-modal').css('margin-top', $(window).scrollTop() + 30 + 'px');
        } else {
            $('#img-modal').css('margin-top', $(window).scrollTop() + 'px');
        }

        //	 $('#img-modal img').css('max-height', imgH + 'px'); 
        // 	$('#img-modal img').css('width', 'auto');


        $('#mask').fadeTo("fast", 0.8);
        $('#img-modal').fadeIn();

        $('#mask, .close-btn').click(function () {
            $('#mask').hide();
            $('#img-modal').hide().remove();
            return false;
        });
    }).on('mouseover', function () {
        $(this).css("cursor", "pointer");
    });

    /***** Editorial Teaser - event handlers *****/

    $(".editorial-teaser-container img").hover(function () {
        showTeaserDetails($(this))
    }, function () {
        hideTeaserDetails();
    });

    $(".editorial-teaser-blackband").hover(function () {
        showTeaserDetails($(this).parent().find('img'))
    }, function () {
        hideTeaserDetails();
    });

    $(".editorial-teaser-container .editorial-teaser-details").hover(function () {
        $(this).show();
    }, function () {
        hideTeaserDetails();
    });

    // Add classes to teasers and small features
    $('.small-feature-container li, .editorial-teaser-container li, .items-container div, .rmRootGroup .rmItem').each(function () {
        $(this).addClass('item' + $(this).index());
    });
    $('.small-feature-container li:last-child, .editorial-teaser-container li:last-child').addClass('last');

    function showTeaserDetails(thisTeaser) {

        var curImg = thisTeaser;
        var tHeight = curImg.height();
        var tWidth = curImg.width();
        var tTop = curImg.offset().top;
        var tLeft = curImg.offset().left;

        /*$(".editorial-teaser-container .editorial-teaser-details").hide();
        rollover_feature_call = setTimeout(function () {
        curImg.parents("li").eq(0).children(".editorial-teaser-details")
        .css({ top: tTop, left: tLeft, width: tWidth, height: tHeight })
        .fadeIn();
        }, 550);
        */

        curImg.parents("li").eq(0).children(".editorial-teaser-details")
        //.css({ top: tTop, left: tLeft, width: tWidth, height: tHeight })
        .css({ width: tWidth, height: tHeight })
            .show();
    }

    function hideTeaserDetails() {
        $(".editorial-teaser-container .editorial-teaser-details").hide();
    }

    /*
    $(".editorial-teaser-container .editorial-teaser-details").bind('mouseover', function () {
    $(this).show();
    }).bind('mouseout', function () {
    clearTimeout(rollover_feature_call);
    rollover_feature_hide = setTimeout(function () {
    $(".editorial-teaser-container .editorial-teaser-details").hide();
    }, 200);
    });

    $(".editorial-teaser-container .editorial-teaser-details a").bind('mouseover', function () {
    clearTimeout(rollover_feature_hide);
    $(this).parents(".editorial-teaser-details").show();
    }); 
    */

    /* Career widget behavior */


    $('#keywordsearch #keywordsearch-q').focus(function (ev) {
        if ($(this).val() == 'Keyword(s)') {
            $(this).val('');
        }
    }).blur(function (ev) {
        if ($(this).val() == '') {
            $(this).val('Keyword(s)');
        }
    });

    $('#careers-email-form #email').focus(function (ev) {
        if ($(this).val() == 'Email address') {
            $(this).val('');
        }
    }).blur(function (ev) {
        if ($(this).val() == '') {
            $(this).val('Email address');
        }
    });


});

function init() {
    /* initial set for the images carousel */
 
    if ($(document).find(".hero-image-carousel-container .hero-image-carousel").length > 0) {
        carousel_num = $(".hero-image-carousel-container .hero-image-carousel").find(".hero-image-block").length;
        $(".hero-image-carousel-container .hero-image-carousel .hero-image-block, .hero-image-carousel-container #carousel-controls").width($(".hero-image-carousel-container .hero-image-carousel").width());
				
        var slide_container_width = (carousel_num * $(".hero-image-carousel-container .hero-image-carousel").width());
       
        if (carousel_num > 1) {
            $(".hero-image-carousel-container #carousel-controls .next").fadeIn('fast');
        }
		
        $(".hero-image-carousel-container .hero-image-carousel .wrppr").width(slide_container_width + 'px');
        $(".hero-image-carousel-container .hero-image-carousel").height($(".hero-image-carousel-container .hero-image-carousel .hero-image-block").eq(0).height());

        $(".hero-image-carousel-container .hero-image-caption-area h2").html($(".hero-image-block").eq(0).find(".hero-image-details").children("h2").html());
        $(".hero-image-carousel-container .hero-image-caption-area p").html($(".hero-image-block").eq(0).find(".hero-image-details").children(".desc").html());
        $(".hero-image-carousel-container .hero-image-caption-area .btn-hero-image-callout").html($(".hero-image-block").eq(0).find(".hero-image-details").children(".callout").html());
		
		$(".hero-image-carousel .wrppr").animate({opacity:"1"}, 200);  // Wei: show the wrppr after all images ready
		captiontopmargin = ($('.hero-image-block').height()) / 2 - 20;
		 if ($(".hero-image-carousel-container .hero-image-carousel").width > 860) {
			 $('#carousel-controls').css('top', 222 + 'px'); }else{
        $('#carousel-controls').css('top', captiontopmargin + 'px');
		 }
	}

	/* initial set for the secondary images carousel */
/*    if ($(document).find(".hero-image-carousel-secondary-container .hero-image-carousel").length > 0) {

        carousel_num_secondary = $(".hero-image-carousel-secondary-container .hero-image-carousel").find(".hero-image-block").length;
	    $(".hero-image-carousel-secondary-container .hero-image-carousel .hero-image-block,.hero-image-carousel-secondary-container  #carousel-controls").width($(".hero-image-carousel-secondary-container .hero-image-carousel").width());
		
	    var slide_container_width = (carousel_num_secondary * $(".hero-image-carousel-secondary-container .hero-image-carousel").width());
	  
	    if (carousel_num_secondary > 1) {
	        $(".hero-image-carousel-secondary-container #carousel-controls .next").fadeIn('slow');
	    }
	    $(".hero-image-carousel-secondary-container .hero-image-carousel .wrppr").width(slide_container_width + 'px');
	    $(".hero-image-carousel-secondary-container .hero-image-carousel").height($(".hero-image-carousel-secondary-container .hero-image-carousel .hero-image-block").eq(0).height());

	    $(".hero-image-carousel-secondary-container .hero-image-caption-area h2").html($(".hero-image-block").eq(0).find(".hero-image-details").children("h2").html());
	    $(".hero-image-carousel-secondary-container .hero-image-caption-area p").html($(".hero-image-block").eq(0).find(".hero-image-details").children(".desc").html());
	    $(".hero-image-carousel-secondary-container .hero-image-caption-area .btn-hero-image-callout").html($(".hero-image-block").eq(0).find(".hero-image-details").children(".callout").html());
 		$(".hero-image-carousel .wrppr").animate({opacity:"1"}, 200);  // Wei: show the wrppr after all images ready
	}
*/

    /*
    $("#show-width").text($(window).width());

    $(window).bind("resize", function () {
        $("#show-width").text($(window).width());
    });
    */

    /****** check for the content intro block and add image caption and image enlarge feature ******/
    if ($(document).find(".img-modal").length > 0) {
        $(".img-modal").each(function () {
            var curImg = $(this);
            var imgFloat = curImg.css("float");
            var cloneImg = curImg.clone();

            $('<div class="img-wrppr"></div>').insertBefore(curImg);
            var curImgWrppr = curImg.siblings(".img-wrppr");
            curImg.remove();
            $(curImgWrppr).addClass("img-wrppr-" + imgFloat).append(cloneImg);

            if ((cloneImg.attr("alt") != "") && (cloneImg.attr("alt") != undefined)) {
                $(curImgWrppr).append('<div class="img-caption"><p>' + cloneImg.attr("alt") + '</p></div>');
            }
            //$(curImgWrppr).find('.img-caption').before('<div class="img-enlarge"></div>');
			$(curImgWrppr).find('.img-modal').after('<div class="img-enlarge"></div>');
        });
    }

    /****** Find the last content block on the page and add a class  ******/
    var numBlock = $("#content-col .sf_cols .sf_colsOut .sf_colsIn > div").length;
    var lastBlockID = $("#content-col .sf_cols .sf_colsOut .sf_colsIn > div").eq(numBlock - 1).children("div").eq(0).attr("id");

    if ((lastBlockID != "") && (lastBlockID !== undefined)) {
        $("#" + lastBlockID).children("div").eq(0).addClass("last-block");
    } else {
        $("#content-col .sf_cols .sf_colsOut .sf_colsIn > div").eq(numBlock - 1).children("div").eq(0).addClass("last-block");
    }
	
	 // find long-feature siblings div
   	//	$(".long-feature").siblings(".right-feature").addClass("hide"); 
		
   // find infographics grandparent div
   		$(".greyline").parent(".sf_colsIn").addClass("greyline-parent");
		$(".greyline-parent").parent("div").addClass("no-bottom-border");
   
   // find the last div parent div
   		$(".sf_3cols_1_33").parent("div").addClass("sfContentBlock");
		$(".sf_2cols_1_50").parent("div").addClass("sfContentBlock");
		$(".show-960").parent(".sf_colsIn").parent(".sf_colsOut").parent(".sfContentBlock").addClass("show-960");
		$(".feature-jobs").parent(".sf_colsIn").parent(".sf_colsOut").addClass("feature-jobs-wrapper");
		$(".feature-jobs").parent(".sf_colsIn").parent(".sf_colsOut").parent(".sf_cols").addClass("career-jobs-email-wrapper");
		$(".ipad-show").parent(".sf_colsIn").parent(".sf_colsOut").parent(".ipad-show").removeClass("sfContentBlock");
		
		$("h1").parent("div").addClass("page-title");
		$("h1").parent("div").removeClass("sfContentBlock");
   		$(".editorial-teaser-container").parent("div").addClass("sfContentBlock");
	    $(".small-feature-container").parent("div").addClass("sfContentBlock");
		$(".small-feature-container").parent("div").addClass("small-feature-parent");
		$('.hero-image-carousel-secondary').parent("div").addClass("sfContentBlock");
		$('.hero-image-carousel-container').parent("div").addClass("sfContentBlock");
		$('.hero-image-carousel-secondary').removeClass("sfContentBlock");
		$(".sfVideoBlock").addClass("sfContentBlock");
		$(".hero-video-carousel-container").parent("div").addClass("sfContentBlock");
		$(".small-feature-left-img").parent("div").addClass("small-feature-left-img-parent");
		$(".small-feature-left-img-parent").parent("div").addClass("small-feature-left-img-g-parent");
		$(".small-feature-left-img-parent").parent("div").addClass("sfContentBlock");
		$(".small-feature-left-img").parent("div").removeClass("sfContentBlock");
		$(".small-feature-left-img-parent").parent("div").addClass("sf_cols");
		$(".small-feature-left-img-parent").parent("div").addClass("sfContentBlock");
		
		$(".small-feature-with-image-single").parent("div").addClass("small-feature-with-image-single-parent");
		$(".small-feature-with-image-single").parent("div").removeClass("sfContentBlock");
		$(".small-feature-with-image-single-parent").parent("div").addClass("sf_cols");
		$(".small-feature-with-image-single-parent").parent("div").addClass("sfContentBlock");
		$("#content-col .sf_colsIn div.sfContentBlock:first").addClass("first-div-block");
		$(".sf_colsIn div.sfContentBlock:last").addClass("last-div-block");
		
		$(".small-feature-container.small-feature-left-img.last-block").parent("div").addClass("last-div-block");
 		$(".small-feature-left-img-parent.last-div-block").parent("div").addClass("last-div-block");
		
		$(".small-feature-container.small-feature-with-image-single.last-block").parent("div").addClass("last-div-block");
 		$(".small-feature-with-image-single-parent.last-div-block").parent("div").addClass("last-div-block");
		

		
	// find editorial-teaser-container parent	
		$(".editorial-teaser-container").parent("div").addClass("editorial-teaser-parent");
	// find the parent in custom 2 column layout
		$(".sf_2cols_1in_50").parent(".sf_colsOut").addClass("col2");
		$(".sf_2cols_1_50.col2").parent(".sfContentBlock").addClass("custom2col");
		
	// find the last div in custom 3 column layout
		$(".sfContentBlock.no-bottom-border").parent(".sf_colsIn").addClass("lb1");
		$(".lb1").parent(".sf_colsOut").addClass("lb2");
		$(".lb2").parent("div").addClass("last-div-block");
		
		$(".sf_colsOut.no-bottom-border").parent(".sf_cols").addClass("last-div-block");
		
			// find image modal 3 column sfcontent block parent	
		$(".img-wrppr").parent(".last-div-block").addClass("no-margin-padding");
		$(".img-wrppr").parent(".first-div-block").removeClass("no-margin-padding");

		//****************************************
		// Flexpaper setup

		// Set the size for the investors stock iframe
		if ($('.investor-teaser').length > 0) {
		    $('#stock-frame').height($('.investor-teaser').height()-2);
		}
		else {

		    $('#stock-frame').height($('#stock-frame').width() * .66);
		}


}

function carousel_next(c_carousel, n_carousel) {
    var new_container_left = n_carousel * (-$(".hero-image-carousel-container .hero-image-carousel .hero-image-block").eq(0).width());
    $(".hero-image-carousel-container .hero-image-carousel .wrppr").animate({ left: new_container_left + 'px' }, 400);

    $(".hero-image-carousel-container .hero-image-caption-area h2").html($(".hero-image-carousel-container .hero-image-block").eq(n_carousel).find(".hero-image-details").children("h2").html());
    $(".hero-image-carousel-container .hero-image-caption-area p").html($(".hero-image-carousel-container .hero-image-block").eq(n_carousel).find(".hero-image-details").children(".desc").html());
    $(".hero-image-carousel-container .hero-image-caption-area .btn-hero-image-callout").html($(".hero-image-carousel-container .hero-image-block").eq(n_carousel).find(".hero-image-details").children(".callout").html());

    if (n_carousel > 0) {
        $(".hero-image-carousel-container #carousel-controls .prev").fadeIn('slow');
        if (n_carousel == (carousel_num - 1)) {
            $(".hero-image-carousel-container #carousel-controls .next").fadeOut('fast');
        } else {
            $(".hero-image-carousel-container #carousel-controls .next").fadeIn('slow');
        }
    } else if (n_carousel <= 0) {
        $(".hero-image-carousel-container #carousel-controls .prev").fadeOut('fast');
        $(".hero-image-carousel-container #carousel-controls .next").fadeIn('slow');
    }

}

/*function secondary_carousel_next(c_carousel, n_carousel) {
    var new_container_left = n_carousel * (-$(".hero-image-carousel-secondary-container .hero-image-carousel .hero-image-block").eq(0).width());
    $(".hero-image-carousel-secondary-container .hero-image-carousel .wrppr").animate({ left: new_container_left + 'px' }, 400);

    $(".hero-image-carousel-secondary-container .hero-image-caption-area h2").html($(".hero-image-carousel-secondary-container .hero-image-block").eq(n_carousel).find(".hero-image-details").children("h2").html());
    $(".hero-image-carousel-secondary-container .hero-image-caption-area p").html($(".hero-image-carousel-secondary-container .hero-image-block").eq(n_carousel).find(".hero-image-details").children(".desc").html());
    $(".hero-image-carousel-secondary-container .hero-image-caption-area .btn-hero-image-callout").html($(".hero-image-carousel-secondary-container .hero-image-block").eq(n_carousel).find(".hero-image-details").children(".callout").html());

    if (n_carousel > 0) {
        $(".hero-image-carousel-secondary-container #carousel-controls .prev").fadeIn('slow');
        if (n_carousel == (carousel_num_secondary - 1)) {
            $(".hero-image-carousel-secondary-container #carousel-controls .next").fadeOut('fast');
        } else {
            $(".hero-image-carousel-secondary-container #carousel-controls .next").fadeIn('slow');
        }
    } else if (n_carousel <= 0) {
        $(".hero-image-carousel-secondary-container #carousel-controls .prev").fadeOut('fast');
        $(".hero-image-carousel-secondary-container #carousel-controls .next").fadeIn('slow');
    }

}*/

$(function () {
	
    $('ul.performance-list_inner li:first-child').addClass('first');
    $('ul.performance-list_inner li:odd').addClass('shaded'); 
    // $('ul.performance-list_inner li').addClass('clrfix');

   // $('.performance-list_top-item').parent.click(function () {
	 $('.performance-list').children('li').children('div').click(function () {
        //$(this).siblings().removeClass('active').find('ul.performance-list_inner').hide();
		  
		 
        if (!$(this).parent().hasClass('active')) {
			  $ ('ul.performance-list_inner').slideUp('1000');
			 $('.performance-list li').removeClass('active');
            $(this).parent().addClass('active').find('ul.performance-list_inner').slideDown('2000');
        }
        else {
            $(this).parent().removeClass('active').find('ul.performance-list_inner').slideUp('1000');
        } 

    }).css("cursor", "pointer");
	
	 $('.perf-show-detail').click(function() {
		
         $(this).next('.perf-details').slideDown(400);
		 $(this).slideUp(400);
		  
	
        });
	  $('.perf-details').click(function() {
		
         $(this).prev('.perf-show-detail').slideDown(400);
			 $(this).slideUp(400);
        });
	 
	  $('.show-all-details').click(function() {
		
         $('.perf-details').slideDown(400);
		   $('.perf-show-detail').slideUp(400);
		 $('.hide-all-details').slideDown(400);
		   $(this).slideUp(400);
		  
	
        });
		  $('.hide-all-details').click(function() {
		
         $('.perf-details').slideUp(400);
		  $('.perf-show-detail').slideDown(400);
		  $('.show-all-details').slideDown(400);
		   $(this).slideUp(400);
		  
	
        });
 
    var winloc = window.location.href;
    var num= winloc.charAt(winloc.length-1);
    $('.performance-list>li').eq(num-1).addClass('active').find('ul.performance-list_inner').show();

   

    if (!$('html').hasClass('ie7')) {
        // nav responsive markup
        $(window).bind("resize", function () {
            if ($(window).width() < 960) {
                $('#hdr-area').append($('#main-nav'));
                $('#main-nav').addClass('span16');
            }
            else {
                $('#hdr-area .span14').append($('#main-nav'));
                $('#main-nav').removeClass('span16');
            }

            if ($(window).width() < 768) {
                $('#footer-wrppr').append($('#rights'));
            }
            else {


            }
        });

    }



    if ($(window).width() < 960) {
        $('#hdr-area').append($('#main-nav'));
        $('#main-nav').addClass('span16');
    }

    if ($(window).width() < 769) {
        $('#footer-wrppr').append($('#rights'));
        //$('#widget-follow-box .inner').clone().prependTo($('#footer-wrppr'));
        $('#nav-search .inner').prependTo($('#footer-wrppr'));
        $('#footer-area .inner').wrap('<fieldset></fieldset>');
        $('#footer-area fieldset,#footer-area .inner').wrapInner('<div></div>');
        //$('#footer-area .inner').addClass('social-links');
        //$('#footer-area .social-links div').prepend('<p>FOLLOW US</p>');

    }

    $('#mobile-menu ').click(function () {
        if ($('#main-nav').is(":visible")) {
            $('#main-nav').slideUp('fast');
        }
        else {
            $('#main-nav').slideDown('fast');
        }

        return false;
    });

    /******* Simulate nav tree for secondary navigation   ***********/

    if ($('#side-col .side-nav-siblings').length > 0) {
        $('.side-nav-children').hide();
        $('.side-nav-children ul').addClass('tree-children').clone().appendTo($('.side-nav-siblings a.rtsSelected').parent());


    }

    /******* Create the secondary nav mobile menu under the page header   ***********/

    $('#side-col .rtsUL').first().clone().insertAfter($('h1.page-title, .page-title h1 '));
    $('#content-col .rtsUL').addClass('resp-side-nav');

    $('h1.page-title, .page-title h1').click(function () {
        if ($(window).width() < 769) {
            if ($('.resp-side-nav').is(":visible")) {
                $('.resp-side-nav').slideUp('fast');
            }
            else {
                $('.resp-side-nav').slideDown('fast');
            }
        }
    });

    /******* Add nav action for IE7. Otherwise the top nav radLink menu has problems   ***********/
    /*
    if ($('html').hasClass('ie7')) {
    $('#main-nav .RadMenu_Sitefinity a.rmRootLink span').click(function () {
    window.location = $(this).parent().attr('href');
    //alert('click');
    });

    $('#main-nav .RadMenu_Sitefinity li').click(function () {

    //alert('click li');
    });
       
    }
    
    $('#main-nav a.rmRootLink').each(function () {
    var thisText = $(this).find('span').text();
    $(this).find('span').remove();
    $(this).text(thisText);
    });
    */

    // Scroll to top 

    $(document).ready(function () {

        $('a[href=#top]').click(function () {
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        });

    });
	
	
	 

         $('#content-slide-down-btn').click(function() {
			 //$('.question').css('font-weight','normal');
		//$(this).css('font-weight','bold');
	 	
		$('#content-slide-down-btn').hide();
		$('#content-slide-up-btn').show();
         $('#content-slide-down').slideDown(700);
		 $('html,body').animate({
    scrollTop: '+=' + $('.videoWrapper').offset().top + 'px'
}, 'slow');
	
        });
 
		  $('#content-slide-up-btn').click(function() {
			 //$('.question').css('font-weight','normal');
		//$(this).css('font-weight','bold');
	 	$('#content-slide-up-btn').hide();
		$('#content-slide-down-btn').show();
         $('#content-slide-down').slideUp(700);
	
        });

 });

 function GoogleEventTracking() {
     $('.wei-slider-menu .OurCompany a').click(function () {
        _gaq.push(['_trackEvent', 'Click', 'Our-Company-Homepage']);
    });
    $('.wei-slider-menu .ShaleEnergy a').click(function () {
        _gaq.push(['_trackEvent', 'Click', 'Shale-Energy-Homepage']);
    });
    $('.wei-slider-menu .Offshore a').click(function () {
        _gaq.push(['_trackEvent', 'Click', 'Off-shore-Homepage']);
    });
    $('.wei-slider-menu .Innovation a').click(function () {
        _gaq.push(['_trackEvent', 'Click', 'Innovation -Homepage']);
    });

    $('a[href="http://www.hessenergy.com/"]').click(function () {
        _gaq.push(['_trackEvent', 'Exit', 'http://www.hess.com/Sitefinity/WebsiteTemplates/HessBootstrap/JS/Hessenergy.com']);
    });

    $('a[href="http://www.hessexpress.com/"]').click(function () {
        _gaq.push(['_trackEvent', 'Exit', 'http://www.hess.com/Sitefinity/WebsiteTemplates/HessBootstrap/JS/Hessexpress.com']);
    });

    $('a[href="http://www.hesstoytruck.com/"]').click(function () {
        _gaq.push(['_trackEvent', 'Exit', 'http://www.hess.com/Sitefinity/WebsiteTemplates/HessBootstrap/JS/Hesstoytruck.com']);
    });

    $('#toolbox ul li#icon-email a').click(function () {
        _gaq.push(['_trackEvent', 'Share', 'Email-Page']);
    });

    $('#toolbox ul li#icon-print').click(function () {
        _gaq.push(['_trackEvent', 'Share', 'Print-Page']);
    });

    $('.addthis_button_facebook').click(function () {
        _gaq.push(['_trackEvent', 'Share', 'Facebook-Share']);
    });

    $('.addthis_button_linkedin').click(function () {
        _gaq.push(['_trackEvent', 'Share', 'Linkedin-Share']);
    });

    $('.addthis_button_blogger').click(function () {
        _gaq.push(['_trackEvent', 'Share', 'Blogger-Share']);
    });

    $('.addthis_button_twitter').click(function () {
        _gaq.push(['_trackEvent', 'Share', 'Tweet']);
    });

    $('.addthis_button_google_plusone').click(function () {
        _gaq.push(['_trackEvent', 'Share', 'Google-Plus-Share']);
    });

    $('.addthis_button_digg').click(function () {
        _gaq.push(['_trackEvent', 'Share', 'Digg-Share']);
    });

    $('#hess-map-tabs .operations').click(function () {
        _gaq.push(['_trackEvent', 'Interaction', 'Global-Ops-Map-View']);
    });

    $('#hess-map-tabs .exploration').click(function () {
        _gaq.push(['_trackEvent', 'Interaction', 'Exploration-Map-View']);
    });

    $('#hess-map-tabs .production').click(function () {
        _gaq.push(['_trackEvent', 'Interaction', 'Production-Map-View']);
    });

    $('#hess-map-tabs .refining').click(function () {
        _gaq.push(['_trackEvent', 'Interaction', 'Refining-Map-View']);
    });

    $('#hess-map-tabs .supply').click(function () {
        _gaq.push(['_trackEvent', 'Interaction', 'Supply-and-Terminals-Map-View']);
    });

    $('#hess-map-tabs .energy').click(function () {
        _gaq.push(['_trackEvent', 'Interaction', 'Energy-Marketing-Map-View']);
    });

    $('#hess-map-tabs .retail').click(function () {
        _gaq.push(['_trackEvent', 'Interaction', 'Retail-Map-View']);
    });

    $('.img-enlarge').click(function () {
        _gaq.push(['_trackEvent', 'Interaction', 'Graphic-View']);
    });

    $('.vjs-big-play-button').click(function () {
        _gaq.push(['_trackEvent', 'Video', 'Values-Video']);
    });

    $('#history-nav-container ul li a').click(function () {
        _gaq.push(['_trackEvent', 'Interaction', 'Company-History-Graphic']);
    });

    /*    
    _gaq.push(['_trackEvent', 'Interaction', 'Global-Offices-Houston']);
    _gaq.push(['_trackEvent', 'Interaction', 'Global-Offices-Woodbridge']);
    _gaq.push(['_trackEvent', 'Interaction', 'Global-Offices-NY']);
    _gaq.push(['_trackEvent', 'Interaction', 'Global-Offices-London']);
    _gaq.push(['_trackEvent', 'Interaction', 'Global-Offices-Kuala-Lumpur']);
    
    _gaq.push(['_trackEvent', 'Form', 'Contact-Company']);
    

    _gaq.push(['_trackEvent', 'Video', 'Student-and-Grad-Video']);
    _gaq.push(['_trackEvent', 'Video', 'Life-at-Hess-Video']);
    */

}

function GoogleEventTrackingUniversal() {

    // External link tracking

    $('a[href^="//"],a[href^="http"]').click(function () {
        ga('send', 'event', 'Exit', $(this).attr('href'));
        //console.log('EXIT: '+$(this).attr('href'));
        //return false;
    });

    // PDF event tracking
    
    $('http://www.hess.com/Sitefinity/WebsiteTemplates/HessBootstrap/JS/a.pdf').click(function () {
        ga('send', 'event', 'Download', $(this).attr('href'));
        //console.log('DOWNLOAD: ' + $(this).attr('href'));
        //return false;
    });

    // Video tracking is handled in the video widget templates

    // Homepage

    $('.wei-slider-menu .OurCompany a').click(function () {
        ga('send', 'event', 'Interaction', 'Homepage-Carousel-Our-Company');
    });
    $('.wei-slider-menu .ShaleEnergy a').click(function () {
        ga('send', 'event', 'Interaction', 'Homepage-Carousel-Shale-Energy');

    });
    $('.wei-slider-menu .Offshore a').click(function () {
        ga('send', 'event', 'Interaction', 'Homepage-Carousel-Offshore');

    });
    $('.wei-slider-menu .Innovation a').click(function () {
        ga('send', 'event', 'Interaction', 'Homepage-Carousel-Innovation');

    });

    // Operations Map

    $('#hess-map-tabs .operations').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-Global-Ops');
    });

    $('#hess-map-tabs .exploration').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-Exploration');
    });

    $('#hess-map-tabs .production').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-Production');
    });

    $('#hess-map-tabs .shale').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-Shale-Energy');
    });



    $('.unitedstates-continental').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-United-States');
    });

    $('unitedstates-continental-bakkenshale').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-US-Bakken');
    });

    $('.unitedstates-continental-permianbasin').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-US-Permian');
    });

    $('.unitedstates-continental-uticashale').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-US-Utica');
    });

    $('.unitedstates-gulfofmexico').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-Gulf-Mexico');
    });

    $('.norway-valhall').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-Norway');
    });

    $('.denmark-southarne').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-Denmark');
    });

    $('.algeria-gassielagrebredevelopmentproject').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-Algeria');
    });

    $('.libya').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-Libya');
    });

    $('.ghana-deepwatertano-capethreepoints').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-Ghana');
    });

    $('.equatorialguinea-okumecomplexandceibafield').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-Eq-Guinea');
    });

    $('.iraq').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-Iraq');
        
    });

    $('.china').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-China');
    });

    $('.malaysia-northmalaybasin').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-Malaysia');
    });

    $('.malaysia-thailandjda-jda').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-JDA');
    });

    $('.australia-equus').click(function () {
        ga('send', 'event', 'Interaction', 'Operations-Map-Australia');
    });

    // Various interactions

    $('#history-nav-container ul li a').click(function () {
        ga('send', 'event', 'Interaction', 'Feature-Hess-History-Timeline');
    });

    $('.hero-image-carousel-secondary').click(function () {
        ga('send', 'event', 'Interaction', 'Slideshow-Shale-Production');
    });

    $('a.trigger').click(function () { //GRI index
        ga('send', 'event', 'Interaction', 'Feature-2012-GRI-Index');        
    });  


    // Sharing toolbox

    $('#toolbox ul li#icon-email a').click(function () {
        ga('send', 'event', 'Share', 'Utility-Link-Email');
    });

    $('#toolbox ul li#icon-print').click(function () {
        ga('send', 'event', 'Share', 'Utility-Link-Print');
    });

    $('.addthis_button_facebook').click(function () {
        ga('send', 'event', 'Share', 'Utility-Link-Share-Facebook');
    });

    $('.addthis_button_linkedin').click(function () {
        ga('send', 'event', 'Share', 'Utility-Link-Share-LinkedIn');
    });

    $('.addthis_button_blogger').click(function () {
        ga('send', 'event', 'Share', 'Utility-Link-Share-Blogger');
    });

    $('.addthis_button_twitter').click(function () {
        ga('send', 'event', 'Share', 'Utility-Link-Share-Twitter');
    });

    $('.addthis_button_google_plusone').click(function () {
        ga('send', 'event', 'Share', 'Utility-Link-Share-Google-Plus');
    });

    $('.addthis_button_digg').click(function () {
        ga('send', 'event', 'Share', 'Utility-Link-Share-Digg');
        
    });

   

   
   


}
