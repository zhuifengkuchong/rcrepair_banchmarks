function getMaxHeight($els) {
	var maxHeight = 0;
	$els.each(function() {
		var height = $(this).height();
		if(height > maxHeight) { maxHeight = height; }
	});
	return maxHeight;
}

function resizeImageBlockElements($els) {
	$els.css("min-height", "inherit");
	var maxHeight = 0;
	$els.each(function() {
			var height = $(this).height();
			if(height > maxHeight) { maxHeight = height; }
	});
	$els.css("min-height", maxHeight + "px");

}

function resizeImageBlockHeightElements() {
	resizeImageBlockElements($(".image-blocks h2"));
	resizeImageBlockElements($(".image-blocks h3"));
	resizeImageBlockElements($(".image-blocks .description"));
}
function resizeImageBlocksNoNthChildSupport() {
	// If no nth-child selector, use jquery to provide image block restructuring
	var width = $(window).width();
	log("adjusting column-up: " + width);
	if (width > 900) {
		$(".block-grid.two-up-at-900>li:nth-child(2n+1)").css("clear", "none");
		$(".block-grid.two-up-at-900>li:nth-child(3n+1)").css("clear", "left");
	} else if (width >= 701 && width <= 900) {
		$(".block-grid.two-up-at-900>li:nth-child(2n+1)").css("clear", "left");
		$(".block-grid.two-up-at-900>li:nth-child(3n+1)").css("clear", "none");
	}
}

function resizeImageBlocks() {
	if (!isNthChildSupported()) {
		resizeImageBlocksNoNthChildSupport();
		// "Immediate" - Wait 0.2 seconds for css to apply before recalculating height
		window.setTimeout(function () {
			resizeImageBlockHeightElements();
		}, 200);
		// "Final" - Wait 1.5 seconds to approximate a "final" calculation for two-up/three-up
		window.setTimeout(function () {
			resizeImageBlocksNoNthChildSupport();
		}, 1500);
	} else {
		// Nth-child supported, recalculate height immediately
		resizeImageBlockHeightElements();
	}
}


$(document).ready(function () {
	$(window).smartresize(function () { // from app.js
		resizeImageBlocks();
	});
});
$(window).load(function () {
	if($.browser.msie) {
		var version = parseFloat($.browser.version);
		if(version <= 8) {
			
		}
	}
	resizeImageBlocks();
});


$(document).unload(function() { log("unload"); });
