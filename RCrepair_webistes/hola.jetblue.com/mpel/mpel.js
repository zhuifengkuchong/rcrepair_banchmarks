MpElDs = {
    'http://hola.jetblue.com/mpel/hola.jetblue.com': 'es',
    'http://hola.jetblue.com/mpel/www.jetblue.com': 'en',
    'http://hola.jetblue.com/mpel/jetblue.com': 'en',
    'http://hola.jetblue.com/mpel/www-stg.jetblue.com': 'en',
    'http://hola.jetblue.com/mpel/book.jetblue.com': 'en',
	'http://hola.jetblue.com/mpel/trueblue.jetblue.com':'en',
	'http://hola.jetblue.com/mpel/mobile.jetblue.com':'en',
	'http://hola.jetblue.com/mpel/movil.jetblue.com':'es',
	'http://hola.jetblue.com/mpel/help.jetblue.com':'en',
	'http://hola.jetblue.com/mpel/vacations.jetblue.com':'en',
	'http://hola.jetblue.com/mpel/redeem.getaways.jetblue.com':'en',
	'http://hola.jetblue.com/mpel/ayuda.jetblue.com':'es',
	'http://hola.jetblue.com/mpel/vacaciones.jetblue.com':'es',
	'http://hola.jetblue.com/mpel/canjear.getaways.jetblue.com':'es'
};
if (!RegExp("MP_LANG=" + MpElDs[location.host]).test(document.cookie)) {
    MpElD = "http://hola.jetblue.com/";
	if (location.host.toString().indexOf('http://hola.jetblue.com/mpel/mobile.jetblue.com') != -1) {
       MpElD = "http://movil.jetblue.com/";
    }
		if (location.host.toString().indexOf('http://hola.jetblue.com/mpel/vacations.jetblue.com') != -1) {
       MpElD = "http://vacaciones.jetblue.com/";
    } 
		if (location.host.toString().indexOf('http://hola.jetblue.com/mpel/redeem.getaways.jetblue.com') != -1) {
       MpElD = "http://canjear.getaways.jetblue.com/";
    } 
		if (location.host.toString().indexOf('http://hola.jetblue.com/mpel/help.jetblue.com') != -1) {
       MpElD = "http://ayuda.jetblue.com/";
    }  
    MpL = navigator.browserLanguage;
    if (!MpL) MpL = navigator.language;
    document.write(decodeURIComponent("%3Cscript src='") + MpElD + "/mpel.js?href=" + encodeURIComponent(location.href) + "&ref=" + encodeURIComponent(document.referrer) + "&lang=" + MpL + "' type='text/javascript'" + decodeURIComponent("%3E%3C/script%3E"))
};