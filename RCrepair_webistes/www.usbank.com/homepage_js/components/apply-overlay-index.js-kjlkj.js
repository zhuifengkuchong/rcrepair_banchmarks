$(document).ready(function(){
	$('#prequalBlock').removeClass('modalBlock').removeClass('modalShow');
	$('#prequalBlock').css({'position':'absolute'})
	$('#prequalBlock').css({'display':'none'})
	$('#prequalContent').removeClass('displayInlineBlock');
	$('#prequalContent').css({'padding':'30px 58px 40px'})

	
$(window).live("resize", function () {
	if ($('#prequalBlock').hasClass('prequalShow')) {
		prequalmodalResize();
	}
});

function prequalmodalResize() {
	Mwidth = 705;
	var windowWidth1;
	if (window.innerWidth) {
		windowWidth1 = window.innerWidth;
	} else {
		windowWidth1 = document.documentElement.clientWidth;
	}
	var left = (windowWidth1 - (Mwidth + 30)) / 2;
	maskObj.width(windowWidth1).height($(document).height()).show();
	$("#prequalBlock").css({
		"left" : "0px",
		"right" : "0px",
		"margin" : "0px auto",
		"width" : "50%",
		"top" : $(window).scrollTop() + 50,
		"z-index" : "9999"
	}).show().addClass('prequalShow');
	$('#prequalBlock .forZip').hide()
}

$("#closeModalBrowserWindow, #closeModalBrowserImage").bind("click", function () {
	prequalCloseFun();
});

function prequalCloseFun() {
	$("#prequalBlock").removeClass('prequalShow').hide();
	$('.mask').hide();
}
$('.openAccountButton').live('click',function(e){
$('#OverlayHide3').hide();
	e.preventDefault(); 
	var fragmentURL;
 	fragmentURL = "https://www.usbank.com/htmlFragments/workplace/apply-overlay-index.html";
	
	if (location.hostname.match("https://www.usbank.com/homepage_js/components/us.bank-dns.com")) {
		fragmentURL	 = "/html/usbank_en" +fragmentURL;
	}
		
		$.ajax({
			url: fragmentURL, 
			async: true, 
			dataType: "html", 
			success: function(html) 
			{ 
				
			  $('#prequalContent').html(html);prequalmodalResize();
			  uatLinks();
			}
		});
});
  
 /*** UAT links ***/
	uatLinks = function(){
		if (location.hostname.match("uat3") || location.hostname.match("https://www.usbank.com/homepage_js/components/us.bank-dns.com")) {
			$('#prequalContent a[href*="apply.usbank.com/apply/"]').each(function(){
				$(this).attr('href',$(this).attr('href').replace("apply","uat4-apply"));
			});
		}
	};
});
       