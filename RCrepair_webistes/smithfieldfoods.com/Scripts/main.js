
$(function () {
    // adjust left column to extend till it hits the footer.  
    //adjust for padding +60;
  

    var searchTxt = $("#txtSearch").val();
    $("#txtSearch").focus(function () {
        if ($("#txtSearch").val() == searchTxt) {
            $("#txtSearch").val("");
        }
    });

    $("#txtSearch").blur(function () {
        if ($(this).val() == "") {

            $(this).val(searchTxt);
        }
    });

    // remove bullets from utility and footer in case
    // there is a lingering bullet at the end of the list.

});

var shorter = false; //boolean to hold a value of whether the window is smaller than the content and footer
var contentHeight;

/**
* called from each rendered page.  
* had to call indiviually because some pages content area is dependent on the loading
* external javascript libraries.
* 
* @param second is this called from the accordion? footer placements gets messed up when this is called multiple times on accordion pages.
*/
function beginPositioning() {

   

    $("footer").addClass("bottom");

    
    if (contentHeight == undefined) {
        if ($("#content").offset().top + $("#content").height() + 150 > $("#main-nav").offset().top + $("#main-nav").height() + 150) {
            contentHeight = $("#content").offset().top + $("#content").height() + 150;
        } else {
            contentHeight = $("#main-nav").offset().top + $("#main-nav").height() + 150;
        }
        
    }
   // $("#main").append(contentHeight + " ");
    positionFooter()

    /**
    * on window resize, sets css for the footer
    */
    
    $(window).resize(function () {
        if ($("#content").offset().top + $("#content").height() + 150 > $("#main-nav").offset().top + $("#main-nav").height() + 150) {
            contentHeight = $("#content").offset().top + $("#content").height() + 150;
        } else {
            contentHeight = $("#main-nav").offset().top + $("#main-nav").height() + 150;
        }
        positionFooter()
        $("#col-left").css("height", $("footer").offset().top + $("footer").height());
    });
    
    $("#col-left").css("height", $("footer").offset().top + $("footer").height());

}

function positionFooter() {
    if ($(window).height() < contentHeight) {

        //$("#main").append("smaller ");
        $("footer").removeClass("bottom");
        $("footer").addClass("stop");
        $("footer").css("top", contentHeight - 75);

    } else if ($(window).height() >= contentHeight) {
        $("footer").addClass("bottom");
        if ($("footer").hasClass("stop")) {
            // $("#main").append("larger ");
            $("footer").removeClass("stop");
            $("footer").attr("style", "");

        }
    }
}

/**
* parses the querystring and returns an array of items.
*
*/
function getQueryString() {
    var result = {}, queryString = location.search.substring(1),
      re = /([^&=]+)=([^&]*)/g, m;

    while (m = re.exec(queryString)) {
        result[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
    }

    return result;
}

