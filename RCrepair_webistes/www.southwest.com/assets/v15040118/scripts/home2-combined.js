
/*! persist.js */
swa.Persist=Backbone.Collection.extend({saveData:function(a){var b="";
_.each(this.models,function(d){try{b+=JSON.stringify(d)+"|"
}catch(c){}});
sessionStorage[a]=b
},readData:function(a){var e=sessionStorage[a],f,b,d,c;
if((e)&&(e!=="")){f=e.split("|");
b=f.length;
for(c=0;
c<b-1;
c+=1){d=JSON.parse(f[c]);
this.add(d)
}}},addModel:function(b){var a=this.findWhere({name:b.get("name")});
if(a!==undefined){this.remove(a,{silent:true})
}this.add(b)
},getModelData:function(a){var c={},b;
b=this.findWhere({name:a});
if(b!==undefined){c=b.attributes
}return c
}});
swa.persist=new swa.Persist([]);
/*! application-resources-common.js */
swa.applicationResourcesCommon={airBookingForm:{originLabel:"Departure city or airport code",destinationLabel:"Arrival city or airport code",calendarTitle1:"Select depart date",calendarTitle2:"Select return date",calendarTooltip1:"Next, select depart date",calendarTooltip2:"Next, select return date",departLabel:"Depart {0}",departDateLabel:"Depart date",returnLabel:"Return {0}",returnDateLabel:"Return date",adultTitleLabel:"Adults",adultTitleDetailsLabel:"(Age 2+)",seniorTitleLabel:"Seniors",seniorTitleDetailsLabel:"(Age 65+)",error:{originLabel:"Enter an origin airport.",destinationLabel:"Enter a destination airport.",departDateLabel:"Enter a valid depart date (mm/dd format).",returnDateLabel:"Enter a valid return date (mm/dd format)."}},hotelBookingForm:{destinationLabel:"Destination",checkInLabel:"Check-in {0}",calendarTitle1:"Select check-in date",calendarTitle2:"Select check-out date",calendarTooltip1:"Next, select check-in date",calendarTooltip2:"Next, select check-out date",checkInDateLabel:"Check-in date",checkOutLabel:"Check-out {0}",checkOutDateLabel:"Check-out date",adultLabel:"Adults",childrenLabel:"Children",error:{destinationLabel:"Enter a destination.",checkInDateLabel:"Enter a valid check-in date (mm/dd format).",checkOutDateLabel:"Enter a valid check-out date (mm/dd format)."}},carBookingForm:{pickUpLocationLabel:"Pick-up location",pickUpLabel:"Pick-up {0}",pickUpDateLabel:"Pick-up date",calendarTitle1:"Select pick-up date",calendarTitle2:"Select drop-off date",dropOffLocationLabel:"Drop-off location",dropOffLabel:"Drop-off {0}",dropOffDateLabel:"Drop-off date",error:{pickUpLocationLabel:"Enter a pick-up location.",dropOffLocationLabel:"Enter a drop-off location.",pickUpDateLabel:"Enter a valid pick-up date (mm/dd format).",dropOffDateLabel:"Enter a valid drop-off date (mm/dd format)."}},vacationsBookingForm:{originLabel:"Departure city or airport code",destinationLabel:"Arrival city or airport code",calendarTitle1:"Select depart date",calendarTitle2:"Select return date",calendarTooltip1:"Next, select depart date",calendarTooltip2:"Next, select return date",departLabel:"Depart {0}",departDateLabel:"Depart date",returnLabel:"Return {0}",returnDateLabel:"Return date",adultTitleLabel:"Adults",childrenTitleLabel:"Children",childrenAgesLabel:"Children's ages",error:{originLabel:"Enter an origin airport.",destinationLabel:"Enter a destination airport.",departDateLabel:"Enter a valid depart date (mm/dd format).",returnDateLabel:"Enter a valid return date (mm/dd format).",childAges:"Enter children's ages."}},checkInForm:{confirmationNumberLabel:"Confirmation number",firstNameLabel:"First name",lastNameLabel:"Last name",error:{confirmationNumberLabel:"Enter the confirmation number.",firstNameLabel:"Enter the passenger's first name.",lastNameLabel:"Enter the passenger's last name."}},changeFlightForm:{confirmationNumberLabel:"Confirmation number",firstNameLabel:"First name",lastNameLabel:"Last name",error:{confirmationNumberLabel:"Enter the confirmation number.",firstNameLabel:"Enter the passenger's first name.",lastNameLabel:"Enter the passenger's last name."}},flightStatusForm:{originAirportLabel:"Departure city or airport code",destinationAirportLabel:"Arrival city or airport code",departDateLabel:"Travel date <i>{0}</i>, {1}",error:{originAirportLabel:"Enter an origin airport.",destinationAirportLabel:"Enter a destination airport."}},commonDateDisplay:{yesterdayDateShort:"Yesterday, {0}",todayDateShort:"Today, {0}",tomorrowDateShort:"Tomorrow, {0}"},dateFormats:{monthDayShort:"MM/DD",monthDayYearLong:"ll",monthDayYearShort:"MM/DD/YYYY"}};
/*! date.js */
swa.YESTERDAY=moment().subtract("days",1);
swa.TODAY=moment();
swa.TOMORROW=moment().add("days",1);
swa.MONTH_DAY_YEAR_SYSTEM_FORMAT="MM/DD/YYYY";
swa.DAY_OF_WEEK_FORMAT="dddd";
swa.LOCALIZED_MONTH_DAY_SHORT_FORMAT=swa.getI18nString(swa.applicationResourcesCommon,"dateFormats.monthDayShort");
swa.getDisplayFormattedWeekdayMonthDayForYesterday=function(){return swa.getI18nString(swa.applicationResourcesCommon,"commonDateDisplay.yesterdayDateShort",swa.YESTERDAY.format(swa.LOCALIZED_MONTH_DAY_SHORT_FORMAT))
};
swa.getDisplayFormattedWeekdayMonthDayForToday=function(){return swa.getI18nString(swa.applicationResourcesCommon,"commonDateDisplay.todayDateShort",swa.TODAY.format(swa.LOCALIZED_MONTH_DAY_SHORT_FORMAT))
};
swa.getDisplayFormattedWeekdayMonthDayForTomorrow=function(){return swa.getI18nString(swa.applicationResourcesCommon,"commonDateDisplay.tomorrowDateShort",swa.TOMORROW.format(swa.LOCALIZED_MONTH_DAY_SHORT_FORMAT))
};
swa.getSystemFormattedDateForYesterday=function(){return swa.YESTERDAY.format(swa.MONTH_DAY_YEAR_SYSTEM_FORMAT)
};
swa.getSystemFormattedDateForToday=function(){return swa.TODAY.format(swa.MONTH_DAY_YEAR_SYSTEM_FORMAT)
};
swa.getSystemFormattedDateForTomorrow=function(){return swa.TOMORROW.format(swa.MONTH_DAY_YEAR_SYSTEM_FORMAT)
};
/*! location-collection.js */
swa.LocationCollection=Backbone.Collection.extend({IS_NOT_PARENT:false,IS_PARENT:true,IS_NOT_SELECTABLE:false,IS_SELECTABLE:true,IS_NOT_SUBITEM:false,IS_SUBITEM:true,IS_NOT_LAST_SUBITEM:false,IS_LAST_SUBITEM:true,getSearchResult:function(a){return this.searchAllForTerm(a)
},rearrange:function(e){var h=e.models,g=h.length,d,b,a,f,c;
if(g>0){this.sortPriorities(e,0,g-1);
d=e.models[0].get("priority");
b=0;
for(c=d;
((c>=0)&&(b<g));
c-=1){a=this.findLastModelOfThisPriority(e,b,g-1);
this.sortAlphabetical(e,b,a);
b=a+1
}b=0;
while(b<g){b=this.findNextParent(e,b);
if(b<g){b+=1;
a=this.findLastChild(e,b);
if(b<a){this.sortChildPriorities(e,b,a);
d=h[b].get("priority");
for(c=d;
((c>=0)&&(b<a));
c-=1){f=this.findLastChildModelOfThisPriority(e,b,a);
this.sortChildAlphabetical(e,b,f);
b=f+1
}}}}}},findNextParent:function(b,a){var d=b.models,c=d.length;
while((a<c)&&(d[a].get("isParent")===false)){a+=1
}return a
},findLastChild:function(b,a){var d=b.models,c=d.length;
while((a<c)&&(d[a].get("isSubitem"))){a+=1
}return a-1
},findLastChildModelOfThisPriority:function(e,c,b){var f=e.models,d=f[c].get("priority"),a;
if(c<=b){a=f[c];
while((a)&&((a.get("priority")===d)&&a.get("isSubitem"))){c+=1;
a=f[c]
}if((!a)||(a.get("priority")!==d)||(a.get("isSubitem")===false)){c-=1
}}return c
},sortPriorities:function(i,d,c){var a=i.models,l=true,b,j,g,h,f,k;
while(l){l=false;
k=d;
while(k<c){h=e(k,c);
f=e(k+1,c);
if(f){k=i.indexOf(f);
b=h.get("priority");
j=f.get("priority");
if(b<j){k=i.indexOf(h);
g=1;
if(f.get("isParent")){g=this.getNumberChildren(i,i.indexOf(f)+1,c)+1
}this.swap(i,g,h,f);
k+=g;
l=true
}}else{k=c
}}}function e(m,o){var n=a[m];
while((m<o)&&(n.get("isSubitem"))){m=m+1;
n=(m<=o)?a[m]:null
}return n
}},sortAlphabetical:function(h,c,b){var a=h.models,n=a.length,m=true,k,i,f,g,e,l,j;
j=h.models[c].get("priority");
while(m){m=false;
l=c;
while(l<b){g=d(l,n,j);
e=d(l+1,n,j);
if(e){l=h.indexOf(e);
k=g.get("value");
i=e.get("value");
if(k>i){l=h.indexOf(g);
f=1;
if(e.get("isParent")){f=this.getNumberChildren(h,h.indexOf(e)+1,b)+1
}this.swap(h,f,g,e);
l+=f;
m=true
}}else{l=b
}}}function d(o,s,q){var p=a[o],r=true;
while(r){if(p.get("isSubitem")){o=o+1;
if(o<s){p=a[o]
}else{p=null;
r=false
}}else{r=false
}if(!r){if((p)&&(p.get("priority")!==q)){p=null
}}}return p
}},findLastModelOfThisPriority:function(e,c,b){var f=e.models,d=f[c].get("priority"),a;
if(c<=b){a=f[c];
while((a)&&((a.get("priority")===d)||a.get("isSubitem"))){c+=1;
a=f[c]
}c-=1
}return c
},sortChildPriorities:function(g,d,c){var a=g.models,k=true,b,h,f,e,j,i;
while(k){k=false;
j=d;
while(j<c){f=a[j];
e=a[j+1];
if(e){j+=1;
b=f.get("priority");
h=e.get("priority");
if(b<h){this.swap(g,1,f,e);
k=true
}}else{j=c
}}}},sortChildAlphabetical:function(g,c,b){var a=g.models,l=true,j,h,f,e,k,i;
i=g.models[c].get("priority");
while(l){l=false;
k=c;
while(k<b){f=a[k];
d(k,b,i);
if(e){k+=1;
j=f.get("value");
h=e.get("value");
if(j>h){this.swap(g,1,f,e);
l=true
}}else{k=b
}}}function d(m,o,n){m=m+1;
e=a[m];
if((m>o)||(e.get("priority")!==n)){e=null
}}},getNumberChildren:function(e,b,d){var g=e.models,f=g[b],a=b,c=0;
while((f)&&(f.get("isSubitem"))){c+=1;
if(a<=d){a=e.indexOf(f)+1;
f=g[a]
}else{f=null
}}return c
},swap:function swap(g,f,d,b){var c=g.indexOf(d),a=g.indexOf(b),e;
for(e=0;
e<f;
e+=1){g.remove(b,{silent:true});
g.add(b,{at:c+e});
a+=1;
b=g.models[a]
}},setEndSeparators:function(c){var e=c.models,d=e.length,a=0,b;
b=e[a];
while(a<d-1){if(b.get("isParent")){a+=1;
b=e[a];
while((b)&&(b.get("isSubitem"))){a+=1;
b=e[a]
}e[a-1].set({isLastSubitem:true})
}else{a+=1;
b=e[a]
}}},filterRedundantSeparators:function(c){var e=false,d=null,b,a;
c.each(function(f){b=f.get("isParent");
if((e)&&(b)){d.set("isLastSubitem",false)
}e=f.get("isSubitem");
d=f
});
a=c.last();
if(a){a.set("isLastSubitem",false)
}},highlightSearchTerms:function(b,a){b.each(function(c){c.set("value",swa.highlightText(c.get("value"),a,true))
})
}});
/*! airport-collection.js */
swa.AirportCollection=swa.LocationCollection.extend({filterKey:"",filterRoutes:null,initialize:function(){this.initAirports()
},setAirportFilter:function(b){var a=null;
if(b.length===3){a=this.get(b)
}if(a){this.filterKey=b;
this.filterRoutes=a.get("routesServed")
}else{this.filterKey="";
this.filterRoutes=null
}},getAirportName:function(b){var a=this.findWhere({id:b});
return(a)?a.get("displayName"):""
},searchAllForTerm:function(a){var d=new Backbone.Collection(),c=new Backbone.Collection(),e,b;
a=a.toUpperCase();
b=this.findWhere({id:a});
if(b){if(this.isStationInRoute(b)){d.add(new swa.MenuModel({priority:swa.MATCH_AIRPORT,value:b.get("displayName")}));
this.addResultsToList(b,d,swa.MATCH_AIRPORT)
}}_.each(this.models,function(f){e=this.searchForTerm(f,a);
if(e!==swa.MATCH_NONE){if(this.isStationInRoute(f)){d.add(new swa.MenuModel({priority:e,value:f.get("displayName")}));
this.addResultsToList(f,d,e)
}}},this);
this.filterDuplicates(d,c);
this.rearrange(c);
this.setEndSeparators(c);
this.filterRedundantSeparators(c);
this.highlightSearchTerms(c,a);
return c
},isAnyMatch:function(c){var d=true,a=false,b=0,e;
if(c.length===3){a=(this.findWhere({id:c})!==undefined)
}else{while(d){e=this.searchForTerm(this.models[b],c);
if(e!==swa.MATCH_NONE){a=true;
d=false
}if(b<this.models.length-1){b+=1
}else{d=false
}}}return a
},searchForTerm:function(d,f){var c=d.get("displayName"),b=swa.MATCH_NONE,a,e,g;
if(d.get("id")===f){b=swa.MATCH_AIRPORT
}else{e=d.get("children");
if((typeof e!=="undefined")&&(e.length===0)){a=c.toUpperCase().indexOf(f);
if(a===0){b=swa.MATCH_START_OF_NAME
}else{if(a>0){b=swa.MATCH_NAME
}}}}if(b===swa.MATCH_NONE){g=d.get("altSearchNames");
if(typeof g!=="undefined"){_.each(g,function(h){if(h.toUpperCase().indexOf(f)!==-1){b=swa.MATCH_ALT_NAME
}})
}}return b
},addResultsToList:function(d,e,g){var c=d.get("children"),a=d.get("displayName"),b=d.get("parents");
if(c.length){f.call(this,g)
}else{if(b.length){d=this.findWhere({id:b[0]});
c=d.get("children");
a=d.get("displayName");
f.call(this,g)
}else{e.add(new swa.MenuModel({priority:g,value:a}))
}}function f(h){e.add(new swa.MenuModel({isParent:true,isSelectable:false,priority:h,value:a}));
_.each(c,function(i){a=this.findWhere({id:i}).get("displayName");
e.add(new swa.MenuModel({isSubitem:true,priority:swa.MATCH_CHILD,value:a}))
},this)
}},isStationInRoute:function(c){var a=true,b;
if(c){if((this.filterRoutes)&&($.inArray(c.get("id"),this.filterRoutes)===-1)){a=false;
b=c.get("children");
if(b.length!==0){_.each(b,function(d){if($.inArray(d,this.filterRoutes)!==-1){a=true
}},this)
}}}return a
},filterDuplicates:function(f,g){var i,j,d,h,a,e,b,c;
f.each(function(k){i=k.get("value");
j=g.findWhere({value:i});
if(j){d=j.get("priority");
a=j.get("isSubitem");
h=k.get("priority");
e=k.get("isSubitem");
if(a){j.set({priority:(h>d)?h:d})
}else{if(e){g.remove(j,{silent:true});
k.set({priority:(h>d)?h:d,isSubitem:true});
g.push(k)
}else{c=k.get("isParent");
b=j.get("isParent");
j.set({isParent:(c||b),isSelectable:!(c||b),priority:(h>d)?h:d,isSubitem:false})
}}}else{g.push(k)
}})
},highlightSearchTerms:function(b,a){b.each(function(c){if(c.get("isSelectable")){c.set("value",swa.highlightText(c.get("value"),a,(c.get("priority")===swa.MATCH_AIRPORT)))
}})
},initAirports:function(){this.reset(swa.airportStationList)
},initRouteData:function(a){var b=this;
_.each(a,function(c,d){b.findWhere({id:d}).set("routesServed",c.routesServed)
})
}});
swa.airportCollection=new swa.AirportCollection();
swa.airportCollection.initRouteData(routes);
/*! car-collection.js */
swa.CarCollection=swa.LocationCollection.extend({initialize:function(){this.initCars()
},searchAllForTerm:function(b){var e=this.models,c=new Backbone.Collection(),a,d;
b=b.toUpperCase();
_.each(e,function(f){a=f.get("displayName");
d=this.searchForTerm(a,b);
if(d){this.addResultsToList(a,c,d)
}},this);
this.rearrange(c);
this.highlightSearchTerms(c,b);
return c
},searchForTerm:function(b,d){var c=swa.MATCH_NONE,a;
b=b.toUpperCase();
a=b.lastIndexOf(d);
if(a===b.length-3){c=swa.MATCH_AIRPORT
}else{a=b.indexOf(d);
if(a===0){c=swa.MATCH_START_OF_NAME
}else{if(a>0){c=swa.MATCH_NAME
}}}return c
},rearrange:function(e){var g=e.models,f=g.length,d,b,a,c;
if(f>0){this.sortPriorities(e,0,f-1);
d=e.models[0].get("priority");
b=0;
for(c=d;
((c>=0)&&(b<f));
c-=1){a=this.findLastModelOfThisPriority(e,b,f-1);
this.sortAlphabetical(e,b,a);
b=a+1
}}},addResultsToList:function(b,a,c){a.add(new swa.MenuModel({priority:c,value:b}))
},initCars:function(){this.reset(swa.carList)
}});
swa.carCollection=new swa.CarCollection();
/*! hotel-collection.js */
swa.HotelCollection=swa.LocationCollection.extend({initialize:function(){this.initHotels()
},searchAllForTerm:function(a){var b=new Backbone.Collection(),c;
a=a.toUpperCase();
_.each(this.models,function(d){c=this.searchForTerm(d,a);
if(c!==swa.MATCH_NONE){this.addResultsToList(d,b,a,c)
}},this);
this.rearrange(b);
this.setEndSeparators(b);
this.filterRedundantSeparators(b);
this.highlightSearchTerms(b,a);
return b
},searchForTerm:function(e,c){var a=e.get("displayName"),f=swa.MATCH_NONE,i=false,b=e.get("children"),d=0,h=0,g;
if(b!==undefined){d=b.length
}g=a.toUpperCase().indexOf(c);
if(g!==-1){f=(g===0)?swa.MATCH_START_OF_NAME:swa.MATCH_NAME
}if(f===swa.MATCH_NONE){if(d){i=true
}while(i){g=b[h].displayName.toUpperCase().indexOf(c);
if(g!==-1){f=(g===0)?swa.MATCH_START_OF_NAME:swa.MATCH_NAME;
i=false
}if(h<d-1){h=h+1
}else{i=false
}}}return f
},addResultsToList:function(k,f,d,a){var c=k.get("children"),j=k.get("displayName"),b=j.toUpperCase(),g=k.get("id"),h=swa.MATCH_NONE,i,l;
i=b.indexOf(d);
if(i!==-1){h=(i===0)?swa.MATCH_START_OF_NAME:swa.MATCH_NAME
}if(c.length){h=(i===0)?swa.MATCH_PARENT_START_OF_NAME:swa.MATCH_PARENT_NAME;
e.call(this)
}else{f.add(new swa.MenuModel({priority:a,value:j,data:g}))
}function e(){var m=h,o,n;
f.add(new swa.MenuModel({isParent:true,priority:h,value:j,data:g}));
o=f.models[f.length-1];
_.each(c,function(p){l=p.id;
n=p.displayName.toUpperCase();
h=swa.MATCH_CHILD;
i=n.lastIndexOf(" - "+d);
if(i===Math.max(n.length-6,0)){h=swa.MATCH_PARENT_AIRPORT
}if(h===swa.MATCH_CHILD){i=n.indexOf(d);
if(i===0){h=swa.MATCH_START_OF_NAME
}else{if(i>0){h=swa.MATCH_NAME
}}}if(m<h){m=h
}f.add(new swa.MenuModel({priority:h,isSubitem:true,value:p.displayName,data:g+"|"+l}))
},this);
o.set("priority",m)
}},getCityName:function(c){var b=this.findWhere({id:c}),a="";
if(b){a=b.get("displayName")
}return a
},initHotels:function(){this.reset(swa.hotelList)
}});
swa.hotelCollection=new swa.HotelCollection();
/*! panel-menu-view.js */
swa.PanelMenu=swa.View.extend({options:null,events:{"click .js-panel-menu-option":"click","click .js-analytics":"sendAnalyticsData",keydown:"keydown"},initialize:function(a){var b={pointerXOffset:0,pointerYOffset:0,template:{},data:{}};
a=_.extend(b,a);
if(!a.el){swa.error("PanelView: el not passed")
}if(!a.panels){swa.error("PanelView: panel callback functions not passed")
}this.options=a;
if(a.$selectionPointer){this.positionPointer(this.$("a."+swa.SELECTED_CLASS),false)
}this.name="PanelMenu"
},render:function(){if(!this.options.template){swa.error("PanelView: template is not passed")
}this.$el.append(_.template(this.options.template,this.options.data))
},click:function(a){this.handlePanelClick(a)
},handlePanelClick:function(a){this.stopBubble(a);
this.swapPanels(a.target)
},keydown:function(g){var e=g.which,d=$(".js-panel-menu-option"),a=$(g.target),c=a.parent().index(),b="."+a.data("tab-focus"),h,f;
if(e===swa.KEY_TAB){h=swa.animationService.getRunningAnimations("position-pointer");
if(h){g.preventDefault();
f=h.vars.onComplete;
h.vars.onComplete=function(){f();
$(b).focus()
}
}}else{if((e===swa.KEY_LEFT)||(e===swa.KEY_UP)){this.stopBubble(g);
d.eq(c-1).click().focus()
}else{if((e===swa.KEY_RIGHT)||(e===swa.KEY_DOWN)){this.stopBubble(g);
if(c===d.length-1){c=-1
}d.eq(c+1).click().focus()
}}}},positionPointer:function(a,c){var b={},d;
if((this.options.$selectionPointer)&&(a.length)){b.$el=this.options.$selectionPointer;
b.relativeContainer=this.options.el;
b.$target=a;
b.side=this.options.pointerSide;
b.offsetX=this.options.pointerXOffset;
b.offsetY=this.options.pointerYOffset;
d=swa.position.positionAbsoluteToTargetWithinRelativeContainer(b);
if((c===undefined)||(c)){b.$el.css({position:"absolute"});
swa.animationService.to(b.$el,1,{css:{top:d.top,left:Math.floor(d.left)},ease:"Quint.easeOut",name:"position-pointer"})
}else{b.$el.css({position:"absolute",display:"block",top:d.top,left:d.left})
}}},sendAnalyticsData:function(a){swa.analytics.sendAnalyticsOnClick(a)
},selectTab:function(a,b){var c=this.$("a");
c.removeClass(swa.SELECTED_CLASS);
c.attr("aria-selected",false);
c.attr("tabindex",-1);
a.addClass(swa.SELECTED_CLASS);
a.attr("aria-selected",true);
a.attr("tabindex",0);
this.positionPointer(a,b)
},swapPanels:function(c){var a=$(c),b;
swa.page.closeOverlay();
if(a.prop("tagName")!=="A"){a=a.parent("A")
}if(a.prop("tagName")==="A"){this.selectTab(a,true)
}b=swa.getHashValue(a.attr("href"));
this.options.panels[b]()
}});
/*! menu-model.js */
swa.MenuModel=Backbone.Model.extend({defaults:{isParent:false,isSubitem:false,isLastSubitem:false,isSelectable:true,priority:0,value:"",data:""}});
/*! menu-view.js */
swa.Menu=swa.Overlay.extend({DATA:"data",MAX_HEIGHT_CONTAINER:325,MAX_NUMBER_ITEMS_VISIBLE:10,UNDEFINED:"undefined",items:null,highlightedItem:null,ndxTopVisibleItem:1,ndxHighlightedItem:1,originalTouchY:0,isMouseDown:false,mouseRepeater:null,mouseWheelDelta:0,templateContainer:null,templateScrollContainer:null,templateStart:null,templateValue:null,events:{click:"click","click .js-menu-scroll-up":"scrollUp","click .js-menu-scroll-down":"scrollDown",keydown:"keydown","mouseover .menu--item":"mouseOver","mousedown .js-menu-scroll-up":"mouseDownOnUpArrow","mouseup .js-menu-scroll-up":"mouseUpOnArrow","mouseout .js-menu-scroll-up":"mouseUpOnArrow","mousedown .js-menu-scroll-down":"mouseDownOnDownArrow","mouseup .js-menu-scroll-down":"mouseUpOnArrow","mouseout .js-menu-scroll-down":"mouseUpOnArrow",mousewheel:"mouseWheel",DOMMouseScroll:"mouseWheel",touchmove:"touchmove",touchstart:"touchstart"},initialize:function(a){var c=$("#js-menu-container"),b=$("#js-menu-scroll-container"),f=$("#js-menu-template-group-start"),d=$("#js-menu-template"),e={name:"Menu"};
if(typeof this.$el.attr("tabindex")===this.UNDEFINED){this.$el.attr("tabindex","99")
}if((!c.length)||(!b.length)||(!f.length)||(!d.length)){swa.error("MenuView: cannot locate menu templates")
}a=_.extend(e,a);
this.templateContainer=c.html();
this.templateScrollContainer=b.html();
this.templateStart=f.html();
this.templateValue=d.html();
swa.Menu.__super__.initialize.call(this,a);
this.name=a.name
},mouseDownOnUpArrow:function(a){if(a.which===1){this.isMouseDown=true;
this.mouseRepeater=setTimeout(_.bind(function(){this.mouseDownRepeatOnUp(a)
},this),250)
}},mouseDownRepeatOnUp:function(a){if(this.isMouseDown){this.scrollUp(a);
this.mouseRepeater=setTimeout(_.bind(function(){this.mouseDownRepeatOnUp(a)
},this),50)
}},mouseDownOnDownArrow:function(a){if(a.which===1){this.isMouseDown=true;
this.mouseRepeater=setTimeout(_.bind(function(){this.mouseDownRepeatOnDown(a)
},this),250)
}},mouseDownRepeatOnDown:function(a){if(this.isMouseDown){this.scrollDown(a);
this.mouseRepeater=setTimeout(_.bind(function(){this.mouseDownRepeatOnDown(a)
},this),50)
}},mouseUpOnArrow:function(){this.isMouseDown=false;
clearTimeout(this.mouseRepeater)
},update:function(h,c,a,e){var f,d;
this.options.content="";
h.each(this.addOne,this);
var b=a?a:"";
f=(h.length>this.MAX_NUMBER_ITEMS_VISIBLE)?this.templateScrollContainer:this.templateContainer;
d=_.template(f);
this.options.content=d({content:this.options.content,overrideClass:b});
this.$el.css("display","block");
swa.page.displayOverlay(this,e);
if(h.length>this.MAX_NUMBER_ITEMS_VISIBLE){this.ndxTopVisibleItem=1;
var g=this.$("#js-menu-wrapper");
g.height(this.MAX_HEIGHT_CONTAINER);
this.showVisibleItems();
this.$(".js-menu-scroll-up").addClass(swa.DISABLED_CLASS)
}this.items=this.$("LI");
this.highlightItem(c)
},addOne:function(b){var a;
if(b.get("isParent")){a=_.template(this.templateStart)
}else{a=_.template(this.templateValue)
}this.options.content+=a(b.toJSON())
},change:function(){var a;
if(this.highlightedItem!==null){a=this.highlightedItem.attr(this.DATA);
this.trigger("change",a)
}},click:function(b){var a=this;
this.stopBubble(b);
if(this.highlightFromMouseEvent(b)){setTimeout(function(){a.hide();
if(a.options.nextFocusCallback){a.options.nextFocusCallback()
}},100)
}},keydown:function(b){var k=this,h=b.which+"",f=parseInt(h),j={"9":c,"13":g,"27":i,"32":a,"38":e,"40":d};
if(j[h]){j[h]()
}else{if((f>=48&&f<=57)||(f>=65&&f<=90)){k.highlightItem(String.fromCharCode(f))
}else{if(f>=96&&f<=105){k.highlightItem(String.fromCharCode(f-48))
}}}this.change();
function d(){k.stopBubble(b);
k.highlightNextItem()
}function e(){k.stopBubble(b);
k.highlightPrevItem()
}function g(){k.stopBubble(b);
k.change();
k.hide();
if(k.options.nextFocusCallback){k.options.nextFocusCallback(b)
}}function i(){k.stopBubble(b);
k.change();
k.hide();
if(k.options.nextFocusCallback){k.options.nextFocusCallback(b)
}}function a(){k.stopBubble(b)
}function c(){k.stopBubble(b);
if(b.ctrlKey===false){k.change()
}k.hide();
if(b.shiftKey){if(k.options.prevFocusCallback){k.options.prevFocusCallback(b)
}}else{if(k.options.nextFocusCallback){k.options.nextFocusCallback(b)
}}}},mouseOver:function(a){this.highlightFromMouseEvent(a)
},mouseWheel:function(c){var a=c.originalEvent,b=a.detail;
if(this.items.length>this.MAX_NUMBER_ITEMS_VISIBLE){if(b){this.mouseWheelDelta=b*-1
}else{this.mouseWheelDelta=a.wheelDelta
}this.handleMouseWheelDelta(c)
}},handleMouseWheelDelta:function(a){if(this.mouseWheelDelta>0){this.scrollUp(a)
}else{this.scrollDown(a)
}},scrollUp:function(a){this.stopBubble(a);
if(this.ndxTopVisibleItem>1){this.ndxTopVisibleItem-=1;
this.showVisibleItems()
}},scrollDown:function(a){this.stopBubble(a);
if(this.ndxTopVisibleItem+this.MAX_NUMBER_ITEMS_VISIBLE<=this.items.length){this.ndxTopVisibleItem+=1;
this.showVisibleItems()
}},touchmove:function(a){a.preventDefault();
if(this.originalTouchY-a.originalEvent.touches[0].pageY>30){this.scrollDown(a);
this.originalTouchY=a.originalEvent.touches[0].pageY
}if(this.originalTouchY-a.originalEvent.touches[0].pageY<-30){this.scrollUp(a);
this.originalTouchY=a.originalEvent.touches[0].pageY
}},touchstart:function(a){this.originalTouchY=a.originalEvent.touches[0].pageY
},showVisibleItems:function(){var d=this.$(".menu"),a=this.$(".js-menu-scroll-up"),e=this.$(".js-menu-scroll-down"),c=this.ndxTopVisibleItem+this.MAX_NUMBER_ITEMS_VISIBLE,f,b;
d.find("li").css({display:"none"});
for(b=this.ndxTopVisibleItem;
b<c;
b+=1){d.find("li:nth-child("+b+")").css({display:"block"})
}if((this.items)&&(this.items.length)){if(this.ndxTopVisibleItem<=1){a.addClass(swa.DISABLED_CLASS)
}else{a.removeClass(swa.DISABLED_CLASS)
}if(this.ndxTopVisibleItem+this.MAX_NUMBER_ITEMS_VISIBLE>this.items.length){e.addClass(swa.DISABLED_CLASS)
}else{e.removeClass(swa.DISABLED_CLASS)
}}f=swa.position.getElementHeight(d);
d.css({marginTop:(this.MAX_HEIGHT_CONTAINER-f)/2})
},highlightItem:function(c){var d=false,b=0,e=c.length;
a.call(this);
if((!d)&&(e!==1)){this.highlightFirstItem()
}else{a.call(this)
}function a(){var h=this.highlightedItem,i=-1,j,l=this.options.target.attr(this.DATA),g,f,k;
if(h){i=h.index()
}_.each(this.items,function(n){if(!d){j=$(n);
f=j.attr(this.DATA);
if(j.hasClass("selectable")){g=j.text().toUpperCase();
k=g.substr(0,1);
if(e>1){c=c.toUpperCase();
if(g.indexOf(c)!==-1){m.call(this)
}else{if((typeof l!==this.UNDEFINED)&&(l===f)){m.call(this)
}}}else{if((e===1)&&(k===c)&&(b>i)){m.call(this)
}}}b+=1
}},this);
function m(){this.removeHighlight();
this.highlightedItem=j;
this.setHighlight();
d=true
}}},highlightFirstItem:function(){var a=false;
if(this.$(".selectable").length){a=true
}if(a){this.removeHighlight();
this.highlightedItem=this.items.first();
if(this.highlightedItem.hasClass("not-selectable")){this.highlightNextItem()
}this.setHighlight()
}},highlightNextItem:function(){if(this.highlightedItem.nextAll(".selectable").length){this.removeHighlight();
this.getNextItem();
while(this.highlightedItem.hasClass("not-selectable")){this.getNextItem()
}this.setHighlight()
}},highlightPrevItem:function(){if(this.highlightedItem.prevAll(".selectable").length){this.removeHighlight();
this.getPrevItem();
while(this.highlightedItem.hasClass("not-selectable")){this.getPrevItem()
}this.setHighlight()
}},getNextItem:function(){var a=this.highlightedItem.next();
if(a.length){this.highlightedItem=a
}},getPrevItem:function(){var a=this.highlightedItem.prev();
if(a.length){this.highlightedItem=a
}},setHighlight:function(){this.highlightedItem.addClass("highlight");
this.ndxHighlightedItem=this.highlightedItem.prevAll().length+1;
if(this.ndxHighlightedItem<this.ndxTopVisibleItem){this.ndxTopVisibleItem=this.ndxHighlightedItem;
this.showVisibleItems()
}if(this.ndxHighlightedItem>=this.ndxTopVisibleItem+this.MAX_NUMBER_ITEMS_VISIBLE){this.ndxTopVisibleItem=this.ndxHighlightedItem-this.MAX_NUMBER_ITEMS_VISIBLE+1;
this.showVisibleItems()
}},getHighlightedItem:function(){return this.highlightedItem
},removeHighlight:function(){if(this.highlightedItem){this.highlightedItem.removeClass("highlight")
}},highlightFromMouseEvent:function(c){var a=$(c.target),b=false;
if(a.prop("tagName")!=="LI"){a=a.parents("LI")
}if((a.length)&&(a.hasClass("not-selectable")===false)){this.removeHighlight();
this.highlightedItem=a;
this.setHighlight();
b=true
}return b
},getNumberOfItems:function(){var a=0;
if(this.items){a=this.items.length
}return a
},hide:function(){swa.page.closeOverlay()
},close:function(){this.change();
swa.Menu.__super__.close.call(this)
},open:function(){if(this.getNumberOfItems()){swa.page.displayOverlay(this)
}},val:function(){var a="";
if(this.highlightedItem){if(this.highlightedItem.hasClass("menu--top-separator")){a=this.highlightedItem.find(".menu--top-separator-text").text()
}else{a=this.highlightedItem.text()
}}return a
}});
/*! autocomplete-view.js */
swa.Autocomplete=swa.View.extend({NO_MATCH_FOUND_MESSAGE:"No Match Found",NOT_VALID_ROUTE_MESSAGE:"Invalid route with departure airport",keys:"",options:null,collection:null,menu:null,initialize:function(a){var b={isAirportCodeOnly:false,name:"Autocomplete"};
if(!a.el){swa.error("Autocomplete: el not passed")
}if(!a.name){swa.error("Autocomplete: name not passed")
}if(!a.collection){swa.error("Autocomplete: collection not passed")
}a=_.extend(b,a);
this.options=a;
this.collection=a.collection;
this.name=a.name
},events:{click:"click",focus:"focus",blur:"blur",keydown:"keydown",keyup:"keyup"},click:function(a){this.stopBubble(a);
this.open();
this.$el.select()
},focus:function(){swa.page.closeOverlay();
this.open()
},blur:function(){var a=this.val();
if(a.length<3){this.$el.val("");
this.change("")
}},keydown:function(a){var j=this,g=a.which,f=g+"",i={"9":c,"13":e,"27":h,"38":d,"40":b};
if(i[f]){i[f]()
}function c(){var k=j.getItemData.call(j);
if(a.ctrlKey===false){j.change(k);
j.hide()
}}function e(){var k=j.getItemData.call(j);
j.change(k);
j.hide();
if(j.options.nextFocusCallback){j.options.nextFocusCallback()
}a.preventDefault()
}function h(){var k=j.getItemData.call(j);
j.change(k);
j.hide();
if(j.options.nextFocusCallback){j.options.nextFocusCallback()
}}function b(){j.stopBubble(a);
if(j.menu){j.menu.highlightNextItem()
}}function d(){j.stopBubble(a);
if(j.menu){j.menu.highlightPrevItem()
}}},getItemData:function(){var b="",a;
if(this.menu){a=this.menu.getHighlightedItem();
if(a){b=a.attr("data")
}}return b
},keyup:function(c){var a=c.which,b;
b=this.$el.val();
this.keys=b;
if(!this.isKeyAlreadyHandled(a)){this.showResults()
}},showResults:function(){var a=false,c,b;
if(this.keys.length>=3){this.hide();
this.menu=new swa.Menu({target:this.$el,nextFocusCallback:this.options.nextFocusCallback,offsetY:swa.OVERLAY_OFFSET_Y});
this.listenTo(this.menu,"change",this.change);
c=this.collection.getSearchResult(this.keys);
if((c)&&(c.length)){this.updateList(c,this.keys)
}else{if((this.collection.isAnyMatch)&&(this.collection.isAnyMatch(this.keys.toUpperCase()))){a=true
}b=new Backbone.Collection([{value:(a)?this.NOT_VALID_ROUTE_MESSAGE:this.NO_MATCH_FOUND_MESSAGE,isLastSubitem:false,isSelectable:false,data:""}]);
this.updateList(b,"")
}}else{this.hide()
}},isKeyAlreadyHandled:function(a){return((a===swa.KEY_ENTER)||(a===swa.KEY_ESCAPE)||(a===swa.KEY_LEFT)||(a===swa.KEY_UP)||(a===swa.KEY_RIGHT)||(a===swa.KEY_DOWN))
},change:function(a){var b=this.val();
if((this.collection.isAnyMatch)&&(!this.collection.isAnyMatch(b.toUpperCase()))){b=this.NO_MATCH_FOUND_MESSAGE
}if(b===this.NO_MATCH_FOUND_MESSAGE){b=""
}else{if(this.options.isAirportCodeOnly){if((b)&&(b.length>2)){b=b.substring(b.length-3)
}b=b.toUpperCase()
}}this.val(b);
this.trigger("change",a)
},hide:function(){if(this.menu){this.stopListening(this.menu);
this.menu.hide();
this.menu=null
}},open:function(){var a=swa.page.getCurrentOverlay();
if(!a){this.keys=this.$el.val();
this.showResults()
}},val:function(b){var a="";
if((b)||(b==="")){this.$el.val(b)
}else{if(this.menu){a=this.menu.val()
}else{a=this.$el.val()
}}return a
},updateList:function(b,a){this.menu.update(b,a)
},getAirportCode:function(){var b=this.val(),a="";
if((b!==this.NO_MATCH_FOUND_MESSAGE)&&(b!==this.NOT_VALID_ROUTE_MESSAGE)){a=b.substring(b.length-3)
}return a
},setAirportFilter:function(a){this.collection.setAirportFilter(a)
}});
/*! trip-type-view.js */
swa.TripType=swa.View.extend({events:{change:"change"},initialize:function(){this.name="TripType"
},change:function(){this.trigger("change")
},val:function(a){if(a){if(a==="true"){this.$('[name="twoWayTrip"]')[0].checked=true
}else{if(a==="false"){this.$('[name="twoWayTrip"]')[1].checked=true
}}}else{return this.$('[name="twoWayTrip"]:checked').val()
}}});
/*! price-type-view.js */
swa.PriceType=swa.View.extend({events:{change:"change"},initialize:function(){this.name="PriceType"
},change:function(){this.trigger("change")
},val:function(a){if(a){if(a==="DOLLARS"){this.$('[name="fareType"]')[0].checked=true
}else{if(a==="POINTS"){this.$('[name="fareType"]')[1].checked=true
}}}else{return this.$('[name="fareType"]:checked').val()
}}});
/*! vacations-type-view.js */
swa.VacationsType=swa.View.extend({events:{change:"change"},initialize:function(){this.name="VacationsType"
},change:function(){this.trigger("change")
},val:function(b){var a=this.$('[name="vacationType"]');
switch(b){case"AH01":a[0].checked=true;
break;
case"AH03":a[1].checked=true;
break;
case"AC01":a[2].checked=true;
break;
default:return this.$('[name="vacationType"]:checked').val();
break
}}});
/*! checkbox-view.js */
swa.Checkbox=swa.View.extend({options:null,initialize:function(a){if(!a.el){swa.error("Checkbox: el not passed")
}this.options=a;
this.name=a.name
},events:{change:"change"},change:function(){this.trigger("change")
},val:function(a){if(typeof a!=="undefined"){if(typeof a==="string"){a=(a==="true")?true:false
}this.$el.prop("checked",a)
}else{return this.$el.prop("checked")
}}});
/*! number-selector-view.js */
swa.NumberSelector=swa.Overlay.extend({options:null,initialize:function(a){var b=$("#js-number-selector-template"),c={initialValue:0,maxValue:9,minValue:0,name:"NumberSelector",title:""};
if(!b.length){swa.error("NumberSelector: cannot locate template #js-number-selector")
}if(!a.target){swa.error("NumberSelector: target was not passed")
}this.options=a=_.extend(c,a);
a.content=_.template(b.html(),{title:a.title});
this.el=$(this.el);
if(this.el.attr("tabindex")===undefined){this.el.attr("tabindex","99")
}this.$el.attr("autocomplete","off");
swa.NumberSelector.__super__.initialize.call(this,a);
this.name=a.name
},render:function(){swa.NumberSelector.__super__.render.call(this);
this.el.focus();
this.val(this.options.initialValue);
this.enableDisableButtons(this.options.initialValue)
},events:{click:"stopBubble","mousedown .js-number-selector-less":"less","mousedown .js-number-selector-more":"more",keydown:"keyDown"},less:function(a){this.stopBubble(a);
this.decrease()
},more:function(a){this.stopBubble(a);
this.increase()
},keyDown:function(b){var a=b.which,c;
if((a===swa.KEY_UP)||(a===swa.KEY_RIGHT)){this.stopBubble(b);
this.increase()
}if((a===swa.KEY_DOWN)||(a===swa.KEY_LEFT)){this.stopBubble(b);
this.decrease()
}if(a===swa.KEY_ENTER){this.stopBubble(b);
swa.page.closeOverlay();
if(this.options.nextFocusCallback){this.options.nextFocusCallback(b)
}}if(a===swa.KEY_ESCAPE){this.stopBubble(b);
swa.page.closeOverlay();
if(this.options.nextFocusCallback){this.options.nextFocusCallback(b)
}}if(a===swa.KEY_SPACE){this.stopBubble(b)
}if(a===swa.KEY_TAB){this.stopBubble(b);
swa.page.closeOverlay();
if(b.shiftKey){if(this.options.prevFocusCallback){this.options.prevFocusCallback(b)
}}else{if(this.options.nextFocusCallback){this.options.nextFocusCallback(b)
}}}c=this.getNumericEntry(a);
if(c!==-1){this.val(c);
this.enableDisableButtons(c)
}},getNumericEntry:function(a){var b=-1;
if((a>=48+this.options.minValue)&&(a<=48+this.options.maxValue)){b=a-48
}if((a>=96+this.options.minValue)&&(a<=96+this.options.maxValue)){b=a-96
}return b
},decrease:function(){var a=this.val();
if(a>this.options.minValue){a=a-1;
this.val(a)
}this.enableDisableButtons(a)
},increase:function(){var a=this.val();
if(a<this.options.maxValue){a=a+1;
this.val(a)
}this.enableDisableButtons(a)
},enableDisableButtons:function(a){var c=this.$(".js-number-selector-less"),b=this.$(".js-number-selector-more");
if(a===this.options.minValue){c.addClass(swa.DISABLED_CLASS)
}if(a!==this.options.minValue){c.removeClass(swa.DISABLED_CLASS)
}if(a===this.options.maxValue){b.addClass(swa.DISABLED_CLASS)
}if(a!==this.options.maxValue){b.removeClass(swa.DISABLED_CLASS)
}},val:function(b){var a=this.$(".js-number-selector-value");
if(b!==undefined){a.html(b);
this.trigger("change",b)
}else{return parseInt(a.html(),10)
}}});
/*! traveler-selector-view.js */
swa.TravelerSelector=swa.NumberSelector.extend({initialize:function(a){var b=$("#js-traveler-selector-template"),d,c={initialValue:0,maxValue:8,minValue:0,name:"TravelerSelector",title:"",titleDetails:""};
if(!b.length){swa.error("TravelerSelector: cannot locate template #js-traveler-selector")
}if(!a.target){swa.error("TravelerSelector: cannot locate target which indicates where to position the number selector overlay.")
}a=_.extend(c,a);
b=_.template(b.html(),{travelerSelectorTitle:a.title,travelerSelectorTitleDetail:a.titleDetails});
d={title:b,target:a.target,offsetX:a.offsetX,offsetY:a.offsetY,minValue:a.minValue,maxValue:a.maxValue,initialValue:a.initialValue,prevFocusCallback:a.prevFocusCallback,nextFocusCallback:a.nextFocusCallback,closeCallback:a.closeCallback};
swa.TravelerSelector.__super__.initialize.call(this,d);
this.name=a.name
}});
/*! time-selector-collection.js */
swa.TimeSelectorCollection=Backbone.Collection.extend({model:swa.MenuModel});
swa.timeSelectorCollection=new swa.TimeSelectorCollection([{value:"05:00 AM"},{value:"05:30 AM"},{value:"06:00 AM"},{value:"06:30 AM"},{value:"07:00 AM"},{value:"07:30 AM"},{value:"08:00 AM"},{value:"08:30 AM"},{value:"09:00 AM"},{value:"09:30 AM"},{value:"10:00 AM"},{value:"10:30 AM"},{value:"11:00 AM"},{value:"11:30 AM"},{value:"12:00 PM"},{value:"12:30 PM"},{value:"01:00 PM"},{value:"01:30 PM"},{value:"02:00 PM"},{value:"02:30 PM"},{value:"03:00 PM"},{value:"03:30 PM"},{value:"04:00 PM"},{value:"04:30 PM"},{value:"05:00 PM"},{value:"05:30 PM"},{value:"06:00 PM"},{value:"06:30 PM"},{value:"07:00 PM"},{value:"07:30 PM"},{value:"08:00 PM"},{value:"08:30 PM"},{value:"09:00 PM"},{value:"09:30 PM"},{value:"10:00 PM"},{value:"10:30 PM"},{value:"11:00 PM"},{value:"11:30 PM"},{value:"11:59 PM"}]);
/*! time-selector-view.js */
swa.TimeSelector=swa.Menu.extend({options:null,initialize:function(a){var c,b={prevFocusCallback:null,nextFocusCallback:null,overrideClass:null};
this.options=_.extend(b,a);
swa.TimeSelector.__super__.initialize.call(this,this.options);
c=swa.timeSelectorCollection;
this.update(c,a.target.val()+" "+a.target.siblings(".js-meridian-indicator").text(),a.overrideClass)
}});
/*! car-vendor-collection.js */
swa.CarVendorCollection=Backbone.Collection.extend({model:swa.MenuModel});
swa.carVendorCollection=new swa.CarVendorCollection([{value:"All Car Companies",data:"SHOP_ALL"},{value:"All Rapid Rewards Partners",data:"SHOP_PARTNERS"},{value:"Alamo",data:"ALAMO"},{value:"Avis",data:"AVIS"},{value:"Budget",data:"BUDGET"},{value:"Dollar",data:"DOLLAR"},{value:"Hertz",data:"HERTZ"},{value:"National",data:"ZL"},{value:"Payless",data:"ZA"},{value:"Thrifty",data:"THRIFTY"}]);
/*! car-size-collection.js */
swa.CarSizeCollection=Backbone.Collection.extend({model:swa.MenuModel});
swa.carSizeCollection=new swa.CarSizeCollection([{value:"Economy",data:"ECONOMY"},{value:"Compact",data:"COMPACT"},{value:"Mid-size",data:"MIDSIZE"},{value:"Full-size",data:"FULLSIZE"},{value:"Premium",data:"PREMIUM"},{value:"Sport Utility",data:"SUV"},{value:"Luxury",data:"LUXURY"},{value:"MiniVan",data:"MINIVAN"},{value:"Convertible",data:"CONVERTIBLE"},{value:"Full Size Van",data:"VAN"}]);
/*! children-age-collection.js */
swa.ChildrenAgeCollection=Backbone.Collection.extend({model:swa.MenuModel});
swa.childrenAgeCollection=new swa.ChildrenAgeCollection([{value:"1",data:1},{value:"2",data:2},{value:"3",data:3},{value:"4",data:4},{value:"5",data:5},{value:"6",data:6},{value:"7",data:7},{value:"8",data:8},{value:"9",data:9},{value:"10",data:10},{value:"11",data:11},{value:"12",data:12},{value:"13",data:13},{value:"14",data:14},{value:"15",data:15},{value:"16",data:16},{value:"17",data:17}]);
/*! where-we-fly-view.js */
swa.WhereWeFly=swa.View.extend({$listView:null,$mapView:null,className:"where-we-fly",name:"",options:null,events:{"click .js-overlay-close-link":"close"},initialize:function(b){var e=$("#js-where-we-fly-modal-background"),d=$("#js-where-we-fly-buttons-template"),a=$("#js-where-we-fly-template"),c={name:"WhereWeFly"};
this.childViews=[];
if((!a.length)||(!d.length)||(!e.length)){swa.error("WhereWeFly: cannot locate templates")
}this.options=_.extend(c,b);
this.$el=$(this.el);
this.templatePageBackground=e.html();
this.templateButtons=d.html();
this.template=a.html();
this.name=this.options.name
},render:function(){$("body").append(this.$el.html(this.template));
this.renderButtonsView();
this.renderMapView();
this.renderListView();
this.renderOverlay();
this.$listView=$(".js-where-we-fly-list");
this.$mapView=$(".js-where-we-fly-map")
},changeViews:function(b){var a=b.$previousSelectedButton,c=b.$newSelectedButton;
if(a){if(a.hasClass("js-show-list")){this.$listView.addClass(swa.HIDDEN_CLASS)
}if(a.hasClass("js-show-map")){this.$mapView.addClass(swa.HIDDEN_CLASS)
}}else{this.$listView.addClass(swa.HIDDEN_CLASS);
this.$mapView.addClass(swa.HIDDEN_CLASS)
}if(c.hasClass("js-show-list")){this.$listView.removeClass(swa.HIDDEN_CLASS)
}if(c.hasClass("js-show-map")){this.$mapView.removeClass(swa.HIDDEN_CLASS)
}},renderButtonsView:function(){this.buttonMenu=new swa.ButtonMenu({el:(".js-button-wrapper"),html:this.templateButtons});
this.registerChildView(this.buttonMenu);
this.buttonMenu.render();
this.listenTo(this.buttonMenu,"change",this.changeViews)
},renderListView:function(){this.listView=new swa.WhereWeFlyList({el:".js-where-we-fly-list"});
this.registerChildView(this.listView);
this.listView.render()
},renderMapView:function(){this.mapView=new swa.WhereWeFlyMap({el:".js-where-we-fly-map"});
this.registerChildView(this.mapView);
this.mapView.render()
},renderOverlay:function(){var d=$(window).scrollTop(),b=$("body"),a=this,e,c;
b.append(this.templatePageBackground);
$(".js-overlay-modal-background").click(function(){if(a.options.closeCallback){a.options.closeCallback()
}});
e=swa.position.centerElementInWindow(this.$el);
c=e.top();
c=(c<0)?d:c+d;
this.$el.css({top:c,left:e.left()})
},close:function(){$(".js-overlay-modal-background").remove();
swa.WhereWeFly.__super__.close.call(this)
}});
/*! where-we-fly-list-view.js */
swa.WhereWeFlyList=swa.View.extend({column1data:[],column2data:[],column3data:[],name:"",options:null,initialize:function(b){var a=$("#js-where-we-fly-list-template"),c={name:"WhereWeFlyList"};
if(!a.length){swa.error("WhereWeFlyList: cannot locate templates")
}if(!b.el){swa.error("WhereWeFlyList: el not passed")
}this.$el=$(b.el);
this.options=_.extend(c,b);
this.template=a.html();
this.name=this.options.name
},render:function(){var a=new Backbone.Collection(),b=_.template(this.template);
this.createExpandedCollection(a);
this.formatColumnLists(a);
this.$el.html(b({column1:this.column1data,column2:this.column2data,column3:this.column3data}))
},convertCollectionToListItems:function(a,c){var b;
_.each(a.models,function(d){b="<li>";
if(d.get("isParent")){b='<li class="panel--list-column-parent-item">'
}if(d.get("isChild")){b='<li class="panel--list-column-child-item">'
}c.push(b+d.get("displayName")+"</li>")
})
},createExpandedCollection:function(c){var b,d,a,e;
_.each(swa.airportStationList,function(f){d=(f.children.length>0);
b=f.displayName;
c.add({displayName:b,isChild:false,childCode:"",isParent:d,parentCode:""});
if(d){_.each(f.children,function(g){e=swa.airportCollection.findWhere({id:g});
b=e.get("displayName");
a=e.get("parents")[0];
c.add({displayName:b,isChild:true,childCode:g,isParent:d,parentCode:a})
})
}})
},formatColumnLists:function(e){var l=e.models.length,f=[],p,a,k,g,c,n,j,d,i,b,o,m,h;
if(l){p=parseInt(l/3,10);
a=l%3;
k=g=p;
if(a===1){k+=1
}else{if(a===2){k+=1;
g+=1
}}c=l-k-g;
n=e.models[k];
d=n.get("isChild");
if(d){i=swa.airportCollection.findWhere({id:n.get("parentCode")});
b=i.get("children");
o=_.indexOf(b,n.get("childCode"));
if(o<b.length/2){k-=o+1;
g+=o+1
}else{k+=b.length-o;
g-=b.length-o
}}n=e.models[k+g];
d=n.get("isChild");
if(d){i=swa.airportCollection.findWhere({id:n.get("parentCode")});
b=i.get("children");
o=_.indexOf(b,n.get("childCode"));
if((k<g)||(o<b.length/2)){g-=o+1
}else{g+=b.length-o
}}else{m=g-c;
h=(m>1);
while(h){j=e.models[k+g-1];
if(j.get("isChild")===false){g-=1;
c+=1;
m=g-c;
h=(m>1)
}else{h=false
}}}}this.convertCollectionToListItems(e,f);
this.column1data=f.slice(0,k).join("");
this.column2data=f.slice(k,k+g).join("");
this.column3data=f.slice(k+g).join("")
}});
/*! where-we-fly-map-view.js */
swa.WhereWeFlyMap=swa.View.extend({$magnifyGlass:null,$mapImage:null,$mapView:null,initialMapOffset:null,initialPoint:null,isFirstZoom:true,isDrag:false,isSvgSupported:undefined,isZoomedIn:false,mapOffset:{},maxImageWidth:0,maxImageHeight:0,name:"",options:null,events:{"click .js-map-zoom":"zoomInOut","mousedown .js-map-container":"mouseDown","mousemove .js-map-container":"mouseMove","mouseout .js-map-container":"stopPan","mouseup .js-map-container":"mouseUp",touchmove:"touchMove",touchstart:"touchStart"},initialize:function(b){var a,c={name:"WhereWeFlyMap"};
a=(this.getIsSvgSupported())?$("#js-where-we-fly-svg-map-template"):$("#js-where-we-fly-png-map-template");
if(!a.length){swa.error("WhereWeFlyMap: cannot locate templates")
}if(!b.el){swa.error("WhereWeFlyMap: el not passed")
}this.options=_.extend(c,b);
this.template=a.html();
this.name=this.options.name
},render:function(){this.$el.html(this.template);
this.$mapView=this.$(".js-map-container");
this.$mapImage=this.$(".js-map-image");
this.$magnifyGlass=this.$(".js-map-zoom");
this.maxImageWidth=this.$mapView.width();
this.maxImageHeight=this.$mapView.height()
},addZoom:function(){var a;
if(this.getIsSvgSupported()){this.$mapView.css("transform-origin","0 0");
a=this.$mapView.attr("class");
this.$mapView.attr("class",a+" panel--map-container_magnify2x")
}else{this.$mapImage.addClass("panel--map-image_magnify2x")
}},getIsSvgSupported:function(){if(this.isSvgSupported===undefined){this.isSvgSupported=document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure","1.1")
}return this.isSvgSupported
},mouseDown:function(a){if(a.which===1){this.stopBubble(a);
if(this.isZoomedIn){this.initialMapOffset=this.$mapView.position();
this.initialPoint=this.normalizeMousePoint(a)
}}},mouseMove:function(b){var a;
this.stopBubble(b);
if(this.initialPoint!==null){a=this.normalizeMousePoint(b);
this.updateMap(a)
}},mouseUp:function(c){var b={},a;
if(c.which===1){this.stopBubble(c);
if(this.isDrag){this.mapOffset.x=parseInt(this.$mapView.css("left"),10);
this.mapOffset.y=parseInt(this.$mapView.css("top"),10)
}else{a=this.normalizeMousePoint(c);
b.x=675/2;
b.y=454/2;
this.isFirstZoom=false;
if(this.isZoomedIn){this.mapOffset.x=-(a.x-this.mapOffset.x-b.x);
this.mapOffset.y=-(a.y-this.mapOffset.y-b.y);
this.moveMap(this.mapOffset)
}else{this.mapOffset.x=-(a.x*2-b.x);
this.mapOffset.y=-(a.y*2-b.y);
this.zoomInOut()
}}this.isDrag=false
}this.initialPoint=null
},moveMap:function(a){a.x=Math.min(0,a.x);
a.y=Math.min(0,a.y);
a.x=Math.max(-this.maxImageWidth,a.x);
a.y=Math.max(-this.maxImageHeight,a.y);
this.$mapView.css({left:a.x,top:a.y})
},normalizeMousePoint:function(b){var a=this.$mapView.parent().offset();
return{x:b.pageX-a.left,y:b.pageY-a.top}
},normalizeTouchPoint:function(b){var a=this.$mapView.parent().offset();
return{x:b.originalEvent.touches[0].pageX-a.left,y:b.originalEvent.touches[0].pageY-a.top}
},removeZoom:function(){var a;
this.stopPan();
if(this.getIsSvgSupported()){a=this.$mapView.attr("class");
this.$mapView.attr("class",a.replace(" panel--map-container_magnify2x",""))
}else{this.$mapImage.removeClass("panel--map-image_magnify2x")
}},stopPan:function(){this.initialPoint=null
},touchMove:function(b){var a;
this.stopBubble(b);
if(this.initialPoint!==null){a=this.normalizeTouchPoint(b);
this.updateMap(a)
}},touchStart:function(c){var a=$(c.target);
var b={};
if(a.hasClass("js-map-zoom")===false){if(this.isZoomedIn&&c.originalEvent.touches.length===1){this.initialMapOffset=this.$mapView.position();
this.initialPoint=this.normalizeTouchPoint(c)
}if(!this.isZoomedIn&&c.originalEvent.touches.length===2){point=this.normalizeTouchPoint(c);
b.x=675/2;
b.y=454/2;
this.mapOffset.x=-(point.x*2-b.x);
this.mapOffset.y=-(point.y*2-b.y);
this.zoomInOut()
}}},updateMap:function(a){a.x-=this.initialPoint.x;
a.y-=this.initialPoint.y;
if((Math.abs(a.x)>2)||(Math.abs(a.y)>2)){this.isDrag=true
}a.x+=this.initialMapOffset.left;
a.y+=this.initialMapOffset.top;
this.moveMap(a)
},zoomInOut:function(a){if(this.isFirstZoom){this.isFirstZoom=false;
this.mapOffset.x=-this.maxImageWidth/2;
this.mapOffset.y=-this.maxImageHeight/2
}if(this.isZoomedIn){this.removeZoom();
this.$mapView.css({left:0,top:0});
this.isZoomedIn=false
}else{this.addZoom();
this.moveMap(this.mapOffset);
this.isZoomedIn=true
}if(this.isZoomedIn){this.$magnifyGlass.removeClass("swa-icon_zoom-in").addClass("swa-icon_zoom-out")
}else{this.$magnifyGlass.removeClass("swa-icon_zoom-out").addClass("swa-icon_zoom-in")
}}});
/*! button-menu-view.js */
swa.ButtonMenu=swa.View.extend({$selectedMenuButton:null,name:"",options:null,template:"",events:{click:"change"},initialize:function(a){var b={name:"ButtonMenu"};
this.childViews=[];
if(!a.el){swa.error("ButtonMenu: el not passed")
}if(!a.html){swa.error("ButtonMenu: html not passed")
}this.options=_.extend(b,a);
this.el=a.el;
this.template=a.html;
this.name=this.options.name
},render:function(){this.$el.append(this.template)
},change:function(b){var a=$(b.target);
if(a.hasClass("swa-button")===false){a=a.parent(".swa-button")
}if(a.length){this.$(".swa-button").removeAttr("aria-selected").removeClass(swa.SELECTED_CLASS);
a.attr("aria-selected","true").addClass(swa.SELECTED_CLASS);
this.trigger("change",{$previousSelectedButton:this.$selectedMenuButton,$newSelectedButton:a});
this.$selectedMenuButton=a
}}});
/*! flight-status-collection.js */
swa.FlightStatusCollection=Backbone.Collection.extend({model:swa.MenuModel});
swa.flightStatusCollection=new swa.FlightStatusCollection([{value:swa.getDisplayFormattedWeekdayMonthDayForYesterday(),data:swa.getSystemFormattedDateForYesterday()},{value:swa.getDisplayFormattedWeekdayMonthDayForToday(),data:swa.getSystemFormattedDateForToday()},{value:swa.getDisplayFormattedWeekdayMonthDayForTomorrow(),data:swa.getSystemFormattedDateForTomorrow()}]);
/*! booking-form-model.js */
swa.FormModel=Backbone.Model.extend({toString:function(){return"FormModel("+JSON.stringify(this.attributes)+")"
},isEmpty:function(a){return(!a)||(!a.length)
},isValid:function(a){return((typeof a!=="undefined")&&(typeof a.isValid==="function")&&(a.isValid()))
},addError:function(b,a){b.push({field:a.field,errorMessageElement:a.errorMessageElement,errorMessageAccessible:a.errorMessageAccessible,errorMessage:a.errorMessage})
},validate:function(){var b=[],a;
_.each(this.validateConfiguration,function(d,c){a=this.get(c);
if(d.validate){d.validate(this,a,d,b)
}else{if(this.isEmpty(a)){this.addError(b,d)
}}},this);
return b
}});
/*! booking-form-view.js */
swa.BookingForm=swa.View.extend({initialize:function(a){this.name=a.name
},submit:function(b){var c=this.model.validate(),a=true;
$(".js-label-accessible-error").html("").siblings("input").removeAttr("aria-invalid");
if(c.length){this.stopBubble(b);
_.map(c,function(d){swa.warn("Validation warning: "+d.errorMessage);
d.field.addClass("error-field").attr("aria-invalid",true);
d.field.siblings(".js-field-icon").addClass("swa-icon").addClass("swa-icon_error swa-g-error").removeClass("swa-g-hidden");
d.field.siblings(".booking-form--field-icon-container").children(".js-field-icon").addClass("swa-icon_error swa-g-error");
d.errorMessageElement.html(d.errorMessage).addClass("error-label");
d.errorMessageAccessible.html(d.errorMessage)
});
a=false
}else{swa.page.savePageData()
}return a
},removeErrorCondition:function(c,b,a,e){var d;
if(!e){d=c.siblings(".booking-form--field-icon-container").children(".js-field-icon")
}else{d=c.siblings(".js-field-icon")
}c.removeClass("error-field");
if(b.hasClass("error-label")){b.html(a).removeClass("error-label")
}d.removeClass("swa-icon_error swa-g-error");
if(e){d.removeClass("swa-icon").addClass("swa-g-hidden")
}},addSimulatedFocus:function(a){a.addClass(swa.SIMULATED_FOCUS_CLASS)
},removeSimulatedFocus:function(a){a.removeClass(swa.SIMULATED_FOCUS_CLASS)
},formatDateString:function(a){var b=a.split(" ");
return"<i>"+b[0]+",</i> "+b[1]+" "+b[2]+", "+b[3]
}});
/*! booking-form-air-model.js */
swa.AirFormModel=swa.FormModel.extend({defaults:{name:"AirFormModel",origin:"",destination:"",departDate:"",returnDate:"",tripType:"true",priceType:"DOLLARS",adult:1,senior:0,promoCode:""},validateConfiguration:{origin:{field:$(".js-origin-airport"),errorMessageElement:$(".js-origin-airport-label"),errorMessageAccessible:$(".js-origin-airport-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.error.originLabel")},destination:{field:$(".js-destination-airport"),errorMessageElement:$(".js-destination-airport-label"),errorMessageAccessible:$(".js-destination-airport-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.error.destinationLabel")},departDate:{field:$(".js-depart-date"),errorMessageElement:$(".js-depart-date-label"),errorMessageAccessible:$(".js-depart-date-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.error.departDateLabel"),validate:function(a,b,d,c){if(!a.isValid(b)){a.addError(c,d)
}}},returnDate:{field:$(".js-return-date"),errorMessageElement:$(".js-return-date-label"),errorMessageAccessible:$(".js-return-date-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.error.returnDateLabel"),validate:function(a,b,d,c){if((a.get("tripType")==="true")&&(!a.isValid(b))){a.addError(c,d)
}}}}});
/*! booking-form-air-horizontal-view.js */
swa.AirBookingForm=swa.BookingForm.extend({model:null,options:null,tripType:null,priceType:null,origin:null,destination:null,departDate:null,returnDate:null,adult:1,$airAdultTarget:null,senior:0,$airSeniorTarget:null,seniorHiddenValueWhenDisabled:0,whereWeFly:null,initialize:function(d){var g,b,h,f,c,e,a;
this.$airAdultTarget=this.$(".js-air-adult-value");
this.$airSeniorTarget=this.$(".js-air-senior-value");
this.childViews=[];
if(!d.name){d.name=d.el.attr("name");
if(!d.name){swa.error("AirBookingForm: name not passed")
}}if(!d.el){swa.error("AirBookingForm: el not passed")
}this.options=d;
this.name=d.name;
g=swa.page.getPageModelData("AirFormModel");
this.model=new swa.AirFormModel(g);
swa.page.registerPageData(this.model);
this.tripType=new swa.TripType({el:".js-trip-type"});
f=this.model.get("tripType");
if(f){this.tripType.val(f)
}this.registerChildView(this.tripType);
this.updateTripType();
this.priceType=new swa.PriceType({el:".js-price-type"});
c=this.model.get("priceType");
if(c){this.priceType.val(c)
}this.registerChildView(this.priceType);
this.updatePriceType();
this.origin=new swa.Autocomplete({el:this.$(".js-origin-airport"),name:"origin",collection:swa.airportCollection,nextFocusCallback:_.bind(this.setFocusToFieldAfterOrigin,this),isAirportCodeOnly:true});
this.registerChildView(this.origin);
b=this.model.get("origin");
this.removeOriginErrorCondition();
this.origin.val(b);
this.updateOriginLabel(b);
this.$(".js-origin-airport-static-label").text(swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.originLabel"));
this.destination=new swa.Autocomplete({el:this.$(".js-destination-airport"),name:"destination",collection:swa.airportCollection,nextFocusCallback:_.bind(this.setFocusToFieldAfterDestination,this),isAirportCodeOnly:true});
this.registerChildView(this.destination);
h=this.model.get("destination");
this.removeDestinationErrorCondition();
this.destination.val(h);
this.updateDestinationLabel(h);
this.$(".js-destination-airport-static-label").text(swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.destinationLabel"));
this.departDate=new swa.DateInput({el:this.$(".js-depart-date"),name:"depart-date",dateSelectorOffsetX:-33,hiddenField:this.$(".js-depart-date-dup"),prevFocusCallback:_.bind(this.setFocusToFieldBeforeDepartDate,this),nextFocusCallback:_.bind(this.setFocusToFieldAfterDepartDate,this)});
this.registerChildView(this.departDate);
e=this.getDepartDate(this.model.get("departDate"));
this.model.set("departDate",e);
this.departDate.setDate(e);
this.updateDepartDate();
this.removeDepartDateErrorCondition();
this.$(".js-depart-date-static-label").text(swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.departDateLabel"));
this.returnDate=new swa.DateInput({el:this.$(".js-return-date"),name:"return-date",dateSelectorOffsetX:-207,hiddenField:this.$(".js-return-date-dup"),prevFocusCallback:_.bind(this.setFocusToFieldBeforeReturnDate,this),nextFocusCallback:_.bind(this.setFocusToFieldAfterReturnDate,this)});
this.registerChildView(this.returnDate);
a=this.getReturnDate(this.model.get("returnDate"));
this.model.set("returnDate",a);
this.returnDate.setDate(a);
this.updateReturnDate();
this.removeReturnDateErrorCondition();
this.$(".js-return-date-static-label").text(swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.returnDateLabel"));
if(f==="false"){this.returnDate.clearDate()
}this.$airAdultTarget.val(this.model.get("adult"));
this.$airSeniorTarget.val(this.model.get("senior"));
this.listenTo(this.tripType,"change",this.updateTripType);
this.listenTo(this.priceType,"change",this.updatePriceType);
this.listenTo(this.origin,"change",this.updateOrigin);
this.listenTo(this.destination,"change",this.updateDestination);
this.listenTo(this.departDate,"change",this.updateDepartDate);
this.listenTo(this.returnDate,"change",this.updateReturnDate);
window.onpageshow=_.bind(function(){var k=this.model.get("tripType"),p=this.model.get("priceType"),j=this.model.get("origin"),o=this.model.get("destination"),m=this.getDepartDate(this.model.get("departDate")),i=this.getReturnDate(this.model.get("returnDate")),l=this.model.get("adult"),n=this.model.get("senior");
this.$(".js-promo-code-trigger").val("");
this.tripType.val(k);
this.updateTripType();
this.priceType.val(p);
this.updatePriceType();
this.origin.val(j);
this.destination.val(o);
this.$(".js-depart-date").val(m);
this.departDate.setDate(m);
this.updateDepartDate();
if(k==="true"){this.$(".js-return-date").val(i);
this.returnDate.setDate(i);
this.updateReturnDate()
}else{this.returnDate.clearDate()
}this.$airAdultTarget.val(l);
this.$airSeniorTarget.val(n)
},this)
},events:{"focus .js-origin-airport":"focusOrigin","focus .js-destination-airport":"focusDestination","focus .js-depart-date":"focusDepartDate","focus .js-return-date":"focusReturnDate","focus .js-air-adult-value":"focusAdultSelector","focus .js-air-senior-value":"focusSeniorSelector","focus .js-promo-code-trigger":"focusPromoCode","click .js-origin-airport-label":"focusToOriginInputField","click .js-destination-airport-label":"focusToDestinationInputField","click .js-depart-date-label":"focusToDepartDateInputField","click .js-return-date-label":"focusToReturnDateInputField","click .js-depart-date-icon":"setFocusToDepartDate","click .js-return-date-icon":"setFocusToReturnDate","click .js-depart-date":"stopBubble","click .js-return-date":"stopBubble","click .js-air-adult":"renderAdultSelector","click .js-air-senior-value":"renderSeniorSelector","click .js-air-senior-icon":"renderSeniorSelector","click .js-air-senior-label":"renderSeniorSelector","click .js-where-we-fly-link":"renderWhereWeFly","blur .js-depart-date":"blurDepartDate","blur .js-return-date":"blurReturnDate","change .js-air-adult-value":"changeAdult","change .js-air-senior-value":"changeSenior",submit:"submit"},focusToOriginInputField:function(a){this.stopBubble(a);
this.origin.$el.focus()
},focusToDestinationInputField:function(a){this.stopBubble(a);
this.destination.$el.focus()
},focusToDepartDateInputField:function(a){this.departDate.$el.focus();
this.focusDepartDate(a)
},focusToReturnDateInputField:function(a){this.returnDate.$el.focus();
this.focusReturnDate(a)
},getDepartDate:function(a){if(a){a=moment(a)
}else{a=moment().add("days",1)
}return a
},getReturnDate:function(a){if(a){a=moment(a)
}else{a=moment().add("days",4)
}return a
},focusOrigin:function(){this.destination.setAirportFilter("");
this.removeOriginErrorCondition()
},focusDestination:function(){var a=this.origin.getAirportCode(),b=this.destination.getAirportCode();
this.destination.setAirportFilter(a);
if((a.length!==0)&&(b.length!==0)){if(!swa.airportCollection.isStationInRoute(swa.airportCollection.findWhere({id:b}))){this.destination.val("")
}}this.removeDestinationErrorCondition()
},focusDepartDate:function(a){swa.page.closeOverlay();
this.removeDepartDateErrorCondition();
this.renderDepartDate(a)
},focusReturnDate:function(a){this.removeReturnDateErrorCondition();
this.renderReturnDate(a)
},focusAdultSelector:function(a){this.renderAdultSelector(a)
},focusSeniorSelector:function(a){this.renderSeniorSelector(a)
},focusPromoCode:function(){swa.page.closeOverlay()
},renderWhereWeFly:function(a){this.stopBubble(a);
swa.page.closeOverlay();
this.whereWeFly=new swa.WhereWeFly({closeCallback:_.bind(this.whereWeFlyClose,this)});
this.whereWeFly.render()
},whereWeFlyClose:function(){this.whereWeFly.close();
this.whereWeFly=null
},setFocusToFieldAfterOrigin:function(){this.destination.el.focus()
},setFocusToFieldAfterDestination:function(a){this.setFocusToDepartDate(a)
},setFocusToDepartDate:function(a){this.stopBubble(a);
this.departDate.el.focus()
},setFocusToReturnDate:function(a){this.stopBubble(a);
this.returnDate.el.focus()
},setFocusToFieldBeforeDepartDate:function(){this.destination.$el.focus().select()
},setFocusToFieldAfterDepartDate:function(a){if(this.returnDate.$el.hasClass(swa.DISABLED_CLASS)){this.focusAdultSelector(a)
}else{this.setFocusToReturnDate()
}},setFocusToFieldBeforeReturnDate:function(){this.setFocusToDepartDate()
},setFocusToFieldAfterReturnDate:function(a){this.focusAdultSelector(a)
},setFocusToFieldBeforeAdultSelector:function(a){if(this.returnDate.$el.hasClass(swa.DISABLED_CLASS)){this.setFocusToDepartDate(a)
}else{this.setFocusToReturnDate(a)
}},setFocusToFieldAfterAdultSelector:function(a){if(this.$airSeniorTarget.hasClass(swa.DISABLED_CLASS)){this.setFocusToFieldAfterSeniorSelector()
}else{this.focusSeniorSelector(a)
}},setFocusToFieldBeforeSeniorSelector:function(a){this.focusAdultSelector(a)
},setFocusToFieldAfterSeniorSelector:function(){this.$(".js-promo-code-trigger").focus()
},renderAdultSelector:function(b){var a=swa.page.getCurrentOverlay();
this.stopBubble(b);
if((!a)||(a.name!=="adultSelector")){this.adult=new swa.TravelerSelector({closeCallback:_.bind(this.closeAdultSelector,this),initialValue:this.model.get("adult"),name:"adultSelector",nextFocusCallback:_.bind(this.setFocusToFieldAfterAdultSelector,this),offsetY:swa.OVERLAY_OFFSET_Y,prevFocusCallback:_.bind(this.setFocusToFieldBeforeAdultSelector,this),target:this.$airAdultTarget,title:swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.adultTitleLabel"),titleDetails:swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.adultTitleDetailsLabel")});
this.listenToOnce(this.adult,"change",this.updateAdult);
swa.page.displayOverlay(this.adult);
this.addSimulatedFocus(this.$airAdultTarget)
}},renderSeniorSelector:function(b){var a=swa.page.getCurrentOverlay();
if($(".js-air-senior-value").hasClass(swa.DISABLED_CLASS)===false){this.stopBubble(b);
if((!a)||(a.name!=="seniorSelector")){this.senior=new swa.TravelerSelector({closeCallback:_.bind(this.closeSeniorSelector,this),initialValue:this.model.get("senior"),name:"seniorSelector",nextFocusCallback:_.bind(this.setFocusToFieldAfterSeniorSelector,this),offsetY:swa.OVERLAY_OFFSET_Y,prevFocusCallback:_.bind(this.setFocusToFieldBeforeSeniorSelector,this),target:this.$airSeniorTarget,title:swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.seniorTitleLabel"),titleDetails:swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.seniorTitleDetailsLabel")});
this.listenToOnce(this.senior,"change",this.updateSenior);
swa.page.displayOverlay(this.senior);
this.addSimulatedFocus(this.$airSeniorTarget)
}}},renderDepartDate:function(f){var d=this.departDate.getDate(),a=this.returnDate.getDate(),c=1,e=[d],b;
this.stopBubble(f);
if(this.tripType.val()==="true"){c=2;
e.push(a)
}b={selectingIndex:0,numberOfSelectableDates:c,selectedDates:e,prevFocusCallback:this.setFocusToFieldBeforeDepartDate,nextFocusCallback:this.setFocusToFieldAfterDepartDate};
this.createDateSelector(b)
},renderReturnDate:function(d){var c=this.departDate.getDate(),a=this.returnDate.getDate(),b={selectingIndex:1,numberOfSelectableDates:2,selectedDates:[c,a],prevFocusCallback:this.setFocusToFieldBeforeReturnDate,nextFocusCallback:this.setFocusToFieldAfterReturnDate};
this.stopBubble(d);
this.createDateSelector(b)
},createDateSelector:function(c){var a=swa.DateTimeFunctions.convertToMoment(this.$("#lastBookableDate").val()),b=moment();
this.dateSelector=new swa.DateSelector({targets:[this.departDate,this.returnDate],prevFocusCallback:_.bind(c.prevFocusCallback,this),nextFocusCallback:_.bind(c.nextFocusCallback,this),dateChangedCallbacks:[_.bind(this.departDateChanged,this),_.bind(this.returnDateChanged,this)],offsetY:swa.OVERLAY_OFFSET_Y,name:"DateSelector",model:new swa.CalendarModel({availableEndDate:a,availableStartDate:b,numberOfSelectableDates:c.numberOfSelectableDates,selectedDates:c.selectedDates,selectingIndex:c.selectingIndex,titles:[swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.calendarTitle1"),swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.calendarTitle2")],tooltips:[swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.calendarTooltip1"),swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.calendarTooltip2")]})});
swa.page.displayOverlay(this.dateSelector)
},departDateChanged:function(a){if((a)&&(a.isValid())){this.removeDepartDateErrorCondition()
}},returnDateChanged:function(a){if((a)&&(a.isValid())){this.removeReturnDateErrorCondition()
}},updateTripType:function(){var d=this.tripType.val(),a=this.$(".js-return-date"),c=this.$(".js-return-date-icon"),b=this.$(".js-return-date-label");
if(d==="false"){a.addClass(swa.DISABLED_CLASS);
a.attr("disabled","disabled");
c.addClass(swa.DISABLED_CLASS);
b.addClass(swa.DISABLED_CLASS);
if(this.returnDate){this.returnDate.clearDate();
this.removeReturnDateErrorCondition()
}}else{a.removeClass(swa.DISABLED_CLASS);
a.removeAttr("disabled");
c.removeClass(swa.DISABLED_CLASS);
b.removeClass(swa.DISABLED_CLASS)
}this.model.set({tripType:d})
},updatePriceType:function(){var c=this.priceType.val(),b=this.$(".js-air-senior-label"),d=this.$(".js-air-senior-icon"),a=this.model;
if(c==="POINTS"){this.$airSeniorTarget.addClass(swa.DISABLED_CLASS).attr("disabled","disabled");
this.$airSeniorTarget.removeClass("booking-form--gradient-treatment");
d.addClass(swa.DISABLED_CLASS);
b.addClass(swa.DISABLED_CLASS);
this.seniorHiddenValueWhenDisabled=this.$airSeniorTarget.val();
a.set({senior:0});
this.$airSeniorTarget.val(0)
}else{this.$airSeniorTarget.removeClass(swa.DISABLED_CLASS).removeAttr("disabled");
this.$airSeniorTarget.addClass("booking-form--gradient-treatment");
d.removeClass(swa.DISABLED_CLASS);
b.removeClass(swa.DISABLED_CLASS);
a.set({senior:this.seniorHiddenValueWhenDisabled});
this.$airSeniorTarget.val(this.seniorHiddenValueWhenDisabled);
this.seniorHiddenValueWhenDisabled=0
}a.set({priceType:c})
},updateOrigin:function(){var a=this.origin.getAirportCode();
this.model.set({origin:a});
this.updateOriginLabel(a)
},updateOriginLabel:function(a){var b=swa.airportCollection.getAirportName(a);
b=(b!=="")?b:swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.originLabel");
this.$(".js-origin-airport-label").html(b)
},updateDestination:function(){var a=this.destination.getAirportCode();
this.model.set({destination:a});
this.updateDestinationLabel(a)
},updateDestinationLabel:function(b){var a=swa.airportCollection.getAirportName(b);
a=(a!=="")?a:swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.destinationLabel");
this.$(".js-destination-airport-label").html(a)
},blurDepartDate:function(){this.updateDepartDate()
},blurReturnDate:function(){this.updateReturnDate()
},closeAdultSelector:function(){this.removeSimulatedFocus(this.$airAdultTarget)
},closeSeniorSelector:function(){this.removeSimulatedFocus(this.$airSeniorTarget)
},updateDepartDate:function(){var a=this.departDate.getDate(),b;
if(swa.DateTimeFunctions.isValidMoment(a)){b=a.format("ddd, ll");
this.$(".js-depart-date-label").html(swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.departLabel",b))
}else{this.$(".js-depart-date-label").html(swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.departDateLabel"))
}this.model.set({departDate:this.departDate.getDate()})
},updateReturnDate:function(){var a=this.returnDate.getDate(),b;
if(swa.DateTimeFunctions.isValidMoment(a)){b=a.format("ddd, ll");
this.$(".js-return-date-label").html(swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.returnLabel",b))
}else{this.$(".js-return-date-label").html(swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.returnDateLabel"))
}this.model.set({returnDate:this.returnDate.getDate()})
},updateAdult:function(){var a=this.adult.val();
this.$airAdultTarget.val(a);
this.model.set({adult:a});
this.listenToOnce(this.adult,"change",this.updateAdult)
},updateSenior:function(){var a=this.senior.val();
this.$airSeniorTarget.val(a);
this.model.set({senior:a});
this.listenToOnce(this.senior,"change",this.updateSenior)
},changeAdult:function(){var a=this.$airAdultTarget.val();
this.model.set({adult:a})
},changeSenior:function(){var a=this.$airSeniorTarget.val();
this.model.set({senior:a})
},removeOriginErrorCondition:function(){this.removeErrorCondition(this.origin.$el,this.$(".js-origin-airport-label"),swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.originLabel"),true)
},removeDestinationErrorCondition:function(){this.removeErrorCondition(this.destination.$el,this.$(".js-destination-airport-label"),swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.destinationLabel"),true)
},removeDepartDateErrorCondition:function(){this.removeErrorCondition(this.departDate.$el,this.$(".js-depart-date-label"),swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.departDateLabel"))
},removeReturnDateErrorCondition:function(){this.removeErrorCondition(this.returnDate.$el,this.$(".js-return-date-label"),swa.getI18nString(swa.applicationResourcesCommon,"airBookingForm.returnDateLabel"))
},close:function(a){swa.page.savePageData();
swa.AirBookingForm.__super__.close.call(this,a)
}});
/*! booking-form-car-model.js */
swa.CarFormModel=swa.FormModel.extend({defaults:{name:"CarFormModel",pickupLocation:"",dropoffLocation:"",pickupDate:null,pickupTime:"11:00 AM",dropoffDate:null,dropoffTime:"11:00 AM",rentalCompany:"All Car Companies",rentalCompanyValue:"SHOP_ALL",vehicleSize:"Economy",vehicleSizeValue:"ECONOMY"},validateConfiguration:{pickupLocation:{field:$(".js-pickup-location"),errorMessageElement:$(".js-pickup-location-label"),errorMessageAccessible:$(".js-pickup-location-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.error.pickUpLocationLabel")},dropoffLocation:{field:$(".js-dropoff-location"),errorMessageElement:$(".js-dropoff-location-label"),errorMessageAccessible:$(".js-dropoff-location-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.error.dropOffLocationLabel")},pickupDate:{field:$(".js-pickup-date"),errorMessageElement:$(".js-pickup-date-label"),errorMessageAccessible:$(".js-pickup-date-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.error.pickUpDateLabel"),validate:function(a,b,d,c){if(!a.isValid(b)){a.addError(c,d)
}}},dropoffDate:{field:$(".js-dropoff-date"),errorMessageElement:$(".js-dropoff-date-label"),errorMessageAccessible:$(".js-dropoff-date-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.error.dropOffDateLabel"),validate:function(a,b,d,c){if(!a.isValid(b)){a.addError(c,d)
}}}}});
/*! booking-form-car-horizontal-view.js */
swa.CarBookingForm=swa.BookingForm.extend({model:null,options:null,pickupLocation:null,dropoffLocation:null,pickupDate:null,dropoffDate:null,initialize:function(j){var i,g,a,d,e,h,b,c,f;
this.childViews=[];
if(!j.name){j.name=j.el.attr("name");
if(!j.name){swa.error("CarBookingForm: name not passed")
}}if(!j.el){swa.error("CarBookingForm: el not passed")
}this.options=j;
i=swa.page.getPageModelData("CarFormModel");
this.model=new swa.CarFormModel(i);
swa.page.registerPageData(this.model);
this.pickupLocation=new swa.Autocomplete({el:this.$(".js-pickup-location"),name:"pickupLocation",collection:swa.carCollection,nextFocusCallback:_.bind(this.setFocusToFieldAfterPickupLocation,this)});
this.registerChildView(this.pickupLocation);
g=this.model.get("pickupLocation");
this.removePickupLocationErrorCondition();
this.pickupLocation.el.value=g;
this.updatePickupLocationLabel(g);
this.$(".js-pickup-location-static-label").text(swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.pickUpLocationLabel"));
this.dropoffLocation=new swa.Autocomplete({el:this.$(".js-dropoff-location"),name:"dropoffLocation",collection:swa.carCollection,nextFocusCallback:_.bind(this.setFocusToFieldAfterDropoffLocation,this)});
this.registerChildView(this.dropoffLocation);
a=this.model.get("dropoffLocation");
this.removeDropoffLocationErrorCondition();
this.dropoffLocation.el.value=a;
this.updateDropoffLocationLabel(a);
this.$(".js-dropoff-location-static-label").text(swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.dropOffLocationLabel"));
this.pickupDate=new swa.DateInput({el:this.$(".js-pickup-date"),name:"pickup-date",dateSelectorOffsetX:145,hiddenField:this.$("input[name='pickUpDate']"),prevFocusCallback:_.bind(this.setFocusToFieldBeforePickUpDate,this),nextFocusCallback:_.bind(this.setFocusToFieldAfterPickUpDate,this)});
this.registerChildView(this.pickupDate);
c=this.getPickupDate(this.model.get("pickupDate"));
this.model.set("pickupDate",c);
this.pickupDate.setDate(c);
this.updatePickupDate();
this.removePickupDateErrorCondition();
this.$(".js-pickup-date-static-label").text(swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.pickUpDateLabel"));
this.dropoffDate=new swa.DateInput({el:this.$(".js-dropoff-date"),name:"dropoff-date",dateSelectorOffsetX:145,hiddenField:this.$("input[name='dropOffDate']"),prevFocusCallback:_.bind(this.setFocusToFieldBeforeDropOffDate,this),nextFocusCallback:_.bind(this.setFocusToFieldAfterDropOffDate,this)});
this.registerChildView(this.dropoffDate);
f=this.getDropoffDate(this.model.get("dropoffDate"));
this.model.set("dropoffDate",f);
this.dropoffDate.setDate(f);
this.updateDropoffDate();
this.removeDropoffDateErrorCondition();
this.$(".js-dropoff-date-static-label").text(swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.dropOffDateLabel"));
d=this.model.get("pickupTime").split(" ");
h=d.join("");
this.$(".js-pickup-time").val(d[0]);
this.$(".js-pickup-car-meridian-indicator").text(d[1]);
this.$(".js-populate-pickup-time").val(h);
e=this.model.get("dropoffTime").split(" ");
b=e.join("");
this.$(".js-dropoff-time").val(e[0]);
this.$(".js-dropoff-car-meridian-indicator").text(e[1]);
this.$(".js-populate-dropoff-time").val(b);
this.$(".js-car-vendor").val(this.model.get("rentalCompany"));
this.$(".js-populate-vendor").val(this.model.get("rentalCompanyValue"));
this.$(".js-car-size").val(this.model.get("vehicleSize"));
this.$(".js-populate-category").val(this.model.get("vehicleSizeValue"));
this.listenTo(this.pickupLocation,"change",this.updatePickupLocation);
this.listenTo(this.dropoffLocation,"change",this.updateDropoffLocation);
this.listenTo(this.pickupDate,"change",this.updatePickupDate);
this.listenTo(this.dropoffDate,"change",this.updateDropoffDate);
window.onpageshow=_.bind(function(){var l=this.model.get("dropoffLocation"),n=this.getPickupDate(this.model.get("pickupDate")),k=this.getDropoffDate(this.model.get("dropoffDate")),o=this.model.get("rentalCompany"),m=this.model.get("vehicleSize");
this.pickupLocation.val(g);
this.dropoffLocation.val(l);
this.$(".js-pickup-date").val(n);
this.pickupDate.setDate(n);
this.updatePickupDate();
this.$(".js-dropoff-date").val(k);
this.dropoffDate.setDate(k);
this.updateDropoffDate();
this.$(".js-car-vendor").val(o);
this.$(".js-car-size").val(m)
},this)
},events:{"focus .js-pickup-location":"focusPickupLocation","focus .js-dropoff-location":"focusDropoffLocation","focus .js-pickup-date":"focusPickupDate","focus .js-dropoff-date":"focusDropoffDate","focus .js-pickup-time":"showPickupTime","click .js-pickup-location-label":"focusToPickupLocationInputField","click .js-pickup-date-label":"focusToPickupDateInputField","click .js-dropoff-location-label":"focusToDropoffLocationInputField","click .js-dropoff-date-label":"focusToDropoffDateInputField","click .js-pickup-time":"showPickupTime","click .js-pickup-car-meridian-indicator":"showPickupTime","click .js-pickup-car-icon-container":"showPickupTime","focus .js-car-vendor":"showCarVendor","click .js-car-vendor":"showCarVendor","click .js-car-vendor-icon":"showCarVendor","focus .js-car-size":"showCarSize","click .js-car-size":"showCarSize","click .js-car-size-icon-small":"showCarSize","click .js-car-size-icon":"showCarSize","focus .js-dropoff-time":"showDropoffTime","click .js-dropoff-time":"showDropoffTime","click .js-dropoff-car-meridian-indicator":"showDropoffTime","click .js-dropoff-car-icon-container":"showDropoffTime","click .js-pickup-date":"stopBubble","click .js-dropoff-date":"stopBubble","click .pick-up-icon":"setFocusToPickupDate","click .drop-off-icon":"setFocusToDropoffDate","blur .js-pickup-date":"blurPickupDate","blur .js-dropoff-date":"blurDropoffDate",submit:"submit"},focusToPickupLocationInputField:function(a){this.stopBubble(a);
this.pickupLocation.$el.focus()
},focusToPickupDateInputField:function(a){this.pickupDate.$el.focus();
this.focusPickupDate(a)
},focusToDropoffLocationInputField:function(a){this.stopBubble(a);
this.dropoffLocation.$el.focus()
},focusToDropoffDateInputField:function(a){this.dropoffDate.$el.focus();
this.focusDropoffDate(a)
},getPickupDate:function(a){if(a){a=moment(a)
}else{a=moment().add("days",1)
}return a
},getDropoffDate:function(a){if(a){a=moment(a)
}else{a=moment().add("days",4);
return a
}return a
},focusPickupLocation:function(){this.removePickupLocationErrorCondition()
},focusDropoffLocation:function(){this.removeDropoffLocationErrorCondition()
},focusPickupDate:function(a){swa.page.closeOverlay();
this.removeDropoffDateErrorCondition();
this.removePickupDateErrorCondition();
this.renderPickupPicker(a)
},focusDropoffDate:function(a){swa.page.closeOverlay();
this.removeDropoffDateErrorCondition();
this.removePickupDateErrorCondition();
this.renderDropoffPicker(a)
},blurPickupDate:function(){this.updatePickupDate()
},blurDropoffDate:function(){this.updateDropoffDate()
},closePickupTime:function(){this.removeSimulatedFocus($(".js-pickup-time"))
},closeDropoffTime:function(){this.removeSimulatedFocus($(".js-dropoff-time"))
},closeCarVendor:function(){this.removeSimulatedFocus($(".js-car-vendor"))
},closeCarSize:function(){this.removeSimulatedFocus($(".js-car-size"))
},setFocusToFieldAfterPickupLocation:function(){this.pickupDate.el.focus()
},setFocusToFieldAfterDropoffLocation:function(){this.dropoffDate.el.focus()
},setFocusToPickupDate:function(a){this.stopBubble(a);
this.pickupDate.el.focus()
},setFocusToFieldAfterPickUpDate:function(){this.$(".js-pickup-time").focus()
},setFocusToFieldBeforePickUpDate:function(){this.pickupLocation.$el.focus().select()
},setFocusToDropoffDate:function(a){this.stopBubble(a);
this.dropoffDate.el.focus()
},setFocusToFieldAfterDropOffDate:function(){this.$(".js-dropoff-time").focus()
},setFocusToFieldBeforeDropOffDate:function(){this.dropoffLocation.$el.focus().select()
},setFocusToFieldBeforeCarVendorSelector:function(){this.$(".js-pickup-time").focus()
},setFocusToFieldAfterCarVendorSelector:function(){this.dropoffLocation.$el.focus().select()
},setFocusToFieldBeforeCarSizeSelector:function(){this.$(".js-dropoff-time").focus()
},setFocusToFieldAfterCarSizeSelector:function(){this.$(".js-link-advanced-search-car").focus()
},renderPickupPicker:function(c){this.stopBubble(c);
var b=this.pickupDate.getDate(),a={nextFocusCallback:this.focusPickupLocation,numberOfSelectableDates:1,prevFocusCallback:this.focusPickupLocation,selectedDates:[b],selectingIndex:0,targets:[this.pickupDate],titles:[swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.calendarTitle1")]};
this.createDateSelector(a)
},renderDropoffPicker:function(c){this.stopBubble(c);
var a=this.dropoffDate.getDate(),b={selectingIndex:0,numberOfSelectableDates:1,selectedDates:[a],prevFocusCallback:this.focusDropoffLocation,nextFocusCallback:this.focusDropoffLocation,targets:[this.dropoffDate],titles:[swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.calendarTitle2")]};
this.createDateSelector(b)
},createDateSelector:function(c){var a=swa.DateTimeFunctions.convertToMoment(this.$("#lastCarBookableDate").val()),b=moment();
this.dateSelector=new swa.DateSelector({selectingIndex:c.selectingIndex,targets:c.targets,prevFocusCallback:_.bind(c.prevFocusCallback,this),nextFocusCallback:_.bind(c.nextFocusCallback,this),offsetY:swa.OVERLAY_OFFSET_Y,name:"DateSelector",model:new swa.CalendarModel({numberOfSelectableDates:c.numberOfSelectableDates,titles:c.titles,selectedDates:c.selectedDates,selectingIndex:c.selectingIndex,availableStartDate:b,availableEndDate:a})});
swa.page.displayOverlay(this.dateSelector)
},setFocusToFieldBeforePickupTimeSelector:function(){this.$(".js-pickup-date").focus()
},setFocusToFieldAfterPickupTimeSelector:function(){this.$(".js-car-vendor").focus()
},setFocusToFieldBeforeDropoffTimeSelector:function(){this.$(".js-dropoff-date").focus()
},setFocusToFieldAfterDropoffTimeSelector:function(){this.$(".js-car-size").focus()
},showCarVendor:function(c){var a=this.$(".js-car-vendor"),b=swa.page.getCurrentOverlay();
this.stopBubble(c);
if((!b)||(b.name!=="carVendor")){this.carVendorSelector=new swa.Menu({closeCallback:_.bind(this.closeCarVendor,this),name:"carVendor",nextFocusCallback:_.bind(this.setFocusToFieldAfterCarVendorSelector,this),offsetY:swa.OVERLAY_OFFSET_Y,overrideClass:"booking-form--car-vendor-menu",prevFocusCallback:_.bind(this.setFocusToFieldBeforeCarVendorSelector,this),target:a});
this.carVendorSelector.update(swa.carVendorCollection,a.val(),this.carVendorSelector.options.overrideClass);
this.carVendorSelector.$el.focus();
this.listenTo(this.carVendorSelector,"change",this.updateVendor);
a.val(this.carVendorSelector.val());
this.addSimulatedFocus(a)
}},showCarSize:function(c){var a=this.$(".js-car-size"),b=swa.page.getCurrentOverlay();
this.stopBubble(c);
if((!b)||(b.name!=="carSize")){this.carSizeSelector=new swa.Menu({closeCallback:_.bind(this.closeCarSize,this),name:"carSize",nextFocusCallback:_.bind(this.setFocusToFieldAfterCarSizeSelector,this),offsetY:swa.OVERLAY_OFFSET_Y,overrideClass:"booking-form--car-size-menu",prevFocusCallback:_.bind(this.setFocusToFieldBeforeCarSizeSelector,this),target:a});
this.carSizeSelector.update(swa.carSizeCollection,a.val(),this.carSizeSelector.options.overrideClass);
this.carSizeSelector.$el.focus();
this.listenTo(this.carSizeSelector,"change",this.updateSize);
a.val(this.carSizeSelector.val());
this.addSimulatedFocus(a)
}},showPickupTime:function(b){var a=swa.page.getCurrentOverlay();
this.stopBubble(b);
if((!a)||(a.name!=="pickupTime")){this.pickupTimeSelector=new swa.TimeSelector({closeCallback:_.bind(this.closePickupTime,this),name:"pickupTime",nextFocusCallback:_.bind(this.setFocusToFieldAfterPickupTimeSelector,this),offsetY:swa.OVERLAY_OFFSET_Y,overrideClass:"booking-form--car-pickup-time-menu",prevFocusCallback:_.bind(this.setFocusToFieldBeforePickupTimeSelector,this),target:this.$(".js-pickup-time")});
this.pickupTimeSelector.$el.focus();
this.listenTo(this.pickupTimeSelector,"change",this.updatePickupTime);
this.addSimulatedFocus(this.$(".js-pickup-time"))
}},showDropoffTime:function(b){var a=swa.page.getCurrentOverlay();
this.stopBubble(b);
if((!a)||(a.name!=="dropoffTime")){this.dropoffTimeSelector=new swa.TimeSelector({closeCallback:_.bind(this.closeDropoffTime,this),name:"dropoffTime",nextFocusCallback:_.bind(this.setFocusToFieldAfterDropoffTimeSelector,this),offsetY:swa.OVERLAY_OFFSET_Y,overrideClass:"booking-form--car-dropoff-time-menu",prevFocusCallback:_.bind(this.setFocusToFieldBeforeDropoffTimeSelector,this),target:this.$(".js-dropoff-time")});
this.dropoffTimeSelector.$el.focus();
this.listenTo(this.dropoffTimeSelector,"change",this.updateDropoffTime);
this.addSimulatedFocus(this.$(".js-dropoff-time"))
}},updatePickupLocation:function(){var a=this.dropoffLocation.val().trim();
var b=this.pickupLocation.val();
this.model.set({pickupLocation:b});
this.updatePickupLocationLabel(b);
if(b&&!a){this.updateDropoffLocation(b)
}else{if(a.length<3){this.dropoffLocation.val("");
this.model.set({dropoffLocation:""});
this.updateDropoffLocationLabel("")
}}},updatePickupLocationLabel:function(a){a=(a!=="")?a:swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.pickUpLocationLabel");
this.$(".js-pickup-location-label").html(a)
},updateDropoffLocation:function(b){var a=this.dropoffLocation.val();
if((b)&&(!a)){a=b;
this.removeDropoffLocationErrorCondition();
this.dropoffLocation.val(a)
}this.model.set({dropoffLocation:a});
this.updateDropoffLocationLabel(a)
},updateDropoffLocationLabel:function(a){a=(a!=="")?a:swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.dropOffLocationLabel");
this.$(".js-dropoff-location-label").html(a)
},updatePickupDate:function(){var a=this.pickupDate.getDate(),b;
if(swa.DateTimeFunctions.isValidMoment(a)){b=a.format("ddd, ll");
this.$(".js-pickup-date-label").html(swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.pickUpLabel",b))
}else{this.$(".js-pickup-date-label").html(swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.pickUpDateLabel"))
}this.model.set({pickupDate:this.pickupDate.getDate()})
},updateDropoffDate:function(){var a=this.dropoffDate.getDate(),b;
if(swa.DateTimeFunctions.isValidMoment(a)){b=a.format("ddd, ll");
this.$(".js-dropoff-date-label").html(swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.dropOffLabel",b))
}else{this.$(".js-dropoff-date-label").html(swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.dropOffDateLabel"))
}this.model.set({dropoffDate:this.dropoffDate.getDate()})
},updateVendor:function(b){var c=this.carVendorSelector.val(),a=this.$(".js-car-vendor");
a.val(c);
this.$(".js-populate-vendor").val(b);
this.model.set({rentalCompany:c});
this.model.set({rentalCompanyValue:b})
},updateSize:function(b){var a=this.carSizeSelector.val(),c=this.$(".js-car-size");
c.val(a);
this.$(".js-populate-category").val(b);
this.model.set({vehicleSize:a});
this.model.set({vehicleSizeValue:b})
},updatePickupTime:function(){var f=this.pickupTimeSelector.val(),d=this.$(".js-pickup-time"),g=this.$(".js-pickup-car-meridian-indicator"),c=f.split(" "),b=c[0],e=c[1],a=c.join("");
d.val(b);
g.text(e);
this.$(".js-populate-pickup-time").val(a);
this.model.set({pickupTime:f});
this.model.set({pickupTimeValue:a})
},updateDropoffTime:function(){var f=this.dropoffTimeSelector.val(),d=this.$(".js-dropoff-time"),g=this.$(".js-dropoff-car-meridian-indicator"),c=f.split(" "),b=c[0],e=c[1],a=c.join("");
d.val(b);
g.text(e);
this.$(".js-populate-dropoff-time").val(a);
this.model.set({dropoffTime:f});
this.model.set({dropoffTimeValue:a})
},removePickupLocationErrorCondition:function(){this.removeErrorCondition(this.pickupLocation.$el,this.$(".js-pickup-location-label"),swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.pickUpLocationLabel"),true)
},removeDropoffLocationErrorCondition:function(){this.removeErrorCondition(this.dropoffLocation.$el,this.$(".js-dropoff-location-label"),swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.dropOffLocationLabel"),true)
},removeDropoffDateErrorCondition:function(){this.removeErrorCondition(this.dropoffDate.$el,this.$(".js-dropoff-date-label"),swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.dropOffDateLabel"))
},removePickupDateErrorCondition:function(){this.removeErrorCondition(this.pickupDate.$el,this.$(".js-pickup-date-label"),swa.getI18nString(swa.applicationResourcesCommon,"carBookingForm.pickUpDateLabel"))
},close:function(a){swa.page.savePageData();
swa.CarBookingForm.__super__.close.call(this,a)
}});
/*! booking-form-hotel-model.js */
swa.HotelFormModel=swa.FormModel.extend({defaults:{name:"HotelFormModel",rapidRewardsOnly:"false",showOnMap:"false",destination:"",checkInDate:null,checkOutDate:null,adult:2,children:0,promoCode:""},validateConfiguration:{destination:{field:$(".js-hotel-destination"),errorMessageElement:$(".js-hotel-destination-label"),errorMessageAccessible:$(".js-hotel-destination-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.error.destinationLabel")},checkInDate:{field:$(".js-check-in-date"),errorMessageElement:$(".js-check-in-date-label"),errorMessageAccessible:$(".js-check-in-date-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.error.checkInDateLabel"),validate:function(a,b,d,c){if(!a.isValid(b)){a.addError(c,d)
}}},checkOutDate:{field:$(".js-check-out-date"),errorMessageElement:$(".js-check-out-date-label"),errorMessageAccessible:$(".js-check-out-date-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.error.checkOutDateLabel"),validate:function(a,b,d,c){if(!a.isValid(b)){a.addError(c,d)
}}}}});
/*! booking-form-hotel-horizontal-view.js */
swa.HotelBookingForm=swa.BookingForm.extend({model:swa.hotelFormModel,options:null,rapidRewardsOnly:null,showOnMap:null,destination:null,checkInDate:null,checkOutDate:null,adult:null,$adultValueInput:null,children:null,$childValueInput:null,initialize:function(b){var e,d,a,c;
this.$adultValueInput=this.$(".js-hotel-adult-value");
this.$childValueInput=this.$(".js-hotel-children-value");
this.childViews=[];
if(!b.name){b.name=b.el.attr("name");
if(!b.name){swa.error("HotelBookingForm: name not passed")
}}if(!b.el){swa.error("HotelBookingForm: el not passed")
}this.options=b;
this.name=b.name;
e=swa.page.getPageModelData("HotelFormModel");
this.model=new swa.HotelFormModel(e);
swa.page.registerPageData(this.model);
this.rapidRewardsOnly=new swa.Checkbox({el:this.$(".js-rapid-rewards-only"),name:"rapid-rewards-only"});
this.registerChildView(this.rapidRewardsOnly);
this.rapidRewardsOnly.val(this.model.get("rapidRewardsOnly"));
this.showOnMap=new swa.Checkbox({el:this.$(".js-show-on-map"),name:"show-on-map"});
this.registerChildView(this.showOnMap);
this.destination=new swa.Autocomplete({el:this.$(".js-hotel-destination"),name:"destination",collection:swa.hotelCollection,nextFocusCallback:_.bind(this.setFocusToFieldAfterDestination,this)});
this.registerChildView(this.destination);
d=this.model.get("destination");
this.removeDestinationErrorCondition();
this.destination.el.value=d;
this.updateDestinationLabel(d);
this.$(".js-hotel-destination-static-label").text(swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.destinationLabel"));
this.checkInDate=new swa.DateInput({el:this.$(".js-check-in-date"),name:"check-in",dateSelectorOffsetX:-33,hiddenField:this.$("input[name='checkInDate']"),prevFocusCallback:_.bind(this.setFocusToFieldBeforeCheckInDate,this),nextFocusCallback:_.bind(this.setFocusToFieldAfterCheckInDate,this)});
this.registerChildView(this.checkInDate);
a=this.getCheckInDate(this.model.get("checkInDate"));
this.model.set("checkInDate",a);
this.checkInDate.setDate(a);
this.updateCheckInDate();
this.removeCheckInErrorCondition();
this.$(".js-check-in-date-static-label").text(swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.checkInDateLabel"));
this.checkOutDate=new swa.DateInput({el:this.$(".js-check-out-date"),name:"check-out",dateSelectorOffsetX:-207,hiddenField:this.$("input[name='checkOutDate']"),prevFocusCallback:_.bind(this.setFocusToFieldBeforeCheckOutDate,this),nextFocusCallback:_.bind(this.setFocusToFieldAfterCheckOutDate,this)});
this.registerChildView(this.checkOutDate);
c=this.getCheckOutDate(this.model.get("checkOutDate"));
this.model.set("checkOutDate",c);
this.checkOutDate.setDate(c);
this.updateCheckOutDate();
this.removeCheckOutErrorCondition();
this.$(".js-check-out-date-static-label").text(swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.checkOutDateLabel"));
this.$adultValueInput.val(this.model.get("adult"));
this.$childValueInput.val(this.model.get("children"));
this.listenTo(this.destination,"change",this.updateDestination);
this.listenTo(this.checkInDate,"change",this.updateCheckInDate);
this.listenTo(this.checkOutDate,"change",this.updateCheckOutDate);
this.listenTo(this.rapidRewardsOnly,"change",this.updateRapidRewardsOnly);
this.listenTo(this.showOnMap,"change",this.updateShowOnMap);
window.onpageshow=_.bind(function(){var j=this.model.get("destination"),f=this.getCheckInDate(this.model.get("checkInDate")),i=this.getCheckOutDate(this.model.get("checkOutDate")),g=this.model.get("adult"),h=this.model.get("children");
this.showOnMap.val(this.model.get("showOnMap"));
this.destination.val(j);
this.$(".js-check-in-date").val(f);
this.checkInDate.setDate(f);
this.updateCheckInDate();
this.$(".js-check-out-date").val(i);
this.checkOutDate.setDate(i);
this.updateCheckOutDate();
this.$adultValueInput.val(g);
this.$childValueInput.val(h)
},this)
},events:{"focus .js-hotel-destination":"focusHotelDestination","focus .js-check-in-date":"focusCheckInDate","focus .js-check-out-date":"focusCheckOutDate","focus .js-hotel-adult-value":"focusAdultSelector","focus .js-hotel-children-value":"focusChildrenSelector","focus .js-promo-code-trigger":"focusPromoCode","click .js-hotel-adult":"renderAdultSelector","click .js-hotel-children":"renderChildrenSelector","click .js-check-in-date":"stopBubble","click .js-check-out-date":"stopBubble","click .check-in-icon":"setFocusToCheckInDate","click .check-out-icon":"setFocusToCheckOutDate","click .js-hotel-destination-label":"focusToHotelDestinationInputField","click .js-check-in-date-label":"focusToHotelCheckInInputField","click .js-check-out-date-label":"focusToHotelCheckOutInputField","change .js-hotel-adult-value":"changeAdult","change .js-hotel-children-value":"changeChildren","blur .js-check-in-date":"blurCheckInDate","blur .js-check-out-date":"blurCheckOutDate",submit:"submit"},getCheckInDate:function(a){if(a){a=moment(a)
}else{a=moment().add("days",1)
}return a
},getCheckOutDate:function(a){if(a){a=moment(a)
}else{a=moment().add("days",4)
}return a
},focusToHotelDestinationInputField:function(a){this.stopBubble(a);
this.destination.$el.focus()
},focusToHotelCheckInInputField:function(a){this.checkInDate.el.focus();
this.renderCheckInPicker(a)
},focusToHotelCheckOutInputField:function(a){this.checkOutDate.el.focus();
this.renderCheckOutPicker(a)
},focusHotelDestination:function(){this.removeDestinationErrorCondition()
},focusAdultSelector:function(a){this.renderAdultSelector(a)
},focusChildrenSelector:function(a){this.renderChildrenSelector(a)
},focusCheckInDate:function(a){swa.page.closeOverlay();
this.removeCheckInErrorCondition();
this.renderCheckInPicker(a)
},focusCheckOutDate:function(a){this.removeCheckOutErrorCondition();
this.renderCheckOutPicker(a)
},focusPromoCode:function(){swa.page.closeOverlay()
},blurCheckInDate:function(){this.updateCheckInDate()
},blurCheckOutDate:function(){this.updateCheckOutDate()
},closeAdultSelector:function(){this.removeSimulatedFocus(this.$adultValueInput)
},closeChildSelector:function(){this.removeSimulatedFocus(this.$childValueInput)
},setFocusToCheckInDate:function(a){this.stopBubble(a);
this.checkInDate.el.focus()
},setFocusToCheckOutDate:function(a){this.stopBubble(a);
this.checkOutDate.el.focus()
},setFocusToFieldAfterDestination:function(a){this.setFocusToCheckInDate(a)
},setFocusToFieldAfterCheckInDate:function(a){this.setFocusToCheckOutDate(a)
},setFocusToFieldBeforeCheckInDate:function(){this.destination.$el.focus().select()
},setFocusToFieldAfterCheckOutDate:function(a){this.focusAdultSelector(a)
},setFocusToFieldBeforeCheckOutDate:function(a){this.setFocusToCheckInDate(a)
},setFocusToFieldBeforeAdultSelector:function(a){this.setFocusToCheckOutDate(a)
},setFocusToFieldAfterAdultSelector:function(a){this.focusChildrenSelector(a)
},setFocusToFieldBeforeChildrenSelector:function(a){this.focusAdultSelector(a)
},setFocusToFieldAfterChildrenSelector:function(){this.$(".js-promo-code-trigger").focus()
},renderCheckInPicker:function(d){this.stopBubble(d);
var c=this.checkInDate.getDate(),a=this.checkOutDate.getDate(),b={selectingIndex:0,numberOfSelectableDates:2,selectedDates:[c,a],prevFocusCallback:this.focusHotelDestination,nextFocusCallback:this.focusCheckOutDate};
this.createDateSelector(b)
},renderCheckOutPicker:function(d){this.stopBubble(d);
var c=this.checkInDate.getDate(),a=this.checkOutDate.getDate(),b={selectingIndex:1,numberOfSelectableDates:2,selectedDates:[c,a],prevFocusCallback:this.focusCheckInDate,nextFocusCallback:this.focusAdultSelector};
this.createDateSelector(b)
},createDateSelector:function(c){var a=swa.DateTimeFunctions.convertToMoment(this.$("#lastHotelBookableDate").val()),b=moment();
this.dateSelector=new swa.DateSelector({targets:[this.checkInDate,this.checkOutDate],prevFocusCallback:_.bind(c.prevFocusCallback,this),nextFocusCallback:_.bind(c.nextFocusCallback,this),dateChangedCallbacks:[_.bind(this.checkInDateChanged,this),_.bind(this.checkOutDateChanged,this)],offsetY:swa.OVERLAY_OFFSET_Y,name:"DateSelector",model:new swa.CalendarModel({availableEndDate:a,availableStartDate:b,numberOfSelectableDates:c.numberOfSelectableDates,selectedDates:c.selectedDates,selectingIndex:c.selectingIndex,titles:[swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.calendarTitle1"),swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.calendarTitle2")],tooltips:[swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.calendarTooltip1"),swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.calendarTooltip2")]})});
swa.page.displayOverlay(this.dateSelector)
},checkInDateChanged:function(a){if((a)&&(a.isValid())){this.removeCheckInErrorCondition()
}},checkOutDateChanged:function(a){if((a)&&(a.isValid())){this.removeCheckOutErrorCondition()
}},renderAdultSelector:function(b){var a=swa.page.getCurrentOverlay();
this.stopBubble(b);
if((!a)||(a.name!=="adultSelector")){this.adult=new swa.TravelerSelector({closeCallback:_.bind(this.closeAdultSelector,this),initialValue:this.model.get("adult"),maxValue:4,minValue:1,name:"adultSelector",nextFocusCallback:_.bind(this.setFocusToFieldAfterAdultSelector,this),offsetY:swa.OVERLAY_OFFSET_Y,prevFocusCallback:_.bind(this.setFocusToFieldBeforeAdultSelector,this),target:this.$adultValueInput,title:swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.adultLabel")});
this.listenToOnce(this.adult,"change",this.updateAdult);
swa.page.displayOverlay(this.adult);
this.addSimulatedFocus(this.$adultValueInput)
}},renderChildrenSelector:function(b){var a=swa.page.getCurrentOverlay();
this.stopBubble(b);
if((!a)||(a.name!=="childrenSelector")){this.children=new swa.TravelerSelector({closeCallback:_.bind(this.closeChildSelector,this),initialValue:this.model.get("children"),maxValue:3,name:"childrenSelector",nextFocusCallback:_.bind(this.setFocusToFieldAfterChildrenSelector,this),offsetY:swa.OVERLAY_OFFSET_Y,prevFocusCallback:_.bind(this.setFocusToFieldBeforeChildrenSelector,this),target:this.$childValueInput,title:swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.childrenLabel")});
this.listenToOnce(this.children,"change",this.updateChildren);
swa.page.displayOverlay(this.children);
this.addSimulatedFocus(this.$childValueInput)
}},updateRapidRewardsOnly:function(){var a=this.rapidRewardsOnly.val();
this.model.set({rapidRewardsOnly:a})
},updateShowOnMap:function(){var a=this.showOnMap.val();
this.model.set({showOnMap:a})
},updateDestination:function(c){var b=this.destination.val(),d=c.split("|"),e,a;
e=d[0];
a=d[d.length-1];
this.$(".js-locationCityName").val(swa.hotelCollection.getCityName(e));
this.$(".js-locationCityId").val(e);
this.$(".js-locationId").val(a);
this.model.set({destination:b});
this.updateDestinationLabel(b)
},updateDestinationLabel:function(a){a=(a!=="")?a:swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.destinationLabel");
this.$(".js-hotel-destination-label").html(a)
},updateCheckInDate:function(){var a=this.checkInDate.getDate(),b;
if(swa.DateTimeFunctions.isValidMoment(a)){b=a.format("ddd, ll");
this.$(".js-check-in-date-label").html(swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.checkInLabel",b))
}else{this.$(".js-check-in-date-label").html(swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.checkInDateLabel"))
}this.model.set({checkInDate:this.checkInDate.getDate()})
},updateCheckOutDate:function(){var a=this.checkOutDate.getDate(),b;
if(swa.DateTimeFunctions.isValidMoment(a)){b=a.format("ddd, ll");
this.$(".js-check-out-date-label").html(swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.checkOutLabel",b))
}else{this.$(".js-check-out-date-label").html(swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.checkOutDateLabel"))
}this.model.set({checkOutDate:this.checkOutDate.getDate()})
},updateAdult:function(){var a=this.adult.val();
this.$adultValueInput.val(a);
this.model.set({adult:a});
this.listenToOnce(this.adult,"change",this.updateAdult)
},updateChildren:function(){var a=this.children.val();
this.$childValueInput.val(a);
this.model.set({children:a});
this.listenToOnce(this.children,"change",this.updateChildren)
},changeAdult:function(){var a=this.$(".js-air-adult-value").val();
this.model.set({adult:a})
},changeChildren:function(){var a=this.$(".js-air-children-value").val();
this.model.set({children:a})
},removeDestinationErrorCondition:function(){this.removeErrorCondition(this.destination.$el,this.$(".js-hotel-destination-label"),swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.destinationLabel"),true)
},removeCheckInErrorCondition:function(){this.removeErrorCondition(this.checkInDate.$el,this.$(".js-check-in-date-label"),swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.checkInDateLabel"))
},removeCheckOutErrorCondition:function(){this.removeErrorCondition(this.checkOutDate.$el,this.$(".js-check-out-date-label"),swa.getI18nString(swa.applicationResourcesCommon,"hotelBookingForm.checkOutDateLabel"))
},close:function(a){swa.page.savePageData();
swa.HotelBookingForm.__super__.close.call(this,a)
}});
/*! booking-form-vacations-model.js */
swa.VacationsFormModel=swa.FormModel.extend({defaults:{adult:2,ADULT_MIN_VALUE:1,children:0,CHILDREN_MAX_VALUE:4,childAges:[0,0,0,0],departDate:"",destination:"",name:"VacationsFormModel",origin:"",promoCode:"",returnDate:"",vacationsType:"AH01"},validateConfiguration:{origin:{field:$(".js-origin-airport"),errorMessageElement:$(".js-origin-airport-label"),errorMessageAccessible:$(".js-origin-airport-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.error.originLabel")},destination:{field:$(".js-destination-airport"),errorMessageElement:$(".js-destination-airport-label"),errorMessageAccessible:$(".js-destination-airport-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.error.destinationLabel")},departDate:{field:$(".js-depart-date"),errorMessageElement:$(".js-depart-date-label"),errorMessageAccessible:$(".js-depart-date-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.error.departDateLabel"),validate:function(a,b,d,c){if(!a.isValid(b)){a.addError(c,d)
}}},returnDate:{field:$(".js-return-date"),errorMessageElement:$(".js-return-date-label"),errorMessageAccessible:$(".js-return-date-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.error.returnDateLabel"),validate:function(a,b,d,c){if(!a.isValid(b)){a.addError(c,d)
}}},childAges:{errorMessageElement:$(".js-vacations-children-ages-label"),errorMessageAccessible:$(".js-vacations-children-ages-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.error.childAges"),validate:function(a,c,f,e){var d=a.get("children"),b;
for(b=1;
b<=d;
b+=1){if(!c[b-1]){f.field=$(".js-vacations-children-age-value-"+b);
a.addError(e,f)
}}}}}});
/*! booking-form-vacations-horizontal-view.js */
swa.VacationsBookingForm=swa.BookingForm.extend({adult:2,children:0,departDate:null,destination:null,isExpandRequired:true,model:null,options:null,origin:null,returnDate:null,vacationsChildSelector:"",vacationsChildValue:"",vacationsType:null,$vacationsAdultTarget:null,$vacationsChildrenAges:null,$vacationsChildrenTarget:null,$vacationsDestinationValue:null,$vacationsOriginValue:null,initialize:function(o){var g,f,m,n,i,h,b,e,l,d,j,a,k,c;
this.childViews=[];
this.$vacationsAdultTarget=this.$(".js-vacations-adult-value");
this.$vacationsBookingFormWrapper=$(".js-booking-forms-wrapper");
this.$vacationsBookingForm=$(".js-booking-panel-vacations");
this.$vacationsChildrenAges=this.$(".js-vacations-children-ages-wrapper");
this.$vacationsChildrenAgesInputs=this.$(".js-vacations-age");
this.$vacationsChildrenAgesSelectors=this.$(".js-vacations-age-selector");
this.$vacationsChildrenTarget=this.$(".js-vacations-children-value");
this.$vacationsChildSelectorsLabel=$(".js-vacations-children-ages-label");
this.$vacationsDestinationValue=this.$(".js-destination-displayed");
this.$vacationsNumberOfTravelers=this.$(".js-vacations-number-of-travelers");
this.$vacationsOriginValue=this.$(".js-origin-displayed");
this.$vacationsTypeTarget=this.$(".js-vacations-type-target");
this.vacationsChildAgeValue=".js-vacations-children-age-value-";
this.vacationsChildSelector=".js-vacations-children-ages-selector";
this.vacationsChildRowFullHeight=61;
this.vacationsChildRowInnerHeight=31;
this.vacationsPanelHeightDefault=196;
if(!o.name){o.name=o.el.attr("name");
if(!o.name){swa.error("VacationsBookingForm: name not passed")
}}if(!o.el){swa.error("VacationsBookingForm: el not passed")
}this.options=o;
this.name=o.name;
n=swa.page.getPageModelData("VacationsFormModel");
this.model=new swa.VacationsFormModel(n);
i=this.model.get("origin");
g=this.model.get("destination");
h=swa.airportCollection.getAirportName(i);
f=swa.airportCollection.getAirportName(g);
m=this.getDepartDate(this.model.get("departDate"));
b=this.getReturnDate(this.model.get("returnDate"));
d=this.model.get("vacationsType");
j=this.model.get("adult");
a=this.model.get("children");
k=this.model.get("childAges");
swa.page.registerPageData(this.model);
this.childrenFromModel=a;
this.origin=new swa.Autocomplete({collection:swa.airportCollection,el:this.$(".js-origin-airport"),isAirportCodeOnly:true,name:"vacations-origin",nextFocusCallback:_.bind(this.setFocusToFieldAfterOrigin,this)});
this.registerChildView(this.origin);
i=this.model.get("origin");
this.removeOriginErrorCondition();
h=swa.airportCollection.getAirportName(i);
this.origin.val(i);
this.$vacationsOriginValue.val(h);
this.updateOriginLabel(i);
this.$(".js-origin-airport-static-label").text(swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.originLabel"));
this.destination=new swa.Autocomplete({collection:swa.airportCollection,el:this.$(".js-destination-airport"),isAirportCodeOnly:true,name:"vacations-destination",nextFocusCallback:_.bind(this.setFocusToFieldAfterDestination,this)});
this.registerChildView(this.destination);
g=this.model.get("destination");
this.removeDestinationErrorCondition();
f=swa.airportCollection.getAirportName(g);
this.destination.val(g);
this.$vacationsDestinationValue.val(f);
this.updateDestinationLabel(g);
this.$(".js-destination-airport-static-label").text(swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.destinationLabel"));
this.departDate=new swa.DateInput({el:this.$(".js-depart-date"),dateSelectorOffsetX:-33,hiddenField:this.$(".js-depart-date-dup"),name:"vacations-depart-date",nextFocusCallback:_.bind(this.setFocusToFieldAfterDepartDate,this),prevFocusCallback:_.bind(this.setFocusToFieldBeforeDepartDate,this)});
this.registerChildView(this.departDate);
m=this.getDepartDate(this.model.get("departDate"));
this.model.set("departDate",m);
this.departDate.setDate(m);
this.updateDepartDate();
this.removeDepartDateErrorCondition();
this.$(".js-depart-date-static-label").text(swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.departDateLabel"));
this.returnDate=new swa.DateInput({dateSelectorOffsetX:-207,el:this.$(".js-return-date"),hiddenField:this.$(".js-return-date-dup"),name:"vacations-return-date",nextFocusCallback:_.bind(this.setFocusToFieldAfterReturnDate,this),prevFocusCallback:_.bind(this.setFocusToFieldBeforeReturnDate,this)});
this.registerChildView(this.returnDate);
b=this.getReturnDate(this.model.get("returnDate"));
this.model.set("returnDate",b);
this.returnDate.setDate(b);
this.updateReturnDate();
this.removeReturnDateErrorCondition();
this.$(".js-return-date-static-label").text(swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.returnDateLabel"));
this.$vacationsAdultTarget.val(this.model.get("adult"));
this.$vacationsChildrenTarget.val(this.childrenFromModel);
_.each(this.$vacationsChildrenAgesSelectors,_.bind(function(p){this.removeChildrenAgeSelectorErrorCondition(p)
},this),this);
if(this.childrenFromModel){this.isExpandRequired=false;
this.showChildrenFields(this.childrenFromModel);
_.each(this.$vacationsChildrenAgesInputs,function(q,p){c=k[p];
if(c){$(q).val((c>9)?c:"0"+c)
}})
}else{this.$vacationsChildrenAges.css({height:0,marginTop:"-"+this.vacationsChildRowInnerHeight+"px",visibility:"hidden"});
this.updateVacationsRender(this.vacationsPanelHeightDefault)
}this.updateNumberOfTravelers();
this.vacationsType=new swa.VacationsType({el:".js-vacations-type"});
e=this.model.get("vacationsType");
if(e){this.vacationsType.val(e)
}this.registerChildView(this.vacationsType);
this.updateVacationsType();
l=$("#js-overlay-template");
if(!l.length){swa.error("VacationsBookingForm: cannot locate overlay template")
}this.listenTo(this.origin,"change",this.updateOrigin);
this.listenTo(this.destination,"change",this.updateDestination);
this.listenTo(this.departDate,"change",this.updateDepartDate);
this.listenTo(this.returnDate,"change",this.updateReturnDate);
this.listenTo(this.vacationsType,"change",this.updateVacationsType);
window.onpageshow=_.bind(function(){i=this.model.get("origin");
g=this.model.get("destination");
h=swa.airportCollection.getAirportName(i);
f=swa.airportCollection.getAirportName(g);
m=this.getDepartDate(this.model.get("departDate"));
b=this.getReturnDate(this.model.get("returnDate"));
d=this.model.get("vacationsType");
j=this.model.get("adult");
a=this.model.get("children");
this.vacationsType.val(d);
this.updateVacationsType();
this.origin.val(i);
this.$vacationsOriginValue.val(h);
this.destination.val(g);
this.$vacationsDestinationValue.val(f);
this.$(".js-depart-date").val(m);
this.departDate.setDate(m);
this.updateDepartDate();
this.$(".js-return-date").val(b);
this.returnDate.setDate(b);
this.updateReturnDate();
this.updateNumberOfTravelers();
this.$vacationsAdultTarget.val(j);
this.$vacationsChildrenTarget.val(a)
},this)
},events:{"focus .js-origin-airport":"focusOrigin","focus .js-destination-airport":"focusDestination","focus .js-depart-date":"focusDepartDate","focus .js-return-date":"focusReturnDate","focus .js-vacations-adult-value":"focusAdultSelector","focus .js-vacations-children-value":"focusChildrenSelector","focus .js-promo-code-trigger":"focusPromoCode","focus .js-vacations-age-selector":"focusChildrenAgesSelector","click .js-origin-airport-label":"focusToOriginAirportInputField","click .js-destination-airport-label":"focusToDestinationAirportInputField","click .js-depart-date-label":"focusToDepartDateInputField","click .js-return-date-label":"focusToReturnDateInputField","click .js-depart-date-icon":"setFocusToDepartDate","click .js-return-date-icon":"setFocusToReturnDate","click .js-depart-date":"stopBubble","click .js-return-date":"stopBubble","click .js-travel-infant-info":"renderTravelInfant","click .js-vacations-adult":"renderAdultSelector","click .js-vacations-children-value":"renderChildrenSelector","click .js-vacations-children-icon":"renderChildrenSelector","click .js-vacations-children-label":"renderChildrenSelector","click .js-vacations-age-selector":"showChildrenAges","click .js-vacations-age-icon":"showChildrenAges","click .js-booking-form-vacations-submit-button":"formSubmit","blur .js-depart-date":"blurDepartDate","blur .js-return-date":"blurReturnDate","change .js-vacations-adult-value":"changeAdult","change .js-vacations-children-value":"changeChildren",submit:"submit"},focusToOriginAirportInputField:function(a){this.stopBubble(a);
this.origin.$el.focus()
},focusToDestinationAirportInputField:function(a){this.stopBubble(a);
this.destination.$el.focus()
},focusToDepartDateInputField:function(a){this.departDate.$el.focus();
this.focusDepartDate(a)
},focusToReturnDateInputField:function(a){this.returnDate.$el.focus();
this.focusReturnDate(a)
},getDepartDate:function(a){if(a){a=moment(a)
}else{a=moment().add("days",1)
}return a
},getReturnDate:function(a){if(a){a=moment(a)
}else{a=moment().add("days",4)
}return a
},focusOrigin:function(){this.destination.setAirportFilter("");
this.removeOriginErrorCondition()
},focusDestination:function(){var a=this.origin.getAirportCode(),b=this.destination.getAirportCode();
this.destination.setAirportFilter(a);
if((a.length!==0)&&(b.length!==0)){if(!swa.airportCollection.isStationInRoute(swa.airportCollection.findWhere({id:b}))){this.destination.val("")
}}this.removeDestinationErrorCondition()
},focusDepartDate:function(a){swa.page.closeOverlay();
this.removeDepartDateErrorCondition();
this.renderDepartDate(a)
},focusReturnDate:function(a){this.removeReturnDateErrorCondition();
this.renderReturnDate(a)
},focusAdultSelector:function(a){this.renderAdultSelector(a)
},focusChildrenSelector:function(a){this.renderChildrenSelector(a)
},focusChildrenAgesSelector:function(a){this.removeChildrenAgeSelectorErrorCondition(a.currentTarget);
this.showChildrenAges(a)
},focusPromoCode:function(){swa.page.closeOverlay()
},setFocusToFieldAfterOrigin:function(){this.destination.el.focus()
},setFocusToFieldAfterDestination:function(a){this.setFocusToDepartDate(a)
},setFocusToDepartDate:function(a){this.stopBubble(a);
this.departDate.el.focus()
},setFocusToReturnDate:function(a){this.stopBubble(a);
this.returnDate.el.focus()
},setFocusToFieldBeforeDepartDate:function(){this.destination.$el.focus().select()
},setFocusToFieldAfterDepartDate:function(){this.setFocusToReturnDate()
},setFocusToFieldBeforeReturnDate:function(){this.setFocusToDepartDate()
},setFocusToFieldAfterReturnDate:function(a){this.focusAdultSelector(a)
},setFocusToFieldBeforeAdultSelector:function(a){this.setFocusToReturnDate(a)
},setFocusToFieldAfterAdultSelector:function(a){this.focusChildrenSelector(a)
},setFocusToFieldBeforeChildrenSelector:function(a){this.focusAdultSelector(a)
},setFocusToFieldAfterChildrenSelector:function(){if(this.model.get("children")>=1){this.$(this.vacationsChildAgeValue+"1").focus()
}else{this.$(".js-promo-code-trigger").focus()
}},setFocusToFieldChildrenAgeSelector:function(e){var c=this.childrenAgeSelector.name,d=parseInt(c.slice(-1)),b=this.$(this.vacationsChildAgeValue+(d-1)),a=this.$(this.vacationsChildAgeValue+(d+1));
this.removeSimulatedFocus(this.$(this.vacationsChildAgeValue+d));
if(e==="before"){if(d>1){b.focus()
}else{this.$(".js-vacations-children-value").focus()
}}else{if((d<4)&&(d+1<=this.children.val())){a.focus()
}else{this.$(".js-vacations-promo-code").focus()
}}},renderTravelInfant:function(b){var a=swa.page.getCurrentOverlay();
this.stopBubble(b);
if((!a)||(a.name!=="travelInfantOverlay")){this.travelInfant=new swa.Overlay({$template:$("#js-overlay-template"),containerPosition:"CENTER",content:$(".js-travel-infant-information").html(),hasCloseIcon:true,name:"travelInfantOverlay",target:$(".js-travel-infant-info")});
swa.page.displayOverlay(this.travelInfant)
}},renderAdultSelector:function(b){var a=swa.page.getCurrentOverlay();
this.stopBubble(b);
if((!a)||(a.name!=="adultSelector")){this.adult=new swa.TravelerSelector({closeCallback:_.bind(this.closeAdultSelector,this),initialValue:this.model.get("adult"),minValue:this.model.get("ADULT_MIN_VALUE"),name:"adultSelector",nextFocusCallback:_.bind(this.setFocusToFieldAfterAdultSelector,this),offsetY:swa.OVERLAY_OFFSET_Y,prevFocusCallback:_.bind(this.setFocusToFieldBeforeAdultSelector,this),target:this.$vacationsAdultTarget,title:swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.adultTitleLabel")});
this.listenToOnce(this.adult,"change",this.updateAdult);
swa.page.displayOverlay(this.adult);
this.addSimulatedFocus(this.$vacationsAdultTarget)
}},renderChildrenSelector:function(b){var a=swa.page.getCurrentOverlay();
if($(".js-vacations-children-value").hasClass(swa.DISABLED_CLASS)===false){this.stopBubble(b);
if((!a)||(a.name!=="childrenSelector")){this.children=new swa.TravelerSelector({closeCallback:_.bind(this.closeChildrenSelector,this),initialValue:this.model.get("children"),maxValue:this.model.get("CHILDREN_MAX_VALUE"),name:"childrenSelector",nextFocusCallback:_.bind(this.setFocusToFieldAfterChildrenSelector,this),offsetY:swa.OVERLAY_OFFSET_Y,prevFocusCallback:_.bind(this.setFocusToFieldBeforeChildrenSelector,this),target:this.$vacationsChildrenTarget,title:swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.childrenTitleLabel")});
this.listenToOnce(this.children,"change",this.updateChildren);
swa.page.displayOverlay(this.children);
this.addSimulatedFocus(this.$vacationsChildrenTarget)
}}},renderDepartDate:function(f){var d=this.departDate.getDate(),a=this.returnDate.getDate(),c=2,e=[d,a],b;
this.stopBubble(f);
b={selectingIndex:0,numberOfSelectableDates:c,selectedDates:e,prevFocusCallback:this.setFocusToFieldBeforeDepartDate,nextFocusCallback:this.setFocusToFieldAfterDepartDate};
this.createDateSelector(b)
},renderReturnDate:function(d){var c=this.departDate.getDate(),a=this.returnDate.getDate(),b={selectingIndex:1,numberOfSelectableDates:2,selectedDates:[c,a],prevFocusCallback:this.setFocusToFieldBeforeReturnDate,nextFocusCallback:this.setFocusToFieldAfterReturnDate};
this.stopBubble(d);
this.createDateSelector(b)
},createDateSelector:function(c){var a=swa.DateTimeFunctions.convertToMoment(this.$("#lastVacationsBookableDate").val()),b=moment();
this.dateSelector=new swa.DateSelector({targets:[this.departDate,this.returnDate],prevFocusCallback:_.bind(c.prevFocusCallback,this),nextFocusCallback:_.bind(c.nextFocusCallback,this),dateChangedCallbacks:[_.bind(this.departDateChanged,this),_.bind(this.returnDateChanged,this)],offsetY:swa.OVERLAY_OFFSET_Y,name:"DateSelector",model:new swa.CalendarModel({availableEndDate:a,availableStartDate:b,numberOfSelectableDates:c.numberOfSelectableDates,selectedDates:c.selectedDates,selectingIndex:c.selectingIndex,titles:[swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.calendarTitle1"),swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.calendarTitle2")],tooltips:[swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.calendarTooltip1"),swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.calendarTooltip2")]})});
swa.page.displayOverlay(this.dateSelector)
},departDateChanged:function(a){if((a)&&(a.isValid())){this.removeDepartDateErrorCondition()
}},returnDateChanged:function(a){if((a)&&(a.isValid())){this.removeReturnDateErrorCondition()
}},updateVacationsType:function(){var a=this.vacationsType.val();
this.model.set({vacationsType:a});
this.$vacationsTypeTarget.val(a)
},updateOrigin:function(){var a=this.origin.getAirportCode();
this.model.set({origin:a});
this.updateOriginLabel(a)
},updateOriginLabel:function(a){var b=swa.airportCollection.getAirportName(a);
b=(b!=="")?b:swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.originLabel");
this.$vacationsOriginValue.val(b);
this.$(".js-origin-airport-label").html(b)
},updateDestination:function(){var a=this.destination.getAirportCode();
this.model.set({destination:a});
this.updateDestinationLabel(a)
},updateDestinationLabel:function(b){var a=swa.airportCollection.getAirportName(b);
a=(a!=="")?a:swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.destinationLabel");
this.$vacationsDestinationValue.val(a);
this.$(".js-destination-airport-label").html(a)
},blurDepartDate:function(){this.updateDepartDate()
},blurReturnDate:function(){this.updateReturnDate()
},closeAdultSelector:function(){this.removeSimulatedFocus(this.$vacationsAdultTarget)
},closeChildrenSelector:function(){this.removeSimulatedFocus(this.$vacationsChildrenTarget)
},closeChildrenAge:function(){this.removeSimulatedFocus(this.$vacationsChildrenAgesSelectors)
},updateDepartDate:function(){var a=this.departDate.getDate(),b;
if(swa.DateTimeFunctions.isValidMoment(a)){b=a.format("ddd, ll");
this.$(".js-depart-date-label").html(swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.departLabel",b))
}else{this.$(".js-depart-date-label").html(swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.departDateLabel"))
}this.model.set({departDate:this.departDate.getDate()})
},updateReturnDate:function(){var a=this.returnDate.getDate(),b;
if(swa.DateTimeFunctions.isValidMoment(a)){b=a.format("ddd, ll");
this.$(".js-return-date-label").html(swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.returnLabel",b))
}else{this.$(".js-return-date-label").html(swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.returnDateLabel"))
}this.model.set({returnDate:this.returnDate.getDate()})
},updateAdult:function(){var a=this.adult.val();
this.$vacationsAdultTarget.val(a);
this.model.set({adult:a});
this.updateNumberOfTravelers();
this.listenToOnce(this.adult,"change",this.updateAdult)
},updateChildren:function(){var c=this.children.val();
var b=this.model.get("children");
var a=(c>b);
this.model.set({children:c});
this.showChildrenFields(c,a);
this.$vacationsChildrenTarget.val(c);
this.updateNumberOfTravelers();
this.listenToOnce(this.children,"change",this.updateChildren)
},updateNumberOfTravelers:function(){var a=this.model.get("children")+this.model.get("adult");
this.$vacationsNumberOfTravelers.val(a)
},updateAge:function(){var b=parseInt(this.childrenAgeSelector.val()),a=isNaN(b)?1:b,g=parseInt(this.childrenAgeSelector.name.slice(-1)),e=this.$(this.vacationsChildAgeValue+g),d=this.$vacationsChildrenAgesInputs,f=this.model.get("childAges"),c=(a>9)?a:"0"+a;
this.removeChildrenAgeSelectorErrorCondition(e);
e.val(a);
f[g-1]=a;
d.eq(g-1).val(c);
this.model.set({childAges:f})
},showChildrenFields:function(c,a){var b={};
if(c>0&&this.isExpandRequired){this.updateVacationsRender(this.vacationsPanelHeightDefault+this.vacationsChildRowFullHeight);
this.isExpandRequired=false;
b={css:{height:this.vacationsChildRowInnerHeight+"px",marginTop:0,visibility:"visible"}}
}else{if(c===0&&!this.isExpandRequired){this.updateVacationsRender(this.vacationsPanelHeightDefault);
this.isExpandRequired=true;
b={css:{height:0,marginTop:"-"+this.vacationsChildRowInnerHeight+"px",visibility:"hidden"}}
}}swa.animationService.to(this.$vacationsChildrenAges,0.5,b);
this.animateChildFields(c,this.$vacationsChildrenAges,a,$(this.vacationsChildSelector));
this.updateChildFieldsValue()
},updateChildFieldsValue:function(){var a=this.$vacationsChildrenAges.find("input"),d=this.model.get("children"),c=this.model.get("childAges"),b=this.$vacationsChildrenAgesInputs;
_.each(a,function(h,e){var g=c[e];
var f=(g<10)?"0"+g:g;
a.eq(e).val(g);
b.eq(e).val(f);
if(e+1>d){a.eq(e).val("");
b.eq(e).val("");
c[e]=undefined
}});
this.model.set({childAges:c})
},animateChildFields:function(b,k,c,i){var d=$(this.vacationsChildSelector).outerWidth(true),a=b,e=this.$vacationsChildSelectorsLabel.outerWidth(true),j=(this.$vacationsChildrenAges.width()-e),g=-j+((d*a)||(d)),f={css:{marginRight:g+"px"},ease:"Quint.easeOut"},h=_.bind(function(){this.setChildSelectorVisibility(i,a)
},this);
if(c){f.onStart=h
}else{f.onComplete=h
}swa.animationService.to(k,0.5,f)
},setChildSelectorVisibility:function(b,a){var c,d;
_.each(b,_.bind(function(f,e){d=b.eq(e);
c=e+1>a;
d.css("visibility",(c)?"hidden":"visible");
if(c){this.removeChildrenAgeSelectorErrorCondition(d.find("input"))
}},this),this)
},updateVacationsRender:function(a){swa.animationService.to(this.$vacationsBookingFormWrapper,1,{css:{height:a},ease:"Quint.easeOut"})
},showChildrenAges:function(d){var a=$(d.currentTarget),c=swa.page.getCurrentOverlay(),b=a.attr("id");
if(b.indexOf("-icon")>-1){b=b.replace("-icon","");
a=$(this.vacationsChildAgeValue+b.slice(-1))
}this.stopBubble(d);
if((!c)||(c.name!==b)){this.childrenAgeSelector=new swa.Menu({closeCallback:_.bind(this.closeChildrenAge,this),name:b,nextFocusCallback:_.bind(this.setFocusToFieldChildrenAgeSelector,this,"after"),offsetY:swa.OVERLAY_OFFSET_Y,overrideClass:"booking-form--age-child-menu",prevFocusCallback:_.bind(this.setFocusToFieldChildrenAgeSelector,this,"before"),target:a});
this.childrenAgeSelector.update(swa.childrenAgeCollection,a.val(),this.childrenAgeSelector.options.overrideClass);
this.childrenAgeSelector.$el.focus();
this.listenTo(this.childrenAgeSelector,"change",this.updateAge);
a.val(this.childrenAgeSelector.val());
this.updateAge();
this.addSimulatedFocus(a)
}},changeAdult:function(){var a=this.$vacationsAdultTarget.val();
this.model.set({adult:a})
},changeChildren:function(){var a=this.$vacationsChildrenTarget.val();
this.model.set({children:a})
},removeOriginErrorCondition:function(){this.removeErrorCondition(this.origin.$el,this.$(".js-origin-airport-label"),swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.originLabel"),true)
},removeDestinationErrorCondition:function(){this.removeErrorCondition(this.destination.$el,this.$(".js-destination-airport-label"),swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.destinationLabel"),true)
},removeDepartDateErrorCondition:function(){this.removeErrorCondition(this.departDate.$el,this.$(".js-depart-date-label"),swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.departDateLabel"))
},removeReturnDateErrorCondition:function(){this.removeErrorCondition(this.returnDate.$el,this.$(".js-return-date-label"),swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.returnDateLabel"))
},removeChildrenAgeSelectorErrorCondition:function(c){var a=false,b;
$(c).removeClass("error-field");
for(b=0;
b<=this.model.get("children");
b+=1){if($(this.vacationsChildSelector).eq(b).is(":visible")&&$(this.vacationsChildAgeValue+b).hasClass("error-field")){a=true;
break
}}if(!a){this.removeErrorCondition($(c),this.$vacationsChildSelectorsLabel,swa.getI18nString(swa.applicationResourcesCommon,"vacationsBookingForm.childrenAgesLabel"))
}},close:function(a){swa.page.savePageData();
swa.VacationsBookingForm.__super__.close.call(this,a)
},formSubmit:function(a){this.stopBubble(a);
if(this.$vacationsNumberOfTravelers.val()>8){this.$vacationsBookingForm.attr("action","https://www.southwest.com/flight/vacationspackages.html?swa_vacationsLanding_submit");
this.$vacationsBookingForm.attr("method","POST");
this.$vacationsBookingForm.attr("target","_self");
$("#origin_displayed").val(this.model.get("origin"));
$("#destination_displayed").val(this.model.get("destination"))
}else{this.$vacationsBookingForm.attr("action","http://res.southwestvacations.com/search/ExternalFormPost.aspx");
this.$vacationsBookingForm.attr("method","GET")
}this.$vacationsBookingForm.submit()
}});
/*! booking-form-check-in-model.js */
swa.CheckInFormModel=swa.FormModel.extend({defaults:{confirmationNumber:"",firstName:"",lastName:"",name:"CheckInFormModel"},validateConfiguration:{confirmationNumber:{field:$(".js-check-in-confirmation-number"),errorMessageElement:$(".js-check-in-confirmation-number-label"),errorMessageAccessible:$(".js-check-in-confirmation-number-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"checkInForm.error.confirmationNumberLabel")},firstName:{field:$(".js-check-in-first-name"),errorMessageElement:$(".js-check-in-first-name-label"),errorMessageAccessible:$(".js-check-in-first-name-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"checkInForm.error.firstNameLabel")},lastName:{field:$(".js-check-in-last-name"),errorMessageElement:$(".js-check-in-last-name-label"),errorMessageAccessible:$(".js-check-in-last-name-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"checkInForm.error.lastNameLabel")}}});
/*! booking-form-check-in-horizontal-view.js */
swa.CheckInForm=swa.BookingForm.extend({model:null,initialize:function(l){var d="change",e=".js-check-in-confirmation-number",c=".js-check-in-first-name",g=".js-check-in-last-name",f="confirmationNumber",a,b="firstName",i,h="lastName",j,k;
this.childViews=[];
if(!l.name){l.name=l.el.attr("name");
if(!l.name){swa.error("CheckInForm: name not passed")
}}if(!l.el){swa.error("CheckInForm: el not passed")
}this.options=l;
this.name=l.name;
k=swa.page.getPageModelData("CheckInFormModel");
this.model=new swa.CheckInFormModel(k);
swa.page.registerPageData(this.model);
this.confirmationNumber=new swa.View({$el:this.$(e),el:e});
this.registerChildView(this.confirmationNumber);
this.confirmationNumber.el.value=this.model.get(f);
this.removeConfirmationNumberErrorCondition();
this.firstName=new swa.View({$el:this.$(c),el:c});
this.registerChildView(this.firstName);
this.firstName.el.value=this.model.get(b);
this.removeFirstNameErrorCondition();
this.lastName=new swa.View({$el:this.$(g),el:g});
this.registerChildView(this.lastName);
this.lastName.el.value=this.model.get(h);
this.removeLastNameErrorCondition();
this.listenTo(this.confirmationNumber,d,this.updateConfirmationNumber);
this.listenTo(this.firstName,d,this.updateFirstName);
this.listenTo(this.lastName,d,this.updateLastName);
window.onpageshow=_.bind(function(){a=this.model.get(f);
i=this.model.get(b);
j=this.model.get(h);
this.confirmationNumber.el.value=a;
this.firstName.el.value=i;
this.lastName.el.value=j
},this)
},events:{"focus .js-check-in-confirmation-number":"focusConfirmationNumber","focus .js-check-in-first-name":"focusFirstName","focus .js-check-in-last-name":"focusLastName","change .js-check-in-confirmation-number":"updateConfirmationNumber","change .js-check-in-first-name":"updateFirstName","change .js-check-in-last-name":"updateLastName",submit:"submit"},focusConfirmationNumber:function(){this.removeConfirmationNumberErrorCondition()
},updateConfirmationNumber:function(){this.model.set({confirmationNumber:this.confirmationNumber.el.value})
},removeConfirmationNumberErrorCondition:function(){this.removeErrorCondition(this.confirmationNumber.$el,this.$(".js-check-in-confirmation-number-label"),swa.getI18nString(swa.applicationResourcesCommon,"checkInForm.confirmationNumberLabel"),true)
},focusFirstName:function(){this.removeFirstNameErrorCondition()
},updateFirstName:function(){this.model.set({firstName:this.firstName.el.value})
},removeFirstNameErrorCondition:function(){this.removeErrorCondition(this.firstName.$el,this.$(".js-check-in-first-name-label"),swa.getI18nString(swa.applicationResourcesCommon,"checkInForm.firstNameLabel"),true)
},focusLastName:function(){this.removeLastNameErrorCondition()
},updateLastName:function(){this.model.set({lastName:this.lastName.el.value})
},removeLastNameErrorCondition:function(){this.removeErrorCondition(this.lastName.$el,this.$(".js-check-in-last-name-label"),swa.getI18nString(swa.applicationResourcesCommon,"checkInForm.lastNameLabel"),true)
},close:function(a){swa.page.savePageData();
swa.CheckInForm.__super__.close.call(this,a)
}});
/*! booking-form-change-flight-model.js */
swa.ChangeFlightFormModel=swa.FormModel.extend({defaults:{confirmationNumber:"",firstName:"",lastName:"",name:"ChangeFlightFormModel"},validateConfiguration:{confirmationNumber:{field:$(".js-change-flight-confirmation-number"),errorMessageElement:$(".js-change-flight-confirmation-number-label"),errorMessageAccessible:$(".js-change-flight-confirmation-number-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"changeFlightForm.error.confirmationNumberLabel")},firstName:{field:$(".js-change-flight-first-name"),errorMessageElement:$(".js-change-flight-first-name-label"),errorMessageAccessible:$(".js-change-flight-first-name-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"changeFlightForm.error.firstNameLabel")},lastName:{field:$(".js-change-flight-last-name"),errorMessageElement:$(".js-change-flight-last-name-label"),errorMessageAccessible:$(".js-change-flight-last-name-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"changeFlightForm.error.lastNameLabel")}}});
/*! booking-form-change-flight-horizontal-view.js */
swa.ChangeFlightForm=swa.BookingForm.extend({model:null,initialize:function(k){var d="change",c=".js-change-flight-confirmation-number",l=".js-change-flight-first-name",j=".js-change-flight-last-name",e="confirmationNumber",a,b="firstName",g,f="lastName",h,i;
this.childViews=[];
if(!k.name){k.name=k.el.attr("name");
if(!k.name){swa.error("ChangeFlightForm: name not passed")
}}if(!k.el){swa.error("ChangeFlightForm: el not passed")
}this.options=k;
this.name=k.name;
i=swa.page.getPageModelData("ChangeFlightFormModel");
this.model=new swa.ChangeFlightFormModel(i);
swa.page.registerPageData(this.model);
this.confirmationNumber=new swa.View({$el:this.$(c),el:c});
this.registerChildView(this.confirmationNumber);
this.confirmationNumber.el.value=this.model.get(e);
this.removeConfirmationNumberErrorCondition();
this.firstName=new swa.View({$el:this.$(l),el:l});
this.registerChildView(this.firstName);
this.firstName.el.value=this.model.get(b);
this.removeFirstNameErrorCondition();
this.lastName=new swa.View({$el:this.$(j),el:j});
this.registerChildView(this.lastName);
this.lastName.el.value=this.model.get(f);
this.removeLastNameErrorCondition();
this.listenTo(this.confirmationNumber,d,this.updateConfirmationNumber);
this.listenTo(this.firstName,d,this.updateFirstName);
this.listenTo(this.lastName,d,this.updateLastName);
window.onpageshow=_.bind(function(){a=this.model.get(e);
g=this.model.get(b);
h=this.model.get(f);
this.confirmationNumber.el.value=a;
this.firstName.el.value=g;
this.lastName.el.value=h
},this)
},events:{"focus .js-change-flight-confirmation-number":"focusConfirmationNumber","focus .js-change-flight-first-name":"focusFirstName","focus .js-change-flight-last-name":"focusLastName","change .js-change-flight-confirmation-number":"updateConfirmationNumber","change .js-change-flight-first-name":"updateFirstName","change .js-change-flight-last-name":"updateLastName",submit:"submit"},focusConfirmationNumber:function(){this.removeConfirmationNumberErrorCondition()
},updateConfirmationNumber:function(){this.model.set({confirmationNumber:this.confirmationNumber.el.value})
},removeConfirmationNumberErrorCondition:function(){this.removeErrorCondition(this.confirmationNumber.$el,this.$(".js-change-flight-confirmation-number-label"),swa.getI18nString(swa.applicationResourcesCommon,"changeFlightForm.confirmationNumberLabel"),true)
},focusFirstName:function(){this.removeFirstNameErrorCondition()
},updateFirstName:function(){this.model.set({firstName:this.firstName.el.value})
},removeFirstNameErrorCondition:function(){this.removeErrorCondition(this.firstName.$el,this.$(".js-change-flight-first-name-label"),swa.getI18nString(swa.applicationResourcesCommon,"changeFlightForm.firstNameLabel"),true)
},focusLastName:function(){this.removeLastNameErrorCondition()
},updateLastName:function(){this.model.set({lastName:this.lastName.el.value})
},removeLastNameErrorCondition:function(){this.removeErrorCondition(this.lastName.$el,this.$(".js-change-flight-last-name-label"),swa.getI18nString(swa.applicationResourcesCommon,"changeFlightForm.lastNameLabel"),true)
},close:function(a){swa.page.savePageData();
swa.ChangeFlightForm.__super__.close.call(this,a)
}});
/*! booking-form-flight-status-model.js */
swa.FlightStatusFormModel=swa.FormModel.extend({defaults:{name:"FlightStatusFormModel",originAirport:"",destinationAirport:"",travelDateDisplayed:swa.getI18nString(swa.applicationResourcesCommon,"commonDateDisplay.todayDateShort",moment().format(swa.getI18nString(swa.applicationResourcesCommon,"dateFormats.monthDayShort"))),travelDate:moment().format("MM/DD/YYYY"),travelDateLabel:swa.getI18nString(swa.applicationResourcesCommon,"flightStatusForm.departDateLabel",[moment().format("ddd"),moment().format(swa.getI18nString(swa.applicationResourcesCommon,"dateFormats.monthDayYearLong"))]),flightNumber:""},validateConfiguration:{originAirport:{field:$(".js-flight-status-origin-airport"),errorMessageElement:$(".js-flight-status-origin-airport-label"),errorMessageAccessible:$(".js-flight-status-origin-airport-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"flightStatusForm.error.originAirportLabel")},destinationAirport:{field:$(".js-flight-status-destination-airport"),errorMessageElement:$(".js-flight-status-destination-airport-label"),errorMessageAccessible:$(".js-flight-status-destination-airport-label-error"),errorMessage:swa.getI18nString(swa.applicationResourcesCommon,"flightStatusForm.error.destinationAirportLabel")}}});
/*! booking-form-flight-status-horizontal-view.js */
swa.FlightStatusForm=swa.BookingForm.extend({CHANGE:"change",DATA:"data",TRAVEL_DATE:"travelDate",$DEPART_DATE:this.$(".js-flight-status-depart-date"),$DEPART_DATE_DISPLAYED:this.$(".js-flight-status-depart-date-displayed"),$DEPART_DATE_LABEL:this.$(".js-flight-status-depart-date-label"),$DESTINATION_AIRPORT_LABEL:this.$(".js-flight-status-destination-airport-label"),$ORIGIN_AIRPORT_LABEL:this.$(".js-flight-status-origin-airport-label"),model:null,initialize:function(o){var c="destinationAirport",f,h,b="flightNumber",g,i="originAirport",d,j,n,l="travelDateDisplayed",m="travelDateLabel",k,a,e;
this.childViews=[];
if(!o.name){o.name=o.el.attr("name");
if(!o.name){swa.error("FlightStatusForm: name not passed")
}}if(!o.el){swa.error("FlightStatusForm: el not passed")
}this.options=o;
this.name=o.name;
n=swa.page.getPageModelData("FlightStatusFormModel");
this.model=new swa.FlightStatusFormModel(n);
swa.page.registerPageData(this.model);
this.originAirport=new swa.Autocomplete({el:".js-flight-status-origin-airport",name:i,collection:swa.airportCollection,nextFocusCallback:_.bind(this.setFocusToFieldAfterOriginAirport,this),isAirportCodeOnly:true});
this.registerChildView(this.originAirport);
j=this.model.get(i);
this.originAirport.el.value=this.model.get(i);
this.removeOriginAirportErrorCondition();
this.updateOriginAirportLabel(j);
this.$(".js-flight-status-origin-airport-static-label").text(swa.getI18nString(swa.applicationResourcesCommon,"flightStatusForm.originAirportLabel"));
this.destinationAirport=new swa.Autocomplete({el:".js-flight-status-destination-airport",name:c,collection:swa.airportCollection,nextFocusCallback:_.bind(this.setFocusToFieldAfterDestinationAirport,this),isAirportCodeOnly:true});
this.registerChildView(this.destinationAirport);
h=this.model.get(c);
this.destinationAirport.el.value=this.model.get(c);
this.removeDestinationAirportErrorCondition();
this.updateDestinationAirportLabel(h);
this.$(".js-flight-status-destination-airport-static-label").text(swa.getI18nString(swa.applicationResourcesCommon,"flightStatusForm.destinationAirportLabel"));
this.flightNumber=new swa.View({el:".js-flight-status-flight-number"});
this.registerChildView(this.flightNumber);
g=this.model.get(b);
this.flightNumber.el.value=this.model.get(b);
k=this.model.get(this.TRAVEL_DATE);
a=this.model.get(l);
e=this.model.get(m);
this.$DEPART_DATE.val(k);
this.$DEPART_DATE_DISPLAYED.val(a);
this.$DEPART_DATE_DISPLAYED.attr(this.DATA,k);
this.$DEPART_DATE_LABEL.html(e);
this.listenTo(this.originAirport,this.CHANGE,this.updateOriginAirport);
this.listenTo(this.destinationAirport,this.CHANGE,this.updateDestinationAirport);
this.listenTo(this.flightNumber,this.CHANGE,this.updateFlightNumber);
window.onpageshow=_.bind(function(){k=this.model.get(this.TRAVEL_DATE);
a=this.model.get(l);
e=this.model.get(m);
g=this.model.get(b);
d=this.model.get(i);
f=this.model.get(c);
this.$DEPART_DATE.val(k);
this.$DEPART_DATE_DISPLAYED.val(a);
this.$DEPART_DATE_DISPLAYED.attr(this.DATA,k);
this.$DEPART_DATE_LABEL.html(e);
this.originAirport.el.value=d;
this.destinationAirport.el.value=f;
this.flightNumber.el.value=g
},this)
},events:{"focus .js-flight-status-origin-airport":"focusOriginAirport","focus .js-flight-status-destination-airport":"focusDestinationAirport","focus .js-flight-status-depart-date-displayed":"showTravelDate","click .js-flight-status-origin-airport-label":"focusToOriginAirportInputField","click .js-flight-status-destination-airport-label":"focusToDestinationAirportInputField","click .js-flight-status-depart-date-label":"focusToTravelDateInputField","click .js-flight-status-depart-date-displayed":"showTravelDate","click .js-flight-status-depart-date-icon":"showTravelDate","change .js-flight-status-origin-airport":"updateOriginAirport","change .js-flight-status-destination-airport":"updateDestinationAirport","change .js-flight-status-flight-number":"updateFlightNumber",submit:"submit"},focusToOriginAirportInputField:function(a){this.stopBubble(a);
this.originAirport.$el.focus()
},focusToDestinationAirportInputField:function(a){this.stopBubble(a);
this.destinationAirport.$el.focus()
},focusToTravelDateInputField:function(a){this.showTravelDate(a)
},setFocusToFieldAfterOriginAirport:function(){this.destinationAirport.$el.focus()
},setFocusToFieldAfterDestinationAirport:function(){this.$DEPART_DATE_DISPLAYED.focus()
},setFocusToFieldAfterDepartDateSelector:function(a){this.setFocusToFlightNumber(a)
},setFocusToFieldBeforeDepartDateSelector:function(a){this.setFocusToDestinationAirport(a)
},focusOriginAirport:function(){this.destinationAirport.setAirportFilter("");
this.removeOriginAirportErrorCondition()
},updateOriginAirport:function(){var a=this.originAirport.el.value;
this.updateOriginAirportLabel(a);
this.model.set({originAirport:a})
},updateOriginAirportLabel:function(a){var b=swa.airportCollection.getAirportName(a);
b=(b!=="")?b:swa.getI18nString(swa.applicationResourcesCommon,"flightStatusForm.originAirportLabel");
this.$ORIGIN_AIRPORT_LABEL.html(b)
},removeOriginAirportErrorCondition:function(){this.removeErrorCondition(this.originAirport.$el,this.$ORIGIN_AIRPORT_LABEL,swa.getI18nString(swa.applicationResourcesCommon,"flightStatusForm.originAirportLabel"),true)
},focusDestinationAirport:function(){var a=this.originAirport.getAirportCode(),b=this.destinationAirport.getAirportCode();
this.destinationAirport.setAirportFilter(a);
if((a.length!==0)&&(b.length!==0)){if(!swa.airportCollection.isStationInRoute(swa.airportCollection.findWhere({id:b}))){this.destinationAirport.val("")
}}this.removeDestinationAirportErrorCondition()
},setFocusToDestinationAirport:function(a){if(a){this.stopBubble(a)
}this.destinationAirport.$el.focus()
},setFocusToFlightNumber:function(a){if(a){this.stopBubble(a)
}this.flightNumber.el.focus()
},updateDestinationAirport:function(){var a=this.destinationAirport.el.value;
this.updateDestinationAirportLabel(a);
this.model.set({destinationAirport:a})
},updateDestinationAirportLabel:function(b){var a=swa.airportCollection.getAirportName(b);
a=(a!=="")?a:swa.getI18nString(swa.applicationResourcesCommon,"flightStatusForm.destinationAirportLabel");
this.$DESTINATION_AIRPORT_LABEL.html(a)
},removeDestinationAirportErrorCondition:function(){this.removeErrorCondition(this.destinationAirport.$el,this.$DESTINATION_AIRPORT_LABEL,swa.getI18nString(swa.applicationResourcesCommon,"flightStatusForm.destinationAirportLabel"),true)
},showTravelDate:function(c){var a=this.$DEPART_DATE_DISPLAYED,b=swa.page.getCurrentOverlay();
this.stopBubble(c);
if((!b)||(b.name!==this.TRAVEL_DATE)){this.travelDateSelector=new swa.Menu({closeCallback:_.bind(this.closeTravelDate,this),name:this.TRAVEL_DATE,nextFocusCallback:_.bind(this.setFocusToFieldAfterDepartDateSelector,this),offsetY:swa.OVERLAY_OFFSET_Y,overrideClass:"flight-status--travel-date-menu",prevFocusCallback:_.bind(this.setFocusToFieldBeforeDepartDateSelector,this),target:a});
this.travelDateSelector.update(swa.flightStatusCollection,a.val(),this.travelDateSelector.options.overrideClass);
this.travelDateSelector.$el.focus();
this.listenTo(this.travelDateSelector,this.CHANGE,this.updateTravelDate);
this.addSimulatedFocus(a)
}},updateTravelDate:function(b){var a=this.$DEPART_DATE_DISPLAYED,d=moment(),e=moment(b,"MM-DD-YYYY"),c=d.diff(e,"days",true),f;
switch(true){case c>=1:f=swa.getI18nString(swa.applicationResourcesCommon,"commonDateDisplay.yesterdayDateShort",e.format(swa.getI18nString(swa.applicationResourcesCommon,"dateFormats.monthDayShort")));
break;
case c>=0:f=swa.getI18nString(swa.applicationResourcesCommon,"commonDateDisplay.todayDateShort",e.format(swa.getI18nString(swa.applicationResourcesCommon,"dateFormats.monthDayShort")));
break;
default:f=swa.getI18nString(swa.applicationResourcesCommon,"commonDateDisplay.tomorrowDateShort",e.format(swa.getI18nString(swa.applicationResourcesCommon,"dateFormats.monthDayShort")));
break
}a.val(f);
a.attr(this.DATA,b);
this.$DEPART_DATE.val(b);
this.model.set({travelDateDisplayed:f});
this.model.set({travelDate:b});
this.updateTravelDateLabel(e)
},updateTravelDateLabel:function(b){var a=swa.getI18nString(swa.applicationResourcesCommon,"flightStatusForm.departDateLabel",[moment(b).format("ddd"),moment(b).format(swa.getI18nString(swa.applicationResourcesCommon,"dateFormats.monthDayYearLong"))]);
this.$DEPART_DATE_LABEL.html(a);
this.model.set({travelDateLabel:a})
},updateFlightNumber:function(){this.model.set({flightNumber:this.flightNumber.el.value})
},closeTravelDate:function(){this.removeSimulatedFocus(this.$DEPART_DATE_DISPLAYED)
},close:function(a){swa.page.savePageData();
swa.FlightStatusForm.__super__.close.call(this,a)
}});
/*! booking-widget-model.js */
swa.BookingWidgetModel=Backbone.Model.extend({defaults:{name:"BookingWidgetModel",currentTab:""}});
/*! booking-widget-view.js */
swa.BookingWidget=swa.View.extend({CURRENT_TAB:"currentTab",AIR:"Air",CAR:"Car",CHANGE_FLIGHT:"Change Flight",CHECK_IN:"Check In",FLIGHT_STATUS:"Flight Status",HOTEL:"Hotel",VACATIONS:"Vacations",$airBookingMenu:null,$hotelBookingMenu:null,$carBookingMenu:null,$vacationsBookingMenu:null,$checkInMenu:null,$changeFlightMenu:null,$flightStatusMenu:null,$allBookingForm:null,$airBookingForm:null,$hotelBookingForm:null,$carBookingForm:null,$vacationsBookingForm:null,$bookingFormActive:null,$checkInForm:null,$changeFlightForm:null,$flightStatusForm:null,formSettings:null,$formsWrapper:null,$formView:null,lastView:null,model:null,initialize:function(){var a;
this.childViews=[];
this.$airBookingMenu=this.$(".js-booking-menu-air");
this.$hotelBookingMenu=this.$(".js-booking-menu-hotel");
this.$carBookingMenu=this.$(".js-booking-menu-car");
this.$vacationsBookingMenu=this.$(".js-booking-menu-vacations");
this.$checkInMenu=this.$(".js-booking-menu-check-in");
this.$flightStatusMenu=this.$(".js-booking-menu-flight-status");
this.$changeFlightMenu=this.$(".js-booking-menu-change-flight");
this.$allBookingForm=this.$(".js-booking-widget-views form");
this.$airBookingForm=this.$(".js-booking-panel-air");
this.$hotelBookingForm=this.$(".js-booking-panel-hotel");
this.$carBookingForm=this.$(".js-booking-panel-car");
this.$vacationsBookingForm=this.$(".js-booking-panel-vacations");
this.$checkInForm=this.$(".js-booking-panel-check-in");
this.$flightStatusForm=this.$(".js-booking-panel-flight-status");
this.$changeFlightForm=this.$(".js-booking-panel-change-flight");
this.$formsWrapper=this.$(".js-booking-forms-wrapper");
this.$formView=this.$(".js-booking-widget-views");
this.formSettings={AIR:{formName:this.AIR,activeForm:this.$airBookingForm,formView:"AirBookingForm"},HOTEL:{formName:this.HOTEL,activeForm:this.$hotelBookingForm,formView:"HotelBookingForm"},CAR:{formName:this.CAR,activeForm:this.$carBookingForm,formView:"CarBookingForm"},VACATIONS:{formName:this.VACATIONS,activeForm:this.$vacationsBookingForm,formView:"VacationsBookingForm"},CHECK_IN:{formName:this.CHECK_IN,activeForm:this.$checkInForm,formView:"CheckInForm"},FLIGHT_STATUS:{formName:this.FLIGHT_STATUS,activeForm:this.$flightStatusForm,formView:"FlightStatusForm"},CHANGE_FLIGHT:{formName:this.CHANGE_FLIGHT,activeForm:this.$changeFlightForm,formView:"ChangeFlightForm"}};
this.panelView=new swa.PanelMenu({el:this.$(".js-form-options-menu"),$selectionPointer:this.$(".js-form-menu-pointer"),pointerSide:swa.OVERLAY_POSITION_BELOW,pointerYOffset:-7,panels:{"js-booking-panel-air":_.bind(this.updateAirForm,this),"js-booking-panel-hotel":_.bind(this.updateHotelForm,this),"js-booking-panel-car":_.bind(this.updateCarForm,this),"js-booking-panel-vacations":_.bind(this.updateVacationsForm,this),"js-booking-panel-check-in":_.bind(this.updateCheckInForm,this),"js-booking-panel-flight-status":_.bind(this.updateFlightStatusForm,this),"js-booking-panel-change-flight":_.bind(this.updateChangeFlightForm,this)}});
this.registerChildView(this.panelView);
a=swa.page.getPageModelData("BookingWidgetModel");
this.model=new swa.BookingWidgetModel(a);
swa.page.registerPageData(this.model);
switch(this.model.get(this.CURRENT_TAB)){case this.HOTEL:this.panelView.selectTab(this.$hotelBookingMenu,false);
this.renderForm(this.formSettings.HOTEL);
break;
case this.CAR:this.panelView.selectTab(this.$carBookingMenu,false);
this.renderForm(this.formSettings.CAR);
break;
case this.VACATIONS:this.panelView.selectTab(this.$vacationsBookingMenu,false);
this.renderForm(this.formSettings.VACATIONS);
break;
case this.CHECK_IN:this.panelView.selectTab(this.$checkInMenu,false);
this.renderForm(this.formSettings.CHECK_IN);
break;
case this.FLIGHT_STATUS:this.panelView.selectTab(this.$flightStatusMenu,false);
this.renderForm(this.formSettings.FLIGHT_STATUS);
break;
case this.CHANGE_FLIGHT:this.panelView.selectTab(this.$changeFlightMenu,false);
this.renderForm(this.formSettings.CHANGE_FLIGHT);
break;
default:this.panelView.selectTab(this.$airBookingMenu,false);
this.renderForm(this.formSettings.AIR);
break
}this.name="BookingWidget"
},closeLastView:function(){if(this.lastView){this.lastView.close(false);
this.lastView=null
}},swapForms:function(g){var c={};
var d;
var b;
var e=swa.position.getElementHeight(g);
var h=g.index();
var a=this.$bookingFormActive;
var f=this.$formsWrapper;
var i=this.$formView;
if(g!==a){this.$allBookingForm.hide();
g.show();
if(a){d=a.index();
b=swa.position.getElementHeight(a);
a.show();
swa.animationService.finishAnimationAndClearCallback("swap-forms");
i.css({marginTop:0});
f.css({height:b});
if(h>d){c={marginTop:-b}
}else{i.css({marginTop:-e});
c={marginTop:0}
}swa.animationService.to(i,1,{css:c,ease:"Quint.easeOut",name:"swap-forms",onComplete:function(){if(a){a.hide();
i.css({marginTop:0});
f.css({height:e})
}}});
swa.animationService.to(this.$formsWrapper,1,{css:{height:e},ease:"Quint.easeOut",name:"reset-form-height"})
}}},updateAirForm:function(){this.setAnalytics(this.$airBookingMenu);
this.renderForm(this.formSettings.AIR)
},updateHotelForm:function(){this.setAnalytics(this.$hotelBookingMenu);
this.renderForm(this.formSettings.HOTEL)
},updateCarForm:function(){this.setAnalytics(this.$carBookingMenu);
this.renderForm(this.formSettings.CAR)
},updateVacationsForm:function(){this.setAnalytics(this.$vacationsBookingMenu);
this.renderForm(this.formSettings.VACATIONS)
},updateCheckInForm:function(){this.setAnalytics(this.$checkInMenu);
this.renderForm(this.formSettings.CHECK_IN)
},updateFlightStatusForm:function(){this.setAnalytics(this.$flightStatusMenu);
this.renderForm(this.formSettings.FLIGHT_STATUS)
},updateChangeFlightForm:function(){this.setAnalytics(this.$changeFlightMenu);
this.renderForm(this.formSettings.CHANGE_FLIGHT)
},renderForm:function(a){this.model.set(this.CURRENT_TAB,a.formName);
this.swapForms(a.activeForm);
this.closeLastView();
this.$bookingFormActive=a.activeForm;
this.lastView=new swa[a.formView]({el:a.activeForm})
},setAnalytics:function(b){var a=$(this),c={environment:"swadev",linkTrackVar:"prop65",linkTrackVarValue:"HP:SWA:",panelName:"",$target:null};
c.name=b.data("panel");
c.panelName=b.attr("aria-controls");
c.$target=a;
swa.setOmnitureValues(c)
}});
/*! date-time-functions.js */
swa.DateTimeFunctions={convertToMoment:function(a){return moment(a,["M/D","M/D/YYYY","YYYY/M/D","YYYY/MMM/D","D/MMM","D/MMM/YYYY","MMM/D","MMM/D/YYYY"],true)
},isBetweenTwoDates:function(b,c,a){return((typeof b!=="undefined")&&(typeof c!=="undefined")&&(typeof a!=="undefined")&&(b.diff(c,"days")>=0)&&(b.diff(a,"days")<=0))
},isValidMoment:function(a){return a&&a.isValid()&&a.year()
},createMoment:function(b){b=this.cleanString(b);
var a=this.convertToMoment(b);
if(this.isValidMoment(a)){var c=this.inferYearFromDate(a,this.isYearProvided(b));
return this.convertToMoment((a.month()+1)+"/"+a.date()+"/"+c)
}else{if(!this.isYearProvided(b)){return this.convertToMoment(b+"/"+(this.getCurrentYear()+1))
}}return this.invalidMoment()
},parseDateString:function(a){return a.split("/")
},cleanString:function(a){var b=a.replace(/^[\/ -\.\\,]+|[\/ -\.\\,]+$/g,"");
return b.replace(/[\/ -\.\\,]+/g,"/")
},isYearProvided:function(b){var a=this.parseDateString(b);
return((a.length>2)&&($.isNumeric(a[2])))
},invalidMoment:function(){return moment("")
},createToday:function(){return moment().startOf("day")
},getCurrentYear:function(){return new Date().getFullYear()
},inferYearFromDate:function(b,a){if(!a){var c=this.getCurrentYear();
b.year(c);
return(b.startOf("day").isBefore(this.createToday()))?c+1:c
}else{return b.year()
}}};
/*! calendar-styler.js */
swa.CalendarStyler=function(){var b=[],o,q,p;
this.setSelectedDates=function(s){b=s
};
this.setToday=function(s){p=s.startOf("day")
};
this.setAvailableStartDate=function(s){o=s.startOf("day")
};
this.setAvailableEndDate=function(s){q=s.startOf("day")
};
this.getCellClass=function(s){var t=[];
if(s){if(a(s)){d(s,t);
r(t)
}else{j(t)
}e(s,t)
}else{h(t)
}return t.join(" ")
};
var a=function(s){return swa.DateTimeFunctions.isBetweenTwoDates(s,o,q)
};
var d=function(s,t){if(l(s)){t.push("range-start-end");
t.push("selected-first-second")
}else{if(g(s)){t.push("range-start-end");
t.push("selected-first")
}else{if(m(s)){t.push("range-start-end");
t.push("selected-second")
}else{if((c(s))&&(a(b[0]))&&(a(b[1]))){t.push("range");
i(s,t)
}else{if(k(s)){t.push("range-start");
i(s,t)
}else{if(f(s)){t.push("range-end");
i(s,t)
}}}}}}};
var l=function(s){return n(s,b[0])&&n(s,b[1])
};
var g=function(s){return n(s,b[0])&&((b.length===1)||(!b[1])||(!b[1].isValid())||(!a(b[1]))||(b[1].isBefore(b[0])))
};
var m=function(s){return n(s,b[1])&&((!b[0])||(!b[0].isValid())||(!a(b[0]))||(b[1].isBefore(b[0])))
};
var c=function(s){return(s.isAfter(b[0]))&&(s.isBefore(b[1]))
};
var k=function(s){return n(s,b[0])
};
var f=function(s){return n(s,b[1])
};
var n=function(t,s){return t.isSame(s)
};
var i=function(s,t){var u;
if(s.date()===1||s.day()===0){t.push("range-first-in-row")
}u=s.clone().endOf("month").startOf("day");
if(s.day()===6||n(s,u)){t.push("range-last-in-row")
}};
var r=function(s){s.push("available")
};
var j=function(s){s.push("unavailable")
};
var e=function(s,t){if(n(s,p)){t.push("today")
}};
var h=function(s){s.push("empty")
}
};
/*! calendar-cell-model.js */
swa.CalendarCellModel=Backbone.Model.extend({defaults:{date:null},getDayOfMonth:function(){if(this.get("date")){return this.get("date").format("D")
}else{return""
}}});
/*! calendar-cell-view.js */
swa.CalendarCell=swa.View.extend({tagName:"td",initialize:function(a){this.name="CalendarCell";
this.childViews=[];
this.calendarStyler=a.calendarStyler;
this.calendarEventBus=a.calendarEventBus;
this.template=_.template($("#js-calendar-cell").html());
_.bindAll(this,"render");
this.listenTo(this.calendarEventBus,"selectedChanged",this.render);
this.render()
},events:{click:"selected"},selected:function(){if(!this.$el.hasClass("unavailable")){this.calendarEventBus.trigger("dateSelected",{date:this.model.get("date"),event:"click",target:this.$el})
}},render:function(){var a=this.template({dayOfMonth:this.model.getDayOfMonth(),date:this.model.get("date"),calendarStyler:this.calendarStyler}),b=$(a);
this.$el.replaceWith(b);
this.setElement(b);
return this.$el
}});
/*! calendar-month-model.js */
swa.CalendarMonthModel=Backbone.Model.extend({initialize:function(b,c){var a=moment([b,c-1,1]);
this.set("firstDayOfMonth",a);
this.set("abbreviatedMonthName",a.format("MMM"));
this.set("monthName",a.format("MMMM"));
this.set("month",c);
this.set("year",b);
this.createCalendarCells()
},createCalendarCells:function(){var d=this.get("firstDayOfMonth"),f=d.day(),a=d.daysInMonth(),g=d,c=new Backbone.Collection();
for(var e=0;
e<f;
e+=1){c.add(new swa.CalendarCellModel())
}for(var b=0;
b<a;
b+=1){c.add(new swa.CalendarCellModel({date:g.clone()}));
g.add("days",1)
}this.set("calendarCells",c)
}});
/*! calendar-month-view.js */
swa.CalendarMonth=swa.View.extend({calendarStyler:null,calendarEventBus:null,DAYS_IN_WEEK:7,$calendarRow:"",initialize:function(a){this.name="CalendarMonth";
this.childViews=[];
this.calendarStyler=a.calendarStyler;
this.calendarEventBus=a.calendarEventBus;
this.template=_.template($("#js-calendar-month").html());
this.cellRowTemplate=_.template($("#js-calendar-cell-row").html());
this.weekdayCounter=0;
_.bindAll(this,"renderCell","render");
this.render()
},render:function(){this.$el.html(this.template(this.model.toJSON()));
_.each(this.model.get("calendarCells").models,this.renderCell);
if(this.weekdayCounter<this.DAYS_IN_WEEK){this.$el.find(".calendar-rows").append(this.$calendarRow)
}},renderCell:function(b){if(this.weekdayCounter===0){this.$calendarRow=$(this.cellRowTemplate())
}var a=new swa.CalendarCell({model:b,calendarStyler:this.calendarStyler,calendarEventBus:this.calendarEventBus});
this.registerChildView(a);
this.$calendarRow.append($(a.render()));
this.weekdayCounter+=1;
if(this.weekdayCounter===this.DAYS_IN_WEEK){this.weekdayCounter=0;
this.$(".calendar-rows").append(this.$calendarRow)
}}});
/*! calendar-model.js */
swa.CalendarModel=Backbone.Model.extend({defaults:{year:2000,month:1,numberOfSelectableDates:1,selectingIndex:0,selectedDates:[],titles:["select date"],availableStartDate:moment(),availableEndDate:moment().add("months",3)}});
/*! calendar-view.js */
swa.Calendar=swa.View.extend({TOOLTIP_CLOSE_DELAY:3000,TOOLTIP_ZINDEX:1002,clickCount:0,initialize:function(a){this.name="Calendar";
this.childViews=[];
_.bindAll(this,"render");
this.calendarEventBus=_.extend({},Backbone.Events);
this.clearInvalidSelectedDates();
this.calendarStyler=a.calendarStyler?a.calendarStyler:new swa.CalendarStyler();
this.calendarStyler.setAvailableStartDate(this.model.get("availableStartDate"));
this.calendarStyler.setAvailableEndDate(this.model.get("availableEndDate"));
this.calendarStyler.setToday(moment());
this.calendarStyler.setSelectedDates(this.model.get("selectedDates"));
this.template=_.template($("#js-calendar").html());
this.tooltipTemplate=_.template($("#js-calendar-tooltip").html());
this.listenTo(this.model,"change",this.render);
this.listenTo(this.calendarEventBus,"dateSelected",this.dateSelected)
},events:{"https://www.southwest.com/assets/v15040118/scripts/click .next":"increment","click .previous":"decrement"},clearInvalidSelectedDates:function(){var b=this.model.get("selectedDates"),a;
for(a=0;
a<b.length;
a+=1){if((b[a])&&(!b[a].isValid())){b[a]=swa.DateTimeFunctions.invalidMoment()
}}return b
},isFirstSelectedDateValid:function(a){return a[0].isValid()
},isSecondSelectedDateValid:function(a){return a[1].isValid()
},areSelectionsValid:function(a,b){return((a===1)||(b.length===2&&(this.isFirstSelectedDateValid(b))&&(this.isSecondSelectedDateValid(b))&&!b[0].isAfter(b[1])))
},closeTooltip:function(){if(this.tip){this.removeChildView(this.tip);
this.tip.close();
this.tip=null
}},displayTooltip:function(b){this.tip=new swa.Overlay({content:this.tooltipTemplate({tooltip:this.model.get("tooltips")[this.model.get("selectingIndex")]}),target:b.target,zIndex:this.TOOLTIP_ZINDEX,offsetY:5,containerSide:swa.OVERLAY_POSITION_ABOVE,pointerClass:"calendar-selector--overlay--pointer",contentContainerClass:"calendar-selector--overlay--container"});
this.tip.render();
this.registerChildView(this.tip);
var a=this;
setTimeout(function(){a.closeTooltip()
},this.TOOLTIP_CLOSE_DELAY)
},isSecondSelectedDateBeforeFirstSelectedDate:function(a,b){return(a>0)&&(b[a].isBefore(b[a-1]))
},isFirstSelectedDateAfterSecondSelectedDate:function(a,c,b){return(a===0)&&(c>1)&&((this.clickCount===0)||(b[a].isAfter(b[a+1])))
},validateSelectedDates:function(){var c=this.model.get("selectedDates"),d=c.length,b=this.model.get("selectingIndex"),a=this.model.get("numberOfSelectableDates");
if(this.isSecondSelectedDateBeforeFirstSelectedDate(b,c)){c[b-1]=swa.DateTimeFunctions.invalidMoment();
this.calendarEventBus.trigger("dateCleared",b-1)
}if(this.isFirstSelectedDateAfterSecondSelectedDate(b,d,c)){c[b+1]=swa.DateTimeFunctions.invalidMoment();
this.calendarEventBus.trigger("dateCleared",b+1)
}this.model.set({selectedDates:c},{silent:true})
},dateSelected:function(e){var c=this.model.get("selectedDates"),d=c.length,b=this.model.get("selectingIndex"),a=this.model.get("numberOfSelectableDates"),f=(e.event==="click");
this.closeTooltip();
if((e.date)&&(swa.DateTimeFunctions.isBetweenTwoDates(e.date,this.model.get("availableStartDate"),this.model.get("availableEndDate")))){if(d>b){c[b]=e.date;
if((a===2)&&(e.event!=="keyed")){this.validateSelectedDates()
}}else{c.push(e.date);
this.model.set({selectedDates:c},{silent:true})
}if(f){if(!this.areSelectionsValid(a,c)){this.model.set({selectingIndex:(b+1)%a},{silent:true});
this.displayTooltip(e)
}this.clickCount+=1
}}else{c[b]=swa.DateTimeFunctions.invalidMoment()
}this.calendarStyler.setSelectedDates(this.model.get("selectedDates"));
this.calendarEventBus.trigger("selectedChanged",{changeTargets:f})
},renderCalendarMonths:function(){this.$el.html(this.template(this.model.toJSON()));
this.calendarOne=new swa.CalendarMonth({el:this.$(".calendar-1 .calendar-month"),model:new swa.CalendarMonthModel(this.model.get("year"),this.model.get("month")),calendarStyler:this.calendarStyler,calendarEventBus:this.calendarEventBus});
this.calendarTwo=new swa.CalendarMonth({el:this.$(".calendar-2 .calendar-month"),model:new swa.CalendarMonthModel(this.getNextYear(),this.getNextMonth()),calendarStyler:this.calendarStyler,calendarEventBus:this.calendarEventBus});
this.registerChildView(this.calendarOne);
this.registerChildView(this.calendarTwo)
},render:function(){this.removeExistingMonths();
this.renderCalendarMonths();
return this
},removeExistingMonths:function(){if(this.calendarOne){this.removeChildView(this.calendarOne);
this.calendarOne.close()
}if(this.calendarTwo){this.removeChildView(this.calendarTwo);
this.calendarTwo.close()
}},increment:function(){this.closeTooltip();
if(this.isNextMonthEnabled()){this.model.set({year:this.getNextYear(),month:this.getNextMonth()})
}},decrement:function(){this.closeTooltip();
if(this.isPreviousMonthEnabled()){this.model.set({year:this.getPreviousYear(),month:this.getPreviousMonth()})
}},getCurrentMoment:function(){return moment([this.model.get("year"),this.model.get("month")-1,1])
},getNextMonthMoment:function(){return this.getCurrentMoment().add("months",1)
},getNextMonth:function(){return this.getNextMonthMoment().month()+1
},getNextYear:function(){return this.getNextMonthMoment().year()
},getPreviousMonth:function(){return this.getCurrentMoment().subtract("months",1).month()+1
},getPreviousYear:function(){return this.getCurrentMoment().subtract("months",1).year()
},isPreviousMonthEnabled:function(){var a=this.model.get("availableStartDate");
return a.isBefore(this.getCurrentMoment())
},isNextMonthEnabled:function(){var a=this.model.get("availableEndDate");
return a.isAfter(this.getNextMonthMoment())
}});
/*! date-selector-view.js */
swa.DateSelector=swa.Overlay.extend({CLOSE_DELAY:500,initialize:function(a){this.name="DateSelector";
this.childViews=[];
this.options=a;
this.targets=a.targets;
this.dateChangedCallbacks=a.dateChangedCallbacks;
this.initializeTarget();
var b=$("#js-calendar-selector");
this.$el.html(_.template(b.html()));
this.initializeViewingMonth();
this.flightCalendar=new swa.Calendar({el:this.$(".calendar-selector"),model:this.model});
this.registerChildView(this.flightCalendar);
this.listenTo(this.flightCalendar.calendarEventBus,"selectedChanged",this.updateTargets);
this.listenTo(this.flightCalendar.calendarEventBus,"dateCleared",this.clearDate);
this.listenTo(this.targets[0],"keyedInput",this.departDateChanged);
if(this.model.get("numberOfSelectableDates")===2){this.listenTo(this.targets[1],"keyedInput",this.returnDateChanged)
}a.view=this.flightCalendar;
if(this.$el.attr("tabindex")===undefined){this.$el.attr("tabindex","99")
}swa.DateSelector.__super__.initialize.call(this,a)
},events:{click:"stopBubble",keydown:"keyDown"},keyDown:function(b){var a=b.which;
if(a===swa.KEY_ESCAPE){swa.page.closeOverlay()
}},initializeViewingMonth:function(){var d=this.model.get("selectedDates")[0],a=this.model.get("selectedDates")[1],c,b=moment();
if(this.model.get("selectingIndex")){if(e(c)){c=a.clone();
c=c.subtract("months",1);
if((e(d))&&(c.isBefore(d))){c=d
}}else{c=(e(d))?d:b
}}else{c=(d)?d:swa.DateTimeFunctions.invalidMoment();
if(!c.isValid()){c=(e(a))?a:b
}}if(c.isBefore(this.model.get("availableStartDate"))){c=this.model.get("availableStartDate")
}else{if(c.isAfter(this.model.get("availableEndDate"))){c=this.model.get("availableEndDate")
}}this.model.set({year:c.year(),month:c.month()+1});
function e(f){return((f)&&(f.isValid()))
}},render:function(){swa.DateSelector.__super__.render.call(this)
},departDateChanged:function(){var a=this.targets[0].getDate();
this.flightCalendar.calendarEventBus.trigger("dateSelected",{date:a,event:"keyed"})
},returnDateChanged:function(){var a=this.targets[1].getDate();
this.flightCalendar.calendarEventBus.trigger("dateSelected",{date:a,event:"keyed"})
},initializeTarget:function(){var a=this.targets[this.model.get("selectingIndex")];
if(a.options.dateSelectorOffsetX){this.options.contentOffsetX=a.options.dateSelectorOffsetX
}this.options.target=a.$el
},shouldUpdateCalendarView:function(){return((this.model.get("selectedDates")[1].month()!==(this.model.get("month")))||(this.model.get("selectedDates")[0].month()!==(this.model.get("month")-1)))
},updateTargets:function(d){if((d)&&(d.changeTargets)){var c=this.model.get("selectedDates");
var a=this.model.get("numberOfSelectableDates");
for(var b=0;
b<a;
b+=1){if(typeof this.dateChangedCallbacks!=="undefined"&&this.dateChangedCallbacks[b]){this.dateChangedCallbacks[b](c[b])
}if((c[b])&&(c[b].isValid())){this.targets[b].setDate(c[b])
}else{this.targets[b].clearDate()
}}if(this.flightCalendar.areSelectionsValid(a,c)){setTimeout(function(){swa.page.closeOverlay()
},this.CLOSE_DELAY)
}else{this.initializeTarget();
this.options.shouldFindBestPosition=false;
this.render()
}}else{if(this.shouldUpdateCalendarView){this.initializeViewingMonth()
}}},clearDate:function(a){this.targets[a].clearDate()
}});
/*! date-input-view.js */
swa.DateInput=swa.View.extend({FORM_DATE_FORMAT:"MM/DD/YYYY",DISPLAY_DATE_FORMAT:"MM/DD",initialize:function(a){this.name="DateInput";
this.name=a.name;
this.options=a;
this.hiddenField=a.hiddenField;
this.model=new swa.CalendarCellModel()
},events:{change:"change",keyup:"keyUp",keydown:"keyDown",blur:"formatInput",focus:"focus"},focus:function(){this.$el.select().one("mouseup",function(a){a.preventDefault()
})
},change:function(){this.trigger("change")
},keyUp:function(b){var a=b.which;
if((a!==swa.KEY_TAB)&&(a!==swa.KEY_SHIFT)&&(a!==swa.KEY_LEFT)&&(a!==swa.KEY_RIGHT)){this.notifyChange({updateDisplay:false})
}},keyDown:function(b){var a=b.which;
if(a===swa.KEY_TAB){this.stopBubble(b);
swa.page.closeOverlay();
if(b.shiftKey){if(this.options.prevFocusCallback){this.options.prevFocusCallback(b)
}}else{if(this.options.nextFocusCallback){this.options.nextFocusCallback(b)
}}}else{if(a===swa.KEY_ESCAPE){swa.page.closeOverlay()
}}},setDate:function(a){if(a.isValid()){a.startOf("day");
this.model.set("date",a);
this.$el.val(a.format(this.DISPLAY_DATE_FORMAT));
if(this.hiddenField){var b=a.format(this.FORM_DATE_FORMAT);
this.hiddenField.val(b)
}this.trigger("change")
}},getDate:function(){if(this.model.get("date")){return this.model.get("date")
}},formatInput:function(){var a=this.model.get("date");
if(a&&a.isValid()){this.$el.val(this.model.get("date").format(this.DISPLAY_DATE_FORMAT))
}},notifyChange:function(b){var a=swa.DateTimeFunctions.createMoment(this.$el.val());
if((swa.DateTimeFunctions.isValidMoment(a))){this.model.set("date",a);
if(b.updateDisplay){this.$el.val(a.format(this.DISPLAY_DATE_FORMAT))
}if(this.hiddenField){var c=a.format(this.FORM_DATE_FORMAT);
this.hiddenField.val(c)
}this.trigger("change")
}else{this.clearHidden()
}this.trigger("keyedInput")
},clearDate:function(){this.$el.val("");
this.clearHidden()
},clearHidden:function(){if(this.hiddenField){this.hiddenField.val("")
}this.model.set("date",null);
this.trigger("change")
},val:function(){var a=this.model.get("date");
return(a)?this.model.get("date").toDate():""
}});
/*! hero-model.js */
swa.HeroModel=Backbone.Model.extend({DEFAULT_KEY:"DEFAULT",ERROR_KEY:"error",isHeroDisplayed:false,defaults:{heroBackground:"",heroForeground:"",link:"",target:"",alt:"",mboxName:""},initialize:function(){var a;
this.isHeroDisplayed=false;
if(swa.hero){a=this.findHero([swa.hero.geolocationStation,this.DEFAULT_KEY]);
if(a){if((a.enable_segmentation)&&(a.mbox_name)){this.setMboxHero(a)
}else{this.setHero(a)
}}}},setHero:function(a){if((!this.isHeroDisplayed)&&(a)){this.isHeroDisplayed=true;
this.set({heroBackground:a.hero_bgnd,heroForeground:a.hero_fgnd,link:a.link,target:a.target,alt:a.alt,enableSegmentation:a.enable_segmentation,mboxName:a.mbox_name})
}},findHero:function(a){var c,d=a.length;
for(var b=0;
(b<d)&&(!c);
b+=1){c=swa.hero.targets[a[b]]
}return c
},setHeroCallback:function(b){var a=this.findHero([b,this.ERROR_KEY,swa.hero.geolocationStation,this.DEFAULT_KEY]);
this.setHero(a)
},setMboxHero:function(a){mboxCreate(a.mbox_name);
setTimeout(function(){if(!swa.heroModel.isHeroDisplayed){swa.error("HeroModel: No response from test and target within "+swa.HERO_MBOX_TIMEOUT+"ms.")
}swa.heroModel.setHero(a)
},swa.HERO_MBOX_TIMEOUT)
}});
swa.heroModel=new swa.HeroModel();
/*! hero-promo-view.js */
swa.HeroPromo=swa.View.extend({initialize:function(){var a=$("#js-hero-promo");
if(!a.length){swa.error("HeroPromo: cannot locate template #js-hero-promo")
}this.template=_.template(a.html());
_.bindAll(this,"render");
this.render();
this.listenTo(swa.heroModel,"change",this.render)
},render:function(){this.$el.html(this.template({imageSource:swa.heroModel.get("heroForeground"),imageAlt:swa.heroModel.get("alt"),link:swa.heroModel.get("link"),target:swa.heroModel.get("target")}))
}});
swa.heroPromo=new swa.HeroPromo({el:$(".hero-overlay")});
/*! hero-background-view.js */
swa.HeroBackground=swa.View.extend({initialize:function(){var a=$("#js-hero-background");
if(!a.length){swa.error("HeroBackground: cannot locate template #js-hero-background")
}this.template=_.template(a.html());
_.bindAll(this,"render");
this.render();
this.listenTo(swa.heroModel,"change",this.render)
},render:function(){this.$el.html(this.template({imageSource:swa.heroModel.get("heroBackground")}))
}});
swa.heroBackground=new swa.HeroBackground({el:$(".page-background")});
/*! luvhomeDefs.js */
s.channel="HP";
s.prop50="HP:SWA";
s.pageName="HP:SWA:Southwest Homepage";