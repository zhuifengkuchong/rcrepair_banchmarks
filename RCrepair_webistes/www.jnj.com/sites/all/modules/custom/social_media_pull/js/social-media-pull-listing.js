(function($) {
  var loadedMobile = false;
  var loadedDesktop = false;

  $(function() {
    var container = $('.social-media-pull-posts');

    // First time load
    imagesLoaded( '.social-media-pull-posts', function() {
        checkDevice(container);
        $( window ).resize(function() {
          checkDevice(container);
        });
    });

    // Filter
    $('.social-media-pull-filter a').click(function() {
      $('.social-media-pull-filter a').removeClass('active');
      $(this).addClass('active');

      var socialMedia = $(this).attr('data-social-media');

      if (!loadedMobile) {
        container.masonry('destroy');
      }

      $('.item').hide().removeClass('item');
      $('.social-type-' + socialMedia).addClass('item').css('display', 'inline-block');

      loadMasonry(container);
    });

    // Load More
    $('.social-media-load-more').click(function(e) {
      loadMoreClick(container);
    });

  });

  function loadReadMore() {
    $.ajax({
      type: "GET",
      url: '/social-media-load-more/' + nextItem + '/' + numItensToLoad,
      success: function(result) {
        if (result != '') {

          var elems = [];
          var time = 100;
          var items = $(result);

          $(items).each(function(index) {

            setTimeout(function() {
              imagesLoaded(container, function() {
                container.append(items.get(index)).masonry('appended', items.get(index));
              });
            }, time);
            time += 100;

          });
         }
      },
      beforeSend: function() {
        var loader = '<div class="social-posts-loader"></div>';

        $('.social-media-load-more').hide();
        $('.social-media-pull-footer-content').append(loader);
      },
      complete: function() {
        $('.social-posts-loader').remove();
        $('.social-media-load-more').show();
      }
    });
  }

  function loadMasonry(container) {
    container.masonry({
      itemSelector: '.item',
      gutter: '.gutter-sizer'
    });
  }

  function checkDevice(container) {
    if (MQ.context == 'smartphone_landscape') {
      if (loadedMobile == false) {
        if ($('.social-media-pull-filter').length > 0) {
          loadMasoryWithoutFilter(container, 5);
        }
        else {
          if (loadedDesktop == false) {
            loadMasoryWithoutFilter(container, 6);
          }
          else {
            loadMasoryWithoutFilter(container, -1);
          }
        }
        loadedMobile = true;
        loadedDesktop = false;
      }
    }
    else {
      if (loadedDesktop == false) {
        if ($('.social-media-pull-filter').length > 0) {
          $('.social-media-pull-filter .twitter').click();
        }
        else {
          $('.social-box').addClass('item').css('display', 'inline-block');
          loadMasonry(container);
        }
        loadedMobile = false;
        loadedDesktop = true;
      }
    }
  }

  function loadMasoryWithoutFilter(container, maxNumber) {
    container.masonry('destroy');
    $('.item').hide().removeClass('item');

    $('.social-media-pull-posts .social-box').each(function(index) {
      if (maxNumber > 0 && (index >= maxNumber)) {
        return false;
      }
      $(this).addClass('item').show();
    });
  }

  function loadMoreClick(container) {
      var numItens = $('.social-media-pull-posts .social-box').length;
      var nextItem = numItens + 1;
      var numItensToLoad = 15;

      // Mobile
      if (MQ.context == 'smartphone_landscape') {
        var hiddenItens = $('.social-media-pull-posts .social-box:hidden').length;
        numItensToLoad = 6;

        if (hiddenItens >=  numItensToLoad) {
          $('.social-media-pull-posts .social-box:hidden').each(function(index) {
            if (index >= numItensToLoad) {
              return false;
            }
            $(this).addClass('item').show();
          });

          return false;
        }
        else if (hiddenItens > 0) {
          numItensToLoad = numItensToLoad - hiddenItens;
        }
      }

      $.ajax({
        type: "GET",
        url: '/social-media-load-more/' + nextItem + '/' + numItensToLoad,
        success: function(result) {
          if (result != '') {

            var elems = [];
            var time = 100;
            var items = $(result);

            if (MQ.context == 'smartphone_landscape') {
              $(container).append(result);
              $('.social-media-pull-posts .social-box').addClass('item').show();
            }
            else {
              $(items).each(function(index) {
                setTimeout(function() {
                  imagesLoaded(container, function() {
                    container.append(items.get(index)).masonry('appended', items.get(index));
                  });
                }, time);
                time += 100;

              });
            }
           }
        },
        beforeSend: function() {
          var loader = '<div class="social-posts-loader"></div>';

          $('.social-media-load-more').hide();
          $('.social-media-pull-footer-content').append(loader);
        },
        complete: function() {
          $('.social-posts-loader').remove();
          $('.social-media-load-more').show();
        }
      });
  }

})(jQuery);

