(function($){

  $(function(){

    /*fix for the media query context load in IE8 and IE7 wich doesnt load
    the context if you access a page by pressing enter on the address bar.*/
    if ($.browser.msie && MQ) {
      setTimeout(function(){
        MQ.listenForChange();
        //alert('Done.');
      }, 1000);
    }

    // fix for tablets with resolution width higher than 1024px.
    MQ.addQuery({
      context: [],
      call_in_each_context: true,
      callback: function() {
        var width = $(window).width();
        if (Modernizr.touch && width<=1280 && width>1024) {
          $('body').addClass('tablet-layout');
        } else {
          $('body').removeClass('tablet-layout');
        }
      }
    });
  });

})(jQuery);