(function($){
  $(function() {
    // Media Queries
    MQ.addQuery({
      context: ['smartphone', 'smartphone_landscape', 'tablet_portrait', 'tablet_landscape', 'standard'],
      call_in_each_context: false,
      callback: function() {
        hideToolboxMobile();

        //Once we're getting a css problem on IE7 we decided to also hide the li parent.
        if ($('.rsbtn_play').length > 0) {
          if ($('a.read-speaker').length > 0) {
            $('a.read-speaker').parents('li').addClass('mobile');
          }
        }
        else {
          //If we do not need to have the readspeaker on the page needs to remove the readspeaker also from menu.
          if ($('li a.read-speaker').length > 0) {
            $('li a.read-speaker').parent().remove();
          }
        }

      }
    });

    $('.open-tools a').click(function(event){
      event.stopPropagation();
      if ($(this).hasClass('open')) {
        hideToolboxMobile();
      }
      else {
        showToolboxMobile();
      }
    });

    $('*').click(function(){
      if ($(this).closest('.toolbox-mobile, .open-tools').length == 0) {
        if ($('.open-tools a').hasClass('open')) {
          hideToolboxMobile();
        }
      }
    });

    // Share
    $('.toolbox-mobile a.share').click(function(event){
      event.preventDefault();
      if (!$(this).hasClass('disabled')) {
        addthis_sendto();
      }
    });

    // Readspeaker
    $('.toolbox-block a.read-speaker').click(function(event){
      event.preventDefault();
      if (!$(this).hasClass('disabled')) {
        jQuery('.rsbtn_play').trigger('click');
      }
    });

    $('.toolbox-mobile').click(function(event){
      event.stopPropagation();
    });

    $('.toolbox-mobile li a').each(function(){
      if ($(this).hasClass('no-mobile')) {
        $(this).closest('li').addClass('no-mobile');
      }
      $(this).prepend('<span />');
    });

    if (Drupal.settings.textonly) {
      $('.toolbox-block .text-only').closest('li').hide();
    }
    else {
      $('.toolbox-block .graphic-version').closest('li').hide();
    }

    if ($('.toolbox-block-desktop').length == 0) {
      $('body').addClass('no-toolbox-desktop');
    }
  });

  function showToolboxMobile() {
    $('.toolbox-mobile').show();
    $('.open-tools a').addClass('open');
  }

  function hideToolboxMobile() {
    $('.toolbox-mobile').hide();
    $('.open-tools a').removeClass('open');
  }
})(jQuery);