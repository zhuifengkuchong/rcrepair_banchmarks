(function($) {

  $(function() {
    if (Drupal.settings.emailafriend.emailafriend_type != "0") {
      $('a.email').click(function(event) {
        if (!$(this).hasClass('disabled')) {
          var email_page = $(this).attr('href');
          event.preventDefault();
          // removed exception for mobile.
  //        if ($.inArray(MQ.context, ['smartphone_portrait', 'smartphone_landscape']) != -1) {
  //        return  openEmailAFriendForm(email_page, document.location.href, "redirect");
  //        }
            return openEmailAFriendForm(email_page, document.location.href, "new");
        }
      });
    }
  });

})(jQuery);