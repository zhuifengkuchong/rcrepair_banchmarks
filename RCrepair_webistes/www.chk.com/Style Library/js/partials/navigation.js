
	$('#mobile-toggle').on('click', function(){
		$('.search-bar').slideUp(400);
		$('.search-bar').css('z-index', 5);
		$('.primary > ul').css('z-index', 50);
		$('.primary > ul').slideToggle('slow');
		$('.primary > ul').children('ul').css('display', '');
		return false;
	});	
	
	// correct for operations menu behavior
	// since the navigation doesn't fire when operations subitems are selected, 
	// this script recollapses the submenus for both desktop and mobile devices
	$('.secondary a').on('click', function( event ){
		var target = $(event.target),
		    subList = target.parents("ul.secondary"),
		    topList = target.parents("ul").last();
		var mql = window.matchMedia('screen and (min-width: 768px)'),
      mediaChanged = function(mql) {
        var isDesktop = ( mql.matches );
        if ( isDesktop ) { 
      		subList.slideToggle('fast', function() {
      		  setTimeout(function(){ $(".primary ul.secondary").attr("style", ""); }, 500);
      		});
        } else {
          if ( !target.parent().hasClass("list-toggle") ) {
            topList.slideToggle('fast');
          }
        }
    };
    window.matchMedia('screen and (min-width: 768px)').addListener(mediaChanged);
    mediaChanged(mql);
	});	

	
	$('.list-toggle').on('click', function(e){
		e.stopPropagation();
		$(this).toggleClass('active');
		$(this).parent().toggleClass('active');
		$(this).prev().slideToggle('slow').attr('data-check', 'true');
		return false;
	});

	var primaryGlobalNavBuild = function(){

		var $primaryNav = $('.primary > ul'),
			$globalNav = $('.global > ul > li').clone();
			
		$globalNav.find('.list-toggle').on('click', function(){
			$(this).toggleClass('active');
			$(this).parent().toggleClass('active');
			$(this).prev().slideToggle('slow').attr('data-check', 'true');
			return false;
		});

		$globalNav.not('.spacer').appendTo($primaryNav);
	};

	var primaryGlobalNavDestroy = function(){
		$globalNav = $('.nav-global-start').nextAll();
		$globalNav.remove();
	};

	var primaryNavShow = function(){
		$(".primary *").removeAttr("style");
		$(".primary *").removeClass("active");
		$(".search-bar").removeAttr("style");
	};

	$('#search-toggle').on('click', function(){
		var width = $(window).width();
		if (width < 768){
			$('.primary > ul').css('z-index', 5);
			$('.primary > ul').slideUp('slow');
		}
		$('.search-bar').css('z-index', 50);
		$('.search-bar').slideToggle(400);
		return false;
	});

