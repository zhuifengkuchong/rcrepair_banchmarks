/*
 * jQuery JavaScript Library v1.8.2
 * http://jquery.com/
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 *
 * Copyright 2012 jQuery Foundation and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: Thu Sep 20 2012 21:13:05 GMT-0400 (Eastern Daylight Time)
 */
(function(window,undefined){var rootjQuery,readyList,document=window.document,location=window.location,navigator=window.navigator,_jQuery=window.jQuery,_$=window.$,core_push=Array.prototype.push,core_slice=Array.prototype.slice,core_indexOf=Array.prototype.indexOf,core_toString=Object.prototype.toString,core_hasOwn=Object.prototype.hasOwnProperty,core_trim=String.prototype.trim,jQuery=function(selector,context){return new jQuery.fn.init(selector,context,rootjQuery)
},core_pnum=/[\-+]?(?:\d*\.|)\d+(?:[eE][\-+]?\d+|)/.source,core_rnotwhite=/\S/,core_rspace=/\s+/,rtrim=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,rquickExpr=/^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/,rsingleTag=/^<(\w+)\s*\/?>(?:<\/\1>|)$/,rvalidchars=/^[\],:{}\s]*$/,rvalidbraces=/(?:^|:|,)(?:\s*\[)+/g,rvalidescape=/\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g,rvalidtokens=/"[^"\\\r\n]*"|true|false|null|-?(?:\d\d*\.|)\d+(?:[eE][\-+]?\d+|)/g,rmsPrefix=/^-ms-/,rdashAlpha=/-([\da-z])/gi,fcamelCase=function(all,letter){return(letter+"").toUpperCase()
},DOMContentLoaded=function(){if(document.addEventListener){document.removeEventListener("DOMContentLoaded",DOMContentLoaded,false);
jQuery.ready()
}else{if(document.readyState==="complete"){document.detachEvent("onreadystatechange",DOMContentLoaded);
jQuery.ready()
}}},class2type={};
jQuery.fn=jQuery.prototype={constructor:jQuery,init:function(selector,context,rootjQuery){var match,elem,ret,doc;
if(!selector){return this
}if(selector.nodeType){this.context=this[0]=selector;
this.length=1;
return this
}if(typeof selector==="string"){if(selector.charAt(0)==="<"&&selector.charAt(selector.length-1)===">"&&selector.length>=3){match=[null,selector,null]
}else{match=rquickExpr.exec(selector)
}if(match&&(match[1]||!context)){if(match[1]){context=context instanceof jQuery?context[0]:context;
doc=(context&&context.nodeType?context.ownerDocument||context:document);
selector=jQuery.parseHTML(match[1],doc,true);
if(rsingleTag.test(match[1])&&jQuery.isPlainObject(context)){this.attr.call(selector,context,true)
}return jQuery.merge(this,selector)
}else{elem=document.getElementById(match[2]);
if(elem&&elem.parentNode){if(elem.id!==match[2]){return rootjQuery.find(selector)
}this.length=1;
this[0]=elem
}this.context=document;
this.selector=selector;
return this
}}else{if(!context||context.jquery){return(context||rootjQuery).find(selector)
}else{return this.constructor(context).find(selector)
}}}else{if(jQuery.isFunction(selector)){return rootjQuery.ready(selector)
}}if(selector.selector!==undefined){this.selector=selector.selector;
this.context=selector.context
}return jQuery.makeArray(selector,this)
},selector:"",jquery:"1.8.2",length:0,size:function(){return this.length
},toArray:function(){return core_slice.call(this)
},get:function(num){return num==null?this.toArray():(num<0?this[this.length+num]:this[num])
},pushStack:function(elems,name,selector){var ret=jQuery.merge(this.constructor(),elems);
ret.prevObject=this;
ret.context=this.context;
if(name==="find"){ret.selector=this.selector+(this.selector?" ":"")+selector
}else{if(name){ret.selector=this.selector+"."+name+"("+selector+")"
}}return ret
},each:function(callback,args){return jQuery.each(this,callback,args)
},ready:function(fn){jQuery.ready.promise().done(fn);
return this
},eq:function(i){i=+i;
return i===-1?this.slice(i):this.slice(i,i+1)
},first:function(){return this.eq(0)
},last:function(){return this.eq(-1)
},slice:function(){return this.pushStack(core_slice.apply(this,arguments),"slice",core_slice.call(arguments).join(","))
},map:function(callback){return this.pushStack(jQuery.map(this,function(elem,i){return callback.call(elem,i,elem)
}))
},end:function(){return this.prevObject||this.constructor(null)
},push:core_push,sort:[].sort,splice:[].splice};
jQuery.fn.init.prototype=jQuery.fn;
jQuery.extend=jQuery.fn.extend=function(){var options,name,src,copy,copyIsArray,clone,target=arguments[0]||{},i=1,length=arguments.length,deep=false;
if(typeof target==="boolean"){deep=target;
target=arguments[1]||{};
i=2
}if(typeof target!=="object"&&!jQuery.isFunction(target)){target={}
}if(length===i){target=this;
--i
}for(;
i<length;
i++){if((options=arguments[i])!=null){for(name in options){src=target[name];
copy=options[name];
if(target===copy){continue
}if(deep&&copy&&(jQuery.isPlainObject(copy)||(copyIsArray=jQuery.isArray(copy)))){if(copyIsArray){copyIsArray=false;
clone=src&&jQuery.isArray(src)?src:[]
}else{clone=src&&jQuery.isPlainObject(src)?src:{}
}target[name]=jQuery.extend(deep,clone,copy)
}else{if(copy!==undefined){target[name]=copy
}}}}}return target
};
jQuery.extend({noConflict:function(deep){if(window.$===jQuery){window.$=_$
}if(deep&&window.jQuery===jQuery){window.jQuery=_jQuery
}return jQuery
},isReady:false,readyWait:1,holdReady:function(hold){if(hold){jQuery.readyWait++
}else{jQuery.ready(true)
}},ready:function(wait){if(wait===true?--jQuery.readyWait:jQuery.isReady){return 
}if(!document.body){return setTimeout(jQuery.ready,1)
}jQuery.isReady=true;
if(wait!==true&&--jQuery.readyWait>0){return 
}readyList.resolveWith(document,[jQuery]);
if(jQuery.fn.trigger){jQuery(document).trigger("ready").off("ready")
}},isFunction:function(obj){return jQuery.type(obj)==="function"
},isArray:Array.isArray||function(obj){return jQuery.type(obj)==="array"
},isWindow:function(obj){return obj!=null&&obj==obj.window
},isNumeric:function(obj){return !isNaN(parseFloat(obj))&&isFinite(obj)
},type:function(obj){return obj==null?String(obj):class2type[core_toString.call(obj)]||"object"
},isPlainObject:function(obj){if(!obj||jQuery.type(obj)!=="object"||obj.nodeType||jQuery.isWindow(obj)){return false
}try{if(obj.constructor&&!core_hasOwn.call(obj,"constructor")&&!core_hasOwn.call(obj.constructor.prototype,"isPrototypeOf")){return false
}}catch(e){return false
}var key;
for(key in obj){}return key===undefined||core_hasOwn.call(obj,key)
},isEmptyObject:function(obj){var name;
for(name in obj){return false
}return true
},error:function(msg){throw new Error(msg)
},parseHTML:function(data,context,scripts){var parsed;
if(!data||typeof data!=="string"){return null
}if(typeof context==="boolean"){scripts=context;
context=0
}context=context||document;
if((parsed=rsingleTag.exec(data))){return[context.createElement(parsed[1])]
}parsed=jQuery.buildFragment([data],context,scripts?null:[]);
return jQuery.merge([],(parsed.cacheable?jQuery.clone(parsed.fragment):parsed.fragment).childNodes)
},parseJSON:function(data){if(!data||typeof data!=="string"){return null
}data=jQuery.trim(data);
if(window.JSON&&window.JSON.parse){return window.JSON.parse(data)
}if(rvalidchars.test(data.replace(rvalidescape,"@").replace(rvalidtokens,"]").replace(rvalidbraces,""))){return(new Function("return "+data))()
}jQuery.error("Invalid JSON: "+data)
},parseXML:function(data){var xml,tmp;
if(!data||typeof data!=="string"){return null
}try{if(window.DOMParser){tmp=new DOMParser();
xml=tmp.parseFromString(data,"text/xml")
}else{xml=new ActiveXObject("Microsoft.XMLDOM");
xml.async="false";
xml.loadXML(data)
}}catch(e){xml=undefined
}if(!xml||!xml.documentElement||xml.getElementsByTagName("parsererror").length){jQuery.error("Invalid XML: "+data)
}return xml
},noop:function(){},globalEval:function(data){if(data&&core_rnotwhite.test(data)){(window.execScript||function(data){window["eval"].call(window,data)
})(data)
}},camelCase:function(string){return string.replace(rmsPrefix,"ms-").replace(rdashAlpha,fcamelCase)
},nodeName:function(elem,name){return elem.nodeName&&elem.nodeName.toLowerCase()===name.toLowerCase()
},each:function(obj,callback,args){var name,i=0,length=obj.length,isObj=length===undefined||jQuery.isFunction(obj);
if(args){if(isObj){for(name in obj){if(callback.apply(obj[name],args)===false){break
}}}else{for(;
i<length;
){if(callback.apply(obj[i++],args)===false){break
}}}}else{if(isObj){for(name in obj){if(callback.call(obj[name],name,obj[name])===false){break
}}}else{for(;
i<length;
){if(callback.call(obj[i],i,obj[i++])===false){break
}}}}return obj
},trim:core_trim&&!core_trim.call("\uFEFF\xA0")?function(text){return text==null?"":core_trim.call(text)
}:function(text){return text==null?"":(text+"").replace(rtrim,"")
},makeArray:function(arr,results){var type,ret=results||[];
if(arr!=null){type=jQuery.type(arr);
if(arr.length==null||type==="string"||type==="function"||type==="regexp"||jQuery.isWindow(arr)){core_push.call(ret,arr)
}else{jQuery.merge(ret,arr)
}}return ret
},inArray:function(elem,arr,i){var len;
if(arr){if(core_indexOf){return core_indexOf.call(arr,elem,i)
}len=arr.length;
i=i?i<0?Math.max(0,len+i):i:0;
for(;
i<len;
i++){if(i in arr&&arr[i]===elem){return i
}}}return -1
},merge:function(first,second){var l=second.length,i=first.length,j=0;
if(typeof l==="number"){for(;
j<l;
j++){first[i++]=second[j]
}}else{while(second[j]!==undefined){first[i++]=second[j++]
}}first.length=i;
return first
},grep:function(elems,callback,inv){var retVal,ret=[],i=0,length=elems.length;
inv=!!inv;
for(;
i<length;
i++){retVal=!!callback(elems[i],i);
if(inv!==retVal){ret.push(elems[i])
}}return ret
},map:function(elems,callback,arg){var value,key,ret=[],i=0,length=elems.length,isArray=elems instanceof jQuery||length!==undefined&&typeof length==="number"&&((length>0&&elems[0]&&elems[length-1])||length===0||jQuery.isArray(elems));
if(isArray){for(;
i<length;
i++){value=callback(elems[i],i,arg);
if(value!=null){ret[ret.length]=value
}}}else{for(key in elems){value=callback(elems[key],key,arg);
if(value!=null){ret[ret.length]=value
}}}return ret.concat.apply([],ret)
},guid:1,proxy:function(fn,context){var tmp,args,proxy;
if(typeof context==="string"){tmp=fn[context];
context=fn;
fn=tmp
}if(!jQuery.isFunction(fn)){return undefined
}args=core_slice.call(arguments,2);
proxy=function(){return fn.apply(context,args.concat(core_slice.call(arguments)))
};
proxy.guid=fn.guid=fn.guid||jQuery.guid++;
return proxy
},access:function(elems,fn,key,value,chainable,emptyGet,pass){var exec,bulk=key==null,i=0,length=elems.length;
if(key&&typeof key==="object"){for(i in key){jQuery.access(elems,fn,i,key[i],1,emptyGet,value)
}chainable=1
}else{if(value!==undefined){exec=pass===undefined&&jQuery.isFunction(value);
if(bulk){if(exec){exec=fn;
fn=function(elem,key,value){return exec.call(jQuery(elem),value)
}
}else{fn.call(elems,value);
fn=null
}}if(fn){for(;
i<length;
i++){fn(elems[i],key,exec?value.call(elems[i],i,fn(elems[i],key)):value,pass)
}}chainable=1
}}return chainable?elems:bulk?fn.call(elems):length?fn(elems[0],key):emptyGet
},now:function(){return(new Date()).getTime()
}});
jQuery.ready.promise=function(obj){if(!readyList){readyList=jQuery.Deferred();
if(document.readyState==="complete"){setTimeout(jQuery.ready,1)
}else{if(document.addEventListener){document.addEventListener("DOMContentLoaded",DOMContentLoaded,false);
window.addEventListener("load",jQuery.ready,false)
}else{document.attachEvent("onreadystatechange",DOMContentLoaded);
window.attachEvent("onload",jQuery.ready);
var top=false;
try{top=window.frameElement==null&&document.documentElement
}catch(e){}if(top&&top.doScroll){(function doScrollCheck(){if(!jQuery.isReady){try{top.doScroll("left")
}catch(e){return setTimeout(doScrollCheck,50)
}jQuery.ready()
}})()
}}}}return readyList.promise(obj)
};
jQuery.each("Boolean Number String Function Array Date RegExp Object".split(" "),function(i,name){class2type["[object "+name+"]"]=name.toLowerCase()
});
rootjQuery=jQuery(document);
var optionsCache={};
function createOptions(options){var object=optionsCache[options]={};
jQuery.each(options.split(core_rspace),function(_,flag){object[flag]=true
});
return object
}jQuery.Callbacks=function(options){options=typeof options==="string"?(optionsCache[options]||createOptions(options)):jQuery.extend({},options);
var memory,fired,firing,firingStart,firingLength,firingIndex,list=[],stack=!options.once&&[],fire=function(data){memory=options.memory&&data;
fired=true;
firingIndex=firingStart||0;
firingStart=0;
firingLength=list.length;
firing=true;
for(;
list&&firingIndex<firingLength;
firingIndex++){if(list[firingIndex].apply(data[0],data[1])===false&&options.stopOnFalse){memory=false;
break
}}firing=false;
if(list){if(stack){if(stack.length){fire(stack.shift())
}}else{if(memory){list=[]
}else{self.disable()
}}}},self={add:function(){if(list){var start=list.length;
(function add(args){jQuery.each(args,function(_,arg){var type=jQuery.type(arg);
if(type==="function"&&(!options.unique||!self.has(arg))){list.push(arg)
}else{if(arg&&arg.length&&type!=="string"){add(arg)
}}})
})(arguments);
if(firing){firingLength=list.length
}else{if(memory){firingStart=start;
fire(memory)
}}}return this
},remove:function(){if(list){jQuery.each(arguments,function(_,arg){var index;
while((index=jQuery.inArray(arg,list,index))>-1){list.splice(index,1);
if(firing){if(index<=firingLength){firingLength--
}if(index<=firingIndex){firingIndex--
}}}})
}return this
},has:function(fn){return jQuery.inArray(fn,list)>-1
},empty:function(){list=[];
return this
},disable:function(){list=stack=memory=undefined;
return this
},disabled:function(){return !list
},lock:function(){stack=undefined;
if(!memory){self.disable()
}return this
},locked:function(){return !stack
},fireWith:function(context,args){args=args||[];
args=[context,args.slice?args.slice():args];
if(list&&(!fired||stack)){if(firing){stack.push(args)
}else{fire(args)
}}return this
},fire:function(){self.fireWith(this,arguments);
return this
},fired:function(){return !!fired
}};
return self
};
jQuery.extend({Deferred:function(func){var tuples=[["resolve","done",jQuery.Callbacks("once memory"),"resolved"],["reject","fail",jQuery.Callbacks("once memory"),"rejected"],["notify","progress",jQuery.Callbacks("memory")]],state="pending",promise={state:function(){return state
},always:function(){deferred.done(arguments).fail(arguments);
return this
},then:function(){var fns=arguments;
return jQuery.Deferred(function(newDefer){jQuery.each(tuples,function(i,tuple){var action=tuple[0],fn=fns[i];
deferred[tuple[1]](jQuery.isFunction(fn)?function(){var returned=fn.apply(this,arguments);
if(returned&&jQuery.isFunction(returned.promise)){returned.promise().done(newDefer.resolve).fail(newDefer.reject).progress(newDefer.notify)
}else{newDefer[action+"With"](this===deferred?newDefer:this,[returned])
}}:newDefer[action])
});
fns=null
}).promise()
},promise:function(obj){return obj!=null?jQuery.extend(obj,promise):promise
}},deferred={};
promise.pipe=promise.then;
jQuery.each(tuples,function(i,tuple){var list=tuple[2],stateString=tuple[3];
promise[tuple[1]]=list.add;
if(stateString){list.add(function(){state=stateString
},tuples[i^1][2].disable,tuples[2][2].lock)
}deferred[tuple[0]]=list.fire;
deferred[tuple[0]+"With"]=list.fireWith
});
promise.promise(deferred);
if(func){func.call(deferred,deferred)
}return deferred
},when:function(subordinate){var i=0,resolveValues=core_slice.call(arguments),length=resolveValues.length,remaining=length!==1||(subordinate&&jQuery.isFunction(subordinate.promise))?length:0,deferred=remaining===1?subordinate:jQuery.Deferred(),updateFunc=function(i,contexts,values){return function(value){contexts[i]=this;
values[i]=arguments.length>1?core_slice.call(arguments):value;
if(values===progressValues){deferred.notifyWith(contexts,values)
}else{if(!(--remaining)){deferred.resolveWith(contexts,values)
}}}
},progressValues,progressContexts,resolveContexts;
if(length>1){progressValues=new Array(length);
progressContexts=new Array(length);
resolveContexts=new Array(length);
for(;
i<length;
i++){if(resolveValues[i]&&jQuery.isFunction(resolveValues[i].promise)){resolveValues[i].promise().done(updateFunc(i,resolveContexts,resolveValues)).fail(deferred.reject).progress(updateFunc(i,progressContexts,progressValues))
}else{--remaining
}}}if(!remaining){deferred.resolveWith(resolveContexts,resolveValues)
}return deferred.promise()
}});
jQuery.support=(function(){var support,all,a,select,opt,input,fragment,eventName,i,isSupported,clickFn,div=document.createElement("div");
div.setAttribute("className","t");
div.innerHTML="  <link/><table></table><a href='http://www.usfoods.com/a'>a</a><input type='checkbox'/>";
all=div.getElementsByTagName("*");
a=div.getElementsByTagName("a")[0];
a.style.cssText="top:1px;float:left;opacity:.5";
if(!all||!all.length){return{}
}select=document.createElement("select");
opt=select.appendChild(document.createElement("option"));
input=div.getElementsByTagName("input")[0];
support={leadingWhitespace:(div.firstChild.nodeType===3),tbody:!div.getElementsByTagName("tbody").length,htmlSerialize:!!div.getElementsByTagName("link").length,style:/top/.test(a.getAttribute("style")),hrefNormalized:(a.getAttribute("href")==="/a"),opacity:/^0.5/.test(a.style.opacity),cssFloat:!!a.style.cssFloat,checkOn:(input.value==="on"),optSelected:opt.selected,getSetAttribute:div.className!=="t",enctype:!!document.createElement("form").enctype,html5Clone:document.createElement("nav").cloneNode(true).outerHTML!=="<:nav></:nav>",boxModel:(document.compatMode==="CSS1Compat"),submitBubbles:true,changeBubbles:true,focusinBubbles:false,deleteExpando:true,noCloneEvent:true,inlineBlockNeedsLayout:false,shrinkWrapBlocks:false,reliableMarginRight:true,boxSizingReliable:true,pixelPosition:false};
input.checked=true;
support.noCloneChecked=input.cloneNode(true).checked;
select.disabled=true;
support.optDisabled=!opt.disabled;
try{delete div.test
}catch(e){support.deleteExpando=false
}if(!div.addEventListener&&div.attachEvent&&div.fireEvent){div.attachEvent("onclick",clickFn=function(){support.noCloneEvent=false
});
div.cloneNode(true).fireEvent("onclick");
div.detachEvent("onclick",clickFn)
}input=document.createElement("input");
input.value="t";
input.setAttribute("type","radio");
support.radioValue=input.value==="t";
input.setAttribute("checked","checked");
input.setAttribute("name","t");
div.appendChild(input);
fragment=document.createDocumentFragment();
fragment.appendChild(div.lastChild);
support.checkClone=fragment.cloneNode(true).cloneNode(true).lastChild.checked;
support.appendChecked=input.checked;
fragment.removeChild(input);
fragment.appendChild(div);
if(div.attachEvent){for(i in {submit:true,change:true,focusin:true}){eventName="on"+i;
isSupported=(eventName in div);
if(!isSupported){div.setAttribute(eventName,"return;");
isSupported=(typeof div[eventName]==="function")
}support[i+"Bubbles"]=isSupported
}}jQuery(function(){var container,div,tds,marginDiv,divReset="padding:0;margin:0;border:0;display:block;overflow:hidden;",body=document.getElementsByTagName("body")[0];
if(!body){return 
}container=document.createElement("div");
container.style.cssText="visibility:hidden;border:0;width:0;height:0;position:static;top:0;margin-top:1px";
body.insertBefore(container,body.firstChild);
div=document.createElement("div");
container.appendChild(div);
div.innerHTML="<table><tr><td></td><td>t</td></tr></table>";
tds=div.getElementsByTagName("td");
tds[0].style.cssText="padding:0;margin:0;border:0;display:none";
isSupported=(tds[0].offsetHeight===0);
tds[0].style.display="";
tds[1].style.display="none";
support.reliableHiddenOffsets=isSupported&&(tds[0].offsetHeight===0);
div.innerHTML="";
div.style.cssText="box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%;";
support.boxSizing=(div.offsetWidth===4);
support.doesNotIncludeMarginInBodyOffset=(body.offsetTop!==1);
if(window.getComputedStyle){support.pixelPosition=(window.getComputedStyle(div,null)||{}).top!=="1%";
support.boxSizingReliable=(window.getComputedStyle(div,null)||{width:"4px"}).width==="4px";
marginDiv=document.createElement("div");
marginDiv.style.cssText=div.style.cssText=divReset;
marginDiv.style.marginRight=marginDiv.style.width="0";
div.style.width="1px";
div.appendChild(marginDiv);
support.reliableMarginRight=!parseFloat((window.getComputedStyle(marginDiv,null)||{}).marginRight)
}if(typeof div.style.zoom!=="undefined"){div.innerHTML="";
div.style.cssText=divReset+"width:1px;padding:1px;display:inline;zoom:1";
support.inlineBlockNeedsLayout=(div.offsetWidth===3);
div.style.display="block";
div.style.overflow="visible";
div.innerHTML="<div></div>";
div.firstChild.style.width="5px";
support.shrinkWrapBlocks=(div.offsetWidth!==3);
container.style.zoom=1
}body.removeChild(container);
container=div=tds=marginDiv=null
});
fragment.removeChild(div);
all=a=select=opt=input=fragment=div=null;
return support
})();
var rbrace=/(?:\{[\s\S]*\}|\[[\s\S]*\])$/,rmultiDash=/([A-Z])/g;
jQuery.extend({cache:{},deletedIds:[],uuid:0,expando:"jQuery"+(jQuery.fn.jquery+Math.random()).replace(/\D/g,""),noData:{embed:true,object:"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",applet:true},hasData:function(elem){elem=elem.nodeType?jQuery.cache[elem[jQuery.expando]]:elem[jQuery.expando];
return !!elem&&!isEmptyDataObject(elem)
},data:function(elem,name,data,pvt){if(!jQuery.acceptData(elem)){return 
}var thisCache,ret,internalKey=jQuery.expando,getByName=typeof name==="string",isNode=elem.nodeType,cache=isNode?jQuery.cache:elem,id=isNode?elem[internalKey]:elem[internalKey]&&internalKey;
if((!id||!cache[id]||(!pvt&&!cache[id].data))&&getByName&&data===undefined){return 
}if(!id){if(isNode){elem[internalKey]=id=jQuery.deletedIds.pop()||jQuery.guid++
}else{id=internalKey
}}if(!cache[id]){cache[id]={};
if(!isNode){cache[id].toJSON=jQuery.noop
}}if(typeof name==="object"||typeof name==="function"){if(pvt){cache[id]=jQuery.extend(cache[id],name)
}else{cache[id].data=jQuery.extend(cache[id].data,name)
}}thisCache=cache[id];
if(!pvt){if(!thisCache.data){thisCache.data={}
}thisCache=thisCache.data
}if(data!==undefined){thisCache[jQuery.camelCase(name)]=data
}if(getByName){ret=thisCache[name];
if(ret==null){ret=thisCache[jQuery.camelCase(name)]
}}else{ret=thisCache
}return ret
},removeData:function(elem,name,pvt){if(!jQuery.acceptData(elem)){return 
}var thisCache,i,l,isNode=elem.nodeType,cache=isNode?jQuery.cache:elem,id=isNode?elem[jQuery.expando]:jQuery.expando;
if(!cache[id]){return 
}if(name){thisCache=pvt?cache[id]:cache[id].data;
if(thisCache){if(!jQuery.isArray(name)){if(name in thisCache){name=[name]
}else{name=jQuery.camelCase(name);
if(name in thisCache){name=[name]
}else{name=name.split(" ")
}}}for(i=0,l=name.length;
i<l;
i++){delete thisCache[name[i]]
}if(!(pvt?isEmptyDataObject:jQuery.isEmptyObject)(thisCache)){return 
}}}if(!pvt){delete cache[id].data;
if(!isEmptyDataObject(cache[id])){return 
}}if(isNode){jQuery.cleanData([elem],true)
}else{if(jQuery.support.deleteExpando||cache!=cache.window){delete cache[id]
}else{cache[id]=null
}}},_data:function(elem,name,data){return jQuery.data(elem,name,data,true)
},acceptData:function(elem){var noData=elem.nodeName&&jQuery.noData[elem.nodeName.toLowerCase()];
return !noData||noData!==true&&elem.getAttribute("classid")===noData
}});
jQuery.fn.extend({data:function(key,value){var parts,part,attr,name,l,elem=this[0],i=0,data=null;
if(key===undefined){if(this.length){data=jQuery.data(elem);
if(elem.nodeType===1&&!jQuery._data(elem,"parsedAttrs")){attr=elem.attributes;
for(l=attr.length;
i<l;
i++){name=attr[i].name;
if(!name.indexOf("data-")){name=jQuery.camelCase(name.substring(5));
dataAttr(elem,name,data[name])
}}jQuery._data(elem,"parsedAttrs",true)
}}return data
}if(typeof key==="object"){return this.each(function(){jQuery.data(this,key)
})
}parts=key.split(".",2);
parts[1]=parts[1]?"."+parts[1]:"";
part=parts[1]+"!";
return jQuery.access(this,function(value){if(value===undefined){data=this.triggerHandler("getData"+part,[parts[0]]);
if(data===undefined&&elem){data=jQuery.data(elem,key);
data=dataAttr(elem,key,data)
}return data===undefined&&parts[1]?this.data(parts[0]):data
}parts[1]=value;
this.each(function(){var self=jQuery(this);
self.triggerHandler("setData"+part,parts);
jQuery.data(this,key,value);
self.triggerHandler("changeData"+part,parts)
})
},null,value,arguments.length>1,null,false)
},removeData:function(key){return this.each(function(){jQuery.removeData(this,key)
})
}});
function dataAttr(elem,key,data){if(data===undefined&&elem.nodeType===1){var name="data-"+key.replace(rmultiDash,"-$1").toLowerCase();
data=elem.getAttribute(name);
if(typeof data==="string"){try{data=data==="true"?true:data==="false"?false:data==="null"?null:+data+""===data?+data:rbrace.test(data)?jQuery.parseJSON(data):data
}catch(e){}jQuery.data(elem,key,data)
}else{data=undefined
}}return data
}function isEmptyDataObject(obj){var name;
for(name in obj){if(name==="data"&&jQuery.isEmptyObject(obj[name])){continue
}if(name!=="toJSON"){return false
}}return true
}jQuery.extend({queue:function(elem,type,data){var queue;
if(elem){type=(type||"fx")+"queue";
queue=jQuery._data(elem,type);
if(data){if(!queue||jQuery.isArray(data)){queue=jQuery._data(elem,type,jQuery.makeArray(data))
}else{queue.push(data)
}}return queue||[]
}},dequeue:function(elem,type){type=type||"fx";
var queue=jQuery.queue(elem,type),startLength=queue.length,fn=queue.shift(),hooks=jQuery._queueHooks(elem,type),next=function(){jQuery.dequeue(elem,type)
};
if(fn==="inprogress"){fn=queue.shift();
startLength--
}if(fn){if(type==="fx"){queue.unshift("inprogress")
}delete hooks.stop;
fn.call(elem,next,hooks)
}if(!startLength&&hooks){hooks.empty.fire()
}},_queueHooks:function(elem,type){var key=type+"queueHooks";
return jQuery._data(elem,key)||jQuery._data(elem,key,{empty:jQuery.Callbacks("once memory").add(function(){jQuery.removeData(elem,type+"queue",true);
jQuery.removeData(elem,key,true)
})})
}});
jQuery.fn.extend({queue:function(type,data){var setter=2;
if(typeof type!=="string"){data=type;
type="fx";
setter--
}if(arguments.length<setter){return jQuery.queue(this[0],type)
}return data===undefined?this:this.each(function(){var queue=jQuery.queue(this,type,data);
jQuery._queueHooks(this,type);
if(type==="fx"&&queue[0]!=="inprogress"){jQuery.dequeue(this,type)
}})
},dequeue:function(type){return this.each(function(){jQuery.dequeue(this,type)
})
},delay:function(time,type){time=jQuery.fx?jQuery.fx.speeds[time]||time:time;
type=type||"fx";
return this.queue(type,function(next,hooks){var timeout=setTimeout(next,time);
hooks.stop=function(){clearTimeout(timeout)
}
})
},clearQueue:function(type){return this.queue(type||"fx",[])
},promise:function(type,obj){var tmp,count=1,defer=jQuery.Deferred(),elements=this,i=this.length,resolve=function(){if(!(--count)){defer.resolveWith(elements,[elements])
}};
if(typeof type!=="string"){obj=type;
type=undefined
}type=type||"fx";
while(i--){tmp=jQuery._data(elements[i],type+"queueHooks");
if(tmp&&tmp.empty){count++;
tmp.empty.add(resolve)
}}resolve();
return defer.promise(obj)
}});
var nodeHook,boolHook,fixSpecified,rclass=/[\t\r\n]/g,rreturn=/\r/g,rtype=/^(?:button|input)$/i,rfocusable=/^(?:button|input|object|select|textarea)$/i,rclickable=/^a(?:rea|)$/i,rboolean=/^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,getSetAttribute=jQuery.support.getSetAttribute;
jQuery.fn.extend({attr:function(name,value){return jQuery.access(this,jQuery.attr,name,value,arguments.length>1)
},removeAttr:function(name){return this.each(function(){jQuery.removeAttr(this,name)
})
},prop:function(name,value){return jQuery.access(this,jQuery.prop,name,value,arguments.length>1)
},removeProp:function(name){name=jQuery.propFix[name]||name;
return this.each(function(){try{this[name]=undefined;
delete this[name]
}catch(e){}})
},addClass:function(value){var classNames,i,l,elem,setClass,c,cl;
if(jQuery.isFunction(value)){return this.each(function(j){jQuery(this).addClass(value.call(this,j,this.className))
})
}if(value&&typeof value==="string"){classNames=value.split(core_rspace);
for(i=0,l=this.length;
i<l;
i++){elem=this[i];
if(elem.nodeType===1){if(!elem.className&&classNames.length===1){elem.className=value
}else{setClass=" "+elem.className+" ";
for(c=0,cl=classNames.length;
c<cl;
c++){if(setClass.indexOf(" "+classNames[c]+" ")<0){setClass+=classNames[c]+" "
}}elem.className=jQuery.trim(setClass)
}}}}return this
},removeClass:function(value){var removes,className,elem,c,cl,i,l;
if(jQuery.isFunction(value)){return this.each(function(j){jQuery(this).removeClass(value.call(this,j,this.className))
})
}if((value&&typeof value==="string")||value===undefined){removes=(value||"").split(core_rspace);
for(i=0,l=this.length;
i<l;
i++){elem=this[i];
if(elem.nodeType===1&&elem.className){className=(" "+elem.className+" ").replace(rclass," ");
for(c=0,cl=removes.length;
c<cl;
c++){while(className.indexOf(" "+removes[c]+" ")>=0){className=className.replace(" "+removes[c]+" "," ")
}}elem.className=value?jQuery.trim(className):""
}}}return this
},toggleClass:function(value,stateVal){var type=typeof value,isBool=typeof stateVal==="boolean";
if(jQuery.isFunction(value)){return this.each(function(i){jQuery(this).toggleClass(value.call(this,i,this.className,stateVal),stateVal)
})
}return this.each(function(){if(type==="string"){var className,i=0,self=jQuery(this),state=stateVal,classNames=value.split(core_rspace);
while((className=classNames[i++])){state=isBool?state:!self.hasClass(className);
self[state?"addClass":"removeClass"](className)
}}else{if(type==="undefined"||type==="boolean"){if(this.className){jQuery._data(this,"__className__",this.className)
}this.className=this.className||value===false?"":jQuery._data(this,"__className__")||""
}}})
},hasClass:function(selector){var className=" "+selector+" ",i=0,l=this.length;
for(;
i<l;
i++){if(this[i].nodeType===1&&(" "+this[i].className+" ").replace(rclass," ").indexOf(className)>=0){return true
}}return false
},val:function(value){var hooks,ret,isFunction,elem=this[0];
if(!arguments.length){if(elem){hooks=jQuery.valHooks[elem.type]||jQuery.valHooks[elem.nodeName.toLowerCase()];
if(hooks&&"get" in hooks&&(ret=hooks.get(elem,"value"))!==undefined){return ret
}ret=elem.value;
return typeof ret==="string"?ret.replace(rreturn,""):ret==null?"":ret
}return 
}isFunction=jQuery.isFunction(value);
return this.each(function(i){var val,self=jQuery(this);
if(this.nodeType!==1){return 
}if(isFunction){val=value.call(this,i,self.val())
}else{val=value
}if(val==null){val=""
}else{if(typeof val==="number"){val+=""
}else{if(jQuery.isArray(val)){val=jQuery.map(val,function(value){return value==null?"":value+""
})
}}}hooks=jQuery.valHooks[this.type]||jQuery.valHooks[this.nodeName.toLowerCase()];
if(!hooks||!("set" in hooks)||hooks.set(this,val,"value")===undefined){this.value=val
}})
}});
jQuery.extend({valHooks:{option:{get:function(elem){var val=elem.attributes.value;
return !val||val.specified?elem.value:elem.text
}},select:{get:function(elem){var value,i,max,option,index=elem.selectedIndex,values=[],options=elem.options,one=elem.type==="select-one";
if(index<0){return null
}i=one?index:0;
max=one?index+1:options.length;
for(;
i<max;
i++){option=options[i];
if(option.selected&&(jQuery.support.optDisabled?!option.disabled:option.getAttribute("disabled")===null)&&(!option.parentNode.disabled||!jQuery.nodeName(option.parentNode,"optgroup"))){value=jQuery(option).val();
if(one){return value
}values.push(value)
}}if(one&&!values.length&&options.length){return jQuery(options[index]).val()
}return values
},set:function(elem,value){var values=jQuery.makeArray(value);
jQuery(elem).find("option").each(function(){this.selected=jQuery.inArray(jQuery(this).val(),values)>=0
});
if(!values.length){elem.selectedIndex=-1
}return values
}}},attrFn:{},attr:function(elem,name,value,pass){var ret,hooks,notxml,nType=elem.nodeType;
if(!elem||nType===3||nType===8||nType===2){return 
}if(pass&&jQuery.isFunction(jQuery.fn[name])){return jQuery(elem)[name](value)
}if(typeof elem.getAttribute==="undefined"){return jQuery.prop(elem,name,value)
}notxml=nType!==1||!jQuery.isXMLDoc(elem);
if(notxml){name=name.toLowerCase();
hooks=jQuery.attrHooks[name]||(rboolean.test(name)?boolHook:nodeHook)
}if(value!==undefined){if(value===null){jQuery.removeAttr(elem,name);
return 
}else{if(hooks&&"set" in hooks&&notxml&&(ret=hooks.set(elem,value,name))!==undefined){return ret
}else{elem.setAttribute(name,value+"");
return value
}}}else{if(hooks&&"get" in hooks&&notxml&&(ret=hooks.get(elem,name))!==null){return ret
}else{ret=elem.getAttribute(name);
return ret===null?undefined:ret
}}},removeAttr:function(elem,value){var propName,attrNames,name,isBool,i=0;
if(value&&elem.nodeType===1){attrNames=value.split(core_rspace);
for(;
i<attrNames.length;
i++){name=attrNames[i];
if(name){propName=jQuery.propFix[name]||name;
isBool=rboolean.test(name);
if(!isBool){jQuery.attr(elem,name,"")
}elem.removeAttribute(getSetAttribute?name:propName);
if(isBool&&propName in elem){elem[propName]=false
}}}}},attrHooks:{type:{set:function(elem,value){if(rtype.test(elem.nodeName)&&elem.parentNode){jQuery.error("type property can't be changed")
}else{if(!jQuery.support.radioValue&&value==="radio"&&jQuery.nodeName(elem,"input")){var val=elem.value;
elem.setAttribute("type",value);
if(val){elem.value=val
}return value
}}}},value:{get:function(elem,name){if(nodeHook&&jQuery.nodeName(elem,"button")){return nodeHook.get(elem,name)
}return name in elem?elem.value:null
},set:function(elem,value,name){if(nodeHook&&jQuery.nodeName(elem,"button")){return nodeHook.set(elem,value,name)
}elem.value=value
}}},propFix:{tabindex:"tabIndex",readonly:"readOnly","for":"htmlFor","class":"className",maxlength:"maxLength",cellspacing:"cellSpacing",cellpadding:"cellPadding",rowspan:"rowSpan",colspan:"colSpan",usemap:"useMap",frameborder:"frameBorder",contenteditable:"contentEditable"},prop:function(elem,name,value){var ret,hooks,notxml,nType=elem.nodeType;
if(!elem||nType===3||nType===8||nType===2){return 
}notxml=nType!==1||!jQuery.isXMLDoc(elem);
if(notxml){name=jQuery.propFix[name]||name;
hooks=jQuery.propHooks[name]
}if(value!==undefined){if(hooks&&"set" in hooks&&(ret=hooks.set(elem,value,name))!==undefined){return ret
}else{return(elem[name]=value)
}}else{if(hooks&&"get" in hooks&&(ret=hooks.get(elem,name))!==null){return ret
}else{return elem[name]
}}},propHooks:{tabIndex:{get:function(elem){var attributeNode=elem.getAttributeNode("tabindex");
return attributeNode&&attributeNode.specified?parseInt(attributeNode.value,10):rfocusable.test(elem.nodeName)||rclickable.test(elem.nodeName)&&elem.href?0:undefined
}}}});
boolHook={get:function(elem,name){var attrNode,property=jQuery.prop(elem,name);
return property===true||typeof property!=="boolean"&&(attrNode=elem.getAttributeNode(name))&&attrNode.nodeValue!==false?name.toLowerCase():undefined
},set:function(elem,value,name){var propName;
if(value===false){jQuery.removeAttr(elem,name)
}else{propName=jQuery.propFix[name]||name;
if(propName in elem){elem[propName]=true
}elem.setAttribute(name,name.toLowerCase())
}return name
}};
if(!getSetAttribute){fixSpecified={name:true,id:true,coords:true};
nodeHook=jQuery.valHooks.button={get:function(elem,name){var ret;
ret=elem.getAttributeNode(name);
return ret&&(fixSpecified[name]?ret.value!=="":ret.specified)?ret.value:undefined
},set:function(elem,value,name){var ret=elem.getAttributeNode(name);
if(!ret){ret=document.createAttribute(name);
elem.setAttributeNode(ret)
}return(ret.value=value+"")
}};
jQuery.each(["width","height"],function(i,name){jQuery.attrHooks[name]=jQuery.extend(jQuery.attrHooks[name],{set:function(elem,value){if(value===""){elem.setAttribute(name,"auto");
return value
}}})
});
jQuery.attrHooks.contenteditable={get:nodeHook.get,set:function(elem,value,name){if(value===""){value="false"
}nodeHook.set(elem,value,name)
}}
}if(!jQuery.support.hrefNormalized){jQuery.each(["href","src","width","height"],function(i,name){jQuery.attrHooks[name]=jQuery.extend(jQuery.attrHooks[name],{get:function(elem){var ret=elem.getAttribute(name,2);
return ret===null?undefined:ret
}})
})
}if(!jQuery.support.style){jQuery.attrHooks.style={get:function(elem){return elem.style.cssText.toLowerCase()||undefined
},set:function(elem,value){return(elem.style.cssText=value+"")
}}
}if(!jQuery.support.optSelected){jQuery.propHooks.selected=jQuery.extend(jQuery.propHooks.selected,{get:function(elem){var parent=elem.parentNode;
if(parent){parent.selectedIndex;
if(parent.parentNode){parent.parentNode.selectedIndex
}}return null
}})
}if(!jQuery.support.enctype){jQuery.propFix.enctype="encoding"
}if(!jQuery.support.checkOn){jQuery.each(["radio","checkbox"],function(){jQuery.valHooks[this]={get:function(elem){return elem.getAttribute("value")===null?"on":elem.value
}}
})
}jQuery.each(["radio","checkbox"],function(){jQuery.valHooks[this]=jQuery.extend(jQuery.valHooks[this],{set:function(elem,value){if(jQuery.isArray(value)){return(elem.checked=jQuery.inArray(jQuery(elem).val(),value)>=0)
}}})
});
var rformElems=/^(?:textarea|input|select)$/i,rtypenamespace=/^([^\.]*|)(?:\.(.+)|)$/,rhoverHack=/(?:^|\s)hover(\.\S+|)\b/,rkeyEvent=/^key/,rmouseEvent=/^(?:mouse|contextmenu)|click/,rfocusMorph=/^(?:focusinfocus|focusoutblur)$/,hoverHack=function(events){return jQuery.event.special.hover?events:events.replace(rhoverHack,"mouseenter$1 mouseleave$1")
};
jQuery.event={add:function(elem,types,handler,data,selector){var elemData,eventHandle,events,t,tns,type,namespaces,handleObj,handleObjIn,handlers,special;
if(elem.nodeType===3||elem.nodeType===8||!types||!handler||!(elemData=jQuery._data(elem))){return 
}if(handler.handler){handleObjIn=handler;
handler=handleObjIn.handler;
selector=handleObjIn.selector
}if(!handler.guid){handler.guid=jQuery.guid++
}events=elemData.events;
if(!events){elemData.events=events={}
}eventHandle=elemData.handle;
if(!eventHandle){elemData.handle=eventHandle=function(e){return typeof jQuery!=="undefined"&&(!e||jQuery.event.triggered!==e.type)?jQuery.event.dispatch.apply(eventHandle.elem,arguments):undefined
};
eventHandle.elem=elem
}types=jQuery.trim(hoverHack(types)).split(" ");
for(t=0;
t<types.length;
t++){tns=rtypenamespace.exec(types[t])||[];
type=tns[1];
namespaces=(tns[2]||"").split(".").sort();
special=jQuery.event.special[type]||{};
type=(selector?special.delegateType:special.bindType)||type;
special=jQuery.event.special[type]||{};
handleObj=jQuery.extend({type:type,origType:tns[1],data:data,handler:handler,guid:handler.guid,selector:selector,needsContext:selector&&jQuery.expr.match.needsContext.test(selector),namespace:namespaces.join(".")},handleObjIn);
handlers=events[type];
if(!handlers){handlers=events[type]=[];
handlers.delegateCount=0;
if(!special.setup||special.setup.call(elem,data,namespaces,eventHandle)===false){if(elem.addEventListener){elem.addEventListener(type,eventHandle,false)
}else{if(elem.attachEvent){elem.attachEvent("on"+type,eventHandle)
}}}}if(special.add){special.add.call(elem,handleObj);
if(!handleObj.handler.guid){handleObj.handler.guid=handler.guid
}}if(selector){handlers.splice(handlers.delegateCount++,0,handleObj)
}else{handlers.push(handleObj)
}jQuery.event.global[type]=true
}elem=null
},global:{},remove:function(elem,types,handler,selector,mappedTypes){var t,tns,type,origType,namespaces,origCount,j,events,special,eventType,handleObj,elemData=jQuery.hasData(elem)&&jQuery._data(elem);
if(!elemData||!(events=elemData.events)){return 
}types=jQuery.trim(hoverHack(types||"")).split(" ");
for(t=0;
t<types.length;
t++){tns=rtypenamespace.exec(types[t])||[];
type=origType=tns[1];
namespaces=tns[2];
if(!type){for(type in events){jQuery.event.remove(elem,type+types[t],handler,selector,true)
}continue
}special=jQuery.event.special[type]||{};
type=(selector?special.delegateType:special.bindType)||type;
eventType=events[type]||[];
origCount=eventType.length;
namespaces=namespaces?new RegExp("(^|\\.)"+namespaces.split(".").sort().join("\\.(?:.*\\.|)")+"(\\.|$)"):null;
for(j=0;
j<eventType.length;
j++){handleObj=eventType[j];
if((mappedTypes||origType===handleObj.origType)&&(!handler||handler.guid===handleObj.guid)&&(!namespaces||namespaces.test(handleObj.namespace))&&(!selector||selector===handleObj.selector||selector==="**"&&handleObj.selector)){eventType.splice(j--,1);
if(handleObj.selector){eventType.delegateCount--
}if(special.remove){special.remove.call(elem,handleObj)
}}}if(eventType.length===0&&origCount!==eventType.length){if(!special.teardown||special.teardown.call(elem,namespaces,elemData.handle)===false){jQuery.removeEvent(elem,type,elemData.handle)
}delete events[type]
}}if(jQuery.isEmptyObject(events)){delete elemData.handle;
jQuery.removeData(elem,"events",true)
}},customEvent:{getData:true,setData:true,changeData:true},trigger:function(event,data,elem,onlyHandlers){if(elem&&(elem.nodeType===3||elem.nodeType===8)){return 
}var cache,exclusive,i,cur,old,ontype,special,handle,eventPath,bubbleType,type=event.type||event,namespaces=[];
if(rfocusMorph.test(type+jQuery.event.triggered)){return 
}if(type.indexOf("!")>=0){type=type.slice(0,-1);
exclusive=true
}if(type.indexOf(".")>=0){namespaces=type.split(".");
type=namespaces.shift();
namespaces.sort()
}if((!elem||jQuery.event.customEvent[type])&&!jQuery.event.global[type]){return 
}event=typeof event==="object"?event[jQuery.expando]?event:new jQuery.Event(type,event):new jQuery.Event(type);
event.type=type;
event.isTrigger=true;
event.exclusive=exclusive;
event.namespace=namespaces.join(".");
event.namespace_re=event.namespace?new RegExp("(^|\\.)"+namespaces.join("\\.(?:.*\\.|)")+"(\\.|$)"):null;
ontype=type.indexOf(":")<0?"on"+type:"";
if(!elem){cache=jQuery.cache;
for(i in cache){if(cache[i].events&&cache[i].events[type]){jQuery.event.trigger(event,data,cache[i].handle.elem,true)
}}return 
}event.result=undefined;
if(!event.target){event.target=elem
}data=data!=null?jQuery.makeArray(data):[];
data.unshift(event);
special=jQuery.event.special[type]||{};
if(special.trigger&&special.trigger.apply(elem,data)===false){return 
}eventPath=[[elem,special.bindType||type]];
if(!onlyHandlers&&!special.noBubble&&!jQuery.isWindow(elem)){bubbleType=special.delegateType||type;
cur=rfocusMorph.test(bubbleType+type)?elem:elem.parentNode;
for(old=elem;
cur;
cur=cur.parentNode){eventPath.push([cur,bubbleType]);
old=cur
}if(old===(elem.ownerDocument||document)){eventPath.push([old.defaultView||old.parentWindow||window,bubbleType])
}}for(i=0;
i<eventPath.length&&!event.isPropagationStopped();
i++){cur=eventPath[i][0];
event.type=eventPath[i][1];
handle=(jQuery._data(cur,"events")||{})[event.type]&&jQuery._data(cur,"handle");
if(handle){handle.apply(cur,data)
}handle=ontype&&cur[ontype];
if(handle&&jQuery.acceptData(cur)&&handle.apply&&handle.apply(cur,data)===false){event.preventDefault()
}}event.type=type;
if(!onlyHandlers&&!event.isDefaultPrevented()){if((!special._default||special._default.apply(elem.ownerDocument,data)===false)&&!(type==="click"&&jQuery.nodeName(elem,"a"))&&jQuery.acceptData(elem)){if(ontype&&elem[type]&&((type!=="focus"&&type!=="blur")||event.target.offsetWidth!==0)&&!jQuery.isWindow(elem)){old=elem[ontype];
if(old){elem[ontype]=null
}jQuery.event.triggered=type;
elem[type]();
jQuery.event.triggered=undefined;
if(old){elem[ontype]=old
}}}}return event.result
},dispatch:function(event){event=jQuery.event.fix(event||window.event);
var i,j,cur,ret,selMatch,matched,matches,handleObj,sel,related,handlers=((jQuery._data(this,"events")||{})[event.type]||[]),delegateCount=handlers.delegateCount,args=core_slice.call(arguments),run_all=!event.exclusive&&!event.namespace,special=jQuery.event.special[event.type]||{},handlerQueue=[];
args[0]=event;
event.delegateTarget=this;
if(special.preDispatch&&special.preDispatch.call(this,event)===false){return 
}if(delegateCount&&!(event.button&&event.type==="click")){for(cur=event.target;
cur!=this;
cur=cur.parentNode||this){if(cur.disabled!==true||event.type!=="click"){selMatch={};
matches=[];
for(i=0;
i<delegateCount;
i++){handleObj=handlers[i];
sel=handleObj.selector;
if(selMatch[sel]===undefined){selMatch[sel]=handleObj.needsContext?jQuery(sel,this).index(cur)>=0:jQuery.find(sel,this,null,[cur]).length
}if(selMatch[sel]){matches.push(handleObj)
}}if(matches.length){handlerQueue.push({elem:cur,matches:matches})
}}}}if(handlers.length>delegateCount){handlerQueue.push({elem:this,matches:handlers.slice(delegateCount)})
}for(i=0;
i<handlerQueue.length&&!event.isPropagationStopped();
i++){matched=handlerQueue[i];
event.currentTarget=matched.elem;
for(j=0;
j<matched.matches.length&&!event.isImmediatePropagationStopped();
j++){handleObj=matched.matches[j];
if(run_all||(!event.namespace&&!handleObj.namespace)||event.namespace_re&&event.namespace_re.test(handleObj.namespace)){event.data=handleObj.data;
event.handleObj=handleObj;
ret=((jQuery.event.special[handleObj.origType]||{}).handle||handleObj.handler).apply(matched.elem,args);
if(ret!==undefined){event.result=ret;
if(ret===false){event.preventDefault();
event.stopPropagation()
}}}}}if(special.postDispatch){special.postDispatch.call(this,event)
}return event.result
},props:"attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(event,original){if(event.which==null){event.which=original.charCode!=null?original.charCode:original.keyCode
}return event
}},mouseHooks:{props:"button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(event,original){var eventDoc,doc,body,button=original.button,fromElement=original.fromElement;
if(event.pageX==null&&original.clientX!=null){eventDoc=event.target.ownerDocument||document;
doc=eventDoc.documentElement;
body=eventDoc.body;
event.pageX=original.clientX+(doc&&doc.scrollLeft||body&&body.scrollLeft||0)-(doc&&doc.clientLeft||body&&body.clientLeft||0);
event.pageY=original.clientY+(doc&&doc.scrollTop||body&&body.scrollTop||0)-(doc&&doc.clientTop||body&&body.clientTop||0)
}if(!event.relatedTarget&&fromElement){event.relatedTarget=fromElement===event.target?original.toElement:fromElement
}if(!event.which&&button!==undefined){event.which=(button&1?1:(button&2?3:(button&4?2:0)))
}return event
}},fix:function(event){if(event[jQuery.expando]){return event
}var i,prop,originalEvent=event,fixHook=jQuery.event.fixHooks[event.type]||{},copy=fixHook.props?this.props.concat(fixHook.props):this.props;
event=jQuery.Event(originalEvent);
for(i=copy.length;
i;
){prop=copy[--i];
event[prop]=originalEvent[prop]
}if(!event.target){event.target=originalEvent.srcElement||document
}if(event.target.nodeType===3){event.target=event.target.parentNode
}event.metaKey=!!event.metaKey;
return fixHook.filter?fixHook.filter(event,originalEvent):event
},special:{load:{noBubble:true},focus:{delegateType:"focusin"},blur:{delegateType:"focusout"},beforeunload:{setup:function(data,namespaces,eventHandle){if(jQuery.isWindow(this)){this.onbeforeunload=eventHandle
}},teardown:function(namespaces,eventHandle){if(this.onbeforeunload===eventHandle){this.onbeforeunload=null
}}}},simulate:function(type,elem,event,bubble){var e=jQuery.extend(new jQuery.Event(),event,{type:type,isSimulated:true,originalEvent:{}});
if(bubble){jQuery.event.trigger(e,null,elem)
}else{jQuery.event.dispatch.call(elem,e)
}if(e.isDefaultPrevented()){event.preventDefault()
}}};
jQuery.event.handle=jQuery.event.dispatch;
jQuery.removeEvent=document.removeEventListener?function(elem,type,handle){if(elem.removeEventListener){elem.removeEventListener(type,handle,false)
}}:function(elem,type,handle){var name="on"+type;
if(elem.detachEvent){if(typeof elem[name]==="undefined"){elem[name]=null
}elem.detachEvent(name,handle)
}};
jQuery.Event=function(src,props){if(!(this instanceof jQuery.Event)){return new jQuery.Event(src,props)
}if(src&&src.type){this.originalEvent=src;
this.type=src.type;
this.isDefaultPrevented=(src.defaultPrevented||src.returnValue===false||src.getPreventDefault&&src.getPreventDefault())?returnTrue:returnFalse
}else{this.type=src
}if(props){jQuery.extend(this,props)
}this.timeStamp=src&&src.timeStamp||jQuery.now();
this[jQuery.expando]=true
};
function returnFalse(){return false
}function returnTrue(){return true
}jQuery.Event.prototype={preventDefault:function(){this.isDefaultPrevented=returnTrue;
var e=this.originalEvent;
if(!e){return 
}if(e.preventDefault){e.preventDefault()
}else{e.returnValue=false
}},stopPropagation:function(){this.isPropagationStopped=returnTrue;
var e=this.originalEvent;
if(!e){return 
}if(e.stopPropagation){e.stopPropagation()
}e.cancelBubble=true
},stopImmediatePropagation:function(){this.isImmediatePropagationStopped=returnTrue;
this.stopPropagation()
},isDefaultPrevented:returnFalse,isPropagationStopped:returnFalse,isImmediatePropagationStopped:returnFalse};
jQuery.each({mouseenter:"mouseover",mouseleave:"mouseout"},function(orig,fix){jQuery.event.special[orig]={delegateType:fix,bindType:fix,handle:function(event){var ret,target=this,related=event.relatedTarget,handleObj=event.handleObj,selector=handleObj.selector;
if(!related||(related!==target&&!jQuery.contains(target,related))){event.type=handleObj.origType;
ret=handleObj.handler.apply(this,arguments);
event.type=fix
}return ret
}}
});
if(!jQuery.support.submitBubbles){jQuery.event.special.submit={setup:function(){if(jQuery.nodeName(this,"form")){return false
}jQuery.event.add(this,"click._submit keypress._submit",function(e){var elem=e.target,form=jQuery.nodeName(elem,"input")||jQuery.nodeName(elem,"button")?elem.form:undefined;
if(form&&!jQuery._data(form,"_submit_attached")){jQuery.event.add(form,"submit._submit",function(event){event._submit_bubble=true
});
jQuery._data(form,"_submit_attached",true)
}})
},postDispatch:function(event){if(event._submit_bubble){delete event._submit_bubble;
if(this.parentNode&&!event.isTrigger){jQuery.event.simulate("submit",this.parentNode,event,true)
}}},teardown:function(){if(jQuery.nodeName(this,"form")){return false
}jQuery.event.remove(this,"._submit")
}}
}if(!jQuery.support.changeBubbles){jQuery.event.special.change={setup:function(){if(rformElems.test(this.nodeName)){if(this.type==="checkbox"||this.type==="radio"){jQuery.event.add(this,"propertychange._change",function(event){if(event.originalEvent.propertyName==="checked"){this._just_changed=true
}});
jQuery.event.add(this,"click._change",function(event){if(this._just_changed&&!event.isTrigger){this._just_changed=false
}jQuery.event.simulate("change",this,event,true)
})
}return false
}jQuery.event.add(this,"beforeactivate._change",function(e){var elem=e.target;
if(rformElems.test(elem.nodeName)&&!jQuery._data(elem,"_change_attached")){jQuery.event.add(elem,"change._change",function(event){if(this.parentNode&&!event.isSimulated&&!event.isTrigger){jQuery.event.simulate("change",this.parentNode,event,true)
}});
jQuery._data(elem,"_change_attached",true)
}})
},handle:function(event){var elem=event.target;
if(this!==elem||event.isSimulated||event.isTrigger||(elem.type!=="radio"&&elem.type!=="checkbox")){return event.handleObj.handler.apply(this,arguments)
}},teardown:function(){jQuery.event.remove(this,"._change");
return !rformElems.test(this.nodeName)
}}
}if(!jQuery.support.focusinBubbles){jQuery.each({focus:"focusin",blur:"focusout"},function(orig,fix){var attaches=0,handler=function(event){jQuery.event.simulate(fix,event.target,jQuery.event.fix(event),true)
};
jQuery.event.special[fix]={setup:function(){if(attaches++===0){document.addEventListener(orig,handler,true)
}},teardown:function(){if(--attaches===0){document.removeEventListener(orig,handler,true)
}}}
})
}jQuery.fn.extend({on:function(types,selector,data,fn,one){var origFn,type;
if(typeof types==="object"){if(typeof selector!=="string"){data=data||selector;
selector=undefined
}for(type in types){this.on(type,selector,data,types[type],one)
}return this
}if(data==null&&fn==null){fn=selector;
data=selector=undefined
}else{if(fn==null){if(typeof selector==="string"){fn=data;
data=undefined
}else{fn=data;
data=selector;
selector=undefined
}}}if(fn===false){fn=returnFalse
}else{if(!fn){return this
}}if(one===1){origFn=fn;
fn=function(event){jQuery().off(event);
return origFn.apply(this,arguments)
};
fn.guid=origFn.guid||(origFn.guid=jQuery.guid++)
}return this.each(function(){jQuery.event.add(this,types,fn,data,selector)
})
},one:function(types,selector,data,fn){return this.on(types,selector,data,fn,1)
},off:function(types,selector,fn){var handleObj,type;
if(types&&types.preventDefault&&types.handleObj){handleObj=types.handleObj;
jQuery(types.delegateTarget).off(handleObj.namespace?handleObj.origType+"."+handleObj.namespace:handleObj.origType,handleObj.selector,handleObj.handler);
return this
}if(typeof types==="object"){for(type in types){this.off(type,selector,types[type])
}return this
}if(selector===false||typeof selector==="function"){fn=selector;
selector=undefined
}if(fn===false){fn=returnFalse
}return this.each(function(){jQuery.event.remove(this,types,fn,selector)
})
},bind:function(types,data,fn){return this.on(types,null,data,fn)
},unbind:function(types,fn){return this.off(types,null,fn)
},live:function(types,data,fn){jQuery(this.context).on(types,this.selector,data,fn);
return this
},die:function(types,fn){jQuery(this.context).off(types,this.selector||"**",fn);
return this
},delegate:function(selector,types,data,fn){return this.on(types,selector,data,fn)
},undelegate:function(selector,types,fn){return arguments.length===1?this.off(selector,"**"):this.off(types,selector||"**",fn)
},trigger:function(type,data){return this.each(function(){jQuery.event.trigger(type,data,this)
})
},triggerHandler:function(type,data){if(this[0]){return jQuery.event.trigger(type,data,this[0],true)
}},toggle:function(fn){var args=arguments,guid=fn.guid||jQuery.guid++,i=0,toggler=function(event){var lastToggle=(jQuery._data(this,"lastToggle"+fn.guid)||0)%i;
jQuery._data(this,"lastToggle"+fn.guid,lastToggle+1);
event.preventDefault();
return args[lastToggle].apply(this,arguments)||false
};
toggler.guid=guid;
while(i<args.length){args[i++].guid=guid
}return this.click(toggler)
},hover:function(fnOver,fnOut){return this.mouseenter(fnOver).mouseleave(fnOut||fnOver)
}});
jQuery.each(("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu").split(" "),function(i,name){jQuery.fn[name]=function(data,fn){if(fn==null){fn=data;
data=null
}return arguments.length>0?this.on(name,null,data,fn):this.trigger(name)
};
if(rkeyEvent.test(name)){jQuery.event.fixHooks[name]=jQuery.event.keyHooks
}if(rmouseEvent.test(name)){jQuery.event.fixHooks[name]=jQuery.event.mouseHooks
}});
/*
 * Sizzle CSS Selector Engine
 * Copyright 2012 jQuery Foundation and other contributors
 * Released under the MIT license
 * http://sizzlejs.com/
 */
(function(window,undefined){var cachedruns,assertGetIdNotName,Expr,getText,isXML,contains,compile,sortOrder,hasDuplicate,outermostContext,baseHasDuplicate=true,strundefined="undefined",expando=("sizcache"+Math.random()).replace(".",""),Token=String,document=window.document,docElem=document.documentElement,dirruns=0,done=0,pop=[].pop,push=[].push,slice=[].slice,indexOf=[].indexOf||function(elem){var i=0,len=this.length;
for(;
i<len;
i++){if(this[i]===elem){return i
}}return -1
},markFunction=function(fn,value){fn[expando]=value==null||value;
return fn
},createCache=function(){var cache={},keys=[];
return markFunction(function(key,value){if(keys.push(key)>Expr.cacheLength){delete cache[keys.shift()]
}return(cache[key]=value)
},cache)
},classCache=createCache(),tokenCache=createCache(),compilerCache=createCache(),whitespace="[\\x20\\t\\r\\n\\f]",characterEncoding="(?:\\\\.|[-\\w]|[^\\x00-\\xa0])+",identifier=characterEncoding.replace("w","w#"),operators="([*^$|!~]?=)",attributes="\\["+whitespace+"*("+characterEncoding+")"+whitespace+"*(?:"+operators+whitespace+"*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|("+identifier+")|)|)"+whitespace+"*\\]",pseudos=":("+characterEncoding+")(?:\\((?:(['\"])((?:\\\\.|[^\\\\])*?)\\2|([^()[\\]]*|(?:(?:"+attributes+")|[^:]|\\\\.)*|.*))\\)|)",pos=":(even|odd|eq|gt|lt|nth|first|last)(?:\\("+whitespace+"*((?:-\\d)?\\d*)"+whitespace+"*\\)|)(?=[^-]|$)",rtrim=new RegExp("^"+whitespace+"+|((?:^|[^\\\\])(?:\\\\.)*)"+whitespace+"+$","g"),rcomma=new RegExp("^"+whitespace+"*,"+whitespace+"*"),rcombinators=new RegExp("^"+whitespace+"*([\\x20\\t\\r\\n\\f>+~])"+whitespace+"*"),rpseudo=new RegExp(pseudos),rquickExpr=/^(?:#([\w\-]+)|(\w+)|\.([\w\-]+))$/,rnot=/^:not/,rsibling=/[\x20\t\r\n\f]*[+~]/,rendsWithNot=/:not\($/,rheader=/h\d/i,rinputs=/input|select|textarea|button/i,rbackslash=/\\(?!\\)/g,matchExpr={ID:new RegExp("^#("+characterEncoding+")"),CLASS:new RegExp("^\\.("+characterEncoding+")"),NAME:new RegExp("^\\[name=['\"]?("+characterEncoding+")['\"]?\\]"),TAG:new RegExp("^("+characterEncoding.replace("w","w*")+")"),ATTR:new RegExp("^"+attributes),PSEUDO:new RegExp("^"+pseudos),POS:new RegExp(pos,"i"),CHILD:new RegExp("^:(only|nth|first|last)-child(?:\\("+whitespace+"*(even|odd|(([+-]|)(\\d*)n|)"+whitespace+"*(?:([+-]|)"+whitespace+"*(\\d+)|))"+whitespace+"*\\)|)","i"),needsContext:new RegExp("^"+whitespace+"*[>+~]|"+pos,"i")},assert=function(fn){var div=document.createElement("div");
try{return fn(div)
}catch(e){return false
}finally{div=null
}},assertTagNameNoComments=assert(function(div){div.appendChild(document.createComment(""));
return !div.getElementsByTagName("*").length
}),assertHrefNotNormalized=assert(function(div){div.innerHTML="<a href='#'></a>";
return div.firstChild&&typeof div.firstChild.getAttribute!==strundefined&&div.firstChild.getAttribute("href")==="#"
}),assertAttributes=assert(function(div){div.innerHTML="<select></select>";
var type=typeof div.lastChild.getAttribute("multiple");
return type!=="boolean"&&type!=="string"
}),assertUsableClassName=assert(function(div){div.innerHTML="<div class='hidden e'></div><div class='hidden'></div>";
if(!div.getElementsByClassName||!div.getElementsByClassName("e").length){return false
}div.lastChild.className="e";
return div.getElementsByClassName("e").length===2
}),assertUsableName=assert(function(div){div.id=expando+0;
div.innerHTML="<a name='"+expando+"'></a><div name='"+expando+"'></div>";
docElem.insertBefore(div,docElem.firstChild);
var pass=document.getElementsByName&&document.getElementsByName(expando).length===2+document.getElementsByName(expando+0).length;
assertGetIdNotName=!document.getElementById(expando);
docElem.removeChild(div);
return pass
});
try{slice.call(docElem.childNodes,0)[0].nodeType
}catch(e){slice=function(i){var elem,results=[];
for(;
(elem=this[i]);
i++){results.push(elem)
}return results
}
}function Sizzle(selector,context,results,seed){results=results||[];
context=context||document;
var match,elem,xml,m,nodeType=context.nodeType;
if(!selector||typeof selector!=="string"){return results
}if(nodeType!==1&&nodeType!==9){return[]
}xml=isXML(context);
if(!xml&&!seed){if((match=rquickExpr.exec(selector))){if((m=match[1])){if(nodeType===9){elem=context.getElementById(m);
if(elem&&elem.parentNode){if(elem.id===m){results.push(elem);
return results
}}else{return results
}}else{if(context.ownerDocument&&(elem=context.ownerDocument.getElementById(m))&&contains(context,elem)&&elem.id===m){results.push(elem);
return results
}}}else{if(match[2]){push.apply(results,slice.call(context.getElementsByTagName(selector),0));
return results
}else{if((m=match[3])&&assertUsableClassName&&context.getElementsByClassName){push.apply(results,slice.call(context.getElementsByClassName(m),0));
return results
}}}}}return select(selector.replace(rtrim,"$1"),context,results,seed,xml)
}Sizzle.matches=function(expr,elements){return Sizzle(expr,null,null,elements)
};
Sizzle.matchesSelector=function(elem,expr){return Sizzle(expr,null,null,[elem]).length>0
};
function createInputPseudo(type){return function(elem){var name=elem.nodeName.toLowerCase();
return name==="input"&&elem.type===type
}
}function createButtonPseudo(type){return function(elem){var name=elem.nodeName.toLowerCase();
return(name==="input"||name==="button")&&elem.type===type
}
}function createPositionalPseudo(fn){return markFunction(function(argument){argument=+argument;
return markFunction(function(seed,matches){var j,matchIndexes=fn([],seed.length,argument),i=matchIndexes.length;
while(i--){if(seed[(j=matchIndexes[i])]){seed[j]=!(matches[j]=seed[j])
}}})
})
}getText=Sizzle.getText=function(elem){var node,ret="",i=0,nodeType=elem.nodeType;
if(nodeType){if(nodeType===1||nodeType===9||nodeType===11){if(typeof elem.textContent==="string"){return elem.textContent
}else{for(elem=elem.firstChild;
elem;
elem=elem.nextSibling){ret+=getText(elem)
}}}else{if(nodeType===3||nodeType===4){return elem.nodeValue
}}}else{for(;
(node=elem[i]);
i++){ret+=getText(node)
}}return ret
};
isXML=Sizzle.isXML=function(elem){var documentElement=elem&&(elem.ownerDocument||elem).documentElement;
return documentElement?documentElement.nodeName!=="HTML":false
};
contains=Sizzle.contains=docElem.contains?function(a,b){var adown=a.nodeType===9?a.documentElement:a,bup=b&&b.parentNode;
return a===bup||!!(bup&&bup.nodeType===1&&adown.contains&&adown.contains(bup))
}:docElem.compareDocumentPosition?function(a,b){return b&&!!(a.compareDocumentPosition(b)&16)
}:function(a,b){while((b=b.parentNode)){if(b===a){return true
}}return false
};
Sizzle.attr=function(elem,name){var val,xml=isXML(elem);
if(!xml){name=name.toLowerCase()
}if((val=Expr.attrHandle[name])){return val(elem)
}if(xml||assertAttributes){return elem.getAttribute(name)
}val=elem.getAttributeNode(name);
return val?typeof elem[name]==="boolean"?elem[name]?name:null:val.specified?val.value:null:null
};
Expr=Sizzle.selectors={cacheLength:50,createPseudo:markFunction,match:matchExpr,attrHandle:assertHrefNotNormalized?{}:{href:function(elem){return elem.getAttribute("href",2)
},type:function(elem){return elem.getAttribute("type")
}},find:{ID:assertGetIdNotName?function(id,context,xml){if(typeof context.getElementById!==strundefined&&!xml){var m=context.getElementById(id);
return m&&m.parentNode?[m]:[]
}}:function(id,context,xml){if(typeof context.getElementById!==strundefined&&!xml){var m=context.getElementById(id);
return m?m.id===id||typeof m.getAttributeNode!==strundefined&&m.getAttributeNode("id").value===id?[m]:undefined:[]
}},TAG:assertTagNameNoComments?function(tag,context){if(typeof context.getElementsByTagName!==strundefined){return context.getElementsByTagName(tag)
}}:function(tag,context){var results=context.getElementsByTagName(tag);
if(tag==="*"){var elem,tmp=[],i=0;
for(;
(elem=results[i]);
i++){if(elem.nodeType===1){tmp.push(elem)
}}return tmp
}return results
},NAME:assertUsableName&&function(tag,context){if(typeof context.getElementsByName!==strundefined){return context.getElementsByName(name)
}},CLASS:assertUsableClassName&&function(className,context,xml){if(typeof context.getElementsByClassName!==strundefined&&!xml){return context.getElementsByClassName(className)
}}},relative:{">":{dir:"parentNode",first:true}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:true},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(match){match[1]=match[1].replace(rbackslash,"");
match[3]=(match[4]||match[5]||"").replace(rbackslash,"");
if(match[2]==="~="){match[3]=" "+match[3]+" "
}return match.slice(0,4)
},CHILD:function(match){match[1]=match[1].toLowerCase();
if(match[1]==="nth"){if(!match[2]){Sizzle.error(match[0])
}match[3]=+(match[3]?match[4]+(match[5]||1):2*(match[2]==="even"||match[2]==="odd"));
match[4]=+((match[6]+match[7])||match[2]==="odd")
}else{if(match[2]){Sizzle.error(match[0])
}}return match
},PSEUDO:function(match){var unquoted,excess;
if(matchExpr.CHILD.test(match[0])){return null
}if(match[3]){match[2]=match[3]
}else{if((unquoted=match[4])){if(rpseudo.test(unquoted)&&(excess=tokenize(unquoted,true))&&(excess=unquoted.indexOf(")",unquoted.length-excess)-unquoted.length)){unquoted=unquoted.slice(0,excess);
match[0]=match[0].slice(0,excess)
}match[2]=unquoted
}}return match.slice(0,3)
}},filter:{ID:assertGetIdNotName?function(id){id=id.replace(rbackslash,"");
return function(elem){return elem.getAttribute("id")===id
}
}:function(id){id=id.replace(rbackslash,"");
return function(elem){var node=typeof elem.getAttributeNode!==strundefined&&elem.getAttributeNode("id");
return node&&node.value===id
}
},TAG:function(nodeName){if(nodeName==="*"){return function(){return true
}
}nodeName=nodeName.replace(rbackslash,"").toLowerCase();
return function(elem){return elem.nodeName&&elem.nodeName.toLowerCase()===nodeName
}
},CLASS:function(className){var pattern=classCache[expando][className];
if(!pattern){pattern=classCache(className,new RegExp("(^|"+whitespace+")"+className+"("+whitespace+"|$)"))
}return function(elem){return pattern.test(elem.className||(typeof elem.getAttribute!==strundefined&&elem.getAttribute("class"))||"")
}
},ATTR:function(name,operator,check){return function(elem,context){var result=Sizzle.attr(elem,name);
if(result==null){return operator==="!="
}if(!operator){return true
}result+="";
return operator==="="?result===check:operator==="!="?result!==check:operator==="^="?check&&result.indexOf(check)===0:operator==="*="?check&&result.indexOf(check)>-1:operator==="$="?check&&result.substr(result.length-check.length)===check:operator==="~="?(" "+result+" ").indexOf(check)>-1:operator==="|="?result===check||result.substr(0,check.length+1)===check+"-":false
}
},CHILD:function(type,argument,first,last){if(type==="nth"){return function(elem){var node,diff,parent=elem.parentNode;
if(first===1&&last===0){return true
}if(parent){diff=0;
for(node=parent.firstChild;
node;
node=node.nextSibling){if(node.nodeType===1){diff++;
if(elem===node){break
}}}}diff-=last;
return diff===first||(diff%first===0&&diff/first>=0)
}
}return function(elem){var node=elem;
switch(type){case"only":case"first":while((node=node.previousSibling)){if(node.nodeType===1){return false
}}if(type==="first"){return true
}node=elem;
case"last":while((node=node.nextSibling)){if(node.nodeType===1){return false
}}return true
}}
},PSEUDO:function(pseudo,argument){var args,fn=Expr.pseudos[pseudo]||Expr.setFilters[pseudo.toLowerCase()]||Sizzle.error("unsupported pseudo: "+pseudo);
if(fn[expando]){return fn(argument)
}if(fn.length>1){args=[pseudo,pseudo,"",argument];
return Expr.setFilters.hasOwnProperty(pseudo.toLowerCase())?markFunction(function(seed,matches){var idx,matched=fn(seed,argument),i=matched.length;
while(i--){idx=indexOf.call(seed,matched[i]);
seed[idx]=!(matches[idx]=matched[i])
}}):function(elem){return fn(elem,0,args)
}
}return fn
}},pseudos:{not:markFunction(function(selector){var input=[],results=[],matcher=compile(selector.replace(rtrim,"$1"));
return matcher[expando]?markFunction(function(seed,matches,context,xml){var elem,unmatched=matcher(seed,null,xml,[]),i=seed.length;
while(i--){if((elem=unmatched[i])){seed[i]=!(matches[i]=elem)
}}}):function(elem,context,xml){input[0]=elem;
matcher(input,null,xml,results);
return !results.pop()
}
}),has:markFunction(function(selector){return function(elem){return Sizzle(selector,elem).length>0
}
}),contains:markFunction(function(text){return function(elem){return(elem.textContent||elem.innerText||getText(elem)).indexOf(text)>-1
}
}),enabled:function(elem){return elem.disabled===false
},disabled:function(elem){return elem.disabled===true
},checked:function(elem){var nodeName=elem.nodeName.toLowerCase();
return(nodeName==="input"&&!!elem.checked)||(nodeName==="option"&&!!elem.selected)
},selected:function(elem){if(elem.parentNode){elem.parentNode.selectedIndex
}return elem.selected===true
},parent:function(elem){return !Expr.pseudos.empty(elem)
},empty:function(elem){var nodeType;
elem=elem.firstChild;
while(elem){if(elem.nodeName>"@"||(nodeType=elem.nodeType)===3||nodeType===4){return false
}elem=elem.nextSibling
}return true
},header:function(elem){return rheader.test(elem.nodeName)
},text:function(elem){var type,attr;
return elem.nodeName.toLowerCase()==="input"&&(type=elem.type)==="text"&&((attr=elem.getAttribute("type"))==null||attr.toLowerCase()===type)
},radio:createInputPseudo("radio"),checkbox:createInputPseudo("checkbox"),file:createInputPseudo("file"),password:createInputPseudo("password"),image:createInputPseudo("image"),submit:createButtonPseudo("submit"),reset:createButtonPseudo("reset"),button:function(elem){var name=elem.nodeName.toLowerCase();
return name==="input"&&elem.type==="button"||name==="button"
},input:function(elem){return rinputs.test(elem.nodeName)
},focus:function(elem){var doc=elem.ownerDocument;
return elem===doc.activeElement&&(!doc.hasFocus||doc.hasFocus())&&!!(elem.type||elem.href)
},active:function(elem){return elem===elem.ownerDocument.activeElement
},first:createPositionalPseudo(function(matchIndexes,length,argument){return[0]
}),last:createPositionalPseudo(function(matchIndexes,length,argument){return[length-1]
}),eq:createPositionalPseudo(function(matchIndexes,length,argument){return[argument<0?argument+length:argument]
}),even:createPositionalPseudo(function(matchIndexes,length,argument){for(var i=0;
i<length;
i+=2){matchIndexes.push(i)
}return matchIndexes
}),odd:createPositionalPseudo(function(matchIndexes,length,argument){for(var i=1;
i<length;
i+=2){matchIndexes.push(i)
}return matchIndexes
}),lt:createPositionalPseudo(function(matchIndexes,length,argument){for(var i=argument<0?argument+length:argument;
--i>=0;
){matchIndexes.push(i)
}return matchIndexes
}),gt:createPositionalPseudo(function(matchIndexes,length,argument){for(var i=argument<0?argument+length:argument;
++i<length;
){matchIndexes.push(i)
}return matchIndexes
})}};
function siblingCheck(a,b,ret){if(a===b){return ret
}var cur=a.nextSibling;
while(cur){if(cur===b){return -1
}cur=cur.nextSibling
}return 1
}sortOrder=docElem.compareDocumentPosition?function(a,b){if(a===b){hasDuplicate=true;
return 0
}return(!a.compareDocumentPosition||!b.compareDocumentPosition?a.compareDocumentPosition:a.compareDocumentPosition(b)&4)?-1:1
}:function(a,b){if(a===b){hasDuplicate=true;
return 0
}else{if(a.sourceIndex&&b.sourceIndex){return a.sourceIndex-b.sourceIndex
}}var al,bl,ap=[],bp=[],aup=a.parentNode,bup=b.parentNode,cur=aup;
if(aup===bup){return siblingCheck(a,b)
}else{if(!aup){return -1
}else{if(!bup){return 1
}}}while(cur){ap.unshift(cur);
cur=cur.parentNode
}cur=bup;
while(cur){bp.unshift(cur);
cur=cur.parentNode
}al=ap.length;
bl=bp.length;
for(var i=0;
i<al&&i<bl;
i++){if(ap[i]!==bp[i]){return siblingCheck(ap[i],bp[i])
}}return i===al?siblingCheck(a,bp[i],-1):siblingCheck(ap[i],b,1)
};
[0,0].sort(sortOrder);
baseHasDuplicate=!hasDuplicate;
Sizzle.uniqueSort=function(results){var elem,i=1;
hasDuplicate=baseHasDuplicate;
results.sort(sortOrder);
if(hasDuplicate){for(;
(elem=results[i]);
i++){if(elem===results[i-1]){results.splice(i--,1)
}}}return results
};
Sizzle.error=function(msg){throw new Error("Syntax error, unrecognized expression: "+msg)
};
function tokenize(selector,parseOnly){var matched,match,tokens,type,soFar,groups,preFilters,cached=tokenCache[expando][selector];
if(cached){return parseOnly?0:cached.slice(0)
}soFar=selector;
groups=[];
preFilters=Expr.preFilter;
while(soFar){if(!matched||(match=rcomma.exec(soFar))){if(match){soFar=soFar.slice(match[0].length)
}groups.push(tokens=[])
}matched=false;
if((match=rcombinators.exec(soFar))){tokens.push(matched=new Token(match.shift()));
soFar=soFar.slice(matched.length);
matched.type=match[0].replace(rtrim," ")
}for(type in Expr.filter){if((match=matchExpr[type].exec(soFar))&&(!preFilters[type]||(match=preFilters[type](match,document,true)))){tokens.push(matched=new Token(match.shift()));
soFar=soFar.slice(matched.length);
matched.type=type;
matched.matches=match
}}if(!matched){break
}}return parseOnly?soFar.length:soFar?Sizzle.error(selector):tokenCache(selector,groups).slice(0)
}function addCombinator(matcher,combinator,base){var dir=combinator.dir,checkNonElements=base&&combinator.dir==="parentNode",doneName=done++;
return combinator.first?function(elem,context,xml){while((elem=elem[dir])){if(checkNonElements||elem.nodeType===1){return matcher(elem,context,xml)
}}}:function(elem,context,xml){if(!xml){var cache,dirkey=dirruns+" "+doneName+" ",cachedkey=dirkey+cachedruns;
while((elem=elem[dir])){if(checkNonElements||elem.nodeType===1){if((cache=elem[expando])===cachedkey){return elem.sizset
}else{if(typeof cache==="string"&&cache.indexOf(dirkey)===0){if(elem.sizset){return elem
}}else{elem[expando]=cachedkey;
if(matcher(elem,context,xml)){elem.sizset=true;
return elem
}elem.sizset=false
}}}}}else{while((elem=elem[dir])){if(checkNonElements||elem.nodeType===1){if(matcher(elem,context,xml)){return elem
}}}}}
}function elementMatcher(matchers){return matchers.length>1?function(elem,context,xml){var i=matchers.length;
while(i--){if(!matchers[i](elem,context,xml)){return false
}}return true
}:matchers[0]
}function condense(unmatched,map,filter,context,xml){var elem,newUnmatched=[],i=0,len=unmatched.length,mapped=map!=null;
for(;
i<len;
i++){if((elem=unmatched[i])){if(!filter||filter(elem,context,xml)){newUnmatched.push(elem);
if(mapped){map.push(i)
}}}}return newUnmatched
}function setMatcher(preFilter,selector,matcher,postFilter,postFinder,postSelector){if(postFilter&&!postFilter[expando]){postFilter=setMatcher(postFilter)
}if(postFinder&&!postFinder[expando]){postFinder=setMatcher(postFinder,postSelector)
}return markFunction(function(seed,results,context,xml){if(seed&&postFinder){return 
}var i,elem,postFilterIn,preMap=[],postMap=[],preexisting=results.length,elems=seed||multipleContexts(selector||"*",context.nodeType?[context]:context,[],seed),matcherIn=preFilter&&(seed||!selector)?condense(elems,preMap,preFilter,context,xml):elems,matcherOut=matcher?postFinder||(seed?preFilter:preexisting||postFilter)?[]:results:matcherIn;
if(matcher){matcher(matcherIn,matcherOut,context,xml)
}if(postFilter){postFilterIn=condense(matcherOut,postMap);
postFilter(postFilterIn,[],context,xml);
i=postFilterIn.length;
while(i--){if((elem=postFilterIn[i])){matcherOut[postMap[i]]=!(matcherIn[postMap[i]]=elem)
}}}if(seed){i=preFilter&&matcherOut.length;
while(i--){if((elem=matcherOut[i])){seed[preMap[i]]=!(results[preMap[i]]=elem)
}}}else{matcherOut=condense(matcherOut===results?matcherOut.splice(preexisting,matcherOut.length):matcherOut);
if(postFinder){postFinder(null,results,matcherOut,xml)
}else{push.apply(results,matcherOut)
}}})
}function matcherFromTokens(tokens){var checkContext,matcher,j,len=tokens.length,leadingRelative=Expr.relative[tokens[0].type],implicitRelative=leadingRelative||Expr.relative[" "],i=leadingRelative?1:0,matchContext=addCombinator(function(elem){return elem===checkContext
},implicitRelative,true),matchAnyContext=addCombinator(function(elem){return indexOf.call(checkContext,elem)>-1
},implicitRelative,true),matchers=[function(elem,context,xml){return(!leadingRelative&&(xml||context!==outermostContext))||((checkContext=context).nodeType?matchContext(elem,context,xml):matchAnyContext(elem,context,xml))
}];
for(;
i<len;
i++){if((matcher=Expr.relative[tokens[i].type])){matchers=[addCombinator(elementMatcher(matchers),matcher)]
}else{matcher=Expr.filter[tokens[i].type].apply(null,tokens[i].matches);
if(matcher[expando]){j=++i;
for(;
j<len;
j++){if(Expr.relative[tokens[j].type]){break
}}return setMatcher(i>1&&elementMatcher(matchers),i>1&&tokens.slice(0,i-1).join("").replace(rtrim,"$1"),matcher,i<j&&matcherFromTokens(tokens.slice(i,j)),j<len&&matcherFromTokens((tokens=tokens.slice(j))),j<len&&tokens.join(""))
}matchers.push(matcher)
}}return elementMatcher(matchers)
}function matcherFromGroupMatchers(elementMatchers,setMatchers){var bySet=setMatchers.length>0,byElement=elementMatchers.length>0,superMatcher=function(seed,context,xml,results,expandContext){var elem,j,matcher,setMatched=[],matchedCount=0,i="0",unmatched=seed&&[],outermost=expandContext!=null,contextBackup=outermostContext,elems=seed||byElement&&Expr.find.TAG("*",expandContext&&context.parentNode||context),dirrunsUnique=(dirruns+=contextBackup==null?1:Math.E);
if(outermost){outermostContext=context!==document&&context;
cachedruns=superMatcher.el
}for(;
(elem=elems[i])!=null;
i++){if(byElement&&elem){for(j=0;
(matcher=elementMatchers[j]);
j++){if(matcher(elem,context,xml)){results.push(elem);
break
}}if(outermost){dirruns=dirrunsUnique;
cachedruns=++superMatcher.el
}}if(bySet){if((elem=!matcher&&elem)){matchedCount--
}if(seed){unmatched.push(elem)
}}}matchedCount+=i;
if(bySet&&i!==matchedCount){for(j=0;
(matcher=setMatchers[j]);
j++){matcher(unmatched,setMatched,context,xml)
}if(seed){if(matchedCount>0){while(i--){if(!(unmatched[i]||setMatched[i])){setMatched[i]=pop.call(results)
}}}setMatched=condense(setMatched)
}push.apply(results,setMatched);
if(outermost&&!seed&&setMatched.length>0&&(matchedCount+setMatchers.length)>1){Sizzle.uniqueSort(results)
}}if(outermost){dirruns=dirrunsUnique;
outermostContext=contextBackup
}return unmatched
};
superMatcher.el=0;
return bySet?markFunction(superMatcher):superMatcher
}compile=Sizzle.compile=function(selector,group){var i,setMatchers=[],elementMatchers=[],cached=compilerCache[expando][selector];
if(!cached){if(!group){group=tokenize(selector)
}i=group.length;
while(i--){cached=matcherFromTokens(group[i]);
if(cached[expando]){setMatchers.push(cached)
}else{elementMatchers.push(cached)
}}cached=compilerCache(selector,matcherFromGroupMatchers(elementMatchers,setMatchers))
}return cached
};
function multipleContexts(selector,contexts,results,seed){var i=0,len=contexts.length;
for(;
i<len;
i++){Sizzle(selector,contexts[i],results,seed)
}return results
}function select(selector,context,results,seed,xml){var i,tokens,token,type,find,match=tokenize(selector),j=match.length;
if(!seed){if(match.length===1){tokens=match[0]=match[0].slice(0);
if(tokens.length>2&&(token=tokens[0]).type==="ID"&&context.nodeType===9&&!xml&&Expr.relative[tokens[1].type]){context=Expr.find.ID(token.matches[0].replace(rbackslash,""),context,xml)[0];
if(!context){return results
}selector=selector.slice(tokens.shift().length)
}for(i=matchExpr.POS.test(selector)?-1:tokens.length-1;
i>=0;
i--){token=tokens[i];
if(Expr.relative[(type=token.type)]){break
}if((find=Expr.find[type])){if((seed=find(token.matches[0].replace(rbackslash,""),rsibling.test(tokens[0].type)&&context.parentNode||context,xml))){tokens.splice(i,1);
selector=seed.length&&tokens.join("");
if(!selector){push.apply(results,slice.call(seed,0));
return results
}break
}}}}}compile(selector,match)(seed,context,xml,results,rsibling.test(selector));
return results
}if(document.querySelectorAll){(function(){var disconnectedMatch,oldSelect=select,rescape=/'|\\/g,rattributeQuotes=/\=[\x20\t\r\n\f]*([^'"\]]*)[\x20\t\r\n\f]*\]/g,rbuggyQSA=[":focus"],rbuggyMatches=[":active",":focus"],matches=docElem.matchesSelector||docElem.mozMatchesSelector||docElem.webkitMatchesSelector||docElem.oMatchesSelector||docElem.msMatchesSelector;
assert(function(div){div.innerHTML="<select><option selected=''></option></select>";
if(!div.querySelectorAll("[selected]").length){rbuggyQSA.push("\\["+whitespace+"*(?:checked|disabled|ismap|multiple|readonly|selected|value)")
}if(!div.querySelectorAll(":checked").length){rbuggyQSA.push(":checked")
}});
assert(function(div){div.innerHTML="<p test=''></p>";
if(div.querySelectorAll("[test^='']").length){rbuggyQSA.push("[*^$]="+whitespace+"*(?:\"\"|'')")
}div.innerHTML="<input type='hidden'/>";
if(!div.querySelectorAll(":enabled").length){rbuggyQSA.push(":enabled",":disabled")
}});
rbuggyQSA=new RegExp(rbuggyQSA.join("|"));
select=function(selector,context,results,seed,xml){if(!seed&&!xml&&(!rbuggyQSA||!rbuggyQSA.test(selector))){var groups,i,old=true,nid=expando,newContext=context,newSelector=context.nodeType===9&&selector;
if(context.nodeType===1&&context.nodeName.toLowerCase()!=="object"){groups=tokenize(selector);
if((old=context.getAttribute("id"))){nid=old.replace(rescape,"\\$&")
}else{context.setAttribute("id",nid)
}nid="[id='"+nid+"'] ";
i=groups.length;
while(i--){groups[i]=nid+groups[i].join("")
}newContext=rsibling.test(selector)&&context.parentNode||context;
newSelector=groups.join(",")
}if(newSelector){try{push.apply(results,slice.call(newContext.querySelectorAll(newSelector),0));
return results
}catch(qsaError){}finally{if(!old){context.removeAttribute("id")
}}}}return oldSelect(selector,context,results,seed,xml)
};
if(matches){assert(function(div){disconnectedMatch=matches.call(div,"div");
try{matches.call(div,"[test!='']:sizzle");
rbuggyMatches.push("!=",pseudos)
}catch(e){}});
rbuggyMatches=new RegExp(rbuggyMatches.join("|"));
Sizzle.matchesSelector=function(elem,expr){expr=expr.replace(rattributeQuotes,"='$1']");
if(!isXML(elem)&&!rbuggyMatches.test(expr)&&(!rbuggyQSA||!rbuggyQSA.test(expr))){try{var ret=matches.call(elem,expr);
if(ret||disconnectedMatch||elem.document&&elem.document.nodeType!==11){return ret
}}catch(e){}}return Sizzle(expr,null,null,[elem]).length>0
}
}})()
}Expr.pseudos.nth=Expr.pseudos.eq;
function setFilters(){}Expr.filters=setFilters.prototype=Expr.pseudos;
Expr.setFilters=new setFilters();
Sizzle.attr=jQuery.attr;
jQuery.find=Sizzle;
jQuery.expr=Sizzle.selectors;
jQuery.expr[":"]=jQuery.expr.pseudos;
jQuery.unique=Sizzle.uniqueSort;
jQuery.text=Sizzle.getText;
jQuery.isXMLDoc=Sizzle.isXML;
jQuery.contains=Sizzle.contains
})(window);
var runtil=/Until$/,rparentsprev=/^(?:parents|prev(?:Until|All))/,isSimple=/^.[^:#\[\.,]*$/,rneedsContext=jQuery.expr.match.needsContext,guaranteedUnique={children:true,contents:true,next:true,prev:true};
jQuery.fn.extend({find:function(selector){var i,l,length,n,r,ret,self=this;
if(typeof selector!=="string"){return jQuery(selector).filter(function(){for(i=0,l=self.length;
i<l;
i++){if(jQuery.contains(self[i],this)){return true
}}})
}ret=this.pushStack("","find",selector);
for(i=0,l=this.length;
i<l;
i++){length=ret.length;
jQuery.find(selector,this[i],ret);
if(i>0){for(n=length;
n<ret.length;
n++){for(r=0;
r<length;
r++){if(ret[r]===ret[n]){ret.splice(n--,1);
break
}}}}}return ret
},has:function(target){var i,targets=jQuery(target,this),len=targets.length;
return this.filter(function(){for(i=0;
i<len;
i++){if(jQuery.contains(this,targets[i])){return true
}}})
},not:function(selector){return this.pushStack(winnow(this,selector,false),"not",selector)
},filter:function(selector){return this.pushStack(winnow(this,selector,true),"filter",selector)
},is:function(selector){return !!selector&&(typeof selector==="string"?rneedsContext.test(selector)?jQuery(selector,this.context).index(this[0])>=0:jQuery.filter(selector,this).length>0:this.filter(selector).length>0)
},closest:function(selectors,context){var cur,i=0,l=this.length,ret=[],pos=rneedsContext.test(selectors)||typeof selectors!=="string"?jQuery(selectors,context||this.context):0;
for(;
i<l;
i++){cur=this[i];
while(cur&&cur.ownerDocument&&cur!==context&&cur.nodeType!==11){if(pos?pos.index(cur)>-1:jQuery.find.matchesSelector(cur,selectors)){ret.push(cur);
break
}cur=cur.parentNode
}}ret=ret.length>1?jQuery.unique(ret):ret;
return this.pushStack(ret,"closest",selectors)
},index:function(elem){if(!elem){return(this[0]&&this[0].parentNode)?this.prevAll().length:-1
}if(typeof elem==="string"){return jQuery.inArray(this[0],jQuery(elem))
}return jQuery.inArray(elem.jquery?elem[0]:elem,this)
},add:function(selector,context){var set=typeof selector==="string"?jQuery(selector,context):jQuery.makeArray(selector&&selector.nodeType?[selector]:selector),all=jQuery.merge(this.get(),set);
return this.pushStack(isDisconnected(set[0])||isDisconnected(all[0])?all:jQuery.unique(all))
},addBack:function(selector){return this.add(selector==null?this.prevObject:this.prevObject.filter(selector))
}});
jQuery.fn.andSelf=jQuery.fn.addBack;
function isDisconnected(node){return !node||!node.parentNode||node.parentNode.nodeType===11
}function sibling(cur,dir){do{cur=cur[dir]
}while(cur&&cur.nodeType!==1);
return cur
}jQuery.each({parent:function(elem){var parent=elem.parentNode;
return parent&&parent.nodeType!==11?parent:null
},parents:function(elem){return jQuery.dir(elem,"parentNode")
},parentsUntil:function(elem,i,until){return jQuery.dir(elem,"parentNode",until)
},next:function(elem){return sibling(elem,"nextSibling")
},prev:function(elem){return sibling(elem,"previousSibling")
},nextAll:function(elem){return jQuery.dir(elem,"nextSibling")
},prevAll:function(elem){return jQuery.dir(elem,"previousSibling")
},nextUntil:function(elem,i,until){return jQuery.dir(elem,"nextSibling",until)
},prevUntil:function(elem,i,until){return jQuery.dir(elem,"previousSibling",until)
},siblings:function(elem){return jQuery.sibling((elem.parentNode||{}).firstChild,elem)
},children:function(elem){return jQuery.sibling(elem.firstChild)
},contents:function(elem){return jQuery.nodeName(elem,"iframe")?elem.contentDocument||elem.contentWindow.document:jQuery.merge([],elem.childNodes)
}},function(name,fn){jQuery.fn[name]=function(until,selector){var ret=jQuery.map(this,fn,until);
if(!runtil.test(name)){selector=until
}if(selector&&typeof selector==="string"){ret=jQuery.filter(selector,ret)
}ret=this.length>1&&!guaranteedUnique[name]?jQuery.unique(ret):ret;
if(this.length>1&&rparentsprev.test(name)){ret=ret.reverse()
}return this.pushStack(ret,name,core_slice.call(arguments).join(","))
}
});
jQuery.extend({filter:function(expr,elems,not){if(not){expr=":not("+expr+")"
}return elems.length===1?jQuery.find.matchesSelector(elems[0],expr)?[elems[0]]:[]:jQuery.find.matches(expr,elems)
},dir:function(elem,dir,until){var matched=[],cur=elem[dir];
while(cur&&cur.nodeType!==9&&(until===undefined||cur.nodeType!==1||!jQuery(cur).is(until))){if(cur.nodeType===1){matched.push(cur)
}cur=cur[dir]
}return matched
},sibling:function(n,elem){var r=[];
for(;
n;
n=n.nextSibling){if(n.nodeType===1&&n!==elem){r.push(n)
}}return r
}});
function winnow(elements,qualifier,keep){qualifier=qualifier||0;
if(jQuery.isFunction(qualifier)){return jQuery.grep(elements,function(elem,i){var retVal=!!qualifier.call(elem,i,elem);
return retVal===keep
})
}else{if(qualifier.nodeType){return jQuery.grep(elements,function(elem,i){return(elem===qualifier)===keep
})
}else{if(typeof qualifier==="string"){var filtered=jQuery.grep(elements,function(elem){return elem.nodeType===1
});
if(isSimple.test(qualifier)){return jQuery.filter(qualifier,filtered,!keep)
}else{qualifier=jQuery.filter(qualifier,filtered)
}}}}return jQuery.grep(elements,function(elem,i){return(jQuery.inArray(elem,qualifier)>=0)===keep
})
}function createSafeFragment(document){var list=nodeNames.split("|"),safeFrag=document.createDocumentFragment();
if(safeFrag.createElement){while(list.length){safeFrag.createElement(list.pop())
}}return safeFrag
}var nodeNames="abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",rinlinejQuery=/ jQuery\d+="(?:null|\d+)"/g,rleadingWhitespace=/^\s+/,rxhtmlTag=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,rtagName=/<([\w:]+)/,rtbody=/<tbody/i,rhtml=/<|&#?\w+;/,rnoInnerhtml=/<(?:script|style|link)/i,rnocache=/<(?:script|object|embed|option|style)/i,rnoshimcache=new RegExp("<(?:"+nodeNames+")[\\s/>]","i"),rcheckableType=/^(?:checkbox|radio)$/,rchecked=/checked\s*(?:[^=]|=\s*.checked.)/i,rscriptType=/\/(java|ecma)script/i,rcleanScript=/^\s*<!(?:\[CDATA\[|\-\-)|[\]\-]{2}>\s*$/g,wrapMap={option:[1,"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],area:[1,"<map>","</map>"],_default:[0,"",""]},safeFragment=createSafeFragment(document),fragmentDiv=safeFragment.appendChild(document.createElement("div"));
wrapMap.optgroup=wrapMap.option;
wrapMap.tbody=wrapMap.tfoot=wrapMap.colgroup=wrapMap.caption=wrapMap.thead;
wrapMap.th=wrapMap.td;
if(!jQuery.support.htmlSerialize){wrapMap._default=[1,"X<div>","</div>"]
}jQuery.fn.extend({text:function(value){return jQuery.access(this,function(value){return value===undefined?jQuery.text(this):this.empty().append((this[0]&&this[0].ownerDocument||document).createTextNode(value))
},null,value,arguments.length)
},wrapAll:function(html){if(jQuery.isFunction(html)){return this.each(function(i){jQuery(this).wrapAll(html.call(this,i))
})
}if(this[0]){var wrap=jQuery(html,this[0].ownerDocument).eq(0).clone(true);
if(this[0].parentNode){wrap.insertBefore(this[0])
}wrap.map(function(){var elem=this;
while(elem.firstChild&&elem.firstChild.nodeType===1){elem=elem.firstChild
}return elem
}).append(this)
}return this
},wrapInner:function(html){if(jQuery.isFunction(html)){return this.each(function(i){jQuery(this).wrapInner(html.call(this,i))
})
}return this.each(function(){var self=jQuery(this),contents=self.contents();
if(contents.length){contents.wrapAll(html)
}else{self.append(html)
}})
},wrap:function(html){var isFunction=jQuery.isFunction(html);
return this.each(function(i){jQuery(this).wrapAll(isFunction?html.call(this,i):html)
})
},unwrap:function(){return this.parent().each(function(){if(!jQuery.nodeName(this,"body")){jQuery(this).replaceWith(this.childNodes)
}}).end()
},append:function(){return this.domManip(arguments,true,function(elem){if(this.nodeType===1||this.nodeType===11){this.appendChild(elem)
}})
},prepend:function(){return this.domManip(arguments,true,function(elem){if(this.nodeType===1||this.nodeType===11){this.insertBefore(elem,this.firstChild)
}})
},before:function(){if(!isDisconnected(this[0])){return this.domManip(arguments,false,function(elem){this.parentNode.insertBefore(elem,this)
})
}if(arguments.length){var set=jQuery.clean(arguments);
return this.pushStack(jQuery.merge(set,this),"before",this.selector)
}},after:function(){if(!isDisconnected(this[0])){return this.domManip(arguments,false,function(elem){this.parentNode.insertBefore(elem,this.nextSibling)
})
}if(arguments.length){var set=jQuery.clean(arguments);
return this.pushStack(jQuery.merge(this,set),"after",this.selector)
}},remove:function(selector,keepData){var elem,i=0;
for(;
(elem=this[i])!=null;
i++){if(!selector||jQuery.filter(selector,[elem]).length){if(!keepData&&elem.nodeType===1){jQuery.cleanData(elem.getElementsByTagName("*"));
jQuery.cleanData([elem])
}if(elem.parentNode){elem.parentNode.removeChild(elem)
}}}return this
},empty:function(){var elem,i=0;
for(;
(elem=this[i])!=null;
i++){if(elem.nodeType===1){jQuery.cleanData(elem.getElementsByTagName("*"))
}while(elem.firstChild){elem.removeChild(elem.firstChild)
}}return this
},clone:function(dataAndEvents,deepDataAndEvents){dataAndEvents=dataAndEvents==null?false:dataAndEvents;
deepDataAndEvents=deepDataAndEvents==null?dataAndEvents:deepDataAndEvents;
return this.map(function(){return jQuery.clone(this,dataAndEvents,deepDataAndEvents)
})
},html:function(value){return jQuery.access(this,function(value){var elem=this[0]||{},i=0,l=this.length;
if(value===undefined){return elem.nodeType===1?elem.innerHTML.replace(rinlinejQuery,""):undefined
}if(typeof value==="string"&&!rnoInnerhtml.test(value)&&(jQuery.support.htmlSerialize||!rnoshimcache.test(value))&&(jQuery.support.leadingWhitespace||!rleadingWhitespace.test(value))&&!wrapMap[(rtagName.exec(value)||["",""])[1].toLowerCase()]){value=value.replace(rxhtmlTag,"<$1></$2>");
try{for(;
i<l;
i++){elem=this[i]||{};
if(elem.nodeType===1){jQuery.cleanData(elem.getElementsByTagName("*"));
elem.innerHTML=value
}}elem=0
}catch(e){}}if(elem){this.empty().append(value)
}},null,value,arguments.length)
},replaceWith:function(value){if(!isDisconnected(this[0])){if(jQuery.isFunction(value)){return this.each(function(i){var self=jQuery(this),old=self.html();
self.replaceWith(value.call(this,i,old))
})
}if(typeof value!=="string"){value=jQuery(value).detach()
}return this.each(function(){var next=this.nextSibling,parent=this.parentNode;
jQuery(this).remove();
if(next){jQuery(next).before(value)
}else{jQuery(parent).append(value)
}})
}return this.length?this.pushStack(jQuery(jQuery.isFunction(value)?value():value),"replaceWith",value):this
},detach:function(selector){return this.remove(selector,true)
},domManip:function(args,table,callback){args=[].concat.apply([],args);
var results,first,fragment,iNoClone,i=0,value=args[0],scripts=[],l=this.length;
if(!jQuery.support.checkClone&&l>1&&typeof value==="string"&&rchecked.test(value)){return this.each(function(){jQuery(this).domManip(args,table,callback)
})
}if(jQuery.isFunction(value)){return this.each(function(i){var self=jQuery(this);
args[0]=value.call(this,i,table?self.html():undefined);
self.domManip(args,table,callback)
})
}if(this[0]){results=jQuery.buildFragment(args,this,scripts);
fragment=results.fragment;
first=fragment.firstChild;
if(fragment.childNodes.length===1){fragment=first
}if(first){table=table&&jQuery.nodeName(first,"tr");
for(iNoClone=results.cacheable||l-1;
i<l;
i++){callback.call(table&&jQuery.nodeName(this[i],"table")?findOrAppend(this[i],"tbody"):this[i],i===iNoClone?fragment:jQuery.clone(fragment,true,true))
}}fragment=first=null;
if(scripts.length){jQuery.each(scripts,function(i,elem){if(elem.src){if(jQuery.ajax){jQuery.ajax({url:elem.src,type:"GET",dataType:"script",async:false,global:false,"throws":true})
}else{jQuery.error("no ajax")
}}else{jQuery.globalEval((elem.text||elem.textContent||elem.innerHTML||"").replace(rcleanScript,""))
}if(elem.parentNode){elem.parentNode.removeChild(elem)
}})
}}return this
}});
function findOrAppend(elem,tag){return elem.getElementsByTagName(tag)[0]||elem.appendChild(elem.ownerDocument.createElement(tag))
}function cloneCopyEvent(src,dest){if(dest.nodeType!==1||!jQuery.hasData(src)){return 
}var type,i,l,oldData=jQuery._data(src),curData=jQuery._data(dest,oldData),events=oldData.events;
if(events){delete curData.handle;
curData.events={};
for(type in events){for(i=0,l=events[type].length;
i<l;
i++){jQuery.event.add(dest,type,events[type][i])
}}}if(curData.data){curData.data=jQuery.extend({},curData.data)
}}function cloneFixAttributes(src,dest){var nodeName;
if(dest.nodeType!==1){return 
}if(dest.clearAttributes){dest.clearAttributes()
}if(dest.mergeAttributes){dest.mergeAttributes(src)
}nodeName=dest.nodeName.toLowerCase();
if(nodeName==="object"){if(dest.parentNode){dest.outerHTML=src.outerHTML
}if(jQuery.support.html5Clone&&(src.innerHTML&&!jQuery.trim(dest.innerHTML))){dest.innerHTML=src.innerHTML
}}else{if(nodeName==="input"&&rcheckableType.test(src.type)){dest.defaultChecked=dest.checked=src.checked;
if(dest.value!==src.value){dest.value=src.value
}}else{if(nodeName==="option"){dest.selected=src.defaultSelected
}else{if(nodeName==="input"||nodeName==="textarea"){dest.defaultValue=src.defaultValue
}else{if(nodeName==="script"&&dest.text!==src.text){dest.text=src.text
}}}}}dest.removeAttribute(jQuery.expando)
}jQuery.buildFragment=function(args,context,scripts){var fragment,cacheable,cachehit,first=args[0];
context=context||document;
context=!context.nodeType&&context[0]||context;
context=context.ownerDocument||context;
if(args.length===1&&typeof first==="string"&&first.length<512&&context===document&&first.charAt(0)==="<"&&!rnocache.test(first)&&(jQuery.support.checkClone||!rchecked.test(first))&&(jQuery.support.html5Clone||!rnoshimcache.test(first))){cacheable=true;
fragment=jQuery.fragments[first];
cachehit=fragment!==undefined
}if(!fragment){fragment=context.createDocumentFragment();
jQuery.clean(args,context,fragment,scripts);
if(cacheable){jQuery.fragments[first]=cachehit&&fragment
}}return{fragment:fragment,cacheable:cacheable}
};
jQuery.fragments={};
jQuery.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(name,original){jQuery.fn[name]=function(selector){var elems,i=0,ret=[],insert=jQuery(selector),l=insert.length,parent=this.length===1&&this[0].parentNode;
if((parent==null||parent&&parent.nodeType===11&&parent.childNodes.length===1)&&l===1){insert[original](this[0]);
return this
}else{for(;
i<l;
i++){elems=(i>0?this.clone(true):this).get();
jQuery(insert[i])[original](elems);
ret=ret.concat(elems)
}return this.pushStack(ret,name,insert.selector)
}}
});
function getAll(elem){if(typeof elem.getElementsByTagName!=="undefined"){return elem.getElementsByTagName("*")
}else{if(typeof elem.querySelectorAll!=="undefined"){return elem.querySelectorAll("*")
}else{return[]
}}}function fixDefaultChecked(elem){if(rcheckableType.test(elem.type)){elem.defaultChecked=elem.checked
}}jQuery.extend({clone:function(elem,dataAndEvents,deepDataAndEvents){var srcElements,destElements,i,clone;
if(jQuery.support.html5Clone||jQuery.isXMLDoc(elem)||!rnoshimcache.test("<"+elem.nodeName+">")){clone=elem.cloneNode(true)
}else{fragmentDiv.innerHTML=elem.outerHTML;
fragmentDiv.removeChild(clone=fragmentDiv.firstChild)
}if((!jQuery.support.noCloneEvent||!jQuery.support.noCloneChecked)&&(elem.nodeType===1||elem.nodeType===11)&&!jQuery.isXMLDoc(elem)){cloneFixAttributes(elem,clone);
srcElements=getAll(elem);
destElements=getAll(clone);
for(i=0;
srcElements[i];
++i){if(destElements[i]){cloneFixAttributes(srcElements[i],destElements[i])
}}}if(dataAndEvents){cloneCopyEvent(elem,clone);
if(deepDataAndEvents){srcElements=getAll(elem);
destElements=getAll(clone);
for(i=0;
srcElements[i];
++i){cloneCopyEvent(srcElements[i],destElements[i])
}}}srcElements=destElements=null;
return clone
},clean:function(elems,context,fragment,scripts){var i,j,elem,tag,wrap,depth,div,hasBody,tbody,len,handleScript,jsTags,safe=context===document&&safeFragment,ret=[];
if(!context||typeof context.createDocumentFragment==="undefined"){context=document
}for(i=0;
(elem=elems[i])!=null;
i++){if(typeof elem==="number"){elem+=""
}if(!elem){continue
}if(typeof elem==="string"){if(!rhtml.test(elem)){elem=context.createTextNode(elem)
}else{safe=safe||createSafeFragment(context);
div=context.createElement("div");
safe.appendChild(div);
elem=elem.replace(rxhtmlTag,"<$1></$2>");
tag=(rtagName.exec(elem)||["",""])[1].toLowerCase();
wrap=wrapMap[tag]||wrapMap._default;
depth=wrap[0];
div.innerHTML=wrap[1]+elem+wrap[2];
while(depth--){div=div.lastChild
}if(!jQuery.support.tbody){hasBody=rtbody.test(elem);
tbody=tag==="table"&&!hasBody?div.firstChild&&div.firstChild.childNodes:wrap[1]==="<table>"&&!hasBody?div.childNodes:[];
for(j=tbody.length-1;
j>=0;
--j){if(jQuery.nodeName(tbody[j],"tbody")&&!tbody[j].childNodes.length){tbody[j].parentNode.removeChild(tbody[j])
}}}if(!jQuery.support.leadingWhitespace&&rleadingWhitespace.test(elem)){div.insertBefore(context.createTextNode(rleadingWhitespace.exec(elem)[0]),div.firstChild)
}elem=div.childNodes;
div.parentNode.removeChild(div)
}}if(elem.nodeType){ret.push(elem)
}else{jQuery.merge(ret,elem)
}}if(div){elem=div=safe=null
}if(!jQuery.support.appendChecked){for(i=0;
(elem=ret[i])!=null;
i++){if(jQuery.nodeName(elem,"input")){fixDefaultChecked(elem)
}else{if(typeof elem.getElementsByTagName!=="undefined"){jQuery.grep(elem.getElementsByTagName("input"),fixDefaultChecked)
}}}}if(fragment){handleScript=function(elem){if(!elem.type||rscriptType.test(elem.type)){return scripts?scripts.push(elem.parentNode?elem.parentNode.removeChild(elem):elem):fragment.appendChild(elem)
}};
for(i=0;
(elem=ret[i])!=null;
i++){if(!(jQuery.nodeName(elem,"script")&&handleScript(elem))){fragment.appendChild(elem);
if(typeof elem.getElementsByTagName!=="undefined"){jsTags=jQuery.grep(jQuery.merge([],elem.getElementsByTagName("script")),handleScript);
ret.splice.apply(ret,[i+1,0].concat(jsTags));
i+=jsTags.length
}}}}return ret
},cleanData:function(elems,acceptData){var data,id,elem,type,i=0,internalKey=jQuery.expando,cache=jQuery.cache,deleteExpando=jQuery.support.deleteExpando,special=jQuery.event.special;
for(;
(elem=elems[i])!=null;
i++){if(acceptData||jQuery.acceptData(elem)){id=elem[internalKey];
data=id&&cache[id];
if(data){if(data.events){for(type in data.events){if(special[type]){jQuery.event.remove(elem,type)
}else{jQuery.removeEvent(elem,type,data.handle)
}}}if(cache[id]){delete cache[id];
if(deleteExpando){delete elem[internalKey]
}else{if(elem.removeAttribute){elem.removeAttribute(internalKey)
}else{elem[internalKey]=null
}}jQuery.deletedIds.push(id)
}}}}}});
(function(){var matched,browser;
jQuery.uaMatch=function(ua){ua=ua.toLowerCase();
var match=/(chrome)[ \/]([\w.]+)/.exec(ua)||/(webkit)[ \/]([\w.]+)/.exec(ua)||/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua)||/(msie) ([\w.]+)/.exec(ua)||ua.indexOf("compatible")<0&&/(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua)||[];
return{browser:match[1]||"",version:match[2]||"0"}
};
matched=jQuery.uaMatch(navigator.userAgent);
browser={};
if(matched.browser){browser[matched.browser]=true;
browser.version=matched.version
}if(browser.chrome){browser.webkit=true
}else{if(browser.webkit){browser.safari=true
}}jQuery.browser=browser;
jQuery.sub=function(){function jQuerySub(selector,context){return new jQuerySub.fn.init(selector,context)
}jQuery.extend(true,jQuerySub,this);
jQuerySub.superclass=this;
jQuerySub.fn=jQuerySub.prototype=this();
jQuerySub.fn.constructor=jQuerySub;
jQuerySub.sub=this.sub;
jQuerySub.fn.init=function init(selector,context){if(context&&context instanceof jQuery&&!(context instanceof jQuerySub)){context=jQuerySub(context)
}return jQuery.fn.init.call(this,selector,context,rootjQuerySub)
};
jQuerySub.fn.init.prototype=jQuerySub.fn;
var rootjQuerySub=jQuerySub(document);
return jQuerySub
}
})();
var curCSS,iframe,iframeDoc,ralpha=/alpha\([^)]*\)/i,ropacity=/opacity=([^)]*)/,rposition=/^(top|right|bottom|left)$/,rdisplayswap=/^(none|table(?!-c[ea]).+)/,rmargin=/^margin/,rnumsplit=new RegExp("^("+core_pnum+")(.*)$","i"),rnumnonpx=new RegExp("^("+core_pnum+")(?!px)[a-z%]+$","i"),rrelNum=new RegExp("^([-+])=("+core_pnum+")","i"),elemdisplay={},cssShow={position:"absolute",visibility:"hidden",display:"block"},cssNormalTransform={letterSpacing:0,fontWeight:400},cssExpand=["Top","Right","Bottom","Left"],cssPrefixes=["Webkit","O","Moz","ms"],eventsToggle=jQuery.fn.toggle;
function vendorPropName(style,name){if(name in style){return name
}var capName=name.charAt(0).toUpperCase()+name.slice(1),origName=name,i=cssPrefixes.length;
while(i--){name=cssPrefixes[i]+capName;
if(name in style){return name
}}return origName
}function isHidden(elem,el){elem=el||elem;
return jQuery.css(elem,"display")==="none"||!jQuery.contains(elem.ownerDocument,elem)
}function showHide(elements,show){var elem,display,values=[],index=0,length=elements.length;
for(;
index<length;
index++){elem=elements[index];
if(!elem.style){continue
}values[index]=jQuery._data(elem,"olddisplay");
if(show){if(!values[index]&&elem.style.display==="none"){elem.style.display=""
}if(elem.style.display===""&&isHidden(elem)){values[index]=jQuery._data(elem,"olddisplay",css_defaultDisplay(elem.nodeName))
}}else{display=curCSS(elem,"display");
if(!values[index]&&display!=="none"){jQuery._data(elem,"olddisplay",display)
}}}for(index=0;
index<length;
index++){elem=elements[index];
if(!elem.style){continue
}if(!show||elem.style.display==="none"||elem.style.display===""){elem.style.display=show?values[index]||"":"none"
}}return elements
}jQuery.fn.extend({css:function(name,value){return jQuery.access(this,function(elem,name,value){return value!==undefined?jQuery.style(elem,name,value):jQuery.css(elem,name)
},name,value,arguments.length>1)
},show:function(){return showHide(this,true)
},hide:function(){return showHide(this)
},toggle:function(state,fn2){var bool=typeof state==="boolean";
if(jQuery.isFunction(state)&&jQuery.isFunction(fn2)){return eventsToggle.apply(this,arguments)
}return this.each(function(){if(bool?state:isHidden(this)){jQuery(this).show()
}else{jQuery(this).hide()
}})
}});
jQuery.extend({cssHooks:{opacity:{get:function(elem,computed){if(computed){var ret=curCSS(elem,"opacity");
return ret===""?"1":ret
}}}},cssNumber:{fillOpacity:true,fontWeight:true,lineHeight:true,opacity:true,orphans:true,widows:true,zIndex:true,zoom:true},cssProps:{"float":jQuery.support.cssFloat?"cssFloat":"styleFloat"},style:function(elem,name,value,extra){if(!elem||elem.nodeType===3||elem.nodeType===8||!elem.style){return 
}var ret,type,hooks,origName=jQuery.camelCase(name),style=elem.style;
name=jQuery.cssProps[origName]||(jQuery.cssProps[origName]=vendorPropName(style,origName));
hooks=jQuery.cssHooks[name]||jQuery.cssHooks[origName];
if(value!==undefined){type=typeof value;
if(type==="string"&&(ret=rrelNum.exec(value))){value=(ret[1]+1)*ret[2]+parseFloat(jQuery.css(elem,name));
type="number"
}if(value==null||type==="number"&&isNaN(value)){return 
}if(type==="number"&&!jQuery.cssNumber[origName]){value+="px"
}if(!hooks||!("set" in hooks)||(value=hooks.set(elem,value,extra))!==undefined){try{style[name]=value
}catch(e){}}}else{if(hooks&&"get" in hooks&&(ret=hooks.get(elem,false,extra))!==undefined){return ret
}return style[name]
}},css:function(elem,name,numeric,extra){var val,num,hooks,origName=jQuery.camelCase(name);
name=jQuery.cssProps[origName]||(jQuery.cssProps[origName]=vendorPropName(elem.style,origName));
hooks=jQuery.cssHooks[name]||jQuery.cssHooks[origName];
if(hooks&&"get" in hooks){val=hooks.get(elem,true,extra)
}if(val===undefined){val=curCSS(elem,name)
}if(val==="normal"&&name in cssNormalTransform){val=cssNormalTransform[name]
}if(numeric||extra!==undefined){num=parseFloat(val);
return numeric||jQuery.isNumeric(num)?num||0:val
}return val
},swap:function(elem,options,callback){var ret,name,old={};
for(name in options){old[name]=elem.style[name];
elem.style[name]=options[name]
}ret=callback.call(elem);
for(name in options){elem.style[name]=old[name]
}return ret
}});
if(window.getComputedStyle){curCSS=function(elem,name){var ret,width,minWidth,maxWidth,computed=window.getComputedStyle(elem,null),style=elem.style;
if(computed){ret=computed[name];
if(ret===""&&!jQuery.contains(elem.ownerDocument,elem)){ret=jQuery.style(elem,name)
}if(rnumnonpx.test(ret)&&rmargin.test(name)){width=style.width;
minWidth=style.minWidth;
maxWidth=style.maxWidth;
style.minWidth=style.maxWidth=style.width=ret;
ret=computed.width;
style.width=width;
style.minWidth=minWidth;
style.maxWidth=maxWidth
}}return ret
}
}else{if(document.documentElement.currentStyle){curCSS=function(elem,name){var left,rsLeft,ret=elem.currentStyle&&elem.currentStyle[name],style=elem.style;
if(ret==null&&style&&style[name]){ret=style[name]
}if(rnumnonpx.test(ret)&&!rposition.test(name)){left=style.left;
rsLeft=elem.runtimeStyle&&elem.runtimeStyle.left;
if(rsLeft){elem.runtimeStyle.left=elem.currentStyle.left
}style.left=name==="fontSize"?"1em":ret;
ret=style.pixelLeft+"px";
style.left=left;
if(rsLeft){elem.runtimeStyle.left=rsLeft
}}return ret===""?"auto":ret
}
}}function setPositiveNumber(elem,value,subtract){var matches=rnumsplit.exec(value);
return matches?Math.max(0,matches[1]-(subtract||0))+(matches[2]||"px"):value
}function augmentWidthOrHeight(elem,name,extra,isBorderBox){var i=extra===(isBorderBox?"border":"content")?4:name==="width"?1:0,val=0;
for(;
i<4;
i+=2){if(extra==="margin"){val+=jQuery.css(elem,extra+cssExpand[i],true)
}if(isBorderBox){if(extra==="content"){val-=parseFloat(curCSS(elem,"padding"+cssExpand[i]))||0
}if(extra!=="margin"){val-=parseFloat(curCSS(elem,"border"+cssExpand[i]+"Width"))||0
}}else{val+=parseFloat(curCSS(elem,"padding"+cssExpand[i]))||0;
if(extra!=="padding"){val+=parseFloat(curCSS(elem,"border"+cssExpand[i]+"Width"))||0
}}}return val
}function getWidthOrHeight(elem,name,extra){var val=name==="width"?elem.offsetWidth:elem.offsetHeight,valueIsBorderBox=true,isBorderBox=jQuery.support.boxSizing&&jQuery.css(elem,"boxSizing")==="border-box";
if(val<=0||val==null){val=curCSS(elem,name);
if(val<0||val==null){val=elem.style[name]
}if(rnumnonpx.test(val)){return val
}valueIsBorderBox=isBorderBox&&(jQuery.support.boxSizingReliable||val===elem.style[name]);
val=parseFloat(val)||0
}return(val+augmentWidthOrHeight(elem,name,extra||(isBorderBox?"border":"content"),valueIsBorderBox))+"px"
}function css_defaultDisplay(nodeName){if(elemdisplay[nodeName]){return elemdisplay[nodeName]
}var elem=jQuery("<"+nodeName+">").appendTo(document.body),display=elem.css("display");
elem.remove();
if(display==="none"||display===""){iframe=document.body.appendChild(iframe||jQuery.extend(document.createElement("iframe"),{frameBorder:0,width:0,height:0}));
if(!iframeDoc||!iframe.createElement){iframeDoc=(iframe.contentWindow||iframe.contentDocument).document;
iframeDoc.write("<!doctype html><html><body>");
iframeDoc.close()
}elem=iframeDoc.body.appendChild(iframeDoc.createElement(nodeName));
display=curCSS(elem,"display");
document.body.removeChild(iframe)
}elemdisplay[nodeName]=display;
return display
}jQuery.each(["height","width"],function(i,name){jQuery.cssHooks[name]={get:function(elem,computed,extra){if(computed){if(elem.offsetWidth===0&&rdisplayswap.test(curCSS(elem,"display"))){return jQuery.swap(elem,cssShow,function(){return getWidthOrHeight(elem,name,extra)
})
}else{return getWidthOrHeight(elem,name,extra)
}}},set:function(elem,value,extra){return setPositiveNumber(elem,value,extra?augmentWidthOrHeight(elem,name,extra,jQuery.support.boxSizing&&jQuery.css(elem,"boxSizing")==="border-box"):0)
}}
});
if(!jQuery.support.opacity){jQuery.cssHooks.opacity={get:function(elem,computed){return ropacity.test((computed&&elem.currentStyle?elem.currentStyle.filter:elem.style.filter)||"")?(0.01*parseFloat(RegExp.$1))+"":computed?"1":""
},set:function(elem,value){var style=elem.style,currentStyle=elem.currentStyle,opacity=jQuery.isNumeric(value)?"alpha(opacity="+value*100+")":"",filter=currentStyle&&currentStyle.filter||style.filter||"";
style.zoom=1;
if(value>=1&&jQuery.trim(filter.replace(ralpha,""))===""&&style.removeAttribute){style.removeAttribute("filter");
if(currentStyle&&!currentStyle.filter){return 
}}style.filter=ralpha.test(filter)?filter.replace(ralpha,opacity):filter+" "+opacity
}}
}jQuery(function(){if(!jQuery.support.reliableMarginRight){jQuery.cssHooks.marginRight={get:function(elem,computed){return jQuery.swap(elem,{display:"inline-block"},function(){if(computed){return curCSS(elem,"marginRight")
}})
}}
}if(!jQuery.support.pixelPosition&&jQuery.fn.position){jQuery.each(["top","left"],function(i,prop){jQuery.cssHooks[prop]={get:function(elem,computed){if(computed){var ret=curCSS(elem,prop);
return rnumnonpx.test(ret)?jQuery(elem).position()[prop]+"px":ret
}}}
})
}});
if(jQuery.expr&&jQuery.expr.filters){jQuery.expr.filters.hidden=function(elem){return(elem.offsetWidth===0&&elem.offsetHeight===0)||(!jQuery.support.reliableHiddenOffsets&&((elem.style&&elem.style.display)||curCSS(elem,"display"))==="none")
};
jQuery.expr.filters.visible=function(elem){return !jQuery.expr.filters.hidden(elem)
}
}jQuery.each({margin:"",padding:"",border:"Width"},function(prefix,suffix){jQuery.cssHooks[prefix+suffix]={expand:function(value){var i,parts=typeof value==="string"?value.split(" "):[value],expanded={};
for(i=0;
i<4;
i++){expanded[prefix+cssExpand[i]+suffix]=parts[i]||parts[i-2]||parts[0]
}return expanded
}};
if(!rmargin.test(prefix)){jQuery.cssHooks[prefix+suffix].set=setPositiveNumber
}});
var r20=/%20/g,rbracket=/\[\]$/,rCRLF=/\r?\n/g,rinput=/^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,rselectTextarea=/^(?:select|textarea)/i;
jQuery.fn.extend({serialize:function(){return jQuery.param(this.serializeArray())
},serializeArray:function(){return this.map(function(){return this.elements?jQuery.makeArray(this.elements):this
}).filter(function(){return this.name&&!this.disabled&&(this.checked||rselectTextarea.test(this.nodeName)||rinput.test(this.type))
}).map(function(i,elem){var val=jQuery(this).val();
return val==null?null:jQuery.isArray(val)?jQuery.map(val,function(val,i){return{name:elem.name,value:val.replace(rCRLF,"\r\n")}
}):{name:elem.name,value:val.replace(rCRLF,"\r\n")}
}).get()
}});
jQuery.param=function(a,traditional){var prefix,s=[],add=function(key,value){value=jQuery.isFunction(value)?value():(value==null?"":value);
s[s.length]=encodeURIComponent(key)+"="+encodeURIComponent(value)
};
if(traditional===undefined){traditional=jQuery.ajaxSettings&&jQuery.ajaxSettings.traditional
}if(jQuery.isArray(a)||(a.jquery&&!jQuery.isPlainObject(a))){jQuery.each(a,function(){add(this.name,this.value)
})
}else{for(prefix in a){buildParams(prefix,a[prefix],traditional,add)
}}return s.join("&").replace(r20,"+")
};
function buildParams(prefix,obj,traditional,add){var name;
if(jQuery.isArray(obj)){jQuery.each(obj,function(i,v){if(traditional||rbracket.test(prefix)){add(prefix,v)
}else{buildParams(prefix+"["+(typeof v==="object"?i:"")+"]",v,traditional,add)
}})
}else{if(!traditional&&jQuery.type(obj)==="object"){for(name in obj){buildParams(prefix+"["+name+"]",obj[name],traditional,add)
}}else{add(prefix,obj)
}}}var ajaxLocParts,ajaxLocation,rhash=/#.*$/,rheaders=/^(.*?):[ \t]*([^\r\n]*)\r?$/mg,rlocalProtocol=/^(?:about|app|app\-storage|.+\-extension|file|res|widget):$/,rnoContent=/^(?:GET|HEAD)$/,rprotocol=/^\/\//,rquery=/\?/,rscript=/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,rts=/([?&])_=[^&]*/,rurl=/^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,_load=jQuery.fn.load,prefilters={},transports={},allTypes=["*/"]+["*"];
try{ajaxLocation=location.href
}catch(e){ajaxLocation=document.createElement("a");
ajaxLocation.href="";
ajaxLocation=ajaxLocation.href
}ajaxLocParts=rurl.exec(ajaxLocation.toLowerCase())||[];
function addToPrefiltersOrTransports(structure){return function(dataTypeExpression,func){if(typeof dataTypeExpression!=="string"){func=dataTypeExpression;
dataTypeExpression="*"
}var dataType,list,placeBefore,dataTypes=dataTypeExpression.toLowerCase().split(core_rspace),i=0,length=dataTypes.length;
if(jQuery.isFunction(func)){for(;
i<length;
i++){dataType=dataTypes[i];
placeBefore=/^\+/.test(dataType);
if(placeBefore){dataType=dataType.substr(1)||"*"
}list=structure[dataType]=structure[dataType]||[];
list[placeBefore?"unshift":"push"](func)
}}}
}function inspectPrefiltersOrTransports(structure,options,originalOptions,jqXHR,dataType,inspected){dataType=dataType||options.dataTypes[0];
inspected=inspected||{};
inspected[dataType]=true;
var selection,list=structure[dataType],i=0,length=list?list.length:0,executeOnly=(structure===prefilters);
for(;
i<length&&(executeOnly||!selection);
i++){selection=list[i](options,originalOptions,jqXHR);
if(typeof selection==="string"){if(!executeOnly||inspected[selection]){selection=undefined
}else{options.dataTypes.unshift(selection);
selection=inspectPrefiltersOrTransports(structure,options,originalOptions,jqXHR,selection,inspected)
}}}if((executeOnly||!selection)&&!inspected["*"]){selection=inspectPrefiltersOrTransports(structure,options,originalOptions,jqXHR,"*",inspected)
}return selection
}function ajaxExtend(target,src){var key,deep,flatOptions=jQuery.ajaxSettings.flatOptions||{};
for(key in src){if(src[key]!==undefined){(flatOptions[key]?target:(deep||(deep={})))[key]=src[key]
}}if(deep){jQuery.extend(true,target,deep)
}}jQuery.fn.load=function(url,params,callback){if(typeof url!=="string"&&_load){return _load.apply(this,arguments)
}if(!this.length){return this
}var selector,type,response,self=this,off=url.indexOf(" ");
if(off>=0){selector=url.slice(off,url.length);
url=url.slice(0,off)
}if(jQuery.isFunction(params)){callback=params;
params=undefined
}else{if(params&&typeof params==="object"){type="POST"
}}jQuery.ajax({url:url,type:type,dataType:"html",data:params,complete:function(jqXHR,status){if(callback){self.each(callback,response||[jqXHR.responseText,status,jqXHR])
}}}).done(function(responseText){response=arguments;
self.html(selector?jQuery("<div>").append(responseText.replace(rscript,"")).find(selector):responseText)
});
return this
};
jQuery.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "),function(i,o){jQuery.fn[o]=function(f){return this.on(o,f)
}
});
jQuery.each(["get","post"],function(i,method){jQuery[method]=function(url,data,callback,type){if(jQuery.isFunction(data)){type=type||callback;
callback=data;
data=undefined
}return jQuery.ajax({type:method,url:url,data:data,success:callback,dataType:type})
}
});
jQuery.extend({getScript:function(url,callback){return jQuery.get(url,undefined,callback,"script")
},getJSON:function(url,data,callback){return jQuery.get(url,data,callback,"json")
},ajaxSetup:function(target,settings){if(settings){ajaxExtend(target,jQuery.ajaxSettings)
}else{settings=target;
target=jQuery.ajaxSettings
}ajaxExtend(target,settings);
return target
},ajaxSettings:{url:ajaxLocation,isLocal:rlocalProtocol.test(ajaxLocParts[1]),global:true,type:"GET",contentType:"application/x-www-form-urlencoded; charset=UTF-8",processData:true,async:true,accepts:{xml:"application/xml, text/xml",html:"text/html",text:"text/plain",json:"application/json, text/javascript","*":allTypes},contents:{xml:/xml/,html:/html/,json:/json/},responseFields:{xml:"responseXML",text:"responseText"},converters:{"* text":window.String,"text html":true,"text json":jQuery.parseJSON,"text xml":jQuery.parseXML},flatOptions:{context:true,url:true}},ajaxPrefilter:addToPrefiltersOrTransports(prefilters),ajaxTransport:addToPrefiltersOrTransports(transports),ajax:function(url,options){if(typeof url==="object"){options=url;
url=undefined
}options=options||{};
var ifModifiedKey,responseHeadersString,responseHeaders,transport,timeoutTimer,parts,fireGlobals,i,s=jQuery.ajaxSetup({},options),callbackContext=s.context||s,globalEventContext=callbackContext!==s&&(callbackContext.nodeType||callbackContext instanceof jQuery)?jQuery(callbackContext):jQuery.event,deferred=jQuery.Deferred(),completeDeferred=jQuery.Callbacks("once memory"),statusCode=s.statusCode||{},requestHeaders={},requestHeadersNames={},state=0,strAbort="canceled",jqXHR={readyState:0,setRequestHeader:function(name,value){if(!state){var lname=name.toLowerCase();
name=requestHeadersNames[lname]=requestHeadersNames[lname]||name;
requestHeaders[name]=value
}return this
},getAllResponseHeaders:function(){return state===2?responseHeadersString:null
},getResponseHeader:function(key){var match;
if(state===2){if(!responseHeaders){responseHeaders={};
while((match=rheaders.exec(responseHeadersString))){responseHeaders[match[1].toLowerCase()]=match[2]
}}match=responseHeaders[key.toLowerCase()]
}return match===undefined?null:match
},overrideMimeType:function(type){if(!state){s.mimeType=type
}return this
},abort:function(statusText){statusText=statusText||strAbort;
if(transport){transport.abort(statusText)
}done(0,statusText);
return this
}};
function done(status,nativeStatusText,responses,headers){var isSuccess,success,error,response,modified,statusText=nativeStatusText;
if(state===2){return 
}state=2;
if(timeoutTimer){clearTimeout(timeoutTimer)
}transport=undefined;
responseHeadersString=headers||"";
jqXHR.readyState=status>0?4:0;
if(responses){response=ajaxHandleResponses(s,jqXHR,responses)
}if(status>=200&&status<300||status===304){if(s.ifModified){modified=jqXHR.getResponseHeader("Last-Modified");
if(modified){jQuery.lastModified[ifModifiedKey]=modified
}modified=jqXHR.getResponseHeader("Etag");
if(modified){jQuery.etag[ifModifiedKey]=modified
}}if(status===304){statusText="notmodified";
isSuccess=true
}else{isSuccess=ajaxConvert(s,response);
statusText=isSuccess.state;
success=isSuccess.data;
error=isSuccess.error;
isSuccess=!error
}}else{error=statusText;
if(!statusText||status){statusText="error";
if(status<0){status=0
}}}jqXHR.status=status;
jqXHR.statusText=(nativeStatusText||statusText)+"";
if(isSuccess){deferred.resolveWith(callbackContext,[success,statusText,jqXHR])
}else{deferred.rejectWith(callbackContext,[jqXHR,statusText,error])
}jqXHR.statusCode(statusCode);
statusCode=undefined;
if(fireGlobals){globalEventContext.trigger("ajax"+(isSuccess?"Success":"Error"),[jqXHR,s,isSuccess?success:error])
}completeDeferred.fireWith(callbackContext,[jqXHR,statusText]);
if(fireGlobals){globalEventContext.trigger("ajaxComplete",[jqXHR,s]);
if(!(--jQuery.active)){jQuery.event.trigger("ajaxStop")
}}}deferred.promise(jqXHR);
jqXHR.success=jqXHR.done;
jqXHR.error=jqXHR.fail;
jqXHR.complete=completeDeferred.add;
jqXHR.statusCode=function(map){if(map){var tmp;
if(state<2){for(tmp in map){statusCode[tmp]=[statusCode[tmp],map[tmp]]
}}else{tmp=map[jqXHR.status];
jqXHR.always(tmp)
}}return this
};
s.url=((url||s.url)+"").replace(rhash,"").replace(rprotocol,ajaxLocParts[1]+"//");
s.dataTypes=jQuery.trim(s.dataType||"*").toLowerCase().split(core_rspace);
if(s.crossDomain==null){parts=rurl.exec(s.url.toLowerCase())||false;
s.crossDomain=parts&&(parts.join(":")+(parts[3]?"":parts[1]==="http:"?80:443))!==(ajaxLocParts.join(":")+(ajaxLocParts[3]?"":ajaxLocParts[1]==="http:"?80:443))
}if(s.data&&s.processData&&typeof s.data!=="string"){s.data=jQuery.param(s.data,s.traditional)
}inspectPrefiltersOrTransports(prefilters,s,options,jqXHR);
if(state===2){return jqXHR
}fireGlobals=s.global;
s.type=s.type.toUpperCase();
s.hasContent=!rnoContent.test(s.type);
if(fireGlobals&&jQuery.active++===0){jQuery.event.trigger("ajaxStart")
}if(!s.hasContent){if(s.data){s.url+=(rquery.test(s.url)?"&":"?")+s.data;
delete s.data
}ifModifiedKey=s.url;
if(s.cache===false){var ts=jQuery.now(),ret=s.url.replace(rts,"$1_="+ts);
s.url=ret+((ret===s.url)?(rquery.test(s.url)?"&":"?")+"_="+ts:"")
}}if(s.data&&s.hasContent&&s.contentType!==false||options.contentType){jqXHR.setRequestHeader("Content-Type",s.contentType)
}if(s.ifModified){ifModifiedKey=ifModifiedKey||s.url;
if(jQuery.lastModified[ifModifiedKey]){jqXHR.setRequestHeader("If-Modified-Since",jQuery.lastModified[ifModifiedKey])
}if(jQuery.etag[ifModifiedKey]){jqXHR.setRequestHeader("If-None-Match",jQuery.etag[ifModifiedKey])
}}jqXHR.setRequestHeader("Accept",s.dataTypes[0]&&s.accepts[s.dataTypes[0]]?s.accepts[s.dataTypes[0]]+(s.dataTypes[0]!=="*"?", "+allTypes+"; q=0.01":""):s.accepts["*"]);
for(i in s.headers){jqXHR.setRequestHeader(i,s.headers[i])
}if(s.beforeSend&&(s.beforeSend.call(callbackContext,jqXHR,s)===false||state===2)){return jqXHR.abort()
}strAbort="abort";
for(i in {success:1,error:1,complete:1}){jqXHR[i](s[i])
}transport=inspectPrefiltersOrTransports(transports,s,options,jqXHR);
if(!transport){done(-1,"No Transport")
}else{jqXHR.readyState=1;
if(fireGlobals){globalEventContext.trigger("ajaxSend",[jqXHR,s])
}if(s.async&&s.timeout>0){timeoutTimer=setTimeout(function(){jqXHR.abort("timeout")
},s.timeout)
}try{state=1;
transport.send(requestHeaders,done)
}catch(e){if(state<2){done(-1,e)
}else{throw e
}}}return jqXHR
},active:0,lastModified:{},etag:{}});
function ajaxHandleResponses(s,jqXHR,responses){var ct,type,finalDataType,firstDataType,contents=s.contents,dataTypes=s.dataTypes,responseFields=s.responseFields;
for(type in responseFields){if(type in responses){jqXHR[responseFields[type]]=responses[type]
}}while(dataTypes[0]==="*"){dataTypes.shift();
if(ct===undefined){ct=s.mimeType||jqXHR.getResponseHeader("content-type")
}}if(ct){for(type in contents){if(contents[type]&&contents[type].test(ct)){dataTypes.unshift(type);
break
}}}if(dataTypes[0] in responses){finalDataType=dataTypes[0]
}else{for(type in responses){if(!dataTypes[0]||s.converters[type+" "+dataTypes[0]]){finalDataType=type;
break
}if(!firstDataType){firstDataType=type
}}finalDataType=finalDataType||firstDataType
}if(finalDataType){if(finalDataType!==dataTypes[0]){dataTypes.unshift(finalDataType)
}return responses[finalDataType]
}}function ajaxConvert(s,response){var conv,conv2,current,tmp,dataTypes=s.dataTypes.slice(),prev=dataTypes[0],converters={},i=0;
if(s.dataFilter){response=s.dataFilter(response,s.dataType)
}if(dataTypes[1]){for(conv in s.converters){converters[conv.toLowerCase()]=s.converters[conv]
}}for(;
(current=dataTypes[++i]);
){if(current!=="*"){if(prev!=="*"&&prev!==current){conv=converters[prev+" "+current]||converters["* "+current];
if(!conv){for(conv2 in converters){tmp=conv2.split(" ");
if(tmp[1]===current){conv=converters[prev+" "+tmp[0]]||converters["* "+tmp[0]];
if(conv){if(conv===true){conv=converters[conv2]
}else{if(converters[conv2]!==true){current=tmp[0];
dataTypes.splice(i--,0,current)
}}break
}}}}if(conv!==true){if(conv&&s["throws"]){response=conv(response)
}else{try{response=conv(response)
}catch(e){return{state:"parsererror",error:conv?e:"No conversion from "+prev+" to "+current}
}}}}prev=current
}}return{state:"success",data:response}
}var oldCallbacks=[],rquestion=/\?/,rjsonp=/(=)\?(?=&|$)|\?\?/,nonce=jQuery.now();
jQuery.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var callback=oldCallbacks.pop()||(jQuery.expando+"_"+(nonce++));
this[callback]=true;
return callback
}});
jQuery.ajaxPrefilter("json jsonp",function(s,originalSettings,jqXHR){var callbackName,overwritten,responseContainer,data=s.data,url=s.url,hasCallback=s.jsonp!==false,replaceInUrl=hasCallback&&rjsonp.test(url),replaceInData=hasCallback&&!replaceInUrl&&typeof data==="string"&&!(s.contentType||"").indexOf("application/x-www-form-urlencoded")&&rjsonp.test(data);
if(s.dataTypes[0]==="jsonp"||replaceInUrl||replaceInData){callbackName=s.jsonpCallback=jQuery.isFunction(s.jsonpCallback)?s.jsonpCallback():s.jsonpCallback;
overwritten=window[callbackName];
if(replaceInUrl){s.url=url.replace(rjsonp,"$1"+callbackName)
}else{if(replaceInData){s.data=data.replace(rjsonp,"$1"+callbackName)
}else{if(hasCallback){s.url+=(rquestion.test(url)?"&":"?")+s.jsonp+"="+callbackName
}}}s.converters["script json"]=function(){if(!responseContainer){jQuery.error(callbackName+" was not called")
}return responseContainer[0]
};
s.dataTypes[0]="json";
window[callbackName]=function(){responseContainer=arguments
};
jqXHR.always(function(){window[callbackName]=overwritten;
if(s[callbackName]){s.jsonpCallback=originalSettings.jsonpCallback;
oldCallbacks.push(callbackName)
}if(responseContainer&&jQuery.isFunction(overwritten)){overwritten(responseContainer[0])
}responseContainer=overwritten=undefined
});
return"script"
}});
jQuery.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/javascript|ecmascript/},converters:{"text script":function(text){jQuery.globalEval(text);
return text
}}});
jQuery.ajaxPrefilter("script",function(s){if(s.cache===undefined){s.cache=false
}if(s.crossDomain){s.type="GET";
s.global=false
}});
jQuery.ajaxTransport("script",function(s){if(s.crossDomain){var script,head=document.head||document.getElementsByTagName("head")[0]||document.documentElement;
return{send:function(_,callback){script=document.createElement("script");
script.async="async";
if(s.scriptCharset){script.charset=s.scriptCharset
}script.src=s.url;
script.onload=script.onreadystatechange=function(_,isAbort){if(isAbort||!script.readyState||/loaded|complete/.test(script.readyState)){script.onload=script.onreadystatechange=null;
if(head&&script.parentNode){head.removeChild(script)
}script=undefined;
if(!isAbort){callback(200,"success")
}}};
head.insertBefore(script,head.firstChild)
},abort:function(){if(script){script.onload(0,1)
}}}
}});
var xhrCallbacks,xhrOnUnloadAbort=window.ActiveXObject?function(){for(var key in xhrCallbacks){xhrCallbacks[key](0,1)
}}:false,xhrId=0;
function createStandardXHR(){try{return new window.XMLHttpRequest()
}catch(e){}}function createActiveXHR(){try{return new window.ActiveXObject("Microsoft.XMLHTTP")
}catch(e){}}jQuery.ajaxSettings.xhr=window.ActiveXObject?function(){return !this.isLocal&&createStandardXHR()||createActiveXHR()
}:createStandardXHR;
(function(xhr){jQuery.extend(jQuery.support,{ajax:!!xhr,cors:!!xhr&&("withCredentials" in xhr)})
})(jQuery.ajaxSettings.xhr());
if(jQuery.support.ajax){jQuery.ajaxTransport(function(s){if(!s.crossDomain||jQuery.support.cors){var callback;
return{send:function(headers,complete){var handle,i,xhr=s.xhr();
if(s.username){xhr.open(s.type,s.url,s.async,s.username,s.password)
}else{xhr.open(s.type,s.url,s.async)
}if(s.xhrFields){for(i in s.xhrFields){xhr[i]=s.xhrFields[i]
}}if(s.mimeType&&xhr.overrideMimeType){xhr.overrideMimeType(s.mimeType)
}if(!s.crossDomain&&!headers["X-Requested-With"]){headers["X-Requested-With"]="XMLHttpRequest"
}try{for(i in headers){xhr.setRequestHeader(i,headers[i])
}}catch(_){}xhr.send((s.hasContent&&s.data)||null);
callback=function(_,isAbort){var status,statusText,responseHeaders,responses,xml;
try{if(callback&&(isAbort||xhr.readyState===4)){callback=undefined;
if(handle){xhr.onreadystatechange=jQuery.noop;
if(xhrOnUnloadAbort){delete xhrCallbacks[handle]
}}if(isAbort){if(xhr.readyState!==4){xhr.abort()
}}else{status=xhr.status;
responseHeaders=xhr.getAllResponseHeaders();
responses={};
xml=xhr.responseXML;
if(xml&&xml.documentElement){responses.xml=xml
}try{responses.text=xhr.responseText
}catch(_){}try{statusText=xhr.statusText
}catch(e){statusText=""
}if(!status&&s.isLocal&&!s.crossDomain){status=responses.text?200:404
}else{if(status===1223){status=204
}}}}}catch(firefoxAccessException){if(!isAbort){complete(-1,firefoxAccessException)
}}if(responses){complete(status,statusText,responses,responseHeaders)
}};
if(!s.async){callback()
}else{if(xhr.readyState===4){setTimeout(callback,0)
}else{handle=++xhrId;
if(xhrOnUnloadAbort){if(!xhrCallbacks){xhrCallbacks={};
jQuery(window).unload(xhrOnUnloadAbort)
}xhrCallbacks[handle]=callback
}xhr.onreadystatechange=callback
}}},abort:function(){if(callback){callback(0,1)
}}}
}})
}var fxNow,timerId,rfxtypes=/^(?:toggle|show|hide)$/,rfxnum=new RegExp("^(?:([-+])=|)("+core_pnum+")([a-z%]*)$","i"),rrun=/queueHooks$/,animationPrefilters=[defaultPrefilter],tweeners={"*":[function(prop,value){var end,unit,tween=this.createTween(prop,value),parts=rfxnum.exec(value),target=tween.cur(),start=+target||0,scale=1,maxIterations=20;
if(parts){end=+parts[2];
unit=parts[3]||(jQuery.cssNumber[prop]?"":"px");
if(unit!=="px"&&start){start=jQuery.css(tween.elem,prop,true)||end||1;
do{scale=scale||".5";
start=start/scale;
jQuery.style(tween.elem,prop,start+unit)
}while(scale!==(scale=tween.cur()/target)&&scale!==1&&--maxIterations)
}tween.unit=unit;
tween.start=start;
tween.end=parts[1]?start+(parts[1]+1)*end:end
}return tween
}]};
function createFxNow(){setTimeout(function(){fxNow=undefined
},0);
return(fxNow=jQuery.now())
}function createTweens(animation,props){jQuery.each(props,function(prop,value){var collection=(tweeners[prop]||[]).concat(tweeners["*"]),index=0,length=collection.length;
for(;
index<length;
index++){if(collection[index].call(animation,prop,value)){return 
}}})
}function Animation(elem,properties,options){var result,index=0,tweenerIndex=0,length=animationPrefilters.length,deferred=jQuery.Deferred().always(function(){delete tick.elem
}),tick=function(){var currentTime=fxNow||createFxNow(),remaining=Math.max(0,animation.startTime+animation.duration-currentTime),percent=1-(remaining/animation.duration||0),index=0,length=animation.tweens.length;
for(;
index<length;
index++){animation.tweens[index].run(percent)
}deferred.notifyWith(elem,[animation,percent,remaining]);
if(percent<1&&length){return remaining
}else{deferred.resolveWith(elem,[animation]);
return false
}},animation=deferred.promise({elem:elem,props:jQuery.extend({},properties),opts:jQuery.extend(true,{specialEasing:{}},options),originalProperties:properties,originalOptions:options,startTime:fxNow||createFxNow(),duration:options.duration,tweens:[],createTween:function(prop,end,easing){var tween=jQuery.Tween(elem,animation.opts,prop,end,animation.opts.specialEasing[prop]||animation.opts.easing);
animation.tweens.push(tween);
return tween
},stop:function(gotoEnd){var index=0,length=gotoEnd?animation.tweens.length:0;
for(;
index<length;
index++){animation.tweens[index].run(1)
}if(gotoEnd){deferred.resolveWith(elem,[animation,gotoEnd])
}else{deferred.rejectWith(elem,[animation,gotoEnd])
}return this
}}),props=animation.props;
propFilter(props,animation.opts.specialEasing);
for(;
index<length;
index++){result=animationPrefilters[index].call(animation,elem,props,animation.opts);
if(result){return result
}}createTweens(animation,props);
if(jQuery.isFunction(animation.opts.start)){animation.opts.start.call(elem,animation)
}jQuery.fx.timer(jQuery.extend(tick,{anim:animation,queue:animation.opts.queue,elem:elem}));
return animation.progress(animation.opts.progress).done(animation.opts.done,animation.opts.complete).fail(animation.opts.fail).always(animation.opts.always)
}function propFilter(props,specialEasing){var index,name,easing,value,hooks;
for(index in props){name=jQuery.camelCase(index);
easing=specialEasing[name];
value=props[index];
if(jQuery.isArray(value)){easing=value[1];
value=props[index]=value[0]
}if(index!==name){props[name]=value;
delete props[index]
}hooks=jQuery.cssHooks[name];
if(hooks&&"expand" in hooks){value=hooks.expand(value);
delete props[name];
for(index in value){if(!(index in props)){props[index]=value[index];
specialEasing[index]=easing
}}}else{specialEasing[name]=easing
}}}jQuery.Animation=jQuery.extend(Animation,{tweener:function(props,callback){if(jQuery.isFunction(props)){callback=props;
props=["*"]
}else{props=props.split(" ")
}var prop,index=0,length=props.length;
for(;
index<length;
index++){prop=props[index];
tweeners[prop]=tweeners[prop]||[];
tweeners[prop].unshift(callback)
}},prefilter:function(callback,prepend){if(prepend){animationPrefilters.unshift(callback)
}else{animationPrefilters.push(callback)
}}});
function defaultPrefilter(elem,props,opts){var index,prop,value,length,dataShow,tween,hooks,oldfire,anim=this,style=elem.style,orig={},handled=[],hidden=elem.nodeType&&isHidden(elem);
if(!opts.queue){hooks=jQuery._queueHooks(elem,"fx");
if(hooks.unqueued==null){hooks.unqueued=0;
oldfire=hooks.empty.fire;
hooks.empty.fire=function(){if(!hooks.unqueued){oldfire()
}}
}hooks.unqueued++;
anim.always(function(){anim.always(function(){hooks.unqueued--;
if(!jQuery.queue(elem,"fx").length){hooks.empty.fire()
}})
})
}if(elem.nodeType===1&&("height" in props||"width" in props)){opts.overflow=[style.overflow,style.overflowX,style.overflowY];
if(jQuery.css(elem,"display")==="inline"&&jQuery.css(elem,"float")==="none"){if(!jQuery.support.inlineBlockNeedsLayout||css_defaultDisplay(elem.nodeName)==="inline"){style.display="inline-block"
}else{style.zoom=1
}}}if(opts.overflow){style.overflow="hidden";
if(!jQuery.support.shrinkWrapBlocks){anim.done(function(){style.overflow=opts.overflow[0];
style.overflowX=opts.overflow[1];
style.overflowY=opts.overflow[2]
})
}}for(index in props){value=props[index];
if(rfxtypes.exec(value)){delete props[index];
if(value===(hidden?"hide":"show")){continue
}handled.push(index)
}}length=handled.length;
if(length){dataShow=jQuery._data(elem,"fxshow")||jQuery._data(elem,"fxshow",{});
if(hidden){jQuery(elem).show()
}else{anim.done(function(){jQuery(elem).hide()
})
}anim.done(function(){var prop;
jQuery.removeData(elem,"fxshow",true);
for(prop in orig){jQuery.style(elem,prop,orig[prop])
}});
for(index=0;
index<length;
index++){prop=handled[index];
tween=anim.createTween(prop,hidden?dataShow[prop]:0);
orig[prop]=dataShow[prop]||jQuery.style(elem,prop);
if(!(prop in dataShow)){dataShow[prop]=tween.start;
if(hidden){tween.end=tween.start;
tween.start=prop==="width"||prop==="height"?1:0
}}}}}function Tween(elem,options,prop,end,easing){return new Tween.prototype.init(elem,options,prop,end,easing)
}jQuery.Tween=Tween;
Tween.prototype={constructor:Tween,init:function(elem,options,prop,end,easing,unit){this.elem=elem;
this.prop=prop;
this.easing=easing||"swing";
this.options=options;
this.start=this.now=this.cur();
this.end=end;
this.unit=unit||(jQuery.cssNumber[prop]?"":"px")
},cur:function(){var hooks=Tween.propHooks[this.prop];
return hooks&&hooks.get?hooks.get(this):Tween.propHooks._default.get(this)
},run:function(percent){var eased,hooks=Tween.propHooks[this.prop];
if(this.options.duration){this.pos=eased=jQuery.easing[this.easing](percent,this.options.duration*percent,0,1,this.options.duration)
}else{this.pos=eased=percent
}this.now=(this.end-this.start)*eased+this.start;
if(this.options.step){this.options.step.call(this.elem,this.now,this)
}if(hooks&&hooks.set){hooks.set(this)
}else{Tween.propHooks._default.set(this)
}return this
}};
Tween.prototype.init.prototype=Tween.prototype;
Tween.propHooks={_default:{get:function(tween){var result;
if(tween.elem[tween.prop]!=null&&(!tween.elem.style||tween.elem.style[tween.prop]==null)){return tween.elem[tween.prop]
}result=jQuery.css(tween.elem,tween.prop,false,"");
return !result||result==="auto"?0:result
},set:function(tween){if(jQuery.fx.step[tween.prop]){jQuery.fx.step[tween.prop](tween)
}else{if(tween.elem.style&&(tween.elem.style[jQuery.cssProps[tween.prop]]!=null||jQuery.cssHooks[tween.prop])){jQuery.style(tween.elem,tween.prop,tween.now+tween.unit)
}else{tween.elem[tween.prop]=tween.now
}}}}};
Tween.propHooks.scrollTop=Tween.propHooks.scrollLeft={set:function(tween){if(tween.elem.nodeType&&tween.elem.parentNode){tween.elem[tween.prop]=tween.now
}}};
jQuery.each(["toggle","show","hide"],function(i,name){var cssFn=jQuery.fn[name];
jQuery.fn[name]=function(speed,easing,callback){return speed==null||typeof speed==="boolean"||(!i&&jQuery.isFunction(speed)&&jQuery.isFunction(easing))?cssFn.apply(this,arguments):this.animate(genFx(name,true),speed,easing,callback)
}
});
jQuery.fn.extend({fadeTo:function(speed,to,easing,callback){return this.filter(isHidden).css("opacity",0).show().end().animate({opacity:to},speed,easing,callback)
},animate:function(prop,speed,easing,callback){var empty=jQuery.isEmptyObject(prop),optall=jQuery.speed(speed,easing,callback),doAnimation=function(){var anim=Animation(this,jQuery.extend({},prop),optall);
if(empty){anim.stop(true)
}};
return empty||optall.queue===false?this.each(doAnimation):this.queue(optall.queue,doAnimation)
},stop:function(type,clearQueue,gotoEnd){var stopQueue=function(hooks){var stop=hooks.stop;
delete hooks.stop;
stop(gotoEnd)
};
if(typeof type!=="string"){gotoEnd=clearQueue;
clearQueue=type;
type=undefined
}if(clearQueue&&type!==false){this.queue(type||"fx",[])
}return this.each(function(){var dequeue=true,index=type!=null&&type+"queueHooks",timers=jQuery.timers,data=jQuery._data(this);
if(index){if(data[index]&&data[index].stop){stopQueue(data[index])
}}else{for(index in data){if(data[index]&&data[index].stop&&rrun.test(index)){stopQueue(data[index])
}}}for(index=timers.length;
index--;
){if(timers[index].elem===this&&(type==null||timers[index].queue===type)){timers[index].anim.stop(gotoEnd);
dequeue=false;
timers.splice(index,1)
}}if(dequeue||!gotoEnd){jQuery.dequeue(this,type)
}})
}});
function genFx(type,includeWidth){var which,attrs={height:type},i=0;
includeWidth=includeWidth?1:0;
for(;
i<4;
i+=2-includeWidth){which=cssExpand[i];
attrs["margin"+which]=attrs["padding"+which]=type
}if(includeWidth){attrs.opacity=attrs.width=type
}return attrs
}jQuery.each({slideDown:genFx("show"),slideUp:genFx("hide"),slideToggle:genFx("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(name,props){jQuery.fn[name]=function(speed,easing,callback){return this.animate(props,speed,easing,callback)
}
});
jQuery.speed=function(speed,easing,fn){var opt=speed&&typeof speed==="object"?jQuery.extend({},speed):{complete:fn||!fn&&easing||jQuery.isFunction(speed)&&speed,duration:speed,easing:fn&&easing||easing&&!jQuery.isFunction(easing)&&easing};
opt.duration=jQuery.fx.off?0:typeof opt.duration==="number"?opt.duration:opt.duration in jQuery.fx.speeds?jQuery.fx.speeds[opt.duration]:jQuery.fx.speeds._default;
if(opt.queue==null||opt.queue===true){opt.queue="fx"
}opt.old=opt.complete;
opt.complete=function(){if(jQuery.isFunction(opt.old)){opt.old.call(this)
}if(opt.queue){jQuery.dequeue(this,opt.queue)
}};
return opt
};
jQuery.easing={linear:function(p){return p
},swing:function(p){return 0.5-Math.cos(p*Math.PI)/2
}};
jQuery.timers=[];
jQuery.fx=Tween.prototype.init;
jQuery.fx.tick=function(){var timer,timers=jQuery.timers,i=0;
for(;
i<timers.length;
i++){timer=timers[i];
if(!timer()&&timers[i]===timer){timers.splice(i--,1)
}}if(!timers.length){jQuery.fx.stop()
}};
jQuery.fx.timer=function(timer){if(timer()&&jQuery.timers.push(timer)&&!timerId){timerId=setInterval(jQuery.fx.tick,jQuery.fx.interval)
}};
jQuery.fx.interval=13;
jQuery.fx.stop=function(){clearInterval(timerId);
timerId=null
};
jQuery.fx.speeds={slow:600,fast:200,_default:400};
jQuery.fx.step={};
if(jQuery.expr&&jQuery.expr.filters){jQuery.expr.filters.animated=function(elem){return jQuery.grep(jQuery.timers,function(fn){return elem===fn.elem
}).length
}
}var rroot=/^(?:body|html)$/i;
jQuery.fn.offset=function(options){if(arguments.length){return options===undefined?this:this.each(function(i){jQuery.offset.setOffset(this,options,i)
})
}var docElem,body,win,clientTop,clientLeft,scrollTop,scrollLeft,box={top:0,left:0},elem=this[0],doc=elem&&elem.ownerDocument;
if(!doc){return 
}if((body=doc.body)===elem){return jQuery.offset.bodyOffset(elem)
}docElem=doc.documentElement;
if(!jQuery.contains(docElem,elem)){return box
}if(typeof elem.getBoundingClientRect!=="undefined"){box=elem.getBoundingClientRect()
}win=getWindow(doc);
clientTop=docElem.clientTop||body.clientTop||0;
clientLeft=docElem.clientLeft||body.clientLeft||0;
scrollTop=win.pageYOffset||docElem.scrollTop;
scrollLeft=win.pageXOffset||docElem.scrollLeft;
return{top:box.top+scrollTop-clientTop,left:box.left+scrollLeft-clientLeft}
};
jQuery.offset={bodyOffset:function(body){var top=body.offsetTop,left=body.offsetLeft;
if(jQuery.support.doesNotIncludeMarginInBodyOffset){top+=parseFloat(jQuery.css(body,"marginTop"))||0;
left+=parseFloat(jQuery.css(body,"marginLeft"))||0
}return{top:top,left:left}
},setOffset:function(elem,options,i){var position=jQuery.css(elem,"position");
if(position==="static"){elem.style.position="relative"
}var curElem=jQuery(elem),curOffset=curElem.offset(),curCSSTop=jQuery.css(elem,"top"),curCSSLeft=jQuery.css(elem,"left"),calculatePosition=(position==="absolute"||position==="fixed")&&jQuery.inArray("auto",[curCSSTop,curCSSLeft])>-1,props={},curPosition={},curTop,curLeft;
if(calculatePosition){curPosition=curElem.position();
curTop=curPosition.top;
curLeft=curPosition.left
}else{curTop=parseFloat(curCSSTop)||0;
curLeft=parseFloat(curCSSLeft)||0
}if(jQuery.isFunction(options)){options=options.call(elem,i,curOffset)
}if(options.top!=null){props.top=(options.top-curOffset.top)+curTop
}if(options.left!=null){props.left=(options.left-curOffset.left)+curLeft
}if("using" in options){options.using.call(elem,props)
}else{curElem.css(props)
}}};
jQuery.fn.extend({position:function(){if(!this[0]){return 
}var elem=this[0],offsetParent=this.offsetParent(),offset=this.offset(),parentOffset=rroot.test(offsetParent[0].nodeName)?{top:0,left:0}:offsetParent.offset();
offset.top-=parseFloat(jQuery.css(elem,"marginTop"))||0;
offset.left-=parseFloat(jQuery.css(elem,"marginLeft"))||0;
parentOffset.top+=parseFloat(jQuery.css(offsetParent[0],"borderTopWidth"))||0;
parentOffset.left+=parseFloat(jQuery.css(offsetParent[0],"borderLeftWidth"))||0;
return{top:offset.top-parentOffset.top,left:offset.left-parentOffset.left}
},offsetParent:function(){return this.map(function(){var offsetParent=this.offsetParent||document.body;
while(offsetParent&&(!rroot.test(offsetParent.nodeName)&&jQuery.css(offsetParent,"position")==="static")){offsetParent=offsetParent.offsetParent
}return offsetParent||document.body
})
}});
jQuery.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(method,prop){var top=/Y/.test(prop);
jQuery.fn[method]=function(val){return jQuery.access(this,function(elem,method,val){var win=getWindow(elem);
if(val===undefined){return win?(prop in win)?win[prop]:win.document.documentElement[method]:elem[method]
}if(win){win.scrollTo(!top?val:jQuery(win).scrollLeft(),top?val:jQuery(win).scrollTop())
}else{elem[method]=val
}},method,val,arguments.length,null)
}
});
function getWindow(elem){return jQuery.isWindow(elem)?elem:elem.nodeType===9?elem.defaultView||elem.parentWindow:false
}jQuery.each({Height:"height",Width:"width"},function(name,type){jQuery.each({padding:"inner"+name,content:type,"":"outer"+name},function(defaultExtra,funcName){jQuery.fn[funcName]=function(margin,value){var chainable=arguments.length&&(defaultExtra||typeof margin!=="boolean"),extra=defaultExtra||(margin===true||value===true?"margin":"border");
return jQuery.access(this,function(elem,type,value){var doc;
if(jQuery.isWindow(elem)){return elem.document.documentElement["client"+name]
}if(elem.nodeType===9){doc=elem.documentElement;
return Math.max(elem.body["scroll"+name],doc["scroll"+name],elem.body["offset"+name],doc["offset"+name],doc["client"+name])
}return value===undefined?jQuery.css(elem,type,value,extra):jQuery.style(elem,type,value,extra)
},type,chainable?margin:undefined,chainable,null)
}
})
});
window.jQuery=window.$=jQuery;
if(typeof define==="function"&&define.amd&&define.amd.jQuery){define("jquery",[],function(){return jQuery
})
}})(window);
/* jQuery UI - v1.9.1 - 2012-11-09
* http://jqueryui.com
* Includes: jquery.ui.core.js, jquery.ui.widget.js, jquery.ui.mouse.js, jquery.ui.position.js, jquery.ui.draggable.js, jquery.ui.droppable.js, jquery.ui.resizable.js, jquery.ui.selectable.js, jquery.ui.sortable.js, jquery.ui.accordion.js, jquery.ui.autocomplete.js, jquery.ui.button.js, jquery.ui.datepicker.js, jquery.ui.dialog.js, jquery.ui.menu.js, jquery.ui.progressbar.js, jquery.ui.slider.js, jquery.ui.spinner.js, jquery.ui.tabs.js, jquery.ui.tooltip.js, jquery.ui.effect.js, jquery.ui.effect-blind.js, jquery.ui.effect-bounce.js, jquery.ui.effect-clip.js, jquery.ui.effect-drop.js, jquery.ui.effect-explode.js, jquery.ui.effect-fade.js, jquery.ui.effect-fold.js, jquery.ui.effect-highlight.js, jquery.ui.effect-pulsate.js, jquery.ui.effect-scale.js, jquery.ui.effect-shake.js, jquery.ui.effect-slide.js, jquery.ui.effect-transfer.js
* Copyright (c) 2012 jQuery Foundation and other contributors Licensed MIT */
(function(B,F){var A=0,E=/^ui-id-\d+$/;
B.ui=B.ui||{};
if(B.ui.version){return 
}B.extend(B.ui,{version:"1.9.1",keyCode:{BACKSPACE:8,COMMA:188,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,LEFT:37,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SPACE:32,TAB:9,UP:38}});
B.fn.extend({_focus:B.fn.focus,focus:function(G,H){return typeof G==="number"?this.each(function(){var I=this;
setTimeout(function(){B(I).focus();
if(H){H.call(I)
}},G)
}):this._focus.apply(this,arguments)
},scrollParent:function(){var G;
if((B.ui.ie&&(/(static|relative)/).test(this.css("position")))||(/absolute/).test(this.css("position"))){G=this.parents().filter(function(){return(/(relative|absolute|fixed)/).test(B.css(this,"position"))&&(/(auto|scroll)/).test(B.css(this,"overflow")+B.css(this,"overflow-y")+B.css(this,"overflow-x"))
}).eq(0)
}else{G=this.parents().filter(function(){return(/(auto|scroll)/).test(B.css(this,"overflow")+B.css(this,"overflow-y")+B.css(this,"overflow-x"))
}).eq(0)
}return(/fixed/).test(this.css("position"))||!G.length?B(document):G
},zIndex:function(J){if(J!==F){return this.css("zIndex",J)
}if(this.length){var H=B(this[0]),G,I;
while(H.length&&H[0]!==document){G=H.css("position");
if(G==="absolute"||G==="relative"||G==="fixed"){I=parseInt(H.css("zIndex"),10);
if(!isNaN(I)&&I!==0){return I
}}H=H.parent()
}}return 0
},uniqueId:function(){return this.each(function(){if(!this.id){this.id="ui-id-"+(++A)
}})
},removeUniqueId:function(){return this.each(function(){if(E.test(this.id)){B(this).removeAttr("id")
}})
}});
if(!B("<a>").outerWidth(1).jquery){B.each(["Width","Height"],function(I,G){var H=G==="Width"?["Left","Right"]:["Top","Bottom"],J=G.toLowerCase(),L={innerWidth:B.fn.innerWidth,innerHeight:B.fn.innerHeight,outerWidth:B.fn.outerWidth,outerHeight:B.fn.outerHeight};
function K(O,N,M,P){B.each(H,function(){N-=parseFloat(B.css(O,"padding"+this))||0;
if(M){N-=parseFloat(B.css(O,"border"+this+"Width"))||0
}if(P){N-=parseFloat(B.css(O,"margin"+this))||0
}});
return N
}B.fn["inner"+G]=function(M){if(M===F){return L["inner"+G].call(this)
}return this.each(function(){B(this).css(J,K(this,M)+"px")
})
};
B.fn["outer"+G]=function(M,N){if(typeof M!=="number"){return L["outer"+G].call(this,M)
}return this.each(function(){B(this).css(J,K(this,M,true,N)+"px")
})
}
})
}function D(I,G){var K,J,H,L=I.nodeName.toLowerCase();
if("area"===L){K=I.parentNode;
J=K.name;
if(!I.href||!J||K.nodeName.toLowerCase()!=="map"){return false
}H=B("img[usemap=#"+J+"]")[0];
return !!H&&C(H)
}return(/input|select|textarea|button|object/.test(L)?!I.disabled:"a"===L?I.href||G:G)&&C(I)
}function C(G){return B.expr.filters.visible(G)&&!B(G).parents().andSelf().filter(function(){return B.css(this,"visibility")==="hidden"
}).length
}B.extend(B.expr[":"],{data:B.expr.createPseudo?B.expr.createPseudo(function(G){return function(H){return !!B.data(H,G)
}
}):function(I,H,G){return !!B.data(I,G[3])
},focusable:function(G){return D(G,!isNaN(B.attr(G,"tabindex")))
},tabbable:function(I){var G=B.attr(I,"tabindex"),H=isNaN(G);
return(H||G>=0)&&D(I,!H)
}});
B(function(){var G=document.body,H=G.appendChild(H=document.createElement("div"));
H.offsetHeight;
B.extend(H.style,{minHeight:"100px",height:"auto",padding:0,borderWidth:0});
B.support.minHeight=H.offsetHeight===100;
B.support.selectstart="onselectstart" in H;
G.removeChild(H).style.display="none"
});
(function(){var G=/msie ([\w.]+)/.exec(navigator.userAgent.toLowerCase())||[];
B.ui.ie=G.length?true:false;
B.ui.ie6=parseFloat(G[1],10)===6
})();
B.fn.extend({disableSelection:function(){return this.bind((B.support.selectstart?"selectstart":"mousedown")+".ui-disableSelection",function(G){G.preventDefault()
})
},enableSelection:function(){return this.unbind(".ui-disableSelection")
}});
B.extend(B.ui,{plugin:{add:function(H,I,K){var G,J=B.ui[H].prototype;
for(G in K){J.plugins[G]=J.plugins[G]||[];
J.plugins[G].push([I,K[G]])
}},call:function(G,I,H){var J,K=G.plugins[I];
if(!K||!G.element[0].parentNode||G.element[0].parentNode.nodeType===11){return 
}for(J=0;
J<K.length;
J++){if(G.options[K[J][0]]){K[J][1].apply(G.element,H)
}}}},contains:B.contains,hasScroll:function(J,H){if(B(J).css("overflow")==="hidden"){return false
}var G=(H&&H==="left")?"scrollLeft":"scrollTop",I=false;
if(J[G]>0){return true
}J[G]=1;
I=(J[G]>0);
J[G]=0;
return I
},isOverAxis:function(H,G,I){return(H>G)&&(H<(G+I))
},isOver:function(L,H,K,J,G,I){return B.ui.isOverAxis(L,K,G)&&B.ui.isOverAxis(H,J,I)
}})
})(jQuery);
(function(B,E){var A=0,D=Array.prototype.slice,C=B.cleanData;
B.cleanData=function(F){for(var G=0,H;
(H=F[G])!=null;
G++){try{B(H).triggerHandler("remove")
}catch(I){}}C(F)
};
B.widget=function(G,J,F){var M,L,I,K,H=G.split(".")[0];
G=G.split(".")[1];
M=H+"-"+G;
if(!F){F=J;
J=B.Widget
}B.expr[":"][M.toLowerCase()]=function(N){return !!B.data(N,M)
};
B[H]=B[H]||{};
L=B[H][G];
I=B[H][G]=function(N,O){if(!this._createWidget){return new I(N,O)
}if(arguments.length){this._createWidget(N,O)
}};
B.extend(I,L,{version:F.version,_proto:B.extend({},F),_childConstructors:[]});
K=new J();
K.options=B.widget.extend({},K.options);
B.each(F,function(O,N){if(B.isFunction(N)){F[O]=(function(){var P=function(){return J.prototype[O].apply(this,arguments)
},Q=function(R){return J.prototype[O].apply(this,R)
};
return function(){var T=this._super,R=this._superApply,S;
this._super=P;
this._superApply=Q;
S=N.apply(this,arguments);
this._super=T;
this._superApply=R;
return S
}
})()
}});
I.prototype=B.widget.extend(K,{widgetEventPrefix:K.widgetEventPrefix||G},F,{constructor:I,namespace:H,widgetName:G,widgetBaseClass:M,widgetFullName:M});
if(L){B.each(L._childConstructors,function(O,P){var N=P.prototype;
B.widget(N.namespace+"."+N.widgetName,I,P._proto)
});
delete L._childConstructors
}else{J._childConstructors.push(I)
}B.widget.bridge(G,I)
};
B.widget.extend=function(K){var G=D.call(arguments,1),J=0,F=G.length,H,I;
for(;
J<F;
J++){for(H in G[J]){I=G[J][H];
if(G[J].hasOwnProperty(H)&&I!==E){if(B.isPlainObject(I)){K[H]=B.isPlainObject(K[H])?B.widget.extend({},K[H],I):B.widget.extend({},I)
}else{K[H]=I
}}}}return K
};
B.widget.bridge=function(G,F){var H=F.prototype.widgetFullName;
B.fn[G]=function(K){var I=typeof K==="string",J=D.call(arguments,1),L=this;
K=!I&&J.length?B.widget.extend.apply(null,[K].concat(J)):K;
if(I){this.each(function(){var N,M=B.data(this,H);
if(!M){return B.error("cannot call methods on "+G+" prior to initialization; attempted to call method '"+K+"'")
}if(!B.isFunction(M[K])||K.charAt(0)==="_"){return B.error("no such method '"+K+"' for "+G+" widget instance")
}N=M[K].apply(M,J);
if(N!==M&&N!==E){L=N&&N.jquery?L.pushStack(N.get()):N;
return false
}})
}else{this.each(function(){var M=B.data(this,H);
if(M){M.option(K||{})._init()
}else{new F(K,this)
}})
}return L
}
};
B.Widget=function(){};
B.Widget._childConstructors=[];
B.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",defaultElement:"<div>",options:{disabled:false,create:null},_createWidget:function(F,G){G=B(G||this.defaultElement||this)[0];
this.element=B(G);
this.uuid=A++;
this.eventNamespace="."+this.widgetName+this.uuid;
this.options=B.widget.extend({},this.options,this._getCreateOptions(),F);
this.bindings=B();
this.hoverable=B();
this.focusable=B();
if(G!==this){B.data(G,this.widgetName,this);
B.data(G,this.widgetFullName,this);
this._on(this.element,{remove:function(H){if(H.target===G){this.destroy()
}}});
this.document=B(G.style?G.ownerDocument:G.document||G);
this.window=B(this.document[0].defaultView||this.document[0].parentWindow)
}this._create();
this._trigger("create",null,this._getCreateEventData());
this._init()
},_getCreateOptions:B.noop,_getCreateEventData:B.noop,_create:B.noop,_init:B.noop,destroy:function(){this._destroy();
this.element.unbind(this.eventNamespace).removeData(this.widgetName).removeData(this.widgetFullName).removeData(B.camelCase(this.widgetFullName));
this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName+"-disabled ui-state-disabled");
this.bindings.unbind(this.eventNamespace);
this.hoverable.removeClass("ui-state-hover");
this.focusable.removeClass("ui-state-focus")
},_destroy:B.noop,widget:function(){return this.element
},option:function(I,J){var F=I,K,H,G;
if(arguments.length===0){return B.widget.extend({},this.options)
}if(typeof I==="string"){F={};
K=I.split(".");
I=K.shift();
if(K.length){H=F[I]=B.widget.extend({},this.options[I]);
for(G=0;
G<K.length-1;
G++){H[K[G]]=H[K[G]]||{};
H=H[K[G]]
}I=K.pop();
if(J===E){return H[I]===E?null:H[I]
}H[I]=J
}else{if(J===E){return this.options[I]===E?null:this.options[I]
}F[I]=J
}}this._setOptions(F);
return this
},_setOptions:function(F){var G;
for(G in F){this._setOption(G,F[G])
}return this
},_setOption:function(F,G){this.options[F]=G;
if(F==="disabled"){this.widget().toggleClass(this.widgetFullName+"-disabled ui-state-disabled",!!G).attr("aria-disabled",G);
this.hoverable.removeClass("ui-state-hover");
this.focusable.removeClass("ui-state-focus")
}return this
},enable:function(){return this._setOption("disabled",false)
},disable:function(){return this._setOption("disabled",true)
},_on:function(H,G){var I,F=this;
if(!G){G=H;
H=this.element;
I=this.widget()
}else{H=I=B(H);
this.bindings=this.bindings.add(H)
}B.each(G,function(O,N){function L(){if(F.options.disabled===true||B(this).hasClass("ui-state-disabled")){return 
}return(typeof N==="string"?F[N]:N).apply(F,arguments)
}if(typeof N!=="string"){L.guid=N.guid=N.guid||L.guid||B.guid++
}var M=O.match(/^(\w+)\s*(.*)$/),K=M[1]+F.eventNamespace,J=M[2];
if(J){I.delegate(J,K,L)
}else{H.bind(K,L)
}})
},_off:function(G,F){F=(F||"").split(" ").join(this.eventNamespace+" ")+this.eventNamespace;
G.unbind(F).undelegate(F)
},_delay:function(I,H){function G(){return(typeof I==="string"?F[I]:I).apply(F,arguments)
}var F=this;
return setTimeout(G,H||0)
},_hoverable:function(F){this.hoverable=this.hoverable.add(F);
this._on(F,{mouseenter:function(G){B(G.currentTarget).addClass("ui-state-hover")
},mouseleave:function(G){B(G.currentTarget).removeClass("ui-state-hover")
}})
},_focusable:function(F){this.focusable=this.focusable.add(F);
this._on(F,{focusin:function(G){B(G.currentTarget).addClass("ui-state-focus")
},focusout:function(G){B(G.currentTarget).removeClass("ui-state-focus")
}})
},_trigger:function(F,G,H){var K,J,I=this.options[F];
H=H||{};
G=B.Event(G);
G.type=(F===this.widgetEventPrefix?F:this.widgetEventPrefix+F).toLowerCase();
G.target=this.element[0];
J=G.originalEvent;
if(J){for(K in J){if(!(K in G)){G[K]=J[K]
}}}this.element.trigger(G,H);
return !(B.isFunction(I)&&I.apply(this.element[0],[G].concat(H))===false||G.isDefaultPrevented())
}};
B.each({show:"fadeIn",hide:"fadeOut"},function(G,F){B.Widget.prototype["_"+G]=function(J,I,L){if(typeof I==="string"){I={effect:I}
}var K,H=!I?G:I===true||typeof I==="number"?F:I.effect||F;
I=I||{};
if(typeof I==="number"){I={duration:I}
}K=!B.isEmptyObject(I);
I.complete=L;
if(I.delay){J.delay(I.delay)
}if(K&&B.effects&&(B.effects.effect[H]||B.uiBackCompat!==false&&B.effects[H])){J[G](I)
}else{if(H!==G&&J[H]){J[H](I.duration,I.easing,L)
}else{J.queue(function(M){B(this)[G]();
if(L){L.call(J[0])
}M()
})
}}}
});
if(B.uiBackCompat!==false){B.Widget.prototype._getCreateOptions=function(){return B.metadata&&B.metadata.get(this.element[0])[this.widgetName]
}
}})(jQuery);
(function(B,C){var A=false;
B(document).mouseup(function(D){A=false
});
B.widget("ui.mouse",{version:"1.9.1",options:{cancel:"input,textarea,button,select,option",distance:1,delay:0},_mouseInit:function(){var D=this;
this.element.bind("mousedown."+this.widgetName,function(E){return D._mouseDown(E)
}).bind("click."+this.widgetName,function(E){if(true===B.data(E.target,D.widgetName+".preventClickEvent")){B.removeData(E.target,D.widgetName+".preventClickEvent");
E.stopImmediatePropagation();
return false
}});
this.started=false
},_mouseDestroy:function(){this.element.unbind("."+this.widgetName);
if(this._mouseMoveDelegate){B(document).unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate)
}},_mouseDown:function(F){if(A){return 
}(this._mouseStarted&&this._mouseUp(F));
this._mouseDownEvent=F;
var E=this,G=(F.which===1),D=(typeof this.options.cancel==="string"&&F.target.nodeName?B(F.target).closest(this.options.cancel).length:false);
if(!G||D||!this._mouseCapture(F)){return true
}this.mouseDelayMet=!this.options.delay;
if(!this.mouseDelayMet){this._mouseDelayTimer=setTimeout(function(){E.mouseDelayMet=true
},this.options.delay)
}if(this._mouseDistanceMet(F)&&this._mouseDelayMet(F)){this._mouseStarted=(this._mouseStart(F)!==false);
if(!this._mouseStarted){F.preventDefault();
return true
}}if(true===B.data(F.target,this.widgetName+".preventClickEvent")){B.removeData(F.target,this.widgetName+".preventClickEvent")
}this._mouseMoveDelegate=function(H){return E._mouseMove(H)
};
this._mouseUpDelegate=function(H){return E._mouseUp(H)
};
B(document).bind("mousemove."+this.widgetName,this._mouseMoveDelegate).bind("mouseup."+this.widgetName,this._mouseUpDelegate);
F.preventDefault();
A=true;
return true
},_mouseMove:function(D){if(B.ui.ie&&!(document.documentMode>=9)&&!D.button){return this._mouseUp(D)
}if(this._mouseStarted){this._mouseDrag(D);
return D.preventDefault()
}if(this._mouseDistanceMet(D)&&this._mouseDelayMet(D)){this._mouseStarted=(this._mouseStart(this._mouseDownEvent,D)!==false);
(this._mouseStarted?this._mouseDrag(D):this._mouseUp(D))
}return !this._mouseStarted
},_mouseUp:function(D){B(document).unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate);
if(this._mouseStarted){this._mouseStarted=false;
if(D.target===this._mouseDownEvent.target){B.data(D.target,this.widgetName+".preventClickEvent",true)
}this._mouseStop(D)
}return false
},_mouseDistanceMet:function(D){return(Math.max(Math.abs(this._mouseDownEvent.pageX-D.pageX),Math.abs(this._mouseDownEvent.pageY-D.pageY))>=this.options.distance)
},_mouseDelayMet:function(D){return this.mouseDelayMet
},_mouseStart:function(D){},_mouseDrag:function(D){},_mouseStop:function(D){},_mouseCapture:function(D){return true
}})
})(jQuery);
(function(E,C){E.ui=E.ui||{};
var I,J=Math.max,N=Math.abs,L=Math.round,D=/left|center|right/,G=/top|center|bottom/,A=/[\+\-]\d+%?/,K=/^\w+/,B=/%$/,F=E.fn.position;
function M(Q,P,O){return[parseInt(Q[0],10)*(B.test(Q[0])?P/100:1),parseInt(Q[1],10)*(B.test(Q[1])?O/100:1)]
}function H(O,P){return parseInt(E.css(O,P),10)||0
}E.position={scrollbarWidth:function(){if(I!==C){return I
}var P,O,R=E("<div style='display:block;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),Q=R.children()[0];
E("body").append(R);
P=Q.offsetWidth;
R.css("overflow","scroll");
O=Q.offsetWidth;
if(P===O){O=R[0].clientWidth
}R.remove();
return(I=P-O)
},getScrollInfo:function(S){var R=S.isWindow?"":S.element.css("overflow-x"),Q=S.isWindow?"":S.element.css("overflow-y"),P=R==="scroll"||(R==="auto"&&S.width<S.element[0].scrollWidth),O=Q==="scroll"||(Q==="auto"&&S.height<S.element[0].scrollHeight);
return{width:P?E.position.scrollbarWidth():0,height:O?E.position.scrollbarWidth():0}
},getWithinInfo:function(P){var Q=E(P||window),O=E.isWindow(Q[0]);
return{element:Q,isWindow:O,offset:Q.offset()||{left:0,top:0},scrollLeft:Q.scrollLeft(),scrollTop:Q.scrollTop(),width:O?Q.width():Q.outerWidth(),height:O?Q.height():Q.outerHeight()}
}};
E.fn.position=function(Y){if(!Y||!Y.of){return F.apply(this,arguments)
}Y=E.extend({},Y);
var Z,V,S,X,R,U=E(Y.of),Q=E.position.getWithinInfo(Y.within),O=E.position.getScrollInfo(Q),T=U[0],W=(Y.collision||"flip").split(" "),P={};
if(T.nodeType===9){V=U.width();
S=U.height();
X={top:0,left:0}
}else{if(E.isWindow(T)){V=U.width();
S=U.height();
X={top:U.scrollTop(),left:U.scrollLeft()}
}else{if(T.preventDefault){Y.at="left top";
V=S=0;
X={top:T.pageY,left:T.pageX}
}else{V=U.outerWidth();
S=U.outerHeight();
X=U.offset()
}}}R=E.extend({},X);
E.each(["my","at"],function(){var c=(Y[this]||"").split(" "),b,a;
if(c.length===1){c=D.test(c[0])?c.concat(["center"]):G.test(c[0])?["center"].concat(c):["center","center"]
}c[0]=D.test(c[0])?c[0]:"center";
c[1]=G.test(c[1])?c[1]:"center";
b=A.exec(c[0]);
a=A.exec(c[1]);
P[this]=[b?b[0]:0,a?a[0]:0];
Y[this]=[K.exec(c[0])[0],K.exec(c[1])[0]]
});
if(W.length===1){W[1]=W[0]
}if(Y.at[0]==="right"){R.left+=V
}else{if(Y.at[0]==="center"){R.left+=V/2
}}if(Y.at[1]==="bottom"){R.top+=S
}else{if(Y.at[1]==="center"){R.top+=S/2
}}Z=M(P.at,V,S);
R.left+=Z[0];
R.top+=Z[1];
return this.each(function(){var b,l,d=E(this),f=d.outerWidth(),c=d.outerHeight(),e=H(this,"marginLeft"),a=H(this,"marginTop"),k=f+e+H(this,"marginRight")+O.width,j=c+a+H(this,"marginBottom")+O.height,g=E.extend({},R),h=M(P.my,d.outerWidth(),d.outerHeight());
if(Y.my[0]==="right"){g.left-=f
}else{if(Y.my[0]==="center"){g.left-=f/2
}}if(Y.my[1]==="bottom"){g.top-=c
}else{if(Y.my[1]==="center"){g.top-=c/2
}}g.left+=h[0];
g.top+=h[1];
if(!E.support.offsetFractions){g.left=L(g.left);
g.top=L(g.top)
}b={marginLeft:e,marginTop:a};
E.each(["left","top"],function(n,m){if(E.ui.position[W[n]]){E.ui.position[W[n]][m](g,{targetWidth:V,targetHeight:S,elemWidth:f,elemHeight:c,collisionPosition:b,collisionWidth:k,collisionHeight:j,offset:[Z[0]+h[0],Z[1]+h[1]],my:Y.my,at:Y.at,within:Q,elem:d})
}});
if(E.fn.bgiframe){d.bgiframe()
}if(Y.using){l=function(p){var r=X.left-g.left,o=r+V-f,q=X.top-g.top,n=q+S-c,m={target:{element:U,left:X.left,top:X.top,width:V,height:S},element:{element:d,left:g.left,top:g.top,width:f,height:c},horizontal:o<0?"left":r>0?"right":"center",vertical:n<0?"top":q>0?"bottom":"middle"};
if(V<f&&N(r+o)<V){m.horizontal="center"
}if(S<c&&N(q+n)<S){m.vertical="middle"
}if(J(N(r),N(o))>J(N(q),N(n))){m.important="horizontal"
}else{m.important="vertical"
}Y.using.call(this,p,m)
}
}d.offset(E.extend(g,{using:l}))
})
};
E.ui.position={fit:{left:function(S,R){var Q=R.within,U=Q.isWindow?Q.scrollLeft:Q.offset.left,W=Q.width,T=S.left-R.collisionPosition.marginLeft,V=U-T,P=T+R.collisionWidth-W-U,O;
if(R.collisionWidth>W){if(V>0&&P<=0){O=S.left+V+R.collisionWidth-W-U;
S.left+=V-O
}else{if(P>0&&V<=0){S.left=U
}else{if(V>P){S.left=U+W-R.collisionWidth
}else{S.left=U
}}}}else{if(V>0){S.left+=V
}else{if(P>0){S.left-=P
}else{S.left=J(S.left-T,S.left)
}}}},top:function(R,Q){var P=Q.within,V=P.isWindow?P.scrollTop:P.offset.top,W=Q.within.height,T=R.top-Q.collisionPosition.marginTop,U=V-T,S=T+Q.collisionHeight-W-V,O;
if(Q.collisionHeight>W){if(U>0&&S<=0){O=R.top+U+Q.collisionHeight-W-V;
R.top+=U-O
}else{if(S>0&&U<=0){R.top=V
}else{if(U>S){R.top=V+W-Q.collisionHeight
}else{R.top=V
}}}}else{if(U>0){R.top+=U
}else{if(S>0){R.top-=S
}else{R.top=J(R.top-T,R.top)
}}}}},flip:{left:function(U,T){var S=T.within,Y=S.offset.left+S.scrollLeft,b=S.width,Q=S.isWindow?S.scrollLeft:S.offset.left,V=U.left-T.collisionPosition.marginLeft,Z=V-Q,P=V+T.collisionWidth-b-Q,X=T.my[0]==="left"?-T.elemWidth:T.my[0]==="right"?T.elemWidth:0,a=T.at[0]==="left"?T.targetWidth:T.at[0]==="right"?-T.targetWidth:0,R=-2*T.offset[0],O,W;
if(Z<0){O=U.left+X+a+R+T.collisionWidth-b-Y;
if(O<0||O<N(Z)){U.left+=X+a+R
}}else{if(P>0){W=U.left-T.collisionPosition.marginLeft+X+a+R-Q;
if(W>0||N(W)<P){U.left+=X+a+R
}}}},top:function(T,S){var R=S.within,a=R.offset.top+R.scrollTop,b=R.height,O=R.isWindow?R.scrollTop:R.offset.top,V=T.top-S.collisionPosition.marginTop,X=V-O,U=V+S.collisionHeight-b-O,Y=S.my[1]==="top",W=Y?-S.elemHeight:S.my[1]==="bottom"?S.elemHeight:0,c=S.at[1]==="top"?S.targetHeight:S.at[1]==="bottom"?-S.targetHeight:0,Q=-2*S.offset[1],Z,P;
if(X<0){P=T.top+W+c+Q+S.collisionHeight-b-a;
if((T.top+W+c+Q)>X&&(P<0||P<N(X))){T.top+=W+c+Q
}}else{if(U>0){Z=T.top-S.collisionPosition.marginTop+W+c+Q-O;
if((T.top+W+c+Q)>U&&(Z>0||N(Z)<U)){T.top+=W+c+Q
}}}}},flipfit:{left:function(){E.ui.position.flip.left.apply(this,arguments);
E.ui.position.fit.left.apply(this,arguments)
},top:function(){E.ui.position.flip.top.apply(this,arguments);
E.ui.position.fit.top.apply(this,arguments)
}}};
(function(){var S,U,P,R,Q,O=document.getElementsByTagName("body")[0],T=document.createElement("div");
S=document.createElement(O?"div":"body");
P={visibility:"hidden",width:0,height:0,border:0,margin:0,background:"none"};
if(O){E.extend(P,{position:"absolute",left:"-1000px",top:"-1000px"})
}for(Q in P){S.style[Q]=P[Q]
}S.appendChild(T);
U=O||document.documentElement;
U.insertBefore(S,U.firstChild);
T.style.cssText="position: absolute; left: 10.7432222px;";
R=E(T).offset().left;
E.support.offsetFractions=R>10&&R<11;
S.innerHTML="";
U.removeChild(S)
})();
if(E.uiBackCompat!==false){(function(P){var O=P.fn.position;
P.fn.position=function(R){if(!R||!R.offset){return O.call(this,R)
}var S=R.offset.split(" "),Q=R.at.split(" ");
if(S.length===1){S[1]=S[0]
}if(/^\d/.test(S[0])){S[0]="+"+S[0]
}if(/^\d/.test(S[1])){S[1]="+"+S[1]
}if(Q.length===1){if(/left|center|right/.test(Q[0])){Q[1]="center"
}else{Q[1]=Q[0];
Q[0]="center"
}}return O.call(this,P.extend(R,{at:Q[0]+S[0]+" "+Q[1]+S[1],offset:C}))
}
}(jQuery))
}}(jQuery));
(function(A,B){A.widget("ui.draggable",A.ui.mouse,{version:"1.9.1",widgetEventPrefix:"drag",options:{addClasses:true,appendTo:"parent",axis:false,connectToSortable:false,containment:false,cursor:"auto",cursorAt:false,grid:false,handle:false,helper:"original",iframeFix:false,opacity:false,refreshPositions:false,revert:false,revertDuration:500,scope:"default",scroll:true,scrollSensitivity:20,scrollSpeed:20,snap:false,snapMode:"both",snapTolerance:20,stack:false,zIndex:false},_create:function(){if(this.options.helper=="original"&&!(/^(?:r|a|f)/).test(this.element.css("position"))){this.element[0].style.position="relative"
}(this.options.addClasses&&this.element.addClass("ui-draggable"));
(this.options.disabled&&this.element.addClass("ui-draggable-disabled"));
this._mouseInit()
},_destroy:function(){this.element.removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled");
this._mouseDestroy()
},_mouseCapture:function(C){var D=this.options;
if(this.helper||D.disabled||A(C.target).is(".ui-resizable-handle")){return false
}this.handle=this._getHandle(C);
if(!this.handle){return false
}A(D.iframeFix===true?"iframe":D.iframeFix).each(function(){A('<div class="ui-draggable-iframeFix" style="background: #fff;"></div>').css({width:this.offsetWidth+"px",height:this.offsetHeight+"px",position:"absolute",opacity:"http://www.usfoods.com/etc/designs/usfoods/clientlibs/0.001",zIndex:1000}).css(A(this).offset()).appendTo("body")
});
return true
},_mouseStart:function(C){var D=this.options;
this.helper=this._createHelper(C);
this.helper.addClass("ui-draggable-dragging");
this._cacheHelperProportions();
if(A.ui.ddmanager){A.ui.ddmanager.current=this
}this._cacheMargins();
this.cssPosition=this.helper.css("position");
this.scrollParent=this.helper.scrollParent();
this.offset=this.positionAbs=this.element.offset();
this.offset={top:this.offset.top-this.margins.top,left:this.offset.left-this.margins.left};
A.extend(this.offset,{click:{left:C.pageX-this.offset.left,top:C.pageY-this.offset.top},parent:this._getParentOffset(),relative:this._getRelativeOffset()});
this.originalPosition=this.position=this._generatePosition(C);
this.originalPageX=C.pageX;
this.originalPageY=C.pageY;
(D.cursorAt&&this._adjustOffsetFromHelper(D.cursorAt));
if(D.containment){this._setContainment()
}if(this._trigger("start",C)===false){this._clear();
return false
}this._cacheHelperProportions();
if(A.ui.ddmanager&&!D.dropBehaviour){A.ui.ddmanager.prepareOffsets(this,C)
}this._mouseDrag(C,true);
if(A.ui.ddmanager){A.ui.ddmanager.dragStart(this,C)
}return true
},_mouseDrag:function(C,E){this.position=this._generatePosition(C);
this.positionAbs=this._convertPositionTo("absolute");
if(!E){var D=this._uiHash();
if(this._trigger("drag",C,D)===false){this._mouseUp({});
return false
}this.position=D.position
}if(!this.options.axis||this.options.axis!="y"){this.helper[0].style.left=this.position.left+"px"
}if(!this.options.axis||this.options.axis!="x"){this.helper[0].style.top=this.position.top+"px"
}if(A.ui.ddmanager){A.ui.ddmanager.drag(this,C)
}return false
},_mouseStop:function(E){var G=false;
if(A.ui.ddmanager&&!this.options.dropBehaviour){G=A.ui.ddmanager.drop(this,E)
}if(this.dropped){G=this.dropped;
this.dropped=false
}var C=this.element[0],F=false;
while(C&&(C=C.parentNode)){if(C==document){F=true
}}if(!F&&this.options.helper==="original"){return false
}if((this.options.revert=="invalid"&&!G)||(this.options.revert=="valid"&&G)||this.options.revert===true||(A.isFunction(this.options.revert)&&this.options.revert.call(this.element,G))){var D=this;
A(this.helper).animate(this.originalPosition,parseInt(this.options.revertDuration,10),function(){if(D._trigger("stop",E)!==false){D._clear()
}})
}else{if(this._trigger("stop",E)!==false){this._clear()
}}return false
},_mouseUp:function(C){A("div.ui-draggable-iframeFix").each(function(){this.parentNode.removeChild(this)
});
if(A.ui.ddmanager){A.ui.ddmanager.dragStop(this,C)
}return A.ui.mouse.prototype._mouseUp.call(this,C)
},cancel:function(){if(this.helper.is(".ui-draggable-dragging")){this._mouseUp({})
}else{this._clear()
}return this
},_getHandle:function(C){var D=!this.options.handle||!A(this.options.handle,this.element).length?true:false;
A(this.options.handle,this.element).find("*").andSelf().each(function(){if(this==C.target){D=true
}});
return D
},_createHelper:function(D){var E=this.options;
var C=A.isFunction(E.helper)?A(E.helper.apply(this.element[0],[D])):(E.helper=="clone"?this.element.clone().removeAttr("id"):this.element);
if(!C.parents("body").length){C.appendTo((E.appendTo=="parent"?this.element[0].parentNode:E.appendTo))
}if(C[0]!=this.element[0]&&!(/(fixed|absolute)/).test(C.css("position"))){C.css("position","absolute")
}return C
},_adjustOffsetFromHelper:function(C){if(typeof C=="string"){C=C.split(" ")
}if(A.isArray(C)){C={left:+C[0],top:+C[1]||0}
}if("left" in C){this.offset.click.left=C.left+this.margins.left
}if("right" in C){this.offset.click.left=this.helperProportions.width-C.right+this.margins.left
}if("top" in C){this.offset.click.top=C.top+this.margins.top
}if("bottom" in C){this.offset.click.top=this.helperProportions.height-C.bottom+this.margins.top
}},_getParentOffset:function(){this.offsetParent=this.helper.offsetParent();
var C=this.offsetParent.offset();
if(this.cssPosition=="absolute"&&this.scrollParent[0]!=document&&A.contains(this.scrollParent[0],this.offsetParent[0])){C.left+=this.scrollParent.scrollLeft();
C.top+=this.scrollParent.scrollTop()
}if((this.offsetParent[0]==document.body)||(this.offsetParent[0].tagName&&this.offsetParent[0].tagName.toLowerCase()=="html"&&A.ui.ie)){C={top:0,left:0}
}return{top:C.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:C.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)}
},_getRelativeOffset:function(){if(this.cssPosition=="relative"){var C=this.element.position();
return{top:C.top-(parseInt(this.helper.css("top"),10)||0)+this.scrollParent.scrollTop(),left:C.left-(parseInt(this.helper.css("left"),10)||0)+this.scrollParent.scrollLeft()}
}else{return{top:0,left:0}
}},_cacheMargins:function(){this.margins={left:(parseInt(this.element.css("marginLeft"),10)||0),top:(parseInt(this.element.css("marginTop"),10)||0),right:(parseInt(this.element.css("marginRight"),10)||0),bottom:(parseInt(this.element.css("marginBottom"),10)||0)}
},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()}
},_setContainment:function(){var F=this.options;
if(F.containment=="parent"){F.containment=this.helper[0].parentNode
}if(F.containment=="document"||F.containment=="window"){this.containment=[F.containment=="document"?0:A(window).scrollLeft()-this.offset.relative.left-this.offset.parent.left,F.containment=="document"?0:A(window).scrollTop()-this.offset.relative.top-this.offset.parent.top,(F.containment=="document"?0:A(window).scrollLeft())+A(F.containment=="document"?document:window).width()-this.helperProportions.width-this.margins.left,(F.containment=="document"?0:A(window).scrollTop())+(A(F.containment=="document"?document:window).height()||document.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top]
}if(!(/^(document|window|parent)$/).test(F.containment)&&F.containment.constructor!=Array){var G=A(F.containment);
var D=G[0];
if(!D){return 
}var E=G.offset();
var C=(A(D).css("overflow")!="hidden");
this.containment=[(parseInt(A(D).css("borderLeftWidth"),10)||0)+(parseInt(A(D).css("paddingLeft"),10)||0),(parseInt(A(D).css("borderTopWidth"),10)||0)+(parseInt(A(D).css("paddingTop"),10)||0),(C?Math.max(D.scrollWidth,D.offsetWidth):D.offsetWidth)-(parseInt(A(D).css("borderLeftWidth"),10)||0)-(parseInt(A(D).css("paddingRight"),10)||0)-this.helperProportions.width-this.margins.left-this.margins.right,(C?Math.max(D.scrollHeight,D.offsetHeight):D.offsetHeight)-(parseInt(A(D).css("borderTopWidth"),10)||0)-(parseInt(A(D).css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top-this.margins.bottom];
this.relative_container=G
}else{if(F.containment.constructor==Array){this.containment=F.containment
}}},_convertPositionTo:function(F,H){if(!H){H=this.position
}var D=F=="absolute"?1:-1;
var E=this.options,C=this.cssPosition=="absolute"&&!(this.scrollParent[0]!=document&&A.contains(this.scrollParent[0],this.offsetParent[0]))?this.offsetParent:this.scrollParent,G=(/(html|body)/i).test(C[0].tagName);
return{top:(H.top+this.offset.relative.top*D+this.offset.parent.top*D-((this.cssPosition=="fixed"?-this.scrollParent.scrollTop():(G?0:C.scrollTop()))*D)),left:(H.left+this.offset.relative.left*D+this.offset.parent.left*D-((this.cssPosition=="fixed"?-this.scrollParent.scrollLeft():G?0:C.scrollLeft())*D))}
},_generatePosition:function(D){var E=this.options,L=this.cssPosition=="absolute"&&!(this.scrollParent[0]!=document&&A.contains(this.scrollParent[0],this.offsetParent[0]))?this.offsetParent:this.scrollParent,I=(/(html|body)/i).test(L[0].tagName);
var H=D.pageX;
var G=D.pageY;
if(this.originalPosition){var C;
if(this.containment){if(this.relative_container){var K=this.relative_container.offset();
C=[this.containment[0]+K.left,this.containment[1]+K.top,this.containment[2]+K.left,this.containment[3]+K.top]
}else{C=this.containment
}if(D.pageX-this.offset.click.left<C[0]){H=C[0]+this.offset.click.left
}if(D.pageY-this.offset.click.top<C[1]){G=C[1]+this.offset.click.top
}if(D.pageX-this.offset.click.left>C[2]){H=C[2]+this.offset.click.left
}if(D.pageY-this.offset.click.top>C[3]){G=C[3]+this.offset.click.top
}}if(E.grid){var J=E.grid[1]?this.originalPageY+Math.round((G-this.originalPageY)/E.grid[1])*E.grid[1]:this.originalPageY;
G=C?(!(J-this.offset.click.top<C[1]||J-this.offset.click.top>C[3])?J:(!(J-this.offset.click.top<C[1])?J-E.grid[1]:J+E.grid[1])):J;
var F=E.grid[0]?this.originalPageX+Math.round((H-this.originalPageX)/E.grid[0])*E.grid[0]:this.originalPageX;
H=C?(!(F-this.offset.click.left<C[0]||F-this.offset.click.left>C[2])?F:(!(F-this.offset.click.left<C[0])?F-E.grid[0]:F+E.grid[0])):F
}}return{top:(G-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+((this.cssPosition=="fixed"?-this.scrollParent.scrollTop():(I?0:L.scrollTop())))),left:(H-this.offset.click.left-this.offset.relative.left-this.offset.parent.left+((this.cssPosition=="fixed"?-this.scrollParent.scrollLeft():I?0:L.scrollLeft())))}
},_clear:function(){this.helper.removeClass("ui-draggable-dragging");
if(this.helper[0]!=this.element[0]&&!this.cancelHelperRemoval){this.helper.remove()
}this.helper=null;
this.cancelHelperRemoval=false
},_trigger:function(C,D,E){E=E||this._uiHash();
A.ui.plugin.call(this,C,[D,E]);
if(C=="drag"){this.positionAbs=this._convertPositionTo("absolute")
}return A.Widget.prototype._trigger.call(this,C,D,E)
},plugins:{},_uiHash:function(C){return{helper:this.helper,position:this.position,originalPosition:this.originalPosition,offset:this.positionAbs}
}});
A.ui.plugin.add("draggable","connectToSortable",{start:function(D,F){var E=A(this).data("draggable"),G=E.options,C=A.extend({},F,{item:E.element});
E.sortables=[];
A(G.connectToSortable).each(function(){var H=A.data(this,"sortable");
if(H&&!H.options.disabled){E.sortables.push({instance:H,shouldRevert:H.options.revert});
H.refreshPositions();
H._trigger("activate",D,C)
}})
},stop:function(D,F){var E=A(this).data("draggable"),C=A.extend({},F,{item:E.element});
A.each(E.sortables,function(){if(this.instance.isOver){this.instance.isOver=0;
E.cancelHelperRemoval=true;
this.instance.cancelHelperRemoval=false;
if(this.shouldRevert){this.instance.options.revert=true
}this.instance._mouseStop(D);
this.instance.options.helper=this.instance.options._helper;
if(E.options.helper=="original"){this.instance.currentItem.css({top:"auto",left:"auto"})
}}else{this.instance.cancelHelperRemoval=false;
this.instance._trigger("deactivate",D,C)
}})
},drag:function(D,G){var F=A(this).data("draggable"),C=this;
var E=function(J){var O=this.offset.click.top,N=this.offset.click.left;
var H=this.positionAbs.top,L=this.positionAbs.left;
var K=J.height,M=J.width;
var P=J.top,I=J.left;
return A.ui.isOver(H+O,L+N,P,I,K,M)
};
A.each(F.sortables,function(I){var H=false;
var J=this;
this.instance.positionAbs=F.positionAbs;
this.instance.helperProportions=F.helperProportions;
this.instance.offset.click=F.offset.click;
if(this.instance._intersectsWith(this.instance.containerCache)){H=true;
A.each(F.sortables,function(){this.instance.positionAbs=F.positionAbs;
this.instance.helperProportions=F.helperProportions;
this.instance.offset.click=F.offset.click;
if(this!=J&&this.instance._intersectsWith(this.instance.containerCache)&&A.ui.contains(J.instance.element[0],this.instance.element[0])){H=false
}return H
})
}if(H){if(!this.instance.isOver){this.instance.isOver=1;
this.instance.currentItem=A(C).clone().removeAttr("id").appendTo(this.instance.element).data("sortable-item",true);
this.instance.options._helper=this.instance.options.helper;
this.instance.options.helper=function(){return G.helper[0]
};
D.target=this.instance.currentItem[0];
this.instance._mouseCapture(D,true);
this.instance._mouseStart(D,true,true);
this.instance.offset.click.top=F.offset.click.top;
this.instance.offset.click.left=F.offset.click.left;
this.instance.offset.parent.left-=F.offset.parent.left-this.instance.offset.parent.left;
this.instance.offset.parent.top-=F.offset.parent.top-this.instance.offset.parent.top;
F._trigger("toSortable",D);
F.dropped=this.instance.element;
F.currentItem=F.element;
this.instance.fromOutside=F
}if(this.instance.currentItem){this.instance._mouseDrag(D)
}}else{if(this.instance.isOver){this.instance.isOver=0;
this.instance.cancelHelperRemoval=true;
this.instance.options.revert=false;
this.instance._trigger("out",D,this.instance._uiHash(this.instance));
this.instance._mouseStop(D,true);
this.instance.options.helper=this.instance.options._helper;
this.instance.currentItem.remove();
if(this.instance.placeholder){this.instance.placeholder.remove()
}F._trigger("fromSortable",D);
F.dropped=false
}}})
}});
A.ui.plugin.add("draggable","cursor",{start:function(D,E){var C=A("body"),F=A(this).data("draggable").options;
if(C.css("cursor")){F._cursor=C.css("cursor")
}C.css("cursor",F.cursor)
},stop:function(C,D){var E=A(this).data("draggable").options;
if(E._cursor){A("body").css("cursor",E._cursor)
}}});
A.ui.plugin.add("draggable","opacity",{start:function(D,E){var C=A(E.helper),F=A(this).data("draggable").options;
if(C.css("opacity")){F._opacity=C.css("opacity")
}C.css("opacity",F.opacity)
},stop:function(C,D){var E=A(this).data("draggable").options;
if(E._opacity){A(D.helper).css("opacity",E._opacity)
}}});
A.ui.plugin.add("draggable","scroll",{start:function(D,E){var C=A(this).data("draggable");
if(C.scrollParent[0]!=document&&C.scrollParent[0].tagName!="HTML"){C.overflowOffset=C.scrollParent.offset()
}},drag:function(E,F){var D=A(this).data("draggable"),G=D.options,C=false;
if(D.scrollParent[0]!=document&&D.scrollParent[0].tagName!="HTML"){if(!G.axis||G.axis!="x"){if((D.overflowOffset.top+D.scrollParent[0].offsetHeight)-E.pageY<G.scrollSensitivity){D.scrollParent[0].scrollTop=C=D.scrollParent[0].scrollTop+G.scrollSpeed
}else{if(E.pageY-D.overflowOffset.top<G.scrollSensitivity){D.scrollParent[0].scrollTop=C=D.scrollParent[0].scrollTop-G.scrollSpeed
}}}if(!G.axis||G.axis!="y"){if((D.overflowOffset.left+D.scrollParent[0].offsetWidth)-E.pageX<G.scrollSensitivity){D.scrollParent[0].scrollLeft=C=D.scrollParent[0].scrollLeft+G.scrollSpeed
}else{if(E.pageX-D.overflowOffset.left<G.scrollSensitivity){D.scrollParent[0].scrollLeft=C=D.scrollParent[0].scrollLeft-G.scrollSpeed
}}}}else{if(!G.axis||G.axis!="x"){if(E.pageY-A(document).scrollTop()<G.scrollSensitivity){C=A(document).scrollTop(A(document).scrollTop()-G.scrollSpeed)
}else{if(A(window).height()-(E.pageY-A(document).scrollTop())<G.scrollSensitivity){C=A(document).scrollTop(A(document).scrollTop()+G.scrollSpeed)
}}}if(!G.axis||G.axis!="y"){if(E.pageX-A(document).scrollLeft()<G.scrollSensitivity){C=A(document).scrollLeft(A(document).scrollLeft()-G.scrollSpeed)
}else{if(A(window).width()-(E.pageX-A(document).scrollLeft())<G.scrollSensitivity){C=A(document).scrollLeft(A(document).scrollLeft()+G.scrollSpeed)
}}}}if(C!==false&&A.ui.ddmanager&&!G.dropBehaviour){A.ui.ddmanager.prepareOffsets(D,E)
}}});
A.ui.plugin.add("draggable","snap",{start:function(D,E){var C=A(this).data("draggable"),F=C.options;
C.snapElements=[];
A(F.snap.constructor!=String?(F.snap.items||":data(draggable)"):F.snap).each(function(){var H=A(this);
var G=H.offset();
if(this!=C.element[0]){C.snapElements.push({item:this,width:H.outerWidth(),height:H.outerHeight(),top:G.top,left:G.left})
}})
},drag:function(O,L){var F=A(this).data("draggable"),M=F.options;
var S=M.snapTolerance;
var R=L.offset.left,Q=R+F.helperProportions.width,E=L.offset.top,D=E+F.helperProportions.height;
for(var P=F.snapElements.length-1;
P>=0;
P--){var N=F.snapElements[P].left,K=N+F.snapElements[P].width,J=F.snapElements[P].top,U=J+F.snapElements[P].height;
if(!((N-S<R&&R<K+S&&J-S<E&&E<U+S)||(N-S<R&&R<K+S&&J-S<D&&D<U+S)||(N-S<Q&&Q<K+S&&J-S<E&&E<U+S)||(N-S<Q&&Q<K+S&&J-S<D&&D<U+S))){if(F.snapElements[P].snapping){(F.options.snap.release&&F.options.snap.release.call(F.element,O,A.extend(F._uiHash(),{snapItem:F.snapElements[P].item})))
}F.snapElements[P].snapping=false;
continue
}if(M.snapMode!="inner"){var C=Math.abs(J-D)<=S;
var T=Math.abs(U-E)<=S;
var H=Math.abs(N-Q)<=S;
var I=Math.abs(K-R)<=S;
if(C){L.position.top=F._convertPositionTo("relative",{top:J-F.helperProportions.height,left:0}).top-F.margins.top
}if(T){L.position.top=F._convertPositionTo("relative",{top:U,left:0}).top-F.margins.top
}if(H){L.position.left=F._convertPositionTo("relative",{top:0,left:N-F.helperProportions.width}).left-F.margins.left
}if(I){L.position.left=F._convertPositionTo("relative",{top:0,left:K}).left-F.margins.left
}}var G=(C||T||H||I);
if(M.snapMode!="outer"){var C=Math.abs(J-E)<=S;
var T=Math.abs(U-D)<=S;
var H=Math.abs(N-R)<=S;
var I=Math.abs(K-Q)<=S;
if(C){L.position.top=F._convertPositionTo("relative",{top:J,left:0}).top-F.margins.top
}if(T){L.position.top=F._convertPositionTo("relative",{top:U-F.helperProportions.height,left:0}).top-F.margins.top
}if(H){L.position.left=F._convertPositionTo("relative",{top:0,left:N}).left-F.margins.left
}if(I){L.position.left=F._convertPositionTo("relative",{top:0,left:K-F.helperProportions.width}).left-F.margins.left
}}if(!F.snapElements[P].snapping&&(C||T||H||I||G)){(F.options.snap.snap&&F.options.snap.snap.call(F.element,O,A.extend(F._uiHash(),{snapItem:F.snapElements[P].item})))
}F.snapElements[P].snapping=(C||T||H||I||G)
}}});
A.ui.plugin.add("draggable","stack",{start:function(D,E){var G=A(this).data("draggable").options;
var F=A.makeArray(A(G.stack)).sort(function(I,H){return(parseInt(A(I).css("zIndex"),10)||0)-(parseInt(A(H).css("zIndex"),10)||0)
});
if(!F.length){return 
}var C=parseInt(F[0].style.zIndex)||0;
A(F).each(function(H){this.style.zIndex=C+H
});
this[0].style.zIndex=C+F.length
}});
A.ui.plugin.add("draggable","zIndex",{start:function(D,E){var C=A(E.helper),F=A(this).data("draggable").options;
if(C.css("zIndex")){F._zIndex=C.css("zIndex")
}C.css("zIndex",F.zIndex)
},stop:function(C,D){var E=A(this).data("draggable").options;
if(E._zIndex){A(D.helper).css("zIndex",E._zIndex)
}}})
})(jQuery);
(function(A,B){A.widget("ui.droppable",{version:"1.9.1",widgetEventPrefix:"drop",options:{accept:"*",activeClass:false,addClasses:true,greedy:false,hoverClass:false,scope:"default",tolerance:"intersect"},_create:function(){var D=this.options,C=D.accept;
this.isover=0;
this.isout=1;
this.accept=A.isFunction(C)?C:function(E){return E.is(C)
};
this.proportions={width:this.element[0].offsetWidth,height:this.element[0].offsetHeight};
A.ui.ddmanager.droppables[D.scope]=A.ui.ddmanager.droppables[D.scope]||[];
A.ui.ddmanager.droppables[D.scope].push(this);
(D.addClasses&&this.element.addClass("ui-droppable"))
},_destroy:function(){var C=A.ui.ddmanager.droppables[this.options.scope];
for(var D=0;
D<C.length;
D++){if(C[D]==this){C.splice(D,1)
}}this.element.removeClass("ui-droppable ui-droppable-disabled")
},_setOption:function(C,D){if(C=="accept"){this.accept=A.isFunction(D)?D:function(E){return E.is(D)
}
}A.Widget.prototype._setOption.apply(this,arguments)
},_activate:function(D){var C=A.ui.ddmanager.current;
if(this.options.activeClass){this.element.addClass(this.options.activeClass)
}(C&&this._trigger("activate",D,this.ui(C)))
},_deactivate:function(D){var C=A.ui.ddmanager.current;
if(this.options.activeClass){this.element.removeClass(this.options.activeClass)
}(C&&this._trigger("deactivate",D,this.ui(C)))
},_over:function(D){var C=A.ui.ddmanager.current;
if(!C||(C.currentItem||C.element)[0]==this.element[0]){return 
}if(this.accept.call(this.element[0],(C.currentItem||C.element))){if(this.options.hoverClass){this.element.addClass(this.options.hoverClass)
}this._trigger("over",D,this.ui(C))
}},_out:function(D){var C=A.ui.ddmanager.current;
if(!C||(C.currentItem||C.element)[0]==this.element[0]){return 
}if(this.accept.call(this.element[0],(C.currentItem||C.element))){if(this.options.hoverClass){this.element.removeClass(this.options.hoverClass)
}this._trigger("out",D,this.ui(C))
}},_drop:function(D,E){var C=E||A.ui.ddmanager.current;
if(!C||(C.currentItem||C.element)[0]==this.element[0]){return false
}var F=false;
this.element.find(":data(droppable)").not(".ui-draggable-dragging").each(function(){var G=A.data(this,"droppable");
if(G.options.greedy&&!G.options.disabled&&G.options.scope==C.options.scope&&G.accept.call(G.element[0],(C.currentItem||C.element))&&A.ui.intersect(C,A.extend(G,{offset:G.element.offset()}),G.options.tolerance)){F=true;
return false
}});
if(F){return false
}if(this.accept.call(this.element[0],(C.currentItem||C.element))){if(this.options.activeClass){this.element.removeClass(this.options.activeClass)
}if(this.options.hoverClass){this.element.removeClass(this.options.hoverClass)
}this._trigger("drop",D,this.ui(C));
return this.element
}return false
},ui:function(C){return{draggable:(C.currentItem||C.element),helper:C.helper,position:C.position,offset:C.positionAbs}
}});
A.ui.intersect=function(P,J,N){if(!J.offset){return false
}var E=(P.positionAbs||P.position.absolute).left,D=E+P.helperProportions.width,M=(P.positionAbs||P.position.absolute).top,L=M+P.helperProportions.height;
var G=J.offset.left,C=G+J.proportions.width,O=J.offset.top,K=O+J.proportions.height;
switch(N){case"fit":return(G<=E&&D<=C&&O<=M&&L<=K);
break;
case"intersect":return(G<E+(P.helperProportions.width/2)&&D-(P.helperProportions.width/2)<C&&O<M+(P.helperProportions.height/2)&&L-(P.helperProportions.height/2)<K);
break;
case"pointer":var H=((P.positionAbs||P.position.absolute).left+(P.clickOffset||P.offset.click).left),I=((P.positionAbs||P.position.absolute).top+(P.clickOffset||P.offset.click).top),F=A.ui.isOver(I,H,O,G,J.proportions.height,J.proportions.width);
return F;
break;
case"touch":return((M>=O&&M<=K)||(L>=O&&L<=K)||(M<O&&L>K))&&((E>=G&&E<=C)||(D>=G&&D<=C)||(E<G&&D>C));
break;
default:return false;
break
}};
A.ui.ddmanager={current:null,droppables:{"default":[]},prepareOffsets:function(F,H){var C=A.ui.ddmanager.droppables[F.options.scope]||[];
var G=H?H.type:null;
var I=(F.currentItem||F.element).find(":data(droppable)").andSelf();
droppablesLoop:for(var E=0;
E<C.length;
E++){if(C[E].options.disabled||(F&&!C[E].accept.call(C[E].element[0],(F.currentItem||F.element)))){continue
}for(var D=0;
D<I.length;
D++){if(I[D]==C[E].element[0]){C[E].proportions.height=0;
continue droppablesLoop
}}C[E].visible=C[E].element.css("display")!="none";
if(!C[E].visible){continue
}if(G=="mousedown"){C[E]._activate.call(C[E],H)
}C[E].offset=C[E].element.offset();
C[E].proportions={width:C[E].element[0].offsetWidth,height:C[E].element[0].offsetHeight}
}},drop:function(C,D){var E=false;
A.each(A.ui.ddmanager.droppables[C.options.scope]||[],function(){if(!this.options){return 
}if(!this.options.disabled&&this.visible&&A.ui.intersect(C,this,this.options.tolerance)){E=this._drop.call(this,D)||E
}if(!this.options.disabled&&this.visible&&this.accept.call(this.element[0],(C.currentItem||C.element))){this.isout=1;
this.isover=0;
this._deactivate.call(this,D)
}});
return E
},dragStart:function(C,D){C.element.parentsUntil("body").bind("scroll.droppable",function(){if(!C.options.refreshPositions){A.ui.ddmanager.prepareOffsets(C,D)
}})
},drag:function(C,D){if(C.options.refreshPositions){A.ui.ddmanager.prepareOffsets(C,D)
}A.each(A.ui.ddmanager.droppables[C.options.scope]||[],function(){if(this.options.disabled||this.greedyChild||!this.visible){return 
}var G=A.ui.intersect(C,this,this.options.tolerance);
var I=!G&&this.isover==1?"isout":(G&&this.isover==0?"isover":null);
if(!I){return 
}var H;
if(this.options.greedy){var F=this.options.scope;
var E=this.element.parents(":data(droppable)").filter(function(){return A.data(this,"droppable").options.scope===F
});
if(E.length){H=A.data(E[0],"droppable");
H.greedyChild=(I=="isover"?1:0)
}}if(H&&I=="isover"){H.isover=0;
H.isout=1;
H._out.call(H,D)
}this[I]=1;
this[I=="isout"?"isover":"isout"]=0;
this[I=="isover"?"_over":"_out"].call(this,D);
if(H&&I=="isout"){H.isout=0;
H.isover=1;
H._over.call(H,D)
}})
},dragStop:function(C,D){C.element.parentsUntil("body").unbind("scroll.droppable");
if(!C.options.refreshPositions){A.ui.ddmanager.prepareOffsets(C,D)
}}}
})(jQuery);
(function(C,D){C.widget("ui.resizable",C.ui.mouse,{version:"1.9.1",widgetEventPrefix:"resize",options:{alsoResize:false,animate:false,animateDuration:"slow",animateEasing:"swing",aspectRatio:false,autoHide:false,containment:false,ghost:false,grid:false,handles:"e,s,se",helper:false,maxHeight:null,maxWidth:null,minHeight:10,minWidth:10,zIndex:1000},_create:function(){var H=this,J=this.options;
this.element.addClass("ui-resizable");
C.extend(this,{_aspectRatio:!!(J.aspectRatio),aspectRatio:J.aspectRatio,originalElement:this.element,_proportionallyResizeElements:[],_helper:J.helper||J.ghost||J.animate?J.helper||"ui-resizable-helper":null});
if(this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i)){this.element.wrap(C('<div class="ui-wrapper" style="overflow: hidden;"></div>').css({position:this.element.css("position"),width:this.element.outerWidth(),height:this.element.outerHeight(),top:this.element.css("top"),left:this.element.css("left")}));
this.element=this.element.parent().data("resizable",this.element.data("resizable"));
this.elementIsWrapper=true;
this.element.css({marginLeft:this.originalElement.css("marginLeft"),marginTop:this.originalElement.css("marginTop"),marginRight:this.originalElement.css("marginRight"),marginBottom:this.originalElement.css("marginBottom")});
this.originalElement.css({marginLeft:0,marginTop:0,marginRight:0,marginBottom:0});
this.originalResizeStyle=this.originalElement.css("resize");
this.originalElement.css("resize","none");
this._proportionallyResizeElements.push(this.originalElement.css({position:"static",zoom:1,display:"block"}));
this.originalElement.css({margin:this.originalElement.css("margin")});
this._proportionallyResize()
}this.handles=J.handles||(!C(".ui-resizable-handle",this.element).length?"e,s,se":{n:".ui-resizable-n",e:".ui-resizable-e",s:".ui-resizable-s",w:".ui-resizable-w",se:".ui-resizable-se",sw:".ui-resizable-sw",ne:".ui-resizable-ne",nw:".ui-resizable-nw"});
if(this.handles.constructor==String){if(this.handles=="all"){this.handles="n,e,s,w,se,sw,ne,nw"
}var K=this.handles.split(",");
this.handles={};
for(var F=0;
F<K.length;
F++){var I=C.trim(K[F]),E="ui-resizable-"+I;
var G=C('<div class="ui-resizable-handle '+E+'"></div>');
G.css({zIndex:J.zIndex});
if("se"==I){G.addClass("ui-icon ui-icon-gripsmall-diagonal-se")
}this.handles[I]=".ui-resizable-"+I;
this.element.append(G)
}}this._renderAxis=function(P){P=P||this.element;
for(var M in this.handles){if(this.handles[M].constructor==String){this.handles[M]=C(this.handles[M],this.element).show()
}if(this.elementIsWrapper&&this.originalElement[0].nodeName.match(/textarea|input|select|button/i)){var N=C(this.handles[M],this.element),O=0;
O=/sw|ne|nw|se|n|s/.test(M)?N.outerHeight():N.outerWidth();
var L=["padding",/ne|nw|n/.test(M)?"Top":/se|sw|s/.test(M)?"Bottom":/^e$/.test(M)?"Right":"Left"].join("");
P.css(L,O);
this._proportionallyResize()
}if(!C(this.handles[M]).length){continue
}}};
this._renderAxis(this.element);
this._handles=C(".ui-resizable-handle",this.element).disableSelection();
this._handles.mouseover(function(){if(!H.resizing){if(this.className){var L=this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i)
}H.axis=L&&L[1]?L[1]:"se"
}});
if(J.autoHide){this._handles.hide();
C(this.element).addClass("ui-resizable-autohide").mouseenter(function(){if(J.disabled){return 
}C(this).removeClass("ui-resizable-autohide");
H._handles.show()
}).mouseleave(function(){if(J.disabled){return 
}if(!H.resizing){C(this).addClass("ui-resizable-autohide");
H._handles.hide()
}})
}this._mouseInit()
},_destroy:function(){this._mouseDestroy();
var E=function(G){C(G).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing").removeData("resizable").removeData("ui-resizable").unbind(".resizable").find(".ui-resizable-handle").remove()
};
if(this.elementIsWrapper){E(this.element);
var F=this.element;
this.originalElement.css({position:F.css("position"),width:F.outerWidth(),height:F.outerHeight(),top:F.css("top"),left:F.css("left")}).insertAfter(F);
F.remove()
}this.originalElement.css("resize",this.originalResizeStyle);
E(this.originalElement);
return this
},_mouseCapture:function(F){var G=false;
for(var E in this.handles){if(C(this.handles[E])[0]==F.target){G=true
}}return !this.options.disabled&&G
},_mouseStart:function(G){var J=this.options,F=this.element.position(),E=this.element;
this.resizing=true;
this.documentScroll={top:C(document).scrollTop(),left:C(document).scrollLeft()};
if(E.is(".ui-draggable")||(/absolute/).test(E.css("position"))){E.css({position:"absolute",top:F.top,left:F.left})
}this._renderProxy();
var K=B(this.helper.css("left")),H=B(this.helper.css("top"));
if(J.containment){K+=C(J.containment).scrollLeft()||0;
H+=C(J.containment).scrollTop()||0
}this.offset=this.helper.offset();
this.position={left:K,top:H};
this.size=this._helper?{width:E.outerWidth(),height:E.outerHeight()}:{width:E.width(),height:E.height()};
this.originalSize=this._helper?{width:E.outerWidth(),height:E.outerHeight()}:{width:E.width(),height:E.height()};
this.originalPosition={left:K,top:H};
this.sizeDiff={width:E.outerWidth()-E.width(),height:E.outerHeight()-E.height()};
this.originalMousePosition={left:G.pageX,top:G.pageY};
this.aspectRatio=(typeof J.aspectRatio=="number")?J.aspectRatio:((this.originalSize.width/this.originalSize.height)||1);
var I=C(".ui-resizable-"+this.axis).css("cursor");
C("body").css("cursor",I=="auto"?this.axis+"-resize":I);
E.addClass("ui-resizable-resizing");
this._propagate("start",G);
return true
},_mouseDrag:function(E){var G=this.helper,F=this.options,L={},K=this,I=this.originalMousePosition,M=this.axis;
var O=(E.pageX-I.left)||0,N=(E.pageY-I.top)||0;
var H=this._change[M];
if(!H){return false
}var J=H.apply(this,[E,O,N]);
this._updateVirtualBoundaries(E.shiftKey);
if(this._aspectRatio||E.shiftKey){J=this._updateRatio(J,E)
}J=this._respectSize(J,E);
this._propagate("resize",E);
G.css({top:this.position.top+"px",left:this.position.left+"px",width:this.size.width+"px",height:this.size.height+"px"});
if(!this._helper&&this._proportionallyResizeElements.length){this._proportionallyResize()
}this._updateCache(J);
this._trigger("resize",E,this.ui());
return false
},_mouseStop:function(H){this.resizing=false;
var I=this.options,L=this;
if(this._helper){var G=this._proportionallyResizeElements,E=G.length&&(/textarea/i).test(G[0].nodeName),F=E&&C.ui.hasScroll(G[0],"left")?0:L.sizeDiff.height,K=E?0:L.sizeDiff.width;
var N={width:(L.helper.width()-K),height:(L.helper.height()-F)},J=(parseInt(L.element.css("left"),10)+(L.position.left-L.originalPosition.left))||null,M=(parseInt(L.element.css("top"),10)+(L.position.top-L.originalPosition.top))||null;
if(!I.animate){this.element.css(C.extend(N,{top:M,left:J}))
}L.helper.height(L.size.height);
L.helper.width(L.size.width);
if(this._helper&&!I.animate){this._proportionallyResize()
}}C("body").css("cursor","auto");
this.element.removeClass("ui-resizable-resizing");
this._propagate("stop",H);
if(this._helper){this.helper.remove()
}return false
},_updateVirtualBoundaries:function(G){var J=this.options,I,H,F,K,E;
E={minWidth:A(J.minWidth)?J.minWidth:0,maxWidth:A(J.maxWidth)?J.maxWidth:Infinity,minHeight:A(J.minHeight)?J.minHeight:0,maxHeight:A(J.maxHeight)?J.maxHeight:Infinity};
if(this._aspectRatio||G){I=E.minHeight*this.aspectRatio;
F=E.minWidth/this.aspectRatio;
H=E.maxHeight*this.aspectRatio;
K=E.maxWidth/this.aspectRatio;
if(I>E.minWidth){E.minWidth=I
}if(F>E.minHeight){E.minHeight=F
}if(H<E.maxWidth){E.maxWidth=H
}if(K<E.maxHeight){E.maxHeight=K
}}this._vBoundaries=E
},_updateCache:function(E){var F=this.options;
this.offset=this.helper.offset();
if(A(E.left)){this.position.left=E.left
}if(A(E.top)){this.position.top=E.top
}if(A(E.height)){this.size.height=E.height
}if(A(E.width)){this.size.width=E.width
}},_updateRatio:function(H,G){var I=this.options,J=this.position,F=this.size,E=this.axis;
if(A(H.height)){H.width=(H.height*this.aspectRatio)
}else{if(A(H.width)){H.height=(H.width/this.aspectRatio)
}}if(E=="sw"){H.left=J.left+(F.width-H.width);
H.top=null
}if(E=="nw"){H.top=J.top+(F.height-H.height);
H.left=J.left+(F.width-H.width)
}return H
},_respectSize:function(L,G){var J=this.helper,I=this._vBoundaries,Q=this._aspectRatio||G.shiftKey,P=this.axis,S=A(L.width)&&I.maxWidth&&(I.maxWidth<L.width),M=A(L.height)&&I.maxHeight&&(I.maxHeight<L.height),H=A(L.width)&&I.minWidth&&(I.minWidth>L.width),R=A(L.height)&&I.minHeight&&(I.minHeight>L.height);
if(H){L.width=I.minWidth
}if(R){L.height=I.minHeight
}if(S){L.width=I.maxWidth
}if(M){L.height=I.maxHeight
}var F=this.originalPosition.left+this.originalSize.width,O=this.position.top+this.size.height;
var K=/sw|nw|w/.test(P),E=/nw|ne|n/.test(P);
if(H&&K){L.left=F-I.minWidth
}if(S&&K){L.left=F-I.maxWidth
}if(R&&E){L.top=O-I.minHeight
}if(M&&E){L.top=O-I.maxHeight
}var N=!L.width&&!L.height;
if(N&&!L.left&&L.top){L.top=null
}else{if(N&&!L.top&&L.left){L.left=null
}}return L
},_proportionallyResize:function(){var J=this.options;
if(!this._proportionallyResizeElements.length){return 
}var G=this.helper||this.element;
for(var F=0;
F<this._proportionallyResizeElements.length;
F++){var H=this._proportionallyResizeElements[F];
if(!this.borderDif){var E=[H.css("borderTopWidth"),H.css("borderRightWidth"),H.css("borderBottomWidth"),H.css("borderLeftWidth")],I=[H.css("paddingTop"),H.css("paddingRight"),H.css("paddingBottom"),H.css("paddingLeft")];
this.borderDif=C.map(E,function(K,M){var L=parseInt(K,10)||0,N=parseInt(I[M],10)||0;
return L+N
})
}H.css({height:(G.height()-this.borderDif[0]-this.borderDif[2])||0,width:(G.width()-this.borderDif[1]-this.borderDif[3])||0})
}},_renderProxy:function(){var E=this.element,H=this.options;
this.elementOffset=E.offset();
if(this._helper){this.helper=this.helper||C('<div style="overflow:hidden;"></div>');
var F=(C.ui.ie6?1:0),G=(C.ui.ie6?2:-1);
this.helper.addClass(this._helper).css({width:this.element.outerWidth()+G,height:this.element.outerHeight()+G,position:"absolute",left:this.elementOffset.left-F+"px",top:this.elementOffset.top-F+"px",zIndex:++H.zIndex});
this.helper.appendTo("body").disableSelection()
}else{this.helper=this.element
}},_change:{e:function(G,F,E){return{width:this.originalSize.width+F}
},w:function(H,F,E){var J=this.options,G=this.originalSize,I=this.originalPosition;
return{left:I.left+F,width:G.width-F}
},n:function(H,F,E){var J=this.options,G=this.originalSize,I=this.originalPosition;
return{top:I.top+E,height:G.height-E}
},s:function(G,F,E){return{height:this.originalSize.height+E}
},se:function(G,F,E){return C.extend(this._change.s.apply(this,arguments),this._change.e.apply(this,[G,F,E]))
},sw:function(G,F,E){return C.extend(this._change.s.apply(this,arguments),this._change.w.apply(this,[G,F,E]))
},ne:function(G,F,E){return C.extend(this._change.n.apply(this,arguments),this._change.e.apply(this,[G,F,E]))
},nw:function(G,F,E){return C.extend(this._change.n.apply(this,arguments),this._change.w.apply(this,[G,F,E]))
}},_propagate:function(F,E){C.ui.plugin.call(this,F,[E,this.ui()]);
(F!="resize"&&this._trigger(F,E,this.ui()))
},plugins:{},ui:function(){return{originalElement:this.originalElement,element:this.element,helper:this.helper,position:this.position,size:this.size,originalSize:this.originalSize,originalPosition:this.originalPosition}
}});
C.ui.plugin.add("resizable","alsoResize",{start:function(F,G){var E=C(this).data("resizable"),I=E.options;
var H=function(J){C(J).each(function(){var K=C(this);
K.data("resizable-alsoresize",{width:parseInt(K.width(),10),height:parseInt(K.height(),10),left:parseInt(K.css("left"),10),top:parseInt(K.css("top"),10)})
})
};
if(typeof (I.alsoResize)=="object"&&!I.alsoResize.parentNode){if(I.alsoResize.length){I.alsoResize=I.alsoResize[0];
H(I.alsoResize)
}else{C.each(I.alsoResize,function(J){H(J)
})
}}else{H(I.alsoResize)
}},resize:function(G,I){var F=C(this).data("resizable"),J=F.options,H=F.originalSize,L=F.originalPosition;
var K={height:(F.size.height-H.height)||0,width:(F.size.width-H.width)||0,top:(F.position.top-L.top)||0,left:(F.position.left-L.left)||0},E=function(M,N){C(M).each(function(){var Q=C(this),R=C(this).data("resizable-alsoresize"),P={},O=N&&N.length?N:Q.parents(I.originalElement[0]).length?["width","height"]:["width","height","top","left"];
C.each(O,function(S,U){var T=(R[U]||0)+(K[U]||0);
if(T&&T>=0){P[U]=T||null
}});
Q.css(P)
})
};
if(typeof (J.alsoResize)=="object"&&!J.alsoResize.nodeType){C.each(J.alsoResize,function(M,N){E(M,N)
})
}else{E(J.alsoResize)
}},stop:function(E,F){C(this).removeData("resizable-alsoresize")
}});
C.ui.plugin.add("resizable","animate",{stop:function(I,O){var M=C(this).data("resizable"),J=M.options;
var H=M._proportionallyResizeElements,E=H.length&&(/textarea/i).test(H[0].nodeName),F=E&&C.ui.hasScroll(H[0],"left")?0:M.sizeDiff.height,L=E?0:M.sizeDiff.width;
var G={width:(M.size.width-L),height:(M.size.height-F)},K=(parseInt(M.element.css("left"),10)+(M.position.left-M.originalPosition.left))||null,N=(parseInt(M.element.css("top"),10)+(M.position.top-M.originalPosition.top))||null;
M.element.animate(C.extend(G,N&&K?{top:N,left:K}:{}),{duration:J.animateDuration,easing:J.animateEasing,step:function(){var P={width:parseInt(M.element.css("width"),10),height:parseInt(M.element.css("height"),10),top:parseInt(M.element.css("top"),10),left:parseInt(M.element.css("left"),10)};
if(H&&H.length){C(H[0]).css({width:P.width,height:P.height})
}M._updateCache(P);
M._propagate("resize",I)
}})
}});
C.ui.plugin.add("resizable","containment",{start:function(F,Q){var O=C(this).data("resizable"),J=O.options,L=O.element;
var G=J.containment,K=(G instanceof C)?G.get(0):(/parent/.test(G))?L.parent().get(0):G;
if(!K){return 
}O.containerElement=C(K);
if(/document/.test(G)||G==document){O.containerOffset={left:0,top:0};
O.containerPosition={left:0,top:0};
O.parentData={element:C(document),left:0,top:0,width:C(document).width(),height:C(document).height()||document.body.parentNode.scrollHeight}
}else{var N=C(K),I=[];
C(["Top","Right","Left","Bottom"]).each(function(T,S){I[T]=B(N.css("padding"+S))
});
O.containerOffset=N.offset();
O.containerPosition=N.position();
O.containerSize={height:(N.innerHeight()-I[3]),width:(N.innerWidth()-I[1])};
var P=O.containerOffset,E=O.containerSize.height,M=O.containerSize.width,H=(C.ui.hasScroll(K,"left")?K.scrollWidth:M),R=(C.ui.hasScroll(K)?K.scrollHeight:E);
O.parentData={element:K,left:P.left,top:P.top,width:H,height:R}
}},resize:function(G,Q){var M=C(this).data("resizable"),I=M.options,F=M.containerSize,P=M.containerOffset,N=M.size,O=M.position,R=M._aspectRatio||G.shiftKey,E={top:0,left:0},H=M.containerElement;
if(H[0]!=document&&(/static/).test(H.css("position"))){E=P
}if(O.left<(M._helper?P.left:0)){M.size.width=M.size.width+(M._helper?(M.position.left-P.left):(M.position.left-E.left));
if(R){M.size.height=M.size.width/M.aspectRatio
}M.position.left=I.helper?P.left:0
}if(O.top<(M._helper?P.top:0)){M.size.height=M.size.height+(M._helper?(M.position.top-P.top):M.position.top);
if(R){M.size.width=M.size.height*M.aspectRatio
}M.position.top=M._helper?P.top:0
}M.offset.left=M.parentData.left+M.position.left;
M.offset.top=M.parentData.top+M.position.top;
var L=Math.abs((M._helper?M.offset.left-E.left:(M.offset.left-E.left))+M.sizeDiff.width),S=Math.abs((M._helper?M.offset.top-E.top:(M.offset.top-P.top))+M.sizeDiff.height);
var K=M.containerElement.get(0)==M.element.parent().get(0),J=/relative|absolute/.test(M.containerElement.css("position"));
if(K&&J){L-=M.parentData.left
}if(L+M.size.width>=M.parentData.width){M.size.width=M.parentData.width-L;
if(R){M.size.height=M.size.width/M.aspectRatio
}}if(S+M.size.height>=M.parentData.height){M.size.height=M.parentData.height-S;
if(R){M.size.width=M.size.height*M.aspectRatio
}}},stop:function(F,N){var K=C(this).data("resizable"),G=K.options,L=K.position,M=K.containerOffset,E=K.containerPosition,H=K.containerElement;
var I=C(K.helper),P=I.offset(),O=I.outerWidth()-K.sizeDiff.width,J=I.outerHeight()-K.sizeDiff.height;
if(K._helper&&!G.animate&&(/relative/).test(H.css("position"))){C(this).css({left:P.left-E.left-M.left,width:O,height:J})
}if(K._helper&&!G.animate&&(/static/).test(H.css("position"))){C(this).css({left:P.left-E.left-M.left,width:O,height:J})
}}});
C.ui.plugin.add("resizable","ghost",{start:function(G,H){var F=C(this).data("resizable"),I=F.options,E=F.size;
F.ghost=F.originalElement.clone();
F.ghost.css({opacity:0.25,display:"block",position:"relative",height:E.height,width:E.width,margin:0,left:0,top:0}).addClass("ui-resizable-ghost").addClass(typeof I.ghost=="string"?I.ghost:"");
F.ghost.appendTo(F.helper)
},resize:function(F,G){var E=C(this).data("resizable"),H=E.options;
if(E.ghost){E.ghost.css({position:"relative",height:E.size.height,width:E.size.width})
}},stop:function(F,G){var E=C(this).data("resizable"),H=E.options;
if(E.ghost&&E.helper){E.helper.get(0).removeChild(E.ghost.get(0))
}}});
C.ui.plugin.add("resizable","grid",{resize:function(E,N){var K=C(this).data("resizable"),H=K.options,L=K.size,I=K.originalSize,J=K.originalPosition,O=K.axis,M=H._aspectRatio||E.shiftKey;
H.grid=typeof H.grid=="number"?[H.grid,H.grid]:H.grid;
var G=Math.round((L.width-I.width)/(H.grid[0]||1))*(H.grid[0]||1),F=Math.round((L.height-I.height)/(H.grid[1]||1))*(H.grid[1]||1);
if(/^(se|s|e)$/.test(O)){K.size.width=I.width+G;
K.size.height=I.height+F
}else{if(/^(ne)$/.test(O)){K.size.width=I.width+G;
K.size.height=I.height+F;
K.position.top=J.top-F
}else{if(/^(sw)$/.test(O)){K.size.width=I.width+G;
K.size.height=I.height+F;
K.position.left=J.left-G
}else{K.size.width=I.width+G;
K.size.height=I.height+F;
K.position.top=J.top-F;
K.position.left=J.left-G
}}}}});
var B=function(E){return parseInt(E,10)||0
};
var A=function(E){return !isNaN(parseInt(E,10))
}
})(jQuery);
(function(A,B){A.widget("ui.selectable",A.ui.mouse,{version:"1.9.1",options:{appendTo:"body",autoRefresh:true,distance:0,filter:"*",tolerance:"touch"},_create:function(){var C=this;
this.element.addClass("ui-selectable");
this.dragged=false;
var D;
this.refresh=function(){D=A(C.options.filter,C.element[0]);
D.addClass("ui-selectee");
D.each(function(){var E=A(this);
var F=E.offset();
A.data(this,"selectable-item",{element:this,$element:E,left:F.left,top:F.top,right:F.left+E.outerWidth(),bottom:F.top+E.outerHeight(),startselected:false,selected:E.hasClass("ui-selected"),selecting:E.hasClass("ui-selecting"),unselecting:E.hasClass("ui-unselecting")})
})
};
this.refresh();
this.selectees=D.addClass("ui-selectee");
this._mouseInit();
this.helper=A("<div class='ui-selectable-helper'></div>")
},_destroy:function(){this.selectees.removeClass("ui-selectee").removeData("selectable-item");
this.element.removeClass("ui-selectable ui-selectable-disabled");
this._mouseDestroy()
},_mouseStart:function(E){var D=this;
this.opos=[E.pageX,E.pageY];
if(this.options.disabled){return 
}var C=this.options;
this.selectees=A(C.filter,this.element[0]);
this._trigger("start",E);
A(C.appendTo).append(this.helper);
this.helper.css({left:E.clientX,top:E.clientY,width:0,height:0});
if(C.autoRefresh){this.refresh()
}this.selectees.filter(".ui-selected").each(function(){var F=A.data(this,"selectable-item");
F.startselected=true;
if(!E.metaKey&&!E.ctrlKey){F.$element.removeClass("ui-selected");
F.selected=false;
F.$element.addClass("ui-unselecting");
F.unselecting=true;
D._trigger("unselecting",E,{unselecting:F.element})
}});
A(E.target).parents().andSelf().each(function(){var G=A.data(this,"selectable-item");
if(G){var F=(!E.metaKey&&!E.ctrlKey)||!G.$element.hasClass("ui-selected");
G.$element.removeClass(F?"ui-unselecting":"ui-selected").addClass(F?"ui-selecting":"ui-unselecting");
G.unselecting=!F;
G.selecting=F;
G.selected=F;
if(F){D._trigger("selecting",E,{selecting:G.element})
}else{D._trigger("unselecting",E,{unselecting:G.element})
}return false
}})
},_mouseDrag:function(J){var I=this;
this.dragged=true;
if(this.options.disabled){return 
}var E=this.options;
var D=this.opos[0],H=this.opos[1],C=J.pageX,G=J.pageY;
if(D>C){var F=C;
C=D;
D=F
}if(H>G){var F=G;
G=H;
H=F
}this.helper.css({left:D,top:H,width:C-D,height:G-H});
this.selectees.each(function(){var K=A.data(this,"selectable-item");
if(!K||K.element==I.element[0]){return 
}var L=false;
if(E.tolerance=="touch"){L=(!(K.left>C||K.right<D||K.top>G||K.bottom<H))
}else{if(E.tolerance=="fit"){L=(K.left>D&&K.right<C&&K.top>H&&K.bottom<G)
}}if(L){if(K.selected){K.$element.removeClass("ui-selected");
K.selected=false
}if(K.unselecting){K.$element.removeClass("ui-unselecting");
K.unselecting=false
}if(!K.selecting){K.$element.addClass("ui-selecting");
K.selecting=true;
I._trigger("selecting",J,{selecting:K.element})
}}else{if(K.selecting){if((J.metaKey||J.ctrlKey)&&K.startselected){K.$element.removeClass("ui-selecting");
K.selecting=false;
K.$element.addClass("ui-selected");
K.selected=true
}else{K.$element.removeClass("ui-selecting");
K.selecting=false;
if(K.startselected){K.$element.addClass("ui-unselecting");
K.unselecting=true
}I._trigger("unselecting",J,{unselecting:K.element})
}}if(K.selected){if(!J.metaKey&&!J.ctrlKey&&!K.startselected){K.$element.removeClass("ui-selected");
K.selected=false;
K.$element.addClass("ui-unselecting");
K.unselecting=true;
I._trigger("unselecting",J,{unselecting:K.element})
}}}});
return false
},_mouseStop:function(E){var D=this;
this.dragged=false;
var C=this.options;
A(".ui-unselecting",this.element[0]).each(function(){var F=A.data(this,"selectable-item");
F.$element.removeClass("ui-unselecting");
F.unselecting=false;
F.startselected=false;
D._trigger("unselected",E,{unselected:F.element})
});
A(".ui-selecting",this.element[0]).each(function(){var F=A.data(this,"selectable-item");
F.$element.removeClass("ui-selecting").addClass("ui-selected");
F.selecting=false;
F.selected=true;
F.startselected=true;
D._trigger("selected",E,{selected:F.element})
});
this._trigger("stop",E);
this.helper.remove();
return false
}})
})(jQuery);
(function(A,B){A.widget("ui.sortable",A.ui.mouse,{version:"1.9.1",widgetEventPrefix:"sort",ready:false,options:{appendTo:"parent",axis:false,connectWith:false,containment:false,cursor:"auto",cursorAt:false,dropOnEmpty:true,forcePlaceholderSize:false,forceHelperSize:false,grid:false,handle:false,helper:"original",items:"> *",opacity:false,placeholder:false,revert:false,scroll:true,scrollSensitivity:20,scrollSpeed:20,scope:"default",tolerance:"intersect",zIndex:1000},_create:function(){var C=this.options;
this.containerCache={};
this.element.addClass("ui-sortable");
this.refresh();
this.floating=this.items.length?C.axis==="x"||(/left|right/).test(this.items[0].item.css("float"))||(/inline|table-cell/).test(this.items[0].item.css("display")):false;
this.offset=this.element.offset();
this._mouseInit();
this.ready=true
},_destroy:function(){this.element.removeClass("ui-sortable ui-sortable-disabled");
this._mouseDestroy();
for(var C=this.items.length-1;
C>=0;
C--){this.items[C].item.removeData(this.widgetName+"-item")
}return this
},_setOption:function(C,D){if(C==="disabled"){this.options[C]=D;
this.widget().toggleClass("ui-sortable-disabled",!!D)
}else{A.Widget.prototype._setOption.apply(this,arguments)
}},_mouseCapture:function(F,G){var E=this;
if(this.reverting){return false
}if(this.options.disabled||this.options.type=="static"){return false
}this._refreshItems(F);
var D=null,C=A(F.target).parents().each(function(){if(A.data(this,E.widgetName+"-item")==E){D=A(this);
return false
}});
if(A.data(F.target,E.widgetName+"-item")==E){D=A(F.target)
}if(!D){return false
}if(this.options.handle&&!G){var H=false;
A(this.options.handle,D).find("*").andSelf().each(function(){if(this==F.target){H=true
}});
if(!H){return false
}}this.currentItem=D;
this._removeCurrentsFromItems();
return true
},_mouseStart:function(E,F,C){var G=this.options;
this.currentContainer=this;
this.refreshPositions();
this.helper=this._createHelper(E);
this._cacheHelperProportions();
this._cacheMargins();
this.scrollParent=this.helper.scrollParent();
this.offset=this.currentItem.offset();
this.offset={top:this.offset.top-this.margins.top,left:this.offset.left-this.margins.left};
A.extend(this.offset,{click:{left:E.pageX-this.offset.left,top:E.pageY-this.offset.top},parent:this._getParentOffset(),relative:this._getRelativeOffset()});
this.helper.css("position","absolute");
this.cssPosition=this.helper.css("position");
this.originalPosition=this._generatePosition(E);
this.originalPageX=E.pageX;
this.originalPageY=E.pageY;
(G.cursorAt&&this._adjustOffsetFromHelper(G.cursorAt));
this.domPosition={prev:this.currentItem.prev()[0],parent:this.currentItem.parent()[0]};
if(this.helper[0]!=this.currentItem[0]){this.currentItem.hide()
}this._createPlaceholder();
if(G.containment){this._setContainment()
}if(G.cursor){if(A("body").css("cursor")){this._storedCursor=A("body").css("cursor")
}A("body").css("cursor",G.cursor)
}if(G.opacity){if(this.helper.css("opacity")){this._storedOpacity=this.helper.css("opacity")
}this.helper.css("opacity",G.opacity)
}if(G.zIndex){if(this.helper.css("zIndex")){this._storedZIndex=this.helper.css("zIndex")
}this.helper.css("zIndex",G.zIndex)
}if(this.scrollParent[0]!=document&&this.scrollParent[0].tagName!="HTML"){this.overflowOffset=this.scrollParent.offset()
}this._trigger("start",E,this._uiHash());
if(!this._preserveHelperProportions){this._cacheHelperProportions()
}if(!C){for(var D=this.containers.length-1;
D>=0;
D--){this.containers[D]._trigger("activate",E,this._uiHash(this))
}}if(A.ui.ddmanager){A.ui.ddmanager.current=this
}if(A.ui.ddmanager&&!G.dropBehaviour){A.ui.ddmanager.prepareOffsets(this,E)
}this.dragging=true;
this.helper.addClass("ui-sortable-helper");
this._mouseDrag(E);
return true
},_mouseDrag:function(G){this.position=this._generatePosition(G);
this.positionAbs=this._convertPositionTo("absolute");
if(!this.lastPositionAbs){this.lastPositionAbs=this.positionAbs
}if(this.options.scroll){var H=this.options,C=false;
if(this.scrollParent[0]!=document&&this.scrollParent[0].tagName!="HTML"){if((this.overflowOffset.top+this.scrollParent[0].offsetHeight)-G.pageY<H.scrollSensitivity){this.scrollParent[0].scrollTop=C=this.scrollParent[0].scrollTop+H.scrollSpeed
}else{if(G.pageY-this.overflowOffset.top<H.scrollSensitivity){this.scrollParent[0].scrollTop=C=this.scrollParent[0].scrollTop-H.scrollSpeed
}}if((this.overflowOffset.left+this.scrollParent[0].offsetWidth)-G.pageX<H.scrollSensitivity){this.scrollParent[0].scrollLeft=C=this.scrollParent[0].scrollLeft+H.scrollSpeed
}else{if(G.pageX-this.overflowOffset.left<H.scrollSensitivity){this.scrollParent[0].scrollLeft=C=this.scrollParent[0].scrollLeft-H.scrollSpeed
}}}else{if(G.pageY-A(document).scrollTop()<H.scrollSensitivity){C=A(document).scrollTop(A(document).scrollTop()-H.scrollSpeed)
}else{if(A(window).height()-(G.pageY-A(document).scrollTop())<H.scrollSensitivity){C=A(document).scrollTop(A(document).scrollTop()+H.scrollSpeed)
}}if(G.pageX-A(document).scrollLeft()<H.scrollSensitivity){C=A(document).scrollLeft(A(document).scrollLeft()-H.scrollSpeed)
}else{if(A(window).width()-(G.pageX-A(document).scrollLeft())<H.scrollSensitivity){C=A(document).scrollLeft(A(document).scrollLeft()+H.scrollSpeed)
}}}if(C!==false&&A.ui.ddmanager&&!H.dropBehaviour){A.ui.ddmanager.prepareOffsets(this,G)
}}this.positionAbs=this._convertPositionTo("absolute");
if(!this.options.axis||this.options.axis!="y"){this.helper[0].style.left=this.position.left+"px"
}if(!this.options.axis||this.options.axis!="x"){this.helper[0].style.top=this.position.top+"px"
}for(var E=this.items.length-1;
E>=0;
E--){var F=this.items[E],D=F.item[0],I=this._intersectsWithPointer(F);
if(!I){continue
}if(F.instance!==this.currentContainer){continue
}if(D!=this.currentItem[0]&&this.placeholder[I==1?"next":"prev"]()[0]!=D&&!A.contains(this.placeholder[0],D)&&(this.options.type=="semi-dynamic"?!A.contains(this.element[0],D):true)){this.direction=I==1?"down":"up";
if(this.options.tolerance=="pointer"||this._intersectsWithSides(F)){this._rearrange(G,F)
}else{break
}this._trigger("change",G,this._uiHash());
break
}}this._contactContainers(G);
if(A.ui.ddmanager){A.ui.ddmanager.drag(this,G)
}this._trigger("sort",G,this._uiHash());
this.lastPositionAbs=this.positionAbs;
return false
},_mouseStop:function(D,E){if(!D){return 
}if(A.ui.ddmanager&&!this.options.dropBehaviour){A.ui.ddmanager.drop(this,D)
}if(this.options.revert){var C=this;
var F=this.placeholder.offset();
this.reverting=true;
A(this.helper).animate({left:F.left-this.offset.parent.left-this.margins.left+(this.offsetParent[0]==document.body?0:this.offsetParent[0].scrollLeft),top:F.top-this.offset.parent.top-this.margins.top+(this.offsetParent[0]==document.body?0:this.offsetParent[0].scrollTop)},parseInt(this.options.revert,10)||500,function(){C._clear(D)
})
}else{this._clear(D,E)
}return false
},cancel:function(){if(this.dragging){this._mouseUp({target:null});
if(this.options.helper=="original"){this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper")
}else{this.currentItem.show()
}for(var C=this.containers.length-1;
C>=0;
C--){this.containers[C]._trigger("deactivate",null,this._uiHash(this));
if(this.containers[C].containerCache.over){this.containers[C]._trigger("out",null,this._uiHash(this));
this.containers[C].containerCache.over=0
}}}if(this.placeholder){if(this.placeholder[0].parentNode){this.placeholder[0].parentNode.removeChild(this.placeholder[0])
}if(this.options.helper!="original"&&this.helper&&this.helper[0].parentNode){this.helper.remove()
}A.extend(this,{helper:null,dragging:false,reverting:false,_noFinalSort:null});
if(this.domPosition.prev){A(this.domPosition.prev).after(this.currentItem)
}else{A(this.domPosition.parent).prepend(this.currentItem)
}}return this
},serialize:function(E){var C=this._getItemsAsjQuery(E&&E.connected);
var D=[];
E=E||{};
A(C).each(function(){var F=(A(E.item||this).attr(E.attribute||"id")||"").match(E.expression||(/(.+)[-=_](.+)/));
if(F){D.push((E.key||F[1]+"[]")+"="+(E.key&&E.expression?F[1]:F[2]))
}});
if(!D.length&&E.key){D.push(E.key+"=")
}return D.join("&")
},toArray:function(E){var C=this._getItemsAsjQuery(E&&E.connected);
var D=[];
E=E||{};
C.each(function(){D.push(A(E.item||this).attr(E.attribute||"id")||"")
});
return D
},_intersectsWith:function(L){var E=this.positionAbs.left,D=E+this.helperProportions.width,K=this.positionAbs.top,J=K+this.helperProportions.height;
var F=L.left,C=F+L.width,M=L.top,I=M+L.height;
var N=this.offset.click.top,H=this.offset.click.left;
var G=(K+N)>M&&(K+N)<I&&(E+H)>F&&(E+H)<C;
if(this.options.tolerance=="pointer"||this.options.forcePointerForContainers||(this.options.tolerance!="pointer"&&this.helperProportions[this.floating?"width":"height"]>L[this.floating?"width":"height"])){return G
}else{return(F<E+(this.helperProportions.width/2)&&D-(this.helperProportions.width/2)<C&&M<K+(this.helperProportions.height/2)&&J-(this.helperProportions.height/2)<I)
}},_intersectsWithPointer:function(E){var F=(this.options.axis==="x")||A.ui.isOverAxis(this.positionAbs.top+this.offset.click.top,E.top,E.height),D=(this.options.axis==="y")||A.ui.isOverAxis(this.positionAbs.left+this.offset.click.left,E.left,E.width),H=F&&D,C=this._getDragVerticalDirection(),G=this._getDragHorizontalDirection();
if(!H){return false
}return this.floating?(((G&&G=="right")||C=="down")?2:1):(C&&(C=="down"?2:1))
},_intersectsWithSides:function(F){var D=A.ui.isOverAxis(this.positionAbs.top+this.offset.click.top,F.top+(F.height/2),F.height),E=A.ui.isOverAxis(this.positionAbs.left+this.offset.click.left,F.left+(F.width/2),F.width),C=this._getDragVerticalDirection(),G=this._getDragHorizontalDirection();
if(this.floating&&G){return((G=="right"&&E)||(G=="left"&&!E))
}else{return C&&((C=="down"&&D)||(C=="up"&&!D))
}},_getDragVerticalDirection:function(){var C=this.positionAbs.top-this.lastPositionAbs.top;
return C!=0&&(C>0?"down":"up")
},_getDragHorizontalDirection:function(){var C=this.positionAbs.left-this.lastPositionAbs.left;
return C!=0&&(C>0?"right":"left")
},refresh:function(C){this._refreshItems(C);
this.refreshPositions();
return this
},_connectWith:function(){var C=this.options;
return C.connectWith.constructor==String?[C.connectWith]:C.connectWith
},_getItemsAsjQuery:function(H){var C=[];
var E=[];
var G=this._connectWith();
if(G&&H){for(var F=G.length-1;
F>=0;
F--){var J=A(G[F]);
for(var D=J.length-1;
D>=0;
D--){var I=A.data(J[D],this.widgetName);
if(I&&I!=this&&!I.options.disabled){E.push([A.isFunction(I.options.items)?I.options.items.call(I.element):A(I.options.items,I.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),I])
}}}}E.push([A.isFunction(this.options.items)?this.options.items.call(this.element,null,{options:this.options,item:this.currentItem}):A(this.options.items,this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),this]);
for(var F=E.length-1;
F>=0;
F--){E[F][0].each(function(){C.push(this)
})
}return A(C)
},_removeCurrentsFromItems:function(){var C=this.currentItem.find(":data("+this.widgetName+"-item)");
this.items=A.grep(this.items,function(E){for(var D=0;
D<C.length;
D++){if(C[D]==E.item[0]){return false
}}return true
})
},_refreshItems:function(C){this.items=[];
this.containers=[this];
var I=this.items;
var G=[[A.isFunction(this.options.items)?this.options.items.call(this.element[0],C,{item:this.currentItem}):A(this.options.items,this.element),this]];
var K=this._connectWith();
if(K&&this.ready){for(var F=K.length-1;
F>=0;
F--){var L=A(K[F]);
for(var E=L.length-1;
E>=0;
E--){var H=A.data(L[E],this.widgetName);
if(H&&H!=this&&!H.options.disabled){G.push([A.isFunction(H.options.items)?H.options.items.call(H.element[0],C,{item:this.currentItem}):A(H.options.items,H.element),H]);
this.containers.push(H)
}}}}for(var F=G.length-1;
F>=0;
F--){var J=G[F][1];
var D=G[F][0];
for(var E=0,M=D.length;
E<M;
E++){var N=A(D[E]);
N.data(this.widgetName+"-item",J);
I.push({item:N,instance:J,width:0,height:0,left:0,top:0})
}}},refreshPositions:function(C){if(this.offsetParent&&this.helper){this.offset.parent=this._getParentOffset()
}for(var E=this.items.length-1;
E>=0;
E--){var F=this.items[E];
if(F.instance!=this.currentContainer&&this.currentContainer&&F.item[0]!=this.currentItem[0]){continue
}var D=this.options.toleranceElement?A(this.options.toleranceElement,F.item):F.item;
if(!C){F.width=D.outerWidth();
F.height=D.outerHeight()
}var G=D.offset();
F.left=G.left;
F.top=G.top
}if(this.options.custom&&this.options.custom.refreshContainers){this.options.custom.refreshContainers.call(this)
}else{for(var E=this.containers.length-1;
E>=0;
E--){var G=this.containers[E].element.offset();
this.containers[E].containerCache.left=G.left;
this.containers[E].containerCache.top=G.top;
this.containers[E].containerCache.width=this.containers[E].element.outerWidth();
this.containers[E].containerCache.height=this.containers[E].element.outerHeight()
}}return this
},_createPlaceholder:function(D){D=D||this;
var E=D.options;
if(!E.placeholder||E.placeholder.constructor==String){var C=E.placeholder;
E.placeholder={element:function(){var F=A(document.createElement(D.currentItem[0].nodeName)).addClass(C||D.currentItem[0].className+" ui-sortable-placeholder").removeClass("ui-sortable-helper")[0];
if(!C){F.style.visibility="hidden"
}return F
},update:function(F,G){if(C&&!E.forcePlaceholderSize){return 
}if(!G.height()){G.height(D.currentItem.innerHeight()-parseInt(D.currentItem.css("paddingTop")||0,10)-parseInt(D.currentItem.css("paddingBottom")||0,10))
}if(!G.width()){G.width(D.currentItem.innerWidth()-parseInt(D.currentItem.css("paddingLeft")||0,10)-parseInt(D.currentItem.css("paddingRight")||0,10))
}}}
}D.placeholder=A(E.placeholder.element.call(D.element,D.currentItem));
D.currentItem.after(D.placeholder);
E.placeholder.update(D,D.placeholder)
},_contactContainers:function(C){var E=null,L=null;
for(var H=this.containers.length-1;
H>=0;
H--){if(A.contains(this.currentItem[0],this.containers[H].element[0])){continue
}if(this._intersectsWith(this.containers[H].containerCache)){if(E&&A.contains(this.containers[H].element[0],E.element[0])){continue
}E=this.containers[H];
L=H
}else{if(this.containers[H].containerCache.over){this.containers[H]._trigger("out",C,this._uiHash(this));
this.containers[H].containerCache.over=0
}}}if(!E){return 
}if(this.containers.length===1){this.containers[L]._trigger("over",C,this._uiHash(this));
this.containers[L].containerCache.over=1
}else{var K=10000;
var I=null;
var J=this.containers[L].floating?"left":"top";
var M=this.containers[L].floating?"width":"height";
var D=this.positionAbs[J]+this.offset.click[J];
for(var F=this.items.length-1;
F>=0;
F--){if(!A.contains(this.containers[L].element[0],this.items[F].item[0])){continue
}if(this.items[F].item[0]==this.currentItem[0]){continue
}var N=this.items[F].item.offset()[J];
var G=false;
if(Math.abs(N-D)>Math.abs(N+this.items[F][M]-D)){G=true;
N+=this.items[F][M]
}if(Math.abs(N-D)<K){K=Math.abs(N-D);
I=this.items[F];
this.direction=G?"up":"down"
}}if(!I&&!this.options.dropOnEmpty){return 
}this.currentContainer=this.containers[L];
I?this._rearrange(C,I,null,true):this._rearrange(C,null,this.containers[L].element,true);
this._trigger("change",C,this._uiHash());
this.containers[L]._trigger("change",C,this._uiHash(this));
this.options.placeholder.update(this.currentContainer,this.placeholder);
this.containers[L]._trigger("over",C,this._uiHash(this));
this.containers[L].containerCache.over=1
}},_createHelper:function(D){var E=this.options;
var C=A.isFunction(E.helper)?A(E.helper.apply(this.element[0],[D,this.currentItem])):(E.helper=="clone"?this.currentItem.clone():this.currentItem);
if(!C.parents("body").length){A(E.appendTo!="parent"?E.appendTo:this.currentItem[0].parentNode)[0].appendChild(C[0])
}if(C[0]==this.currentItem[0]){this._storedCSS={width:this.currentItem[0].style.width,height:this.currentItem[0].style.height,position:this.currentItem.css("position"),top:this.currentItem.css("top"),left:this.currentItem.css("left")}
}if(C[0].style.width==""||E.forceHelperSize){C.width(this.currentItem.width())
}if(C[0].style.height==""||E.forceHelperSize){C.height(this.currentItem.height())
}return C
},_adjustOffsetFromHelper:function(C){if(typeof C=="string"){C=C.split(" ")
}if(A.isArray(C)){C={left:+C[0],top:+C[1]||0}
}if("left" in C){this.offset.click.left=C.left+this.margins.left
}if("right" in C){this.offset.click.left=this.helperProportions.width-C.right+this.margins.left
}if("top" in C){this.offset.click.top=C.top+this.margins.top
}if("bottom" in C){this.offset.click.top=this.helperProportions.height-C.bottom+this.margins.top
}},_getParentOffset:function(){this.offsetParent=this.helper.offsetParent();
var C=this.offsetParent.offset();
if(this.cssPosition=="absolute"&&this.scrollParent[0]!=document&&A.contains(this.scrollParent[0],this.offsetParent[0])){C.left+=this.scrollParent.scrollLeft();
C.top+=this.scrollParent.scrollTop()
}if((this.offsetParent[0]==document.body)||(this.offsetParent[0].tagName&&this.offsetParent[0].tagName.toLowerCase()=="html"&&A.ui.ie)){C={top:0,left:0}
}return{top:C.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:C.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)}
},_getRelativeOffset:function(){if(this.cssPosition=="relative"){var C=this.currentItem.position();
return{top:C.top-(parseInt(this.helper.css("top"),10)||0)+this.scrollParent.scrollTop(),left:C.left-(parseInt(this.helper.css("left"),10)||0)+this.scrollParent.scrollLeft()}
}else{return{top:0,left:0}
}},_cacheMargins:function(){this.margins={left:(parseInt(this.currentItem.css("marginLeft"),10)||0),top:(parseInt(this.currentItem.css("marginTop"),10)||0)}
},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()}
},_setContainment:function(){var F=this.options;
if(F.containment=="parent"){F.containment=this.helper[0].parentNode
}if(F.containment=="document"||F.containment=="window"){this.containment=[0-this.offset.relative.left-this.offset.parent.left,0-this.offset.relative.top-this.offset.parent.top,A(F.containment=="document"?document:window).width()-this.helperProportions.width-this.margins.left,(A(F.containment=="document"?document:window).height()||document.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top]
}if(!(/^(document|window|parent)$/).test(F.containment)){var D=A(F.containment)[0];
var E=A(F.containment).offset();
var C=(A(D).css("overflow")!="hidden");
this.containment=[E.left+(parseInt(A(D).css("borderLeftWidth"),10)||0)+(parseInt(A(D).css("paddingLeft"),10)||0)-this.margins.left,E.top+(parseInt(A(D).css("borderTopWidth"),10)||0)+(parseInt(A(D).css("paddingTop"),10)||0)-this.margins.top,E.left+(C?Math.max(D.scrollWidth,D.offsetWidth):D.offsetWidth)-(parseInt(A(D).css("borderLeftWidth"),10)||0)-(parseInt(A(D).css("paddingRight"),10)||0)-this.helperProportions.width-this.margins.left,E.top+(C?Math.max(D.scrollHeight,D.offsetHeight):D.offsetHeight)-(parseInt(A(D).css("borderTopWidth"),10)||0)-(parseInt(A(D).css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top]
}},_convertPositionTo:function(F,H){if(!H){H=this.position
}var D=F=="absolute"?1:-1;
var E=this.options,C=this.cssPosition=="absolute"&&!(this.scrollParent[0]!=document&&A.contains(this.scrollParent[0],this.offsetParent[0]))?this.offsetParent:this.scrollParent,G=(/(html|body)/i).test(C[0].tagName);
return{top:(H.top+this.offset.relative.top*D+this.offset.parent.top*D-((this.cssPosition=="fixed"?-this.scrollParent.scrollTop():(G?0:C.scrollTop()))*D)),left:(H.left+this.offset.relative.left*D+this.offset.parent.left*D-((this.cssPosition=="fixed"?-this.scrollParent.scrollLeft():G?0:C.scrollLeft())*D))}
},_generatePosition:function(F){var I=this.options,C=this.cssPosition=="absolute"&&!(this.scrollParent[0]!=document&&A.contains(this.scrollParent[0],this.offsetParent[0]))?this.offsetParent:this.scrollParent,J=(/(html|body)/i).test(C[0].tagName);
if(this.cssPosition=="relative"&&!(this.scrollParent[0]!=document&&this.scrollParent[0]!=this.offsetParent[0])){this.offset.relative=this._getRelativeOffset()
}var E=F.pageX;
var D=F.pageY;
if(this.originalPosition){if(this.containment){if(F.pageX-this.offset.click.left<this.containment[0]){E=this.containment[0]+this.offset.click.left
}if(F.pageY-this.offset.click.top<this.containment[1]){D=this.containment[1]+this.offset.click.top
}if(F.pageX-this.offset.click.left>this.containment[2]){E=this.containment[2]+this.offset.click.left
}if(F.pageY-this.offset.click.top>this.containment[3]){D=this.containment[3]+this.offset.click.top
}}if(I.grid){var H=this.originalPageY+Math.round((D-this.originalPageY)/I.grid[1])*I.grid[1];
D=this.containment?(!(H-this.offset.click.top<this.containment[1]||H-this.offset.click.top>this.containment[3])?H:(!(H-this.offset.click.top<this.containment[1])?H-I.grid[1]:H+I.grid[1])):H;
var G=this.originalPageX+Math.round((E-this.originalPageX)/I.grid[0])*I.grid[0];
E=this.containment?(!(G-this.offset.click.left<this.containment[0]||G-this.offset.click.left>this.containment[2])?G:(!(G-this.offset.click.left<this.containment[0])?G-I.grid[0]:G+I.grid[0])):G
}}return{top:(D-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+((this.cssPosition=="fixed"?-this.scrollParent.scrollTop():(J?0:C.scrollTop())))),left:(E-this.offset.click.left-this.offset.relative.left-this.offset.parent.left+((this.cssPosition=="fixed"?-this.scrollParent.scrollLeft():J?0:C.scrollLeft())))}
},_rearrange:function(G,F,D,E){D?D[0].appendChild(this.placeholder[0]):F.item[0].parentNode.insertBefore(this.placeholder[0],(this.direction=="down"?F.item[0]:F.item[0].nextSibling));
this.counter=this.counter?++this.counter:1;
var C=this.counter;
this._delay(function(){if(C==this.counter){this.refreshPositions(!E)
}})
},_clear:function(D,E){this.reverting=false;
var F=[];
if(!this._noFinalSort&&this.currentItem.parent().length){this.placeholder.before(this.currentItem)
}this._noFinalSort=null;
if(this.helper[0]==this.currentItem[0]){for(var C in this._storedCSS){if(this._storedCSS[C]=="auto"||this._storedCSS[C]=="static"){this._storedCSS[C]=""
}}this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper")
}else{this.currentItem.show()
}if(this.fromOutside&&!E){F.push(function(G){this._trigger("receive",G,this._uiHash(this.fromOutside))
})
}if((this.fromOutside||this.domPosition.prev!=this.currentItem.prev().not(".ui-sortable-helper")[0]||this.domPosition.parent!=this.currentItem.parent()[0])&&!E){F.push(function(G){this._trigger("update",G,this._uiHash())
})
}if(this!==this.currentContainer){if(!E){F.push(function(G){this._trigger("remove",G,this._uiHash())
});
F.push((function(G){return function(H){G._trigger("receive",H,this._uiHash(this))
}
}).call(this,this.currentContainer));
F.push((function(G){return function(H){G._trigger("update",H,this._uiHash(this))
}
}).call(this,this.currentContainer))
}}for(var C=this.containers.length-1;
C>=0;
C--){if(!E){F.push((function(G){return function(H){G._trigger("deactivate",H,this._uiHash(this))
}
}).call(this,this.containers[C]))
}if(this.containers[C].containerCache.over){F.push((function(G){return function(H){G._trigger("out",H,this._uiHash(this))
}
}).call(this,this.containers[C]));
this.containers[C].containerCache.over=0
}}if(this._storedCursor){A("body").css("cursor",this._storedCursor)
}if(this._storedOpacity){this.helper.css("opacity",this._storedOpacity)
}if(this._storedZIndex){this.helper.css("zIndex",this._storedZIndex=="auto"?"":this._storedZIndex)
}this.dragging=false;
if(this.cancelHelperRemoval){if(!E){this._trigger("beforeStop",D,this._uiHash());
for(var C=0;
C<F.length;
C++){F[C].call(this,D)
}this._trigger("stop",D,this._uiHash())
}this.fromOutside=false;
return false
}if(!E){this._trigger("beforeStop",D,this._uiHash())
}this.placeholder[0].parentNode.removeChild(this.placeholder[0]);
if(this.helper[0]!=this.currentItem[0]){this.helper.remove()
}this.helper=null;
if(!E){for(var C=0;
C<F.length;
C++){F[C].call(this,D)
}this._trigger("stop",D,this._uiHash())
}this.fromOutside=false;
return true
},_trigger:function(){if(A.Widget.prototype._trigger.apply(this,arguments)===false){this.cancel()
}},_uiHash:function(C){var D=C||this;
return{helper:D.helper,placeholder:D.placeholder||A([]),position:D.position,originalPosition:D.originalPosition,offset:D.positionAbs,item:D.currentItem,sender:C?C.element:null}
}})
})(jQuery);
(function(D,E){var B=0,C={},A={};
C.height=C.paddingTop=C.paddingBottom=C.borderTopWidth=C.borderBottomWidth="hide";
A.height=A.paddingTop=A.paddingBottom=A.borderTopWidth=A.borderBottomWidth="show";
D.widget("ui.accordion",{version:"1.9.1",options:{active:0,animate:{},collapsible:false,event:"click",header:"> li > :first-child,> :not(li):even",heightStyle:"auto",icons:{activeHeader:"ui-icon-triangle-1-s",header:"ui-icon-triangle-1-e"},activate:null,beforeActivate:null},_create:function(){var G=this.accordionId="ui-accordion-"+(this.element.attr("id")||++B),F=this.options;
this.prevShow=this.prevHide=D();
this.element.addClass("ui-accordion ui-widget ui-helper-reset");
this.headers=this.element.find(F.header).addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-all");
this._hoverable(this.headers);
this._focusable(this.headers);
this.headers.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").hide();
if(!F.collapsible&&(F.active===false||F.active==null)){F.active=0
}if(F.active<0){F.active+=this.headers.length
}this.active=this._findActive(F.active).addClass("ui-accordion-header-active ui-state-active").toggleClass("ui-corner-all ui-corner-top");
this.active.next().addClass("ui-accordion-content-active").show();
this._createIcons();
this.refresh();
this.element.attr("role","tablist");
this.headers.attr("role","tab").each(function(K){var L=D(this),J=L.attr("id"),H=L.next(),I=H.attr("id");
if(!J){J=G+"-header-"+K;
L.attr("id",J)
}if(!I){I=G+"-panel-"+K;
H.attr("id",I)
}L.attr("aria-controls",I);
H.attr("aria-labelledby",J)
}).next().attr("role","tabpanel");
this.headers.not(this.active).attr({"aria-selected":"false",tabIndex:-1}).next().attr({"aria-expanded":"false","aria-hidden":"true"}).hide();
if(!this.active.length){this.headers.eq(0).attr("tabIndex",0)
}else{this.active.attr({"aria-selected":"true",tabIndex:0}).next().attr({"aria-expanded":"true","aria-hidden":"false"})
}this._on(this.headers,{keydown:"_keydown"});
this._on(this.headers.next(),{keydown:"_panelKeyDown"});
this._setupEvents(F.event)
},_getCreateEventData:function(){return{header:this.active,content:!this.active.length?D():this.active.next()}
},_createIcons:function(){var F=this.options.icons;
if(F){D("<span>").addClass("ui-accordion-header-icon ui-icon "+F.header).prependTo(this.headers);
this.active.children(".ui-accordion-header-icon").removeClass(F.header).addClass(F.activeHeader);
this.headers.addClass("ui-accordion-icons")
}},_destroyIcons:function(){this.headers.removeClass("ui-accordion-icons").children(".ui-accordion-header-icon").remove()
},_destroy:function(){var F;
this.element.removeClass("ui-accordion ui-widget ui-helper-reset").removeAttr("role");
this.headers.removeClass("ui-accordion-header ui-accordion-header-active ui-helper-reset ui-state-default ui-corner-all ui-state-active ui-state-disabled ui-corner-top").removeAttr("role").removeAttr("aria-selected").removeAttr("aria-controls").removeAttr("tabIndex").each(function(){if(/^ui-accordion/.test(this.id)){this.removeAttribute("id")
}});
this._destroyIcons();
F=this.headers.next().css("display","").removeAttr("role").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-labelledby").removeClass("ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content ui-accordion-content-active ui-state-disabled").each(function(){if(/^ui-accordion/.test(this.id)){this.removeAttribute("id")
}});
if(this.options.heightStyle!=="content"){F.css("height","")
}},_setOption:function(F,G){if(F==="active"){this._activate(G);
return 
}if(F==="event"){if(this.options.event){this._off(this.headers,this.options.event)
}this._setupEvents(G)
}this._super(F,G);
if(F==="collapsible"&&!G&&this.options.active===false){this._activate(0)
}if(F==="icons"){this._destroyIcons();
if(G){this._createIcons()
}}if(F==="disabled"){this.headers.add(this.headers.next()).toggleClass("ui-state-disabled",!!G)
}},_keydown:function(I){if(I.altKey||I.ctrlKey){return 
}var J=D.ui.keyCode,H=this.headers.length,F=this.headers.index(I.target),G=false;
switch(I.keyCode){case J.RIGHT:case J.DOWN:G=this.headers[(F+1)%H];
break;
case J.LEFT:case J.UP:G=this.headers[(F-1+H)%H];
break;
case J.SPACE:case J.ENTER:this._eventHandler(I);
break;
case J.HOME:G=this.headers[0];
break;
case J.END:G=this.headers[H-1];
break
}if(G){D(I.target).attr("tabIndex",-1);
D(G).attr("tabIndex",0);
G.focus();
I.preventDefault()
}},_panelKeyDown:function(F){if(F.keyCode===D.ui.keyCode.UP&&F.ctrlKey){D(F.currentTarget).prev().focus()
}},refresh:function(){var H,I,F=this.options.heightStyle,G=this.element.parent();
if(F==="fill"){if(!D.support.minHeight){I=G.css("overflow");
G.css("overflow","hidden")
}H=G.height();
this.element.siblings(":visible").each(function(){var K=D(this),J=K.css("position");
if(J==="absolute"||J==="fixed"){return 
}H-=K.outerHeight(true)
});
if(I){G.css("overflow",I)
}this.headers.each(function(){H-=D(this).outerHeight(true)
});
this.headers.next().each(function(){D(this).height(Math.max(0,H-D(this).innerHeight()+D(this).height()))
}).css("overflow","auto")
}else{if(F==="auto"){H=0;
this.headers.next().each(function(){H=Math.max(H,D(this).height("").height())
}).height(H)
}}},_activate:function(F){var G=this._findActive(F)[0];
if(G===this.active[0]){return 
}G=G||this.active[0];
this._eventHandler({target:G,currentTarget:G,preventDefault:D.noop})
},_findActive:function(F){return typeof F==="number"?this.headers.eq(F):D()
},_setupEvents:function(G){var F={};
if(!G){return 
}D.each(G.split(" "),function(I,H){F[H]="_eventHandler"
});
this._on(this.headers,F)
},_eventHandler:function(F){var N=this.options,I=this.active,J=D(F.currentTarget),L=J[0]===I[0],G=L&&N.collapsible,H=G?D():J.next(),K=I.next(),M={oldHeader:I,oldPanel:K,newHeader:G?D():J,newPanel:H};
F.preventDefault();
if((L&&!N.collapsible)||(this._trigger("beforeActivate",F,M)===false)){return 
}N.active=G?false:this.headers.index(J);
this.active=L?D():J;
this._toggle(M);
I.removeClass("ui-accordion-header-active ui-state-active");
if(N.icons){I.children(".ui-accordion-header-icon").removeClass(N.icons.activeHeader).addClass(N.icons.header)
}if(!L){J.removeClass("ui-corner-all").addClass("ui-accordion-header-active ui-state-active ui-corner-top");
if(N.icons){J.children(".ui-accordion-header-icon").removeClass(N.icons.header).addClass(N.icons.activeHeader)
}J.next().addClass("ui-accordion-content-active")
}},_toggle:function(H){var F=H.newPanel,G=this.prevShow.length?this.prevShow:H.oldPanel;
this.prevShow.add(this.prevHide).stop(true,true);
this.prevShow=F;
this.prevHide=G;
if(this.options.animate){this._animate(F,G,H)
}else{G.hide();
F.show();
this._toggleComplete(H)
}G.attr({"aria-expanded":"false","aria-hidden":"true"});
G.prev().attr("aria-selected","false");
if(F.length&&G.length){G.prev().attr("tabIndex",-1)
}else{if(F.length){this.headers.filter(function(){return D(this).attr("tabIndex")===0
}).attr("tabIndex",-1)
}}F.attr({"aria-expanded":"true","aria-hidden":"false"}).prev().attr({"aria-selected":"true",tabIndex:0})
},_animate:function(F,N,J){var M,L,I,K=this,O=0,P=F.length&&(!N.length||(F.index()<N.index())),H=this.options.animate||{},Q=P&&H.down||H,G=function(){K._toggleComplete(J)
};
if(typeof Q==="number"){I=Q
}if(typeof Q==="string"){L=Q
}L=L||Q.easing||H.easing;
I=I||Q.duration||H.duration;
if(!N.length){return F.animate(A,I,L,G)
}if(!F.length){return N.animate(C,I,L,G)
}M=F.show().outerHeight();
N.animate(C,{duration:I,easing:L,step:function(R,S){S.now=Math.round(R)
}});
F.hide().animate(A,{duration:I,easing:L,complete:G,step:function(R,S){S.now=Math.round(R);
if(S.prop!=="height"){O+=S.now
}else{if(K.options.heightStyle!=="content"){S.now=Math.round(M-N.outerHeight()-O);
O=0
}}}})
},_toggleComplete:function(G){var F=G.oldPanel;
F.removeClass("ui-accordion-content-active").prev().removeClass("ui-corner-top").addClass("ui-corner-all");
if(F.length){F.parent()[0].className=F.parent()[0].className
}this._trigger("activate",null,G)
}});
if(D.uiBackCompat!==false){(function(G,F){G.extend(F.options,{navigation:false,navigationFilter:function(){return this.href.toLowerCase()===location.href.toLowerCase()
}});
var H=F._create;
F._create=function(){if(this.options.navigation){var J=this,L=this.element.find(this.options.header),I=L.next(),K=L.add(I).find("a").filter(this.options.navigationFilter)[0];
if(K){L.add(I).each(function(M){if(G.contains(this,K)){J.options.active=Math.floor(M/2);
return false
}})
}}H.call(this)
}
}(jQuery,jQuery.ui.accordion.prototype));
(function(H,F){H.extend(F.options,{heightStyle:null,autoHeight:true,clearStyle:false,fillSpace:false});
var I=F._create,G=F._setOption;
H.extend(F,{_create:function(){this.options.heightStyle=this.options.heightStyle||this._mergeHeightStyle();
I.call(this)
},_setOption:function(J){if(J==="autoHeight"||J==="clearStyle"||J==="fillSpace"){this.options.heightStyle=this._mergeHeightStyle()
}G.apply(this,arguments)
},_mergeHeightStyle:function(){var J=this.options;
if(J.fillSpace){return"fill"
}if(J.clearStyle){return"content"
}if(J.autoHeight){return"auto"
}}})
}(jQuery,jQuery.ui.accordion.prototype));
(function(H,G){H.extend(G.options.icons,{activeHeader:null,headerSelected:"ui-icon-triangle-1-s"});
var F=G._createIcons;
G._createIcons=function(){if(this.options.icons){this.options.icons.activeHeader=this.options.icons.activeHeader||this.options.icons.headerSelected
}F.call(this)
}
}(jQuery,jQuery.ui.accordion.prototype));
(function(H,G){G.activate=G._activate;
var F=G._findActive;
G._findActive=function(I){if(I===-1){I=false
}if(I&&typeof I!=="number"){I=this.headers.index(this.headers.filter(I));
if(I===-1){I=false
}}return F.call(this,I)
}
}(jQuery,jQuery.ui.accordion.prototype));
jQuery.ui.accordion.prototype.resize=jQuery.ui.accordion.prototype.refresh;
(function(H,G){H.extend(G.options,{change:null,changestart:null});
var F=G._trigger;
G._trigger=function(J,K,L){var I=F.apply(this,arguments);
if(!I){return false
}if(J==="beforeActivate"){I=F.call(this,"changestart",K,{oldHeader:L.oldHeader,oldContent:L.oldPanel,newHeader:L.newHeader,newContent:L.newPanel})
}else{if(J==="activate"){I=F.call(this,"change",K,{oldHeader:L.oldHeader,oldContent:L.oldPanel,newHeader:L.newHeader,newContent:L.newPanel})
}}return I
}
}(jQuery,jQuery.ui.accordion.prototype));
(function(G,F){G.extend(F.options,{animate:null,animated:"slide"});
var H=F._create;
F._create=function(){var I=this.options;
if(I.animate===null){if(!I.animated){I.animate=false
}else{if(I.animated==="slide"){I.animate=300
}else{if(I.animated==="bounceslide"){I.animate={duration:200,down:{easing:"easeOutBounce",duration:1000}}
}else{I.animate=I.animated
}}}}H.call(this)
}
}(jQuery,jQuery.ui.accordion.prototype))
}})(jQuery);
(function(A,B){var C=0;
A.widget("ui.autocomplete",{version:"1.9.1",defaultElement:"<input>",options:{appendTo:"body",autoFocus:false,delay:300,minLength:1,position:{my:"left top",at:"left bottom",collision:"none"},source:null,change:null,close:null,focus:null,open:null,response:null,search:null,select:null},pending:0,_create:function(){var E,D,F;
this.isMultiLine=this._isMultiLine();
this.valueMethod=this.element[this.element.is("input,textarea")?"val":"text"];
this.isNewMenu=true;
this.element.addClass("ui-autocomplete-input").attr("autocomplete","off");
this._on(this.element,{keydown:function(G){if(this.element.prop("readOnly")){E=true;
F=true;
D=true;
return 
}E=false;
F=false;
D=false;
var H=A.ui.keyCode;
switch(G.keyCode){case H.PAGE_UP:E=true;
this._move("previousPage",G);
break;
case H.PAGE_DOWN:E=true;
this._move("nextPage",G);
break;
case H.UP:E=true;
this._keyEvent("previous",G);
break;
case H.DOWN:E=true;
this._keyEvent("next",G);
break;
case H.ENTER:case H.NUMPAD_ENTER:if(this.menu.active){E=true;
G.preventDefault();
this.menu.select(G)
}break;
case H.TAB:if(this.menu.active){this.menu.select(G)
}break;
case H.ESCAPE:if(this.menu.element.is(":visible")){this._value(this.term);
this.close(G);
G.preventDefault()
}break;
default:D=true;
this._searchTimeout(G);
break
}},keypress:function(G){if(E){E=false;
G.preventDefault();
return 
}if(D){return 
}var H=A.ui.keyCode;
switch(G.keyCode){case H.PAGE_UP:this._move("previousPage",G);
break;
case H.PAGE_DOWN:this._move("nextPage",G);
break;
case H.UP:this._keyEvent("previous",G);
break;
case H.DOWN:this._keyEvent("next",G);
break
}},input:function(G){if(F){F=false;
G.preventDefault();
return 
}this._searchTimeout(G)
},focus:function(){this.selectedItem=null;
this.previous=this._value()
},blur:function(G){if(this.cancelBlur){delete this.cancelBlur;
return 
}clearTimeout(this.searching);
this.close(G);
this._change(G)
}});
this._initSource();
this.menu=A("<ul>").addClass("ui-autocomplete").appendTo(this.document.find(this.options.appendTo||"body")[0]).menu({input:A(),role:null}).zIndex(this.element.zIndex()+1).hide().data("menu");
this._on(this.menu.element,{mousedown:function(G){G.preventDefault();
this.cancelBlur=true;
this._delay(function(){delete this.cancelBlur
});
var H=this.menu.element[0];
if(!A(G.target).closest(".ui-menu-item").length){this._delay(function(){var I=this;
this.document.one("mousedown",function(J){if(J.target!==I.element[0]&&J.target!==H&&!A.contains(H,J.target)){I.close()
}})
})
}},menufocus:function(H,I){if(this.isNewMenu){this.isNewMenu=false;
if(H.originalEvent&&/^mouse/.test(H.originalEvent.type)){this.menu.blur();
this.document.one("mousemove",function(){A(H.target).trigger(H.originalEvent)
});
return 
}}var G=I.item.data("ui-autocomplete-item")||I.item.data("item.autocomplete");
if(false!==this._trigger("focus",H,{item:G})){if(H.originalEvent&&/^key/.test(H.originalEvent.type)){this._value(G.value)
}}else{this.liveRegion.text(G.value)
}},menuselect:function(I,J){var H=J.item.data("ui-autocomplete-item")||J.item.data("item.autocomplete"),G=this.previous;
if(this.element[0]!==this.document[0].activeElement){this.element.focus();
this.previous=G;
this._delay(function(){this.previous=G;
this.selectedItem=H
})
}if(false!==this._trigger("select",I,{item:H})){this._value(H.value)
}this.term=this._value();
this.close(I);
this.selectedItem=H
}});
this.liveRegion=A("<span>",{role:"status","aria-live":"polite"}).addClass("ui-helper-hidden-accessible").insertAfter(this.element);
if(A.fn.bgiframe){this.menu.element.bgiframe()
}this._on(this.window,{beforeunload:function(){this.element.removeAttr("autocomplete")
}})
},_destroy:function(){clearTimeout(this.searching);
this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete");
this.menu.element.remove();
this.liveRegion.remove()
},_setOption:function(D,E){this._super(D,E);
if(D==="source"){this._initSource()
}if(D==="appendTo"){this.menu.element.appendTo(this.document.find(E||"body")[0])
}if(D==="disabled"&&E&&this.xhr){this.xhr.abort()
}},_isMultiLine:function(){if(this.element.is("textarea")){return true
}if(this.element.is("input")){return false
}return this.element.prop("isContentEditable")
},_initSource:function(){var F,D,E=this;
if(A.isArray(this.options.source)){F=this.options.source;
this.source=function(H,G){G(A.ui.autocomplete.filter(F,H.term))
}
}else{if(typeof this.options.source==="string"){D=this.options.source;
this.source=function(H,G){if(E.xhr){E.xhr.abort()
}E.xhr=A.ajax({url:D,data:H,dataType:"json",success:function(I){G(I)
},error:function(){G([])
}})
}
}else{this.source=this.options.source
}}},_searchTimeout:function(D){clearTimeout(this.searching);
this.searching=this._delay(function(){if(this.term!==this._value()){this.selectedItem=null;
this.search(null,D)
}},this.options.delay)
},search:function(E,D){E=E!=null?E:this._value();
this.term=this._value();
if(E.length<this.options.minLength){return this.close(D)
}if(this._trigger("search",D)===false){return 
}return this._search(E)
},_search:function(D){this.pending++;
this.element.addClass("ui-autocomplete-loading");
this.cancelSearch=false;
this.source({term:D},this._response())
},_response:function(){var E=this,D=++C;
return function(F){if(D===C){E.__response(F)
}E.pending--;
if(!E.pending){E.element.removeClass("ui-autocomplete-loading")
}}
},__response:function(D){if(D){D=this._normalize(D)
}this._trigger("response",null,{content:D});
if(!this.options.disabled&&D&&D.length&&!this.cancelSearch){this._suggest(D);
this._trigger("open")
}else{this._close()
}},close:function(D){this.cancelSearch=true;
this._close(D)
},_close:function(D){if(this.menu.element.is(":visible")){this.menu.element.hide();
this.menu.blur();
this.isNewMenu=true;
this._trigger("close",D)
}},_change:function(D){if(this.previous!==this._value()){this._trigger("change",D,{item:this.selectedItem})
}},_normalize:function(D){if(D.length&&D[0].label&&D[0].value){return D
}return A.map(D,function(E){if(typeof E==="string"){return{label:E,value:E}
}return A.extend({label:E.label||E.value,value:E.value||E.label},E)
})
},_suggest:function(D){var E=this.menu.element.empty().zIndex(this.element.zIndex()+1);
this._renderMenu(E,D);
this.menu.refresh();
E.show();
this._resizeMenu();
E.position(A.extend({of:this.element},this.options.position));
if(this.options.autoFocus){this.menu.next()
}},_resizeMenu:function(){var D=this.menu.element;
D.outerWidth(Math.max(D.width("").outerWidth()+1,this.element.outerWidth()))
},_renderMenu:function(E,D){var F=this;
A.each(D,function(G,H){F._renderItemData(E,H)
})
},_renderItemData:function(D,E){return this._renderItem(D,E).data("ui-autocomplete-item",E)
},_renderItem:function(D,E){return A("<li>").append(A("<a>").text(E.label)).appendTo(D)
},_move:function(E,D){if(!this.menu.element.is(":visible")){this.search(null,D);
return 
}if(this.menu.isFirstItem()&&/^previous/.test(E)||this.menu.isLastItem()&&/^next/.test(E)){this._value(this.term);
this.menu.blur();
return 
}this.menu[E](D)
},widget:function(){return this.menu.element
},_value:function(){return this.valueMethod.apply(this.element,arguments)
},_keyEvent:function(E,D){if(!this.isMultiLine||this.menu.element.is(":visible")){this._move(E,D);
D.preventDefault()
}}});
A.extend(A.ui.autocomplete,{escapeRegex:function(D){return D.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&")
},filter:function(F,D){var E=new RegExp(A.ui.autocomplete.escapeRegex(D),"i");
return A.grep(F,function(G){return E.test(G.label||G.value||G)
})
}});
A.widget("ui.autocomplete",A.ui.autocomplete,{options:{messages:{noResults:"No search results.",results:function(D){return D+(D>1?" results are":" result is")+" available, use up and down arrow keys to navigate."
}}},__response:function(E){var D;
this._superApply(arguments);
if(this.options.disabled||this.cancelSearch){return 
}if(E&&E.length){D=this.options.messages.results(E.length)
}else{D=this.options.messages.noResults
}this.liveRegion.text(D)
}})
}(jQuery));
(function(F,B){var K,E,A,H,I="ui-button ui-widget ui-state-default ui-corner-all",C="ui-state-hover ui-state-active ",G="ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon-primary ui-button-text-icon-secondary ui-button-text-only",J=function(){var L=F(this).find(":ui-button");
setTimeout(function(){L.button("refresh")
},1)
},D=function(M){var L=M.name,N=M.form,O=F([]);
if(L){if(N){O=F(N).find("[name='"+L+"']")
}else{O=F("[name='"+L+"']",M.ownerDocument).filter(function(){return !this.form
})
}}return O
};
F.widget("ui.button",{version:"1.9.1",defaultElement:"<button>",options:{disabled:null,text:true,label:null,icons:{primary:null,secondary:null}},_create:function(){this.element.closest("form").unbind("reset"+this.eventNamespace).bind("reset"+this.eventNamespace,J);
if(typeof this.options.disabled!=="boolean"){this.options.disabled=!!this.element.prop("disabled")
}else{this.element.prop("disabled",this.options.disabled)
}this._determineButtonType();
this.hasTitle=!!this.buttonElement.attr("title");
var N=this,M=this.options,O=this.type==="checkbox"||this.type==="radio",P="ui-state-hover"+(!O?" ui-state-active":""),L="ui-state-focus";
if(M.label===null){M.label=(this.type==="input"?this.buttonElement.val():this.buttonElement.html())
}this.buttonElement.addClass(I).attr("role","button").bind("mouseenter"+this.eventNamespace,function(){if(M.disabled){return 
}F(this).addClass("ui-state-hover");
if(this===K){F(this).addClass("ui-state-active")
}}).bind("mouseleave"+this.eventNamespace,function(){if(M.disabled){return 
}F(this).removeClass(P)
}).bind("click"+this.eventNamespace,function(Q){if(M.disabled){Q.preventDefault();
Q.stopImmediatePropagation()
}});
this.element.bind("focus"+this.eventNamespace,function(){N.buttonElement.addClass(L)
}).bind("blur"+this.eventNamespace,function(){N.buttonElement.removeClass(L)
});
if(O){this.element.bind("change"+this.eventNamespace,function(){if(H){return 
}N.refresh()
});
this.buttonElement.bind("mousedown"+this.eventNamespace,function(Q){if(M.disabled){return 
}H=false;
E=Q.pageX;
A=Q.pageY
}).bind("mouseup"+this.eventNamespace,function(Q){if(M.disabled){return 
}if(E!==Q.pageX||A!==Q.pageY){H=true
}})
}if(this.type==="checkbox"){this.buttonElement.bind("click"+this.eventNamespace,function(){if(M.disabled||H){return false
}F(this).toggleClass("ui-state-active");
N.buttonElement.attr("aria-pressed",N.element[0].checked)
})
}else{if(this.type==="radio"){this.buttonElement.bind("click"+this.eventNamespace,function(){if(M.disabled||H){return false
}F(this).addClass("ui-state-active");
N.buttonElement.attr("aria-pressed","true");
var Q=N.element[0];
D(Q).not(Q).map(function(){return F(this).button("widget")[0]
}).removeClass("ui-state-active").attr("aria-pressed","false")
})
}else{this.buttonElement.bind("mousedown"+this.eventNamespace,function(){if(M.disabled){return false
}F(this).addClass("ui-state-active");
K=this;
N.document.one("mouseup",function(){K=null
})
}).bind("mouseup"+this.eventNamespace,function(){if(M.disabled){return false
}F(this).removeClass("ui-state-active")
}).bind("keydown"+this.eventNamespace,function(Q){if(M.disabled){return false
}if(Q.keyCode===F.ui.keyCode.SPACE||Q.keyCode===F.ui.keyCode.ENTER){F(this).addClass("ui-state-active")
}}).bind("keyup"+this.eventNamespace,function(){F(this).removeClass("ui-state-active")
});
if(this.buttonElement.is("a")){this.buttonElement.keyup(function(Q){if(Q.keyCode===F.ui.keyCode.SPACE){F(this).click()
}})
}}}this._setOption("disabled",M.disabled);
this._resetButton()
},_determineButtonType:function(){var L,N,M;
if(this.element.is("[type=checkbox]")){this.type="checkbox"
}else{if(this.element.is("[type=radio]")){this.type="radio"
}else{if(this.element.is("input")){this.type="input"
}else{this.type="button"
}}}if(this.type==="checkbox"||this.type==="radio"){L=this.element.parents().last();
N="label[for='"+this.element.attr("id")+"']";
this.buttonElement=L.find(N);
if(!this.buttonElement.length){L=L.length?L.siblings():this.element.siblings();
this.buttonElement=L.filter(N);
if(!this.buttonElement.length){this.buttonElement=L.find(N)
}}this.element.addClass("ui-helper-hidden-accessible");
M=this.element.is(":checked");
if(M){this.buttonElement.addClass("ui-state-active")
}this.buttonElement.prop("aria-pressed",M)
}else{this.buttonElement=this.element
}},widget:function(){return this.buttonElement
},_destroy:function(){this.element.removeClass("ui-helper-hidden-accessible");
this.buttonElement.removeClass(I+" "+C+" "+G).removeAttr("role").removeAttr("aria-pressed").html(this.buttonElement.find(".ui-button-text").html());
if(!this.hasTitle){this.buttonElement.removeAttr("title")
}},_setOption:function(L,M){this._super(L,M);
if(L==="disabled"){if(M){this.element.prop("disabled",true)
}else{this.element.prop("disabled",false)
}return 
}this._resetButton()
},refresh:function(){var L=this.element.is(":disabled")||this.element.hasClass("ui-button-disabled");
if(L!==this.options.disabled){this._setOption("disabled",L)
}if(this.type==="radio"){D(this.element[0]).each(function(){if(F(this).is(":checked")){F(this).button("widget").addClass("ui-state-active").attr("aria-pressed","true")
}else{F(this).button("widget").removeClass("ui-state-active").attr("aria-pressed","false")
}})
}else{if(this.type==="checkbox"){if(this.element.is(":checked")){this.buttonElement.addClass("ui-state-active").attr("aria-pressed","true")
}else{this.buttonElement.removeClass("ui-state-active").attr("aria-pressed","false")
}}}},_resetButton:function(){if(this.type==="input"){if(this.options.label){this.element.val(this.options.label)
}return 
}var P=this.buttonElement.removeClass(G),N=F("<span></span>",this.document[0]).addClass("ui-button-text").html(this.options.label).appendTo(P.empty()).text(),M=this.options.icons,L=M.primary&&M.secondary,O=[];
if(M.primary||M.secondary){if(this.options.text){O.push("ui-button-text-icon"+(L?"s":(M.primary?"-primary":"-secondary")))
}if(M.primary){P.prepend("<span class='ui-button-icon-primary ui-icon "+M.primary+"'></span>")
}if(M.secondary){P.append("<span class='ui-button-icon-secondary ui-icon "+M.secondary+"'></span>")
}if(!this.options.text){O.push(L?"ui-button-icons-only":"ui-button-icon-only");
if(!this.hasTitle){P.attr("title",F.trim(N))
}}}else{O.push("ui-button-text-only")
}P.addClass(O.join(" "))
}});
F.widget("ui.buttonset",{version:"1.9.1",options:{items:"button, input[type=button], input[type=submit], input[type=reset], input[type=checkbox], input[type=radio], a, :data(button)"},_create:function(){this.element.addClass("ui-buttonset")
},_init:function(){this.refresh()
},_setOption:function(L,M){if(L==="disabled"){this.buttons.button("option",L,M)
}this._super(L,M)
},refresh:function(){var L=this.element.css("direction")==="rtl";
this.buttons=this.element.find(this.options.items).filter(":ui-button").button("refresh").end().not(":ui-button").button().end().map(function(){return F(this).button("widget")[0]
}).removeClass("ui-corner-all ui-corner-left ui-corner-right").filter(":first").addClass(L?"ui-corner-right":"ui-corner-left").end().filter(":last").addClass(L?"ui-corner-left":"ui-corner-right").end().end()
},_destroy:function(){this.element.removeClass("ui-buttonset");
this.buttons.map(function(){return F(this).button("widget")[0]
}).removeClass("ui-corner-left ui-corner-right").end().button("destroy")
}})
}(jQuery));
(function($,undefined){$.extend($.ui,{datepicker:{version:"1.9.1"}});
var PROP_NAME="datepicker";
var dpuuid=new Date().getTime();
var instActive;
function Datepicker(){this.debug=false;
this._curInst=null;
this._keyEvent=false;
this._disabledInputs=[];
this._datepickerShowing=false;
this._inDialog=false;
this._mainDivId="ui-datepicker-div";
this._inlineClass="ui-datepicker-inline";
this._appendClass="ui-datepicker-append";
this._triggerClass="ui-datepicker-trigger";
this._dialogClass="ui-datepicker-dialog";
this._disableClass="ui-datepicker-disabled";
this._unselectableClass="ui-datepicker-unselectable";
this._currentClass="ui-datepicker-current-day";
this._dayOverClass="ui-datepicker-days-cell-over";
this.regional=[];
this.regional[""]={closeText:"Done",prevText:"Prev",nextText:"Next",currentText:"Today",monthNames:["January","February","March","April","May","June","July","August","September","October","November","December"],monthNamesShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],dayNames:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],dayNamesShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],dayNamesMin:["Su","Mo","Tu","We","Th","Fr","Sa"],weekHeader:"Wk",dateFormat:"mm/dd/yy",firstDay:0,isRTL:false,showMonthAfterYear:false,yearSuffix:""};
this._defaults={showOn:"focus",showAnim:"fadeIn",showOptions:{},defaultDate:null,appendText:"",buttonText:"...",buttonImage:"",buttonImageOnly:false,hideIfNoPrevNext:false,navigationAsDateFormat:false,gotoCurrent:false,changeMonth:false,changeYear:false,yearRange:"c-10:c+10",showOtherMonths:false,selectOtherMonths:false,showWeek:false,calculateWeek:this.iso8601Week,shortYearCutoff:"+10",minDate:null,maxDate:null,duration:"fast",beforeShowDay:null,beforeShow:null,onSelect:null,onChangeMonthYear:null,onClose:null,numberOfMonths:1,showCurrentAtPos:0,stepMonths:1,stepBigMonths:12,altField:"",altFormat:"",constrainInput:true,showButtonPanel:false,autoSize:false,disabled:false};
$.extend(this._defaults,this.regional[""]);
this.dpDiv=bindHover($('<div id="'+this._mainDivId+'" class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>'))
}$.extend(Datepicker.prototype,{markerClassName:"hasDatepicker",maxRows:4,log:function(){if(this.debug){console.log.apply("",arguments)
}},_widgetDatepicker:function(){return this.dpDiv
},setDefaults:function(settings){extendRemove(this._defaults,settings||{});
return this
},_attachDatepicker:function(target,settings){var inlineSettings=null;
for(var attrName in this._defaults){var attrValue=target.getAttribute("date:"+attrName);
if(attrValue){inlineSettings=inlineSettings||{};
try{inlineSettings[attrName]=eval(attrValue)
}catch(err){inlineSettings[attrName]=attrValue
}}}var nodeName=target.nodeName.toLowerCase();
var inline=(nodeName=="div"||nodeName=="span");
if(!target.id){this.uuid+=1;
target.id="dp"+this.uuid
}var inst=this._newInst($(target),inline);
inst.settings=$.extend({},settings||{},inlineSettings||{});
if(nodeName=="input"){this._connectDatepicker(target,inst)
}else{if(inline){this._inlineDatepicker(target,inst)
}}},_newInst:function(target,inline){var id=target[0].id.replace(/([^A-Za-z0-9_-])/g,"\\\\$1");
return{id:id,input:target,selectedDay:0,selectedMonth:0,selectedYear:0,drawMonth:0,drawYear:0,inline:inline,dpDiv:(!inline?this.dpDiv:bindHover($('<div class="'+this._inlineClass+' ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>')))}
},_connectDatepicker:function(target,inst){var input=$(target);
inst.append=$([]);
inst.trigger=$([]);
if(input.hasClass(this.markerClassName)){return 
}this._attachments(input,inst);
input.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp).bind("setData.datepicker",function(event,key,value){inst.settings[key]=value
}).bind("getData.datepicker",function(event,key){return this._get(inst,key)
});
this._autoSize(inst);
$.data(target,PROP_NAME,inst);
if(inst.settings.disabled){this._disableDatepicker(target)
}},_attachments:function(input,inst){var appendText=this._get(inst,"appendText");
var isRTL=this._get(inst,"isRTL");
if(inst.append){inst.append.remove()
}if(appendText){inst.append=$('<span class="'+this._appendClass+'">'+appendText+"</span>");
input[isRTL?"before":"after"](inst.append)
}input.unbind("focus",this._showDatepicker);
if(inst.trigger){inst.trigger.remove()
}var showOn=this._get(inst,"showOn");
if(showOn=="focus"||showOn=="both"){input.focus(this._showDatepicker)
}if(showOn=="button"||showOn=="both"){var buttonText=this._get(inst,"buttonText");
var buttonImage=this._get(inst,"buttonImage");
inst.trigger=$(this._get(inst,"buttonImageOnly")?$("<img/>").addClass(this._triggerClass).attr({src:buttonImage,alt:buttonText,title:buttonText}):$('<button type="button"></button>').addClass(this._triggerClass).html(buttonImage==""?buttonText:$("<img/>").attr({src:buttonImage,alt:buttonText,title:buttonText})));
input[isRTL?"before":"after"](inst.trigger);
inst.trigger.click(function(){if($.datepicker._datepickerShowing&&$.datepicker._lastInput==input[0]){$.datepicker._hideDatepicker()
}else{if($.datepicker._datepickerShowing&&$.datepicker._lastInput!=input[0]){$.datepicker._hideDatepicker();
$.datepicker._showDatepicker(input[0])
}else{$.datepicker._showDatepicker(input[0])
}}return false
})
}},_autoSize:function(inst){if(this._get(inst,"autoSize")&&!inst.inline){var date=new Date(2009,12-1,20);
var dateFormat=this._get(inst,"dateFormat");
if(dateFormat.match(/[DM]/)){var findMax=function(names){var max=0;
var maxI=0;
for(var i=0;
i<names.length;
i++){if(names[i].length>max){max=names[i].length;
maxI=i
}}return maxI
};
date.setMonth(findMax(this._get(inst,(dateFormat.match(/MM/)?"monthNames":"monthNamesShort"))));
date.setDate(findMax(this._get(inst,(dateFormat.match(/DD/)?"dayNames":"dayNamesShort")))+20-date.getDay())
}inst.input.attr("size",this._formatDate(inst,date).length)
}},_inlineDatepicker:function(target,inst){var divSpan=$(target);
if(divSpan.hasClass(this.markerClassName)){return 
}divSpan.addClass(this.markerClassName).append(inst.dpDiv).bind("setData.datepicker",function(event,key,value){inst.settings[key]=value
}).bind("getData.datepicker",function(event,key){return this._get(inst,key)
});
$.data(target,PROP_NAME,inst);
this._setDate(inst,this._getDefaultDate(inst),true);
this._updateDatepicker(inst);
this._updateAlternate(inst);
if(inst.settings.disabled){this._disableDatepicker(target)
}inst.dpDiv.css("display","block")
},_dialogDatepicker:function(input,date,onSelect,settings,pos){var inst=this._dialogInst;
if(!inst){this.uuid+=1;
var id="dp"+this.uuid;
this._dialogInput=$('<input type="text" id="'+id+'" style="position: absolute; top: -100px; width: 0px;"/>');
this._dialogInput.keydown(this._doKeyDown);
$("body").append(this._dialogInput);
inst=this._dialogInst=this._newInst(this._dialogInput,false);
inst.settings={};
$.data(this._dialogInput[0],PROP_NAME,inst)
}extendRemove(inst.settings,settings||{});
date=(date&&date.constructor==Date?this._formatDate(inst,date):date);
this._dialogInput.val(date);
this._pos=(pos?(pos.length?pos:[pos.pageX,pos.pageY]):null);
if(!this._pos){var browserWidth=document.documentElement.clientWidth;
var browserHeight=document.documentElement.clientHeight;
var scrollX=document.documentElement.scrollLeft||document.body.scrollLeft;
var scrollY=document.documentElement.scrollTop||document.body.scrollTop;
this._pos=[(browserWidth/2)-100+scrollX,(browserHeight/2)-150+scrollY]
}this._dialogInput.css("left",(this._pos[0]+20)+"px").css("top",this._pos[1]+"px");
inst.settings.onSelect=onSelect;
this._inDialog=true;
this.dpDiv.addClass(this._dialogClass);
this._showDatepicker(this._dialogInput[0]);
if($.blockUI){$.blockUI(this.dpDiv)
}$.data(this._dialogInput[0],PROP_NAME,inst);
return this
},_destroyDatepicker:function(target){var $target=$(target);
var inst=$.data(target,PROP_NAME);
if(!$target.hasClass(this.markerClassName)){return 
}var nodeName=target.nodeName.toLowerCase();
$.removeData(target,PROP_NAME);
if(nodeName=="input"){inst.append.remove();
inst.trigger.remove();
$target.removeClass(this.markerClassName).unbind("focus",this._showDatepicker).unbind("keydown",this._doKeyDown).unbind("keypress",this._doKeyPress).unbind("keyup",this._doKeyUp)
}else{if(nodeName=="div"||nodeName=="span"){$target.removeClass(this.markerClassName).empty()
}}},_enableDatepicker:function(target){var $target=$(target);
var inst=$.data(target,PROP_NAME);
if(!$target.hasClass(this.markerClassName)){return 
}var nodeName=target.nodeName.toLowerCase();
if(nodeName=="input"){target.disabled=false;
inst.trigger.filter("button").each(function(){this.disabled=false
}).end().filter("img").css({opacity:"1.0",cursor:""})
}else{if(nodeName=="div"||nodeName=="span"){var inline=$target.children("."+this._inlineClass);
inline.children().removeClass("ui-state-disabled");
inline.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",false)
}}this._disabledInputs=$.map(this._disabledInputs,function(value){return(value==target?null:value)
})
},_disableDatepicker:function(target){var $target=$(target);
var inst=$.data(target,PROP_NAME);
if(!$target.hasClass(this.markerClassName)){return 
}var nodeName=target.nodeName.toLowerCase();
if(nodeName=="input"){target.disabled=true;
inst.trigger.filter("button").each(function(){this.disabled=true
}).end().filter("img").css({opacity:"0.5",cursor:"default"})
}else{if(nodeName=="div"||nodeName=="span"){var inline=$target.children("."+this._inlineClass);
inline.children().addClass("ui-state-disabled");
inline.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",true)
}}this._disabledInputs=$.map(this._disabledInputs,function(value){return(value==target?null:value)
});
this._disabledInputs[this._disabledInputs.length]=target
},_isDisabledDatepicker:function(target){if(!target){return false
}for(var i=0;
i<this._disabledInputs.length;
i++){if(this._disabledInputs[i]==target){return true
}}return false
},_getInst:function(target){try{return $.data(target,PROP_NAME)
}catch(err){throw"Missing instance data for this datepicker"
}},_optionDatepicker:function(target,name,value){var inst=this._getInst(target);
if(arguments.length==2&&typeof name=="string"){return(name=="defaults"?$.extend({},$.datepicker._defaults):(inst?(name=="all"?$.extend({},inst.settings):this._get(inst,name)):null))
}var settings=name||{};
if(typeof name=="string"){settings={};
settings[name]=value
}if(inst){if(this._curInst==inst){this._hideDatepicker()
}var date=this._getDateDatepicker(target,true);
var minDate=this._getMinMaxDate(inst,"min");
var maxDate=this._getMinMaxDate(inst,"max");
extendRemove(inst.settings,settings);
if(minDate!==null&&settings.dateFormat!==undefined&&settings.minDate===undefined){inst.settings.minDate=this._formatDate(inst,minDate)
}if(maxDate!==null&&settings.dateFormat!==undefined&&settings.maxDate===undefined){inst.settings.maxDate=this._formatDate(inst,maxDate)
}this._attachments($(target),inst);
this._autoSize(inst);
this._setDate(inst,date);
this._updateAlternate(inst);
this._updateDatepicker(inst)
}},_changeDatepicker:function(target,name,value){this._optionDatepicker(target,name,value)
},_refreshDatepicker:function(target){var inst=this._getInst(target);
if(inst){this._updateDatepicker(inst)
}},_setDateDatepicker:function(target,date){var inst=this._getInst(target);
if(inst){this._setDate(inst,date);
this._updateDatepicker(inst);
this._updateAlternate(inst)
}},_getDateDatepicker:function(target,noDefault){var inst=this._getInst(target);
if(inst&&!inst.inline){this._setDateFromField(inst,noDefault)
}return(inst?this._getDate(inst):null)
},_doKeyDown:function(event){var inst=$.datepicker._getInst(event.target);
var handled=true;
var isRTL=inst.dpDiv.is(".ui-datepicker-rtl");
inst._keyEvent=true;
if($.datepicker._datepickerShowing){switch(event.keyCode){case 9:$.datepicker._hideDatepicker();
handled=false;
break;
case 13:var sel=$("td."+$.datepicker._dayOverClass+":not(."+$.datepicker._currentClass+")",inst.dpDiv);
if(sel[0]){$.datepicker._selectDay(event.target,inst.selectedMonth,inst.selectedYear,sel[0])
}var onSelect=$.datepicker._get(inst,"onSelect");
if(onSelect){var dateStr=$.datepicker._formatDate(inst);
onSelect.apply((inst.input?inst.input[0]:null),[dateStr,inst])
}else{$.datepicker._hideDatepicker()
}return false;
break;
case 27:$.datepicker._hideDatepicker();
break;
case 33:$.datepicker._adjustDate(event.target,(event.ctrlKey?-$.datepicker._get(inst,"stepBigMonths"):-$.datepicker._get(inst,"stepMonths")),"M");
break;
case 34:$.datepicker._adjustDate(event.target,(event.ctrlKey?+$.datepicker._get(inst,"stepBigMonths"):+$.datepicker._get(inst,"stepMonths")),"M");
break;
case 35:if(event.ctrlKey||event.metaKey){$.datepicker._clearDate(event.target)
}handled=event.ctrlKey||event.metaKey;
break;
case 36:if(event.ctrlKey||event.metaKey){$.datepicker._gotoToday(event.target)
}handled=event.ctrlKey||event.metaKey;
break;
case 37:if(event.ctrlKey||event.metaKey){$.datepicker._adjustDate(event.target,(isRTL?+1:-1),"D")
}handled=event.ctrlKey||event.metaKey;
if(event.originalEvent.altKey){$.datepicker._adjustDate(event.target,(event.ctrlKey?-$.datepicker._get(inst,"stepBigMonths"):-$.datepicker._get(inst,"stepMonths")),"M")
}break;
case 38:if(event.ctrlKey||event.metaKey){$.datepicker._adjustDate(event.target,-7,"D")
}handled=event.ctrlKey||event.metaKey;
break;
case 39:if(event.ctrlKey||event.metaKey){$.datepicker._adjustDate(event.target,(isRTL?-1:+1),"D")
}handled=event.ctrlKey||event.metaKey;
if(event.originalEvent.altKey){$.datepicker._adjustDate(event.target,(event.ctrlKey?+$.datepicker._get(inst,"stepBigMonths"):+$.datepicker._get(inst,"stepMonths")),"M")
}break;
case 40:if(event.ctrlKey||event.metaKey){$.datepicker._adjustDate(event.target,+7,"D")
}handled=event.ctrlKey||event.metaKey;
break;
default:handled=false
}}else{if(event.keyCode==36&&event.ctrlKey){$.datepicker._showDatepicker(this)
}else{handled=false
}}if(handled){event.preventDefault();
event.stopPropagation()
}},_doKeyPress:function(event){var inst=$.datepicker._getInst(event.target);
if($.datepicker._get(inst,"constrainInput")){var chars=$.datepicker._possibleChars($.datepicker._get(inst,"dateFormat"));
var chr=String.fromCharCode(event.charCode==undefined?event.keyCode:event.charCode);
return event.ctrlKey||event.metaKey||(chr<" "||!chars||chars.indexOf(chr)>-1)
}},_doKeyUp:function(event){var inst=$.datepicker._getInst(event.target);
if(inst.input.val()!=inst.lastVal){try{var date=$.datepicker.parseDate($.datepicker._get(inst,"dateFormat"),(inst.input?inst.input.val():null),$.datepicker._getFormatConfig(inst));
if(date){$.datepicker._setDateFromField(inst);
$.datepicker._updateAlternate(inst);
$.datepicker._updateDatepicker(inst)
}}catch(err){$.datepicker.log(err)
}}return true
},_showDatepicker:function(input){input=input.target||input;
if(input.nodeName.toLowerCase()!="input"){input=$("input",input.parentNode)[0]
}if($.datepicker._isDisabledDatepicker(input)||$.datepicker._lastInput==input){return 
}var inst=$.datepicker._getInst(input);
if($.datepicker._curInst&&$.datepicker._curInst!=inst){$.datepicker._curInst.dpDiv.stop(true,true);
if(inst&&$.datepicker._datepickerShowing){$.datepicker._hideDatepicker($.datepicker._curInst.input[0])
}}var beforeShow=$.datepicker._get(inst,"beforeShow");
var beforeShowSettings=beforeShow?beforeShow.apply(input,[input,inst]):{};
if(beforeShowSettings===false){return 
}extendRemove(inst.settings,beforeShowSettings);
inst.lastVal=null;
$.datepicker._lastInput=input;
$.datepicker._setDateFromField(inst);
if($.datepicker._inDialog){input.value=""
}if(!$.datepicker._pos){$.datepicker._pos=$.datepicker._findPos(input);
$.datepicker._pos[1]+=input.offsetHeight
}var isFixed=false;
$(input).parents().each(function(){isFixed|=$(this).css("position")=="fixed";
return !isFixed
});
var offset={left:$.datepicker._pos[0],top:$.datepicker._pos[1]};
$.datepicker._pos=null;
inst.dpDiv.empty();
inst.dpDiv.css({position:"absolute",display:"block",top:"-1000px"});
$.datepicker._updateDatepicker(inst);
offset=$.datepicker._checkOffset(inst,offset,isFixed);
inst.dpDiv.css({position:($.datepicker._inDialog&&$.blockUI?"static":(isFixed?"fixed":"absolute")),display:"none",left:offset.left+"px",top:offset.top+"px"});
if(!inst.inline){var showAnim=$.datepicker._get(inst,"showAnim");
var duration=$.datepicker._get(inst,"duration");
var postProcess=function(){var cover=inst.dpDiv.find("iframe.ui-datepicker-cover");
if(!!cover.length){var borders=$.datepicker._getBorders(inst.dpDiv);
cover.css({left:-borders[0],top:-borders[1],width:inst.dpDiv.outerWidth(),height:inst.dpDiv.outerHeight()})
}};
inst.dpDiv.zIndex($(input).zIndex()+1);
$.datepicker._datepickerShowing=true;
if($.effects&&($.effects.effect[showAnim]||$.effects[showAnim])){inst.dpDiv.show(showAnim,$.datepicker._get(inst,"showOptions"),duration,postProcess)
}else{inst.dpDiv[showAnim||"show"]((showAnim?duration:null),postProcess)
}if(!showAnim||!duration){postProcess()
}if(inst.input.is(":visible")&&!inst.input.is(":disabled")){inst.input.focus()
}$.datepicker._curInst=inst
}},_updateDatepicker:function(inst){this.maxRows=4;
var borders=$.datepicker._getBorders(inst.dpDiv);
instActive=inst;
inst.dpDiv.empty().append(this._generateHTML(inst));
this._attachHandlers(inst);
var cover=inst.dpDiv.find("iframe.ui-datepicker-cover");
if(!!cover.length){cover.css({left:-borders[0],top:-borders[1],width:inst.dpDiv.outerWidth(),height:inst.dpDiv.outerHeight()})
}inst.dpDiv.find("."+this._dayOverClass+" a").mouseover();
var numMonths=this._getNumberOfMonths(inst);
var cols=numMonths[1];
var width=17;
inst.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width("");
if(cols>1){inst.dpDiv.addClass("ui-datepicker-multi-"+cols).css("width",(width*cols)+"em")
}inst.dpDiv[(numMonths[0]!=1||numMonths[1]!=1?"add":"remove")+"Class"]("ui-datepicker-multi");
inst.dpDiv[(this._get(inst,"isRTL")?"add":"remove")+"Class"]("ui-datepicker-rtl");
if(inst==$.datepicker._curInst&&$.datepicker._datepickerShowing&&inst.input&&inst.input.is(":visible")&&!inst.input.is(":disabled")&&inst.input[0]!=document.activeElement){inst.input.focus()
}if(inst.yearshtml){var origyearshtml=inst.yearshtml;
setTimeout(function(){if(origyearshtml===inst.yearshtml&&inst.yearshtml){inst.dpDiv.find("select.ui-datepicker-year:first").replaceWith(inst.yearshtml)
}origyearshtml=inst.yearshtml=null
},0)
}},_getBorders:function(elem){var convert=function(value){return{thin:1,medium:2,thick:3}[value]||value
};
return[parseFloat(convert(elem.css("border-left-width"))),parseFloat(convert(elem.css("border-top-width")))]
},_checkOffset:function(inst,offset,isFixed){var dpWidth=inst.dpDiv.outerWidth();
var dpHeight=inst.dpDiv.outerHeight();
var inputWidth=inst.input?inst.input.outerWidth():0;
var inputHeight=inst.input?inst.input.outerHeight():0;
var viewWidth=document.documentElement.clientWidth+(isFixed?0:$(document).scrollLeft());
var viewHeight=document.documentElement.clientHeight+(isFixed?0:$(document).scrollTop());
offset.left-=(this._get(inst,"isRTL")?(dpWidth-inputWidth):0);
offset.left-=(isFixed&&offset.left==inst.input.offset().left)?$(document).scrollLeft():0;
offset.top-=(isFixed&&offset.top==(inst.input.offset().top+inputHeight))?$(document).scrollTop():0;
offset.left-=Math.min(offset.left,(offset.left+dpWidth>viewWidth&&viewWidth>dpWidth)?Math.abs(offset.left+dpWidth-viewWidth):0);
offset.top-=Math.min(offset.top,(offset.top+dpHeight>viewHeight&&viewHeight>dpHeight)?Math.abs(dpHeight+inputHeight):0);
return offset
},_findPos:function(obj){var inst=this._getInst(obj);
var isRTL=this._get(inst,"isRTL");
while(obj&&(obj.type=="hidden"||obj.nodeType!=1||$.expr.filters.hidden(obj))){obj=obj[isRTL?"previousSibling":"nextSibling"]
}var position=$(obj).offset();
return[position.left,position.top]
},_hideDatepicker:function(input){var inst=this._curInst;
if(!inst||(input&&inst!=$.data(input,PROP_NAME))){return 
}if(this._datepickerShowing){var showAnim=this._get(inst,"showAnim");
var duration=this._get(inst,"duration");
var postProcess=function(){$.datepicker._tidyDialog(inst)
};
if($.effects&&($.effects.effect[showAnim]||$.effects[showAnim])){inst.dpDiv.hide(showAnim,$.datepicker._get(inst,"showOptions"),duration,postProcess)
}else{inst.dpDiv[(showAnim=="slideDown"?"slideUp":(showAnim=="fadeIn"?"fadeOut":"hide"))]((showAnim?duration:null),postProcess)
}if(!showAnim){postProcess()
}this._datepickerShowing=false;
var onClose=this._get(inst,"onClose");
if(onClose){onClose.apply((inst.input?inst.input[0]:null),[(inst.input?inst.input.val():""),inst])
}this._lastInput=null;
if(this._inDialog){this._dialogInput.css({position:"absolute",left:"0",top:"-100px"});
if($.blockUI){$.unblockUI();
$("body").append(this.dpDiv)
}}this._inDialog=false
}},_tidyDialog:function(inst){inst.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar")
},_checkExternalClick:function(event){if(!$.datepicker._curInst){return 
}var $target=$(event.target),inst=$.datepicker._getInst($target[0]);
if((($target[0].id!=$.datepicker._mainDivId&&$target.parents("#"+$.datepicker._mainDivId).length==0&&!$target.hasClass($.datepicker.markerClassName)&&!$target.closest("."+$.datepicker._triggerClass).length&&$.datepicker._datepickerShowing&&!($.datepicker._inDialog&&$.blockUI)))||($target.hasClass($.datepicker.markerClassName)&&$.datepicker._curInst!=inst)){$.datepicker._hideDatepicker()
}},_adjustDate:function(id,offset,period){var target=$(id);
var inst=this._getInst(target[0]);
if(this._isDisabledDatepicker(target[0])){return 
}this._adjustInstDate(inst,offset+(period=="M"?this._get(inst,"showCurrentAtPos"):0),period);
this._updateDatepicker(inst)
},_gotoToday:function(id){var target=$(id);
var inst=this._getInst(target[0]);
if(this._get(inst,"gotoCurrent")&&inst.currentDay){inst.selectedDay=inst.currentDay;
inst.drawMonth=inst.selectedMonth=inst.currentMonth;
inst.drawYear=inst.selectedYear=inst.currentYear
}else{var date=new Date();
inst.selectedDay=date.getDate();
inst.drawMonth=inst.selectedMonth=date.getMonth();
inst.drawYear=inst.selectedYear=date.getFullYear()
}this._notifyChange(inst);
this._adjustDate(target)
},_selectMonthYear:function(id,select,period){var target=$(id);
var inst=this._getInst(target[0]);
inst["selected"+(period=="M"?"Month":"Year")]=inst["draw"+(period=="M"?"Month":"Year")]=parseInt(select.options[select.selectedIndex].value,10);
this._notifyChange(inst);
this._adjustDate(target)
},_selectDay:function(id,month,year,td){var target=$(id);
if($(td).hasClass(this._unselectableClass)||this._isDisabledDatepicker(target[0])){return 
}var inst=this._getInst(target[0]);
inst.selectedDay=inst.currentDay=$("a",td).html();
inst.selectedMonth=inst.currentMonth=month;
inst.selectedYear=inst.currentYear=year;
this._selectDate(id,this._formatDate(inst,inst.currentDay,inst.currentMonth,inst.currentYear))
},_clearDate:function(id){var target=$(id);
var inst=this._getInst(target[0]);
this._selectDate(target,"")
},_selectDate:function(id,dateStr){var target=$(id);
var inst=this._getInst(target[0]);
dateStr=(dateStr!=null?dateStr:this._formatDate(inst));
if(inst.input){inst.input.val(dateStr)
}this._updateAlternate(inst);
var onSelect=this._get(inst,"onSelect");
if(onSelect){onSelect.apply((inst.input?inst.input[0]:null),[dateStr,inst])
}else{if(inst.input){inst.input.trigger("change")
}}if(inst.inline){this._updateDatepicker(inst)
}else{this._hideDatepicker();
this._lastInput=inst.input[0];
if(typeof (inst.input[0])!="object"){inst.input.focus()
}this._lastInput=null
}},_updateAlternate:function(inst){var altField=this._get(inst,"altField");
if(altField){var altFormat=this._get(inst,"altFormat")||this._get(inst,"dateFormat");
var date=this._getDate(inst);
var dateStr=this.formatDate(altFormat,date,this._getFormatConfig(inst));
$(altField).each(function(){$(this).val(dateStr)
})
}},noWeekends:function(date){var day=date.getDay();
return[(day>0&&day<6),""]
},iso8601Week:function(date){var checkDate=new Date(date.getTime());
checkDate.setDate(checkDate.getDate()+4-(checkDate.getDay()||7));
var time=checkDate.getTime();
checkDate.setMonth(0);
checkDate.setDate(1);
return Math.floor(Math.round((time-checkDate)/86400000)/7)+1
},parseDate:function(format,value,settings){if(format==null||value==null){throw"Invalid arguments"
}value=(typeof value=="object"?value.toString():value+"");
if(value==""){return null
}var shortYearCutoff=(settings?settings.shortYearCutoff:null)||this._defaults.shortYearCutoff;
shortYearCutoff=(typeof shortYearCutoff!="string"?shortYearCutoff:new Date().getFullYear()%100+parseInt(shortYearCutoff,10));
var dayNamesShort=(settings?settings.dayNamesShort:null)||this._defaults.dayNamesShort;
var dayNames=(settings?settings.dayNames:null)||this._defaults.dayNames;
var monthNamesShort=(settings?settings.monthNamesShort:null)||this._defaults.monthNamesShort;
var monthNames=(settings?settings.monthNames:null)||this._defaults.monthNames;
var year=-1;
var month=-1;
var day=-1;
var doy=-1;
var literal=false;
var lookAhead=function(match){var matches=(iFormat+1<format.length&&format.charAt(iFormat+1)==match);
if(matches){iFormat++
}return matches
};
var getNumber=function(match){var isDoubled=lookAhead(match);
var size=(match=="@"?14:(match=="!"?20:(match=="y"&&isDoubled?4:(match=="o"?3:2))));
var digits=new RegExp("^\\d{1,"+size+"}");
var num=value.substring(iValue).match(digits);
if(!num){throw"Missing number at position "+iValue
}iValue+=num[0].length;
return parseInt(num[0],10)
};
var getName=function(match,shortNames,longNames){var names=$.map(lookAhead(match)?longNames:shortNames,function(v,k){return[[k,v]]
}).sort(function(a,b){return -(a[1].length-b[1].length)
});
var index=-1;
$.each(names,function(i,pair){var name=pair[1];
if(value.substr(iValue,name.length).toLowerCase()==name.toLowerCase()){index=pair[0];
iValue+=name.length;
return false
}});
if(index!=-1){return index+1
}else{throw"Unknown name at position "+iValue
}};
var checkLiteral=function(){if(value.charAt(iValue)!=format.charAt(iFormat)){throw"Unexpected literal at position "+iValue
}iValue++
};
var iValue=0;
for(var iFormat=0;
iFormat<format.length;
iFormat++){if(literal){if(format.charAt(iFormat)=="'"&&!lookAhead("'")){literal=false
}else{checkLiteral()
}}else{switch(format.charAt(iFormat)){case"d":day=getNumber("d");
break;
case"D":getName("D",dayNamesShort,dayNames);
break;
case"o":doy=getNumber("o");
break;
case"m":month=getNumber("m");
break;
case"M":month=getName("M",monthNamesShort,monthNames);
break;
case"y":year=getNumber("y");
break;
case"@":var date=new Date(getNumber("@"));
year=date.getFullYear();
month=date.getMonth()+1;
day=date.getDate();
break;
case"!":var date=new Date((getNumber("!")-this._ticksTo1970)/10000);
year=date.getFullYear();
month=date.getMonth()+1;
day=date.getDate();
break;
case"'":if(lookAhead("'")){checkLiteral()
}else{literal=true
}break;
default:checkLiteral()
}}}if(iValue<value.length){var extra=value.substr(iValue);
if(!/^\s+/.test(extra)){throw"Extra/unparsed characters found in date: "+extra
}}if(year==-1){year=new Date().getFullYear()
}else{if(year<100){year+=new Date().getFullYear()-new Date().getFullYear()%100+(year<=shortYearCutoff?0:-100)
}}if(doy>-1){month=1;
day=doy;
do{var dim=this._getDaysInMonth(year,month-1);
if(day<=dim){break
}month++;
day-=dim
}while(true)
}var date=this._daylightSavingAdjust(new Date(year,month-1,day));
if(date.getFullYear()!=year||date.getMonth()+1!=month||date.getDate()!=day){throw"Invalid date"
}return date
},ATOM:"yy-mm-dd",COOKIE:"D, dd M yy",ISO_8601:"yy-mm-dd",RFC_822:"D, d M y",RFC_850:"DD, dd-M-y",RFC_1036:"D, d M y",RFC_1123:"D, d M yy",RFC_2822:"D, d M yy",RSS:"D, d M y",TICKS:"!",TIMESTAMP:"@",W3C:"yy-mm-dd",_ticksTo1970:(((1970-1)*365+Math.floor(1970/4)-Math.floor(1970/100)+Math.floor(1970/400))*24*60*60*10000000),formatDate:function(format,date,settings){if(!date){return""
}var dayNamesShort=(settings?settings.dayNamesShort:null)||this._defaults.dayNamesShort;
var dayNames=(settings?settings.dayNames:null)||this._defaults.dayNames;
var monthNamesShort=(settings?settings.monthNamesShort:null)||this._defaults.monthNamesShort;
var monthNames=(settings?settings.monthNames:null)||this._defaults.monthNames;
var lookAhead=function(match){var matches=(iFormat+1<format.length&&format.charAt(iFormat+1)==match);
if(matches){iFormat++
}return matches
};
var formatNumber=function(match,value,len){var num=""+value;
if(lookAhead(match)){while(num.length<len){num="0"+num
}}return num
};
var formatName=function(match,value,shortNames,longNames){return(lookAhead(match)?longNames[value]:shortNames[value])
};
var output="";
var literal=false;
if(date){for(var iFormat=0;
iFormat<format.length;
iFormat++){if(literal){if(format.charAt(iFormat)=="'"&&!lookAhead("'")){literal=false
}else{output+=format.charAt(iFormat)
}}else{switch(format.charAt(iFormat)){case"d":output+=formatNumber("d",date.getDate(),2);
break;
case"D":output+=formatName("D",date.getDay(),dayNamesShort,dayNames);
break;
case"o":output+=formatNumber("o",Math.round((new Date(date.getFullYear(),date.getMonth(),date.getDate()).getTime()-new Date(date.getFullYear(),0,0).getTime())/86400000),3);
break;
case"m":output+=formatNumber("m",date.getMonth()+1,2);
break;
case"M":output+=formatName("M",date.getMonth(),monthNamesShort,monthNames);
break;
case"y":output+=(lookAhead("y")?date.getFullYear():(date.getYear()%100<10?"0":"")+date.getYear()%100);
break;
case"@":output+=date.getTime();
break;
case"!":output+=date.getTime()*10000+this._ticksTo1970;
break;
case"'":if(lookAhead("'")){output+="'"
}else{literal=true
}break;
default:output+=format.charAt(iFormat)
}}}}return output
},_possibleChars:function(format){var chars="";
var literal=false;
var lookAhead=function(match){var matches=(iFormat+1<format.length&&format.charAt(iFormat+1)==match);
if(matches){iFormat++
}return matches
};
for(var iFormat=0;
iFormat<format.length;
iFormat++){if(literal){if(format.charAt(iFormat)=="'"&&!lookAhead("'")){literal=false
}else{chars+=format.charAt(iFormat)
}}else{switch(format.charAt(iFormat)){case"d":case"m":case"y":case"@":chars+="0123456789";
break;
case"D":case"M":return null;
case"'":if(lookAhead("'")){chars+="'"
}else{literal=true
}break;
default:chars+=format.charAt(iFormat)
}}}return chars
},_get:function(inst,name){return inst.settings[name]!==undefined?inst.settings[name]:this._defaults[name]
},_setDateFromField:function(inst,noDefault){if(inst.input.val()==inst.lastVal){return 
}var dateFormat=this._get(inst,"dateFormat");
var dates=inst.lastVal=inst.input?inst.input.val():null;
var date,defaultDate;
date=defaultDate=this._getDefaultDate(inst);
var settings=this._getFormatConfig(inst);
try{date=this.parseDate(dateFormat,dates,settings)||defaultDate
}catch(event){this.log(event);
dates=(noDefault?"":dates)
}inst.selectedDay=date.getDate();
inst.drawMonth=inst.selectedMonth=date.getMonth();
inst.drawYear=inst.selectedYear=date.getFullYear();
inst.currentDay=(dates?date.getDate():0);
inst.currentMonth=(dates?date.getMonth():0);
inst.currentYear=(dates?date.getFullYear():0);
this._adjustInstDate(inst)
},_getDefaultDate:function(inst){return this._restrictMinMax(inst,this._determineDate(inst,this._get(inst,"defaultDate"),new Date()))
},_determineDate:function(inst,date,defaultDate){var offsetNumeric=function(offset){var date=new Date();
date.setDate(date.getDate()+offset);
return date
};
var offsetString=function(offset){try{return $.datepicker.parseDate($.datepicker._get(inst,"dateFormat"),offset,$.datepicker._getFormatConfig(inst))
}catch(e){}var date=(offset.toLowerCase().match(/^c/)?$.datepicker._getDate(inst):null)||new Date();
var year=date.getFullYear();
var month=date.getMonth();
var day=date.getDate();
var pattern=/([+-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g;
var matches=pattern.exec(offset);
while(matches){switch(matches[2]||"d"){case"d":case"D":day+=parseInt(matches[1],10);
break;
case"w":case"W":day+=parseInt(matches[1],10)*7;
break;
case"m":case"M":month+=parseInt(matches[1],10);
day=Math.min(day,$.datepicker._getDaysInMonth(year,month));
break;
case"y":case"Y":year+=parseInt(matches[1],10);
day=Math.min(day,$.datepicker._getDaysInMonth(year,month));
break
}matches=pattern.exec(offset)
}return new Date(year,month,day)
};
var newDate=(date==null||date===""?defaultDate:(typeof date=="string"?offsetString(date):(typeof date=="number"?(isNaN(date)?defaultDate:offsetNumeric(date)):new Date(date.getTime()))));
newDate=(newDate&&newDate.toString()=="Invalid Date"?defaultDate:newDate);
if(newDate){newDate.setHours(0);
newDate.setMinutes(0);
newDate.setSeconds(0);
newDate.setMilliseconds(0)
}return this._daylightSavingAdjust(newDate)
},_daylightSavingAdjust:function(date){if(!date){return null
}date.setHours(date.getHours()>12?date.getHours()+2:0);
return date
},_setDate:function(inst,date,noChange){var clear=!date;
var origMonth=inst.selectedMonth;
var origYear=inst.selectedYear;
var newDate=this._restrictMinMax(inst,this._determineDate(inst,date,new Date()));
inst.selectedDay=inst.currentDay=newDate.getDate();
inst.drawMonth=inst.selectedMonth=inst.currentMonth=newDate.getMonth();
inst.drawYear=inst.selectedYear=inst.currentYear=newDate.getFullYear();
if((origMonth!=inst.selectedMonth||origYear!=inst.selectedYear)&&!noChange){this._notifyChange(inst)
}this._adjustInstDate(inst);
if(inst.input){inst.input.val(clear?"":this._formatDate(inst))
}},_getDate:function(inst){var startDate=(!inst.currentYear||(inst.input&&inst.input.val()=="")?null:this._daylightSavingAdjust(new Date(inst.currentYear,inst.currentMonth,inst.currentDay)));
return startDate
},_attachHandlers:function(inst){var stepMonths=this._get(inst,"stepMonths");
var id="#"+inst.id.replace(/\\\\/g,"\\");
inst.dpDiv.find("[data-handler]").map(function(){var handler={prev:function(){window["DP_jQuery_"+dpuuid].datepicker._adjustDate(id,-stepMonths,"M")
},next:function(){window["DP_jQuery_"+dpuuid].datepicker._adjustDate(id,+stepMonths,"M")
},hide:function(){window["DP_jQuery_"+dpuuid].datepicker._hideDatepicker()
},today:function(){window["DP_jQuery_"+dpuuid].datepicker._gotoToday(id)
},selectDay:function(){window["DP_jQuery_"+dpuuid].datepicker._selectDay(id,+this.getAttribute("data-month"),+this.getAttribute("data-year"),this);
return false
},selectMonth:function(){window["DP_jQuery_"+dpuuid].datepicker._selectMonthYear(id,this,"M");
return false
},selectYear:function(){window["DP_jQuery_"+dpuuid].datepicker._selectMonthYear(id,this,"Y");
return false
}};
$(this).bind(this.getAttribute("data-event"),handler[this.getAttribute("data-handler")])
})
},_generateHTML:function(inst){var today=new Date();
today=this._daylightSavingAdjust(new Date(today.getFullYear(),today.getMonth(),today.getDate()));
var isRTL=this._get(inst,"isRTL");
var showButtonPanel=this._get(inst,"showButtonPanel");
var hideIfNoPrevNext=this._get(inst,"hideIfNoPrevNext");
var navigationAsDateFormat=this._get(inst,"navigationAsDateFormat");
var numMonths=this._getNumberOfMonths(inst);
var showCurrentAtPos=this._get(inst,"showCurrentAtPos");
var stepMonths=this._get(inst,"stepMonths");
var isMultiMonth=(numMonths[0]!=1||numMonths[1]!=1);
var currentDate=this._daylightSavingAdjust((!inst.currentDay?new Date(9999,9,9):new Date(inst.currentYear,inst.currentMonth,inst.currentDay)));
var minDate=this._getMinMaxDate(inst,"min");
var maxDate=this._getMinMaxDate(inst,"max");
var drawMonth=inst.drawMonth-showCurrentAtPos;
var drawYear=inst.drawYear;
if(drawMonth<0){drawMonth+=12;
drawYear--
}if(maxDate){var maxDraw=this._daylightSavingAdjust(new Date(maxDate.getFullYear(),maxDate.getMonth()-(numMonths[0]*numMonths[1])+1,maxDate.getDate()));
maxDraw=(minDate&&maxDraw<minDate?minDate:maxDraw);
while(this._daylightSavingAdjust(new Date(drawYear,drawMonth,1))>maxDraw){drawMonth--;
if(drawMonth<0){drawMonth=11;
drawYear--
}}}inst.drawMonth=drawMonth;
inst.drawYear=drawYear;
var prevText=this._get(inst,"prevText");
prevText=(!navigationAsDateFormat?prevText:this.formatDate(prevText,this._daylightSavingAdjust(new Date(drawYear,drawMonth-stepMonths,1)),this._getFormatConfig(inst)));
var prev=(this._canAdjustMonth(inst,-1,drawYear,drawMonth)?'<a class="ui-datepicker-prev ui-corner-all" data-handler="prev" data-event="click" title="'+prevText+'"><span class="ui-icon ui-icon-circle-triangle-'+(isRTL?"e":"w")+'">'+prevText+"</span></a>":(hideIfNoPrevNext?"":'<a class="ui-datepicker-prev ui-corner-all ui-state-disabled" title="'+prevText+'"><span class="ui-icon ui-icon-circle-triangle-'+(isRTL?"e":"w")+'">'+prevText+"</span></a>"));
var nextText=this._get(inst,"nextText");
nextText=(!navigationAsDateFormat?nextText:this.formatDate(nextText,this._daylightSavingAdjust(new Date(drawYear,drawMonth+stepMonths,1)),this._getFormatConfig(inst)));
var next=(this._canAdjustMonth(inst,+1,drawYear,drawMonth)?'<a class="ui-datepicker-next ui-corner-all" data-handler="next" data-event="click" title="'+nextText+'"><span class="ui-icon ui-icon-circle-triangle-'+(isRTL?"w":"e")+'">'+nextText+"</span></a>":(hideIfNoPrevNext?"":'<a class="ui-datepicker-next ui-corner-all ui-state-disabled" title="'+nextText+'"><span class="ui-icon ui-icon-circle-triangle-'+(isRTL?"w":"e")+'">'+nextText+"</span></a>"));
var currentText=this._get(inst,"currentText");
var gotoDate=(this._get(inst,"gotoCurrent")&&inst.currentDay?currentDate:today);
currentText=(!navigationAsDateFormat?currentText:this.formatDate(currentText,gotoDate,this._getFormatConfig(inst)));
var controls=(!inst.inline?'<button type="button" class="ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all" data-handler="hide" data-event="click">'+this._get(inst,"closeText")+"</button>":"");
var buttonPanel=(showButtonPanel)?'<div class="ui-datepicker-buttonpane ui-widget-content">'+(isRTL?controls:"")+(this._isInRange(inst,gotoDate)?'<button type="button" class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" data-handler="today" data-event="click">'+currentText+"</button>":"")+(isRTL?"":controls)+"</div>":"";
var firstDay=parseInt(this._get(inst,"firstDay"),10);
firstDay=(isNaN(firstDay)?0:firstDay);
var showWeek=this._get(inst,"showWeek");
var dayNames=this._get(inst,"dayNames");
var dayNamesShort=this._get(inst,"dayNamesShort");
var dayNamesMin=this._get(inst,"dayNamesMin");
var monthNames=this._get(inst,"monthNames");
var monthNamesShort=this._get(inst,"monthNamesShort");
var beforeShowDay=this._get(inst,"beforeShowDay");
var showOtherMonths=this._get(inst,"showOtherMonths");
var selectOtherMonths=this._get(inst,"selectOtherMonths");
var calculateWeek=this._get(inst,"calculateWeek")||this.iso8601Week;
var defaultDate=this._getDefaultDate(inst);
var html="";
for(var row=0;
row<numMonths[0];
row++){var group="";
this.maxRows=4;
for(var col=0;
col<numMonths[1];
col++){var selectedDate=this._daylightSavingAdjust(new Date(drawYear,drawMonth,inst.selectedDay));
var cornerClass=" ui-corner-all";
var calender="";
if(isMultiMonth){calender+='<div class="ui-datepicker-group';
if(numMonths[1]>1){switch(col){case 0:calender+=" ui-datepicker-group-first";
cornerClass=" ui-corner-"+(isRTL?"right":"left");
break;
case numMonths[1]-1:calender+=" ui-datepicker-group-last";
cornerClass=" ui-corner-"+(isRTL?"left":"right");
break;
default:calender+=" ui-datepicker-group-middle";
cornerClass="";
break
}}calender+='">'
}calender+='<div class="ui-datepicker-header ui-widget-header ui-helper-clearfix'+cornerClass+'">'+(/all|left/.test(cornerClass)&&row==0?(isRTL?next:prev):"")+(/all|right/.test(cornerClass)&&row==0?(isRTL?prev:next):"")+this._generateMonthYearHeader(inst,drawMonth,drawYear,minDate,maxDate,row>0||col>0,monthNames,monthNamesShort)+'</div><table class="ui-datepicker-calendar"><thead><tr>';
var thead=(showWeek?'<th class="ui-datepicker-week-col">'+this._get(inst,"weekHeader")+"</th>":"");
for(var dow=0;
dow<7;
dow++){var day=(dow+firstDay)%7;
thead+="<th"+((dow+firstDay+6)%7>=5?' class="ui-datepicker-week-end"':"")+'><span title="'+dayNames[day]+'">'+dayNamesMin[day]+"</span></th>"
}calender+=thead+"</tr></thead><tbody>";
var daysInMonth=this._getDaysInMonth(drawYear,drawMonth);
if(drawYear==inst.selectedYear&&drawMonth==inst.selectedMonth){inst.selectedDay=Math.min(inst.selectedDay,daysInMonth)
}var leadDays=(this._getFirstDayOfMonth(drawYear,drawMonth)-firstDay+7)%7;
var curRows=Math.ceil((leadDays+daysInMonth)/7);
var numRows=(isMultiMonth?this.maxRows>curRows?this.maxRows:curRows:curRows);
this.maxRows=numRows;
var printDate=this._daylightSavingAdjust(new Date(drawYear,drawMonth,1-leadDays));
for(var dRow=0;
dRow<numRows;
dRow++){calender+="<tr>";
var tbody=(!showWeek?"":'<td class="ui-datepicker-week-col">'+this._get(inst,"calculateWeek")(printDate)+"</td>");
for(var dow=0;
dow<7;
dow++){var daySettings=(beforeShowDay?beforeShowDay.apply((inst.input?inst.input[0]:null),[printDate]):[true,""]);
var otherMonth=(printDate.getMonth()!=drawMonth);
var unselectable=(otherMonth&&!selectOtherMonths)||!daySettings[0]||(minDate&&printDate<minDate)||(maxDate&&printDate>maxDate);
tbody+='<td class="'+((dow+firstDay+6)%7>=5?" ui-datepicker-week-end":"")+(otherMonth?" ui-datepicker-other-month":"")+((printDate.getTime()==selectedDate.getTime()&&drawMonth==inst.selectedMonth&&inst._keyEvent)||(defaultDate.getTime()==printDate.getTime()&&defaultDate.getTime()==selectedDate.getTime())?" "+this._dayOverClass:"")+(unselectable?" "+this._unselectableClass+" ui-state-disabled":"")+(otherMonth&&!showOtherMonths?"":" "+daySettings[1]+(printDate.getTime()==currentDate.getTime()?" "+this._currentClass:"")+(printDate.getTime()==today.getTime()?" ui-datepicker-today":""))+'"'+((!otherMonth||showOtherMonths)&&daySettings[2]?' title="'+daySettings[2]+'"':"")+(unselectable?"":' data-handler="selectDay" data-event="click" data-month="'+printDate.getMonth()+'" data-year="'+printDate.getFullYear()+'"')+">"+(otherMonth&&!showOtherMonths?"&#xa0;":(unselectable?'<span class="ui-state-default">'+printDate.getDate()+"</span>":'<a class="ui-state-default'+(printDate.getTime()==today.getTime()?" ui-state-highlight":"")+(printDate.getTime()==currentDate.getTime()?" ui-state-active":"")+(otherMonth?" ui-priority-secondary":"")+'" href="#">'+printDate.getDate()+"</a>"))+"</td>";
printDate.setDate(printDate.getDate()+1);
printDate=this._daylightSavingAdjust(printDate)
}calender+=tbody+"</tr>"
}drawMonth++;
if(drawMonth>11){drawMonth=0;
drawYear++
}calender+="</tbody></table>"+(isMultiMonth?"</div>"+((numMonths[0]>0&&col==numMonths[1]-1)?'<div class="ui-datepicker-row-break"></div>':""):"");
group+=calender
}html+=group
}html+=buttonPanel+($.ui.ie6&&!inst.inline?'<iframe src="javascript:false;" class="ui-datepicker-cover" frameborder="0"></iframe>':"");
inst._keyEvent=false;
return html
},_generateMonthYearHeader:function(inst,drawMonth,drawYear,minDate,maxDate,secondary,monthNames,monthNamesShort){var changeMonth=this._get(inst,"changeMonth");
var changeYear=this._get(inst,"changeYear");
var showMonthAfterYear=this._get(inst,"showMonthAfterYear");
var html='<div class="ui-datepicker-title">';
var monthHtml="";
if(secondary||!changeMonth){monthHtml+='<span class="ui-datepicker-month">'+monthNames[drawMonth]+"</span>"
}else{var inMinYear=(minDate&&minDate.getFullYear()==drawYear);
var inMaxYear=(maxDate&&maxDate.getFullYear()==drawYear);
monthHtml+='<select class="ui-datepicker-month" data-handler="selectMonth" data-event="change">';
for(var month=0;
month<12;
month++){if((!inMinYear||month>=minDate.getMonth())&&(!inMaxYear||month<=maxDate.getMonth())){monthHtml+='<option value="'+month+'"'+(month==drawMonth?' selected="selected"':"")+">"+monthNamesShort[month]+"</option>"
}}monthHtml+="</select>"
}if(!showMonthAfterYear){html+=monthHtml+(secondary||!(changeMonth&&changeYear)?"&#xa0;":"")
}if(!inst.yearshtml){inst.yearshtml="";
if(secondary||!changeYear){html+='<span class="ui-datepicker-year">'+drawYear+"</span>"
}else{var years=this._get(inst,"yearRange").split(":");
var thisYear=new Date().getFullYear();
var determineYear=function(value){var year=(value.match(/c[+-].*/)?drawYear+parseInt(value.substring(1),10):(value.match(/[+-].*/)?thisYear+parseInt(value,10):parseInt(value,10)));
return(isNaN(year)?thisYear:year)
};
var year=determineYear(years[0]);
var endYear=Math.max(year,determineYear(years[1]||""));
year=(minDate?Math.max(year,minDate.getFullYear()):year);
endYear=(maxDate?Math.min(endYear,maxDate.getFullYear()):endYear);
inst.yearshtml+='<select class="ui-datepicker-year" data-handler="selectYear" data-event="change">';
for(;
year<=endYear;
year++){inst.yearshtml+='<option value="'+year+'"'+(year==drawYear?' selected="selected"':"")+">"+year+"</option>"
}inst.yearshtml+="</select>";
html+=inst.yearshtml;
inst.yearshtml=null
}}html+=this._get(inst,"yearSuffix");
if(showMonthAfterYear){html+=(secondary||!(changeMonth&&changeYear)?"&#xa0;":"")+monthHtml
}html+="</div>";
return html
},_adjustInstDate:function(inst,offset,period){var year=inst.drawYear+(period=="Y"?offset:0);
var month=inst.drawMonth+(period=="M"?offset:0);
var day=Math.min(inst.selectedDay,this._getDaysInMonth(year,month))+(period=="D"?offset:0);
var date=this._restrictMinMax(inst,this._daylightSavingAdjust(new Date(year,month,day)));
inst.selectedDay=date.getDate();
inst.drawMonth=inst.selectedMonth=date.getMonth();
inst.drawYear=inst.selectedYear=date.getFullYear();
if(period=="M"||period=="Y"){this._notifyChange(inst)
}},_restrictMinMax:function(inst,date){var minDate=this._getMinMaxDate(inst,"min");
var maxDate=this._getMinMaxDate(inst,"max");
var newDate=(minDate&&date<minDate?minDate:date);
newDate=(maxDate&&newDate>maxDate?maxDate:newDate);
return newDate
},_notifyChange:function(inst){var onChange=this._get(inst,"onChangeMonthYear");
if(onChange){onChange.apply((inst.input?inst.input[0]:null),[inst.selectedYear,inst.selectedMonth+1,inst])
}},_getNumberOfMonths:function(inst){var numMonths=this._get(inst,"numberOfMonths");
return(numMonths==null?[1,1]:(typeof numMonths=="number"?[1,numMonths]:numMonths))
},_getMinMaxDate:function(inst,minMax){return this._determineDate(inst,this._get(inst,minMax+"Date"),null)
},_getDaysInMonth:function(year,month){return 32-this._daylightSavingAdjust(new Date(year,month,32)).getDate()
},_getFirstDayOfMonth:function(year,month){return new Date(year,month,1).getDay()
},_canAdjustMonth:function(inst,offset,curYear,curMonth){var numMonths=this._getNumberOfMonths(inst);
var date=this._daylightSavingAdjust(new Date(curYear,curMonth+(offset<0?offset:numMonths[0]*numMonths[1]),1));
if(offset<0){date.setDate(this._getDaysInMonth(date.getFullYear(),date.getMonth()))
}return this._isInRange(inst,date)
},_isInRange:function(inst,date){var minDate=this._getMinMaxDate(inst,"min");
var maxDate=this._getMinMaxDate(inst,"max");
return((!minDate||date.getTime()>=minDate.getTime())&&(!maxDate||date.getTime()<=maxDate.getTime()))
},_getFormatConfig:function(inst){var shortYearCutoff=this._get(inst,"shortYearCutoff");
shortYearCutoff=(typeof shortYearCutoff!="string"?shortYearCutoff:new Date().getFullYear()%100+parseInt(shortYearCutoff,10));
return{shortYearCutoff:shortYearCutoff,dayNamesShort:this._get(inst,"dayNamesShort"),dayNames:this._get(inst,"dayNames"),monthNamesShort:this._get(inst,"monthNamesShort"),monthNames:this._get(inst,"monthNames")}
},_formatDate:function(inst,day,month,year){if(!day){inst.currentDay=inst.selectedDay;
inst.currentMonth=inst.selectedMonth;
inst.currentYear=inst.selectedYear
}var date=(day?(typeof day=="object"?day:this._daylightSavingAdjust(new Date(year,month,day))):this._daylightSavingAdjust(new Date(inst.currentYear,inst.currentMonth,inst.currentDay)));
return this.formatDate(this._get(inst,"dateFormat"),date,this._getFormatConfig(inst))
}});
function bindHover(dpDiv){var selector="button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";
return dpDiv.delegate(selector,"mouseout",function(){$(this).removeClass("ui-state-hover");
if(this.className.indexOf("ui-datepicker-prev")!=-1){$(this).removeClass("ui-datepicker-prev-hover")
}if(this.className.indexOf("ui-datepicker-next")!=-1){$(this).removeClass("ui-datepicker-next-hover")
}}).delegate(selector,"mouseover",function(){if(!$.datepicker._isDisabledDatepicker(instActive.inline?dpDiv.parent()[0]:instActive.input[0])){$(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover");
$(this).addClass("ui-state-hover");
if(this.className.indexOf("ui-datepicker-prev")!=-1){$(this).addClass("ui-datepicker-prev-hover")
}if(this.className.indexOf("ui-datepicker-next")!=-1){$(this).addClass("ui-datepicker-next-hover")
}}})
}function extendRemove(target,props){$.extend(target,props);
for(var name in props){if(props[name]==null||props[name]==undefined){target[name]=props[name]
}}return target
}$.fn.datepicker=function(options){if(!this.length){return this
}if(!$.datepicker.initialized){$(document).mousedown($.datepicker._checkExternalClick).find(document.body).append($.datepicker.dpDiv);
$.datepicker.initialized=true
}var otherArgs=Array.prototype.slice.call(arguments,1);
if(typeof options=="string"&&(options=="isDisabled"||options=="getDate"||options=="widget")){return $.datepicker["_"+options+"Datepicker"].apply($.datepicker,[this[0]].concat(otherArgs))
}if(options=="option"&&arguments.length==2&&typeof arguments[1]=="string"){return $.datepicker["_"+options+"Datepicker"].apply($.datepicker,[this[0]].concat(otherArgs))
}return this.each(function(){typeof options=="string"?$.datepicker["_"+options+"Datepicker"].apply($.datepicker,[this].concat(otherArgs)):$.datepicker._attachDatepicker(this,options)
})
};
$.datepicker=new Datepicker();
$.datepicker.initialized=false;
$.datepicker.uuid=new Date().getTime();
$.datepicker.version="1.9.1";
window["DP_jQuery_"+dpuuid]=$
})(jQuery);
(function(D,E){var B="ui-dialog ui-widget ui-widget-content ui-corner-all ",A={buttons:true,height:true,maxHeight:true,maxWidth:true,minHeight:true,minWidth:true,width:true},C={maxHeight:true,maxWidth:true,minHeight:true,minWidth:true};
D.widget("ui.dialog",{version:"1.9.1",options:{autoOpen:true,buttons:{},closeOnEscape:true,closeText:"close",dialogClass:"",draggable:true,hide:null,height:"auto",maxHeight:false,maxWidth:false,minHeight:150,minWidth:150,modal:false,position:{my:"center",at:"center",of:window,collision:"fit",using:function(G){var F=D(this).css(G).offset().top;
if(F<0){D(this).css("top",G.top-F)
}}},resizable:true,show:null,stack:true,title:"",width:300,zIndex:1000},_create:function(){this.originalTitle=this.element.attr("title");
if(typeof this.originalTitle!=="string"){this.originalTitle=""
}this.oldPosition={parent:this.element.parent(),index:this.element.parent().children().index(this.element)};
this.options.title=this.options.title||this.originalTitle;
var K=this,J=this.options,M=J.title||"&#160;",H,L,F,I,G;
H=(this.uiDialog=D("<div>")).addClass(B+J.dialogClass).css({display:"none",outline:0,zIndex:J.zIndex}).attr("tabIndex",-1).keydown(function(N){if(J.closeOnEscape&&!N.isDefaultPrevented()&&N.keyCode&&N.keyCode===D.ui.keyCode.ESCAPE){K.close(N);
N.preventDefault()
}}).mousedown(function(N){K.moveToTop(false,N)
}).appendTo("body");
this.element.show().removeAttr("title").addClass("ui-dialog-content ui-widget-content").appendTo(H);
L=(this.uiDialogTitlebar=D("<div>")).addClass("ui-dialog-titlebar  ui-widget-header  ui-corner-all  ui-helper-clearfix").bind("mousedown",function(){H.focus()
}).prependTo(H);
F=D("<a href='#'></a>").addClass("ui-dialog-titlebar-close  ui-corner-all").attr("role","button").click(function(N){N.preventDefault();
K.close(N)
}).appendTo(L);
(this.uiDialogTitlebarCloseText=D("<span>")).addClass("ui-icon ui-icon-closethick").text(J.closeText).appendTo(F);
I=D("<span>").uniqueId().addClass("ui-dialog-title").html(M).prependTo(L);
G=(this.uiDialogButtonPane=D("<div>")).addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix");
(this.uiButtonSet=D("<div>")).addClass("ui-dialog-buttonset").appendTo(G);
H.attr({role:"dialog","aria-labelledby":I.attr("id")});
L.find("*").add(L).disableSelection();
this._hoverable(F);
this._focusable(F);
if(J.draggable&&D.fn.draggable){this._makeDraggable()
}if(J.resizable&&D.fn.resizable){this._makeResizable()
}this._createButtons(J.buttons);
this._isOpen=false;
if(D.fn.bgiframe){H.bgiframe()
}this._on(H,{keydown:function(P){if(!J.modal||P.keyCode!==D.ui.keyCode.TAB){return 
}var O=D(":tabbable",H),Q=O.filter(":first"),N=O.filter(":last");
if(P.target===N[0]&&!P.shiftKey){Q.focus(1);
return false
}else{if(P.target===Q[0]&&P.shiftKey){N.focus(1);
return false
}}}})
},_init:function(){if(this.options.autoOpen){this.open()
}},_destroy:function(){var G,F=this.oldPosition;
if(this.overlay){this.overlay.destroy()
}this.uiDialog.hide();
this.element.removeClass("ui-dialog-content ui-widget-content").hide().appendTo("body");
this.uiDialog.remove();
if(this.originalTitle){this.element.attr("title",this.originalTitle)
}G=F.parent.children().eq(F.index);
if(G.length&&G[0]!==this.element[0]){G.before(this.element)
}else{F.parent.append(this.element)
}},widget:function(){return this.uiDialog
},close:function(I){var H=this,G,F;
if(!this._isOpen){return 
}if(false===this._trigger("beforeClose",I)){return 
}this._isOpen=false;
if(this.overlay){this.overlay.destroy()
}if(this.options.hide){this._hide(this.uiDialog,this.options.hide,function(){H._trigger("close",I)
})
}else{this.uiDialog.hide();
this._trigger("close",I)
}D.ui.dialog.overlay.resize();
if(this.options.modal){G=0;
D(".ui-dialog").each(function(){if(this!==H.uiDialog[0]){F=D(this).css("z-index");
if(!isNaN(F)){G=Math.max(G,F)
}}});
D.ui.dialog.maxZ=G
}return this
},isOpen:function(){return this._isOpen
},moveToTop:function(I,H){var G=this.options,F;
if((G.modal&&!I)||(!G.stack&&!G.modal)){return this._trigger("focus",H)
}if(G.zIndex>D.ui.dialog.maxZ){D.ui.dialog.maxZ=G.zIndex
}if(this.overlay){D.ui.dialog.maxZ+=1;
D.ui.dialog.overlay.maxZ=D.ui.dialog.maxZ;
this.overlay.$el.css("z-index",D.ui.dialog.overlay.maxZ)
}F={scrollTop:this.element.scrollTop(),scrollLeft:this.element.scrollLeft()};
D.ui.dialog.maxZ+=1;
this.uiDialog.css("z-index",D.ui.dialog.maxZ);
this.element.attr(F);
this._trigger("focus",H);
return this
},open:function(){if(this._isOpen){return 
}var H,G=this.options,F=this.uiDialog;
this._size();
this._position(G.position);
F.show(G.show);
this.overlay=G.modal?new D.ui.dialog.overlay(this):null;
this.moveToTop(true);
H=this.element.find(":tabbable");
if(!H.length){H=this.uiDialogButtonPane.find(":tabbable");
if(!H.length){H=F
}}H.eq(0).focus();
this._isOpen=true;
this._trigger("open");
return this
},_createButtons:function(H){var G=this,F=false;
this.uiDialogButtonPane.remove();
this.uiButtonSet.empty();
if(typeof H==="object"&&H!==null){D.each(H,function(){return !(F=true)
})
}if(F){D.each(H,function(I,K){K=D.isFunction(K)?{click:K,text:I}:K;
var J=D("<button type='button'></button>").attr(K,true).unbind("click").click(function(){K.click.apply(G.element[0],arguments)
}).appendTo(G.uiButtonSet);
if(D.fn.button){J.button()
}});
this.uiDialog.addClass("ui-dialog-buttons");
this.uiDialogButtonPane.appendTo(this.uiDialog)
}else{this.uiDialog.removeClass("ui-dialog-buttons")
}},_makeDraggable:function(){var H=this,G=this.options;
function F(I){return{position:I.position,offset:I.offset}
}this.uiDialog.draggable({cancel:".ui-dialog-content, .ui-dialog-titlebar-close",handle:".ui-dialog-titlebar",containment:"document",start:function(I,J){D(this).addClass("ui-dialog-dragging");
H._trigger("dragStart",I,F(J))
},drag:function(I,J){H._trigger("drag",I,F(J))
},stop:function(I,J){G.position=[J.position.left-H.document.scrollLeft(),J.position.top-H.document.scrollTop()];
D(this).removeClass("ui-dialog-dragging");
H._trigger("dragStop",I,F(J));
D.ui.dialog.overlay.resize()
}})
},_makeResizable:function(J){J=(J===E?this.options.resizable:J);
var K=this,I=this.options,F=this.uiDialog.css("position"),H=typeof J==="string"?J:"n,e,s,w,se,sw,ne,nw";
function G(L){return{originalPosition:L.originalPosition,originalSize:L.originalSize,position:L.position,size:L.size}
}this.uiDialog.resizable({cancel:".ui-dialog-content",containment:"document",alsoResize:this.element,maxWidth:I.maxWidth,maxHeight:I.maxHeight,minWidth:I.minWidth,minHeight:this._minHeight(),handles:H,start:function(L,M){D(this).addClass("ui-dialog-resizing");
K._trigger("resizeStart",L,G(M))
},resize:function(L,M){K._trigger("resize",L,G(M))
},stop:function(L,M){D(this).removeClass("ui-dialog-resizing");
I.height=D(this).height();
I.width=D(this).width();
K._trigger("resizeStop",L,G(M));
D.ui.dialog.overlay.resize()
}}).css("position",F).find(".ui-resizable-se").addClass("ui-icon ui-icon-grip-diagonal-se")
},_minHeight:function(){var F=this.options;
if(F.height==="auto"){return F.minHeight
}else{return Math.min(F.minHeight,F.height)
}},_position:function(G){var H=[],I=[0,0],F;
if(G){if(typeof G==="string"||(typeof G==="object"&&"0" in G)){H=G.split?G.split(" "):[G[0],G[1]];
if(H.length===1){H[1]=H[0]
}D.each(["left","top"],function(K,J){if(+H[K]===H[K]){I[K]=H[K];
H[K]=J
}});
G={my:H[0]+(I[0]<0?I[0]:"+"+I[0])+" "+H[1]+(I[1]<0?I[1]:"+"+I[1]),at:H.join(" ")}
}G=D.extend({},D.ui.dialog.prototype.options.position,G)
}else{G=D.ui.dialog.prototype.options.position
}F=this.uiDialog.is(":visible");
if(!F){this.uiDialog.show()
}this.uiDialog.position(G);
if(!F){this.uiDialog.hide()
}},_setOptions:function(H){var I=this,F={},G=false;
D.each(H,function(J,K){I._setOption(J,K);
if(J in A){G=true
}if(J in C){F[J]=K
}});
if(G){this._size()
}if(this.uiDialog.is(":data(resizable)")){this.uiDialog.resizable("option",F)
}},_setOption:function(H,I){var G,J,F=this.uiDialog;
switch(H){case"buttons":this._createButtons(I);
break;
case"closeText":this.uiDialogTitlebarCloseText.text(""+I);
break;
case"dialogClass":F.removeClass(this.options.dialogClass).addClass(B+I);
break;
case"disabled":if(I){F.addClass("ui-dialog-disabled")
}else{F.removeClass("ui-dialog-disabled")
}break;
case"draggable":G=F.is(":data(draggable)");
if(G&&!I){F.draggable("destroy")
}if(!G&&I){this._makeDraggable()
}break;
case"position":this._position(I);
break;
case"resizable":J=F.is(":data(resizable)");
if(J&&!I){F.resizable("destroy")
}if(J&&typeof I==="string"){F.resizable("option","handles",I)
}if(!J&&I!==false){this._makeResizable(I)
}break;
case"title":D(".ui-dialog-title",this.uiDialogTitlebar).html(""+(I||"&#160;"));
break
}this._super(H,I)
},_size:function(){var G,J,I,H=this.options,F=this.uiDialog.is(":visible");
this.element.show().css({width:"auto",minHeight:0,height:0});
if(H.minWidth>H.width){H.width=H.minWidth
}G=this.uiDialog.css({height:"auto",width:H.width}).outerHeight();
J=Math.max(0,H.minHeight-G);
if(H.height==="auto"){if(D.support.minHeight){this.element.css({minHeight:J,height:"auto"})
}else{this.uiDialog.show();
I=this.element.css("height","auto").height();
if(!F){this.uiDialog.hide()
}this.element.height(Math.max(I,J))
}}else{this.element.height(Math.max(H.height-G,0))
}if(this.uiDialog.is(":data(resizable)")){this.uiDialog.resizable("option","minHeight",this._minHeight())
}}});
D.extend(D.ui.dialog,{uuid:0,maxZ:0,getTitleId:function(F){var G=F.attr("id");
if(!G){this.uuid+=1;
G=this.uuid
}return"ui-dialog-title-"+G
},overlay:function(F){this.$el=D.ui.dialog.overlay.create(F)
}});
D.extend(D.ui.dialog.overlay,{instances:[],oldInstances:[],maxZ:0,events:D.map("focus,mousedown,mouseup,keydown,keypress,click".split(","),function(F){return F+".dialog-overlay"
}).join(" "),create:function(G){if(this.instances.length===0){setTimeout(function(){if(D.ui.dialog.overlay.instances.length){D(document).bind(D.ui.dialog.overlay.events,function(H){if(D(H.target).zIndex()<D.ui.dialog.overlay.maxZ){return false
}})
}},1);
D(window).bind("resize.dialog-overlay",D.ui.dialog.overlay.resize)
}var F=(this.oldInstances.pop()||D("<div>").addClass("ui-widget-overlay"));
D(document).bind("keydown.dialog-overlay",function(H){var I=D.ui.dialog.overlay.instances;
if(I.length!==0&&I[I.length-1]===F&&G.options.closeOnEscape&&!H.isDefaultPrevented()&&H.keyCode&&H.keyCode===D.ui.keyCode.ESCAPE){G.close(H);
H.preventDefault()
}});
F.appendTo(document.body).css({width:this.width(),height:this.height()});
if(D.fn.bgiframe){F.bgiframe()
}this.instances.push(F);
return F
},destroy:function(F){var G=D.inArray(F,this.instances),H=0;
if(G!==-1){this.oldInstances.push(this.instances.splice(G,1)[0])
}if(this.instances.length===0){D([document,window]).unbind(".dialog-overlay")
}F.height(0).width(0).remove();
D.each(this.instances,function(){H=Math.max(H,this.css("z-index"))
});
this.maxZ=H
},height:function(){var G,F;
if(D.ui.ie){G=Math.max(document.documentElement.scrollHeight,document.body.scrollHeight);
F=Math.max(document.documentElement.offsetHeight,document.body.offsetHeight);
if(G<F){return D(window).height()+"px"
}else{return G+"px"
}}else{return D(document).height()+"px"
}},width:function(){var F,G;
if(D.ui.ie){F=Math.max(document.documentElement.scrollWidth,document.body.scrollWidth);
G=Math.max(document.documentElement.offsetWidth,document.body.offsetWidth);
if(F<G){return D(window).width()+"px"
}else{return F+"px"
}}else{return D(document).width()+"px"
}},resize:function(){var F=D([]);
D.each(D.ui.dialog.overlay.instances,function(){F=F.add(this)
});
F.css({width:0,height:0}).css({width:D.ui.dialog.overlay.width(),height:D.ui.dialog.overlay.height()})
}});
D.extend(D.ui.dialog.overlay.prototype,{destroy:function(){D.ui.dialog.overlay.destroy(this.$el)
}})
}(jQuery));
(function(B,C){var A=false;
B.widget("http://www.usfoods.com/etc/designs/usfoods/clientlibs/ui.menu",{version:"1.9.1",defaultElement:"<ul>",delay:300,options:{icons:{submenu:"ui-icon-carat-1-e"},menus:"ul",position:{my:"left top",at:"right top"},role:"menu",blur:null,focus:null,select:null},_create:function(){this.activeMenu=this.element;
this.element.uniqueId().addClass("ui-menu ui-widget ui-widget-content ui-corner-all").toggleClass("ui-menu-icons",!!this.element.find(".ui-icon").length).attr({role:this.options.role,tabIndex:0}).bind("click"+this.eventNamespace,B.proxy(function(D){if(this.options.disabled){D.preventDefault()
}},this));
if(this.options.disabled){this.element.addClass("ui-state-disabled").attr("aria-disabled","true")
}this._on({"mousedown .ui-menu-item > a":function(D){D.preventDefault()
},"click .ui-state-disabled > a":function(D){D.preventDefault()
},"click .ui-menu-item:has(a)":function(D){var E=B(D.target).closest(".ui-menu-item");
if(!A&&E.not(".ui-state-disabled").length){A=true;
this.select(D);
if(E.has(".ui-menu").length){this.expand(D)
}else{if(!this.element.is(":focus")){this.element.trigger("focus",[true]);
if(this.active&&this.active.parents(".ui-menu").length===1){clearTimeout(this.timer)
}}}}},"mouseenter .ui-menu-item":function(D){var E=B(D.currentTarget);
E.siblings().children(".ui-state-active").removeClass("ui-state-active");
this.focus(D,E)
},mouseleave:"collapseAll","mouseleave .ui-menu":"collapseAll",focus:function(F,D){var E=this.active||this.element.children(".ui-menu-item").eq(0);
if(!D){this.focus(F,E)
}},blur:function(D){this._delay(function(){if(!B.contains(this.element[0],this.document[0].activeElement)){this.collapseAll(D)
}})
},keydown:"_keydown"});
this.refresh();
this._on(this.document,{click:function(D){if(!B(D.target).closest(".ui-menu").length){this.collapseAll(D)
}A=false
}})
},_destroy:function(){this.element.removeAttr("aria-activedescendant").find(".ui-menu").andSelf().removeClass("ui-menu ui-widget ui-widget-content ui-corner-all ui-menu-icons").removeAttr("role").removeAttr("tabIndex").removeAttr("aria-labelledby").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-disabled").removeUniqueId().show();
this.element.find(".ui-menu-item").removeClass("ui-menu-item").removeAttr("role").removeAttr("aria-disabled").children("a").removeUniqueId().removeClass("ui-corner-all ui-state-hover").removeAttr("tabIndex").removeAttr("role").removeAttr("aria-haspopup").children().each(function(){var D=B(this);
if(D.data("ui-menu-submenu-carat")){D.remove()
}});
this.element.find(".ui-menu-divider").removeClass("ui-menu-divider ui-widget-content")
},_keydown:function(J){var E,I,K,H,G,D=true;
function F(L){return L.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&")
}switch(J.keyCode){case B.ui.keyCode.PAGE_UP:this.previousPage(J);
break;
case B.ui.keyCode.PAGE_DOWN:this.nextPage(J);
break;
case B.ui.keyCode.HOME:this._move("first","first",J);
break;
case B.ui.keyCode.END:this._move("last","last",J);
break;
case B.ui.keyCode.UP:this.previous(J);
break;
case B.ui.keyCode.DOWN:this.next(J);
break;
case B.ui.keyCode.LEFT:this.collapse(J);
break;
case B.ui.keyCode.RIGHT:if(this.active&&!this.active.is(".ui-state-disabled")){this.expand(J)
}break;
case B.ui.keyCode.ENTER:case B.ui.keyCode.SPACE:this._activate(J);
break;
case B.ui.keyCode.ESCAPE:this.collapse(J);
break;
default:D=false;
I=this.previousFilter||"";
K=String.fromCharCode(J.keyCode);
H=false;
clearTimeout(this.filterTimer);
if(K===I){H=true
}else{K=I+K
}G=new RegExp("^"+F(K),"i");
E=this.activeMenu.children(".ui-menu-item").filter(function(){return G.test(B(this).children("a").text())
});
E=H&&E.index(this.active.next())!==-1?this.active.nextAll(".ui-menu-item"):E;
if(!E.length){K=String.fromCharCode(J.keyCode);
G=new RegExp("^"+F(K),"i");
E=this.activeMenu.children(".ui-menu-item").filter(function(){return G.test(B(this).children("a").text())
})
}if(E.length){this.focus(J,E);
if(E.length>1){this.previousFilter=K;
this.filterTimer=this._delay(function(){delete this.previousFilter
},1000)
}else{delete this.previousFilter
}}else{delete this.previousFilter
}}if(D){J.preventDefault()
}},_activate:function(D){if(!this.active.is(".ui-state-disabled")){if(this.active.children("a[aria-haspopup='true']").length){this.expand(D)
}else{this.select(D)
}}},refresh:function(){var F,E=this.options.icons.submenu,D=this.element.find(this.options.menus+":not(.ui-menu)").addClass("ui-menu ui-widget ui-widget-content ui-corner-all").hide().attr({role:this.options.role,"aria-hidden":"true","aria-expanded":"false"});
F=D.add(this.element);
F.children(":not(.ui-menu-item):has(a)").addClass("ui-menu-item").attr("role","presentation").children("a").uniqueId().addClass("ui-corner-all").attr({tabIndex:-1,role:this._itemRole()});
F.children(":not(.ui-menu-item)").each(function(){var G=B(this);
if(!/[^\-â€”â€“\s]/.test(G.text())){G.addClass("ui-widget-content ui-menu-divider")
}});
F.children(".ui-state-disabled").attr("aria-disabled","true");
D.each(function(){var I=B(this),H=I.prev("a"),G=B("<span>").addClass("ui-menu-icon ui-icon "+E).data("ui-menu-submenu-carat",true);
H.attr("aria-haspopup","true").prepend(G);
I.attr("aria-labelledby",H.attr("id"))
});
if(this.active&&!B.contains(this.element[0],this.active[0])){this.blur()
}},_itemRole:function(){return{menu:"menuitem",listbox:"option"}[this.options.role]
},focus:function(E,D){var G,F;
this.blur(E,E&&E.type==="focus");
this._scrollIntoView(D);
this.active=D.first();
F=this.active.children("a").addClass("ui-state-focus");
if(this.options.role){this.element.attr("aria-activedescendant",F.attr("id"))
}this.active.parent().closest(".ui-menu-item").children("a:first").addClass("ui-state-active");
if(E&&E.type==="keydown"){this._close()
}else{this.timer=this._delay(function(){this._close()
},this.delay)
}G=D.children(".ui-menu");
if(G.length&&(/^mouse/.test(E.type))){this._startOpening(G)
}this.activeMenu=D.parent();
this._trigger("focus",E,{item:D})
},_scrollIntoView:function(G){var J,F,H,D,E,I;
if(this._hasScroll()){J=parseFloat(B.css(this.activeMenu[0],"borderTopWidth"))||0;
F=parseFloat(B.css(this.activeMenu[0],"paddingTop"))||0;
H=G.offset().top-this.activeMenu.offset().top-J-F;
D=this.activeMenu.scrollTop();
E=this.activeMenu.height();
I=G.height();
if(H<0){this.activeMenu.scrollTop(D+H)
}else{if(H+I>E){this.activeMenu.scrollTop(D+H-E+I)
}}}},blur:function(E,D){if(!D){clearTimeout(this.timer)
}if(!this.active){return 
}this.active.children("a").removeClass("ui-state-focus");
this.active=null;
this._trigger("blur",E,{item:this.active})
},_startOpening:function(D){clearTimeout(this.timer);
if(D.attr("aria-hidden")!=="true"){return 
}this.timer=this._delay(function(){this._close();
this._open(D)
},this.delay)
},_open:function(E){var D=B.extend({of:this.active},this.options.position);
clearTimeout(this.timer);
this.element.find(".ui-menu").not(E.parents(".ui-menu")).hide().attr("aria-hidden","true");
E.show().removeAttr("aria-hidden").attr("aria-expanded","true").position(D)
},collapseAll:function(E,D){clearTimeout(this.timer);
this.timer=this._delay(function(){var F=D?this.element:B(E&&E.target).closest(this.element.find(".ui-menu"));
if(!F.length){F=this.element
}this._close(F);
this.blur(E);
this.activeMenu=F
},this.delay)
},_close:function(D){if(!D){D=this.active?this.active.parent():this.element
}D.find(".ui-menu").hide().attr("aria-hidden","true").attr("aria-expanded","false").end().find("a.ui-state-active").removeClass("ui-state-active")
},collapse:function(E){var D=this.active&&this.active.parent().closest(".ui-menu-item",this.element);
if(D&&D.length){this._close();
this.focus(E,D)
}},expand:function(E){var D=this.active&&this.active.children(".ui-menu ").children(".ui-menu-item").first();
if(D&&D.length){this._open(D.parent());
this._delay(function(){this.focus(E,D)
})
}},next:function(D){this._move("next","first",D)
},previous:function(D){this._move("prev","last",D)
},isFirstItem:function(){return this.active&&!this.active.prevAll(".ui-menu-item").length
},isLastItem:function(){return this.active&&!this.active.nextAll(".ui-menu-item").length
},_move:function(G,E,F){var D;
if(this.active){if(G==="first"||G==="last"){D=this.active[G==="first"?"prevAll":"nextAll"](".ui-menu-item").eq(-1)
}else{D=this.active[G+"All"](".ui-menu-item").eq(0)
}}if(!D||!D.length||!this.active){D=this.activeMenu.children(".ui-menu-item")[E]()
}this.focus(F,D)
},nextPage:function(F){var E,G,D;
if(!this.active){this.next(F);
return 
}if(this.isLastItem()){return 
}if(this._hasScroll()){G=this.active.offset().top;
D=this.element.height();
this.active.nextAll(".ui-menu-item").each(function(){E=B(this);
return E.offset().top-G-D<0
});
this.focus(F,E)
}else{this.focus(F,this.activeMenu.children(".ui-menu-item")[!this.active?"first":"last"]())
}},previousPage:function(F){var E,G,D;
if(!this.active){this.next(F);
return 
}if(this.isFirstItem()){return 
}if(this._hasScroll()){G=this.active.offset().top;
D=this.element.height();
this.active.prevAll(".ui-menu-item").each(function(){E=B(this);
return E.offset().top-G+D>0
});
this.focus(F,E)
}else{this.focus(F,this.activeMenu.children(".ui-menu-item").first())
}},_hasScroll:function(){return this.element.outerHeight()<this.element.prop("scrollHeight")
},select:function(D){this.active=this.active||B(D.target).closest(".ui-menu-item");
var E={item:this.active};
if(!this.active.has(".ui-menu").length){this.collapseAll(D,true)
}this._trigger("select",D,E)
}})
}(jQuery));
(function(A,B){A.widget("ui.progressbar",{version:"1.9.1",options:{value:0,max:100},min:0,_create:function(){this.element.addClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").attr({role:"progressbar","aria-valuemin":this.min,"aria-valuemax":this.options.max,"aria-valuenow":this._value()});
this.valueDiv=A("<div class='ui-progressbar-value ui-widget-header ui-corner-left'></div>").appendTo(this.element);
this.oldValue=this._value();
this._refreshValue()
},_destroy:function(){this.element.removeClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow");
this.valueDiv.remove()
},value:function(C){if(C===B){return this._value()
}this._setOption("value",C);
return this
},_setOption:function(C,D){if(C==="value"){this.options.value=D;
this._refreshValue();
if(this._value()===this.options.max){this._trigger("complete")
}}this._super(C,D)
},_value:function(){var C=this.options.value;
if(typeof C!=="number"){C=0
}return Math.min(this.options.max,Math.max(this.min,C))
},_percentage:function(){return 100*this._value()/this.options.max
},_refreshValue:function(){var D=this.value(),C=this._percentage();
if(this.oldValue!==D){this.oldValue=D;
this._trigger("change")
}this.valueDiv.toggle(D>this.min).toggleClass("ui-corner-right",D===this.options.max).width(C.toFixed(0)+"%");
this.element.attr("aria-valuenow",D)
}})
})(jQuery);
(function(B,C){var A=5;
B.widget("ui.slider",B.ui.mouse,{version:"1.9.1",widgetEventPrefix:"slide",options:{animate:false,distance:0,max:100,min:0,orientation:"horizontal",range:false,step:1,value:0,values:null},_create:function(){var F,D,I=this.options,H=this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"),G="<a class='ui-slider-handle ui-state-default ui-corner-all' href='#'></a>",E=[];
this._keySliding=false;
this._mouseSliding=false;
this._animateOff=true;
this._handleIndex=null;
this._detectOrientation();
this._mouseInit();
this.element.addClass("ui-slider ui-slider-"+this.orientation+" ui-widget ui-widget-content ui-corner-all"+(I.disabled?" ui-slider-disabled ui-disabled":""));
this.range=B([]);
if(I.range){if(I.range===true){if(!I.values){I.values=[this._valueMin(),this._valueMin()]
}if(I.values.length&&I.values.length!==2){I.values=[I.values[0],I.values[0]]
}}this.range=B("<div></div>").appendTo(this.element).addClass("ui-slider-range ui-widget-header"+((I.range==="min"||I.range==="max")?" ui-slider-range-"+I.range:""))
}D=(I.values&&I.values.length)||1;
for(F=H.length;
F<D;
F++){E.push(G)
}this.handles=H.add(B(E.join("")).appendTo(this.element));
this.handle=this.handles.eq(0);
this.handles.add(this.range).filter("a").click(function(J){J.preventDefault()
}).mouseenter(function(){if(!I.disabled){B(this).addClass("ui-state-hover")
}}).mouseleave(function(){B(this).removeClass("ui-state-hover")
}).focus(function(){if(!I.disabled){B(".ui-slider .ui-state-focus").removeClass("ui-state-focus");
B(this).addClass("ui-state-focus")
}else{B(this).blur()
}}).blur(function(){B(this).removeClass("ui-state-focus")
});
this.handles.each(function(J){B(this).data("ui-slider-handle-index",J)
});
this._on(this.handles,{keydown:function(N){var O,L,K,M,J=B(N.target).data("ui-slider-handle-index");
switch(N.keyCode){case B.ui.keyCode.HOME:case B.ui.keyCode.END:case B.ui.keyCode.PAGE_UP:case B.ui.keyCode.PAGE_DOWN:case B.ui.keyCode.UP:case B.ui.keyCode.RIGHT:case B.ui.keyCode.DOWN:case B.ui.keyCode.LEFT:N.preventDefault();
if(!this._keySliding){this._keySliding=true;
B(N.target).addClass("ui-state-active");
O=this._start(N,J);
if(O===false){return 
}}break
}M=this.options.step;
if(this.options.values&&this.options.values.length){L=K=this.values(J)
}else{L=K=this.value()
}switch(N.keyCode){case B.ui.keyCode.HOME:K=this._valueMin();
break;
case B.ui.keyCode.END:K=this._valueMax();
break;
case B.ui.keyCode.PAGE_UP:K=this._trimAlignValue(L+((this._valueMax()-this._valueMin())/A));
break;
case B.ui.keyCode.PAGE_DOWN:K=this._trimAlignValue(L-((this._valueMax()-this._valueMin())/A));
break;
case B.ui.keyCode.UP:case B.ui.keyCode.RIGHT:if(L===this._valueMax()){return 
}K=this._trimAlignValue(L+M);
break;
case B.ui.keyCode.DOWN:case B.ui.keyCode.LEFT:if(L===this._valueMin()){return 
}K=this._trimAlignValue(L-M);
break
}this._slide(N,J,K)
},keyup:function(K){var J=B(K.target).data("ui-slider-handle-index");
if(this._keySliding){this._keySliding=false;
this._stop(K,J);
this._change(K,J);
B(K.target).removeClass("ui-state-active")
}}});
this._refreshValue();
this._animateOff=false
},_destroy:function(){this.handles.remove();
this.range.remove();
this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-slider-disabled ui-widget ui-widget-content ui-corner-all");
this._mouseDestroy()
},_mouseCapture:function(F){var J,M,E,H,L,N,I,D,K=this,G=this.options;
if(G.disabled){return false
}this.elementSize={width:this.element.outerWidth(),height:this.element.outerHeight()};
this.elementOffset=this.element.offset();
J={x:F.pageX,y:F.pageY};
M=this._normValueFromMouse(J);
E=this._valueMax()-this._valueMin()+1;
this.handles.each(function(O){var P=Math.abs(M-K.values(O));
if(E>P){E=P;
H=B(this);
L=O
}});
if(G.range===true&&this.values(1)===G.min){L+=1;
H=B(this.handles[L])
}N=this._start(F,L);
if(N===false){return false
}this._mouseSliding=true;
this._handleIndex=L;
H.addClass("ui-state-active").focus();
I=H.offset();
D=!B(F.target).parents().andSelf().is(".ui-slider-handle");
this._clickOffset=D?{left:0,top:0}:{left:F.pageX-I.left-(H.width()/2),top:F.pageY-I.top-(H.height()/2)-(parseInt(H.css("borderTopWidth"),10)||0)-(parseInt(H.css("borderBottomWidth"),10)||0)+(parseInt(H.css("marginTop"),10)||0)};
if(!this.handles.hasClass("ui-state-hover")){this._slide(F,L,M)
}this._animateOff=true;
return true
},_mouseStart:function(){return true
},_mouseDrag:function(F){var D={x:F.pageX,y:F.pageY},E=this._normValueFromMouse(D);
this._slide(F,this._handleIndex,E);
return false
},_mouseStop:function(D){this.handles.removeClass("ui-state-active");
this._mouseSliding=false;
this._stop(D,this._handleIndex);
this._change(D,this._handleIndex);
this._handleIndex=null;
this._clickOffset=null;
this._animateOff=false;
return false
},_detectOrientation:function(){this.orientation=(this.options.orientation==="vertical")?"vertical":"horizontal"
},_normValueFromMouse:function(E){var D,H,G,F,I;
if(this.orientation==="horizontal"){D=this.elementSize.width;
H=E.x-this.elementOffset.left-(this._clickOffset?this._clickOffset.left:0)
}else{D=this.elementSize.height;
H=E.y-this.elementOffset.top-(this._clickOffset?this._clickOffset.top:0)
}G=(H/D);
if(G>1){G=1
}if(G<0){G=0
}if(this.orientation==="vertical"){G=1-G
}F=this._valueMax()-this._valueMin();
I=this._valueMin()+G*F;
return this._trimAlignValue(I)
},_start:function(F,E){var D={handle:this.handles[E],value:this.value()};
if(this.options.values&&this.options.values.length){D.value=this.values(E);
D.values=this.values()
}return this._trigger("start",F,D)
},_slide:function(H,G,F){var D,E,I;
if(this.options.values&&this.options.values.length){D=this.values(G?0:1);
if((this.options.values.length===2&&this.options.range===true)&&((G===0&&F>D)||(G===1&&F<D))){F=D
}if(F!==this.values(G)){E=this.values();
E[G]=F;
I=this._trigger("slide",H,{handle:this.handles[G],value:F,values:E});
D=this.values(G?0:1);
if(I!==false){this.values(G,F,true)
}}}else{if(F!==this.value()){I=this._trigger("slide",H,{handle:this.handles[G],value:F});
if(I!==false){this.value(F)
}}}},_stop:function(F,E){var D={handle:this.handles[E],value:this.value()};
if(this.options.values&&this.options.values.length){D.value=this.values(E);
D.values=this.values()
}this._trigger("stop",F,D)
},_change:function(F,E){if(!this._keySliding&&!this._mouseSliding){var D={handle:this.handles[E],value:this.value()};
if(this.options.values&&this.options.values.length){D.value=this.values(E);
D.values=this.values()
}this._trigger("change",F,D)
}},value:function(D){if(arguments.length){this.options.value=this._trimAlignValue(D);
this._refreshValue();
this._change(null,0);
return 
}return this._value()
},values:function(E,H){var G,D,F;
if(arguments.length>1){this.options.values[E]=this._trimAlignValue(H);
this._refreshValue();
this._change(null,E);
return 
}if(arguments.length){if(B.isArray(arguments[0])){G=this.options.values;
D=arguments[0];
for(F=0;
F<G.length;
F+=1){G[F]=this._trimAlignValue(D[F]);
this._change(null,F)
}this._refreshValue()
}else{if(this.options.values&&this.options.values.length){return this._values(E)
}else{return this.value()
}}}else{return this._values()
}},_setOption:function(E,F){var D,G=0;
if(B.isArray(this.options.values)){G=this.options.values.length
}B.Widget.prototype._setOption.apply(this,arguments);
switch(E){case"disabled":if(F){this.handles.filter(".ui-state-focus").blur();
this.handles.removeClass("ui-state-hover");
this.handles.prop("disabled",true);
this.element.addClass("ui-disabled")
}else{this.handles.prop("disabled",false);
this.element.removeClass("ui-disabled")
}break;
case"orientation":this._detectOrientation();
this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-"+this.orientation);
this._refreshValue();
break;
case"value":this._animateOff=true;
this._refreshValue();
this._change(null,0);
this._animateOff=false;
break;
case"values":this._animateOff=true;
this._refreshValue();
for(D=0;
D<G;
D+=1){this._change(null,D)
}this._animateOff=false;
break;
case"min":case"max":this._animateOff=true;
this._refreshValue();
this._animateOff=false;
break
}},_value:function(){var D=this.options.value;
D=this._trimAlignValue(D);
return D
},_values:function(D){var G,F,E;
if(arguments.length){G=this.options.values[D];
G=this._trimAlignValue(G);
return G
}else{F=this.options.values.slice();
for(E=0;
E<F.length;
E+=1){F[E]=this._trimAlignValue(F[E])
}return F
}},_trimAlignValue:function(G){if(G<=this._valueMin()){return this._valueMin()
}if(G>=this._valueMax()){return this._valueMax()
}var D=(this.options.step>0)?this.options.step:1,F=(G-this._valueMin())%D,E=G-F;
if(Math.abs(F)*2>=D){E+=(F>0)?D:(-D)
}return parseFloat(E.toFixed(5))
},_valueMin:function(){return this.options.min
},_valueMax:function(){return this.options.max
},_refreshValue:function(){var I,H,L,J,M,G=this.options.range,F=this.options,K=this,E=(!this._animateOff)?F.animate:false,D={};
if(this.options.values&&this.options.values.length){this.handles.each(function(N){H=(K.values(N)-K._valueMin())/(K._valueMax()-K._valueMin())*100;
D[K.orientation==="horizontal"?"left":"bottom"]=H+"%";
B(this).stop(1,1)[E?"animate":"css"](D,F.animate);
if(K.options.range===true){if(K.orientation==="horizontal"){if(N===0){K.range.stop(1,1)[E?"animate":"css"]({left:H+"%"},F.animate)
}if(N===1){K.range[E?"animate":"css"]({width:(H-I)+"%"},{queue:false,duration:F.animate})
}}else{if(N===0){K.range.stop(1,1)[E?"animate":"css"]({bottom:(H)+"%"},F.animate)
}if(N===1){K.range[E?"animate":"css"]({height:(H-I)+"%"},{queue:false,duration:F.animate})
}}}I=H
})
}else{L=this.value();
J=this._valueMin();
M=this._valueMax();
H=(M!==J)?(L-J)/(M-J)*100:0;
D[this.orientation==="horizontal"?"left":"bottom"]=H+"%";
this.handle.stop(1,1)[E?"animate":"css"](D,F.animate);
if(G==="min"&&this.orientation==="horizontal"){this.range.stop(1,1)[E?"animate":"css"]({width:H+"%"},F.animate)
}if(G==="max"&&this.orientation==="horizontal"){this.range[E?"animate":"css"]({width:(100-H)+"%"},{queue:false,duration:F.animate})
}if(G==="min"&&this.orientation==="vertical"){this.range.stop(1,1)[E?"animate":"css"]({height:H+"%"},F.animate)
}if(G==="max"&&this.orientation==="vertical"){this.range[E?"animate":"css"]({height:(100-H)+"%"},{queue:false,duration:F.animate})
}}}})
}(jQuery));
(function(B){function A(C){return function(){var D=this.element.val();
C.apply(this,arguments);
this._refresh();
if(D!==this.element.val()){this._trigger("change")
}}
}B.widget("ui.spinner",{version:"1.9.1",defaultElement:"<input>",widgetEventPrefix:"spin",options:{culture:null,icons:{down:"ui-icon-triangle-1-s",up:"ui-icon-triangle-1-n"},incremental:true,max:null,min:null,numberFormat:null,page:10,step:1,change:null,spin:null,start:null,stop:null},_create:function(){this._setOption("max",this.options.max);
this._setOption("min",this.options.min);
this._setOption("step",this.options.step);
this._value(this.element.val(),true);
this._draw();
this._on(this._events);
this._refresh();
this._on(this.window,{beforeunload:function(){this.element.removeAttr("autocomplete")
}})
},_getCreateOptions:function(){var C={},D=this.element;
B.each(["min","max","step"],function(E,F){var G=D.attr(F);
if(G!==undefined&&G.length){C[F]=G
}});
return C
},_events:{keydown:function(C){if(this._start(C)&&this._keydown(C)){C.preventDefault()
}},keyup:"_stop",focus:function(){this.previous=this.element.val()
},blur:function(C){if(this.cancelBlur){delete this.cancelBlur;
return 
}this._refresh();
if(this.previous!==this.element.val()){this._trigger("change",C)
}},mousewheel:function(C,D){if(!D){return 
}if(!this.spinning&&!this._start(C)){return false
}this._spin((D>0?1:-1)*this.options.step,C);
clearTimeout(this.mousewheelTimer);
this.mousewheelTimer=this._delay(function(){if(this.spinning){this._stop(C)
}},100);
C.preventDefault()
},"mousedown .ui-spinner-button":function(D){var C;
C=this.element[0]===this.document[0].activeElement?this.previous:this.element.val();
function E(){var F=this.element[0]===this.document[0].activeElement;
if(!F){this.element.focus();
this.previous=C;
this._delay(function(){this.previous=C
})
}}D.preventDefault();
E.call(this);
this.cancelBlur=true;
this._delay(function(){delete this.cancelBlur;
E.call(this)
});
if(this._start(D)===false){return 
}this._repeat(null,B(D.currentTarget).hasClass("ui-spinner-up")?1:-1,D)
},"mouseup .ui-spinner-button":"_stop","mouseenter .ui-spinner-button":function(C){if(!B(C.currentTarget).hasClass("ui-state-active")){return 
}if(this._start(C)===false){return false
}this._repeat(null,B(C.currentTarget).hasClass("ui-spinner-up")?1:-1,C)
},"mouseleave .ui-spinner-button":"_stop"},_draw:function(){var C=this.uiSpinner=this.element.addClass("ui-spinner-input").attr("autocomplete","off").wrap(this._uiSpinnerHtml()).parent().append(this._buttonHtml());
this.element.attr("role","spinbutton");
this.buttons=C.find(".ui-spinner-button").attr("tabIndex",-1).button().removeClass("ui-corner-all");
if(this.buttons.height()>Math.ceil(C.height()*0.5)&&C.height()>0){C.height(C.height())
}if(this.options.disabled){this.disable()
}},_keydown:function(D){var C=this.options,E=B.ui.keyCode;
switch(D.keyCode){case E.UP:this._repeat(null,1,D);
return true;
case E.DOWN:this._repeat(null,-1,D);
return true;
case E.PAGE_UP:this._repeat(null,C.page,D);
return true;
case E.PAGE_DOWN:this._repeat(null,-C.page,D);
return true
}return false
},_uiSpinnerHtml:function(){return"<span class='ui-spinner ui-widget ui-widget-content ui-corner-all'></span>"
},_buttonHtml:function(){return"<a class='ui-spinner-button ui-spinner-up ui-corner-tr'><span class='ui-icon "+this.options.icons.up+"'>&#9650;</span></a><a class='ui-spinner-button ui-spinner-down ui-corner-br'><span class='ui-icon "+this.options.icons.down+"'>&#9660;</span></a>"
},_start:function(C){if(!this.spinning&&this._trigger("start",C)===false){return false
}if(!this.counter){this.counter=1
}this.spinning=true;
return true
},_repeat:function(D,C,E){D=D||500;
clearTimeout(this.timer);
this.timer=this._delay(function(){this._repeat(40,C,E)
},D);
this._spin(C*this.options.step,E)
},_spin:function(D,C){var E=this.value()||0;
if(!this.counter){this.counter=1
}E=this._adjustValue(E+D*this._increment(this.counter));
if(!this.spinning||this._trigger("spin",C,{value:E})!==false){this._value(E);
this.counter++
}},_increment:function(C){var D=this.options.incremental;
if(D){return B.isFunction(D)?D(C):Math.floor(C*C*C/50000-C*C/500+17*C/200+1)
}return 1
},_precision:function(){var C=this._precisionOf(this.options.step);
if(this.options.min!==null){C=Math.max(C,this._precisionOf(this.options.min))
}return C
},_precisionOf:function(D){var E=D.toString(),C=E.indexOf(".");
return C===-1?0:E.length-C-1
},_adjustValue:function(E){var D,F,C=this.options;
D=C.min!==null?C.min:0;
F=E-D;
F=Math.round(F/C.step)*C.step;
E=D+F;
E=parseFloat(E.toFixed(this._precision()));
if(C.max!==null&&E>C.max){return C.max
}if(C.min!==null&&E<C.min){return C.min
}return E
},_stop:function(C){if(!this.spinning){return 
}clearTimeout(this.timer);
clearTimeout(this.mousewheelTimer);
this.counter=0;
this.spinning=false;
this._trigger("stop",C)
},_setOption:function(C,D){if(C==="culture"||C==="numberFormat"){var E=this._parse(this.element.val());
this.options[C]=D;
this.element.val(this._format(E));
return 
}if(C==="max"||C==="min"||C==="step"){if(typeof D==="string"){D=this._parse(D)
}}this._super(C,D);
if(C==="disabled"){if(D){this.element.prop("disabled",true);
this.buttons.button("disable")
}else{this.element.prop("disabled",false);
this.buttons.button("enable")
}}},_setOptions:A(function(C){this._super(C);
this._value(this.element.val())
}),_parse:function(C){if(typeof C==="string"&&C!==""){C=window.Globalize&&this.options.numberFormat?Globalize.parseFloat(C,10,this.options.culture):+C
}return C===""||isNaN(C)?null:C
},_format:function(C){if(C===""){return""
}return window.Globalize&&this.options.numberFormat?Globalize.format(C,this.options.numberFormat,this.options.culture):C
},_refresh:function(){this.element.attr({"aria-valuemin":this.options.min,"aria-valuemax":this.options.max,"aria-valuenow":this._parse(this.element.val())})
},_value:function(E,C){var D;
if(E!==""){D=this._parse(E);
if(D!==null){if(!C){D=this._adjustValue(D)
}E=this._format(D)
}}this.element.val(E);
this._refresh()
},_destroy:function(){this.element.removeClass("ui-spinner-input").prop("disabled",false).removeAttr("autocomplete").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow");
this.uiSpinner.replaceWith(this.element)
},stepUp:A(function(C){this._stepUp(C)
}),_stepUp:function(C){this._spin((C||1)*this.options.step)
},stepDown:A(function(C){this._stepDown(C)
}),_stepDown:function(C){this._spin((C||1)*-this.options.step)
},pageUp:A(function(C){this._stepUp((C||1)*this.options.page)
}),pageDown:A(function(C){this._stepDown((C||1)*this.options.page)
}),value:function(C){if(!arguments.length){return this._parse(this.element.val())
}A(this._value).call(this,C)
},widget:function(){return this.uiSpinner
}})
}(jQuery));
(function(C,E){var A=0,F=/#.*$/;
function D(){return ++A
}function B(G){return G.hash.length>1&&G.href.replace(F,"")===location.href.replace(F,"")
}C.widget("http://www.usfoods.com/etc/designs/usfoods/clientlibs/ui.tabs",{version:"1.9.1",delay:300,options:{active:null,collapsible:false,event:"click",heightStyle:"content",hide:null,show:null,activate:null,beforeActivate:null,beforeLoad:null,load:null},_create:function(){var H=this,G=this.options,I=G.active,J=location.hash.substring(1);
this.running=false;
this.element.addClass("ui-tabs ui-widget ui-widget-content ui-corner-all").toggleClass("ui-tabs-collapsible",G.collapsible).delegate(".ui-tabs-nav > li","mousedown"+this.eventNamespace,function(K){if(C(this).is(".ui-state-disabled")){K.preventDefault()
}}).delegate(".ui-tabs-anchor","focus"+this.eventNamespace,function(){if(C(this).closest("li").is(".ui-state-disabled")){this.blur()
}});
this._processTabs();
if(I===null){if(J){this.tabs.each(function(K,L){if(C(L).attr("aria-controls")===J){I=K;
return false
}})
}if(I===null){I=this.tabs.index(this.tabs.filter(".ui-tabs-active"))
}if(I===null||I===-1){I=this.tabs.length?0:false
}}if(I!==false){I=this.tabs.index(this.tabs.eq(I));
if(I===-1){I=G.collapsible?false:0
}}G.active=I;
if(!G.collapsible&&G.active===false&&this.anchors.length){G.active=0
}if(C.isArray(G.disabled)){G.disabled=C.unique(G.disabled.concat(C.map(this.tabs.filter(".ui-state-disabled"),function(K){return H.tabs.index(K)
}))).sort()
}if(this.options.active!==false&&this.anchors.length){this.active=this._findActive(this.options.active)
}else{this.active=C()
}this._refresh();
if(this.active.length){this.load(G.active)
}},_getCreateEventData:function(){return{tab:this.active,panel:!this.active.length?C():this._getPanelForTab(this.active)}
},_tabKeydown:function(I){var H=C(this.document[0].activeElement).closest("li"),G=this.tabs.index(H),J=true;
if(this._handlePageNav(I)){return 
}switch(I.keyCode){case C.ui.keyCode.RIGHT:case C.ui.keyCode.DOWN:G++;
break;
case C.ui.keyCode.UP:case C.ui.keyCode.LEFT:J=false;
G--;
break;
case C.ui.keyCode.END:G=this.anchors.length-1;
break;
case C.ui.keyCode.HOME:G=0;
break;
case C.ui.keyCode.SPACE:I.preventDefault();
clearTimeout(this.activating);
this._activate(G);
return ;
case C.ui.keyCode.ENTER:I.preventDefault();
clearTimeout(this.activating);
this._activate(G===this.options.active?false:G);
return ;
default:return 
}I.preventDefault();
clearTimeout(this.activating);
G=this._focusNextTab(G,J);
if(!I.ctrlKey){H.attr("aria-selected","false");
this.tabs.eq(G).attr("aria-selected","true");
this.activating=this._delay(function(){this.option("active",G)
},this.delay)
}},_panelKeydown:function(G){if(this._handlePageNav(G)){return 
}if(G.ctrlKey&&G.keyCode===C.ui.keyCode.UP){G.preventDefault();
this.active.focus()
}},_handlePageNav:function(G){if(G.altKey&&G.keyCode===C.ui.keyCode.PAGE_UP){this._activate(this._focusNextTab(this.options.active-1,false));
return true
}if(G.altKey&&G.keyCode===C.ui.keyCode.PAGE_DOWN){this._activate(this._focusNextTab(this.options.active+1,true));
return true
}},_findNextTab:function(H,I){var G=this.tabs.length-1;
function J(){if(H>G){H=0
}if(H<0){H=G
}return H
}while(C.inArray(J(),this.options.disabled)!==-1){H=I?H+1:H-1
}return H
},_focusNextTab:function(G,H){G=this._findNextTab(G,H);
this.tabs.eq(G).focus();
return G
},_setOption:function(G,H){if(G==="active"){this._activate(H);
return 
}if(G==="disabled"){this._setupDisabled(H);
return 
}this._super(G,H);
if(G==="collapsible"){this.element.toggleClass("ui-tabs-collapsible",H);
if(!H&&this.options.active===false){this._activate(0)
}}if(G==="event"){this._setupEvents(H)
}if(G==="heightStyle"){this._setupHeightStyle(H)
}},_tabId:function(G){return G.attr("aria-controls")||"ui-tabs-"+D()
},_sanitizeSelector:function(G){return G?G.replace(/[!"$%&'()*+,.\/:;<=>?@\[\]\^`{|}~]/g,"\\$&"):""
},refresh:function(){var H=this.options,G=this.tablist.children(":has(a[href])");
H.disabled=C.map(G.filter(".ui-state-disabled"),function(I){return G.index(I)
});
this._processTabs();
if(H.active===false||!this.anchors.length){H.active=false;
this.active=C()
}else{if(this.active.length&&!C.contains(this.tablist[0],this.active[0])){if(this.tabs.length===H.disabled.length){H.active=false;
this.active=C()
}else{this._activate(this._findNextTab(Math.max(0,H.active-1),false))
}}else{H.active=this.tabs.index(this.active)
}}this._refresh()
},_refresh:function(){this._setupDisabled(this.options.disabled);
this._setupEvents(this.options.event);
this._setupHeightStyle(this.options.heightStyle);
this.tabs.not(this.active).attr({"aria-selected":"false",tabIndex:-1});
this.panels.not(this._getPanelForTab(this.active)).hide().attr({"aria-expanded":"false","aria-hidden":"true"});
if(!this.active.length){this.tabs.eq(0).attr("tabIndex",0)
}else{this.active.addClass("ui-tabs-active ui-state-active").attr({"aria-selected":"true",tabIndex:0});
this._getPanelForTab(this.active).show().attr({"aria-expanded":"true","aria-hidden":"false"})
}},_processTabs:function(){var G=this;
this.tablist=this._getList().addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").attr("role","tablist");
this.tabs=this.tablist.find("> li:has(a[href])").addClass("ui-state-default ui-corner-top").attr({role:"tab",tabIndex:-1});
this.anchors=this.tabs.map(function(){return C("a",this)[0]
}).addClass("ui-tabs-anchor").attr({role:"presentation",tabIndex:-1});
this.panels=C();
this.anchors.each(function(M,K){var H,I,L,J=C(K).uniqueId().attr("id"),N=C(K).closest("li"),O=N.attr("aria-controls");
if(B(K)){H=K.hash;
I=G.element.find(G._sanitizeSelector(H))
}else{L=G._tabId(N);
H="#"+L;
I=G.element.find(H);
if(!I.length){I=G._createPanel(L);
I.insertAfter(G.panels[M-1]||G.tablist)
}I.attr("aria-live","polite")
}if(I.length){G.panels=G.panels.add(I)
}if(O){N.data("ui-tabs-aria-controls",O)
}N.attr({"aria-controls":H.substring(1),"aria-labelledby":J});
I.attr("aria-labelledby",J)
});
this.panels.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").attr("role","tabpanel")
},_getList:function(){return this.element.find("ol,ul").eq(0)
},_createPanel:function(G){return C("<div>").attr("id",G).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").data("ui-tabs-destroy",true)
},_setupDisabled:function(I){if(C.isArray(I)){if(!I.length){I=false
}else{if(I.length===this.anchors.length){I=true
}}}for(var H=0,G;
(G=this.tabs[H]);
H++){if(I===true||C.inArray(H,I)!==-1){C(G).addClass("ui-state-disabled").attr("aria-disabled","true")
}else{C(G).removeClass("ui-state-disabled").removeAttr("aria-disabled")
}}this.options.disabled=I
},_setupEvents:function(H){var G={click:function(I){I.preventDefault()
}};
if(H){C.each(H.split(" "),function(J,I){G[I]="_eventHandler"
})
}this._off(this.anchors.add(this.tabs).add(this.panels));
this._on(this.anchors,G);
this._on(this.tabs,{keydown:"_tabKeydown"});
this._on(this.panels,{keydown:"_panelKeydown"});
this._focusable(this.tabs);
this._hoverable(this.tabs)
},_setupHeightStyle:function(G){var I,J,H=this.element.parent();
if(G==="fill"){if(!C.support.minHeight){J=H.css("overflow");
H.css("overflow","hidden")
}I=H.height();
this.element.siblings(":visible").each(function(){var L=C(this),K=L.css("position");
if(K==="absolute"||K==="fixed"){return 
}I-=L.outerHeight(true)
});
if(J){H.css("overflow",J)
}this.element.children().not(this.panels).each(function(){I-=C(this).outerHeight(true)
});
this.panels.each(function(){C(this).height(Math.max(0,I-C(this).innerHeight()+C(this).height()))
}).css("overflow","auto")
}else{if(G==="auto"){I=0;
this.panels.each(function(){I=Math.max(I,C(this).height("").height())
}).height(I)
}}},_eventHandler:function(G){var P=this.options,K=this.active,L=C(G.currentTarget),J=L.closest("li"),N=J[0]===K[0],H=N&&P.collapsible,I=H?C():this._getPanelForTab(J),M=!K.length?C():this._getPanelForTab(K),O={oldTab:K,oldPanel:M,newTab:H?C():J,newPanel:I};
G.preventDefault();
if(J.hasClass("ui-state-disabled")||J.hasClass("ui-tabs-loading")||this.running||(N&&!P.collapsible)||(this._trigger("beforeActivate",G,O)===false)){return 
}P.active=H?false:this.tabs.index(J);
this.active=N?C():J;
if(this.xhr){this.xhr.abort()
}if(!M.length&&!I.length){C.error("jQuery UI Tabs: Mismatching fragment identifier.")
}if(I.length){this.load(this.tabs.index(J),G)
}this._toggle(G,O)
},_toggle:function(M,L){var K=this,G=L.newPanel,J=L.oldPanel;
this.running=true;
function I(){K.running=false;
K._trigger("activate",M,L)
}function H(){L.newTab.closest("li").addClass("ui-tabs-active ui-state-active");
if(G.length&&K.options.show){K._show(G,K.options.show,I)
}else{G.show();
I()
}}if(J.length&&this.options.hide){this._hide(J,this.options.hide,function(){L.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active");
H()
})
}else{L.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active");
J.hide();
H()
}J.attr({"aria-expanded":"false","aria-hidden":"true"});
L.oldTab.attr("aria-selected","false");
if(G.length&&J.length){L.oldTab.attr("tabIndex",-1)
}else{if(G.length){this.tabs.filter(function(){return C(this).attr("tabIndex")===0
}).attr("tabIndex",-1)
}}G.attr({"aria-expanded":"true","aria-hidden":"false"});
L.newTab.attr({"aria-selected":"true",tabIndex:0})
},_activate:function(H){var G,I=this._findActive(H);
if(I[0]===this.active[0]){return 
}if(!I.length){I=this.active
}G=I.find(".ui-tabs-anchor")[0];
this._eventHandler({target:G,currentTarget:G,preventDefault:C.noop})
},_findActive:function(G){return G===false?C():this.tabs.eq(G)
},_getIndex:function(G){if(typeof G==="string"){G=this.anchors.index(this.anchors.filter("[href$='"+G+"']"))
}return G
},_destroy:function(){if(this.xhr){this.xhr.abort()
}this.element.removeClass("ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible");
this.tablist.removeClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").removeAttr("role");
this.anchors.removeClass("ui-tabs-anchor").removeAttr("role").removeAttr("tabIndex").removeData("http://www.usfoods.com/etc/designs/usfoods/clientlibs/href.tabs").removeData("http://www.usfoods.com/etc/designs/usfoods/clientlibs/load.tabs").removeUniqueId();
this.tabs.add(this.panels).each(function(){if(C.data(this,"ui-tabs-destroy")){C(this).remove()
}else{C(this).removeClass("ui-state-default ui-state-active ui-state-disabled ui-corner-top ui-corner-bottom ui-widget-content ui-tabs-active ui-tabs-panel").removeAttr("tabIndex").removeAttr("aria-live").removeAttr("aria-busy").removeAttr("aria-selected").removeAttr("aria-labelledby").removeAttr("aria-hidden").removeAttr("aria-expanded").removeAttr("role")
}});
this.tabs.each(function(){var G=C(this),H=G.data("ui-tabs-aria-controls");
if(H){G.attr("aria-controls",H)
}else{G.removeAttr("aria-controls")
}});
if(this.options.heightStyle!=="content"){this.panels.css("height","")
}},enable:function(G){var H=this.options.disabled;
if(H===false){return 
}if(G===E){H=false
}else{G=this._getIndex(G);
if(C.isArray(H)){H=C.map(H,function(I){return I!==G?I:null
})
}else{H=C.map(this.tabs,function(I,J){return J!==G?J:null
})
}}this._setupDisabled(H)
},disable:function(G){var H=this.options.disabled;
if(H===true){return 
}if(G===E){H=true
}else{G=this._getIndex(G);
if(C.inArray(G,H)!==-1){return 
}if(C.isArray(H)){H=C.merge([G],H).sort()
}else{H=[G]
}}this._setupDisabled(H)
},load:function(I,M){I=this._getIndex(I);
var L=this,J=this.tabs.eq(I),H=J.find(".ui-tabs-anchor"),G=this._getPanelForTab(J),K={tab:J,panel:G};
if(B(H[0])){return 
}this.xhr=C.ajax(this._ajaxSettings(H,M,K));
if(this.xhr&&this.xhr.statusText!=="canceled"){J.addClass("ui-tabs-loading");
G.attr("aria-busy","true");
this.xhr.success(function(N){setTimeout(function(){G.html(N);
L._trigger("load",M,K)
},1)
}).complete(function(O,N){setTimeout(function(){if(N==="abort"){L.panels.stop(false,true)
}J.removeClass("ui-tabs-loading");
G.removeAttr("aria-busy");
if(O===L.xhr){delete L.xhr
}},1)
})
}},_ajaxSettings:function(G,J,I){var H=this;
return{url:G.attr("href"),beforeSend:function(L,K){return H._trigger("beforeLoad",J,C.extend({jqXHR:L,ajaxSettings:K},I))
}}
},_getPanelForTab:function(G){var H=C(G).attr("aria-controls");
return this.element.find(this._sanitizeSelector("#"+H))
}});
if(C.uiBackCompat!==false){C.ui.tabs.prototype._ui=function(H,G){return{tab:H,panel:G,index:this.anchors.index(H)}
};
C.widget("http://www.usfoods.com/etc/designs/usfoods/clientlibs/ui.tabs",C.ui.tabs,{url:function(H,G){this.anchors.eq(H).attr("href",G)
}});
C.widget("http://www.usfoods.com/etc/designs/usfoods/clientlibs/ui.tabs",C.ui.tabs,{options:{ajaxOptions:null,cache:false},_create:function(){this._super();
var G=this;
this._on({tabsbeforeload:function(H,I){if(C.data(I.tab[0],"http://www.usfoods.com/etc/designs/usfoods/clientlibs/cache.tabs")){H.preventDefault();
return 
}I.jqXHR.success(function(){if(G.options.cache){C.data(I.tab[0],"http://www.usfoods.com/etc/designs/usfoods/clientlibs/cache.tabs",true)
}})
}})
},_ajaxSettings:function(H,I,J){var G=this.options.ajaxOptions;
return C.extend({},G,{error:function(M,K){try{G.error(M,K,J.tab.closest("li").index(),J.tab[0])
}catch(L){}}},this._superApply(arguments))
},_setOption:function(G,H){if(G==="cache"&&H===false){this.anchors.removeData("http://www.usfoods.com/etc/designs/usfoods/clientlibs/cache.tabs")
}this._super(G,H)
},_destroy:function(){this.anchors.removeData("http://www.usfoods.com/etc/designs/usfoods/clientlibs/cache.tabs");
this._super()
},url:function(G){this.anchors.eq(G).removeData("http://www.usfoods.com/etc/designs/usfoods/clientlibs/cache.tabs");
this._superApply(arguments)
}});
C.widget("http://www.usfoods.com/etc/designs/usfoods/clientlibs/ui.tabs",C.ui.tabs,{abort:function(){if(this.xhr){this.xhr.abort()
}}});
C.widget("http://www.usfoods.com/etc/designs/usfoods/clientlibs/ui.tabs",C.ui.tabs,{options:{spinner:"<em>Loading&#8230;</em>"},_create:function(){this._super();
this._on({tabsbeforeload:function(I,J){if(I.target!==this.element[0]||!this.options.spinner){return 
}var H=J.tab.find("span"),G=H.html();
H.html(this.options.spinner);
J.jqXHR.complete(function(){H.html(G)
})
}})
}});
C.widget("http://www.usfoods.com/etc/designs/usfoods/clientlibs/ui.tabs",C.ui.tabs,{options:{enable:null,disable:null},enable:function(I){var H=this.options,G;
if(I&&H.disabled===true||(C.isArray(H.disabled)&&C.inArray(I,H.disabled)!==-1)){G=true
}this._superApply(arguments);
if(G){this._trigger("enable",null,this._ui(this.anchors[I],this.panels[I]))
}},disable:function(I){var H=this.options,G;
if(I&&H.disabled===false||(C.isArray(H.disabled)&&C.inArray(I,H.disabled)===-1)){G=true
}this._superApply(arguments);
if(G){this._trigger("disable",null,this._ui(this.anchors[I],this.panels[I]))
}}});
C.widget("http://www.usfoods.com/etc/designs/usfoods/clientlibs/ui.tabs",C.ui.tabs,{options:{add:null,remove:null,tabTemplate:"<li><a href='#{href}'><span>#{label}</span></a></li>"},add:function(L,K,J){if(J===E){J=this.anchors.length
}var M,H,I=this.options,G=C(I.tabTemplate.replace(/#\{href\}/g,L).replace(/#\{label\}/g,K)),N=!L.indexOf("#")?L.replace("#",""):this._tabId(G);
G.addClass("ui-state-default ui-corner-top").data("ui-tabs-destroy",true);
G.attr("aria-controls",N);
M=J>=this.tabs.length;
H=this.element.find("#"+N);
if(!H.length){H=this._createPanel(N);
if(M){if(J>0){H.insertAfter(this.panels.eq(-1))
}else{H.appendTo(this.element)
}}else{H.insertBefore(this.panels[J])
}}H.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").hide();
if(M){G.appendTo(this.tablist)
}else{G.insertBefore(this.tabs[J])
}I.disabled=C.map(I.disabled,function(O){return O>=J?++O:O
});
this.refresh();
if(this.tabs.length===1&&I.active===false){this.option("active",0)
}this._trigger("add",null,this._ui(this.anchors[J],this.panels[J]));
return this
},remove:function(I){I=this._getIndex(I);
var H=this.options,J=this.tabs.eq(I).remove(),G=this._getPanelForTab(J).remove();
if(J.hasClass("ui-tabs-active")&&this.anchors.length>2){this._activate(I+(I+1<this.anchors.length?1:-1))
}H.disabled=C.map(C.grep(H.disabled,function(K){return K!==I
}),function(K){return K>=I?--K:K
});
this.refresh();
this._trigger("remove",null,this._ui(J.find("a")[0],G[0]));
return this
}});
C.widget("http://www.usfoods.com/etc/designs/usfoods/clientlibs/ui.tabs",C.ui.tabs,{length:function(){return this.anchors.length
}});
C.widget("http://www.usfoods.com/etc/designs/usfoods/clientlibs/ui.tabs",C.ui.tabs,{options:{idPrefix:"ui-tabs-"},_tabId:function(H){var G=H.is("li")?H.find("a[href]"):H;
G=G[0];
return C(G).closest("li").attr("aria-controls")||G.title&&G.title.replace(/\s/g,"_").replace(/[^\w\u00c0-\uFFFF\-]/g,"")||this.options.idPrefix+D()
}});
C.widget("http://www.usfoods.com/etc/designs/usfoods/clientlibs/ui.tabs",C.ui.tabs,{options:{panelTemplate:"<div></div>"},_createPanel:function(G){return C(this.options.panelTemplate).attr("id",G).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").data("ui-tabs-destroy",true)
}});
C.widget("http://www.usfoods.com/etc/designs/usfoods/clientlibs/ui.tabs",C.ui.tabs,{_create:function(){var G=this.options;
if(G.active===null&&G.selected!==E){G.active=G.selected===-1?false:G.selected
}this._super();
G.selected=G.active;
if(G.selected===false){G.selected=-1
}},_setOption:function(H,I){if(H!=="selected"){return this._super(H,I)
}var G=this.options;
this._super("active",I===-1?false:I);
G.selected=G.active;
if(G.selected===false){G.selected=-1
}},_eventHandler:function(){this._superApply(arguments);
this.options.selected=this.options.active;
if(this.options.selected===false){this.options.selected=-1
}}});
C.widget("http://www.usfoods.com/etc/designs/usfoods/clientlibs/ui.tabs",C.ui.tabs,{options:{show:null,select:null},_create:function(){this._super();
if(this.options.active!==false){this._trigger("show",null,this._ui(this.active.find(".ui-tabs-anchor")[0],this._getPanelForTab(this.active)[0]))
}},_trigger:function(H,I,J){var G=this._superApply(arguments);
if(!G){return false
}if(H==="beforeActivate"&&J.newTab.length){G=this._super("select",I,{tab:J.newTab.find(".ui-tabs-anchor")[0],panel:J.newPanel[0],index:J.newTab.closest("li").index()})
}else{if(H==="activate"&&J.newTab.length){G=this._super("show",I,{tab:J.newTab.find(".ui-tabs-anchor")[0],panel:J.newPanel[0],index:J.newTab.closest("li").index()})
}}return G
}});
C.widget("http://www.usfoods.com/etc/designs/usfoods/clientlibs/ui.tabs",C.ui.tabs,{select:function(G){G=this._getIndex(G);
if(G===-1){if(this.options.collapsible&&this.options.selected!==-1){G=this.options.selected
}else{return 
}}this.anchors.eq(G).trigger(this.options.event+this.eventNamespace)
}});
(function(){var G=0;
C.widget("http://www.usfoods.com/etc/designs/usfoods/clientlibs/ui.tabs",C.ui.tabs,{options:{cookie:null},_create:function(){var H=this.options,I;
if(H.active==null&&H.cookie){I=parseInt(this._cookie(),10);
if(I===-1){I=false
}H.active=I
}this._super()
},_cookie:function(I){var H=[this.cookie||(this.cookie=this.options.cookie.name||"ui-tabs-"+(++G))];
if(arguments.length){H.push(I===false?-1:I);
H.push(this.options.cookie)
}return C.cookie.apply(null,H)
},_refresh:function(){this._super();
if(this.options.cookie){this._cookie(this.options.active,this.options.cookie)
}},_eventHandler:function(){this._superApply(arguments);
if(this.options.cookie){this._cookie(this.options.active,this.options.cookie)
}},_destroy:function(){this._super();
if(this.options.cookie){this._cookie(null,this.options.cookie)
}}})
})();
C.widget("http://www.usfoods.com/etc/designs/usfoods/clientlibs/ui.tabs",C.ui.tabs,{_trigger:function(H,I,J){var G=C.extend({},J);
if(H==="load"){G.panel=G.panel[0];
G.tab=G.tab.find(".ui-tabs-anchor")[0]
}return this._super(H,I,G)
}});
C.widget("http://www.usfoods.com/etc/designs/usfoods/clientlibs/ui.tabs",C.ui.tabs,{options:{fx:null},_getFx:function(){var H,G,I=this.options.fx;
if(I){if(C.isArray(I)){H=I[0];
G=I[1]
}else{H=G=I
}}return I?{show:G,hide:H}:null
},_toggle:function(N,M){var L=this,G=M.newPanel,J=M.oldPanel,K=this._getFx();
if(!K){return this._super(N,M)
}L.running=true;
function I(){L.running=false;
L._trigger("activate",N,M)
}function H(){M.newTab.closest("li").addClass("ui-tabs-active ui-state-active");
if(G.length&&K.show){G.animate(K.show,K.show.duration,function(){I()
})
}else{G.show();
I()
}}if(J.length&&K.hide){J.animate(K.hide,K.hide.duration,function(){M.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active");
H()
})
}else{M.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active");
J.hide();
H()
}}})
}})(jQuery);
(function(D){var B=0;
function C(F,G){var E=(F.attr("aria-describedby")||"").split(/\s+/);
E.push(G);
F.data("ui-tooltip-id",G).attr("aria-describedby",D.trim(E.join(" ")))
}function A(G){var H=G.data("ui-tooltip-id"),F=(G.attr("aria-describedby")||"").split(/\s+/),E=D.inArray(H,F);
if(E!==-1){F.splice(E,1)
}G.removeData("ui-tooltip-id");
F=D.trim(F.join(" "));
if(F){G.attr("aria-describedby",F)
}else{G.removeAttr("aria-describedby")
}}D.widget("ui.tooltip",{version:"1.9.1",options:{content:function(){return D(this).attr("title")
},hide:true,items:"[title]:not([disabled])",position:{my:"left top+15",at:"left bottom",collision:"flipfit flipfit"},show:true,tooltipClass:null,track:false,close:null,open:null},_create:function(){this._on({mouseover:"open",focusin:"open"});
this.tooltips={};
this.parents={};
if(this.options.disabled){this._disable()
}},_setOption:function(E,G){var F=this;
if(E==="disabled"){this[G?"_disable":"_enable"]();
this.options[E]=G;
return 
}this._super(E,G);
if(E==="content"){D.each(this.tooltips,function(I,H){F._updateContent(H)
})
}},_disable:function(){var E=this;
D.each(this.tooltips,function(H,F){var G=D.Event("blur");
G.target=G.currentTarget=F[0];
E.close(G,true)
});
this.element.find(this.options.items).andSelf().each(function(){var F=D(this);
if(F.is("[title]")){F.data("ui-tooltip-title",F.attr("title")).attr("title","")
}})
},_enable:function(){this.element.find(this.options.items).andSelf().each(function(){var E=D(this);
if(E.data("ui-tooltip-title")){E.attr("title",E.data("ui-tooltip-title"))
}})
},open:function(F){var E=this,G=D(F?F.target:this.element).closest(this.options.items);
if(!G.length){return 
}if(this.options.track&&G.data("ui-tooltip-id")){this._find(G).position(D.extend({of:G},this.options.position));
this._off(this.document,"mousemove");
return 
}if(G.attr("title")){G.data("ui-tooltip-title",G.attr("title"))
}G.data("tooltip-open",true);
if(F&&F.type==="mouseover"){G.parents().each(function(){var H;
if(D(this).data("tooltip-open")){H=D.Event("blur");
H.target=H.currentTarget=this;
E.close(H,true)
}if(this.title){D(this).uniqueId();
E.parents[this.id]={element:this,title:this.title};
this.title=""
}})
}this._updateContent(G,F)
},_updateContent:function(I,H){var G,E=this.options.content,F=this;
if(typeof E==="string"){return this._open(H,I,E)
}G=E.call(I[0],function(J){if(!I.data("tooltip-open")){return 
}F._delay(function(){this._open(H,I,J)
})
});
if(G){this._open(H,I,G)
}},_open:function(I,K,H){var J,G,F,L=D.extend({},this.options.position);
if(!H){return 
}J=this._find(K);
if(J.length){J.find(".ui-tooltip-content").html(H);
return 
}if(K.is("[title]")){if(I&&I.type==="mouseover"){K.attr("title","")
}else{K.removeAttr("title")
}}J=this._tooltip(K);
C(K,J.attr("id"));
J.find(".ui-tooltip-content").html(H);
function E(M){L.of=M;
if(J.is(":hidden")){return 
}J.position(L)
}if(this.options.track&&I&&/^mouse/.test(I.originalEvent.type)){this._on(this.document,{mousemove:E});
E(I)
}else{J.position(D.extend({of:K},this.options.position))
}J.hide();
this._show(J,this.options.show);
if(this.options.show&&this.options.show.delay){F=setInterval(function(){if(J.is(":visible")){E(L.of);
clearInterval(F)
}},D.fx.interval)
}this._trigger("open",I,{tooltip:J});
G={keyup:function(M){if(M.keyCode===D.ui.keyCode.ESCAPE){var N=D.Event(M);
N.currentTarget=K[0];
this.close(N,true)
}},remove:function(){this._removeTooltip(J)
}};
if(!I||I.type==="mouseover"){G.mouseleave="close"
}if(!I||I.type==="focusin"){G.focusout="close"
}this._on(K,G)
},close:function(F){var E=this,H=D(F?F.currentTarget:this.element),G=this._find(H);
if(this.closing){return 
}if(H.data("ui-tooltip-title")){H.attr("title",H.data("ui-tooltip-title"))
}A(H);
G.stop(true);
this._hide(G,this.options.hide,function(){E._removeTooltip(D(this))
});
H.removeData("tooltip-open");
this._off(H,"mouseleave focusout keyup");
if(H[0]!==this.element[0]){this._off(H,"remove")
}this._off(this.document,"mousemove");
if(F&&F.type==="mouseleave"){D.each(this.parents,function(J,I){I.element.title=I.title;
delete E.parents[J]
})
}this.closing=true;
this._trigger("close",F,{tooltip:G});
this.closing=false
},_tooltip:function(E){var G="ui-tooltip-"+B++,F=D("<div>").attr({id:G,role:"tooltip"}).addClass("ui-tooltip ui-widget ui-corner-all ui-widget-content "+(this.options.tooltipClass||""));
D("<div>").addClass("ui-tooltip-content").appendTo(F);
F.appendTo(this.document[0].body);
if(D.fn.bgiframe){F.bgiframe()
}this.tooltips[G]=E;
return F
},_find:function(E){var F=E.data("ui-tooltip-id");
return F?D("#"+F):D()
},_removeTooltip:function(E){E.remove();
delete this.tooltips[E.attr("id")]
},_destroy:function(){var E=this;
D.each(this.tooltips,function(H,F){var G=D.Event("blur");
G.target=G.currentTarget=F[0];
E.close(G,true);
D("#"+H).remove();
if(F.data("ui-tooltip-title")){F.attr("title",F.data("ui-tooltip-title"));
F.removeData("ui-tooltip-title")
}})
}})
}(jQuery));
(jQuery.effects||(function($,undefined){var backCompat=$.uiBackCompat!==false,dataSpace="ui-effects-";
$.effects={effect:{}};
/*
 * jQuery Color Animations v2.0.0
 * http://jquery.com/
 *
 * Copyright 2012 jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * Date: Mon Aug 13 13:41:02 2012 -0500
 */
(function(jQuery,undefined){var stepHooks="backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor".split(" "),rplusequals=/^([\-+])=\s*(\d+\.?\d*)/,stringParsers=[{re:/rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/,parse:function(execResult){return[execResult[1],execResult[2],execResult[3],execResult[4]]
}},{re:/rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/,parse:function(execResult){return[execResult[1]*2.55,execResult[2]*2.55,execResult[3]*2.55,execResult[4]]
}},{re:/#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,parse:function(execResult){return[parseInt(execResult[1],16),parseInt(execResult[2],16),parseInt(execResult[3],16)]
}},{re:/#([a-f0-9])([a-f0-9])([a-f0-9])/,parse:function(execResult){return[parseInt(execResult[1]+execResult[1],16),parseInt(execResult[2]+execResult[2],16),parseInt(execResult[3]+execResult[3],16)]
}},{re:/hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/,space:"hsla",parse:function(execResult){return[execResult[1],execResult[2]/100,execResult[3]/100,execResult[4]]
}}],color=jQuery.Color=function(color,green,blue,alpha){return new jQuery.Color.fn.parse(color,green,blue,alpha)
},spaces={rgba:{props:{red:{idx:0,type:"byte"},green:{idx:1,type:"byte"},blue:{idx:2,type:"byte"}}},hsla:{props:{hue:{idx:0,type:"degrees"},saturation:{idx:1,type:"percent"},lightness:{idx:2,type:"percent"}}}},propTypes={"byte":{floor:true,max:255},percent:{max:1},degrees:{mod:360,floor:true}},support=color.support={},supportElem=jQuery("<p>")[0],colors,each=jQuery.each;
supportElem.style.cssText="background-color:rgba(1,1,1,.5)";
support.rgba=supportElem.style.backgroundColor.indexOf("rgba")>-1;
each(spaces,function(spaceName,space){space.cache="_"+spaceName;
space.props.alpha={idx:3,type:"percent",def:1}
});
function clamp(value,prop,allowEmpty){var type=propTypes[prop.type]||{};
if(value==null){return(allowEmpty||!prop.def)?null:prop.def
}value=type.floor?~~value:parseFloat(value);
if(isNaN(value)){return prop.def
}if(type.mod){return(value+type.mod)%type.mod
}return 0>value?0:type.max<value?type.max:value
}function stringParse(string){var inst=color(),rgba=inst._rgba=[];
string=string.toLowerCase();
each(stringParsers,function(i,parser){var parsed,match=parser.re.exec(string),values=match&&parser.parse(match),spaceName=parser.space||"rgba";
if(values){parsed=inst[spaceName](values);
inst[spaces[spaceName].cache]=parsed[spaces[spaceName].cache];
rgba=inst._rgba=parsed._rgba;
return false
}});
if(rgba.length){if(rgba.join()==="0,0,0,0"){jQuery.extend(rgba,colors.transparent)
}return inst
}return colors[string]
}color.fn=jQuery.extend(color.prototype,{parse:function(red,green,blue,alpha){if(red===undefined){this._rgba=[null,null,null,null];
return this
}if(red.jquery||red.nodeType){red=jQuery(red).css(green);
green=undefined
}var inst=this,type=jQuery.type(red),rgba=this._rgba=[];
if(green!==undefined){red=[red,green,blue,alpha];
type="array"
}if(type==="string"){return this.parse(stringParse(red)||colors._default)
}if(type==="array"){each(spaces.rgba.props,function(key,prop){rgba[prop.idx]=clamp(red[prop.idx],prop)
});
return this
}if(type==="object"){if(red instanceof color){each(spaces,function(spaceName,space){if(red[space.cache]){inst[space.cache]=red[space.cache].slice()
}})
}else{each(spaces,function(spaceName,space){var cache=space.cache;
each(space.props,function(key,prop){if(!inst[cache]&&space.to){if(key==="alpha"||red[key]==null){return 
}inst[cache]=space.to(inst._rgba)
}inst[cache][prop.idx]=clamp(red[key],prop,true)
});
if(inst[cache]&&$.inArray(null,inst[cache].slice(0,3))<0){inst[cache][3]=1;
if(space.from){inst._rgba=space.from(inst[cache])
}}})
}return this
}},is:function(compare){var is=color(compare),same=true,inst=this;
each(spaces,function(_,space){var localCache,isCache=is[space.cache];
if(isCache){localCache=inst[space.cache]||space.to&&space.to(inst._rgba)||[];
each(space.props,function(_,prop){if(isCache[prop.idx]!=null){same=(isCache[prop.idx]===localCache[prop.idx]);
return same
}})
}return same
});
return same
},_space:function(){var used=[],inst=this;
each(spaces,function(spaceName,space){if(inst[space.cache]){used.push(spaceName)
}});
return used.pop()
},transition:function(other,distance){var end=color(other),spaceName=end._space(),space=spaces[spaceName],startColor=this.alpha()===0?color("transparent"):this,start=startColor[space.cache]||space.to(startColor._rgba),result=start.slice();
end=end[space.cache];
each(space.props,function(key,prop){var index=prop.idx,startValue=start[index],endValue=end[index],type=propTypes[prop.type]||{};
if(endValue===null){return 
}if(startValue===null){result[index]=endValue
}else{if(type.mod){if(endValue-startValue>type.mod/2){startValue+=type.mod
}else{if(startValue-endValue>type.mod/2){startValue-=type.mod
}}}result[index]=clamp((endValue-startValue)*distance+startValue,prop)
}});
return this[spaceName](result)
},blend:function(opaque){if(this._rgba[3]===1){return this
}var rgb=this._rgba.slice(),a=rgb.pop(),blend=color(opaque)._rgba;
return color(jQuery.map(rgb,function(v,i){return(1-a)*blend[i]+a*v
}))
},toRgbaString:function(){var prefix="rgba(",rgba=jQuery.map(this._rgba,function(v,i){return v==null?(i>2?1:0):v
});
if(rgba[3]===1){rgba.pop();
prefix="rgb("
}return prefix+rgba.join()+")"
},toHslaString:function(){var prefix="hsla(",hsla=jQuery.map(this.hsla(),function(v,i){if(v==null){v=i>2?1:0
}if(i&&i<3){v=Math.round(v*100)+"%"
}return v
});
if(hsla[3]===1){hsla.pop();
prefix="hsl("
}return prefix+hsla.join()+")"
},toHexString:function(includeAlpha){var rgba=this._rgba.slice(),alpha=rgba.pop();
if(includeAlpha){rgba.push(~~(alpha*255))
}return"#"+jQuery.map(rgba,function(v){v=(v||0).toString(16);
return v.length===1?"0"+v:v
}).join("")
},toString:function(){return this._rgba[3]===0?"transparent":this.toRgbaString()
}});
color.fn.parse.prototype=color.fn;
function hue2rgb(p,q,h){h=(h+1)%1;
if(h*6<1){return p+(q-p)*h*6
}if(h*2<1){return q
}if(h*3<2){return p+(q-p)*((2/3)-h)*6
}return p
}spaces.hsla.to=function(rgba){if(rgba[0]==null||rgba[1]==null||rgba[2]==null){return[null,null,null,rgba[3]]
}var r=rgba[0]/255,g=rgba[1]/255,b=rgba[2]/255,a=rgba[3],max=Math.max(r,g,b),min=Math.min(r,g,b),diff=max-min,add=max+min,l=add*0.5,h,s;
if(min===max){h=0
}else{if(r===max){h=(60*(g-b)/diff)+360
}else{if(g===max){h=(60*(b-r)/diff)+120
}else{h=(60*(r-g)/diff)+240
}}}if(l===0||l===1){s=l
}else{if(l<=0.5){s=diff/add
}else{s=diff/(2-add)
}}return[Math.round(h)%360,s,l,a==null?1:a]
};
spaces.hsla.from=function(hsla){if(hsla[0]==null||hsla[1]==null||hsla[2]==null){return[null,null,null,hsla[3]]
}var h=hsla[0]/360,s=hsla[1],l=hsla[2],a=hsla[3],q=l<=0.5?l*(1+s):l+s-l*s,p=2*l-q;
return[Math.round(hue2rgb(p,q,h+(1/3))*255),Math.round(hue2rgb(p,q,h)*255),Math.round(hue2rgb(p,q,h-(1/3))*255),a]
};
each(spaces,function(spaceName,space){var props=space.props,cache=space.cache,to=space.to,from=space.from;
color.fn[spaceName]=function(value){if(to&&!this[cache]){this[cache]=to(this._rgba)
}if(value===undefined){return this[cache].slice()
}var ret,type=jQuery.type(value),arr=(type==="array"||type==="object")?value:arguments,local=this[cache].slice();
each(props,function(key,prop){var val=arr[type==="object"?key:prop.idx];
if(val==null){val=local[prop.idx]
}local[prop.idx]=clamp(val,prop)
});
if(from){ret=color(from(local));
ret[cache]=local;
return ret
}else{return color(local)
}};
each(props,function(key,prop){if(color.fn[key]){return 
}color.fn[key]=function(value){var vtype=jQuery.type(value),fn=(key==="alpha"?(this._hsla?"hsla":"rgba"):spaceName),local=this[fn](),cur=local[prop.idx],match;
if(vtype==="undefined"){return cur
}if(vtype==="function"){value=value.call(this,cur);
vtype=jQuery.type(value)
}if(value==null&&prop.empty){return this
}if(vtype==="string"){match=rplusequals.exec(value);
if(match){value=cur+parseFloat(match[2])*(match[1]==="+"?1:-1)
}}local[prop.idx]=value;
return this[fn](local)
}
})
});
each(stepHooks,function(i,hook){jQuery.cssHooks[hook]={set:function(elem,value){var parsed,curElem,backgroundColor="";
if(jQuery.type(value)!=="string"||(parsed=stringParse(value))){value=color(parsed||value);
if(!support.rgba&&value._rgba[3]!==1){curElem=hook==="backgroundColor"?elem.parentNode:elem;
while((backgroundColor===""||backgroundColor==="transparent")&&curElem&&curElem.style){try{backgroundColor=jQuery.css(curElem,"backgroundColor");
curElem=curElem.parentNode
}catch(e){}}value=value.blend(backgroundColor&&backgroundColor!=="transparent"?backgroundColor:"_default")
}value=value.toRgbaString()
}try{elem.style[hook]=value
}catch(error){}}};
jQuery.fx.step[hook]=function(fx){if(!fx.colorInit){fx.start=color(fx.elem,hook);
fx.end=color(fx.end);
fx.colorInit=true
}jQuery.cssHooks[hook].set(fx.elem,fx.start.transition(fx.end,fx.pos))
}
});
jQuery.cssHooks.borderColor={expand:function(value){var expanded={};
each(["Top","Right","Bottom","Left"],function(i,part){expanded["border"+part+"Color"]=value
});
return expanded
}};
colors=jQuery.Color.names={aqua:"#00ffff",black:"#000000",blue:"#0000ff",fuchsia:"#ff00ff",gray:"#808080",green:"#008000",lime:"#00ff00",maroon:"#800000",navy:"#000080",olive:"#808000",purple:"#800080",red:"#ff0000",silver:"#c0c0c0",teal:"#008080",white:"#ffffff",yellow:"#ffff00",transparent:[null,null,null,0],_default:"#ffffff"}
})(jQuery);
(function(){var classAnimationActions=["add","remove","toggle"],shorthandStyles={border:1,borderBottom:1,borderColor:1,borderLeft:1,borderRight:1,borderTop:1,borderWidth:1,margin:1,padding:1};
$.each(["borderLeftStyle","borderRightStyle","borderBottomStyle","borderTopStyle"],function(_,prop){$.fx.step[prop]=function(fx){if(fx.end!=="none"&&!fx.setAttr||fx.pos===1&&!fx.setAttr){jQuery.style(fx.elem,prop,fx.end);
fx.setAttr=true
}}
});
function getElementStyles(){var style=this.ownerDocument.defaultView?this.ownerDocument.defaultView.getComputedStyle(this,null):this.currentStyle,newStyle={},key,len;
if(style&&style.length&&style[0]&&style[style[0]]){len=style.length;
while(len--){key=style[len];
if(typeof style[key]==="string"){newStyle[$.camelCase(key)]=style[key]
}}}else{for(key in style){if(typeof style[key]==="string"){newStyle[key]=style[key]
}}}return newStyle
}function styleDifference(oldStyle,newStyle){var diff={},name,value;
for(name in newStyle){value=newStyle[name];
if(oldStyle[name]!==value){if(!shorthandStyles[name]){if($.fx.step[name]||!isNaN(parseFloat(value))){diff[name]=value
}}}}return diff
}$.effects.animateClass=function(value,duration,easing,callback){var o=$.speed(duration,easing,callback);
return this.queue(function(){var animated=$(this),baseClass=animated.attr("class")||"",applyClassChange,allAnimations=o.children?animated.find("*").andSelf():animated;
allAnimations=allAnimations.map(function(){var el=$(this);
return{el:el,start:getElementStyles.call(this)}
});
applyClassChange=function(){$.each(classAnimationActions,function(i,action){if(value[action]){animated[action+"Class"](value[action])
}})
};
applyClassChange();
allAnimations=allAnimations.map(function(){this.end=getElementStyles.call(this.el[0]);
this.diff=styleDifference(this.start,this.end);
return this
});
animated.attr("class",baseClass);
allAnimations=allAnimations.map(function(){var styleInfo=this,dfd=$.Deferred(),opts=jQuery.extend({},o,{queue:false,complete:function(){dfd.resolve(styleInfo)
}});
this.el.animate(this.diff,opts);
return dfd.promise()
});
$.when.apply($,allAnimations.get()).done(function(){applyClassChange();
$.each(arguments,function(){var el=this.el;
$.each(this.diff,function(key){el.css(key,"")
})
});
o.complete.call(animated[0])
})
})
};
$.fn.extend({_addClass:$.fn.addClass,addClass:function(classNames,speed,easing,callback){return speed?$.effects.animateClass.call(this,{add:classNames},speed,easing,callback):this._addClass(classNames)
},_removeClass:$.fn.removeClass,removeClass:function(classNames,speed,easing,callback){return speed?$.effects.animateClass.call(this,{remove:classNames},speed,easing,callback):this._removeClass(classNames)
},_toggleClass:$.fn.toggleClass,toggleClass:function(classNames,force,speed,easing,callback){if(typeof force==="boolean"||force===undefined){if(!speed){return this._toggleClass(classNames,force)
}else{return $.effects.animateClass.call(this,(force?{add:classNames}:{remove:classNames}),speed,easing,callback)
}}else{return $.effects.animateClass.call(this,{toggle:classNames},force,speed,easing)
}},switchClass:function(remove,add,speed,easing,callback){return $.effects.animateClass.call(this,{add:add,remove:remove},speed,easing,callback)
}})
})();
(function(){$.extend($.effects,{version:"1.9.1",save:function(element,set){for(var i=0;
i<set.length;
i++){if(set[i]!==null){element.data(dataSpace+set[i],element[0].style[set[i]])
}}},restore:function(element,set){var val,i;
for(i=0;
i<set.length;
i++){if(set[i]!==null){val=element.data(dataSpace+set[i]);
if(val===undefined){val=""
}element.css(set[i],val)
}}},setMode:function(el,mode){if(mode==="toggle"){mode=el.is(":hidden")?"show":"hide"
}return mode
},getBaseline:function(origin,original){var y,x;
switch(origin[0]){case"top":y=0;
break;
case"middle":y=0.5;
break;
case"bottom":y=1;
break;
default:y=origin[0]/original.height
}switch(origin[1]){case"left":x=0;
break;
case"center":x=0.5;
break;
case"right":x=1;
break;
default:x=origin[1]/original.width
}return{x:x,y:y}
},createWrapper:function(element){if(element.parent().is(".ui-effects-wrapper")){return element.parent()
}var props={width:element.outerWidth(true),height:element.outerHeight(true),"float":element.css("float")},wrapper=$("<div></div>").addClass("ui-effects-wrapper").css({fontSize:"100%",background:"transparent",border:"none",margin:0,padding:0}),size={width:element.width(),height:element.height()},active=document.activeElement;
try{active.id
}catch(e){active=document.body
}element.wrap(wrapper);
if(element[0]===active||$.contains(element[0],active)){$(active).focus()
}wrapper=element.parent();
if(element.css("position")==="static"){wrapper.css({position:"relative"});
element.css({position:"relative"})
}else{$.extend(props,{position:element.css("position"),zIndex:element.css("z-index")});
$.each(["top","left","bottom","right"],function(i,pos){props[pos]=element.css(pos);
if(isNaN(parseInt(props[pos],10))){props[pos]="auto"
}});
element.css({position:"relative",top:0,left:0,right:"auto",bottom:"auto"})
}element.css(size);
return wrapper.css(props).show()
},removeWrapper:function(element){var active=document.activeElement;
if(element.parent().is(".ui-effects-wrapper")){element.parent().replaceWith(element);
if(element[0]===active||$.contains(element[0],active)){$(active).focus()
}}return element
},setTransition:function(element,list,factor,value){value=value||{};
$.each(list,function(i,x){var unit=element.cssUnit(x);
if(unit[0]>0){value[x]=unit[0]*factor+unit[1]
}});
return value
}});
function _normalizeArguments(effect,options,speed,callback){if($.isPlainObject(effect)){options=effect;
effect=effect.effect
}effect={effect:effect};
if(options==null){options={}
}if($.isFunction(options)){callback=options;
speed=null;
options={}
}if(typeof options==="number"||$.fx.speeds[options]){callback=speed;
speed=options;
options={}
}if($.isFunction(speed)){callback=speed;
speed=null
}if(options){$.extend(effect,options)
}speed=speed||options.duration;
effect.duration=$.fx.off?0:typeof speed==="number"?speed:speed in $.fx.speeds?$.fx.speeds[speed]:$.fx.speeds._default;
effect.complete=callback||options.complete;
return effect
}function standardSpeed(speed){if(!speed||typeof speed==="number"||$.fx.speeds[speed]){return true
}if(typeof speed==="string"&&!$.effects.effect[speed]){if(backCompat&&$.effects[speed]){return false
}return true
}return false
}$.fn.extend({effect:function(){var args=_normalizeArguments.apply(this,arguments),mode=args.mode,queue=args.queue,effectMethod=$.effects.effect[args.effect],oldEffectMethod=!effectMethod&&backCompat&&$.effects[args.effect];
if($.fx.off||!(effectMethod||oldEffectMethod)){if(mode){return this[mode](args.duration,args.complete)
}else{return this.each(function(){if(args.complete){args.complete.call(this)
}})
}}function run(next){var elem=$(this),complete=args.complete,mode=args.mode;
function done(){if($.isFunction(complete)){complete.call(elem[0])
}if($.isFunction(next)){next()
}}if(elem.is(":hidden")?mode==="hide":mode==="show"){done()
}else{effectMethod.call(elem[0],args,done)
}}if(effectMethod){return queue===false?this.each(run):this.queue(queue||"fx",run)
}else{return oldEffectMethod.call(this,{options:args,duration:args.duration,callback:args.complete,mode:args.mode})
}},_show:$.fn.show,show:function(speed){if(standardSpeed(speed)){return this._show.apply(this,arguments)
}else{var args=_normalizeArguments.apply(this,arguments);
args.mode="show";
return this.effect.call(this,args)
}},_hide:$.fn.hide,hide:function(speed){if(standardSpeed(speed)){return this._hide.apply(this,arguments)
}else{var args=_normalizeArguments.apply(this,arguments);
args.mode="hide";
return this.effect.call(this,args)
}},__toggle:$.fn.toggle,toggle:function(speed){if(standardSpeed(speed)||typeof speed==="boolean"||$.isFunction(speed)){return this.__toggle.apply(this,arguments)
}else{var args=_normalizeArguments.apply(this,arguments);
args.mode="toggle";
return this.effect.call(this,args)
}},cssUnit:function(key){var style=this.css(key),val=[];
$.each(["em","px","%","pt"],function(i,unit){if(style.indexOf(unit)>0){val=[parseFloat(style),unit]
}});
return val
}})
})();
(function(){var baseEasings={};
$.each(["Quad","Cubic","Quart","Quint","Expo"],function(i,name){baseEasings[name]=function(p){return Math.pow(p,i+2)
}
});
$.extend(baseEasings,{Sine:function(p){return 1-Math.cos(p*Math.PI/2)
},Circ:function(p){return 1-Math.sqrt(1-p*p)
},Elastic:function(p){return p===0||p===1?p:-Math.pow(2,8*(p-1))*Math.sin(((p-1)*80-7.5)*Math.PI/15)
},Back:function(p){return p*p*(3*p-2)
},Bounce:function(p){var pow2,bounce=4;
while(p<((pow2=Math.pow(2,--bounce))-1)/11){}return 1/Math.pow(4,3-bounce)-7.5625*Math.pow((pow2*3-2)/22-p,2)
}});
$.each(baseEasings,function(name,easeIn){$.easing["easeIn"+name]=easeIn;
$.easing["easeOut"+name]=function(p){return 1-easeIn(1-p)
};
$.easing["easeInOut"+name]=function(p){return p<0.5?easeIn(p*2)/2:1-easeIn(p*-2+2)/2
}
})
})()
})(jQuery));
(function(B,D){var A=/up|down|vertical/,C=/up|left|vertical|horizontal/;
B.effects.effect.blind=function(G,M){var H=B(this),P=["position","top","bottom","left","right","height","width"],N=B.effects.setMode(H,G.mode||"hide"),Q=G.direction||"up",J=A.test(Q),I=J?"height":"width",O=J?"top":"left",S=C.test(Q),L={},R=N==="show",F,E,K;
if(H.parent().is(".ui-effects-wrapper")){B.effects.save(H.parent(),P)
}else{B.effects.save(H,P)
}H.show();
F=B.effects.createWrapper(H).css({overflow:"hidden"});
E=F[I]();
K=parseFloat(F.css(O))||0;
L[I]=R?E:0;
if(!S){H.css(J?"bottom":"right",0).css(J?"top":"left","auto").css({position:"absolute"});
L[O]=R?K:E+K
}if(R){F.css(I,0);
if(!S){F.css(O,K+E)
}}F.animate(L,{duration:G.duration,easing:G.easing,queue:false,complete:function(){if(N==="hide"){H.hide()
}B.effects.restore(H,P);
B.effects.removeWrapper(H);
M()
}})
}
})(jQuery);
(function(A,B){A.effects.effect.bounce=function(L,K){var C=A(this),D=["position","top","bottom","left","right","height","width"],J=A.effects.setMode(C,L.mode||"effect"),I=J==="hide",T=J==="show",U=L.direction||"up",E=L.distance,H=L.times||5,V=H*2+(T||I?1:0),S=L.duration/V,N=L.easing,F=(U==="up"||U==="down")?"top":"left",M=(U==="up"||U==="left"),R,G,Q,O=C.queue(),P=O.length;
if(T||I){D.push("opacity")
}A.effects.save(C,D);
C.show();
A.effects.createWrapper(C);
if(!E){E=C[F==="top"?"outerHeight":"outerWidth"]()/3
}if(T){Q={opacity:1};
Q[F]=0;
C.css("opacity",0).css(F,M?-E*2:E*2).animate(Q,S,N)
}if(I){E=E/Math.pow(2,H-1)
}Q={};
Q[F]=0;
for(R=0;
R<H;
R++){G={};
G[F]=(M?"-=":"+=")+E;
C.animate(G,S,N).animate(Q,S,N);
E=I?E*2:E/2
}if(I){G={opacity:0};
G[F]=(M?"-=":"+=")+E;
C.animate(G,S,N)
}C.queue(function(){if(I){C.hide()
}A.effects.restore(C,D);
A.effects.removeWrapper(C);
K()
});
if(P>1){O.splice.apply(O,[1,0].concat(O.splice(P,V+1)))
}C.dequeue()
}
})(jQuery);
(function(A,B){A.effects.effect.clip=function(F,I){var G=A(this),M=["position","top","bottom","left","right","height","width"],L=A.effects.setMode(G,F.mode||"hide"),O=L==="show",N=F.direction||"vertical",K=N==="vertical",P=K?"height":"width",J=K?"top":"left",H={},D,E,C;
A.effects.save(G,M);
G.show();
D=A.effects.createWrapper(G).css({overflow:"hidden"});
E=(G[0].tagName==="IMG")?D:G;
C=E[P]();
if(O){E.css(P,0);
E.css(J,C/2)
}H[P]=O?C:0;
H[J]=O?0:C/2;
E.animate(H,{queue:false,duration:F.duration,easing:F.easing,complete:function(){if(!O){G.hide()
}A.effects.restore(G,M);
A.effects.removeWrapper(G);
I()
}})
}
})(jQuery);
(function(A,B){A.effects.effect.drop=function(D,H){var E=A(this),J=["position","top","bottom","left","right","opacity","height","width"],I=A.effects.setMode(E,D.mode||"hide"),L=I==="show",K=D.direction||"left",F=(K==="up"||K==="down")?"top":"left",M=(K==="up"||K==="left")?"pos":"neg",G={opacity:L?1:0},C;
A.effects.save(E,J);
E.show();
A.effects.createWrapper(E);
C=D.distance||E[F==="top"?"outerHeight":"outerWidth"](true)/2;
if(L){E.css("opacity",0).css(F,M==="pos"?-C:C)
}G[F]=(L?(M==="pos"?"+=":"-="):(M==="pos"?"-=":"+="))+C;
E.animate(G,{queue:false,duration:D.duration,easing:D.easing,complete:function(){if(I==="hide"){E.hide()
}A.effects.restore(E,J);
A.effects.removeWrapper(E);
H()
}})
}
})(jQuery);
(function(A,B){A.effects.effect.explode=function(P,O){var I=P.pieces?Math.round(Math.sqrt(P.pieces)):3,D=I,C=A(this),K=A.effects.setMode(C,P.mode||"hide"),T=K==="show",G=C.show().css("visibility","hidden").offset(),Q=Math.ceil(C.outerWidth()/D),N=Math.ceil(C.outerHeight()/I),H=[],S,R,E,M,L,J;
function U(){H.push(this);
if(H.length===I*D){F()
}}for(S=0;
S<I;
S++){M=G.top+S*N;
J=S-(I-1)/2;
for(R=0;
R<D;
R++){E=G.left+R*Q;
L=R-(D-1)/2;
C.clone().appendTo("body").wrap("<div></div>").css({position:"absolute",visibility:"visible",left:-R*Q,top:-S*N}).parent().addClass("ui-effects-explode").css({position:"absolute",overflow:"hidden",width:Q,height:N,left:E+(T?L*Q:0),top:M+(T?J*N:0),opacity:T?0:1}).animate({left:E+(T?0:L*Q),top:M+(T?0:J*N),opacity:T?1:0},P.duration||500,P.easing,U)
}}function F(){C.css({visibility:"visible"});
A(H).remove();
if(!T){C.hide()
}O()
}}
})(jQuery);
(function(A,B){A.effects.effect.fade=function(F,C){var D=A(this),E=A.effects.setMode(D,F.mode||"toggle");
D.animate({opacity:E},{queue:false,duration:F.duration,easing:F.easing,complete:C})
}
})(jQuery);
(function(A,B){A.effects.effect.fold=function(E,I){var F=A(this),N=["position","top","bottom","left","right","height","width"],K=A.effects.setMode(F,E.mode||"hide"),Q=K==="show",L=K==="hide",S=E.size||15,M=/([0-9]+)%/.exec(S),R=!!E.horizFirst,J=Q!==R,G=J?["width","height"]:["height","width"],H=E.duration/2,D,C,P={},O={};
A.effects.save(F,N);
F.show();
D=A.effects.createWrapper(F).css({overflow:"hidden"});
C=J?[D.width(),D.height()]:[D.height(),D.width()];
if(M){S=parseInt(M[1],10)/100*C[L?0:1]
}if(Q){D.css(R?{height:0,width:S}:{height:S,width:0})
}P[G[0]]=Q?C[0]:S;
O[G[1]]=Q?C[1]:0;
D.animate(P,H,E.easing).animate(O,H,E.easing,function(){if(L){F.hide()
}A.effects.restore(F,N);
A.effects.removeWrapper(F);
I()
})
}
})(jQuery);
(function(A,B){A.effects.effect.highlight=function(H,C){var E=A(this),D=["backgroundImage","backgroundColor","opacity"],G=A.effects.setMode(E,H.mode||"show"),F={backgroundColor:E.css("backgroundColor")};
if(G==="hide"){F.opacity=0
}A.effects.save(E,D);
E.show().css({backgroundImage:"none",backgroundColor:H.color||"#ffff99"}).animate(F,{queue:false,duration:H.duration,easing:H.easing,complete:function(){if(G==="hide"){E.hide()
}A.effects.restore(E,D);
C()
}})
}
})(jQuery);
(function(A,B){A.effects.effect.pulsate=function(C,G){var E=A(this),J=A.effects.setMode(E,C.mode||"show"),N=J==="show",K=J==="hide",O=(N||J==="hide"),L=((C.times||5)*2)+(O?1:0),F=C.duration/L,M=0,I=E.queue(),D=I.length,H;
if(N||!E.is(":visible")){E.css("opacity",0).show();
M=1
}for(H=1;
H<L;
H++){E.animate({opacity:M},F,C.easing);
M=1-M
}E.animate({opacity:M},F,C.easing);
E.queue(function(){if(K){E.hide()
}G()
});
if(D>1){I.splice.apply(I,[1,0].concat(I.splice(D,L+1)))
}E.dequeue()
}
})(jQuery);
(function(A,B){A.effects.effect.puff=function(J,C){var H=A(this),I=A.effects.setMode(H,J.mode||"hide"),F=I==="hide",G=parseInt(J.percent,10)||150,E=G/100,D={height:H.height(),width:H.width()};
A.extend(J,{effect:"scale",queue:false,fade:true,mode:I,complete:C,percent:F?G:100,from:F?D:{height:D.height*E,width:D.width*E}});
H.effect(J)
};
A.effects.effect.scale=function(C,F){var D=A(this),L=A.extend(true,{},C),G=A.effects.setMode(D,C.mode||"effect"),H=parseInt(C.percent,10)||(parseInt(C.percent,10)===0?0:(G==="hide"?0:100)),J=C.direction||"both",K=C.origin,E={height:D.height(),width:D.width(),outerHeight:D.outerHeight(),outerWidth:D.outerWidth()},I={y:J!=="horizontal"?(H/100):1,x:J!=="vertical"?(H/100):1};
L.effect="size";
L.queue=false;
L.complete=F;
if(G!=="effect"){L.origin=K||["middle","center"];
L.restore=true
}L.from=C.from||(G==="show"?{height:0,width:0}:E);
L.to={height:E.height*I.y,width:E.width*I.x,outerHeight:E.outerHeight*I.y,outerWidth:E.outerWidth*I.x};
if(L.fade){if(G==="show"){L.from.opacity=0;
L.to.opacity=1
}if(G==="hide"){L.from.opacity=1;
L.to.opacity=0
}}D.effect(L)
};
A.effects.effect.size=function(L,K){var P,I,J,C=A(this),O=["position","top","bottom","left","right","width","height","overflow","opacity"],N=["position","top","bottom","left","right","overflow","opacity"],M=["width","height","overflow"],G=["fontSize"],R=["borderTopWidth","borderBottomWidth","paddingTop","paddingBottom"],D=["borderLeftWidth","borderRightWidth","paddingLeft","paddingRight"],H=A.effects.setMode(C,L.mode||"effect"),Q=L.restore||H!=="effect",U=L.scale||"both",S=L.origin||["middle","center"],T=C.css("position"),E=Q?O:N,F={height:0,width:0};
if(H==="show"){C.show()
}P={height:C.height(),width:C.width(),outerHeight:C.outerHeight(),outerWidth:C.outerWidth()};
if(L.mode==="toggle"&&H==="show"){C.from=L.to||F;
C.to=L.from||P
}else{C.from=L.from||(H==="show"?F:P);
C.to=L.to||(H==="hide"?F:P)
}J={from:{y:C.from.height/P.height,x:C.from.width/P.width},to:{y:C.to.height/P.height,x:C.to.width/P.width}};
if(U==="box"||U==="both"){if(J.from.y!==J.to.y){E=E.concat(R);
C.from=A.effects.setTransition(C,R,J.from.y,C.from);
C.to=A.effects.setTransition(C,R,J.to.y,C.to)
}if(J.from.x!==J.to.x){E=E.concat(D);
C.from=A.effects.setTransition(C,D,J.from.x,C.from);
C.to=A.effects.setTransition(C,D,J.to.x,C.to)
}}if(U==="content"||U==="both"){if(J.from.y!==J.to.y){E=E.concat(G).concat(M);
C.from=A.effects.setTransition(C,G,J.from.y,C.from);
C.to=A.effects.setTransition(C,G,J.to.y,C.to)
}}A.effects.save(C,E);
C.show();
A.effects.createWrapper(C);
C.css("overflow","hidden").css(C.from);
if(S){I=A.effects.getBaseline(S,P);
C.from.top=(P.outerHeight-C.outerHeight())*I.y;
C.from.left=(P.outerWidth-C.outerWidth())*I.x;
C.to.top=(P.outerHeight-C.to.outerHeight)*I.y;
C.to.left=(P.outerWidth-C.to.outerWidth)*I.x
}C.css(C.from);
if(U==="content"||U==="both"){R=R.concat(["marginTop","marginBottom"]).concat(G);
D=D.concat(["marginLeft","marginRight"]);
M=O.concat(R).concat(D);
C.find("*[width]").each(function(){var W=A(this),V={height:W.height(),width:W.width()};
if(Q){A.effects.save(W,M)
}W.from={height:V.height*J.from.y,width:V.width*J.from.x};
W.to={height:V.height*J.to.y,width:V.width*J.to.x};
if(J.from.y!==J.to.y){W.from=A.effects.setTransition(W,R,J.from.y,W.from);
W.to=A.effects.setTransition(W,R,J.to.y,W.to)
}if(J.from.x!==J.to.x){W.from=A.effects.setTransition(W,D,J.from.x,W.from);
W.to=A.effects.setTransition(W,D,J.to.x,W.to)
}W.css(W.from);
W.animate(W.to,L.duration,L.easing,function(){if(Q){A.effects.restore(W,M)
}})
})
}C.animate(C.to,{queue:false,duration:L.duration,easing:L.easing,complete:function(){if(C.to.opacity===0){C.css("opacity",C.from.opacity)
}if(H==="hide"){C.hide()
}A.effects.restore(C,E);
if(!Q){if(T==="static"){C.css({position:"relative",top:C.to.top,left:C.to.left})
}else{A.each(["top","left"],function(V,W){C.css(W,function(Y,a){var Z=parseInt(a,10),X=V?C.to.left:C.to.top;
if(a==="auto"){return X+"px"
}return Z+X+"px"
})
})
}}A.effects.removeWrapper(C);
K()
}})
}
})(jQuery);
(function(A,B){A.effects.effect.shake=function(K,J){var C=A(this),D=["position","top","bottom","left","right","height","width"],I=A.effects.setMode(C,K.mode||"effect"),S=K.direction||"left",E=K.distance||20,H=K.times||3,T=H*2+1,O=Math.round(K.duration/T),G=(S==="up"||S==="down")?"top":"left",F=(S==="up"||S==="left"),R={},Q={},P={},N,L=C.queue(),M=L.length;
A.effects.save(C,D);
C.show();
A.effects.createWrapper(C);
R[G]=(F?"-=":"+=")+E;
Q[G]=(F?"+=":"-=")+E*2;
P[G]=(F?"-=":"+=")+E*2;
C.animate(R,O,K.easing);
for(N=1;
N<H;
N++){C.animate(Q,O,K.easing).animate(P,O,K.easing)
}C.animate(Q,O,K.easing).animate(R,O/2,K.easing).queue(function(){if(I==="hide"){C.hide()
}A.effects.restore(C,D);
A.effects.removeWrapper(C);
J()
});
if(M>1){L.splice.apply(L,[1,0].concat(L.splice(M,T+1)))
}C.dequeue()
}
})(jQuery);
(function(A,B){A.effects.effect.slide=function(E,I){var F=A(this),K=["position","top","bottom","left","right","width","height"],J=A.effects.setMode(F,E.mode||"show"),M=J==="show",L=E.direction||"left",G=(L==="up"||L==="down")?"top":"left",D=(L==="up"||L==="left"),C,H={};
A.effects.save(F,K);
F.show();
C=E.distance||F[G==="top"?"outerHeight":"outerWidth"](true);
A.effects.createWrapper(F).css({overflow:"hidden"});
if(M){F.css(G,D?(isNaN(C)?"-"+C:-C):C)
}H[G]=(M?(D?"+=":"-="):(D?"-=":"+="))+C;
F.animate(H,{queue:false,duration:E.duration,easing:E.easing,complete:function(){if(J==="hide"){F.hide()
}A.effects.restore(F,K);
A.effects.removeWrapper(F);
I()
}})
}
})(jQuery);
(function(A,B){A.effects.effect.transfer=function(D,H){var F=A(this),K=A(D.to),N=K.css("position")==="fixed",J=A("body"),L=N?J.scrollTop():0,M=N?J.scrollLeft():0,C=K.offset(),G={top:C.top-L,left:C.left-M,height:K.innerHeight(),width:K.innerWidth()},I=F.offset(),E=A('<div class="ui-effects-transfer"></div>').appendTo(document.body).addClass(D.className).css({top:I.top-L,left:I.left-M,height:F.innerHeight(),width:F.innerWidth(),position:N?"fixed":"absolute"}).animate(G,D.duration,D.easing,function(){E.remove();
H()
})
}
})(jQuery);
/*
 * jQuery Form Plugin
 * version: 2.87 (20-OCT-2011)
 * @requires jQuery v1.3.2 or later
 *
 * Examples and documentation at: http://malsup.com/jquery/form/
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */
(function(B){B.fn.ajaxSubmit=function(D){if(!this.length){A("ajaxSubmit: skipping submit process - no element selected");
return this
}var C,O,F,H=this;
if(typeof D=="function"){D={success:D}
}C=this.attr("method");
O=this.attr("action");
F=(typeof O==="string")?B.trim(O):"";
F=F||window.location.href||"";
if(F){F=(F.match(/^([^#]+)/)||[])[1]
}D=B.extend(true,{url:F,success:B.ajaxSettings.success,type:C||"GET",iframeSrc:/^https/i.test(window.location.href||"")?"javascript:false":"about:blank"},D);
var K={};
this.trigger("form-pre-serialize",[this,D,K]);
if(K.veto){A("ajaxSubmit: submit vetoed via form-pre-serialize trigger");
return this
}if(D.beforeSerialize&&D.beforeSerialize(this,D)===false){A("ajaxSubmit: submit aborted via beforeSerialize callback");
return this
}var G=D.traditional;
if(G===undefined){G=B.ajaxSettings.traditional
}var S,N,J,T=this.formToArray(D.semantic);
if(D.data){D.extraData=D.data;
S=B.param(D.data,G)
}if(D.beforeSubmit&&D.beforeSubmit(T,this,D)===false){A("ajaxSubmit: submit aborted via beforeSubmit callback");
return this
}this.trigger("form-submit-validate",[T,this,D,K]);
if(K.veto){A("ajaxSubmit: submit vetoed via form-submit-validate trigger");
return this
}var M=B.param(T,G);
if(S){M=(M?(M+"&"+S):S)
}if(D.type.toUpperCase()=="GET"){D.url+=(D.url.indexOf("?")>=0?"&":"?")+M;
D.data=null
}else{D.data=M
}var U=[];
if(D.resetForm){U.push(function(){H.resetForm()
})
}if(D.clearForm){U.push(function(){H.clearForm(D.includeHidden)
})
}if(!D.dataType&&D.target){var E=D.success||function(){};
U.push(function(W){var V=D.replaceTarget?"replaceWith":"html";
B(D.target)[V](W).each(E,arguments)
})
}else{if(D.success){U.push(D.success)
}}D.success=function(Z,W,a){var Y=D.context||D;
for(var X=0,V=U.length;
X<V;
X++){U[X].apply(Y,[Z,W,a||H,H])
}};
var Q=B("input:file",this).length>0;
var P="multipart/form-data";
var L=(H.attr("enctype")==P||H.attr("encoding")==P);
if(D.iframe!==false&&(Q||D.iframe||L)){if(D.closeKeepAlive){B.get(D.closeKeepAlive,function(){I(T)
})
}else{I(T)
}}else{if(B.browser.msie&&C=="get"&&typeof D.type==="undefined"){var R=H[0].getAttribute("method");
if(typeof R==="string"){D.type=R
}}B.ajax(D)
}this.trigger("form-submit-notify",[this,D]);
return this;
function I(y){var Y=H[0],X,u,m,w,q,b,f,d,e,r,v,k;
var c=!!B.fn.prop;
if(y){if(c){for(u=0;
u<y.length;
u++){X=B(Y[y[u].name]);
X.prop("disabled",false)
}}else{for(u=0;
u<y.length;
u++){X=B(Y[y[u].name]);
X.removeAttr("disabled")
}}}if(B(":input[name=submit],:input[id=submit]",Y).length){alert('Error: Form elements must not have name or id of "submit".');
return 
}m=B.extend(true,{},B.ajaxSettings,D);
m.context=m.context||m;
q="jqFormIO"+(new Date().getTime());
if(m.iframeTarget){b=B(m.iframeTarget);
r=b.attr("name");
if(r==null){b.attr("name",q)
}else{q=r
}}else{b=B('<iframe name="'+q+'" src="'+m.iframeSrc+'" />');
b.css({position:"absolute",top:"-1000px",left:"-1000px"})
}f=b[0];
d={aborted:0,responseText:null,responseXML:null,status:0,statusText:"n/a",getAllResponseHeaders:function(){},getResponseHeader:function(){},setRequestHeader:function(){},abort:function(a){var g=(a==="timeout"?"timeout":"aborted");
A("aborting upload... "+g);
this.aborted=1;
b.attr("src",m.iframeSrc);
d.error=g;
m.error&&m.error.call(m.context,d,g,a);
w&&B.event.trigger("ajaxError",[d,m,g]);
m.complete&&m.complete.call(m.context,d,g)
}};
w=m.global;
if(w&&!B.active++){B.event.trigger("ajaxStart")
}if(w){B.event.trigger("ajaxSend",[d,m])
}if(m.beforeSend&&m.beforeSend.call(m.context,d,m)===false){if(m.global){B.active--
}return 
}if(d.aborted){return 
}e=Y.clk;
if(e){r=e.name;
if(r&&!e.disabled){m.extraData=m.extraData||{};
m.extraData[r]=e.value;
if(e.type=="image"){m.extraData[r+".x"]=Y.clk_x;
m.extraData[r+".y"]=Y.clk_y
}}}var l=1;
var h=2;
function j(g){var a=g.contentWindow?g.contentWindow.document:g.contentDocument?g.contentDocument:g.document;
return a
}function t(){var AB=H.attr("target"),g=H.attr("action");
Y.setAttribute("target",q);
if(!C){Y.setAttribute("method","POST")
}if(g!=m.url){Y.setAttribute("action",m.url)
}if(!m.skipEncodingOverride&&(!C||/post/i.test(C))){H.attr({encoding:"multipart/form-data",enctype:"multipart/form-data"})
}if(m.timeout){k=setTimeout(function(){v=true;
p(l)
},m.timeout)
}function AC(){try{var a=j(f).readyState;
A("state = "+a);
if(a.toLowerCase()=="uninitialized"){setTimeout(AC,50)
}}catch(n){A("Server abort: ",n," (",n.name,")");
p(h);
k&&clearTimeout(k);
k=undefined
}}var s=[];
try{if(m.extraData){for(var AD in m.extraData){s.push(B('<input type="hidden" name="'+AD+'" />').attr("value",m.extraData[AD]).appendTo(Y)[0])
}}if(!m.iframeTarget){b.appendTo("body");
f.attachEvent?f.attachEvent("onload",p):f.addEventListener("load",p,false)
}setTimeout(AC,15);
Y.submit()
}finally{Y.setAttribute("action",g);
if(AB){Y.setAttribute("target",AB)
}else{H.removeAttr("target")
}B(s).remove()
}}if(m.forceSync){t()
}else{setTimeout(t,10)
}var z,AA,x=50,Z;
function p(AC){if(d.aborted||Z){return 
}try{AA=j(f)
}catch(AF){A("cannot access response document: ",AF);
AC=h
}if(AC===l&&d){d.abort("timeout");
return 
}else{if(AC==h&&d){d.abort("server abort");
return 
}}if(!AA||AA.location.href==m.iframeSrc){if(!v){return 
}}f.detachEvent?f.detachEvent("onload",p):f.removeEventListener("load",p,false);
var s="success",AE;
try{if(v){throw"timeout"
}var n=m.dataType=="xml"||AA.XMLDocument||B.isXMLDoc(AA);
A("isXml="+n);
if(!n&&window.opera&&(AA.body==null||AA.body.innerHTML=="")){if(--x){A("requeing onLoad callback, DOM not available");
setTimeout(p,250);
return 
}}var AG=AA.body?AA.body:AA.documentElement;
d.responseText=AG?AG.innerHTML:null;
d.responseXML=AA.XMLDocument?AA.XMLDocument:AA;
if(n){m.dataType="xml"
}d.getResponseHeader=function(AJ){var AI={"content-type":m.dataType};
return AI[AJ]
};
if(AG){d.status=Number(AG.getAttribute("status"))||d.status;
d.statusText=AG.getAttribute("statusText")||d.statusText
}var a=(m.dataType||"").toLowerCase();
var AD=/(json|script|text)/.test(a);
if(AD||m.textarea){var AB=AA.getElementsByTagName("textarea")[0];
if(AB){d.responseText=AB.value;
d.status=Number(AB.getAttribute("status"))||d.status;
d.statusText=AB.getAttribute("statusText")||d.statusText
}else{if(AD){var g=AA.getElementsByTagName("pre")[0];
var AH=AA.getElementsByTagName("body")[0];
if(g){d.responseText=g.textContent?g.textContent:g.innerText
}else{if(AH){d.responseText=AH.textContent?AH.textContent:AH.innerText
}}}}}else{if(a=="xml"&&!d.responseXML&&d.responseText!=null){d.responseXML=o(d.responseText)
}}try{z=V(d,a,m)
}catch(AC){s="parsererror";
d.error=AE=(AC||s)
}}catch(AC){A("error caught: ",AC);
s="error";
d.error=AE=(AC||s)
}if(d.aborted){A("upload aborted");
s=null
}if(d.status){s=(d.status>=200&&d.status<300||d.status===304)?"success":"error"
}if(s==="success"){m.success&&m.success.call(m.context,z,"success",d);
w&&B.event.trigger("ajaxSuccess",[d,m])
}else{if(s){if(AE==undefined){AE=d.statusText
}m.error&&m.error.call(m.context,d,s,AE);
w&&B.event.trigger("ajaxError",[d,m,AE])
}}w&&B.event.trigger("ajaxComplete",[d,m]);
if(w&&!--B.active){B.event.trigger("ajaxStop")
}m.complete&&m.complete.call(m.context,d,s);
Z=true;
if(m.timeout){clearTimeout(k)
}setTimeout(function(){if(!m.iframeTarget){b.remove()
}d.responseXML=null
},100)
}var o=B.parseXML||function(a,g){if(window.ActiveXObject){g=new ActiveXObject("Microsoft.XMLDOM");
g.async="false";
g.loadXML(a)
}else{g=(new DOMParser()).parseFromString(a,"text/xml")
}return(g&&g.documentElement&&g.documentElement.nodeName!="parsererror")?g:null
};
var W=B.parseJSON||function(a){return window["eval"]("("+a+")")
};
var V=function(AD,AB,n){var g=AD.getResponseHeader("content-type")||"",a=AB==="xml"||!AB&&g.indexOf("xml")>=0,AC=a?AD.responseXML:AD.responseText;
if(a&&AC.documentElement.nodeName==="parsererror"){B.error&&B.error("parsererror")
}if(n&&n.dataFilter){AC=n.dataFilter(AC,AB)
}if(typeof AC==="string"){if(AB==="json"||!AB&&g.indexOf("json")>=0){AC=W(AC)
}else{if(AB==="script"||!AB&&g.indexOf("javascript")>=0){B.globalEval(AC)
}}}return AC
}
}};
B.fn.ajaxForm=function(C){if(this.length===0){var D={s:this.selector,c:this.context};
if(!B.isReady&&D.s){A("DOM not ready, queuing ajaxForm");
B(function(){B(D.s,D.c).ajaxForm(C)
});
return this
}A("terminating; zero elements found by selector"+(B.isReady?"":" (DOM not ready)"));
return this
}return this.ajaxFormUnbind().bind("submit.form-plugin",function(E){if(!E.isDefaultPrevented()){E.preventDefault();
B(this).ajaxSubmit(C)
}}).bind("click.form-plugin",function(I){var H=I.target;
var F=B(H);
if(!(F.is(":submit,input:image"))){var E=F.closest(":submit");
if(E.length==0){return 
}H=E[0]
}var G=this;
G.clk=H;
if(H.type=="image"){if(I.offsetX!=undefined){G.clk_x=I.offsetX;
G.clk_y=I.offsetY
}else{if(typeof B.fn.offset=="function"){var J=F.offset();
G.clk_x=I.pageX-J.left;
G.clk_y=I.pageY-J.top
}else{G.clk_x=I.pageX-H.offsetLeft;
G.clk_y=I.pageY-H.offsetTop
}}}setTimeout(function(){G.clk=G.clk_x=G.clk_y=null
},100)
})
};
B.fn.ajaxFormUnbind=function(){return this.unbind("submit.form-plugin click.form-plugin")
};
B.fn.formToArray=function(N){var M=[];
if(this.length===0){return M
}var D=this[0];
var G=N?D.getElementsByTagName("*"):D.elements;
if(!G){return M
}var I,H,F,O,E,K,C;
for(I=0,K=G.length;
I<K;
I++){E=G[I];
F=E.name;
if(!F){continue
}if(N&&D.clk&&E.type=="image"){if(!E.disabled&&D.clk==E){M.push({name:F,value:B(E).val()});
M.push({name:F+".x",value:D.clk_x},{name:F+".y",value:D.clk_y})
}continue
}O=B.fieldValue(E,true);
if(O&&O.constructor==Array){for(H=0,C=O.length;
H<C;
H++){M.push({name:F,value:O[H]})
}}else{if(O!==null&&typeof O!="undefined"){M.push({name:F,value:O})
}}}if(!N&&D.clk){var J=B(D.clk),L=J[0];
F=L.name;
if(F&&!L.disabled&&L.type=="image"){M.push({name:F,value:J.val()});
M.push({name:F+".x",value:D.clk_x},{name:F+".y",value:D.clk_y})
}}return M
};
B.fn.formSerialize=function(C){return B.param(this.formToArray(C))
};
B.fn.fieldSerialize=function(D){var C=[];
this.each(function(){var H=this.name;
if(!H){return 
}var F=B.fieldValue(this,D);
if(F&&F.constructor==Array){for(var G=0,E=F.length;
G<E;
G++){C.push({name:H,value:F[G]})
}}else{if(F!==null&&typeof F!="undefined"){C.push({name:this.name,value:F})
}}});
return B.param(C)
};
B.fn.fieldValue=function(H){for(var G=[],E=0,C=this.length;
E<C;
E++){var F=this[E];
var D=B.fieldValue(F,H);
if(D===null||typeof D=="undefined"||(D.constructor==Array&&!D.length)){continue
}D.constructor==Array?B.merge(G,D):G.push(D)
}return G
};
B.fieldValue=function(C,I){var E=C.name,N=C.type,O=C.tagName.toLowerCase();
if(I===undefined){I=true
}if(I&&(!E||C.disabled||N=="reset"||N=="button"||(N=="checkbox"||N=="radio")&&!C.checked||(N=="submit"||N=="image")&&C.form&&C.form.clk!=C||O=="select"&&C.selectedIndex==-1)){return null
}if(O=="select"){var J=C.selectedIndex;
if(J<0){return null
}var L=[],D=C.options;
var G=(N=="select-one");
var K=(G?J+1:D.length);
for(var F=(G?J:0);
F<K;
F++){var H=D[F];
if(H.selected){var M=H.value;
if(!M){M=(H.attributes&&H.attributes.value&&!(H.attributes.value.specified))?H.text:H.value
}if(G){return M
}L.push(M)
}}return L
}return B(C).val()
};
B.fn.clearForm=function(C){return this.each(function(){B("input,select,textarea",this).clearFields(C)
})
};
B.fn.clearFields=B.fn.clearInputs=function(C){var D=/^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i;
return this.each(function(){var F=this.type,E=this.tagName.toLowerCase();
if(D.test(F)||E=="textarea"||(C&&/hidden/.test(F))){this.value=""
}else{if(F=="checkbox"||F=="radio"){this.checked=false
}else{if(E=="select"){this.selectedIndex=-1
}}}})
};
B.fn.resetForm=function(){return this.each(function(){if(typeof this.reset=="function"||(typeof this.reset=="object"&&!this.reset.nodeType)){this.reset()
}})
};
B.fn.enable=function(C){if(C===undefined){C=true
}return this.each(function(){this.disabled=!C
})
};
B.fn.selected=function(C){if(C===undefined){C=true
}return this.each(function(){var D=this.type;
if(D=="checkbox"||D=="radio"){this.checked=C
}else{if(this.tagName.toLowerCase()=="option"){var E=B(this).parent("select");
if(C&&E[0]&&E[0].type=="select-one"){E.find("option").selected(false)
}this.selected=C
}}})
};
B.fn.ajaxSubmit.debug=false;
function A(){if(!B.fn.ajaxSubmit.debug){return 
}var C="[jquery.form] "+Array.prototype.join.call(arguments,"");
if(window.console&&window.console.log){window.console.log(C)
}else{if(window.opera&&window.opera.postError){window.opera.postError(C)
}}}})(jQuery);
/*
 * jQuery Cookie Plugin v1.3
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2011, Klaus Hartl
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.opensource.org/licenses/GPL-2.0
 */
(function(F,B,G){var A=/\+/g;
function E(H){return H
}function C(H){return decodeURIComponent(H.replace(A," "))
}var D=F.cookie=function(N,M,R){if(M!==G){R=F.extend({},D.defaults,R);
if(M===null){R.expires=-1
}if(typeof R.expires==="number"){var O=R.expires,Q=R.expires=new Date();
Q.setDate(Q.getDate()+O)
}M=D.json?JSON.stringify(M):String(M);
return(B.cookie=[encodeURIComponent(N),"=",D.raw?M:encodeURIComponent(M),R.expires?"; expires="+R.expires.toUTCString():"",R.path?"; path="+R.path:"",R.domain?"; domain="+R.domain:"",R.secure?"; secure":""].join(""))
}var H=D.raw?E:C;
var P=B.cookie.split("; ");
for(var L=0,J=P.length;
L<J;
L++){var K=P[L].split("=");
if(H(K.shift())===N){var I=H(K.join("="));
return D.json?JSON.parse(I):I
}}return null
};
D.defaults={};
F.removeCookie=function(I,H){if(F.cookie(I)!==null){F.cookie(I,null,H);
return true
}return false
}
})(jQuery,document);
(function(A){A.fn.dcMegaMenu=function(C){var D={classParent:"dc-mega",classContainer:"sub-container",classSubParent:"mega-hdr",classSubLink:"mega-hdr",classWidget:"dc-extra",rowItems:3,speed:"fast",effect:"fade",event:"hover",fullWidth:false,onLoad:function(){},beforeOpen:function(){},beforeClose:function(){}};
var C=A.extend(D,C);
var B=this;
return B.each(function(P){var J=D.classSubParent;
var O=D.classSubLink;
var M=D.classParent;
var N=D.classContainer;
var K=D.classWidget;
L();
function I(){var Q=A(".sub",this);
A(this).addClass("mega-hover");
if(D.effect=="fade"){A(Q).fadeIn(D.speed)
}if(D.effect=="slide"){A(Q).show(D.speed)
}D.beforeOpen.call(this)
}function F(R){var Q=A(".sub",R);
A(R).addClass("mega-hover");
if(D.effect=="fade"){A(Q).fadeIn(D.speed)
}if(D.effect=="slide"){A(Q).show(D.speed)
}D.beforeOpen.call(this)
}function H(){var Q=A(".sub",this);
A(this).removeClass("mega-hover");
A(Q).hide();
D.beforeClose.call(this)
}function E(R){var Q=A(".sub",R);
A(R).removeClass("mega-hover");
A(Q).hide();
D.beforeClose.call(this)
}function G(){A("li",B).removeClass("mega-hover");
A(".sub",B).hide()
}function L(){$arrow='<span class="dc-mega-icon"></span>';
var S=M+"-li";
var R=B.outerWidth();
A("> li",B).each(function(){var d=A("> ul",this);
var U=A("> a",this);
if(d.length){U.addClass(M).append($arrow);
d.addClass("sub").wrap('<div class="'+N+'" />');
var Y=A(this).position();
pl=Y.left;
if(A("ul",d).length){A(this).addClass(S);
A("."+N,this).addClass("mega");
A("> li",d).each(function(){if(!A(this).hasClass(K)){A(this).addClass("mega-unit");
if(A("> ul",this).length){A(this).addClass(J);
A("> a",this).addClass(J+"-a")
}else{A(this).addClass(O);
A("> a",this).addClass(O+"-a")
}}});
var V=A(".mega-unit",this);
rowSize=parseInt(D.rowItems);
for(var k=0;
k<V.length;
k+=rowSize){V.slice(k,k+rowSize).wrapAll('<div class="row" />')
}d.show();
var b=A(this).width();
var h=pl+b;
var g=R-h;
var f=d.outerWidth();
var e=d.parent("."+N).outerWidth();
var c=e-f;
if(D.fullWidth==true){var W=R-c;
d.parent("."+N).css({width:W+"px"});
B.addClass("full-width")
}var X=A(".mega-unit",d).outerWidth(true);
var j=A(".row:eq(0) .mega-unit",d).length;
var Z=X*j;
var a=Z+c;
A(".row",this).each(function(){A(".mega-unit:last",this).addClass("last");
var o=undefined;
A(".mega-unit > a",this).each(function(){var p=parseInt(A(this).height());
if(o===undefined||o<p){o=p
}});
A(".mega-unit > a",this).css("height",o+"px");
A(this).css("width",Z+"px")
});
if(D.fullWidth==true){n={left:0}
}else{var l=g<l?l+l-g:(a-b)/2;
var m=pl-l;
var n={left:pl+"px",marginLeft:-l+"px"};
if(m<0){n={left:0}
}else{if(g<l){n={right:0}
}}}A("."+N,this).css(n);
A(".row",d).each(function(){var o=A(this).height();
A(".mega-unit",this).css({height:o+"px"});
A(this).parent(".row").css({height:o+"px"})
});
d.hide()
}else{A("."+N,this).addClass("non-mega").css("left",pl+"px")
}}});
var T=A("> li > a",B).outerHeight(true);
A("."+N,B).css({top:T+"px"}).css("z-index","1000");
if(D.event=="hover"){var Q={sensitivity:2,interval:100,over:I,timeout:400,out:H};
A("li",B).hoverIntent(Q)
}if(D.event=="click"){A("body").mouseup(function(U){if(!A(U.target).parents(".mega-hover").length){G()
}});
A("> li > a."+M,B).click(function(U){var V=A(this).parent();
if(V.hasClass("mega-hover")){E(V)
}else{F(V)
}U.preventDefault()
})
}D.onLoad.call(this)
}})
}
})(jQuery);
(function(A){A.fn.hoverIntent=function(I,H){var J={sensitivity:7,interval:100,timeout:0};
J=A.extend(J,H?{over:I,out:H}:I);
var L,K,F,D;
var E=function(M){L=M.pageX;
K=M.pageY
};
var C=function(N,M){M.hoverIntent_t=clearTimeout(M.hoverIntent_t);
if((Math.abs(F-L)+Math.abs(D-K))<J.sensitivity){A(M).unbind("mousemove",E);
M.hoverIntent_s=1;
return J.over.apply(M,[N])
}else{F=L;
D=K;
M.hoverIntent_t=setTimeout(function(){C(N,M)
},J.interval)
}};
var G=function(N,M){M.hoverIntent_t=clearTimeout(M.hoverIntent_t);
M.hoverIntent_s=0;
return J.out.apply(M,[N])
};
var B=function(O){var N=jQuery.extend({},O);
var M=this;
if(M.hoverIntent_t){M.hoverIntent_t=clearTimeout(M.hoverIntent_t)
}if(O.type=="mouseenter"){F=N.pageX;
D=N.pageY;
A(M).bind("mousemove",E);
if(M.hoverIntent_s!=1){M.hoverIntent_t=setTimeout(function(){C(N,M)
},J.interval)
}}else{A(M).unbind("mousemove",E);
if(M.hoverIntent_s==1){M.hoverIntent_t=setTimeout(function(){G(N,M)
},J.timeout)
}}};
return this.bind("mouseenter",B).bind("mouseleave",B)
}
})(jQuery);
/* jQuery Validation Plugin - v1.10.0 - 9/7/2012
* https://github.com/jzaefferer/jquery-validation
* Copyright (c) 2012 JÃ¶rn Zaefferer; Licensed MIT, GPL */
(function(A){A.extend(A.fn,{validate:function(B){if(!this.length){if(B&&B.debug&&window.console){console.warn("nothing selected, can't validate, returning nothing")
}return 
}var C=A.data(this[0],"validator");
if(C){return C
}this.attr("novalidate","novalidate");
C=new A.validator(B,this[0]);
A.data(this[0],"validator",C);
if(C.settings.onsubmit){this.validateDelegate(":submit","click",function(D){if(C.settings.submitHandler){C.submitButton=D.target
}if(A(D.target).hasClass("cancel")){C.cancelSubmit=true
}});
this.submit(function(D){if(C.settings.debug){D.preventDefault()
}function E(){var F;
if(C.settings.submitHandler){if(C.submitButton){F=A("<input type='hidden'/>").attr("name",C.submitButton.name).val(C.submitButton.value).appendTo(C.currentForm)
}C.settings.submitHandler.call(C,C.currentForm,D);
if(C.submitButton){F.remove()
}return false
}return true
}if(C.cancelSubmit){C.cancelSubmit=false;
return E()
}if(C.form()){if(C.pendingRequest){C.formSubmitted=true;
return false
}return E()
}else{C.focusInvalid();
return false
}})
}return C
},valid:function(){if(A(this[0]).is("form")){return this.validate().form()
}else{var C=true;
var B=A(this[0].form).validate();
this.each(function(){C&=B.element(this)
});
return C
}},removeAttrs:function(D){var B={},C=this;
A.each(D.split(/\s/),function(E,F){B[F]=C.attr(F);
C.removeAttr(F)
});
return B
},rules:function(E,B){var G=this[0];
if(E){var D=A.data(G.form,"validator").settings;
var I=D.rules;
var J=A.validator.staticRules(G);
switch(E){case"add":A.extend(J,A.validator.normalizeRule(B));
I[G.name]=J;
if(B.messages){D.messages[G.name]=A.extend(D.messages[G.name],B.messages)
}break;
case"remove":if(!B){delete I[G.name];
return J
}var H={};
A.each(B.split(/\s/),function(K,L){H[L]=J[L];
delete J[L]
});
return H
}}var F=A.validator.normalizeRules(A.extend({},A.validator.metadataRules(G),A.validator.classRules(G),A.validator.attributeRules(G),A.validator.staticRules(G)),G);
if(F.required){var C=F.required;
delete F.required;
F=A.extend({required:C},F)
}return F
}});
A.extend(A.expr[":"],{blank:function(B){return !A.trim(""+B.value)
},filled:function(B){return !!A.trim(""+B.value)
},unchecked:function(B){return !B.checked
}});
A.validator=function(B,C){this.settings=A.extend(true,{},A.validator.defaults,B);
this.currentForm=C;
this.init()
};
A.validator.format=function(B,C){if(arguments.length===1){return function(){var D=A.makeArray(arguments);
D.unshift(B);
return A.validator.format.apply(this,D)
}
}if(arguments.length>2&&C.constructor!==Array){C=A.makeArray(arguments).slice(1)
}if(C.constructor!==Array){C=[C]
}A.each(C,function(D,E){B=B.replace(new RegExp("\\{"+D+"\\}","g"),E)
});
return B
};
A.extend(A.validator,{defaults:{messages:{},groups:{},rules:{},errorClass:"error",validClass:"valid",errorElement:"label",focusInvalid:true,errorContainer:A([]),errorLabelContainer:A([]),onsubmit:true,ignore:":hidden",ignoreTitle:false,onfocusin:function(B,C){this.lastActive=B;
if(this.settings.focusCleanup&&!this.blockFocusCleanup){if(this.settings.unhighlight){this.settings.unhighlight.call(this,B,this.settings.errorClass,this.settings.validClass)
}this.addWrapper(this.errorsFor(B)).hide()
}},onfocusout:function(B,C){if(!this.checkable(B)&&(B.name in this.submitted||!this.optional(B))){this.element(B)
}},onkeyup:function(B,C){if(C.which===9&&this.elementValue(B)===""){return 
}else{if(B.name in this.submitted||B===this.lastActive){this.element(B)
}}},onclick:function(B,C){if(B.name in this.submitted){this.element(B)
}else{if(B.parentNode.name in this.submitted){this.element(B.parentNode)
}}},highlight:function(D,B,C){if(D.type==="radio"){this.findByName(D.name).addClass(B).removeClass(C)
}else{A(D).addClass(B).removeClass(C)
}},unhighlight:function(D,B,C){if(D.type==="radio"){this.findByName(D.name).removeClass(B).addClass(C)
}else{A(D).removeClass(B).addClass(C)
}}},setDefaults:function(B){A.extend(A.validator.defaults,B)
},messages:{required:"This field is required.",remote:"Please fix this field.",email:"Please enter a valid email address.",url:"Please enter a valid URL.",date:"Please enter a valid date.",dateISO:"Please enter a valid date (ISO).",number:"Please enter a valid number.",digits:"Please enter only digits.",creditcard:"Please enter a valid credit card number.",equalTo:"Please enter the same value again.",maxlength:A.validator.format("Please enter no more than {0} characters."),minlength:A.validator.format("Please enter at least {0} characters."),rangelength:A.validator.format("Please enter a value between {0} and {1} characters long."),range:A.validator.format("Please enter a value between {0} and {1}."),max:A.validator.format("Please enter a value less than or equal to {0}."),min:A.validator.format("Please enter a value greater than or equal to {0}.")},autoCreateRanges:false,prototype:{init:function(){this.labelContainer=A(this.settings.errorLabelContainer);
this.errorContext=this.labelContainer.length&&this.labelContainer||A(this.currentForm);
this.containers=A(this.settings.errorContainer).add(this.settings.errorLabelContainer);
this.submitted={};
this.valueCache={};
this.pendingRequest=0;
this.pending={};
this.invalid={};
this.reset();
var B=(this.groups={});
A.each(this.settings.groups,function(E,F){A.each(F.split(/\s/),function(H,G){B[G]=E
})
});
var D=this.settings.rules;
A.each(D,function(E,F){D[E]=A.validator.normalizeRule(F)
});
function C(G){var F=A.data(this[0].form,"validator"),E="on"+G.type.replace(/^validate/,"");
if(F.settings[E]){F.settings[E].call(F,this[0],G)
}}A(this.currentForm).validateDelegate(":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'] ,[type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'] ","focusin focusout keyup",C).validateDelegate("[type='radio'], [type='checkbox'], select, option","click",C);
if(this.settings.invalidHandler){A(this.currentForm).bind("invalid-form.validate",this.settings.invalidHandler)
}},form:function(){this.checkForm();
A.extend(this.submitted,this.errorMap);
this.invalid=A.extend({},this.errorMap);
if(!this.valid()){A(this.currentForm).triggerHandler("invalid-form",[this])
}this.showErrors();
return this.valid()
},checkForm:function(){this.prepareForm();
for(var B=0,C=(this.currentElements=this.elements());
C[B];
B++){this.check(C[B])
}return this.valid()
},element:function(C){C=this.validationTargetFor(this.clean(C));
this.lastElement=C;
this.prepareElement(C);
this.currentElements=A(C);
var B=this.check(C)!==false;
if(B){delete this.invalid[C.name]
}else{this.invalid[C.name]=true
}if(!this.numberOfInvalids()){this.toHide=this.toHide.add(this.containers)
}this.showErrors();
return B
},showErrors:function(C){if(C){A.extend(this.errorMap,C);
this.errorList=[];
for(var B in C){this.errorList.push({message:C[B],element:this.findByName(B)[0]})
}this.successList=A.grep(this.successList,function(D){return !(D.name in C)
})
}if(this.settings.showErrors){this.settings.showErrors.call(this,this.errorMap,this.errorList)
}else{this.defaultShowErrors()
}},resetForm:function(){if(A.fn.resetForm){A(this.currentForm).resetForm()
}this.submitted={};
this.lastElement=null;
this.prepareForm();
this.hideErrors();
this.elements().removeClass(this.settings.errorClass).removeData("previousValue")
},numberOfInvalids:function(){return this.objectLength(this.invalid)
},objectLength:function(D){var C=0;
for(var B in D){C++
}return C
},hideErrors:function(){this.addWrapper(this.toHide).hide()
},valid:function(){return this.size()===0
},size:function(){return this.errorList.length
},focusInvalid:function(){if(this.settings.focusInvalid){try{A(this.findLastActive()||this.errorList.length&&this.errorList[0].element||[]).filter(":visible").focus().trigger("focusin")
}catch(B){}}},findLastActive:function(){var B=this.lastActive;
return B&&A.grep(this.errorList,function(C){return C.element.name===B.name
}).length===1&&B
},elements:function(){var C=this,B={};
return A(this.currentForm).find("input, select, textarea").not(":submit, :reset, :image, [disabled]").not(this.settings.ignore).filter(function(){if(!this.name&&C.settings.debug&&window.console){console.error("%o has no name assigned",this)
}if(this.name in B||!C.objectLength(A(this).rules())){return false
}B[this.name]=true;
return true
})
},clean:function(B){return A(B)[0]
},errors:function(){var B=this.settings.errorClass.replace(" ",".");
return A(this.settings.errorElement+"."+B,this.errorContext)
},reset:function(){this.successList=[];
this.errorList=[];
this.errorMap={};
this.toShow=A([]);
this.toHide=A([]);
this.currentElements=A([])
},prepareForm:function(){this.reset();
this.toHide=this.errors().add(this.containers)
},prepareElement:function(B){this.reset();
this.toHide=this.errorsFor(B)
},elementValue:function(B){var C=A(B).attr("type"),D=A(B).val();
if(C==="radio"||C==="checkbox"){return A('input[name="'+A(B).attr("name")+'"]:checked').val()
}if(typeof D==="string"){return D.replace(/\r/g,"")
}return D
},check:function(C){C=this.validationTargetFor(this.clean(C));
var H=A(C).rules();
var D=false;
var G=this.elementValue(C);
var B;
for(var I in H){var F={method:I,parameters:H[I]};
try{B=A.validator.methods[I].call(this,G,C,F.parameters);
if(B==="dependency-mismatch"){D=true;
continue
}D=false;
if(B==="pending"){this.toHide=this.toHide.not(this.errorsFor(C));
return 
}if(!B){this.formatAndAdd(C,F);
return false
}}catch(E){if(this.settings.debug&&window.console){console.log("exception occured when checking element "+C.id+", check the '"+F.method+"' method",E)
}throw E
}}if(D){return 
}if(this.objectLength(H)){this.successList.push(C)
}return true
},customMetaMessage:function(B,D){if(!A.metadata){return 
}var C=this.settings.meta?A(B).metadata()[this.settings.meta]:A(B).metadata();
return C&&C.messages&&C.messages[D]
},customDataMessage:function(B,C){return A(B).data("msg-"+C.toLowerCase())||(B.attributes&&A(B).attr("data-msg-"+C.toLowerCase()))
},customMessage:function(C,D){var B=this.settings.messages[C];
return B&&(B.constructor===String?B:B[D])
},findDefined:function(){for(var B=0;
B<arguments.length;
B++){if(arguments[B]!==undefined){return arguments[B]
}}return undefined
},defaultMessage:function(B,C){return this.findDefined(this.customMessage(B.name,C),this.customDataMessage(B,C),this.customMetaMessage(B,C),!this.settings.ignoreTitle&&B.title||undefined,A.validator.messages[C],"<strong>Warning: No message defined for "+B.name+"</strong>")
},formatAndAdd:function(C,E){var D=this.defaultMessage(C,E.method),B=/\$?\{(\d+)\}/g;
if(typeof D==="function"){D=D.call(this,E.parameters,C)
}else{if(B.test(D)){D=A.validator.format(D.replace(B,"{$1}"),E.parameters)
}}this.errorList.push({message:D,element:C});
this.errorMap[C.name]=D;
this.submitted[C.name]=D
},addWrapper:function(B){if(this.settings.wrapper){B=B.add(B.parent(this.settings.wrapper))
}return B
},defaultShowErrors:function(){var C,D;
for(C=0;
this.errorList[C];
C++){var B=this.errorList[C];
if(this.settings.highlight){this.settings.highlight.call(this,B.element,this.settings.errorClass,this.settings.validClass)
}this.showLabel(B.element,B.message)
}if(this.errorList.length){this.toShow=this.toShow.add(this.containers)
}if(this.settings.success){for(C=0;
this.successList[C];
C++){this.showLabel(this.successList[C])
}}if(this.settings.unhighlight){for(C=0,D=this.validElements();
D[C];
C++){this.settings.unhighlight.call(this,D[C],this.settings.errorClass,this.settings.validClass)
}}this.toHide=this.toHide.not(this.toShow);
this.hideErrors();
this.addWrapper(this.toShow).show()
},validElements:function(){return this.currentElements.not(this.invalidElements())
},invalidElements:function(){return A(this.errorList).map(function(){return this.element
})
},showLabel:function(C,D){var B=this.errorsFor(C);
if(B.length){B.removeClass(this.settings.validClass).addClass(this.settings.errorClass);
if(B.attr("generated")){B.html(D)
}}else{B=A("<"+this.settings.errorElement+"/>").attr({"for":this.idOrName(C),generated:true}).addClass(this.settings.errorClass).html(D||"");
if(this.settings.wrapper){B=B.hide().show().wrap("<"+this.settings.wrapper+"/>").parent()
}if(!this.labelContainer.append(B).length){if(this.settings.errorPlacement){this.settings.errorPlacement(B,A(C))
}else{B.insertAfter(C)
}}}if(!D&&this.settings.success){B.text("");
if(typeof this.settings.success==="string"){B.addClass(this.settings.success)
}else{this.settings.success(B,C)
}}this.toShow=this.toShow.add(B)
},errorsFor:function(C){var B=this.idOrName(C);
return this.errors().filter(function(){return A(this).attr("for")===B
})
},idOrName:function(B){return this.groups[B.name]||(this.checkable(B)?B.name:B.id||B.name)
},validationTargetFor:function(B){if(this.checkable(B)){B=this.findByName(B.name).not(this.settings.ignore)[0]
}return B
},checkable:function(B){return(/radio|checkbox/i).test(B.type)
},findByName:function(B){return A(this.currentForm).find('[name="'+B+'"]')
},getLength:function(C,B){switch(B.nodeName.toLowerCase()){case"select":return A("option:selected",B).length;
case"input":if(this.checkable(B)){return this.findByName(B.name).filter(":checked").length
}}return C.length
},depend:function(C,B){return this.dependTypes[typeof C]?this.dependTypes[typeof C](C,B):true
},dependTypes:{"boolean":function(C,B){return C
},string:function(C,B){return !!A(C,B.form).length
},"function":function(C,B){return C(B)
}},optional:function(B){var C=this.elementValue(B);
return !A.validator.methods.required.call(this,C,B)&&"dependency-mismatch"
},startRequest:function(B){if(!this.pending[B.name]){this.pendingRequest++;
this.pending[B.name]=true
}},stopRequest:function(B,C){this.pendingRequest--;
if(this.pendingRequest<0){this.pendingRequest=0
}delete this.pending[B.name];
if(C&&this.pendingRequest===0&&this.formSubmitted&&this.form()){A(this.currentForm).submit();
this.formSubmitted=false
}else{if(!C&&this.pendingRequest===0&&this.formSubmitted){A(this.currentForm).triggerHandler("invalid-form",[this]);
this.formSubmitted=false
}}},previousValue:function(B){return A.data(B,"previousValue")||A.data(B,"previousValue",{old:null,valid:true,message:this.defaultMessage(B,"remote")})
}},classRuleSettings:{required:{required:true},email:{email:true},url:{url:true},date:{date:true},dateISO:{dateISO:true},number:{number:true},digits:{digits:true},creditcard:{creditcard:true}},addClassRules:function(B,C){if(B.constructor===String){this.classRuleSettings[B]=C
}else{A.extend(this.classRuleSettings,B)
}},classRules:function(C){var D={};
var B=A(C).attr("class");
if(B){A.each(B.split(" "),function(){if(this in A.validator.classRuleSettings){A.extend(D,A.validator.classRuleSettings[this])
}})
}return D
},attributeRules:function(C){var E={};
var B=A(C);
for(var F in A.validator.methods){var D;
if(F==="required"){D=B.get(0).getAttribute(F);
if(D===""){D=true
}D=!!D
}else{D=B.attr(F)
}if(D){E[F]=D
}else{if(B[0].getAttribute("type")===F){E[F]=true
}}}if(E.maxlength&&/-1|2147483647|524288/.test(E.maxlength)){delete E.maxlength
}return E
},metadataRules:function(B){if(!A.metadata){return{}
}var C=A.data(B.form,"validator").settings.meta;
return C?A(B).metadata()[C]:A(B).metadata()
},staticRules:function(C){var D={};
var B=A.data(C.form,"validator");
if(B.settings.rules){D=A.validator.normalizeRule(B.settings.rules[C.name])||{}
}return D
},normalizeRules:function(C,B){A.each(C,function(F,E){if(E===false){delete C[F];
return 
}if(E.param||E.depends){var D=true;
switch(typeof E.depends){case"string":D=!!A(E.depends,B.form).length;
break;
case"function":D=E.depends.call(B,B);
break
}if(D){C[F]=E.param!==undefined?E.param:true
}else{delete C[F]
}}});
A.each(C,function(D,E){C[D]=A.isFunction(E)?E(B):E
});
A.each(["minlength","maxlength","min","max"],function(){if(C[this]){C[this]=Number(C[this])
}});
A.each(["rangelength","range"],function(){if(C[this]){C[this]=[Number(C[this][0]),Number(C[this][1])]
}});
if(A.validator.autoCreateRanges){if(C.min&&C.max){C.range=[C.min,C.max];
delete C.min;
delete C.max
}if(C.minlength&&C.maxlength){C.rangelength=[C.minlength,C.maxlength];
delete C.minlength;
delete C.maxlength
}}if(C.messages){delete C.messages
}return C
},normalizeRule:function(C){if(typeof C==="string"){var B={};
A.each(C.split(/\s/),function(){B[this]=true
});
C=B
}return C
},addMethod:function(B,D,C){A.validator.methods[B]=D;
A.validator.messages[B]=C!==undefined?C:A.validator.messages[B];
if(D.length<3){A.validator.addClassRules(B,A.validator.normalizeRule(B))
}},methods:{required:function(C,B,E){if(!this.depend(E,B)){return"dependency-mismatch"
}if(B.nodeName.toLowerCase()==="select"){var D=A(B).val();
return D&&D.length>0
}if(this.checkable(B)){return this.getLength(C,B)>0
}return A.trim(C).length>0
},remote:function(F,C,G){if(this.optional(C)){return"dependency-mismatch"
}var D=this.previousValue(C);
if(!this.settings.messages[C.name]){this.settings.messages[C.name]={}
}D.originalMessage=this.settings.messages[C.name].remote;
this.settings.messages[C.name].remote=D.message;
G=typeof G==="string"&&{url:G}||G;
if(this.pending[C.name]){return"pending"
}if(D.old===F){return D.valid
}D.old=F;
var B=this;
this.startRequest(C);
var E={};
E[C.name]=F;
A.ajax(A.extend(true,{url:G,mode:"abort",port:"validate"+C.name,dataType:"json",data:E,success:function(I){B.settings.messages[C.name].remote=D.originalMessage;
var K=I===true||I==="true";
if(K){var H=B.formSubmitted;
B.prepareElement(C);
B.formSubmitted=H;
B.successList.push(C);
delete B.invalid[C.name];
B.showErrors()
}else{var L={};
var J=I||B.defaultMessage(C,"remote");
L[C.name]=D.message=A.isFunction(J)?J(F):J;
B.invalid[C.name]=true;
B.showErrors(L)
}D.valid=K;
B.stopRequest(C,K)
}},G));
return"pending"
},minlength:function(D,B,E){var C=A.isArray(D)?D.length:this.getLength(A.trim(D),B);
return this.optional(B)||C>=E
},maxlength:function(D,B,E){var C=A.isArray(D)?D.length:this.getLength(A.trim(D),B);
return this.optional(B)||C<=E
},rangelength:function(D,B,E){var C=A.isArray(D)?D.length:this.getLength(A.trim(D),B);
return this.optional(B)||(C>=E[0]&&C<=E[1])
},min:function(C,B,D){return this.optional(B)||C>=D
},max:function(C,B,D){return this.optional(B)||C<=D
},range:function(C,B,D){return this.optional(B)||(C>=D[0]&&C<=D[1])
},email:function(C,B){return this.optional(B)||/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(C)
},url:function(C,B){return this.optional(B)||/^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(C)
},date:function(C,B){return this.optional(B)||!/Invalid|NaN/.test(new Date(C))
},dateISO:function(C,B){return this.optional(B)||/^\d{4}[\/\-]\d{1,2}[\/\-]\d{1,2}$/.test(C)
},number:function(C,B){return this.optional(B)||/^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(C)
},digits:function(C,B){return this.optional(B)||/^\d+$/.test(C)
},creditcard:function(F,C){if(this.optional(C)){return"dependency-mismatch"
}if(/[^0-9 \-]+/.test(F)){return false
}var G=0,E=0,B=false;
F=F.replace(/\D/g,"");
for(var H=F.length-1;
H>=0;
H--){var D=F.charAt(H);
E=parseInt(D,10);
if(B){if((E*=2)>9){E-=9
}}G+=E;
B=!B
}return(G%10)===0
},equalTo:function(C,B,E){var D=A(E);
if(this.settings.onfocusout){D.unbind(".validate-equalTo").bind("blur.validate-equalTo",function(){A(B).valid()
})
}return C===D.val()
}}});
A.format=A.validator.format
}(jQuery));
(function(C){var A={};
if(C.ajaxPrefilter){C.ajaxPrefilter(function(F,E,G){var D=F.port;
if(F.mode==="abort"){if(A[D]){A[D].abort()
}A[D]=G
}})
}else{var B=C.ajax;
C.ajax=function(E){var F=("mode" in E?E:C.ajaxSettings).mode,D=("port" in E?E:C.ajaxSettings).port;
if(F==="abort"){if(A[D]){A[D].abort()
}return(A[D]=B.apply(this,arguments))
}return B.apply(this,arguments)
}
}}(jQuery));
(function(A){if(!jQuery.event.special.focusin&&!jQuery.event.special.focusout&&document.addEventListener){A.each({focus:"focusin",blur:"focusout"},function(C,B){A.event.special[B]={setup:function(){this.addEventListener(C,D,true)
},teardown:function(){this.removeEventListener(C,D,true)
},handler:function(F){var E=arguments;
E[0]=A.event.fix(F);
E[0].type=B;
return A.event.handle.apply(this,E)
}};
function D(E){E=A.event.fix(E);
E.type=B;
return A.event.handle.call(this,E)
}})
}A.extend(A.fn,{validateDelegate:function(D,C,B){return this.bind(C,function(E){var F=A(E.target);
if(F.is(D)){return B.apply(F,arguments)
}})
}})
}(jQuery));
(function(A){var B;
var C=function(H,D,E){var I=this,F=A(H),J={byPassKeys:[8,9,37,38,39,40],maskChars:{":":":","-":"-",".":"\\.","(":"\\(",")":"\\)","/":"/",",":",",_:"_"," ":"\\s","+":"\\+"},translationNumbers:{0:"\\d",1:"\\d",2:"\\d",3:"\\d",4:"\\d",5:"\\d",6:"\\d",7:"\\d",8:"\\d",9:"\\d"},translation:{A:"[a-zA-Z0-9]",S:"[a-zA-Z]"}};
I.init=function(){I.settings={};
E=E||{};
J.translation=A.extend({},J.translation,J.translationNumbers);
I.settings=A.extend(true,{},J,E);
I.settings.specialChars=A.extend({},I.settings.maskChars,I.settings.translation);
F.each(function(){D=G.resolveDynamicMask(D,A(this).val(),B,A(this),E);
F.attr("maxlength",D.length);
F.attr("autocomplete","off");
G.destroyEvents();
G.setOnKeyUp();
G.setOnPaste()
})
};
var G={resolveDynamicMask:function(K,M,O,N,L){return typeof K=="function"?K(M,O,N,L):K
},onlyNonMaskChars:function(K){A.each(I.settings.maskChars,function(M,L){K=K.replace(new RegExp("("+I.settings.maskChars[M]+")?","g"),"")
});
return K
},onPasteMethod:function(){setTimeout(function(){F.trigger("keyup")
},100)
},setOnPaste:function(){(G.hasOnSupport())?F.on("paste",G.onPasteMethod):F.onpaste=G.onPasteMethod
},setOnKeyUp:function(){F.keyup(G.maskBehaviour).trigger("keyup")
},hasOnSupport:function(){return A.isFunction(A.on)
},destroyEvents:function(){F.unbind("keyup").unbind("onpaste")
},maskBehaviour:function(Q){Q=Q||window.event;
var P=Q.keyCode||Q.which;
if(A.inArray(P,I.settings.byPassKeys)>-1){return true
}var M=G.onlyNonMaskChars(F.val()),L=F.val().length-1,O=F.val()[L],R=G.getMask(M,D);
if(O===D[L]&&I.settings.maskChars[O]){var N=G.cleanBullShit(F.val(),D);
if(N!==""){N+=O
}F.val(N);
return true
}var K=G.applyMask(Q,F,R,E);
if(K!==F.val()){F.val(K).trigger("change")
}return G.seekCallbacks(Q,E,K,D,F)
},applyMask:function(O,N,K,M){if(K===""){return 
}var L=G.cleanBullShit(G.onlyNonMaskChars(N.val()).substring(0,G.onlyNonMaskChars(K).length),K),P=new RegExp(G.maskToRegex(G.getMask(L,K)));
return L.replace(P,function(){for(var R=1,Q="";
R<arguments.length-2;
R++){if(typeof arguments[R]==="undefined"||arguments[R]===""){arguments[R]=K.charAt(R-1)
}Q+=arguments[R]
}return Q
})
},getMask:function(L,K){return(typeof E.reverse=="boolean"&&E.reverse===true)?G.getProportionalReverseMask(L,K):G.getProportionalMask(L,K)
},getProportionalMask:function(M,L){var N=0,K=0;
while(K<=M.length-1){while(I.settings.maskChars[L.charAt(N)]){N++
}N++;
K++
}return L.substring(0,N)
},getProportionalReverseMask:function(M,L){var P=0,O=0,K=0,N=L.length;
P=(N>=1)?N:N-1;
O=P;
while(K<=M.length-1){while(I.settings.maskChars[L.charAt(O-1)]){O--
}O--;
K++
}O=(L.length>=1)?O:O-1;
return L.substring(P,O)
},maskToRegex:function(K){for(var L=0,M="";
L<K.length;
L++){var N=I.settings.specialChars[K.charAt(L)];
if(N){M+="("+N+")"
}if(I.settings.maskChars[K.charAt(L)]){M+="?"
}}return M
},validDigit:function(K,M){var L="("+I.settings.specialChars[K]+")";
if(I.settings.maskChars[K]){L+="?"
}return new RegExp(L).test(M)
},cleanBullShit:function(L,M,O){O=O||0;
L=L.split("");
for(var P=O,K=O,Q=M.length,N=L.length;
P<Q;
P++,K++){while(I.settings.maskChars[M.charAt(K)]){K++
}if(!G.validDigit(M.charAt(K),L[P])&&typeof L[P]!=="undefined"){L[P]="";
L=L.join("");
return G.cleanBullShit(L,G.getMask(L,M),0)
}}return L.join("")
},seekCallbacks:function(O,M,K,L,N){if(M.onKeyPress&&O.isTrigger===undefined&&typeof M.onKeyPress=="function"){M.onKeyPress(K,O,N,M)
}if(M.onComplete&&O.isTrigger===undefined&&K.length===L.length&&typeof M.onComplete=="function"){M.onComplete(K,O,N,M)
}}};
if(typeof QUNIT==="boolean"){I.__p=G
}I.remove=function(){G.destroyEvents();
F.val(G.onlyNonMaskChars(F.val()))
};
I.init()
};
A.fn.mask=function(D,E){return this.each(function(){A(this).data("mask",new C(this,D,E))
})
}
})(jQuery);
/* http://mths.be/placeholder v2.0.7 by @mathias */
(function(G,I,D){var A="placeholder" in I.createElement("input"),E="placeholder" in I.createElement("textarea"),J=D.fn,C=D.valHooks,L,K;
if(A&&E){K=J.placeholder=function(){return this
};
K.input=K.textarea=true
}else{K=J.placeholder=function(){var M=this;
M.filter((A?"textarea":":input")+"[placeholder]").not(".placeholder").bind({"focus.placeholder":B,"blur.placeholder":F}).data("placeholder-enabled",true).trigger("blur.placeholder");
return M
};
K.input=A;
K.textarea=E;
L={get:function(N){var M=D(N);
return M.data("placeholder-enabled")&&M.hasClass("placeholder")?"":N.value
},set:function(N,O){var M=D(N);
if(!M.data("placeholder-enabled")){return N.value=O
}if(O==""){N.value=O;
if(N!=I.activeElement){F.call(N)
}}else{if(M.hasClass("placeholder")){B.call(N,true,O)||(N.value=O)
}else{N.value=O
}}return M
}};
A||(C.input=L);
E||(C.textarea=L);
D(function(){D(I).delegate("form","submit.placeholder",function(){var M=D(".placeholder",this).each(B);
setTimeout(function(){M.each(F)
},10)
})
});
D(G).bind("beforeunload.placeholder",function(){D(".placeholder").each(function(){this.value=""
})
})
}function H(N){var M={},O=/^jQuery\d+$/;
D.each(N.attributes,function(Q,P){if(P.specified&&!O.test(P.name)){M[P.name]=P.value
}});
return M
}function B(N,O){var M=this,P=D(M);
if(M.value==P.attr("placeholder")&&P.hasClass("placeholder")){if(P.data("placeholder-password")){P=P.hide().next().show().attr("id",P.removeAttr("id").data("placeholder-id"));
if(N===true){return P[0].value=O
}P.focus()
}else{M.value="";
P.removeClass("placeholder");
M==I.activeElement&&M.select()
}}}function F(){var R,M=this,Q=D(M),N=Q,P=this.id;
if(M.value==""){if(M.type=="password"){if(!Q.data("placeholder-textinput")){try{R=Q.clone().attr({type:"text"})
}catch(O){R=D("<input>").attr(D.extend(H(this),{type:"text"}))
}R.removeAttr("name").data({"placeholder-password":true,"placeholder-id":P}).bind("focus.placeholder",B);
Q.data({"placeholder-textinput":R,"placeholder-id":P}).before(R)
}Q=Q.removeAttr("id").hide().prev().attr("id",P).show()
}Q.addClass("placeholder");
Q[0].value=Q.attr("placeholder")
}else{Q.removeClass("placeholder")
}}}(this,document,jQuery));
(function(G){G.fn.adGallery=function(J,M){var L={loader_image:"main/images/loader.gif"/*tpa=http://www.usfoods.com/etc/designs/usfoods/clientlibs/main/images/loader.gif*/,start_at_index:0,description_wrapper:false,thumb_opacity:0.7,animate_first_image:false,animation_speed:400,width:false,height:false,display_next_and_prev:true,display_back_and_forward:true,scroll_jump:0,slideshow:{enable:true,autostart:true,speed:M,start_label:"",stop_label:"",stop_on_scroll:true,countdown_prefix:"",countdown_sufix:"",onStart:false,onStop:false},effect:"slide-hori",enable_keyboard_move:true,cycle:true,callbacks:{init:false,afterImageVisible:false,beforeImageVisible:false}};
var K=G.extend(false,L,J);
if(J&&J.slideshow){K.slideshow=G.extend(false,L.slideshow,J.slideshow)
}if(!K.slideshow.enable){K.slideshow.autostart=false
}var I=[];
G(this).each(function(){var N=new C(this,K);
I[I.length]=N
});
return I
};
function F(J,K,L){var M=parseInt(J.css("top"),10);
if(K=="left"){var I="-"+this.image_wrapper_height+"px";
J.css("top",this.image_wrapper_height+"px")
}else{var I=this.image_wrapper_height+"px";
J.css("top","-"+this.image_wrapper_height+"px")
}if(L){L.css("bottom","-"+L[0].offsetHeight+"px");
L.animate({bottom:0},this.settings.animation_speed*2)
}if(this.current_description){this.current_description.animate({bottom:"-"+this.current_description[0].offsetHeight+"px"},this.settings.animation_speed*2)
}return{old_image:{top:I},new_image:{top:M}}
}function E(J,K,M){var N=G(this).find(".thumbNails img[title]");
N.tooltip("hide");
var L=parseInt(J.css("left"),10);
if(K=="left"){var I="-"+this.image_wrapper_width+"px";
J.css("left",this.image_wrapper_width+"px")
}else{var I=this.image_wrapper_width+"px";
J.css("left","-"+this.image_wrapper_width+"px")
}if(M){M.css("bottom","-"+M[0].offsetHeight+"px");
M.animate({bottom:0},this.settings.animation_speed*2)
}if(this.current_description){this.current_description.animate({bottom:"-"+this.current_description[0].offsetHeight+"px"},this.settings.animation_speed*2)
}N.tooltip("show");
return{old_image:{left:I},new_image:{left:L}}
}function D(K,L,N){var J=K.width();
var I=K.height();
var M=parseInt(K.css("left"),10);
var O=parseInt(K.css("top"),10);
K.css({width:0,height:0,top:this.image_wrapper_height/2,left:this.image_wrapper_width/2});
return{old_image:{width:0,height:0,top:this.image_wrapper_height/2,left:this.image_wrapper_width/2},new_image:{width:J,height:I,top:O,left:M}}
}function B(I,J,K){I.css("opacity",0);
return{old_image:{opacity:0},new_image:{opacity:1}}
}function H(I,J,K){I.css("opacity",0);
return{old_image:{opacity:0},new_image:{opacity:1},speed:0}
}function C(J,I){this.init(J,I)
}C.prototype={wrapper:false,image_wrapper:false,gallery_info:false,nav:false,loader:false,preloads:false,thumbs_wrapper:false,scroll_back:false,scroll_forward:false,next_link:false,prev_link:false,slideshow:false,image_wrapper_width:0,image_wrapper_height:0,current_index:0,current_image:false,current_description:false,nav_display_width:0,settings:false,images:false,in_transition:false,animations:false,init:function(M,L){var K=this;
this.wrapper=G(M);
this.settings=L;
this.setupElements();
this.setupAnimations();
if(this.settings.width){this.image_wrapper_width=this.settings.width;
this.image_wrapper.width(this.settings.width);
this.wrapper.width(this.settings.width)
}else{this.image_wrapper_width=this.image_wrapper.width()
}if(this.settings.height){this.image_wrapper_height=this.settings.height;
this.image_wrapper.height(this.settings.height)
}else{this.image_wrapper_height=this.image_wrapper.height()
}this.nav_display_width=800;
this.current_index=0;
this.current_image=false;
this.current_description=false;
this.in_transition=false;
this.findImages();
if(this.settings.display_next_and_prev){this.initNextAndPrev()
}var I=function(N){return K.nextImage(N)
};
this.slideshow=new A(I,this.settings.slideshow);
this.controls.append(this.slideshow.create());
if(this.settings.slideshow.enable){this.slideshow.enable()
}else{this.slideshow.disable()
}if(this.settings.display_back_and_forward){this.initBackAndForward()
}if(this.settings.enable_keyboard_move){this.initKeyEvents()
}var J=parseInt(this.settings.start_at_index,10);
if(window.location.hash&&window.location.hash.indexOf("#ad-image")===0){J=window.location.hash.replace(/[^0-9]+/g,"");
if((J*1)!=J){J=this.settings.start_at_index
}}this.loading(true);
this.showImage(J,function(){if(K.settings.slideshow.autostart){K.preloadImage(J+1);
K.slideshow.start()
}});
this.fireCallback(this.settings.callbacks.init)
},setupAnimations:function(){this.animations={"slide-vert":F,"slide-hori":E,resize:D,fade:B,none:H}
},setupElements:function(){this.controls=this.wrapper.find(".ad-controls");
this.gallery_info=G('<p class="ad-info"></p>');
this.controls.append(this.gallery_info);
this.image_wrapper=this.wrapper.find(".ad-image-wrapper");
this.image_wrapper.empty();
this.nav=this.wrapper.find(".ad-nav");
this.thumbs_wrapper=this.nav.find(".ad-thumbs");
this.preloads=G('<div class="ad-preloads"></div>');
this.loader=G('<div class="ad-loader"></div>');
this.image_wrapper.append(this.loader);
this.loader.hide();
G(document.body).append(this.preloads)
},loading:function(I){if(I){this.loader.show()
}else{this.loader.hide()
}},addAnimation:function(I,J){if(G.isFunction(J)){this.animations[I]=J
}},findImages:function(){var N=this;
this.images=[];
var M=0;
var L=0;
var K=this.thumbs_wrapper.find("a");
var J=K.length;
if(this.settings.thumb_opacity<1){K.find("img").css("opacity",this.settings.thumb_opacity)
}K.each(function(P){var R=G(this);
var Q=R.attr("href");
var O=R.find("img");
if(!N.isImageLoaded(O[0])){O.load(function(){L++
})
}else{L++
}R.addClass("ad-thumb"+P);
R.click(function(){if(G(this).hasClass("ad-active")){return false
}N.showImage(P);
N.slideshow.stop();
return false
}).hover(function(){if(!G(this).is(".ad-active")&&N.settings.thumb_opacity<1){G(this).find("img").fadeTo(300,1)
}N.preloadImage(P)
},function(){if(!G(this).is(".ad-active")&&N.settings.thumb_opacity<1){G(this).find("img").fadeTo(300,N.settings.thumb_opacity)
}});
var R=false;
if(O.data("ad-link")){R=O.data("ad-link")
}else{if(O.attr("longdesc")&&O.attr("longdesc").length){R=O.attr("longdesc")
}}var T=false;
if(O.data("ad-desc")){T=O.data("ad-desc")
}else{if(O.attr("alt")&&O.attr("alt").length){T=O.attr("alt")
}}var S=false;
N.images[P]={thumb:O.attr("src"),image:Q,error:false,preloaded:false,desc:T,title:S,size:false,link:R}
});
var I=setInterval(function(){if(J==L){M=J*190;
var Q=N.nav.find(".ad-thumb-list");
Q.css("width",M+"px");
var P=1;
var O=Q.height();
while(P<201){Q.css("width",(M+P)+"px");
if(O!=Q.height()){break
}O=Q.height();
P++
}clearInterval(I)
}},100)
},initKeyEvents:function(){var I=this;
G(document).keydown(function(J){if(J.keyCode==39){I.nextImage();
I.slideshow.stop()
}else{if(J.keyCode==37){I.prevImage();
I.slideshow.stop()
}}})
},initNextAndPrev:function(){this.next_link=G('<div class="ad-next"><div class="ad-next-image"></div></div>');
this.prev_link=G('<div class="ad-prev"><div class="ad-prev-image"></div></div>');
this.image_wrapper.append(this.next_link);
this.image_wrapper.append(this.prev_link);
var I=this;
this.prev_link.add(this.next_link).mouseover(function(J){G(this).css("height",I.image_wrapper_height);
G(this).find("div").show()
}).mouseout(function(J){G(this).find("div").hide()
}).click(function(){if(G(this).is(".ad-next")){I.nextImage();
I.slideshow.stop()
}else{I.prevImage();
I.slideshow.stop()
}}).find("div").css("opacity",0.7)
},initBackAndForward:function(){var K=this;
this.scroll_forward=G('<div class="ad-forward"></div>');
this.scroll_back=G('<div class="ad-back"></div>');
this.nav.append(this.scroll_forward);
this.nav.prepend(this.scroll_back);
var J=0;
var I=false;
G(this.scroll_back).add(this.scroll_forward).click(function(){var L=K.nav_display_width-50;
if(K.settings.scroll_jump>0){var L=K.settings.scroll_jump
}if(G(this).is(".ad-forward")){var M=K.thumbs_wrapper.scrollLeft()+L
}else{var M=K.thumbs_wrapper.scrollLeft()-L
}if(K.settings.slideshow.stop_on_scroll){K.slideshow.stop()
}K.thumbs_wrapper.animate({scrollLeft:M+"px"});
return false
}).css("opacity",0.6).hover(function(){var L="left";
if(G(this).is(".ad-forward")){L="right"
}I=setInterval(function(){J++;
if(J>30&&K.settings.slideshow.stop_on_scroll){K.slideshow.stop()
}var M=K.thumbs_wrapper.scrollLeft()+1;
if(L=="left"){M=K.thumbs_wrapper.scrollLeft()-1
}K.thumbs_wrapper.scrollLeft(M)
},10);
G(this).css("opacity",1)
},function(){J=0;
clearInterval(I);
G(this).css("opacity",0.6)
})
},_afterShow:function(){if(!this.settings.cycle){this.prev_link.show().css("height",this.image_wrapper_height);
this.next_link.show().css("height",this.image_wrapper_height);
if(this.current_index==(this.images.length-1)){this.next_link.hide()
}if(this.current_index==0){this.prev_link.hide()
}}this.fireCallback(this.settings.callbacks.afterImageVisible)
},_getContainedImageSize:function(J,I){if(I>this.image_wrapper_height){var K=J/I;
I=this.image_wrapper_height;
J=this.image_wrapper_height*K
}if(J>this.image_wrapper_width){var K=I/J;
J=this.image_wrapper_width;
I=this.image_wrapper_width*K
}return{width:J,height:I}
},_centerImage:function(L,J,I){L.css("top","0px");
if(I<this.image_wrapper_height){var K=this.image_wrapper_height-I;
L.css("top",(K/2)+"px")
}L.css("left","0px");
if(J<this.image_wrapper_width){var K=this.image_wrapper_width-J;
L.css("left",(K/2)+"px")
}},_getDescription:function(I){var K=false;
if(I.desc.length||I.title.length){var J="";
if(I.title.length){J='<strong class="ad-description-title">'+I.title+"</strong>"
}var K="";
if(I.desc.length){K="<span>"+I.desc+"</span>"
}K=G('<p class="ad-image-description">'+J+K+"</p>")
}return K
},showImage:function(I,L){if(this.images[I]&&!this.in_transition){var J=this;
var K=this.images[I];
this.in_transition=true;
if(!K.preloaded){this.loading(true);
this.preloadImage(I,function(){J.loading(false);
J._showWhenLoaded(I,L)
})
}else{this._showWhenLoaded(I,L)
}}},_showWhenLoaded:function(Q,W){if(this.images[Q]){var J=this;
var L=this.images[Q];
var M=G(document.createElement("div")).addClass("ad-image");
var O=G(new Image()).attr("src",L.image);
if(L.link){var T=G('<a href="'+L.link+'" target="_self"></a>');
T.append(O);
M.append(T)
}else{M.append(O)
}this.image_wrapper.prepend(M);
var X=this._getContainedImageSize(L.size.width,L.size.height);
O.attr("width",X.width);
O.attr("height",X.height);
M.css({width:X.width+"px",height:X.height+"px"});
this._centerImage(M,X.width,X.height);
var P=this._getDescription(L,M);
if(P){if(!this.settings.description_wrapper){M.append(P);
var I=X.width-parseInt(P.css("padding-left"),10)-parseInt(P.css("padding-right"),10);
P.css("width",I+"px")
}else{this.settings.description_wrapper.append(P)
}}this.highLightThumb(this.nav.find(".ad-thumb"+Q));
var U="right";
if(this.current_index<Q){U="left"
}this.fireCallback(this.settings.callbacks.beforeImageVisible);
if(this.current_image||this.settings.animate_first_image){var V=this.settings.animation_speed;
var S="swing";
var N=this.animations[this.settings.effect].call(this,M,U,P);
if(typeof N.speed!="undefined"){V=N.speed
}if(typeof N.easing!="undefined"){S=N.easing
}if(this.current_image){var K=this.current_image;
var R=this.current_description;
K.animate(N.old_image,V,S,function(){K.remove();
if(R){R.remove()
}})
}M.animate(N.new_image,V,S,function(){J.current_index=Q;
J.current_image=M;
J.current_description=P;
J.in_transition=false;
J._afterShow();
J.fireCallback(W)
})
}else{this.current_index=Q;
this.current_image=M;
J.current_description=P;
this.in_transition=false;
J._afterShow();
this.fireCallback(W)
}}},nextIndex:function(){if(this.current_index==(this.images.length-1)){if(!this.settings.cycle){return false
}var I=0
}else{var I=this.current_index+1
}return I
},nextImage:function(J){var I=this.nextIndex();
if(I===false){return false
}this.preloadImage(I+1);
this.showImage(I,J);
return true
},prevIndex:function(){if(this.current_index==0){if(!this.settings.cycle){return false
}var I=this.images.length-1
}else{var I=this.current_index-1
}return I
},prevImage:function(J){var I=this.prevIndex();
if(I===false){return false
}this.preloadImage(I-1);
this.showImage(I,J);
return true
},preloadAll:function(){var J=this;
var I=0;
function K(){if(I<J.images.length){I++;
J.preloadImage(I,K)
}}J.preloadImage(I,K)
},preloadImage:function(J,M){if(this.images[J]){var L=this.images[J];
if(!this.images[J].preloaded){var I=G(new Image());
I.attr("src",L.image);
if(!this.isImageLoaded(I[0])){this.preloads.append(I);
var K=this;
I.load(function(){L.preloaded=true;
L.size={width:this.width,height:this.height};
K.fireCallback(M)
}).error(function(){L.error=true;
L.preloaded=false;
L.size=false
})
}else{L.preloaded=true;
L.size={width:I[0].width,height:I[0].height};
this.fireCallback(M)
}}else{this.fireCallback(M)
}}},isImageLoaded:function(I){if(typeof I.complete!="undefined"&&!I.complete){return false
}if(typeof I.naturalWidth!="undefined"&&I.naturalWidth==0){return false
}return true
},highLightThumb:function(I){this.thumbs_wrapper.find(".ad-active").removeClass("ad-active");
I.addClass("ad-active");
if(this.settings.thumb_opacity<1){this.thumbs_wrapper.find("a:not(.ad-active) img").fadeTo(300,this.settings.thumb_opacity);
I.find("img").fadeTo(300,1)
}var J=I[0].parentNode.offsetLeft;
J-=(this.nav_display_width/2)-(I[0].offsetWidth/2);
this.thumbs_wrapper.animate({scrollLeft:J+"px"})
},fireCallback:function(I){if(G.isFunction(I)){I.call(this)
}}};
function A(I,J){this.init(I,J)
}A.prototype={start_link:false,stop_link:false,countdown:false,controls:false,settings:false,nextimage_callback:false,enabled:false,running:false,countdown_interval:false,init:function(I,K){var J=this;
this.nextimage_callback=I;
this.settings=K
},create:function(){this.start_link=G('<span class="ad-slideshow-start">'+this.settings.start_label+"</span>");
this.stop_link=G('<span class="ad-slideshow-stop">'+this.settings.stop_label+"</span>");
this.countdown=G('<span class="ad-slideshow-countdown"></span>');
this.controls=G('<div class="ad-slideshow-controls"></div>');
this.controls.append(this.start_link).append(this.stop_link).append(this.countdown);
this.countdown.hide();
var I=this;
this.start_link.click(function(){I.start()
});
this.stop_link.click(function(){I.stop()
});
G(document).keydown(function(J){if(J.keyCode==83){if(I.running){I.stop()
}else{I.start()
}}});
return this.controls
},disable:function(){this.enabled=false;
this.stop();
this.controls.hide()
},enable:function(){this.enabled=true;
this.controls.show()
},toggle:function(){if(this.enabled){this.disable()
}else{this.enable()
}},start:function(){if(this.running||!this.enabled){return false
}var I=this;
this.running=true;
this.controls.addClass("ad-slideshow-running");
this._next();
this.fireCallback(this.settings.onStart);
return true
},stop:function(){if(!this.running){return false
}this.running=false;
this.countdown.hide();
this.controls.removeClass("ad-slideshow-running");
clearInterval(this.countdown_interval);
this.fireCallback(this.settings.onStop);
return true
},_next:function(){var K=this;
var L=this.settings.countdown_prefix;
var J=this.settings.countdown_sufix;
clearInterval(K.countdown_interval);
var I=0;
this.countdown_interval=setInterval(function(){I+=1000;
if(I>=K.settings.speed){var N=function(){if(K.running){K._next()
}I=0
};
if(!K.nextimage_callback(N)){K.stop()
}I=0
}var M=parseInt(K.countdown.text().replace(/[^0-9]/g,""),10);
M--;
if(M>0){}},1000)
},fireCallback:function(I){if(G.isFunction(I)){I.call(this)
}}}
})(jQuery);
/*
* jQuery Tools v1.2.6 - The missing UI library for the Web
* 
* tooltip/tooltip.js
* 
* NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
* 
* http://flowplayer.org/tools/
* 
*/
(function(B){B.tools=B.tools||{version:"v1.2.6"},B.tools.tooltip={conf:{effect:"toggle",fadeOutSpeed:"fast",predelay:0,delay:30,opacity:1,tip:0,fadeIE:!1,position:["top","center"],offset:[0,0],relative:!1,cancelDefault:!0,events:{def:"mouseenter,mouseleave",input:"focus,blur",widget:"focus mouseenter,blur mouseleave",tooltip:"mouseenter,mouseleave"},layout:"<div/>",tipClass:"tooltip"},addEffect:function(E,G,F){A[E]=[G,F]
}};
var A={toggle:[function(F){var E=this.getConf(),H=this.getTip(),G=E.opacity;
G<1&&H.css({opacity:G}),H.show(),F.call()
},function(E){this.getTip().hide(),E.call()
}],fade:[function(E){var F=this.getConf();
!B.browser.msie||F.fadeIE?this.getTip().fadeTo(F.fadeInSpeed,F.opacity,E):(this.getTip().show(),E())
},function(E){var F=this.getConf();
!B.browser.msie||F.fadeIE?this.getTip().fadeOut(F.fadeOutSpeed,E):(this.getTip().hide(),E())
}]};
function D(E,L,K){var J=K.relative?E.position().top:E.offset().top,I=K.relative?E.position().left:E.offset().left,H=K.position[0];
J-=L.outerHeight()-K.offset[0],I+=E.outerWidth()+K.offset[1],/iPad/i.test(navigator.userAgent)&&(J-=B(window).scrollTop());
var G=L.outerHeight()+E.outerHeight();
H=="center"&&(J+=G/2),H=="bottom"&&(J+=G),H=K.position[1];
var F=L.outerWidth()+E.outerWidth();
H=="center"&&(I-=F/2),H=="left"&&(I-=F);
return{top:J,left:I}
}function C(S,R){var Q=this,P=S.add(Q),O,N=0,M=0,L=S.attr("title"),K=S.attr("data-tooltip"),J=A[R.effect],I,H=S.is(":input"),G=H&&S.is(":checkbox, :radio, select, :button, :submit"),F=S.attr("type"),E=R.events[F]||R.events[H?G?"widget":"input":"def"];
if(!J){throw'Nonexistent effect "'+R.effect+'"'
}E=E.split(/,\s*/);
if(E.length!=2){throw"Tooltip: bad events configuration for "+F
}S.bind(E[0],function(T){clearTimeout(N),R.predelay?M=setTimeout(function(){Q.show(T)
},R.predelay):Q.show(T)
}).bind(E[1],function(T){clearTimeout(M),R.delay?N=setTimeout(function(){Q.hide(T)
},R.delay):Q.hide(T)
}),L&&R.cancelDefault&&(S.removeAttr("title"),S.data("title",L)),B.extend(Q,{show:function(T){if(!O){K?O=B(K):R.tip?O=B(R.tip).eq(0):L?O=B(R.layout).addClass(R.tipClass).appendTo(document.body).hide().append(L):(O=S.next(),O.length||(O=S.parent().next()));
if(!O.length){throw"Cannot find tooltip for "+S
}}if(Q.isShown()){return Q
}O.stop(!0,!0);
var V=D(S,O,R);
R.tip&&O.html(S.data("title")),T=B.Event(),T.type="onBeforeShow",P.trigger(T,[V]);
if(T.isDefaultPrevented()){return Q
}V=D(S,O,R),O.css({position:"absolute",top:V.top,left:V.left}),I=!0,J[0].call(Q,function(){T.type="onShow",I="full",P.trigger(T)
});
var U=R.events.tooltip.split(/,\s*/);
O.data("__set")||(O.unbind(U[0]).bind(U[0],function(){clearTimeout(N),clearTimeout(M)
}),U[1]&&!S.is("input:not(:checkbox, :radio), textarea")&&O.unbind(U[1]).bind(U[1],function(W){W.relatedTarget!=S[0]&&S.trigger(E[1].split(" ")[0])
}),R.tip||O.data("__set",!0));
return Q
},hide:function(T){if(!O||!Q.isShown()){return Q
}T=B.Event(),T.type="onBeforeHide",P.trigger(T);
if(!T.isDefaultPrevented()){I=!1,A[R.effect][1].call(Q,function(){T.type="onHide",P.trigger(T)
});
return Q
}},isShown:function(T){return T?I=="full":I
},getConf:function(){return R
},getTip:function(){return O
},getTrigger:function(){return S
}}),B.each("onHide,onBeforeShow,onShow,onBeforeHide".split(","),function(T,U){B.isFunction(R[U])&&B(Q).bind(U,R[U]),Q[U]=function(V){V&&B(Q).bind(U,V);
return Q
}
})
}B.fn.tooltip=function(E){var F=this.data("tooltip");
if(F){return F
}E=B.extend(!0,{},B.tools.tooltip.conf,E),typeof E.position=="string"&&(E.position=E.position.split(/,?\s/)),this.each(function(){F=new C(B(this),E),B(this).data("tooltip",F)
});
return E.api?F:this
}
})(jQuery);
(function(window,document,undefined){(function(factory){if(typeof define==="function"&&define.amd){define(["jquery"],factory)
}else{if(jQuery&&!jQuery.fn.dataTable){factory(jQuery)
}}}(function($){var DataTable=function(oInit){function _fnAddColumn(oSettings,nTh){var oDefaults=DataTable.defaults.columns;
var iCol=oSettings.aoColumns.length;
var oCol=$.extend({},DataTable.models.oColumn,oDefaults,{sSortingClass:oSettings.oClasses.sSortable,sSortingClassJUI:oSettings.oClasses.sSortJUI,nTh:nTh?nTh:document.createElement("th"),sTitle:oDefaults.sTitle?oDefaults.sTitle:nTh?nTh.innerHTML:"",aDataSort:oDefaults.aDataSort?oDefaults.aDataSort:[iCol],mData:oDefaults.mData?oDefaults.oDefaults:iCol});
oSettings.aoColumns.push(oCol);
if(oSettings.aoPreSearchCols[iCol]===undefined||oSettings.aoPreSearchCols[iCol]===null){oSettings.aoPreSearchCols[iCol]=$.extend({},DataTable.models.oSearch)
}else{var oPre=oSettings.aoPreSearchCols[iCol];
if(oPre.bRegex===undefined){oPre.bRegex=true
}if(oPre.bSmart===undefined){oPre.bSmart=true
}if(oPre.bCaseInsensitive===undefined){oPre.bCaseInsensitive=true
}}_fnColumnOptions(oSettings,iCol,null)
}function _fnColumnOptions(oSettings,iCol,oOptions){var oCol=oSettings.aoColumns[iCol];
if(oOptions!==undefined&&oOptions!==null){if(oOptions.mDataProp&&!oOptions.mData){oOptions.mData=oOptions.mDataProp
}if(oOptions.sType!==undefined){oCol.sType=oOptions.sType;
oCol._bAutoType=false
}$.extend(oCol,oOptions);
_fnMap(oCol,oOptions,"sWidth","sWidthOrig");
if(oOptions.iDataSort!==undefined){oCol.aDataSort=[oOptions.iDataSort]
}_fnMap(oCol,oOptions,"aDataSort")
}var mRender=oCol.mRender?_fnGetObjectDataFn(oCol.mRender):null;
var mData=_fnGetObjectDataFn(oCol.mData);
oCol.fnGetData=function(oData,sSpecific){var innerData=mData(oData,sSpecific);
if(oCol.mRender&&(sSpecific&&sSpecific!=="")){return mRender(innerData,sSpecific,oData)
}return innerData
};
oCol.fnSetData=_fnSetObjectDataFn(oCol.mData);
if(!oSettings.oFeatures.bSort){oCol.bSortable=false
}if(!oCol.bSortable||($.inArray("asc",oCol.asSorting)==-1&&$.inArray("desc",oCol.asSorting)==-1)){oCol.sSortingClass=oSettings.oClasses.sSortableNone;
oCol.sSortingClassJUI=""
}else{if($.inArray("asc",oCol.asSorting)==-1&&$.inArray("desc",oCol.asSorting)==-1){oCol.sSortingClass=oSettings.oClasses.sSortable;
oCol.sSortingClassJUI=oSettings.oClasses.sSortJUI
}else{if($.inArray("asc",oCol.asSorting)!=-1&&$.inArray("desc",oCol.asSorting)==-1){oCol.sSortingClass=oSettings.oClasses.sSortableAsc;
oCol.sSortingClassJUI=oSettings.oClasses.sSortJUIAscAllowed
}else{if($.inArray("asc",oCol.asSorting)==-1&&$.inArray("desc",oCol.asSorting)!=-1){oCol.sSortingClass=oSettings.oClasses.sSortableDesc;
oCol.sSortingClassJUI=oSettings.oClasses.sSortJUIDescAllowed
}}}}}function _fnAdjustColumnSizing(oSettings){if(oSettings.oFeatures.bAutoWidth===false){return false
}_fnCalculateColumnWidths(oSettings);
for(var i=0,iLen=oSettings.aoColumns.length;
i<iLen;
i++){oSettings.aoColumns[i].nTh.style.width=oSettings.aoColumns[i].sWidth
}}function _fnVisibleToColumnIndex(oSettings,iMatch){var aiVis=_fnGetColumns(oSettings,"bVisible");
return typeof aiVis[iMatch]==="number"?aiVis[iMatch]:null
}function _fnColumnIndexToVisible(oSettings,iMatch){var aiVis=_fnGetColumns(oSettings,"bVisible");
var iPos=$.inArray(iMatch,aiVis);
return iPos!==-1?iPos:null
}function _fnVisbleColumns(oSettings){return _fnGetColumns(oSettings,"bVisible").length
}function _fnGetColumns(oSettings,sParam){var a=[];
$.map(oSettings.aoColumns,function(val,i){if(val[sParam]){a.push(i)
}});
return a
}function _fnDetectType(sData){var aTypes=DataTable.ext.aTypes;
var iLen=aTypes.length;
for(var i=0;
i<iLen;
i++){var sType=aTypes[i](sData);
if(sType!==null){return sType
}}return"string"
}function _fnReOrderIndex(oSettings,sColumns){var aColumns=sColumns.split(",");
var aiReturn=[];
for(var i=0,iLen=oSettings.aoColumns.length;
i<iLen;
i++){for(var j=0;
j<iLen;
j++){if(oSettings.aoColumns[i].sName==aColumns[j]){aiReturn.push(j);
break
}}}return aiReturn
}function _fnColumnOrdering(oSettings){var sNames="";
for(var i=0,iLen=oSettings.aoColumns.length;
i<iLen;
i++){sNames+=oSettings.aoColumns[i].sName+","
}if(sNames.length==iLen){return""
}return sNames.slice(0,-1)
}function _fnApplyColumnDefs(oSettings,aoColDefs,aoCols,fn){var i,iLen,j,jLen,k,kLen;
if(aoColDefs){for(i=aoColDefs.length-1;
i>=0;
i--){var aTargets=aoColDefs[i].aTargets;
if(!$.isArray(aTargets)){_fnLog(oSettings,1,"aTargets must be an array of targets, not a "+(typeof aTargets))
}for(j=0,jLen=aTargets.length;
j<jLen;
j++){if(typeof aTargets[j]==="number"&&aTargets[j]>=0){while(oSettings.aoColumns.length<=aTargets[j]){_fnAddColumn(oSettings)
}fn(aTargets[j],aoColDefs[i])
}else{if(typeof aTargets[j]==="number"&&aTargets[j]<0){fn(oSettings.aoColumns.length+aTargets[j],aoColDefs[i])
}else{if(typeof aTargets[j]==="string"){for(k=0,kLen=oSettings.aoColumns.length;
k<kLen;
k++){if(aTargets[j]=="_all"||$(oSettings.aoColumns[k].nTh).hasClass(aTargets[j])){fn(k,aoColDefs[i])
}}}}}}}}if(aoCols){for(i=0,iLen=aoCols.length;
i<iLen;
i++){fn(i,aoCols[i])
}}}function _fnAddData(oSettings,aDataSupplied){var oCol;
var aDataIn=($.isArray(aDataSupplied))?aDataSupplied.slice():$.extend(true,{},aDataSupplied);
var iRow=oSettings.aoData.length;
var oData=$.extend(true,{},DataTable.models.oRow);
oData._aData=aDataIn;
oSettings.aoData.push(oData);
var nTd,sThisType;
for(var i=0,iLen=oSettings.aoColumns.length;
i<iLen;
i++){oCol=oSettings.aoColumns[i];
if(typeof oCol.fnRender==="function"&&oCol.bUseRendered&&oCol.mData!==null){_fnSetCellData(oSettings,iRow,i,_fnRender(oSettings,iRow,i))
}else{_fnSetCellData(oSettings,iRow,i,_fnGetCellData(oSettings,iRow,i))
}if(oCol._bAutoType&&oCol.sType!="string"){var sVarType=_fnGetCellData(oSettings,iRow,i,"type");
if(sVarType!==null&&sVarType!==""){sThisType=_fnDetectType(sVarType);
if(oCol.sType===null){oCol.sType=sThisType
}else{if(oCol.sType!=sThisType&&oCol.sType!="html"){oCol.sType="string"
}}}}}oSettings.aiDisplayMaster.push(iRow);
if(!oSettings.oFeatures.bDeferRender){_fnCreateTr(oSettings,iRow)
}return iRow
}function _fnGatherData(oSettings){var iLoop,i,iLen,j,jLen,jInner,nTds,nTrs,nTd,nTr,aLocalData,iThisIndex,iRow,iRows,iColumn,iColumns,sNodeName,oCol,oData;
if(oSettings.bDeferLoading||oSettings.sAjaxSource===null){nTr=oSettings.nTBody.firstChild;
while(nTr){if(nTr.nodeName.toUpperCase()=="TR"){iThisIndex=oSettings.aoData.length;
nTr._DT_RowIndex=iThisIndex;
oSettings.aoData.push($.extend(true,{},DataTable.models.oRow,{nTr:nTr}));
oSettings.aiDisplayMaster.push(iThisIndex);
nTd=nTr.firstChild;
jInner=0;
while(nTd){sNodeName=nTd.nodeName.toUpperCase();
if(sNodeName=="TD"||sNodeName=="TH"){_fnSetCellData(oSettings,iThisIndex,jInner,$.trim(nTd.innerHTML));
jInner++
}nTd=nTd.nextSibling
}}nTr=nTr.nextSibling
}}nTrs=_fnGetTrNodes(oSettings);
nTds=[];
for(i=0,iLen=nTrs.length;
i<iLen;
i++){nTd=nTrs[i].firstChild;
while(nTd){sNodeName=nTd.nodeName.toUpperCase();
if(sNodeName=="TD"||sNodeName=="TH"){nTds.push(nTd)
}nTd=nTd.nextSibling
}}for(iColumn=0,iColumns=oSettings.aoColumns.length;
iColumn<iColumns;
iColumn++){oCol=oSettings.aoColumns[iColumn];
if(oCol.sTitle===null){oCol.sTitle=oCol.nTh.innerHTML
}var bAutoType=oCol._bAutoType,bRender=typeof oCol.fnRender==="function",bClass=oCol.sClass!==null,bVisible=oCol.bVisible,nCell,sThisType,sRendered,sValType;
if(bAutoType||bRender||bClass||!bVisible){for(iRow=0,iRows=oSettings.aoData.length;
iRow<iRows;
iRow++){oData=oSettings.aoData[iRow];
nCell=nTds[(iRow*iColumns)+iColumn];
if(bAutoType&&oCol.sType!="string"){sValType=_fnGetCellData(oSettings,iRow,iColumn,"type");
if(sValType!==""){sThisType=_fnDetectType(sValType);
if(oCol.sType===null){oCol.sType=sThisType
}else{if(oCol.sType!=sThisType&&oCol.sType!="html"){oCol.sType="string"
}}}}if(oCol.mRender){nCell.innerHTML=_fnGetCellData(oSettings,iRow,iColumn,"display")
}else{if(oCol.mData!==iColumn){nCell.innerHTML=_fnGetCellData(oSettings,iRow,iColumn,"display")
}}if(bRender){sRendered=_fnRender(oSettings,iRow,iColumn);
nCell.innerHTML=sRendered;
if(oCol.bUseRendered){_fnSetCellData(oSettings,iRow,iColumn,sRendered)
}}if(bClass){nCell.className+=" "+oCol.sClass
}if(!bVisible){oData._anHidden[iColumn]=nCell;
nCell.parentNode.removeChild(nCell)
}else{oData._anHidden[iColumn]=null
}if(oCol.fnCreatedCell){oCol.fnCreatedCell.call(oSettings.oInstance,nCell,_fnGetCellData(oSettings,iRow,iColumn,"display"),oData._aData,iRow,iColumn)
}}}}if(oSettings.aoRowCreatedCallback.length!==0){for(i=0,iLen=oSettings.aoData.length;
i<iLen;
i++){oData=oSettings.aoData[i];
_fnCallbackFire(oSettings,"aoRowCreatedCallback",null,[oData.nTr,oData._aData,i])
}}}function _fnNodeToDataIndex(oSettings,n){return(n._DT_RowIndex!==undefined)?n._DT_RowIndex:null
}function _fnNodeToColumnIndex(oSettings,iRow,n){var anCells=_fnGetTdNodes(oSettings,iRow);
for(var i=0,iLen=oSettings.aoColumns.length;
i<iLen;
i++){if(anCells[i]===n){return i
}}return -1
}function _fnGetRowData(oSettings,iRow,sSpecific,aiColumns){var out=[];
for(var i=0,iLen=aiColumns.length;
i<iLen;
i++){out.push(_fnGetCellData(oSettings,iRow,aiColumns[i],sSpecific))
}return out
}function _fnGetCellData(oSettings,iRow,iCol,sSpecific){var sData;
var oCol=oSettings.aoColumns[iCol];
var oData=oSettings.aoData[iRow]._aData;
if((sData=oCol.fnGetData(oData,sSpecific))===undefined){if(oSettings.iDrawError!=oSettings.iDraw&&oCol.sDefaultContent===null){_fnLog(oSettings,0,"Requested unknown parameter "+(typeof oCol.mData=="function"?"{mData function}":"'"+oCol.mData+"'")+" from the data source for row "+iRow);
oSettings.iDrawError=oSettings.iDraw
}return oCol.sDefaultContent
}if(sData===null&&oCol.sDefaultContent!==null){sData=oCol.sDefaultContent
}else{if(typeof sData==="function"){return sData()
}}if(sSpecific=="display"&&sData===null){return""
}return sData
}function _fnSetCellData(oSettings,iRow,iCol,val){var oCol=oSettings.aoColumns[iCol];
var oData=oSettings.aoData[iRow]._aData;
oCol.fnSetData(oData,val)
}var __reArray=/\[.*?\]$/;
function _fnGetObjectDataFn(mSource){if(mSource===null){return function(data,type){return null
}
}else{if(typeof mSource==="function"){return function(data,type,extra){return mSource(data,type,extra)
}
}else{if(typeof mSource==="string"&&(mSource.indexOf(".")!==-1||mSource.indexOf("[")!==-1)){var fetchData=function(data,type,src){var a=src.split(".");
var arrayNotation,out,innerSrc;
if(src!==""){for(var i=0,iLen=a.length;
i<iLen;
i++){arrayNotation=a[i].match(__reArray);
if(arrayNotation){a[i]=a[i].replace(__reArray,"");
if(a[i]!==""){data=data[a[i]]
}out=[];
a.splice(0,i+1);
innerSrc=a.join(".");
for(var j=0,jLen=data.length;
j<jLen;
j++){out.push(fetchData(data[j],type,innerSrc))
}var join=arrayNotation[0].substring(1,arrayNotation[0].length-1);
data=(join==="")?out:out.join(join);
break
}if(data===null||data[a[i]]===undefined){return undefined
}data=data[a[i]]
}}return data
};
return function(data,type){return fetchData(data,type,mSource)
}
}else{return function(data,type){return data[mSource]
}
}}}}function _fnSetObjectDataFn(mSource){if(mSource===null){return function(data,val){}
}else{if(typeof mSource==="function"){return function(data,val){mSource(data,"set",val)
}
}else{if(typeof mSource==="string"&&(mSource.indexOf(".")!==-1||mSource.indexOf("[")!==-1)){var setData=function(data,val,src){var a=src.split("."),b;
var arrayNotation,o,innerSrc;
for(var i=0,iLen=a.length-1;
i<iLen;
i++){arrayNotation=a[i].match(__reArray);
if(arrayNotation){a[i]=a[i].replace(__reArray,"");
data[a[i]]=[];
b=a.slice();
b.splice(0,i+1);
innerSrc=b.join(".");
for(var j=0,jLen=val.length;
j<jLen;
j++){o={};
setData(o,val[j],innerSrc);
data[a[i]].push(o)
}return 
}if(data[a[i]]===null||data[a[i]]===undefined){data[a[i]]={}
}data=data[a[i]]
}data[a[a.length-1].replace(__reArray,"")]=val
};
return function(data,val){return setData(data,val,mSource)
}
}else{return function(data,val){data[mSource]=val
}
}}}}function _fnGetDataMaster(oSettings){var aData=[];
var iLen=oSettings.aoData.length;
for(var i=0;
i<iLen;
i++){aData.push(oSettings.aoData[i]._aData)
}return aData
}function _fnClearTable(oSettings){oSettings.aoData.splice(0,oSettings.aoData.length);
oSettings.aiDisplayMaster.splice(0,oSettings.aiDisplayMaster.length);
oSettings.aiDisplay.splice(0,oSettings.aiDisplay.length);
_fnCalculateEnd(oSettings)
}function _fnDeleteIndex(a,iTarget){var iTargetIndex=-1;
for(var i=0,iLen=a.length;
i<iLen;
i++){if(a[i]==iTarget){iTargetIndex=i
}else{if(a[i]>iTarget){a[i]--
}}}if(iTargetIndex!=-1){a.splice(iTargetIndex,1)
}}function _fnRender(oSettings,iRow,iCol){var oCol=oSettings.aoColumns[iCol];
return oCol.fnRender({iDataRow:iRow,iDataColumn:iCol,oSettings:oSettings,aData:oSettings.aoData[iRow]._aData,mDataProp:oCol.mData},_fnGetCellData(oSettings,iRow,iCol,"display"))
}function _fnCreateTr(oSettings,iRow){var oData=oSettings.aoData[iRow];
var nTd;
if(oData.nTr===null){oData.nTr=document.createElement("tr");
oData.nTr._DT_RowIndex=iRow;
if(oData._aData.DT_RowId){oData.nTr.id=oData._aData.DT_RowId
}if(oData._aData.DT_RowClass){oData.nTr.className=oData._aData.DT_RowClass
}for(var i=0,iLen=oSettings.aoColumns.length;
i<iLen;
i++){var oCol=oSettings.aoColumns[i];
nTd=document.createElement(oCol.sCellType);
nTd.innerHTML=(typeof oCol.fnRender==="function"&&(!oCol.bUseRendered||oCol.mData===null))?_fnRender(oSettings,iRow,i):_fnGetCellData(oSettings,iRow,i,"display");
if(oCol.sClass!==null){nTd.className=oCol.sClass
}if(oCol.bVisible){oData.nTr.appendChild(nTd);
oData._anHidden[i]=null
}else{oData._anHidden[i]=nTd
}if(oCol.fnCreatedCell){oCol.fnCreatedCell.call(oSettings.oInstance,nTd,_fnGetCellData(oSettings,iRow,i,"display"),oData._aData,iRow,i)
}}_fnCallbackFire(oSettings,"aoRowCreatedCallback",null,[oData.nTr,oData._aData,iRow])
}}function _fnBuildHead(oSettings){var i,nTh,iLen,j,jLen;
var iThs=$("th, td",oSettings.nTHead).length;
var iCorrector=0;
var jqChildren;
if(iThs!==0){for(i=0,iLen=oSettings.aoColumns.length;
i<iLen;
i++){nTh=oSettings.aoColumns[i].nTh;
nTh.setAttribute("role","columnheader");
if(oSettings.aoColumns[i].bSortable){nTh.setAttribute("tabindex",oSettings.iTabIndex);
nTh.setAttribute("aria-controls",oSettings.sTableId)
}if(oSettings.aoColumns[i].sClass!==null){$(nTh).addClass(oSettings.aoColumns[i].sClass)
}if(oSettings.aoColumns[i].sTitle!=nTh.innerHTML){nTh.innerHTML=oSettings.aoColumns[i].sTitle
}}}else{var nTr=document.createElement("tr");
for(i=0,iLen=oSettings.aoColumns.length;
i<iLen;
i++){nTh=oSettings.aoColumns[i].nTh;
nTh.innerHTML=oSettings.aoColumns[i].sTitle;
nTh.setAttribute("tabindex","0");
if(oSettings.aoColumns[i].sClass!==null){$(nTh).addClass(oSettings.aoColumns[i].sClass)
}nTr.appendChild(nTh)
}$(oSettings.nTHead).html("")[0].appendChild(nTr);
_fnDetectHeader(oSettings.aoHeader,oSettings.nTHead)
}$(oSettings.nTHead).children("tr").attr("role","row");
if(oSettings.bJUI){for(i=0,iLen=oSettings.aoColumns.length;
i<iLen;
i++){nTh=oSettings.aoColumns[i].nTh;
var nDiv=document.createElement("div");
nDiv.className=oSettings.oClasses.sSortJUIWrapper;
$(nTh).contents().appendTo(nDiv);
var nSpan=document.createElement("span");
nSpan.className=oSettings.oClasses.sSortIcon;
nDiv.appendChild(nSpan);
nTh.appendChild(nDiv)
}}if(oSettings.oFeatures.bSort){for(i=0;
i<oSettings.aoColumns.length;
i++){if(oSettings.aoColumns[i].bSortable!==false){_fnSortAttachListener(oSettings,oSettings.aoColumns[i].nTh,i)
}else{$(oSettings.aoColumns[i].nTh).addClass(oSettings.oClasses.sSortableNone)
}}}if(oSettings.oClasses.sFooterTH!==""){$(oSettings.nTFoot).children("tr").children("th").addClass(oSettings.oClasses.sFooterTH)
}if(oSettings.nTFoot!==null){var anCells=_fnGetUniqueThs(oSettings,null,oSettings.aoFooter);
for(i=0,iLen=oSettings.aoColumns.length;
i<iLen;
i++){if(anCells[i]){oSettings.aoColumns[i].nTf=anCells[i];
if(oSettings.aoColumns[i].sClass){$(anCells[i]).addClass(oSettings.aoColumns[i].sClass)
}}}}}function _fnDrawHead(oSettings,aoSource,bIncludeHidden){var i,iLen,j,jLen,k,kLen,n,nLocalTr;
var aoLocal=[];
var aApplied=[];
var iColumns=oSettings.aoColumns.length;
var iRowspan,iColspan;
if(bIncludeHidden===undefined){bIncludeHidden=false
}for(i=0,iLen=aoSource.length;
i<iLen;
i++){aoLocal[i]=aoSource[i].slice();
aoLocal[i].nTr=aoSource[i].nTr;
for(j=iColumns-1;
j>=0;
j--){if(!oSettings.aoColumns[j].bVisible&&!bIncludeHidden){aoLocal[i].splice(j,1)
}}aApplied.push([])
}for(i=0,iLen=aoLocal.length;
i<iLen;
i++){nLocalTr=aoLocal[i].nTr;
if(nLocalTr){while((n=nLocalTr.firstChild)){nLocalTr.removeChild(n)
}}for(j=0,jLen=aoLocal[i].length;
j<jLen;
j++){iRowspan=1;
iColspan=1;
if(aApplied[i][j]===undefined){nLocalTr.appendChild(aoLocal[i][j].cell);
aApplied[i][j]=1;
while(aoLocal[i+iRowspan]!==undefined&&aoLocal[i][j].cell==aoLocal[i+iRowspan][j].cell){aApplied[i+iRowspan][j]=1;
iRowspan++
}while(aoLocal[i][j+iColspan]!==undefined&&aoLocal[i][j].cell==aoLocal[i][j+iColspan].cell){for(k=0;
k<iRowspan;
k++){aApplied[i+k][j+iColspan]=1
}iColspan++
}aoLocal[i][j].cell.rowSpan=iRowspan;
aoLocal[i][j].cell.colSpan=iColspan
}}}}function _fnDraw(oSettings){var aPreDraw=_fnCallbackFire(oSettings,"aoPreDrawCallback","preDraw",[oSettings]);
if($.inArray(false,aPreDraw)!==-1){_fnProcessingDisplay(oSettings,false);
return 
}var i,iLen,n;
var anRows=[];
var iRowCount=0;
var iStripes=oSettings.asStripeClasses.length;
var iOpenRows=oSettings.aoOpenRows.length;
oSettings.bDrawing=true;
if(oSettings.iInitDisplayStart!==undefined&&oSettings.iInitDisplayStart!=-1){if(oSettings.oFeatures.bServerSide){oSettings._iDisplayStart=oSettings.iInitDisplayStart
}else{oSettings._iDisplayStart=(oSettings.iInitDisplayStart>=oSettings.fnRecordsDisplay())?0:oSettings.iInitDisplayStart
}oSettings.iInitDisplayStart=-1;
_fnCalculateEnd(oSettings)
}if(oSettings.bDeferLoading){oSettings.bDeferLoading=false;
oSettings.iDraw++
}else{if(!oSettings.oFeatures.bServerSide){oSettings.iDraw++
}else{if(!oSettings.bDestroying&&!_fnAjaxUpdate(oSettings)){return 
}}}if(oSettings.aiDisplay.length!==0){var iStart=oSettings._iDisplayStart;
var iEnd=oSettings._iDisplayEnd;
if(oSettings.oFeatures.bServerSide){iStart=0;
iEnd=oSettings.aoData.length
}for(var j=iStart;
j<iEnd;
j++){var aoData=oSettings.aoData[oSettings.aiDisplay[j]];
if(aoData.nTr===null){_fnCreateTr(oSettings,oSettings.aiDisplay[j])
}var nRow=aoData.nTr;
if(iStripes!==0){var sStripe=oSettings.asStripeClasses[iRowCount%iStripes];
if(aoData._sRowStripe!=sStripe){$(nRow).removeClass(aoData._sRowStripe).addClass(sStripe);
aoData._sRowStripe=sStripe
}}_fnCallbackFire(oSettings,"aoRowCallback",null,[nRow,oSettings.aoData[oSettings.aiDisplay[j]]._aData,iRowCount,j]);
anRows.push(nRow);
iRowCount++;
if(iOpenRows!==0){for(var k=0;
k<iOpenRows;
k++){if(nRow==oSettings.aoOpenRows[k].nParent){anRows.push(oSettings.aoOpenRows[k].nTr);
break
}}}}}else{anRows[0]=document.createElement("tr");
if(oSettings.asStripeClasses[0]){anRows[0].className=oSettings.asStripeClasses[0]
}var oLang=oSettings.oLanguage;
var sZero=oLang.sZeroRecords;
if(oSettings.iDraw==1&&oSettings.sAjaxSource!==null&&!oSettings.oFeatures.bServerSide){sZero=oLang.sLoadingRecords
}else{if(oLang.sEmptyTable&&oSettings.fnRecordsTotal()===0){sZero=oLang.sEmptyTable
}}var nTd=document.createElement("td");
nTd.setAttribute("valign","top");
nTd.colSpan=_fnVisbleColumns(oSettings);
nTd.className=oSettings.oClasses.sRowEmpty;
nTd.innerHTML=_fnInfoMacros(oSettings,sZero);
anRows[iRowCount].appendChild(nTd)
}_fnCallbackFire(oSettings,"aoHeaderCallback","header",[$(oSettings.nTHead).children("tr")[0],_fnGetDataMaster(oSettings),oSettings._iDisplayStart,oSettings.fnDisplayEnd(),oSettings.aiDisplay]);
_fnCallbackFire(oSettings,"aoFooterCallback","footer",[$(oSettings.nTFoot).children("tr")[0],_fnGetDataMaster(oSettings),oSettings._iDisplayStart,oSettings.fnDisplayEnd(),oSettings.aiDisplay]);
var nAddFrag=document.createDocumentFragment(),nRemoveFrag=document.createDocumentFragment(),nBodyPar,nTrs;
if(oSettings.nTBody){nBodyPar=oSettings.nTBody.parentNode;
nRemoveFrag.appendChild(oSettings.nTBody);
if(!oSettings.oScroll.bInfinite||!oSettings._bInitComplete||oSettings.bSorted||oSettings.bFiltered){while((n=oSettings.nTBody.firstChild)){oSettings.nTBody.removeChild(n)
}}for(i=0,iLen=anRows.length;
i<iLen;
i++){nAddFrag.appendChild(anRows[i])
}oSettings.nTBody.appendChild(nAddFrag);
if(nBodyPar!==null){nBodyPar.appendChild(oSettings.nTBody)
}}_fnCallbackFire(oSettings,"aoDrawCallback","draw",[oSettings]);
oSettings.bSorted=false;
oSettings.bFiltered=false;
oSettings.bDrawing=false;
if(oSettings.oFeatures.bServerSide){_fnProcessingDisplay(oSettings,false);
if(!oSettings._bInitComplete){_fnInitComplete(oSettings)
}}}function _fnReDraw(oSettings){if(oSettings.oFeatures.bSort){_fnSort(oSettings,oSettings.oPreviousSearch)
}else{if(oSettings.oFeatures.bFilter){_fnFilterComplete(oSettings,oSettings.oPreviousSearch)
}else{_fnCalculateEnd(oSettings);
_fnDraw(oSettings)
}}}function _fnAddOptionsHtml(oSettings){var nHolding=$("<div></div>")[0];
oSettings.nTable.parentNode.insertBefore(nHolding,oSettings.nTable);
oSettings.nTableWrapper=$('<div id="'+oSettings.sTableId+'_wrapper" class="'+oSettings.oClasses.sWrapper+'" role="grid"></div>')[0];
oSettings.nTableReinsertBefore=oSettings.nTable.nextSibling;
var nInsertNode=oSettings.nTableWrapper;
var aDom=oSettings.sDom.split("");
var nTmp,iPushFeature,cOption,nNewNode,cNext,sAttr,j;
for(var i=0;
i<aDom.length;
i++){iPushFeature=0;
cOption=aDom[i];
if(cOption=="<"){nNewNode=$("<div></div>")[0];
cNext=aDom[i+1];
if(cNext=="'"||cNext=='"'){sAttr="";
j=2;
while(aDom[i+j]!=cNext){sAttr+=aDom[i+j];
j++
}if(sAttr=="H"){sAttr=oSettings.oClasses.sJUIHeader
}else{if(sAttr=="F"){sAttr=oSettings.oClasses.sJUIFooter
}}if(sAttr.indexOf(".")!=-1){var aSplit=sAttr.split(".");
nNewNode.id=aSplit[0].substr(1,aSplit[0].length-1);
nNewNode.className=aSplit[1]
}else{if(sAttr.charAt(0)=="#"){nNewNode.id=sAttr.substr(1,sAttr.length-1)
}else{nNewNode.className=sAttr
}}i+=j
}nInsertNode.appendChild(nNewNode);
nInsertNode=nNewNode
}else{if(cOption==">"){nInsertNode=nInsertNode.parentNode
}else{if(cOption=="l"&&oSettings.oFeatures.bPaginate&&oSettings.oFeatures.bLengthChange){nTmp=_fnFeatureHtmlLength(oSettings);
iPushFeature=1
}else{if(cOption=="f"&&oSettings.oFeatures.bFilter){nTmp=_fnFeatureHtmlFilter(oSettings);
iPushFeature=1
}else{if(cOption=="r"&&oSettings.oFeatures.bProcessing){nTmp=_fnFeatureHtmlProcessing(oSettings);
iPushFeature=1
}else{if(cOption=="t"){nTmp=_fnFeatureHtmlTable(oSettings);
iPushFeature=1
}else{if(cOption=="i"&&oSettings.oFeatures.bInfo){nTmp=_fnFeatureHtmlInfo(oSettings);
iPushFeature=1
}else{if(cOption=="p"&&oSettings.oFeatures.bPaginate){nTmp=_fnFeatureHtmlPaginate(oSettings);
iPushFeature=1
}else{if(DataTable.ext.aoFeatures.length!==0){var aoFeatures=DataTable.ext.aoFeatures;
for(var k=0,kLen=aoFeatures.length;
k<kLen;
k++){if(cOption==aoFeatures[k].cFeature){nTmp=aoFeatures[k].fnInit(oSettings);
if(nTmp){iPushFeature=1
}break
}}}}}}}}}}}if(iPushFeature==1&&nTmp!==null){if(typeof oSettings.aanFeatures[cOption]!=="object"){oSettings.aanFeatures[cOption]=[]
}oSettings.aanFeatures[cOption].push(nTmp);
nInsertNode.appendChild(nTmp)
}}nHolding.parentNode.replaceChild(oSettings.nTableWrapper,nHolding)
}function _fnDetectHeader(aLayout,nThead){var nTrs=$(nThead).children("tr");
var nTr,nCell;
var i,k,l,iLen,jLen,iColShifted,iColumn,iColspan,iRowspan;
var bUnique;
var fnShiftCol=function(a,i,j){var k=a[i];
while(k[j]){j++
}return j
};
aLayout.splice(0,aLayout.length);
for(i=0,iLen=nTrs.length;
i<iLen;
i++){aLayout.push([])
}for(i=0,iLen=nTrs.length;
i<iLen;
i++){nTr=nTrs[i];
iColumn=0;
nCell=nTr.firstChild;
while(nCell){if(nCell.nodeName.toUpperCase()=="TD"||nCell.nodeName.toUpperCase()=="TH"){iColspan=nCell.getAttribute("colspan")*1;
iRowspan=nCell.getAttribute("rowspan")*1;
iColspan=(!iColspan||iColspan===0||iColspan===1)?1:iColspan;
iRowspan=(!iRowspan||iRowspan===0||iRowspan===1)?1:iRowspan;
iColShifted=fnShiftCol(aLayout,i,iColumn);
bUnique=iColspan===1?true:false;
for(l=0;
l<iColspan;
l++){for(k=0;
k<iRowspan;
k++){aLayout[i+k][iColShifted+l]={cell:nCell,unique:bUnique};
aLayout[i+k].nTr=nTr
}}}nCell=nCell.nextSibling
}}}function _fnGetUniqueThs(oSettings,nHeader,aLayout){var aReturn=[];
if(!aLayout){aLayout=oSettings.aoHeader;
if(nHeader){aLayout=[];
_fnDetectHeader(aLayout,nHeader)
}}for(var i=0,iLen=aLayout.length;
i<iLen;
i++){for(var j=0,jLen=aLayout[i].length;
j<jLen;
j++){if(aLayout[i][j].unique&&(!aReturn[j]||!oSettings.bSortCellsTop)){aReturn[j]=aLayout[i][j].cell
}}}return aReturn
}function _fnAjaxUpdate(oSettings){if(oSettings.bAjaxDataGet){oSettings.iDraw++;
_fnProcessingDisplay(oSettings,true);
var iColumns=oSettings.aoColumns.length;
var aoData=_fnAjaxParameters(oSettings);
_fnServerParams(oSettings,aoData);
oSettings.fnServerData.call(oSettings.oInstance,oSettings.sAjaxSource,aoData,function(json){_fnAjaxUpdateDraw(oSettings,json)
},oSettings);
return false
}else{return true
}}function _fnAjaxParameters(oSettings){var iColumns=oSettings.aoColumns.length;
var aoData=[],mDataProp,aaSort,aDataSort;
var i,j;
aoData.push({name:"sEcho",value:oSettings.iDraw});
aoData.push({name:"iColumns",value:iColumns});
aoData.push({name:"sColumns",value:_fnColumnOrdering(oSettings)});
aoData.push({name:"iDisplayStart",value:oSettings._iDisplayStart});
aoData.push({name:"iDisplayLength",value:oSettings.oFeatures.bPaginate!==false?oSettings._iDisplayLength:-1});
for(i=0;
i<iColumns;
i++){mDataProp=oSettings.aoColumns[i].mData;
aoData.push({name:"mDataProp_"+i,value:typeof (mDataProp)==="function"?"function":mDataProp})
}if(oSettings.oFeatures.bFilter!==false){aoData.push({name:"sSearch",value:oSettings.oPreviousSearch.sSearch});
aoData.push({name:"bRegex",value:oSettings.oPreviousSearch.bRegex});
for(i=0;
i<iColumns;
i++){aoData.push({name:"sSearch_"+i,value:oSettings.aoPreSearchCols[i].sSearch});
aoData.push({name:"bRegex_"+i,value:oSettings.aoPreSearchCols[i].bRegex});
aoData.push({name:"bSearchable_"+i,value:oSettings.aoColumns[i].bSearchable})
}}if(oSettings.oFeatures.bSort!==false){var iCounter=0;
aaSort=(oSettings.aaSortingFixed!==null)?oSettings.aaSortingFixed.concat(oSettings.aaSorting):oSettings.aaSorting.slice();
for(i=0;
i<aaSort.length;
i++){aDataSort=oSettings.aoColumns[aaSort[i][0]].aDataSort;
for(j=0;
j<aDataSort.length;
j++){aoData.push({name:"iSortCol_"+iCounter,value:aDataSort[j]});
aoData.push({name:"sSortDir_"+iCounter,value:aaSort[i][1]});
iCounter++
}}aoData.push({name:"iSortingCols",value:iCounter});
for(i=0;
i<iColumns;
i++){aoData.push({name:"bSortable_"+i,value:oSettings.aoColumns[i].bSortable})
}}return aoData
}function _fnServerParams(oSettings,aoData){_fnCallbackFire(oSettings,"aoServerParams","serverParams",[aoData])
}function _fnAjaxUpdateDraw(oSettings,json){if(json.sEcho!==undefined){if(json.sEcho*1<oSettings.iDraw){return 
}else{oSettings.iDraw=json.sEcho*1
}}if(!oSettings.oScroll.bInfinite||(oSettings.oScroll.bInfinite&&(oSettings.bSorted||oSettings.bFiltered))){_fnClearTable(oSettings)
}oSettings._iRecordsTotal=parseInt(json.iTotalRecords,10);
oSettings._iRecordsDisplay=parseInt(json.iTotalDisplayRecords,10);
var sOrdering=_fnColumnOrdering(oSettings);
var bReOrder=(json.sColumns!==undefined&&sOrdering!==""&&json.sColumns!=sOrdering);
var aiIndex;
if(bReOrder){aiIndex=_fnReOrderIndex(oSettings,json.sColumns)
}var aData=_fnGetObjectDataFn(oSettings.sAjaxDataProp)(json);
for(var i=0,iLen=aData.length;
i<iLen;
i++){if(bReOrder){var aDataSorted=[];
for(var j=0,jLen=oSettings.aoColumns.length;
j<jLen;
j++){aDataSorted.push(aData[i][aiIndex[j]])
}_fnAddData(oSettings,aDataSorted)
}else{_fnAddData(oSettings,aData[i])
}}oSettings.aiDisplay=oSettings.aiDisplayMaster.slice();
oSettings.bAjaxDataGet=false;
_fnDraw(oSettings);
oSettings.bAjaxDataGet=true;
_fnProcessingDisplay(oSettings,false)
}function _fnFeatureHtmlFilter(oSettings){var oPreviousSearch=oSettings.oPreviousSearch;
var sSearchStr=oSettings.oLanguage.sSearch;
sSearchStr=(sSearchStr.indexOf("_INPUT_")!==-1)?sSearchStr.replace("_INPUT_",'<input type="text" />'):sSearchStr===""?'<input type="text" />':sSearchStr+' <input type="text" />';
var nFilter=document.createElement("div");
nFilter.className=oSettings.oClasses.sFilter;
nFilter.innerHTML="<label>"+sSearchStr+"</label>";
if(!oSettings.aanFeatures.f){nFilter.id=oSettings.sTableId+"_filter"
}var jqFilter=$('input[type="text"]',nFilter);
nFilter._DT_Input=jqFilter[0];
jqFilter.val(oPreviousSearch.sSearch.replace('"',"&quot;"));
jqFilter.bind("http://www.usfoods.com/etc/designs/usfoods/clientlibs/keyup.DT",function(e){var n=oSettings.aanFeatures.f;
var val=this.value===""?"":this.value;
for(var i=0,iLen=n.length;
i<iLen;
i++){if(n[i]!=$(this).parents("div.dataTables_filter")[0]){$(n[i]._DT_Input).val(val)
}}if(val!=oPreviousSearch.sSearch){_fnFilterComplete(oSettings,{sSearch:val,bRegex:oPreviousSearch.bRegex,bSmart:oPreviousSearch.bSmart,bCaseInsensitive:oPreviousSearch.bCaseInsensitive})
}});
jqFilter.attr("aria-controls",oSettings.sTableId).bind("http://www.usfoods.com/etc/designs/usfoods/clientlibs/keypress.DT",function(e){if(e.keyCode==13){return false
}});
return nFilter
}function _fnFilterComplete(oSettings,oInput,iForce){var oPrevSearch=oSettings.oPreviousSearch;
var aoPrevSearch=oSettings.aoPreSearchCols;
var fnSaveFilter=function(oFilter){oPrevSearch.sSearch=oFilter.sSearch;
oPrevSearch.bRegex=oFilter.bRegex;
oPrevSearch.bSmart=oFilter.bSmart;
oPrevSearch.bCaseInsensitive=oFilter.bCaseInsensitive
};
if(!oSettings.oFeatures.bServerSide){_fnFilter(oSettings,oInput.sSearch,iForce,oInput.bRegex,oInput.bSmart,oInput.bCaseInsensitive);
fnSaveFilter(oInput);
for(var i=0;
i<oSettings.aoPreSearchCols.length;
i++){_fnFilterColumn(oSettings,aoPrevSearch[i].sSearch,i,aoPrevSearch[i].bRegex,aoPrevSearch[i].bSmart,aoPrevSearch[i].bCaseInsensitive)
}_fnFilterCustom(oSettings)
}else{fnSaveFilter(oInput)
}oSettings.bFiltered=true;
$(oSettings.oInstance).trigger("filter",oSettings);
oSettings._iDisplayStart=0;
_fnCalculateEnd(oSettings);
_fnDraw(oSettings);
_fnBuildSearchArray(oSettings,0)
}function _fnFilterCustom(oSettings){var afnFilters=DataTable.ext.afnFiltering;
var aiFilterColumns=_fnGetColumns(oSettings,"bSearchable");
for(var i=0,iLen=afnFilters.length;
i<iLen;
i++){var iCorrector=0;
for(var j=0,jLen=oSettings.aiDisplay.length;
j<jLen;
j++){var iDisIndex=oSettings.aiDisplay[j-iCorrector];
var bTest=afnFilters[i](oSettings,_fnGetRowData(oSettings,iDisIndex,"filter",aiFilterColumns),iDisIndex);
if(!bTest){oSettings.aiDisplay.splice(j-iCorrector,1);
iCorrector++
}}}}function _fnFilterColumn(oSettings,sInput,iColumn,bRegex,bSmart,bCaseInsensitive){if(sInput===""){return 
}var iIndexCorrector=0;
var rpSearch=_fnFilterCreateSearch(sInput,bRegex,bSmart,bCaseInsensitive);
for(var i=oSettings.aiDisplay.length-1;
i>=0;
i--){var sData=_fnDataToSearch(_fnGetCellData(oSettings,oSettings.aiDisplay[i],iColumn,"filter"),oSettings.aoColumns[iColumn].sType);
if(!rpSearch.test(sData)){oSettings.aiDisplay.splice(i,1);
iIndexCorrector++
}}}function _fnFilter(oSettings,sInput,iForce,bRegex,bSmart,bCaseInsensitive){var i;
var rpSearch=_fnFilterCreateSearch(sInput,bRegex,bSmart,bCaseInsensitive);
var oPrevSearch=oSettings.oPreviousSearch;
if(!iForce){iForce=0
}if(DataTable.ext.afnFiltering.length!==0){iForce=1
}if(sInput.length<=0){oSettings.aiDisplay.splice(0,oSettings.aiDisplay.length);
oSettings.aiDisplay=oSettings.aiDisplayMaster.slice()
}else{if(oSettings.aiDisplay.length==oSettings.aiDisplayMaster.length||oPrevSearch.sSearch.length>sInput.length||iForce==1||sInput.indexOf(oPrevSearch.sSearch)!==0){oSettings.aiDisplay.splice(0,oSettings.aiDisplay.length);
_fnBuildSearchArray(oSettings,1);
for(i=0;
i<oSettings.aiDisplayMaster.length;
i++){if(rpSearch.test(oSettings.asDataSearch[i])){oSettings.aiDisplay.push(oSettings.aiDisplayMaster[i])
}}}else{var iIndexCorrector=0;
for(i=0;
i<oSettings.asDataSearch.length;
i++){if(!rpSearch.test(oSettings.asDataSearch[i])){oSettings.aiDisplay.splice(i-iIndexCorrector,1);
iIndexCorrector++
}}}}}function _fnBuildSearchArray(oSettings,iMaster){if(!oSettings.oFeatures.bServerSide){oSettings.asDataSearch=[];
var aiFilterColumns=_fnGetColumns(oSettings,"bSearchable");
var aiIndex=(iMaster===1)?oSettings.aiDisplayMaster:oSettings.aiDisplay;
for(var i=0,iLen=aiIndex.length;
i<iLen;
i++){oSettings.asDataSearch[i]=_fnBuildSearchRow(oSettings,_fnGetRowData(oSettings,aiIndex[i],"filter",aiFilterColumns))
}}}function _fnBuildSearchRow(oSettings,aData){var sSearch=aData.join("  ");
if(sSearch.indexOf("&")!==-1){sSearch=$("<div>").html(sSearch).text()
}return sSearch.replace(/[\n\r]/g," ")
}function _fnFilterCreateSearch(sSearch,bRegex,bSmart,bCaseInsensitive){var asSearch,sRegExpString;
if(bSmart){asSearch=bRegex?sSearch.split(" "):_fnEscapeRegex(sSearch).split(" ");
sRegExpString="^(?=.*?"+asSearch.join(")(?=.*?")+").*$";
return new RegExp(sRegExpString,bCaseInsensitive?"i":"")
}else{sSearch=bRegex?sSearch:_fnEscapeRegex(sSearch);
return new RegExp(sSearch,bCaseInsensitive?"i":"")
}}function _fnDataToSearch(sData,sType){if(typeof DataTable.ext.ofnSearch[sType]==="function"){return DataTable.ext.ofnSearch[sType](sData)
}else{if(sData===null){return""
}else{if(sType=="html"){return sData.replace(/[\r\n]/g," ").replace(/<.*?>/g,"")
}else{if(typeof sData==="string"){return sData.replace(/[\r\n]/g," ")
}}}}return sData
}function _fnEscapeRegex(sVal){var acEscape=["/",".","*","+","?","|","(",")","[","]","{","}","\\","$","^","-"];
var reReplace=new RegExp("(\\"+acEscape.join("|\\")+")","g");
return sVal.replace(reReplace,"\\$1")
}function _fnFeatureHtmlInfo(oSettings){var nInfo=document.createElement("div");
nInfo.className=oSettings.oClasses.sInfo;
if(!oSettings.aanFeatures.i){oSettings.aoDrawCallback.push({fn:_fnUpdateInfo,sName:"information"});
nInfo.id=oSettings.sTableId+"_info"
}oSettings.nTable.setAttribute("aria-describedby",oSettings.sTableId+"_info");
return nInfo
}function _fnUpdateInfo(oSettings){if(!oSettings.oFeatures.bInfo||oSettings.aanFeatures.i.length===0){return 
}var oLang=oSettings.oLanguage,iStart=oSettings._iDisplayStart+1,iEnd=oSettings.fnDisplayEnd(),iMax=oSettings.fnRecordsTotal(),iTotal=oSettings.fnRecordsDisplay(),sOut;
if(iTotal===0){sOut=oLang.sInfoEmpty
}else{sOut=oLang.sInfo
}if(iTotal!=iMax){sOut+=" "+oLang.sInfoFiltered
}sOut+=oLang.sInfoPostFix;
sOut=_fnInfoMacros(oSettings,sOut);
if(oLang.fnInfoCallback!==null){sOut=oLang.fnInfoCallback.call(oSettings.oInstance,oSettings,iStart,iEnd,iMax,iTotal,sOut)
}var n=oSettings.aanFeatures.i;
for(var i=0,iLen=n.length;
i<iLen;
i++){$(n[i]).html(sOut)
}}function _fnInfoMacros(oSettings,str){var iStart=oSettings._iDisplayStart+1,sStart=oSettings.fnFormatNumber(iStart),iEnd=oSettings.fnDisplayEnd(),sEnd=oSettings.fnFormatNumber(iEnd),iTotal=oSettings.fnRecordsDisplay(),sTotal=oSettings.fnFormatNumber(iTotal),iMax=oSettings.fnRecordsTotal(),sMax=oSettings.fnFormatNumber(iMax);
if(oSettings.oScroll.bInfinite){sStart=oSettings.fnFormatNumber(1)
}return str.replace(/_START_/g,sStart).replace(/_END_/g,sEnd).replace(/_TOTAL_/g,sTotal).replace(/_MAX_/g,sMax)
}function _fnInitialise(oSettings){var i,iLen,iAjaxStart=oSettings.iInitDisplayStart;
if(oSettings.bInitialised===false){setTimeout(function(){_fnInitialise(oSettings)
},200);
return 
}_fnAddOptionsHtml(oSettings);
_fnBuildHead(oSettings);
_fnDrawHead(oSettings,oSettings.aoHeader);
if(oSettings.nTFoot){_fnDrawHead(oSettings,oSettings.aoFooter)
}_fnProcessingDisplay(oSettings,true);
if(oSettings.oFeatures.bAutoWidth){_fnCalculateColumnWidths(oSettings)
}for(i=0,iLen=oSettings.aoColumns.length;
i<iLen;
i++){if(oSettings.aoColumns[i].sWidth!==null){oSettings.aoColumns[i].nTh.style.width=_fnStringToCss(oSettings.aoColumns[i].sWidth)
}}if(oSettings.oFeatures.bSort){_fnSort(oSettings)
}else{if(oSettings.oFeatures.bFilter){_fnFilterComplete(oSettings,oSettings.oPreviousSearch)
}else{oSettings.aiDisplay=oSettings.aiDisplayMaster.slice();
_fnCalculateEnd(oSettings);
_fnDraw(oSettings)
}}if(oSettings.sAjaxSource!==null&&!oSettings.oFeatures.bServerSide){var aoData=[];
_fnServerParams(oSettings,aoData);
oSettings.fnServerData.call(oSettings.oInstance,oSettings.sAjaxSource,aoData,function(json){var aData=(oSettings.sAjaxDataProp!=="")?_fnGetObjectDataFn(oSettings.sAjaxDataProp)(json):json;
for(i=0;
i<aData.length;
i++){_fnAddData(oSettings,aData[i])
}oSettings.iInitDisplayStart=iAjaxStart;
if(oSettings.oFeatures.bSort){_fnSort(oSettings)
}else{oSettings.aiDisplay=oSettings.aiDisplayMaster.slice();
_fnCalculateEnd(oSettings);
_fnDraw(oSettings)
}_fnProcessingDisplay(oSettings,false);
_fnInitComplete(oSettings,json)
},oSettings);
return 
}if(!oSettings.oFeatures.bServerSide){_fnProcessingDisplay(oSettings,false);
_fnInitComplete(oSettings)
}}function _fnInitComplete(oSettings,json){oSettings._bInitComplete=true;
_fnCallbackFire(oSettings,"aoInitComplete","init",[oSettings,json])
}function _fnLanguageCompat(oLanguage){var oDefaults=DataTable.defaults.oLanguage;
if(!oLanguage.sEmptyTable&&oLanguage.sZeroRecords&&oDefaults.sEmptyTable==="No data available in table"){_fnMap(oLanguage,oLanguage,"sZeroRecords","sEmptyTable")
}if(!oLanguage.sLoadingRecords&&oLanguage.sZeroRecords&&oDefaults.sLoadingRecords==="Loading..."){_fnMap(oLanguage,oLanguage,"sZeroRecords","sLoadingRecords")
}}function _fnFeatureHtmlLength(oSettings){if(oSettings.oScroll.bInfinite){return null
}var sName='name="'+oSettings.sTableId+'_length"';
var sStdMenu='<select size="1" '+sName+">";
var i,iLen;
var aLengthMenu=oSettings.aLengthMenu;
if(aLengthMenu.length==2&&typeof aLengthMenu[0]==="object"&&typeof aLengthMenu[1]==="object"){for(i=0,iLen=aLengthMenu[0].length;
i<iLen;
i++){sStdMenu+='<option value="'+aLengthMenu[0][i]+'">'+aLengthMenu[1][i]+"</option>"
}}else{for(i=0,iLen=aLengthMenu.length;
i<iLen;
i++){sStdMenu+='<option value="'+aLengthMenu[i]+'">'+aLengthMenu[i]+"</option>"
}}sStdMenu+="</select>";
var nLength=document.createElement("div");
if(!oSettings.aanFeatures.l){nLength.id=oSettings.sTableId+"_length"
}nLength.className=oSettings.oClasses.sLength;
nLength.innerHTML="<label>"+oSettings.oLanguage.sLengthMenu.replace("_MENU_",sStdMenu)+"</label>";
$('select option[value="'+oSettings._iDisplayLength+'"]',nLength).attr("selected",true);
$("select",nLength).bind("http://www.usfoods.com/etc/designs/usfoods/clientlibs/change.DT",function(e){var iVal=$(this).val();
var n=oSettings.aanFeatures.l;
for(i=0,iLen=n.length;
i<iLen;
i++){if(n[i]!=this.parentNode){$("select",n[i]).val(iVal)
}}oSettings._iDisplayLength=parseInt(iVal,10);
_fnCalculateEnd(oSettings);
if(oSettings.fnDisplayEnd()==oSettings.fnRecordsDisplay()){oSettings._iDisplayStart=oSettings.fnDisplayEnd()-oSettings._iDisplayLength;
if(oSettings._iDisplayStart<0){oSettings._iDisplayStart=0
}}if(oSettings._iDisplayLength==-1){oSettings._iDisplayStart=0
}_fnDraw(oSettings)
});
$("select",nLength).attr("aria-controls",oSettings.sTableId);
return nLength
}function _fnCalculateEnd(oSettings){if(oSettings.oFeatures.bPaginate===false){oSettings._iDisplayEnd=oSettings.aiDisplay.length
}else{if(oSettings._iDisplayStart+oSettings._iDisplayLength>oSettings.aiDisplay.length||oSettings._iDisplayLength==-1){oSettings._iDisplayEnd=oSettings.aiDisplay.length
}else{oSettings._iDisplayEnd=oSettings._iDisplayStart+oSettings._iDisplayLength
}}}function _fnFeatureHtmlPaginate(oSettings){if(oSettings.oScroll.bInfinite){return null
}var nPaginate=document.createElement("div");
nPaginate.className=oSettings.oClasses.sPaging+oSettings.sPaginationType;
DataTable.ext.oPagination[oSettings.sPaginationType].fnInit(oSettings,nPaginate,function(oSettings){_fnCalculateEnd(oSettings);
_fnDraw(oSettings)
});
if(!oSettings.aanFeatures.p){oSettings.aoDrawCallback.push({fn:function(oSettings){DataTable.ext.oPagination[oSettings.sPaginationType].fnUpdate(oSettings,function(oSettings){_fnCalculateEnd(oSettings);
_fnDraw(oSettings)
})
},sName:"pagination"})
}return nPaginate
}function _fnPageChange(oSettings,mAction){var iOldStart=oSettings._iDisplayStart;
if(typeof mAction==="number"){oSettings._iDisplayStart=mAction*oSettings._iDisplayLength;
if(oSettings._iDisplayStart>oSettings.fnRecordsDisplay()){oSettings._iDisplayStart=0
}}else{if(mAction=="first"){oSettings._iDisplayStart=0
}else{if(mAction=="previous"){oSettings._iDisplayStart=oSettings._iDisplayLength>=0?oSettings._iDisplayStart-oSettings._iDisplayLength:0;
if(oSettings._iDisplayStart<0){oSettings._iDisplayStart=0
}}else{if(mAction=="next"){if(oSettings._iDisplayLength>=0){if(oSettings._iDisplayStart+oSettings._iDisplayLength<oSettings.fnRecordsDisplay()){oSettings._iDisplayStart+=oSettings._iDisplayLength
}}else{oSettings._iDisplayStart=0
}}else{if(mAction=="last"){if(oSettings._iDisplayLength>=0){var iPages=parseInt((oSettings.fnRecordsDisplay()-1)/oSettings._iDisplayLength,10)+1;
oSettings._iDisplayStart=(iPages-1)*oSettings._iDisplayLength
}else{oSettings._iDisplayStart=0
}}else{_fnLog(oSettings,0,"Unknown paging action: "+mAction)
}}}}}$(oSettings.oInstance).trigger("page",oSettings);
return iOldStart!=oSettings._iDisplayStart
}function _fnFeatureHtmlProcessing(oSettings){var nProcessing=document.createElement("div");
if(!oSettings.aanFeatures.r){nProcessing.id=oSettings.sTableId+"_processing"
}nProcessing.innerHTML=oSettings.oLanguage.sProcessing;
nProcessing.className=oSettings.oClasses.sProcessing;
oSettings.nTable.parentNode.insertBefore(nProcessing,oSettings.nTable);
return nProcessing
}function _fnProcessingDisplay(oSettings,bShow){if(oSettings.oFeatures.bProcessing){var an=oSettings.aanFeatures.r;
for(var i=0,iLen=an.length;
i<iLen;
i++){an[i].style.visibility=bShow?"visible":"hidden"
}}$(oSettings.oInstance).trigger("processing",[oSettings,bShow])
}function _fnFeatureHtmlTable(oSettings){if(oSettings.oScroll.sX===""&&oSettings.oScroll.sY===""){return oSettings.nTable
}var nScroller=document.createElement("div"),nScrollHead=document.createElement("div"),nScrollHeadInner=document.createElement("div"),nScrollBody=document.createElement("div"),nScrollFoot=document.createElement("div"),nScrollFootInner=document.createElement("div"),nScrollHeadTable=oSettings.nTable.cloneNode(false),nScrollFootTable=oSettings.nTable.cloneNode(false),nThead=oSettings.nTable.getElementsByTagName("thead")[0],nTfoot=oSettings.nTable.getElementsByTagName("tfoot").length===0?null:oSettings.nTable.getElementsByTagName("tfoot")[0],oClasses=oSettings.oClasses;
nScrollHead.appendChild(nScrollHeadInner);
nScrollFoot.appendChild(nScrollFootInner);
nScrollBody.appendChild(oSettings.nTable);
nScroller.appendChild(nScrollHead);
nScroller.appendChild(nScrollBody);
nScrollHeadInner.appendChild(nScrollHeadTable);
nScrollHeadTable.appendChild(nThead);
if(nTfoot!==null){nScroller.appendChild(nScrollFoot);
nScrollFootInner.appendChild(nScrollFootTable);
nScrollFootTable.appendChild(nTfoot)
}nScroller.className=oClasses.sScrollWrapper;
nScrollHead.className=oClasses.sScrollHead;
nScrollHeadInner.className=oClasses.sScrollHeadInner;
nScrollBody.className=oClasses.sScrollBody;
nScrollFoot.className=oClasses.sScrollFoot;
nScrollFootInner.className=oClasses.sScrollFootInner;
if(oSettings.oScroll.bAutoCss){nScrollHead.style.overflow="hidden";
nScrollHead.style.position="relative";
nScrollFoot.style.overflow="hidden";
nScrollBody.style.overflow="auto"
}nScrollHead.style.border="0";
nScrollHead.style.width="100%";
nScrollFoot.style.border="0";
nScrollHeadInner.style.width=oSettings.oScroll.sXInner!==""?oSettings.oScroll.sXInner:"100%";
nScrollHeadTable.removeAttribute("id");
nScrollHeadTable.style.marginLeft="0";
oSettings.nTable.style.marginLeft="0";
if(nTfoot!==null){nScrollFootTable.removeAttribute("id");
nScrollFootTable.style.marginLeft="0"
}var nCaption=$(oSettings.nTable).children("caption");
if(nCaption.length>0){nCaption=nCaption[0];
if(nCaption._captionSide==="top"){nScrollHeadTable.appendChild(nCaption)
}else{if(nCaption._captionSide==="bottom"&&nTfoot){nScrollFootTable.appendChild(nCaption)
}}}if(oSettings.oScroll.sX!==""){nScrollHead.style.width=_fnStringToCss(oSettings.oScroll.sX);
nScrollBody.style.width=_fnStringToCss(oSettings.oScroll.sX);
if(nTfoot!==null){nScrollFoot.style.width=_fnStringToCss(oSettings.oScroll.sX)
}$(nScrollBody).scroll(function(e){nScrollHead.scrollLeft=this.scrollLeft;
if(nTfoot!==null){nScrollFoot.scrollLeft=this.scrollLeft
}})
}if(oSettings.oScroll.sY!==""){nScrollBody.style.height=_fnStringToCss(oSettings.oScroll.sY)
}oSettings.aoDrawCallback.push({fn:_fnScrollDraw,sName:"scrolling"});
if(oSettings.oScroll.bInfinite){$(nScrollBody).scroll(function(){if(!oSettings.bDrawing&&$(this).scrollTop()!==0){if($(this).scrollTop()+$(this).height()>$(oSettings.nTable).height()-oSettings.oScroll.iLoadGap){if(oSettings.fnDisplayEnd()<oSettings.fnRecordsDisplay()){_fnPageChange(oSettings,"next");
_fnCalculateEnd(oSettings);
_fnDraw(oSettings)
}}}})
}oSettings.nScrollHead=nScrollHead;
oSettings.nScrollFoot=nScrollFoot;
return nScroller
}function _fnScrollDraw(o){var nScrollHeadInner=o.nScrollHead.getElementsByTagName("div")[0],nScrollHeadTable=nScrollHeadInner.getElementsByTagName("table")[0],nScrollBody=o.nTable.parentNode,i,iLen,j,jLen,anHeadToSize,anHeadSizers,anFootSizers,anFootToSize,oStyle,iVis,nTheadSize,nTfootSize,iWidth,aApplied=[],aAppliedFooter=[],iSanityWidth,nScrollFootInner=(o.nTFoot!==null)?o.nScrollFoot.getElementsByTagName("div")[0]:null,nScrollFootTable=(o.nTFoot!==null)?nScrollFootInner.getElementsByTagName("table")[0]:null,ie67=o.oBrowser.bScrollOversize,zeroOut=function(nSizer){oStyle=nSizer.style;
oStyle.paddingTop="0";
oStyle.paddingBottom="0";
oStyle.borderTopWidth="0";
oStyle.borderBottomWidth="0";
oStyle.height=0
};
$(o.nTable).children("thead, tfoot").remove();
nTheadSize=$(o.nTHead).clone()[0];
o.nTable.insertBefore(nTheadSize,o.nTable.childNodes[0]);
anHeadToSize=o.nTHead.getElementsByTagName("tr");
anHeadSizers=nTheadSize.getElementsByTagName("tr");
if(o.nTFoot!==null){nTfootSize=$(o.nTFoot).clone()[0];
o.nTable.insertBefore(nTfootSize,o.nTable.childNodes[1]);
anFootToSize=o.nTFoot.getElementsByTagName("tr");
anFootSizers=nTfootSize.getElementsByTagName("tr")
}if(o.oScroll.sX===""){nScrollBody.style.width="100%";
nScrollHeadInner.parentNode.style.width="100%"
}var nThs=_fnGetUniqueThs(o,nTheadSize);
for(i=0,iLen=nThs.length;
i<iLen;
i++){iVis=_fnVisibleToColumnIndex(o,i);
nThs[i].style.width=o.aoColumns[iVis].sWidth
}if(o.nTFoot!==null){_fnApplyToChildren(function(n){n.style.width=""
},anFootSizers)
}if(o.oScroll.bCollapse&&o.oScroll.sY!==""){nScrollBody.style.height=(nScrollBody.offsetHeight+o.nTHead.offsetHeight)+"px"
}iSanityWidth=$(o.nTable).outerWidth();
if(o.oScroll.sX===""){o.nTable.style.width="100%";
if(ie67&&($("tbody",nScrollBody).height()>nScrollBody.offsetHeight||$(nScrollBody).css("overflow-y")=="scroll")){o.nTable.style.width=_fnStringToCss($(o.nTable).outerWidth()-o.oScroll.iBarWidth)
}}else{if(o.oScroll.sXInner!==""){o.nTable.style.width=_fnStringToCss(o.oScroll.sXInner)
}else{if(iSanityWidth==$(nScrollBody).width()&&$(nScrollBody).height()<$(o.nTable).height()){o.nTable.style.width=_fnStringToCss(iSanityWidth-o.oScroll.iBarWidth);
if($(o.nTable).outerWidth()>iSanityWidth-o.oScroll.iBarWidth){o.nTable.style.width=_fnStringToCss(iSanityWidth)
}}else{o.nTable.style.width=_fnStringToCss(iSanityWidth)
}}}iSanityWidth=$(o.nTable).outerWidth();
_fnApplyToChildren(zeroOut,anHeadSizers);
_fnApplyToChildren(function(nSizer){aApplied.push(_fnStringToCss($(nSizer).width()))
},anHeadSizers);
_fnApplyToChildren(function(nToSize,i){nToSize.style.width=aApplied[i]
},anHeadToSize);
$(anHeadSizers).height(0);
if(o.nTFoot!==null){_fnApplyToChildren(zeroOut,anFootSizers);
_fnApplyToChildren(function(nSizer){aAppliedFooter.push(_fnStringToCss($(nSizer).width()))
},anFootSizers);
_fnApplyToChildren(function(nToSize,i){nToSize.style.width=aAppliedFooter[i]
},anFootToSize);
$(anFootSizers).height(0)
}_fnApplyToChildren(function(nSizer,i){nSizer.innerHTML="";
nSizer.style.width=aApplied[i]
},anHeadSizers);
if(o.nTFoot!==null){_fnApplyToChildren(function(nSizer,i){nSizer.innerHTML="";
nSizer.style.width=aAppliedFooter[i]
},anFootSizers)
}if($(o.nTable).outerWidth()<iSanityWidth){var iCorrection=((nScrollBody.scrollHeight>nScrollBody.offsetHeight||$(nScrollBody).css("overflow-y")=="scroll"))?iSanityWidth+o.oScroll.iBarWidth:iSanityWidth;
if(ie67&&(nScrollBody.scrollHeight>nScrollBody.offsetHeight||$(nScrollBody).css("overflow-y")=="scroll")){o.nTable.style.width=_fnStringToCss(iCorrection-o.oScroll.iBarWidth)
}nScrollBody.style.width=_fnStringToCss(iCorrection);
o.nScrollHead.style.width=_fnStringToCss(iCorrection);
if(o.nTFoot!==null){o.nScrollFoot.style.width=_fnStringToCss(iCorrection)
}if(o.oScroll.sX===""){_fnLog(o,1,"The table cannot fit into the current element which will cause column misalignment. The table has been drawn at its minimum possible width.")
}else{if(o.oScroll.sXInner!==""){_fnLog(o,1,"The table cannot fit into the current element which will cause column misalignment. Increase the sScrollXInner value or remove it to allow automatic calculation")
}}}else{nScrollBody.style.width=_fnStringToCss("100%");
o.nScrollHead.style.width=_fnStringToCss("100%");
if(o.nTFoot!==null){o.nScrollFoot.style.width=_fnStringToCss("100%")
}}if(o.oScroll.sY===""){if(ie67){nScrollBody.style.height=_fnStringToCss(o.nTable.offsetHeight+o.oScroll.iBarWidth)
}}if(o.oScroll.sY!==""&&o.oScroll.bCollapse){nScrollBody.style.height=_fnStringToCss(o.oScroll.sY);
var iExtra=(o.oScroll.sX!==""&&o.nTable.offsetWidth>nScrollBody.offsetWidth)?o.oScroll.iBarWidth:0;
if(o.nTable.offsetHeight<nScrollBody.offsetHeight){nScrollBody.style.height=_fnStringToCss(o.nTable.offsetHeight+iExtra)
}}var iOuterWidth=$(o.nTable).outerWidth();
nScrollHeadTable.style.width=_fnStringToCss(iOuterWidth);
nScrollHeadInner.style.width=_fnStringToCss(iOuterWidth);
var bScrolling=$(o.nTable).height()>nScrollBody.clientHeight||$(nScrollBody).css("overflow-y")=="scroll";
nScrollHeadInner.style.paddingRight=bScrolling?o.oScroll.iBarWidth+"px":"0px";
if(o.nTFoot!==null){nScrollFootTable.style.width=_fnStringToCss(iOuterWidth);
nScrollFootInner.style.width=_fnStringToCss(iOuterWidth);
nScrollFootInner.style.paddingRight=bScrolling?o.oScroll.iBarWidth+"px":"0px"
}$(nScrollBody).scroll();
if(o.bSorted||o.bFiltered){nScrollBody.scrollTop=0
}}function _fnApplyToChildren(fn,an1,an2){var index=0,i=0,iLen=an1.length;
var nNode1,nNode2;
while(i<iLen){nNode1=an1[i].firstChild;
nNode2=an2?an2[i].firstChild:null;
while(nNode1){if(nNode1.nodeType===1){if(an2){fn(nNode1,nNode2,index)
}else{fn(nNode1,index)
}index++
}nNode1=nNode1.nextSibling;
nNode2=an2?nNode2.nextSibling:null
}i++
}}function _fnConvertToWidth(sWidth,nParent){if(!sWidth||sWidth===null||sWidth===""){return 0
}if(!nParent){nParent=document.body
}var iWidth;
var nTmp=document.createElement("div");
nTmp.style.width=_fnStringToCss(sWidth);
nParent.appendChild(nTmp);
iWidth=nTmp.offsetWidth;
nParent.removeChild(nTmp);
return(iWidth)
}function _fnCalculateColumnWidths(oSettings){var iTableWidth=oSettings.nTable.offsetWidth;
var iUserInputs=0;
var iTmpWidth;
var iVisibleColumns=0;
var iColums=oSettings.aoColumns.length;
var i,iIndex,iCorrector,iWidth;
var oHeaders=$("th",oSettings.nTHead);
var widthAttr=oSettings.nTable.getAttribute("width");
var nWrapper=oSettings.nTable.parentNode;
for(i=0;
i<iColums;
i++){if(oSettings.aoColumns[i].bVisible){iVisibleColumns++;
if(oSettings.aoColumns[i].sWidth!==null){iTmpWidth=_fnConvertToWidth(oSettings.aoColumns[i].sWidthOrig,nWrapper);
if(iTmpWidth!==null){oSettings.aoColumns[i].sWidth=_fnStringToCss(iTmpWidth)
}iUserInputs++
}}}if(iColums==oHeaders.length&&iUserInputs===0&&iVisibleColumns==iColums&&oSettings.oScroll.sX===""&&oSettings.oScroll.sY===""){for(i=0;
i<oSettings.aoColumns.length;
i++){iTmpWidth=$(oHeaders[i]).width();
if(iTmpWidth!==null){oSettings.aoColumns[i].sWidth=_fnStringToCss(iTmpWidth)
}}}else{var nCalcTmp=oSettings.nTable.cloneNode(false),nTheadClone=oSettings.nTHead.cloneNode(true),nBody=document.createElement("tbody"),nTr=document.createElement("tr"),nDivSizing;
nCalcTmp.removeAttribute("id");
nCalcTmp.appendChild(nTheadClone);
if(oSettings.nTFoot!==null){nCalcTmp.appendChild(oSettings.nTFoot.cloneNode(true));
_fnApplyToChildren(function(n){n.style.width=""
},nCalcTmp.getElementsByTagName("tr"))
}nCalcTmp.appendChild(nBody);
nBody.appendChild(nTr);
var jqColSizing=$("thead th",nCalcTmp);
if(jqColSizing.length===0){jqColSizing=$("tbody tr:eq(0)>td",nCalcTmp)
}var nThs=_fnGetUniqueThs(oSettings,nTheadClone);
iCorrector=0;
for(i=0;
i<iColums;
i++){var oColumn=oSettings.aoColumns[i];
if(oColumn.bVisible&&oColumn.sWidthOrig!==null&&oColumn.sWidthOrig!==""){nThs[i-iCorrector].style.width=_fnStringToCss(oColumn.sWidthOrig)
}else{if(oColumn.bVisible){nThs[i-iCorrector].style.width=""
}else{iCorrector++
}}}for(i=0;
i<iColums;
i++){if(oSettings.aoColumns[i].bVisible){var nTd=_fnGetWidestNode(oSettings,i);
if(nTd!==null){nTd=nTd.cloneNode(true);
if(oSettings.aoColumns[i].sContentPadding!==""){nTd.innerHTML+=oSettings.aoColumns[i].sContentPadding
}nTr.appendChild(nTd)
}}}nWrapper.appendChild(nCalcTmp);
if(oSettings.oScroll.sX!==""&&oSettings.oScroll.sXInner!==""){nCalcTmp.style.width=_fnStringToCss(oSettings.oScroll.sXInner)
}else{if(oSettings.oScroll.sX!==""){nCalcTmp.style.width="";
if($(nCalcTmp).width()<nWrapper.offsetWidth){nCalcTmp.style.width=_fnStringToCss(nWrapper.offsetWidth)
}}else{if(oSettings.oScroll.sY!==""){nCalcTmp.style.width=_fnStringToCss(nWrapper.offsetWidth)
}else{if(widthAttr){nCalcTmp.style.width=_fnStringToCss(widthAttr)
}}}}nCalcTmp.style.visibility="hidden";
_fnScrollingWidthAdjust(oSettings,nCalcTmp);
var oNodes=$("tbody tr:eq(0)",nCalcTmp).children();
if(oNodes.length===0){oNodes=_fnGetUniqueThs(oSettings,$("thead",nCalcTmp)[0])
}if(oSettings.oScroll.sX!==""){var iTotal=0;
iCorrector=0;
for(i=0;
i<oSettings.aoColumns.length;
i++){if(oSettings.aoColumns[i].bVisible){if(oSettings.aoColumns[i].sWidthOrig===null){iTotal+=$(oNodes[iCorrector]).outerWidth()
}else{iTotal+=parseInt(oSettings.aoColumns[i].sWidth.replace("px",""),10)+($(oNodes[iCorrector]).outerWidth()-$(oNodes[iCorrector]).width())
}iCorrector++
}}nCalcTmp.style.width=_fnStringToCss(iTotal);
oSettings.nTable.style.width=_fnStringToCss(iTotal)
}iCorrector=0;
for(i=0;
i<oSettings.aoColumns.length;
i++){if(oSettings.aoColumns[i].bVisible){iWidth=$(oNodes[iCorrector]).width();
if(iWidth!==null&&iWidth>0){oSettings.aoColumns[i].sWidth=_fnStringToCss(iWidth)
}iCorrector++
}}var cssWidth=$(nCalcTmp).css("width");
oSettings.nTable.style.width=(cssWidth.indexOf("%")!==-1)?cssWidth:_fnStringToCss($(nCalcTmp).outerWidth());
nCalcTmp.parentNode.removeChild(nCalcTmp)
}if(widthAttr){oSettings.nTable.style.width=_fnStringToCss(widthAttr)
}}function _fnScrollingWidthAdjust(oSettings,n){if(oSettings.oScroll.sX===""&&oSettings.oScroll.sY!==""){var iOrigWidth=$(n).width();
n.style.width=_fnStringToCss($(n).outerWidth()-oSettings.oScroll.iBarWidth)
}else{if(oSettings.oScroll.sX!==""){n.style.width=_fnStringToCss($(n).outerWidth())
}}}function _fnGetWidestNode(oSettings,iCol){var iMaxIndex=_fnGetMaxLenString(oSettings,iCol);
if(iMaxIndex<0){return null
}if(oSettings.aoData[iMaxIndex].nTr===null){var n=document.createElement("td");
n.innerHTML=_fnGetCellData(oSettings,iMaxIndex,iCol,"");
return n
}return _fnGetTdNodes(oSettings,iMaxIndex)[iCol]
}function _fnGetMaxLenString(oSettings,iCol){var iMax=-1;
var iMaxIndex=-1;
for(var i=0;
i<oSettings.aoData.length;
i++){var s=_fnGetCellData(oSettings,i,iCol,"display")+"";
s=s.replace(/<.*?>/g,"");
if(s.length>iMax){iMax=s.length;
iMaxIndex=i
}}return iMaxIndex
}function _fnStringToCss(s){if(s===null){return"0px"
}if(typeof s=="number"){if(s<0){return"0px"
}return s+"px"
}var c=s.charCodeAt(s.length-1);
if(c<48||c>57){return s
}return s+"px"
}function _fnScrollBarWidth(){var inner=document.createElement("p");
var style=inner.style;
style.width="100%";
style.height="200px";
style.padding="0px";
var outer=document.createElement("div");
style=outer.style;
style.position="absolute";
style.top="0px";
style.left="0px";
style.visibility="hidden";
style.width="200px";
style.height="150px";
style.padding="0px";
style.overflow="hidden";
outer.appendChild(inner);
document.body.appendChild(outer);
var w1=inner.offsetWidth;
outer.style.overflow="scroll";
var w2=inner.offsetWidth;
if(w1==w2){w2=outer.clientWidth
}document.body.removeChild(outer);
return(w1-w2)
}function _fnSort(oSettings,bApplyClasses){var i,iLen,j,jLen,k,kLen,sDataType,nTh,aaSort=[],aiOrig=[],oSort=DataTable.ext.oSort,aoData=oSettings.aoData,aoColumns=oSettings.aoColumns,oAria=oSettings.oLanguage.oAria;
if(!oSettings.oFeatures.bServerSide&&(oSettings.aaSorting.length!==0||oSettings.aaSortingFixed!==null)){aaSort=(oSettings.aaSortingFixed!==null)?oSettings.aaSortingFixed.concat(oSettings.aaSorting):oSettings.aaSorting.slice();
for(i=0;
i<aaSort.length;
i++){var iColumn=aaSort[i][0];
var iVisColumn=_fnColumnIndexToVisible(oSettings,iColumn);
sDataType=oSettings.aoColumns[iColumn].sSortDataType;
if(DataTable.ext.afnSortData[sDataType]){var aData=DataTable.ext.afnSortData[sDataType].call(oSettings.oInstance,oSettings,iColumn,iVisColumn);
if(aData.length===aoData.length){for(j=0,jLen=aoData.length;
j<jLen;
j++){_fnSetCellData(oSettings,j,iColumn,aData[j])
}}else{_fnLog(oSettings,0,"Returned data sort array (col "+iColumn+") is the wrong length")
}}}for(i=0,iLen=oSettings.aiDisplayMaster.length;
i<iLen;
i++){aiOrig[oSettings.aiDisplayMaster[i]]=i
}var iSortLen=aaSort.length;
var fnSortFormat,aDataSort;
for(i=0,iLen=aoData.length;
i<iLen;
i++){for(j=0;
j<iSortLen;
j++){aDataSort=aoColumns[aaSort[j][0]].aDataSort;
for(k=0,kLen=aDataSort.length;
k<kLen;
k++){sDataType=aoColumns[aDataSort[k]].sType;
fnSortFormat=oSort[(sDataType?sDataType:"string")+"-pre"];
aoData[i]._aSortData[aDataSort[k]]=fnSortFormat?fnSortFormat(_fnGetCellData(oSettings,i,aDataSort[k],"sort")):_fnGetCellData(oSettings,i,aDataSort[k],"sort")
}}}oSettings.aiDisplayMaster.sort(function(a,b){var k,l,lLen,iTest,aDataSort,sDataType;
for(k=0;
k<iSortLen;
k++){aDataSort=aoColumns[aaSort[k][0]].aDataSort;
for(l=0,lLen=aDataSort.length;
l<lLen;
l++){sDataType=aoColumns[aDataSort[l]].sType;
iTest=oSort[(sDataType?sDataType:"string")+"-"+aaSort[k][1]](aoData[a]._aSortData[aDataSort[l]],aoData[b]._aSortData[aDataSort[l]]);
if(iTest!==0){return iTest
}}}return oSort["numeric-asc"](aiOrig[a],aiOrig[b])
})
}if((bApplyClasses===undefined||bApplyClasses)&&!oSettings.oFeatures.bDeferRender){_fnSortingClasses(oSettings)
}for(i=0,iLen=oSettings.aoColumns.length;
i<iLen;
i++){var sTitle=aoColumns[i].sTitle.replace(/<.*?>/g,"");
nTh=aoColumns[i].nTh;
nTh.removeAttribute("aria-sort");
nTh.removeAttribute("aria-label");
if(aoColumns[i].bSortable){if(aaSort.length>0&&aaSort[0][0]==i){nTh.setAttribute("aria-sort",aaSort[0][1]=="asc"?"ascending":"descending");
var nextSort=(aoColumns[i].asSorting[aaSort[0][2]+1])?aoColumns[i].asSorting[aaSort[0][2]+1]:aoColumns[i].asSorting[0];
nTh.setAttribute("aria-label",sTitle+(nextSort=="asc"?oAria.sSortAscending:oAria.sSortDescending))
}else{nTh.setAttribute("aria-label",sTitle+(aoColumns[i].asSorting[0]=="asc"?oAria.sSortAscending:oAria.sSortDescending))
}}else{nTh.setAttribute("aria-label",sTitle)
}}oSettings.bSorted=true;
$(oSettings.oInstance).trigger("sort",oSettings);
if(oSettings.oFeatures.bFilter){_fnFilterComplete(oSettings,oSettings.oPreviousSearch,1)
}else{oSettings.aiDisplay=oSettings.aiDisplayMaster.slice();
oSettings._iDisplayStart=0;
_fnCalculateEnd(oSettings);
_fnDraw(oSettings)
}}function _fnSortAttachListener(oSettings,nNode,iDataIndex,fnCallback){_fnBindAction(nNode,{},function(e){if(oSettings.aoColumns[iDataIndex].bSortable===false){return 
}var fnInnerSorting=function(){var iColumn,iNextSort;
if(e.shiftKey){var bFound=false;
for(var i=0;
i<oSettings.aaSorting.length;
i++){if(oSettings.aaSorting[i][0]==iDataIndex){bFound=true;
iColumn=oSettings.aaSorting[i][0];
iNextSort=oSettings.aaSorting[i][2]+1;
if(!oSettings.aoColumns[iColumn].asSorting[iNextSort]){oSettings.aaSorting.splice(i,1)
}else{oSettings.aaSorting[i][1]=oSettings.aoColumns[iColumn].asSorting[iNextSort];
oSettings.aaSorting[i][2]=iNextSort
}break
}}if(bFound===false){oSettings.aaSorting.push([iDataIndex,oSettings.aoColumns[iDataIndex].asSorting[0],0])
}}else{if(oSettings.aaSorting.length==1&&oSettings.aaSorting[0][0]==iDataIndex){iColumn=oSettings.aaSorting[0][0];
iNextSort=oSettings.aaSorting[0][2]+1;
if(!oSettings.aoColumns[iColumn].asSorting[iNextSort]){iNextSort=0
}oSettings.aaSorting[0][1]=oSettings.aoColumns[iColumn].asSorting[iNextSort];
oSettings.aaSorting[0][2]=iNextSort
}else{oSettings.aaSorting.splice(0,oSettings.aaSorting.length);
oSettings.aaSorting.push([iDataIndex,oSettings.aoColumns[iDataIndex].asSorting[0],0])
}}_fnSort(oSettings)
};
if(!oSettings.oFeatures.bProcessing){fnInnerSorting()
}else{_fnProcessingDisplay(oSettings,true);
setTimeout(function(){fnInnerSorting();
if(!oSettings.oFeatures.bServerSide){_fnProcessingDisplay(oSettings,false)
}},0)
}if(typeof fnCallback=="function"){fnCallback(oSettings)
}})
}function _fnSortingClasses(oSettings){var i,iLen,j,jLen,iFound;
var aaSort,sClass;
var iColumns=oSettings.aoColumns.length;
var oClasses=oSettings.oClasses;
for(i=0;
i<iColumns;
i++){if(oSettings.aoColumns[i].bSortable){$(oSettings.aoColumns[i].nTh).removeClass(oClasses.sSortAsc+" "+oClasses.sSortDesc+" "+oSettings.aoColumns[i].sSortingClass)
}}if(oSettings.aaSortingFixed!==null){aaSort=oSettings.aaSortingFixed.concat(oSettings.aaSorting)
}else{aaSort=oSettings.aaSorting.slice()
}for(i=0;
i<oSettings.aoColumns.length;
i++){if(oSettings.aoColumns[i].bSortable){sClass=oSettings.aoColumns[i].sSortingClass;
iFound=-1;
for(j=0;
j<aaSort.length;
j++){if(aaSort[j][0]==i){sClass=(aaSort[j][1]=="asc")?oClasses.sSortAsc:oClasses.sSortDesc;
iFound=j;
break
}}$(oSettings.aoColumns[i].nTh).addClass(sClass);
if(oSettings.bJUI){var jqSpan=$("span."+oClasses.sSortIcon,oSettings.aoColumns[i].nTh);
jqSpan.removeClass(oClasses.sSortJUIAsc+" "+oClasses.sSortJUIDesc+" "+oClasses.sSortJUI+" "+oClasses.sSortJUIAscAllowed+" "+oClasses.sSortJUIDescAllowed);
var sSpanClass;
if(iFound==-1){sSpanClass=oSettings.aoColumns[i].sSortingClassJUI
}else{if(aaSort[iFound][1]=="asc"){sSpanClass=oClasses.sSortJUIAsc
}else{sSpanClass=oClasses.sSortJUIDesc
}}jqSpan.addClass(sSpanClass)
}}else{$(oSettings.aoColumns[i].nTh).addClass(oSettings.aoColumns[i].sSortingClass)
}}sClass=oClasses.sSortColumn;
if(oSettings.oFeatures.bSort&&oSettings.oFeatures.bSortClasses){var nTds=_fnGetTdNodes(oSettings);
var iClass,iTargetCol;
var asClasses=[];
for(i=0;
i<iColumns;
i++){asClasses.push("")
}for(i=0,iClass=1;
i<aaSort.length;
i++){iTargetCol=parseInt(aaSort[i][0],10);
asClasses[iTargetCol]=sClass+iClass;
if(iClass<3){iClass++
}}var reClass=new RegExp(sClass+"[123]");
var sTmpClass,sCurrentClass,sNewClass;
for(i=0,iLen=nTds.length;
i<iLen;
i++){iTargetCol=i%iColumns;
sCurrentClass=nTds[i].className;
sNewClass=asClasses[iTargetCol];
sTmpClass=sCurrentClass.replace(reClass,sNewClass);
if(sTmpClass!=sCurrentClass){nTds[i].className=$.trim(sTmpClass)
}else{if(sNewClass.length>0&&sCurrentClass.indexOf(sNewClass)==-1){nTds[i].className=sCurrentClass+" "+sNewClass
}}}}}function _fnSaveState(oSettings){if(!oSettings.oFeatures.bStateSave||oSettings.bDestroying){return 
}var i,iLen,bInfinite=oSettings.oScroll.bInfinite;
var oState={iCreate:new Date().getTime(),iStart:(bInfinite?0:oSettings._iDisplayStart),iEnd:(bInfinite?oSettings._iDisplayLength:oSettings._iDisplayEnd),iLength:oSettings._iDisplayLength,aaSorting:$.extend(true,[],oSettings.aaSorting),oSearch:$.extend(true,{},oSettings.oPreviousSearch),aoSearchCols:$.extend(true,[],oSettings.aoPreSearchCols),abVisCols:[]};
for(i=0,iLen=oSettings.aoColumns.length;
i<iLen;
i++){oState.abVisCols.push(oSettings.aoColumns[i].bVisible)
}_fnCallbackFire(oSettings,"aoStateSaveParams","stateSaveParams",[oSettings,oState]);
oSettings.fnStateSave.call(oSettings.oInstance,oSettings,oState)
}function _fnLoadState(oSettings,oInit){if(!oSettings.oFeatures.bStateSave){return 
}var oData=oSettings.fnStateLoad.call(oSettings.oInstance,oSettings);
if(!oData){return 
}var abStateLoad=_fnCallbackFire(oSettings,"aoStateLoadParams","stateLoadParams",[oSettings,oData]);
if($.inArray(false,abStateLoad)!==-1){return 
}oSettings.oLoadedState=$.extend(true,{},oData);
oSettings._iDisplayStart=oData.iStart;
oSettings.iInitDisplayStart=oData.iStart;
oSettings._iDisplayEnd=oData.iEnd;
oSettings._iDisplayLength=oData.iLength;
oSettings.aaSorting=oData.aaSorting.slice();
oSettings.saved_aaSorting=oData.aaSorting.slice();
$.extend(oSettings.oPreviousSearch,oData.oSearch);
$.extend(true,oSettings.aoPreSearchCols,oData.aoSearchCols);
oInit.saved_aoColumns=[];
for(var i=0;
i<oData.abVisCols.length;
i++){oInit.saved_aoColumns[i]={};
oInit.saved_aoColumns[i].bVisible=oData.abVisCols[i]
}_fnCallbackFire(oSettings,"aoStateLoaded","stateLoaded",[oSettings,oData])
}function _fnCreateCookie(sName,sValue,iSecs,sBaseName,fnCallback){var date=new Date();
date.setTime(date.getTime()+(iSecs*1000));
var aParts=window.location.pathname.split("/");
var sNameFile=sName+"_"+aParts.pop().replace(/[\/:]/g,"").toLowerCase();
var sFullCookie,oData;
if(fnCallback!==null){oData=(typeof $.parseJSON==="function")?$.parseJSON(sValue):eval("("+sValue+")");
sFullCookie=fnCallback(sNameFile,oData,date.toGMTString(),aParts.join("/")+"/")
}else{sFullCookie=sNameFile+"="+encodeURIComponent(sValue)+"; expires="+date.toGMTString()+"; path="+aParts.join("/")+"/"
}var aCookies=document.cookie.split(";"),iNewCookieLen=sFullCookie.split(";")[0].length,aOldCookies=[];
if(iNewCookieLen+document.cookie.length+10>4096){for(var i=0,iLen=aCookies.length;
i<iLen;
i++){if(aCookies[i].indexOf(sBaseName)!=-1){var aSplitCookie=aCookies[i].split("=");
try{oData=eval("("+decodeURIComponent(aSplitCookie[1])+")");
if(oData&&oData.iCreate){aOldCookies.push({name:aSplitCookie[0],time:oData.iCreate})
}}catch(e){}}}aOldCookies.sort(function(a,b){return b.time-a.time
});
while(iNewCookieLen+document.cookie.length+10>4096){if(aOldCookies.length===0){return 
}var old=aOldCookies.pop();
document.cookie=old.name+"=; expires=Thu, 01-Jan-1970 00:00:01 GMT; path="+aParts.join("/")+"/"
}}document.cookie=sFullCookie
}function _fnReadCookie(sName){var aParts=window.location.pathname.split("/"),sNameEQ=sName+"_"+aParts[aParts.length-1].replace(/[\/:]/g,"").toLowerCase()+"=",sCookieContents=document.cookie.split(";");
for(var i=0;
i<sCookieContents.length;
i++){var c=sCookieContents[i];
while(c.charAt(0)==" "){c=c.substring(1,c.length)
}if(c.indexOf(sNameEQ)===0){return decodeURIComponent(c.substring(sNameEQ.length,c.length))
}}return null
}function _fnSettingsFromNode(nTable){for(var i=0;
i<DataTable.settings.length;
i++){if(DataTable.settings[i].nTable===nTable){return DataTable.settings[i]
}}return null
}function _fnGetTrNodes(oSettings){var aNodes=[];
var aoData=oSettings.aoData;
for(var i=0,iLen=aoData.length;
i<iLen;
i++){if(aoData[i].nTr!==null){aNodes.push(aoData[i].nTr)
}}return aNodes
}function _fnGetTdNodes(oSettings,iIndividualRow){var anReturn=[];
var iCorrector;
var anTds,nTd;
var iRow,iRows=oSettings.aoData.length,iColumn,iColumns,oData,sNodeName,iStart=0,iEnd=iRows;
if(iIndividualRow!==undefined){iStart=iIndividualRow;
iEnd=iIndividualRow+1
}for(iRow=iStart;
iRow<iEnd;
iRow++){oData=oSettings.aoData[iRow];
if(oData.nTr!==null){anTds=[];
nTd=oData.nTr.firstChild;
while(nTd){sNodeName=nTd.nodeName.toLowerCase();
if(sNodeName=="td"||sNodeName=="th"){anTds.push(nTd)
}nTd=nTd.nextSibling
}iCorrector=0;
for(iColumn=0,iColumns=oSettings.aoColumns.length;
iColumn<iColumns;
iColumn++){if(oSettings.aoColumns[iColumn].bVisible){anReturn.push(anTds[iColumn-iCorrector])
}else{anReturn.push(oData._anHidden[iColumn]);
iCorrector++
}}}}return anReturn
}function _fnLog(oSettings,iLevel,sMesg){var sAlert=(oSettings===null)?"DataTables warning: "+sMesg:"DataTables warning (table id = '"+oSettings.sTableId+"'): "+sMesg;
if(iLevel===0){if(DataTable.ext.sErrMode=="alert"){alert(sAlert)
}else{throw new Error(sAlert)
}return 
}else{if(window.console&&console.log){console.log(sAlert)
}}}function _fnMap(oRet,oSrc,sName,sMappedName){if(sMappedName===undefined){sMappedName=sName
}if(oSrc[sName]!==undefined){oRet[sMappedName]=oSrc[sName]
}}function _fnExtend(oOut,oExtender){var val;
for(var prop in oExtender){if(oExtender.hasOwnProperty(prop)){val=oExtender[prop];
if(typeof oInit[prop]==="object"&&val!==null&&$.isArray(val)===false){$.extend(true,oOut[prop],val)
}else{oOut[prop]=val
}}}return oOut
}function _fnBindAction(n,oData,fn){$(n).bind("http://www.usfoods.com/etc/designs/usfoods/clientlibs/click.DT",oData,function(e){n.blur();
fn(e)
}).bind("http://www.usfoods.com/etc/designs/usfoods/clientlibs/keypress.DT",oData,function(e){if(e.which===13){fn(e)
}}).bind("http://www.usfoods.com/etc/designs/usfoods/clientlibs/selectstart.DT",function(){return false
})
}function _fnCallbackReg(oSettings,sStore,fn,sName){if(fn){oSettings[sStore].push({fn:fn,sName:sName})
}}function _fnCallbackFire(oSettings,sStore,sTrigger,aArgs){var aoStore=oSettings[sStore];
var aRet=[];
for(var i=aoStore.length-1;
i>=0;
i--){aRet.push(aoStore[i].fn.apply(oSettings.oInstance,aArgs))
}if(sTrigger!==null){$(oSettings.oInstance).trigger(sTrigger,aArgs)
}return aRet
}var _fnJsonString=(window.JSON)?JSON.stringify:function(o){var sType=typeof o;
if(sType!=="object"||o===null){if(sType==="string"){o='"'+o+'"'
}return o+""
}var sProp,mValue,json=[],bArr=$.isArray(o);
for(sProp in o){mValue=o[sProp];
sType=typeof mValue;
if(sType==="string"){mValue='"'+mValue+'"'
}else{if(sType==="object"&&mValue!==null){mValue=_fnJsonString(mValue)
}}json.push((bArr?"":'"'+sProp+'":')+mValue)
}return(bArr?"[":"{")+json+(bArr?"]":"}")
};
function _fnBrowserDetect(oSettings){var n=$('<div style="position:absolute; top:0; left:0; height:1px; width:1px; overflow:hidden"><div style="position:absolute; top:1px; left:1px; width:100px; overflow:scroll;"><div id="DT_BrowserTest" style="width:100%; height:10px;"></div></div></div>')[0];
document.body.appendChild(n);
oSettings.oBrowser.bScrollOversize=$("#DT_BrowserTest",n)[0].offsetWidth===100?true:false;
document.body.removeChild(n)
}this.$=function(sSelector,oOpts){var i,iLen,a=[],tr;
var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);
var aoData=oSettings.aoData;
var aiDisplay=oSettings.aiDisplay;
var aiDisplayMaster=oSettings.aiDisplayMaster;
if(!oOpts){oOpts={}
}oOpts=$.extend({},{filter:"none",order:"current",page:"all"},oOpts);
if(oOpts.page=="current"){for(i=oSettings._iDisplayStart,iLen=oSettings.fnDisplayEnd();
i<iLen;
i++){tr=aoData[aiDisplay[i]].nTr;
if(tr){a.push(tr)
}}}else{if(oOpts.order=="current"&&oOpts.filter=="none"){for(i=0,iLen=aiDisplayMaster.length;
i<iLen;
i++){tr=aoData[aiDisplayMaster[i]].nTr;
if(tr){a.push(tr)
}}}else{if(oOpts.order=="current"&&oOpts.filter=="applied"){for(i=0,iLen=aiDisplay.length;
i<iLen;
i++){tr=aoData[aiDisplay[i]].nTr;
if(tr){a.push(tr)
}}}else{if(oOpts.order=="original"&&oOpts.filter=="none"){for(i=0,iLen=aoData.length;
i<iLen;
i++){tr=aoData[i].nTr;
if(tr){a.push(tr)
}}}else{if(oOpts.order=="original"&&oOpts.filter=="applied"){for(i=0,iLen=aoData.length;
i<iLen;
i++){tr=aoData[i].nTr;
if($.inArray(i,aiDisplay)!==-1&&tr){a.push(tr)
}}}else{_fnLog(oSettings,1,"Unknown selection options")
}}}}}var jqA=$(a);
var jqTRs=jqA.filter(sSelector);
var jqDescendants=jqA.find(sSelector);
return $([].concat($.makeArray(jqTRs),$.makeArray(jqDescendants)))
};
this._=function(sSelector,oOpts){var aOut=[];
var i,iLen,iIndex;
var aTrs=this.$(sSelector,oOpts);
for(i=0,iLen=aTrs.length;
i<iLen;
i++){aOut.push(this.fnGetData(aTrs[i]))
}return aOut
};
this.fnAddData=function(mData,bRedraw){if(mData.length===0){return[]
}var aiReturn=[];
var iTest;
var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);
if(typeof mData[0]==="object"&&mData[0]!==null){for(var i=0;
i<mData.length;
i++){iTest=_fnAddData(oSettings,mData[i]);
if(iTest==-1){return aiReturn
}aiReturn.push(iTest)
}}else{iTest=_fnAddData(oSettings,mData);
if(iTest==-1){return aiReturn
}aiReturn.push(iTest)
}oSettings.aiDisplay=oSettings.aiDisplayMaster.slice();
if(bRedraw===undefined||bRedraw){_fnReDraw(oSettings)
}return aiReturn
};
this.fnAdjustColumnSizing=function(bRedraw){var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);
_fnAdjustColumnSizing(oSettings);
if(bRedraw===undefined||bRedraw){this.fnDraw(false)
}else{if(oSettings.oScroll.sX!==""||oSettings.oScroll.sY!==""){this.oApi._fnScrollDraw(oSettings)
}}};
this.fnClearTable=function(bRedraw){var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);
_fnClearTable(oSettings);
if(bRedraw===undefined||bRedraw){_fnDraw(oSettings)
}};
this.fnClose=function(nTr){var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);
for(var i=0;
i<oSettings.aoOpenRows.length;
i++){if(oSettings.aoOpenRows[i].nParent==nTr){var nTrParent=oSettings.aoOpenRows[i].nTr.parentNode;
if(nTrParent){nTrParent.removeChild(oSettings.aoOpenRows[i].nTr)
}oSettings.aoOpenRows.splice(i,1);
return 0
}}return 1
};
this.fnDeleteRow=function(mTarget,fnCallBack,bRedraw){var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);
var i,iLen,iAODataIndex;
iAODataIndex=(typeof mTarget==="object")?_fnNodeToDataIndex(oSettings,mTarget):mTarget;
var oData=oSettings.aoData.splice(iAODataIndex,1);
for(i=0,iLen=oSettings.aoData.length;
i<iLen;
i++){if(oSettings.aoData[i].nTr!==null){oSettings.aoData[i].nTr._DT_RowIndex=i
}}var iDisplayIndex=$.inArray(iAODataIndex,oSettings.aiDisplay);
oSettings.asDataSearch.splice(iDisplayIndex,1);
_fnDeleteIndex(oSettings.aiDisplayMaster,iAODataIndex);
_fnDeleteIndex(oSettings.aiDisplay,iAODataIndex);
if(typeof fnCallBack==="function"){fnCallBack.call(this,oSettings,oData)
}if(oSettings._iDisplayStart>=oSettings.fnRecordsDisplay()){oSettings._iDisplayStart-=oSettings._iDisplayLength;
if(oSettings._iDisplayStart<0){oSettings._iDisplayStart=0
}}if(bRedraw===undefined||bRedraw){_fnCalculateEnd(oSettings);
_fnDraw(oSettings)
}return oData
};
this.fnDestroy=function(bRemove){var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);
var nOrig=oSettings.nTableWrapper.parentNode;
var nBody=oSettings.nTBody;
var i,iLen;
bRemove=(bRemove===undefined)?false:bRemove;
oSettings.bDestroying=true;
_fnCallbackFire(oSettings,"aoDestroyCallback","destroy",[oSettings]);
if(!bRemove){for(i=0,iLen=oSettings.aoColumns.length;
i<iLen;
i++){if(oSettings.aoColumns[i].bVisible===false){this.fnSetColumnVis(i,true)
}}}$(oSettings.nTableWrapper).find("*").andSelf().unbind(".DT");
$("tbody>tr>td."+oSettings.oClasses.sRowEmpty,oSettings.nTable).parent().remove();
if(oSettings.nTable!=oSettings.nTHead.parentNode){$(oSettings.nTable).children("thead").remove();
oSettings.nTable.appendChild(oSettings.nTHead)
}if(oSettings.nTFoot&&oSettings.nTable!=oSettings.nTFoot.parentNode){$(oSettings.nTable).children("tfoot").remove();
oSettings.nTable.appendChild(oSettings.nTFoot)
}oSettings.nTable.parentNode.removeChild(oSettings.nTable);
$(oSettings.nTableWrapper).remove();
oSettings.aaSorting=[];
oSettings.aaSortingFixed=[];
_fnSortingClasses(oSettings);
$(_fnGetTrNodes(oSettings)).removeClass(oSettings.asStripeClasses.join(" "));
$("th, td",oSettings.nTHead).removeClass([oSettings.oClasses.sSortable,oSettings.oClasses.sSortableAsc,oSettings.oClasses.sSortableDesc,oSettings.oClasses.sSortableNone].join(" "));
if(oSettings.bJUI){$("th span."+oSettings.oClasses.sSortIcon+", td span."+oSettings.oClasses.sSortIcon,oSettings.nTHead).remove();
$("th, td",oSettings.nTHead).each(function(){var jqWrapper=$("div."+oSettings.oClasses.sSortJUIWrapper,this);
var kids=jqWrapper.contents();
$(this).append(kids);
jqWrapper.remove()
})
}if(!bRemove&&oSettings.nTableReinsertBefore){nOrig.insertBefore(oSettings.nTable,oSettings.nTableReinsertBefore)
}else{if(!bRemove){nOrig.appendChild(oSettings.nTable)
}}for(i=0,iLen=oSettings.aoData.length;
i<iLen;
i++){if(oSettings.aoData[i].nTr!==null){nBody.appendChild(oSettings.aoData[i].nTr)
}}if(oSettings.oFeatures.bAutoWidth===true){oSettings.nTable.style.width=_fnStringToCss(oSettings.sDestroyWidth)
}iLen=oSettings.asDestroyStripes.length;
if(iLen){var anRows=$(nBody).children("tr");
for(i=0;
i<iLen;
i++){anRows.filter(":nth-child("+iLen+"n + "+i+")").addClass(oSettings.asDestroyStripes[i])
}}for(i=0,iLen=DataTable.settings.length;
i<iLen;
i++){if(DataTable.settings[i]==oSettings){DataTable.settings.splice(i,1)
}}oSettings=null;
oInit=null
};
this.fnDraw=function(bComplete){var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);
if(bComplete===false){_fnCalculateEnd(oSettings);
_fnDraw(oSettings)
}else{_fnReDraw(oSettings)
}};
this.fnFilter=function(sInput,iColumn,bRegex,bSmart,bShowGlobal,bCaseInsensitive){var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);
if(!oSettings.oFeatures.bFilter){return 
}if(bRegex===undefined||bRegex===null){bRegex=false
}if(bSmart===undefined||bSmart===null){bSmart=true
}if(bShowGlobal===undefined||bShowGlobal===null){bShowGlobal=true
}if(bCaseInsensitive===undefined||bCaseInsensitive===null){bCaseInsensitive=true
}if(iColumn===undefined||iColumn===null){_fnFilterComplete(oSettings,{sSearch:sInput+"",bRegex:bRegex,bSmart:bSmart,bCaseInsensitive:bCaseInsensitive},1);
if(bShowGlobal&&oSettings.aanFeatures.f){var n=oSettings.aanFeatures.f;
for(var i=0,iLen=n.length;
i<iLen;
i++){try{if(n[i]._DT_Input!=document.activeElement){$(n[i]._DT_Input).val(sInput)
}}catch(e){$(n[i]._DT_Input).val(sInput)
}}}}else{$.extend(oSettings.aoPreSearchCols[iColumn],{sSearch:sInput+"",bRegex:bRegex,bSmart:bSmart,bCaseInsensitive:bCaseInsensitive});
_fnFilterComplete(oSettings,oSettings.oPreviousSearch,1)
}};
this.fnGetData=function(mRow,iCol){var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);
if(mRow!==undefined){var iRow=mRow;
if(typeof mRow==="object"){var sNode=mRow.nodeName.toLowerCase();
if(sNode==="tr"){iRow=_fnNodeToDataIndex(oSettings,mRow)
}else{if(sNode==="td"){iRow=_fnNodeToDataIndex(oSettings,mRow.parentNode);
iCol=_fnNodeToColumnIndex(oSettings,iRow,mRow)
}}}if(iCol!==undefined){return _fnGetCellData(oSettings,iRow,iCol,"")
}return(oSettings.aoData[iRow]!==undefined)?oSettings.aoData[iRow]._aData:null
}return _fnGetDataMaster(oSettings)
};
this.fnGetNodes=function(iRow){var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);
if(iRow!==undefined){return(oSettings.aoData[iRow]!==undefined)?oSettings.aoData[iRow].nTr:null
}return _fnGetTrNodes(oSettings)
};
this.fnGetPosition=function(nNode){var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);
var sNodeName=nNode.nodeName.toUpperCase();
if(sNodeName=="TR"){return _fnNodeToDataIndex(oSettings,nNode)
}else{if(sNodeName=="TD"||sNodeName=="TH"){var iDataIndex=_fnNodeToDataIndex(oSettings,nNode.parentNode);
var iColumnIndex=_fnNodeToColumnIndex(oSettings,iDataIndex,nNode);
return[iDataIndex,_fnColumnIndexToVisible(oSettings,iColumnIndex),iColumnIndex]
}}return null
};
this.fnIsOpen=function(nTr){var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);
var aoOpenRows=oSettings.aoOpenRows;
for(var i=0;
i<oSettings.aoOpenRows.length;
i++){if(oSettings.aoOpenRows[i].nParent==nTr){return true
}}return false
};
this.fnOpen=function(nTr,mHtml,sClass){var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);
var nTableRows=_fnGetTrNodes(oSettings);
if($.inArray(nTr,nTableRows)===-1){return 
}this.fnClose(nTr);
var nNewRow=document.createElement("tr");
var nNewCell=document.createElement("td");
nNewRow.appendChild(nNewCell);
nNewCell.className=sClass;
nNewCell.colSpan=_fnVisbleColumns(oSettings);
if(typeof mHtml==="string"){nNewCell.innerHTML=mHtml
}else{$(nNewCell).html(mHtml)
}var nTrs=$("tr",oSettings.nTBody);
if($.inArray(nTr,nTrs)!=-1){$(nNewRow).insertAfter(nTr)
}oSettings.aoOpenRows.push({nTr:nNewRow,nParent:nTr});
return nNewRow
};
this.fnPageChange=function(mAction,bRedraw){var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);
_fnPageChange(oSettings,mAction);
_fnCalculateEnd(oSettings);
if(bRedraw===undefined||bRedraw){_fnDraw(oSettings)
}};
this.fnSetColumnVis=function(iCol,bShow,bRedraw){var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);
var i,iLen;
var aoColumns=oSettings.aoColumns;
var aoData=oSettings.aoData;
var nTd,bAppend,iBefore;
if(aoColumns[iCol].bVisible==bShow){return 
}if(bShow){var iInsert=0;
for(i=0;
i<iCol;
i++){if(aoColumns[i].bVisible){iInsert++
}}bAppend=(iInsert>=_fnVisbleColumns(oSettings));
if(!bAppend){for(i=iCol;
i<aoColumns.length;
i++){if(aoColumns[i].bVisible){iBefore=i;
break
}}}for(i=0,iLen=aoData.length;
i<iLen;
i++){if(aoData[i].nTr!==null){if(bAppend){aoData[i].nTr.appendChild(aoData[i]._anHidden[iCol])
}else{aoData[i].nTr.insertBefore(aoData[i]._anHidden[iCol],_fnGetTdNodes(oSettings,i)[iBefore])
}}}}else{for(i=0,iLen=aoData.length;
i<iLen;
i++){if(aoData[i].nTr!==null){nTd=_fnGetTdNodes(oSettings,i)[iCol];
aoData[i]._anHidden[iCol]=nTd;
nTd.parentNode.removeChild(nTd)
}}}aoColumns[iCol].bVisible=bShow;
_fnDrawHead(oSettings,oSettings.aoHeader);
if(oSettings.nTFoot){_fnDrawHead(oSettings,oSettings.aoFooter)
}for(i=0,iLen=oSettings.aoOpenRows.length;
i<iLen;
i++){oSettings.aoOpenRows[i].nTr.colSpan=_fnVisbleColumns(oSettings)
}if(bRedraw===undefined||bRedraw){_fnAdjustColumnSizing(oSettings);
_fnDraw(oSettings)
}_fnSaveState(oSettings)
};
this.fnSettings=function(){return _fnSettingsFromNode(this[DataTable.ext.iApiIndex])
};
this.fnSort=function(aaSort){var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);
oSettings.aaSorting=aaSort;
_fnSort(oSettings)
};
this.fnSortListener=function(nNode,iColumn,fnCallback){_fnSortAttachListener(_fnSettingsFromNode(this[DataTable.ext.iApiIndex]),nNode,iColumn,fnCallback)
};
this.fnUpdate=function(mData,mRow,iColumn,bRedraw,bAction){var oSettings=_fnSettingsFromNode(this[DataTable.ext.iApiIndex]);
var i,iLen,sDisplay;
var iRow=(typeof mRow==="object")?_fnNodeToDataIndex(oSettings,mRow):mRow;
if($.isArray(mData)&&iColumn===undefined){oSettings.aoData[iRow]._aData=mData.slice();
for(i=0;
i<oSettings.aoColumns.length;
i++){this.fnUpdate(_fnGetCellData(oSettings,iRow,i),iRow,i,false,false)
}}else{if($.isPlainObject(mData)&&iColumn===undefined){oSettings.aoData[iRow]._aData=$.extend(true,{},mData);
for(i=0;
i<oSettings.aoColumns.length;
i++){this.fnUpdate(_fnGetCellData(oSettings,iRow,i),iRow,i,false,false)
}}else{_fnSetCellData(oSettings,iRow,iColumn,mData);
sDisplay=_fnGetCellData(oSettings,iRow,iColumn,"display");
var oCol=oSettings.aoColumns[iColumn];
if(oCol.fnRender!==null){sDisplay=_fnRender(oSettings,iRow,iColumn);
if(oCol.bUseRendered){_fnSetCellData(oSettings,iRow,iColumn,sDisplay)
}}if(oSettings.aoData[iRow].nTr!==null){_fnGetTdNodes(oSettings,iRow)[iColumn].innerHTML=sDisplay
}}}var iDisplayIndex=$.inArray(iRow,oSettings.aiDisplay);
oSettings.asDataSearch[iDisplayIndex]=_fnBuildSearchRow(oSettings,_fnGetRowData(oSettings,iRow,"filter",_fnGetColumns(oSettings,"bSearchable")));
if(bAction===undefined||bAction){_fnAdjustColumnSizing(oSettings)
}if(bRedraw===undefined||bRedraw){_fnReDraw(oSettings)
}return 0
};
this.fnVersionCheck=DataTable.ext.fnVersionCheck;
function _fnExternApiFunc(sFunc){return function(){var aArgs=[_fnSettingsFromNode(this[DataTable.ext.iApiIndex])].concat(Array.prototype.slice.call(arguments));
return DataTable.ext.oApi[sFunc].apply(this,aArgs)
}
}this.oApi={_fnExternApiFunc:_fnExternApiFunc,_fnInitialise:_fnInitialise,_fnInitComplete:_fnInitComplete,_fnLanguageCompat:_fnLanguageCompat,_fnAddColumn:_fnAddColumn,_fnColumnOptions:_fnColumnOptions,_fnAddData:_fnAddData,_fnCreateTr:_fnCreateTr,_fnGatherData:_fnGatherData,_fnBuildHead:_fnBuildHead,_fnDrawHead:_fnDrawHead,_fnDraw:_fnDraw,_fnReDraw:_fnReDraw,_fnAjaxUpdate:_fnAjaxUpdate,_fnAjaxParameters:_fnAjaxParameters,_fnAjaxUpdateDraw:_fnAjaxUpdateDraw,_fnServerParams:_fnServerParams,_fnAddOptionsHtml:_fnAddOptionsHtml,_fnFeatureHtmlTable:_fnFeatureHtmlTable,_fnScrollDraw:_fnScrollDraw,_fnAdjustColumnSizing:_fnAdjustColumnSizing,_fnFeatureHtmlFilter:_fnFeatureHtmlFilter,_fnFilterComplete:_fnFilterComplete,_fnFilterCustom:_fnFilterCustom,_fnFilterColumn:_fnFilterColumn,_fnFilter:_fnFilter,_fnBuildSearchArray:_fnBuildSearchArray,_fnBuildSearchRow:_fnBuildSearchRow,_fnFilterCreateSearch:_fnFilterCreateSearch,_fnDataToSearch:_fnDataToSearch,_fnSort:_fnSort,_fnSortAttachListener:_fnSortAttachListener,_fnSortingClasses:_fnSortingClasses,_fnFeatureHtmlPaginate:_fnFeatureHtmlPaginate,_fnPageChange:_fnPageChange,_fnFeatureHtmlInfo:_fnFeatureHtmlInfo,_fnUpdateInfo:_fnUpdateInfo,_fnFeatureHtmlLength:_fnFeatureHtmlLength,_fnFeatureHtmlProcessing:_fnFeatureHtmlProcessing,_fnProcessingDisplay:_fnProcessingDisplay,_fnVisibleToColumnIndex:_fnVisibleToColumnIndex,_fnColumnIndexToVisible:_fnColumnIndexToVisible,_fnNodeToDataIndex:_fnNodeToDataIndex,_fnVisbleColumns:_fnVisbleColumns,_fnCalculateEnd:_fnCalculateEnd,_fnConvertToWidth:_fnConvertToWidth,_fnCalculateColumnWidths:_fnCalculateColumnWidths,_fnScrollingWidthAdjust:_fnScrollingWidthAdjust,_fnGetWidestNode:_fnGetWidestNode,_fnGetMaxLenString:_fnGetMaxLenString,_fnStringToCss:_fnStringToCss,_fnDetectType:_fnDetectType,_fnSettingsFromNode:_fnSettingsFromNode,_fnGetDataMaster:_fnGetDataMaster,_fnGetTrNodes:_fnGetTrNodes,_fnGetTdNodes:_fnGetTdNodes,_fnEscapeRegex:_fnEscapeRegex,_fnDeleteIndex:_fnDeleteIndex,_fnReOrderIndex:_fnReOrderIndex,_fnColumnOrdering:_fnColumnOrdering,_fnLog:_fnLog,_fnClearTable:_fnClearTable,_fnSaveState:_fnSaveState,_fnLoadState:_fnLoadState,_fnCreateCookie:_fnCreateCookie,_fnReadCookie:_fnReadCookie,_fnDetectHeader:_fnDetectHeader,_fnGetUniqueThs:_fnGetUniqueThs,_fnScrollBarWidth:_fnScrollBarWidth,_fnApplyToChildren:_fnApplyToChildren,_fnMap:_fnMap,_fnGetRowData:_fnGetRowData,_fnGetCellData:_fnGetCellData,_fnSetCellData:_fnSetCellData,_fnGetObjectDataFn:_fnGetObjectDataFn,_fnSetObjectDataFn:_fnSetObjectDataFn,_fnApplyColumnDefs:_fnApplyColumnDefs,_fnBindAction:_fnBindAction,_fnExtend:_fnExtend,_fnCallbackReg:_fnCallbackReg,_fnCallbackFire:_fnCallbackFire,_fnJsonString:_fnJsonString,_fnRender:_fnRender,_fnNodeToColumnIndex:_fnNodeToColumnIndex,_fnInfoMacros:_fnInfoMacros,_fnBrowserDetect:_fnBrowserDetect,_fnGetColumns:_fnGetColumns};
$.extend(DataTable.ext.oApi,this.oApi);
for(var sFunc in DataTable.ext.oApi){if(sFunc){this[sFunc]=_fnExternApiFunc(sFunc)
}}var _that=this;
this.each(function(){var i=0,iLen,j,jLen,k,kLen;
var sId=this.getAttribute("id");
var bInitHandedOff=false;
var bUsePassedData=false;
if(this.nodeName.toLowerCase()!="table"){_fnLog(null,0,"Attempted to initialise DataTables on a node which is not a table: "+this.nodeName);
return 
}for(i=0,iLen=DataTable.settings.length;
i<iLen;
i++){if(DataTable.settings[i].nTable==this){if(oInit===undefined||oInit.bRetrieve){return DataTable.settings[i].oInstance
}else{if(oInit.bDestroy){DataTable.settings[i].oInstance.fnDestroy();
break
}else{_fnLog(DataTable.settings[i],0,"Cannot reinitialise DataTable.\n\nTo retrieve the DataTables object for this table, pass no arguments or see the docs for bRetrieve and bDestroy");
return 
}}}if(DataTable.settings[i].sTableId==this.id){DataTable.settings.splice(i,1);
break
}}if(sId===null||sId===""){sId="DataTables_Table_"+(DataTable.ext._oExternConfig.iNextUnique++);
this.id=sId
}var oSettings=$.extend(true,{},DataTable.models.oSettings,{nTable:this,oApi:_that.oApi,oInit:oInit,sDestroyWidth:$(this).width(),sInstance:sId,sTableId:sId});
DataTable.settings.push(oSettings);
oSettings.oInstance=(_that.length===1)?_that:$(this).dataTable();
if(!oInit){oInit={}
}if(oInit.oLanguage){_fnLanguageCompat(oInit.oLanguage)
}oInit=_fnExtend($.extend(true,{},DataTable.defaults),oInit);
_fnMap(oSettings.oFeatures,oInit,"bPaginate");
_fnMap(oSettings.oFeatures,oInit,"bLengthChange");
_fnMap(oSettings.oFeatures,oInit,"bFilter");
_fnMap(oSettings.oFeatures,oInit,"bSort");
_fnMap(oSettings.oFeatures,oInit,"bInfo");
_fnMap(oSettings.oFeatures,oInit,"bProcessing");
_fnMap(oSettings.oFeatures,oInit,"bAutoWidth");
_fnMap(oSettings.oFeatures,oInit,"bSortClasses");
_fnMap(oSettings.oFeatures,oInit,"bServerSide");
_fnMap(oSettings.oFeatures,oInit,"bDeferRender");
_fnMap(oSettings.oScroll,oInit,"sScrollX","sX");
_fnMap(oSettings.oScroll,oInit,"sScrollXInner","sXInner");
_fnMap(oSettings.oScroll,oInit,"sScrollY","sY");
_fnMap(oSettings.oScroll,oInit,"bScrollCollapse","bCollapse");
_fnMap(oSettings.oScroll,oInit,"bScrollInfinite","bInfinite");
_fnMap(oSettings.oScroll,oInit,"iScrollLoadGap","iLoadGap");
_fnMap(oSettings.oScroll,oInit,"bScrollAutoCss","bAutoCss");
_fnMap(oSettings,oInit,"asStripeClasses");
_fnMap(oSettings,oInit,"asStripClasses","asStripeClasses");
_fnMap(oSettings,oInit,"fnServerData");
_fnMap(oSettings,oInit,"fnFormatNumber");
_fnMap(oSettings,oInit,"sServerMethod");
_fnMap(oSettings,oInit,"aaSorting");
_fnMap(oSettings,oInit,"aaSortingFixed");
_fnMap(oSettings,oInit,"aLengthMenu");
_fnMap(oSettings,oInit,"sPaginationType");
_fnMap(oSettings,oInit,"sAjaxSource");
_fnMap(oSettings,oInit,"sAjaxDataProp");
_fnMap(oSettings,oInit,"iCookieDuration");
_fnMap(oSettings,oInit,"sCookiePrefix");
_fnMap(oSettings,oInit,"sDom");
_fnMap(oSettings,oInit,"bSortCellsTop");
_fnMap(oSettings,oInit,"iTabIndex");
_fnMap(oSettings,oInit,"oSearch","oPreviousSearch");
_fnMap(oSettings,oInit,"aoSearchCols","aoPreSearchCols");
_fnMap(oSettings,oInit,"iDisplayLength","_iDisplayLength");
_fnMap(oSettings,oInit,"bJQueryUI","bJUI");
_fnMap(oSettings,oInit,"fnCookieCallback");
_fnMap(oSettings,oInit,"fnStateLoad");
_fnMap(oSettings,oInit,"fnStateSave");
_fnMap(oSettings.oLanguage,oInit,"fnInfoCallback");
_fnCallbackReg(oSettings,"aoDrawCallback",oInit.fnDrawCallback,"user");
_fnCallbackReg(oSettings,"aoServerParams",oInit.fnServerParams,"user");
_fnCallbackReg(oSettings,"aoStateSaveParams",oInit.fnStateSaveParams,"user");
_fnCallbackReg(oSettings,"aoStateLoadParams",oInit.fnStateLoadParams,"user");
_fnCallbackReg(oSettings,"aoStateLoaded",oInit.fnStateLoaded,"user");
_fnCallbackReg(oSettings,"aoRowCallback",oInit.fnRowCallback,"user");
_fnCallbackReg(oSettings,"aoRowCreatedCallback",oInit.fnCreatedRow,"user");
_fnCallbackReg(oSettings,"aoHeaderCallback",oInit.fnHeaderCallback,"user");
_fnCallbackReg(oSettings,"aoFooterCallback",oInit.fnFooterCallback,"user");
_fnCallbackReg(oSettings,"aoInitComplete",oInit.fnInitComplete,"user");
_fnCallbackReg(oSettings,"aoPreDrawCallback",oInit.fnPreDrawCallback,"user");
if(oSettings.oFeatures.bServerSide&&oSettings.oFeatures.bSort&&oSettings.oFeatures.bSortClasses){_fnCallbackReg(oSettings,"aoDrawCallback",_fnSortingClasses,"server_side_sort_classes")
}else{if(oSettings.oFeatures.bDeferRender){_fnCallbackReg(oSettings,"aoDrawCallback",_fnSortingClasses,"defer_sort_classes")
}}if(oInit.bJQueryUI){$.extend(oSettings.oClasses,DataTable.ext.oJUIClasses);
if(oInit.sDom===DataTable.defaults.sDom&&DataTable.defaults.sDom==="lfrtip"){oSettings.sDom='<"H"lfr>t<"F"ip>'
}}else{$.extend(oSettings.oClasses,DataTable.ext.oStdClasses)
}$(this).addClass(oSettings.oClasses.sTable);
if(oSettings.oScroll.sX!==""||oSettings.oScroll.sY!==""){oSettings.oScroll.iBarWidth=_fnScrollBarWidth()
}if(oSettings.iInitDisplayStart===undefined){oSettings.iInitDisplayStart=oInit.iDisplayStart;
oSettings._iDisplayStart=oInit.iDisplayStart
}if(oInit.bStateSave){oSettings.oFeatures.bStateSave=true;
_fnLoadState(oSettings,oInit);
_fnCallbackReg(oSettings,"aoDrawCallback",_fnSaveState,"state_save")
}if(oInit.iDeferLoading!==null){oSettings.bDeferLoading=true;
var tmp=$.isArray(oInit.iDeferLoading);
oSettings._iRecordsDisplay=tmp?oInit.iDeferLoading[0]:oInit.iDeferLoading;
oSettings._iRecordsTotal=tmp?oInit.iDeferLoading[1]:oInit.iDeferLoading
}if(oInit.aaData!==null){bUsePassedData=true
}if(oInit.oLanguage.sUrl!==""){oSettings.oLanguage.sUrl=oInit.oLanguage.sUrl;
$.getJSON(oSettings.oLanguage.sUrl,null,function(json){_fnLanguageCompat(json);
$.extend(true,oSettings.oLanguage,oInit.oLanguage,json);
_fnInitialise(oSettings)
});
bInitHandedOff=true
}else{$.extend(true,oSettings.oLanguage,oInit.oLanguage)
}if(oInit.asStripeClasses===null){oSettings.asStripeClasses=[oSettings.oClasses.sStripeOdd,oSettings.oClasses.sStripeEven]
}iLen=oSettings.asStripeClasses.length;
oSettings.asDestroyStripes=[];
if(iLen){var bStripeRemove=false;
var anRows=$(this).children("tbody").children("tr:lt("+iLen+")");
for(i=0;
i<iLen;
i++){if(anRows.hasClass(oSettings.asStripeClasses[i])){bStripeRemove=true;
oSettings.asDestroyStripes.push(oSettings.asStripeClasses[i])
}}if(bStripeRemove){anRows.removeClass(oSettings.asStripeClasses.join(" "))
}}var anThs=[];
var aoColumnsInit;
var nThead=this.getElementsByTagName("thead");
if(nThead.length!==0){_fnDetectHeader(oSettings.aoHeader,nThead[0]);
anThs=_fnGetUniqueThs(oSettings)
}if(oInit.aoColumns===null){aoColumnsInit=[];
for(i=0,iLen=anThs.length;
i<iLen;
i++){aoColumnsInit.push(null)
}}else{aoColumnsInit=oInit.aoColumns
}for(i=0,iLen=aoColumnsInit.length;
i<iLen;
i++){if(oInit.saved_aoColumns!==undefined&&oInit.saved_aoColumns.length==iLen){if(aoColumnsInit[i]===null){aoColumnsInit[i]={}
}aoColumnsInit[i].bVisible=oInit.saved_aoColumns[i].bVisible
}_fnAddColumn(oSettings,anThs?anThs[i]:null)
}_fnApplyColumnDefs(oSettings,oInit.aoColumnDefs,aoColumnsInit,function(iCol,oDef){_fnColumnOptions(oSettings,iCol,oDef)
});
for(i=0,iLen=oSettings.aaSorting.length;
i<iLen;
i++){if(oSettings.aaSorting[i][0]>=oSettings.aoColumns.length){oSettings.aaSorting[i][0]=0
}var oColumn=oSettings.aoColumns[oSettings.aaSorting[i][0]];
if(oSettings.aaSorting[i][2]===undefined){oSettings.aaSorting[i][2]=0
}if(oInit.aaSorting===undefined&&oSettings.saved_aaSorting===undefined){oSettings.aaSorting[i][1]=oColumn.asSorting[0]
}for(j=0,jLen=oColumn.asSorting.length;
j<jLen;
j++){if(oSettings.aaSorting[i][1]==oColumn.asSorting[j]){oSettings.aaSorting[i][2]=j;
break
}}}_fnSortingClasses(oSettings);
_fnBrowserDetect(oSettings);
var captions=$(this).children("caption").each(function(){this._captionSide=$(this).css("caption-side")
});
var thead=$(this).children("thead");
if(thead.length===0){thead=[document.createElement("thead")];
this.appendChild(thead[0])
}oSettings.nTHead=thead[0];
var tbody=$(this).children("tbody");
if(tbody.length===0){tbody=[document.createElement("tbody")];
this.appendChild(tbody[0])
}oSettings.nTBody=tbody[0];
oSettings.nTBody.setAttribute("role","alert");
oSettings.nTBody.setAttribute("aria-live","polite");
oSettings.nTBody.setAttribute("aria-relevant","all");
var tfoot=$(this).children("tfoot");
if(tfoot.length===0&&captions.length>0&&(oSettings.oScroll.sX!==""||oSettings.oScroll.sY!=="")){tfoot=[document.createElement("tfoot")];
this.appendChild(tfoot[0])
}if(tfoot.length>0){oSettings.nTFoot=tfoot[0];
_fnDetectHeader(oSettings.aoFooter,oSettings.nTFoot)
}if(bUsePassedData){for(i=0;
i<oInit.aaData.length;
i++){_fnAddData(oSettings,oInit.aaData[i])
}}else{_fnGatherData(oSettings)
}oSettings.aiDisplay=oSettings.aiDisplayMaster.slice();
oSettings.bInitialised=true;
if(bInitHandedOff===false){_fnInitialise(oSettings)
}});
_that=null;
return this
};
DataTable.fnVersionCheck=function(sVersion){var fnZPad=function(Zpad,count){while(Zpad.length<count){Zpad+="0"
}return Zpad
};
var aThis=DataTable.ext.sVersion.split(".");
var aThat=sVersion.split(".");
var sThis="",sThat="";
for(var i=0,iLen=aThat.length;
i<iLen;
i++){sThis+=fnZPad(aThis[i],3);
sThat+=fnZPad(aThat[i],3)
}return parseInt(sThis,10)>=parseInt(sThat,10)
};
DataTable.fnIsDataTable=function(nTable){var o=DataTable.settings;
for(var i=0;
i<o.length;
i++){if(o[i].nTable===nTable||o[i].nScrollHead===nTable||o[i].nScrollFoot===nTable){return true
}}return false
};
DataTable.fnTables=function(bVisible){var out=[];
jQuery.each(DataTable.settings,function(i,o){if(!bVisible||(bVisible===true&&$(o.nTable).is(":visible"))){out.push(o.nTable)
}});
return out
};
DataTable.version="1.9.4";
DataTable.settings=[];
DataTable.models={};
DataTable.models.ext={afnFiltering:[],afnSortData:[],aoFeatures:[],aTypes:[],fnVersionCheck:DataTable.fnVersionCheck,iApiIndex:0,ofnSearch:{},oApi:{},oStdClasses:{},oJUIClasses:{},oPagination:{},oSort:{},sVersion:DataTable.version,sErrMode:"alert",_oExternConfig:{iNextUnique:0}};
DataTable.models.oSearch={bCaseInsensitive:true,sSearch:"",bRegex:false,bSmart:true};
DataTable.models.oRow={nTr:null,_aData:[],_aSortData:[],_anHidden:[],_sRowStripe:""};
DataTable.models.oColumn={aDataSort:null,asSorting:null,bSearchable:null,bSortable:null,bUseRendered:null,bVisible:null,_bAutoType:true,fnCreatedCell:null,fnGetData:null,fnRender:null,fnSetData:null,mData:null,mRender:null,nTh:null,nTf:null,sClass:null,sContentPadding:null,sDefaultContent:null,sName:null,sSortDataType:"std",sSortingClass:null,sSortingClassJUI:null,sTitle:null,sType:null,sWidth:null,sWidthOrig:null};
DataTable.defaults={aaData:null,aaSorting:[[0,"asc"]],aaSortingFixed:null,aLengthMenu:[10,25,50,100],aoColumns:null,aoColumnDefs:null,aoSearchCols:[],asStripeClasses:null,bAutoWidth:true,bDeferRender:false,bDestroy:false,bFilter:true,bInfo:true,bJQueryUI:false,bLengthChange:true,bPaginate:true,bProcessing:false,bRetrieve:false,bScrollAutoCss:true,bScrollCollapse:false,bScrollInfinite:false,bServerSide:false,bSort:true,bSortCellsTop:false,bSortClasses:true,bStateSave:false,fnCookieCallback:null,fnCreatedRow:null,fnDrawCallback:null,fnFooterCallback:null,fnFormatNumber:function(iIn){if(iIn<1000){return iIn
}var s=(iIn+""),a=s.split(""),out="",iLen=s.length;
for(var i=0;
i<iLen;
i++){if(i%3===0&&i!==0){out=this.oLanguage.sInfoThousands+out
}out=a[iLen-i-1]+out
}return out
},fnHeaderCallback:null,fnInfoCallback:null,fnInitComplete:null,fnPreDrawCallback:null,fnRowCallback:null,fnServerData:function(sUrl,aoData,fnCallback,oSettings){oSettings.jqXHR=$.ajax({url:sUrl,data:aoData,success:function(json){if(json.sError){oSettings.oApi._fnLog(oSettings,0,json.sError)
}$(oSettings.oInstance).trigger("xhr",[oSettings,json]);
fnCallback(json)
},dataType:"json",cache:false,type:oSettings.sServerMethod,error:function(xhr,error,thrown){if(error=="parsererror"){oSettings.oApi._fnLog(oSettings,0,"DataTables warning: JSON data from server could not be parsed. This is caused by a JSON formatting error.")
}}})
},fnServerParams:null,fnStateLoad:function(oSettings){var sData=this.oApi._fnReadCookie(oSettings.sCookiePrefix+oSettings.sInstance);
var oData;
try{oData=(typeof $.parseJSON==="function")?$.parseJSON(sData):eval("("+sData+")")
}catch(e){oData=null
}return oData
},fnStateLoadParams:null,fnStateLoaded:null,fnStateSave:function(oSettings,oData){this.oApi._fnCreateCookie(oSettings.sCookiePrefix+oSettings.sInstance,this.oApi._fnJsonString(oData),oSettings.iCookieDuration,oSettings.sCookiePrefix,oSettings.fnCookieCallback)
},fnStateSaveParams:null,iCookieDuration:7200,iDeferLoading:null,iDisplayLength:10,iDisplayStart:0,iScrollLoadGap:100,iTabIndex:0,oLanguage:{oAria:{sSortAscending:": activate to sort column ascending",sSortDescending:": activate to sort column descending"},oPaginate:{sFirst:"First",sLast:"Last",sNext:"Next",sPrevious:"Previous"},sEmptyTable:"No data available in table",sInfo:"Showing _START_ to _END_ of _TOTAL_ entries",sInfoEmpty:"Showing 0 to 0 of 0 entries",sInfoFiltered:"(filtered from _MAX_ total entries)",sInfoPostFix:"",sInfoThousands:",",sLengthMenu:"Show _MENU_ entries",sLoadingRecords:"Loading...",sProcessing:"Processing...",sSearch:"Search:",sUrl:"",sZeroRecords:"No matching records found"},oSearch:$.extend({},DataTable.models.oSearch),sAjaxDataProp:"aaData",sAjaxSource:null,sCookiePrefix:"SpryMedia_DataTables_",sDom:"lfrtip",sPaginationType:"two_button",sScrollX:"",sScrollXInner:"",sScrollY:"",sServerMethod:"GET"};
DataTable.defaults.columns={aDataSort:null,asSorting:["asc","desc"],bSearchable:true,bSortable:true,bUseRendered:true,bVisible:true,fnCreatedCell:null,fnRender:null,iDataSort:-1,mData:null,mRender:null,sCellType:"td",sClass:"",sContentPadding:"",sDefaultContent:null,sName:"",sSortDataType:"std",sTitle:null,sType:null,sWidth:null};
DataTable.models.oSettings={oFeatures:{bAutoWidth:null,bDeferRender:null,bFilter:null,bInfo:null,bLengthChange:null,bPaginate:null,bProcessing:null,bServerSide:null,bSort:null,bSortClasses:null,bStateSave:null},oScroll:{bAutoCss:null,bCollapse:null,bInfinite:null,iBarWidth:0,iLoadGap:null,sX:null,sXInner:null,sY:null},oLanguage:{fnInfoCallback:null},oBrowser:{bScrollOversize:false},aanFeatures:[],aoData:[],aiDisplay:[],aiDisplayMaster:[],aoColumns:[],aoHeader:[],aoFooter:[],asDataSearch:[],oPreviousSearch:{},aoPreSearchCols:[],aaSorting:null,aaSortingFixed:null,asStripeClasses:null,asDestroyStripes:[],sDestroyWidth:0,aoRowCallback:[],aoHeaderCallback:[],aoFooterCallback:[],aoDrawCallback:[],aoRowCreatedCallback:[],aoPreDrawCallback:[],aoInitComplete:[],aoStateSaveParams:[],aoStateLoadParams:[],aoStateLoaded:[],sTableId:"",nTable:null,nTHead:null,nTFoot:null,nTBody:null,nTableWrapper:null,bDeferLoading:false,bInitialised:false,aoOpenRows:[],sDom:null,sPaginationType:"two_button",iCookieDuration:0,sCookiePrefix:"",fnCookieCallback:null,aoStateSave:[],aoStateLoad:[],oLoadedState:null,sAjaxSource:null,sAjaxDataProp:null,bAjaxDataGet:true,jqXHR:null,fnServerData:null,aoServerParams:[],sServerMethod:null,fnFormatNumber:null,aLengthMenu:null,iDraw:0,bDrawing:false,iDrawError:-1,_iDisplayLength:10,_iDisplayStart:0,_iDisplayEnd:10,_iRecordsTotal:0,_iRecordsDisplay:0,bJUI:null,oClasses:{},bFiltered:false,bSorted:false,bSortCellsTop:null,oInit:null,aoDestroyCallback:[],fnRecordsTotal:function(){if(this.oFeatures.bServerSide){return parseInt(this._iRecordsTotal,10)
}else{return this.aiDisplayMaster.length
}},fnRecordsDisplay:function(){if(this.oFeatures.bServerSide){return parseInt(this._iRecordsDisplay,10)
}else{return this.aiDisplay.length
}},fnDisplayEnd:function(){if(this.oFeatures.bServerSide){if(this.oFeatures.bPaginate===false||this._iDisplayLength==-1){return this._iDisplayStart+this.aiDisplay.length
}else{return Math.min(this._iDisplayStart+this._iDisplayLength,this._iRecordsDisplay)
}}else{return this._iDisplayEnd
}},oInstance:null,sInstance:null,iTabIndex:0,nScrollHead:null,nScrollFoot:null};
DataTable.ext=$.extend(true,{},DataTable.models.ext);
$.extend(DataTable.ext.oStdClasses,{sTable:"dataTable",sPagePrevEnabled:"paginate_enabled_previous",sPagePrevDisabled:"paginate_disabled_previous",sPageNextEnabled:"paginate_enabled_next",sPageNextDisabled:"paginate_disabled_next",sPageJUINext:"",sPageJUIPrev:"",sPageButton:"paginate_button",sPageButtonActive:"paginate_active",sPageButtonStaticDisabled:"paginate_button paginate_button_disabled",sPageFirst:"first",sPagePrevious:"previous",sPageNext:"next",sPageLast:"last",sStripeOdd:"odd",sStripeEven:"even",sRowEmpty:"dataTables_empty",sWrapper:"dataTables_wrapper",sFilter:"dataTables_filter",sInfo:"dataTables_info",sPaging:"dataTables_paginate paging_",sLength:"dataTables_length",sProcessing:"dataTables_processing",sSortAsc:"sorting_asc",sSortDesc:"sorting_desc",sSortable:"sorting",sSortableAsc:"sorting_asc_disabled",sSortableDesc:"sorting_desc_disabled",sSortableNone:"sorting_disabled",sSortColumn:"sorting_",sSortJUIAsc:"",sSortJUIDesc:"",sSortJUI:"",sSortJUIAscAllowed:"",sSortJUIDescAllowed:"",sSortJUIWrapper:"",sSortIcon:"",sScrollWrapper:"dataTables_scroll",sScrollHead:"dataTables_scrollHead",sScrollHeadInner:"dataTables_scrollHeadInner",sScrollBody:"dataTables_scrollBody",sScrollFoot:"dataTables_scrollFoot",sScrollFootInner:"dataTables_scrollFootInner",sFooterTH:"",sJUIHeader:"",sJUIFooter:""});
$.extend(DataTable.ext.oJUIClasses,DataTable.ext.oStdClasses,{sPagePrevEnabled:"fg-button ui-button ui-state-default ui-corner-left",sPagePrevDisabled:"fg-button ui-button ui-state-default ui-corner-left ui-state-disabled",sPageNextEnabled:"fg-button ui-button ui-state-default ui-corner-right",sPageNextDisabled:"fg-button ui-button ui-state-default ui-corner-right ui-state-disabled",sPageJUINext:"ui-icon ui-icon-circle-arrow-e",sPageJUIPrev:"ui-icon ui-icon-circle-arrow-w",sPageButton:"fg-button ui-button ui-state-default",sPageButtonActive:"fg-button ui-button ui-state-default ui-state-disabled",sPageButtonStaticDisabled:"fg-button ui-button ui-state-default ui-state-disabled",sPageFirst:"first ui-corner-tl ui-corner-bl",sPageLast:"last ui-corner-tr ui-corner-br",sPaging:"dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_",sSortAsc:"ui-state-default",sSortDesc:"ui-state-default",sSortable:"ui-state-default",sSortableAsc:"ui-state-default",sSortableDesc:"ui-state-default",sSortableNone:"ui-state-default",sSortJUIAsc:"css_right ui-icon ui-icon-triangle-1-n",sSortJUIDesc:"css_right ui-icon ui-icon-triangle-1-s",sSortJUI:"css_right ui-icon ui-icon-carat-2-n-s",sSortJUIAscAllowed:"css_right ui-icon ui-icon-carat-1-n",sSortJUIDescAllowed:"css_right ui-icon ui-icon-carat-1-s",sSortJUIWrapper:"DataTables_sort_wrapper",sSortIcon:"DataTables_sort_icon",sScrollHead:"dataTables_scrollHead ui-state-default",sScrollFoot:"dataTables_scrollFoot ui-state-default",sFooterTH:"ui-state-default",sJUIHeader:"fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix",sJUIFooter:"fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix"});
$.extend(DataTable.ext.oPagination,{two_button:{fnInit:function(oSettings,nPaging,fnCallbackDraw){var oLang=oSettings.oLanguage.oPaginate;
var oClasses=oSettings.oClasses;
var fnClickHandler=function(e){if(oSettings.oApi._fnPageChange(oSettings,e.data.action)){fnCallbackDraw(oSettings)
}};
var sAppend=(!oSettings.bJUI)?'<a class="'+oSettings.oClasses.sPagePrevDisabled+'" tabindex="'+oSettings.iTabIndex+'" role="button">'+oLang.sPrevious+'</a><a class="'+oSettings.oClasses.sPageNextDisabled+'" tabindex="'+oSettings.iTabIndex+'" role="button">'+oLang.sNext+"</a>":'<a class="'+oSettings.oClasses.sPagePrevDisabled+'" tabindex="'+oSettings.iTabIndex+'" role="button"><span class="'+oSettings.oClasses.sPageJUIPrev+'"></span></a><a class="'+oSettings.oClasses.sPageNextDisabled+'" tabindex="'+oSettings.iTabIndex+'" role="button"><span class="'+oSettings.oClasses.sPageJUINext+'"></span></a>';
$(nPaging).append(sAppend);
var els=$("a",nPaging);
var nPrevious=els[0],nNext=els[1];
oSettings.oApi._fnBindAction(nPrevious,{action:"previous"},fnClickHandler);
oSettings.oApi._fnBindAction(nNext,{action:"next"},fnClickHandler);
if(!oSettings.aanFeatures.p){nPaging.id=oSettings.sTableId+"_paginate";
nPrevious.id=oSettings.sTableId+"_previous";
nNext.id=oSettings.sTableId+"_next";
nPrevious.setAttribute("aria-controls",oSettings.sTableId);
nNext.setAttribute("aria-controls",oSettings.sTableId)
}},fnUpdate:function(oSettings,fnCallbackDraw){if(!oSettings.aanFeatures.p){return 
}var oClasses=oSettings.oClasses;
var an=oSettings.aanFeatures.p;
var nNode;
for(var i=0,iLen=an.length;
i<iLen;
i++){nNode=an[i].firstChild;
if(nNode){nNode.className=(oSettings._iDisplayStart===0)?oClasses.sPagePrevDisabled:oClasses.sPagePrevEnabled;
nNode=nNode.nextSibling;
nNode.className=(oSettings.fnDisplayEnd()==oSettings.fnRecordsDisplay())?oClasses.sPageNextDisabled:oClasses.sPageNextEnabled
}}}},iFullNumbersShowPages:5,full_numbers:{fnInit:function(oSettings,nPaging,fnCallbackDraw){var oLang=oSettings.oLanguage.oPaginate;
var oClasses=oSettings.oClasses;
var fnClickHandler=function(e){if(oSettings.oApi._fnPageChange(oSettings,e.data.action)){fnCallbackDraw(oSettings)
}};
$(nPaging).append('<a  tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButton+" "+oClasses.sPageFirst+'">'+oLang.sFirst+'</a><a  tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButton+" "+oClasses.sPagePrevious+'">'+oLang.sPrevious+'</a><span></span><a tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButton+" "+oClasses.sPageNext+'">'+oLang.sNext+'</a><a tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButton+" "+oClasses.sPageLast+'">'+oLang.sLast+"</a>");
var els=$("a",nPaging);
var nFirst=els[0],nPrev=els[1],nNext=els[2],nLast=els[3];
oSettings.oApi._fnBindAction(nFirst,{action:"first"},fnClickHandler);
oSettings.oApi._fnBindAction(nPrev,{action:"previous"},fnClickHandler);
oSettings.oApi._fnBindAction(nNext,{action:"next"},fnClickHandler);
oSettings.oApi._fnBindAction(nLast,{action:"last"},fnClickHandler);
if(!oSettings.aanFeatures.p){nPaging.id=oSettings.sTableId+"_paginate";
nFirst.id=oSettings.sTableId+"_first";
nPrev.id=oSettings.sTableId+"_previous";
nNext.id=oSettings.sTableId+"_next";
nLast.id=oSettings.sTableId+"_last"
}},fnUpdate:function(oSettings,fnCallbackDraw){if(!oSettings.aanFeatures.p){return 
}var iPageCount=DataTable.ext.oPagination.iFullNumbersShowPages;
var iPageCountHalf=Math.floor(iPageCount/2);
var iPages=Math.ceil((oSettings.fnRecordsDisplay())/oSettings._iDisplayLength);
var iCurrentPage=Math.ceil(oSettings._iDisplayStart/oSettings._iDisplayLength)+1;
var sList="";
var iStartButton,iEndButton,i,iLen;
var oClasses=oSettings.oClasses;
var anButtons,anStatic,nPaginateList,nNode;
var an=oSettings.aanFeatures.p;
var fnBind=function(j){oSettings.oApi._fnBindAction(this,{page:j+iStartButton-1},function(e){oSettings.oApi._fnPageChange(oSettings,e.data.page);
fnCallbackDraw(oSettings);
e.preventDefault()
})
};
if(oSettings._iDisplayLength===-1){iStartButton=1;
iEndButton=1;
iCurrentPage=1
}else{if(iPages<iPageCount){iStartButton=1;
iEndButton=iPages
}else{if(iCurrentPage<=iPageCountHalf){iStartButton=1;
iEndButton=iPageCount
}else{if(iCurrentPage>=(iPages-iPageCountHalf)){iStartButton=iPages-iPageCount+1;
iEndButton=iPages
}else{iStartButton=iCurrentPage-Math.ceil(iPageCount/2)+1;
iEndButton=iStartButton+iPageCount-1
}}}}for(i=iStartButton;
i<=iEndButton;
i++){sList+=(iCurrentPage!==i)?'<a tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButton+'">'+oSettings.fnFormatNumber(i)+"</a>":'<a tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButtonActive+'">'+oSettings.fnFormatNumber(i)+"</a>"
}for(i=0,iLen=an.length;
i<iLen;
i++){nNode=an[i];
if(!nNode.hasChildNodes()){continue
}$("span:eq(0)",nNode).html(sList).children("a").each(fnBind);
anButtons=nNode.getElementsByTagName("a");
anStatic=[anButtons[0],anButtons[1],anButtons[anButtons.length-2],anButtons[anButtons.length-1]];
$(anStatic).removeClass(oClasses.sPageButton+" "+oClasses.sPageButtonActive+" "+oClasses.sPageButtonStaticDisabled);
$([anStatic[0],anStatic[1]]).addClass((iCurrentPage==1)?oClasses.sPageButtonStaticDisabled:oClasses.sPageButton);
$([anStatic[2],anStatic[3]]).addClass((iPages===0||iCurrentPage===iPages||oSettings._iDisplayLength===-1)?oClasses.sPageButtonStaticDisabled:oClasses.sPageButton)
}}}});
$.extend(DataTable.ext.oSort,{"string-pre":function(a){if(typeof a!="string"){a=(a!==null&&a.toString)?a.toString():""
}return a.toLowerCase()
},"string-asc":function(x,y){return((x<y)?-1:((x>y)?1:0))
},"string-desc":function(x,y){return((x<y)?1:((x>y)?-1:0))
},"html-pre":function(a){return a.replace(/<.*?>/g,"").toLowerCase()
},"html-asc":function(x,y){return((x<y)?-1:((x>y)?1:0))
},"html-desc":function(x,y){return((x<y)?1:((x>y)?-1:0))
},"date-pre":function(a){var x=Date.parse(a);
if(isNaN(x)||x===""){x=Date.parse("01/01/1970 00:00:00")
}return x
},"date-asc":function(x,y){return x-y
},"date-desc":function(x,y){return y-x
},"numeric-pre":function(a){return(a=="-"||a==="")?0:a*1
},"numeric-asc":function(x,y){return x-y
},"numeric-desc":function(x,y){return y-x
}});
$.extend(DataTable.ext.aTypes,[function(sData){if(typeof sData==="number"){return"numeric"
}else{if(typeof sData!=="string"){return null
}}var sValidFirstChars="0123456789-";
var sValidChars="0123456789.";
var Char;
var bDecimal=false;
Char=sData.charAt(0);
if(sValidFirstChars.indexOf(Char)==-1){return null
}for(var i=1;
i<sData.length;
i++){Char=sData.charAt(i);
if(sValidChars.indexOf(Char)==-1){return null
}if(Char=="."){if(bDecimal){return null
}bDecimal=true
}}return"numeric"
},function(sData){var iParse=Date.parse(sData);
if((iParse!==null&&!isNaN(iParse))||(typeof sData==="string"&&sData.length===0)){return"date"
}return null
},function(sData){if(typeof sData==="string"&&sData.indexOf("<")!=-1&&sData.indexOf(">")!=-1){return"html"
}return null
}]);
$.fn.DataTable=DataTable;
$.fn.dataTable=DataTable;
$.fn.dataTableSettings=DataTable.settings;
$.fn.dataTableExt=DataTable.ext
}))
}(window,document));
!function(A){var B=function(D,C){this.$element=A(D);
this.options=C;
this.options.slide&&this.slide(this.options.slide);
this.options.pause=="hover"&&this.$element.on("mouseenter",A.proxy(this.pause,this)).on("mouseleave",A.proxy(this.cycle,this))
};
B.prototype={cycle:function(C){if(!C){this.paused=false
}this.options.interval&&!this.paused&&(this.interval=setInterval(A.proxy(this.next,this),this.options.interval));
return this
},to:function(G){var C=this.$element.find(".item.active"),D=C.parent().children(),E=D.index(C),F=this;
if(G>(D.length-1)||G<0){return 
}if(this.sliding){return this.$element.one("slid",function(){F.to(G)
})
}if(E==G){return this.pause().cycle()
}return this.slide(G>E?"next":"prev",A(D[G]))
},pause:function(C){if(!C){this.paused=true
}if(this.$element.find(".next, .prev").length&&A.support.transition.end){this.$element.trigger(A.support.transition.end);
this.cycle()
}clearInterval(this.interval);
this.interval=null;
return this
},next:function(){if(this.sliding){return 
}return this.slide("next")
},prev:function(){if(this.sliding){return 
}return this.slide("prev")
},slide:function(I,D){var K=this.$element.find(".item.active"),C=D||K[I](),H=this.interval,J=I=="next"?"left":"right",E=I=="next"?"first":"last",F=this,G=A.Event("slide",{relatedTarget:C[0]});
this.sliding=true;
H&&this.pause();
C=C.length?C:this.$element.find(".item")[E]();
if(C.hasClass("active")){return 
}if(A.support.transition&&this.$element.hasClass("slide")){this.$element.trigger(G);
if(G.isDefaultPrevented()){return 
}C.addClass(I);
C[0].offsetWidth;
K.addClass(J);
C.addClass(J);
this.$element.one(A.support.transition.end,function(){C.removeClass([I,J].join(" ")).addClass("active");
K.removeClass(["active",J].join(" "));
F.sliding=false;
setTimeout(function(){F.$element.trigger("slid")
},0)
})
}else{this.$element.trigger(G);
if(G.isDefaultPrevented()){return 
}K.removeClass("active");
C.addClass("active");
this.sliding=false;
this.$element.trigger("slid")
}H&&this.cycle();
return this
}};
A.fn.carousel=function(C){return this.each(function(){var G=A(this),F=G.data("carousel"),D=A.extend({},A.fn.carousel.defaults,typeof C=="object"&&C),E=typeof C=="string"?C:D.slide;
if(!F){G.data("carousel",(F=new B(this,D)))
}if(typeof C=="number"){F.to(C)
}else{if(E){F[E]()
}else{if(D.interval){F.cycle()
}}}})
};
A.fn.carousel.defaults={interval:5000,pause:"hover"};
A.fn.carousel.Constructor=B;
A(function(){A("body").on("click.carousel.data-api","[data-slide]",function(G){var F=A(this),D,C=A(F.attr("data-target")||(D=F.attr("href"))&&D.replace(/.*(?=#[^\s]+$)/,"")),E=!C.data("modal")&&A.extend({},C.data(),F.data());
C.carousel(E);
G.preventDefault()
})
})
}(window.jQuery);
!function(B){var A=function(D,C){this.options=C;
this.$element=B(D).delegate('[data-dismiss="modal"]',"click.dismiss.modal",B.proxy(this.hide,this));
this.options.remote&&this.$element.find(".modal-body").load(this.options.remote)
};
A.prototype={constructor:A,toggle:function(){return this[!this.isShown?"show":"hide"]()
},show:function(){var C=this,D=B.Event("show");
this.$element.trigger(D);
if(this.isShown||D.isDefaultPrevented()){return 
}B("body").addClass("modal-open");
this.isShown=true;
this.escape();
this.backdrop(function(){var E=B.support.transition&&C.$element.hasClass("fade");
if(!C.$element.parent().length){C.$element.appendTo(document.body)
}C.$element.show();
if(E){C.$element[0].offsetWidth
}C.$element.addClass("in").attr("aria-hidden",false).focus();
C.enforceFocus();
E?C.$element.one(B.support.transition.end,function(){C.$element.trigger("shown")
}):C.$element.trigger("shown")
})
},hide:function(D){D&&D.preventDefault();
var C=this;
D=B.Event("hide");
this.$element.trigger(D);
if(!this.isShown||D.isDefaultPrevented()){return 
}this.isShown=false;
B("body").removeClass("modal-open");
this.escape();
B(document).off("focusin.modal");
this.$element.removeClass("in").attr("aria-hidden",true);
B.support.transition&&this.$element.hasClass("fade")?this.hideWithTransition():this.hideModal()
},enforceFocus:function(){var C=this;
B(document).on("focusin.modal",function(D){if(C.$element[0]!==D.target&&!C.$element.has(D.target).length){C.$element.focus()
}})
},escape:function(){var C=this;
if(this.isShown&&this.options.keyboard){this.$element.on("keyup.dismiss.modal",function(D){D.which==27&&C.hide()
})
}else{if(!this.isShown){this.$element.off("keyup.dismiss.modal")
}}},hideWithTransition:function(){var C=this,D=setTimeout(function(){C.$element.off(B.support.transition.end);
C.hideModal()
},500);
this.$element.one(B.support.transition.end,function(){clearTimeout(D);
C.hideModal()
})
},hideModal:function(C){this.$element.hide().trigger("hidden");
this.backdrop()
},removeBackdrop:function(){this.$backdrop.remove();
this.$backdrop=null
},backdrop:function(F){var E=this,D=this.$element.hasClass("fade")?"fade":"";
if(this.isShown&&this.options.backdrop){var C=B.support.transition&&D;
this.$backdrop=B('<div class="modal-backdrop '+D+'" />').appendTo(document.body);
if(this.options.backdrop!="static"){this.$backdrop.click(B.proxy(this.hide,this))
}if(C){this.$backdrop[0].offsetWidth
}this.$backdrop.addClass("in");
C?this.$backdrop.one(B.support.transition.end,F):F()
}else{if(!this.isShown&&this.$backdrop){this.$backdrop.removeClass("in");
B.support.transition&&this.$element.hasClass("fade")?this.$backdrop.one(B.support.transition.end,B.proxy(this.removeBackdrop,this)):this.removeBackdrop()
}else{if(F){F()
}}}}};
B.fn.modal=function(C){return this.each(function(){var F=B(this),E=F.data("modal"),D=B.extend({},B.fn.modal.defaults,F.data(),typeof C=="object"&&C);
if(!E){F.data("modal",(E=new A(this,D)))
}if(typeof C=="string"){E[C]()
}else{if(D.show){E.show()
}}})
};
B.fn.modal.defaults={backdrop:true,keyboard:true,show:true};
B.fn.modal.Constructor=A;
B(function(){B("body").on("click.modal.data-api",'[data-toggle="modal"]',function(G){var F=B(this),D=F.attr("href"),C=B(F.attr("data-target")||(D&&D.replace(/.*(?=#[^\s]+$)/,""))),E=C.data("modal")?"toggle":B.extend({remote:!/#/.test(D)&&D},C.data(),F.data());
G.preventDefault();
C.modal(E).one("hide",function(){F.focus()
})
})
})
}(window.jQuery);
!function(B){var A=function(C){this.element=B(C)
};
A.prototype={constructor:A,show:function(){var H=this.element,E=H.closest("ul:not(.dropdown-menu)"),D=H.attr("data-target"),F,C,G;
if(!D){D=H.attr("href");
D=D&&D.replace(/.*(?=#[^\s]*$)/,"")
}if(H.parent("li").hasClass("active")){return 
}F=E.find(".active a").last()[0];
G=B.Event("show",{relatedTarget:F});
H.trigger(G);
if(G.isDefaultPrevented()){return 
}C=B(D);
this.activate(H.parent("li"),E);
this.activate(C,C.parent(),function(){H.trigger({type:"shown",relatedTarget:F})
})
},activate:function(E,D,H){var C=D.find("> .active"),G=H&&B.support.transition&&C.hasClass("fade");
function F(){C.removeClass("active").find("> .dropdown-menu > .active").removeClass("active");
E.addClass("active");
if(G){E[0].offsetWidth;
E.addClass("in")
}else{E.removeClass("fade")
}if(E.parent(".dropdown-menu")){E.closest("li.dropdown").addClass("active")
}H&&H()
}G?C.one(B.support.transition.end,F):F();
C.removeClass("in")
}};
B.fn.tab=function(C){return this.each(function(){var E=B(this),D=E.data("tab");
if(!D){E.data("tab",(D=new A(this)))
}if(typeof C=="string"){D[C]()
}})
};
B.fn.tab.Constructor=A;
B(function(){B("body").on("click.tab.data-api",'[data-toggle="tab"], [data-toggle="pill"]',function(C){C.preventDefault();
B(this).tab("show")
})
})
}(window.jQuery);
!function(A){A(function(){A.support.transition=(function(){var B=(function(){var E=document.createElement("bootstrap"),D={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"},C;
for(C in D){if(E.style[C]!==undefined){return D[C]
}}}());
return B&&{end:B}
})()
})
}(window.jQuery);
var JSON;
if(!JSON){JSON={}
}(function(){function f(n){return n<10?"0"+n:n
}if(typeof Date.prototype.toJSON!=="function"){Date.prototype.toJSON=function(key){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+f(this.getUTCMonth()+1)+"-"+f(this.getUTCDate())+"T"+f(this.getUTCHours())+":"+f(this.getUTCMinutes())+":"+f(this.getUTCSeconds())+"Z":null
};
String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(key){return this.valueOf()
}
}var cx=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,escapable=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,gap,indent,meta={"\b":"\\b","\t":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},rep;
function quote(string){escapable.lastIndex=0;
return escapable.test(string)?'"'+string.replace(escapable,function(a){var c=meta[a];
return typeof c==="string"?c:"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)
})+'"':'"'+string+'"'
}function str(key,holder){var i,k,v,length,mind=gap,partial,value=holder[key];
if(value&&typeof value==="object"&&typeof value.toJSON==="function"){value=value.toJSON(key)
}if(typeof rep==="function"){value=rep.call(holder,key,value)
}switch(typeof value){case"string":return quote(value);
case"number":return isFinite(value)?String(value):"null";
case"boolean":case"null":return String(value);
case"object":if(!value){return"null"
}gap+=indent;
partial=[];
if(Object.prototype.toString.apply(value)==="[object Array]"){length=value.length;
for(i=0;
i<length;
i+=1){partial[i]=str(i,value)||"null"
}v=partial.length===0?"[]":gap?"[\n"+gap+partial.join(",\n"+gap)+"\n"+mind+"]":"["+partial.join(",")+"]";
gap=mind;
return v
}if(rep&&typeof rep==="object"){length=rep.length;
for(i=0;
i<length;
i+=1){if(typeof rep[i]==="string"){k=rep[i];
v=str(k,value);
if(v){partial.push(quote(k)+(gap?": ":":")+v)
}}}}else{for(k in value){if(Object.prototype.hasOwnProperty.call(value,k)){v=str(k,value);
if(v){partial.push(quote(k)+(gap?": ":":")+v)
}}}}v=partial.length===0?"{}":gap?"{\n"+gap+partial.join(",\n"+gap)+"\n"+mind+"}":"{"+partial.join(",")+"}";
gap=mind;
return v
}}if(typeof JSON.stringify!=="function"){JSON.stringify=function(value,replacer,space){var i;
gap="";
indent="";
if(typeof space==="number"){for(i=0;
i<space;
i+=1){indent+=" "
}}else{if(typeof space==="string"){indent=space
}}rep=replacer;
if(replacer&&typeof replacer!=="function"&&(typeof replacer!=="object"||typeof replacer.length!=="number")){throw new Error("JSON.stringify")
}return str("",{"":value})
}
}if(typeof JSON.parse!=="function"){JSON.parse=function(text,reviver){var j;
function walk(holder,key){var k,v,value=holder[key];
if(value&&typeof value==="object"){for(k in value){if(Object.prototype.hasOwnProperty.call(value,k)){v=walk(value,k);
if(v!==undefined){value[k]=v
}else{delete value[k]
}}}}return reviver.call(holder,key,value)
}text=String(text);
cx.lastIndex=0;
if(cx.test(text)){text=text.replace(cx,function(a){return"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)
})
}if(/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,""))){j=eval("("+text+")");
return typeof reviver==="function"?walk({"":j},""):j
}throw new SyntaxError("JSON.parse")
}
}}());
(function(){function AF(A,G,F){if(A===G){return 0!==A||1/A==1/G
}if(null==A||null==G){return A===G
}A._chain&&(A=A._wrapped);
G._chain&&(G=G._wrapped);
if(A.isEqual&&AR.isFunction(A.isEqual)){return A.isEqual(G)
}if(G.isEqual&&AR.isFunction(G.isEqual)){return G.isEqual(A)
}var E=AN.call(A);
if(E!=AN.call(G)){return !1
}switch(E){case"[object String]":return A==""+G;
case"[object Number]":return A!=+A?G!=+G:0==A?1/A==1/G:A==+G;
case"[object Date]":case"[object Boolean]":return +A==+G;
case"[object RegExp]":return A.source==G.source&&A.global==G.global&&A.multiline==G.multiline&&A.ignoreCase==G.ignoreCase
}if("object"!=typeof A||"object"!=typeof G){return !1
}for(var D=F.length;
D--;
){if(F[D]==A){return !0
}}F.push(A);
var D=0,C=!0;
if("[object Array]"==E){if(D=A.length,C=D==G.length){for(;
D--&&(C=D in A==D in G&&AF(A[D],G[D],F));
){}}}else{if("constructor" in A!="constructor" in G||A.constructor!=G.constructor){return !1
}for(var B in A){if(AR.has(A,B)&&(D++,!(C=AR.has(G,B)&&AF(A[B],G[B],F)))){break
}}if(C){for(B in G){if(AR.has(G,B)&&!D--){break
}}C=!D
}}F.pop();
return C
}var AC=this,a=AC._,AK={},AO=Array.prototype,AJ=Object.prototype,AQ=AO.slice,X=AO.unshift,AN=AJ.toString,V=AJ.hasOwnProperty,Y=AO.forEach,W=AO.map,AI=AO.reduce,AH=AO.reduceRight,AE=AO.filter,AD=AO.every,AB=AO.some,AG=AO.indexOf,h=AO.lastIndexOf,AJ=Array.isArray,U=Object.keys,AA=Function.prototype.bind,AR=function(A){return new AM(A)
};
"undefined"!==typeof exports?("undefined"!==typeof module&&module.exports&&(exports=module.exports=AR),exports._=AR):AC._=AR;
AR.VERSION="1.3.3";
var AP=AR.each=AR.forEach=function(A,E,D){if(A!=null){if(Y&&A.forEach===Y){A.forEach(E,D)
}else{if(A.length===+A.length){for(var C=0,B=A.length;
C<B;
C++){if(C in A&&E.call(D,A[C],C,A)===AK){break
}}}else{for(C in A){if(AR.has(A,C)&&E.call(D,A[C],C,A)===AK){break
}}}}}};
AR.map=AR.collect=function(B,D,A){var C=[];
if(B==null){return C
}if(W&&B.map===W){return B.map(D,A)
}AP(B,function(E,G,F){C[C.length]=D.call(A,E,G,F)
});
if(B.length===+B.length){C.length=B.length
}return C
};
AR.reduce=AR.foldl=AR.inject=function(A,E,D,C){var B=arguments.length>2;
A==null&&(A=[]);
if(AI&&A.reduce===AI){C&&(E=AR.bind(E,C));
return B?A.reduce(E,D):A.reduce(E)
}AP(A,function(G,F,H){if(B){D=E.call(C,D,G,F,H)
}else{D=G;
B=true
}});
if(!B){throw new TypeError("Reduce of empty array with no initial value")
}return D
};
AR.reduceRight=AR.foldr=function(A,F,E,D){var C=arguments.length>2;
A==null&&(A=[]);
if(AH&&A.reduceRight===AH){D&&(F=AR.bind(F,D));
return C?A.reduceRight(F,E):A.reduceRight(F)
}var B=AR.toArray(A).reverse();
D&&!C&&(F=AR.bind(F,D));
return C?AR.reduce(B,F,E,D):AR.reduce(B,F)
};
AR.find=AR.detect=function(B,D,A){var C;
f(B,function(E,G,F){if(D.call(A,E,G,F)){C=E;
return true
}});
return C
};
AR.filter=AR.select=function(B,D,A){var C=[];
if(B==null){return C
}if(AE&&B.filter===AE){return B.filter(D,A)
}AP(B,function(E,G,F){D.call(A,E,G,F)&&(C[C.length]=E)
});
return C
};
AR.reject=function(B,D,A){var C=[];
if(B==null){return C
}AP(B,function(E,G,F){D.call(A,E,G,F)||(C[C.length]=E)
});
return C
};
AR.every=AR.all=function(B,D,A){var C=true;
if(B==null){return C
}if(AD&&B.every===AD){return B.every(D,A)
}AP(B,function(E,G,F){if(!(C=C&&D.call(A,E,G,F))){return AK
}});
return !!C
};
var f=AR.some=AR.any=function(A,D,C){D||(D=AR.identity);
var B=false;
if(A==null){return B
}if(AB&&A.some===AB){return A.some(D,C)
}AP(A,function(F,E,G){if(B||(B=D.call(C,F,E,G))){return AK
}});
return !!B
};
AR.include=AR.contains=function(B,C){var A=false;
if(B==null){return A
}if(AG&&B.indexOf===AG){return B.indexOf(C)!=-1
}return A=f(B,function(D){return D===C
})
};
AR.invoke=function(A,C){var B=AQ.call(arguments,2);
return AR.map(A,function(D){return(AR.isFunction(C)?C||D:D[C]).apply(D,B)
})
};
AR.pluck=function(A,B){return AR.map(A,function(C){return C[B]
})
};
AR.max=function(A,D,C){if(!D&&AR.isArray(A)&&A[0]===+A[0]){return Math.max.apply(Math,A)
}if(!D&&AR.isEmpty(A)){return -Infinity
}var B={computed:-Infinity};
AP(A,function(F,E,G){E=D?D.call(C,F,E,G):F;
E>=B.computed&&(B={value:F,computed:E})
});
return B.value
};
AR.min=function(A,D,C){if(!D&&AR.isArray(A)&&A[0]===+A[0]){return Math.min.apply(Math,A)
}if(!D&&AR.isEmpty(A)){return Infinity
}var B={computed:Infinity};
AP(A,function(F,E,G){E=D?D.call(C,F,E,G):F;
E<B.computed&&(B={value:F,computed:E})
});
return B.value
};
AR.shuffle=function(B){var A=[],C;
AP(B,function(D,E){C=Math.floor(Math.random()*(E+1));
A[E]=A[C];
A[C]=D
});
return A
};
AR.sortBy=function(A,D,C){var B=AR.isFunction(D)?D:function(E){return E[D]
};
return AR.pluck(AR.map(A,function(F,E,G){return{value:F,criteria:B.call(C,F,E,G)}
}).sort(function(F,E){var H=F.criteria,G=E.criteria;
return H===void 0?1:G===void 0?-1:H<G?-1:H>G?1:0
}),"value")
};
AR.groupBy=function(A,D){var C={},B=AR.isFunction(D)?D:function(E){return E[D]
};
AP(A,function(F,E){var G=B(F,E);
(C[G]||(C[G]=[])).push(F)
});
return C
};
AR.sortedIndex=function(A,F,E){E||(E=AR.identity);
for(var D=0,C=A.length;
D<C;
){var B=D+C>>1;
E(A[B])<E(F)?D=B+1:C=B
}return D
};
AR.toArray=function(A){return !A?[]:AR.isArray(A)||AR.isArguments(A)?AQ.call(A):A.toArray&&AR.isFunction(A.toArray)?A.toArray():AR.values(A)
};
AR.size=function(A){return AR.isArray(A)?A.length:AR.keys(A).length
};
AR.first=AR.head=AR.take=function(B,A,C){return A!=null&&!C?AQ.call(B,0,A):B[0]
};
AR.initial=function(B,A,C){return AQ.call(B,0,B.length-(A==null||C?1:A))
};
AR.last=function(B,A,C){return A!=null&&!C?AQ.call(B,Math.max(B.length-A,0)):B[B.length-1]
};
AR.rest=AR.tail=function(B,A,C){return AQ.call(B,A==null||C?1:A)
};
AR.compact=function(A){return AR.filter(A,function(B){return !!B
})
};
AR.flatten=function(A,B){return AR.reduce(A,function(C,D){if(AR.isArray(D)){return C.concat(B?D:AR.flatten(D))
}C[C.length]=D;
return C
},[])
};
AR.without=function(A){return AR.difference(A,AQ.call(arguments,1))
};
AR.uniq=AR.unique=function(A,D,C){var C=C?AR.map(A,C):A,B=[];
A.length<3&&(D=true);
AR.reduce(C,function(G,F,E){if(D?AR.last(G)!==F||!G.length:!AR.include(G,F)){G.push(F);
B.push(A[E])
}return G
},[]);
return B
};
AR.union=function(){return AR.uniq(AR.flatten(arguments,true))
};
AR.intersection=AR.intersect=function(A){var B=AQ.call(arguments,1);
return AR.filter(AR.uniq(A),function(C){return AR.every(B,function(D){return AR.indexOf(D,C)>=0
})
})
};
AR.difference=function(A){var B=AR.flatten(AQ.call(arguments,1),true);
return AR.filter(A,function(C){return !AR.include(B,C)
})
};
AR.zip=function(){for(var A=AQ.call(arguments),D=AR.max(AR.pluck(A,"length")),C=Array(D),B=0;
B<D;
B++){C[B]=AR.pluck(A,""+B)
}return C
};
AR.indexOf=function(A,D,C){if(A==null){return -1
}var B;
if(C){C=AR.sortedIndex(A,D);
return A[C]===D?C:-1
}if(AG&&A.indexOf===AG){return A.indexOf(D)
}C=0;
for(B=A.length;
C<B;
C++){if(C in A&&A[C]===D){return C
}}return -1
};
AR.lastIndexOf=function(B,A){if(B==null){return -1
}if(h&&B.lastIndexOf===h){return B.lastIndexOf(A)
}for(var C=B.length;
C--;
){if(C in B&&B[C]===A){return C
}}return -1
};
AR.range=function(B,A,F){if(arguments.length<=1){A=B||0;
B=0
}for(var F=arguments[2]||1,E=Math.max(Math.ceil((A-B)/F),0),D=0,C=Array(E);
D<E;
){C[D++]=B;
B=B+F
}return C
};
var d=function(){};
AR.bind=function(A,D){var C,B;
if(A.bind===AA&&AA){return AA.apply(A,AQ.call(arguments,1))
}if(!AR.isFunction(A)){throw new TypeError
}B=AQ.call(arguments,2);
return C=function(){if(!(this instanceof C)){return A.apply(D,B.concat(AQ.call(arguments)))
}d.prototype=A.prototype;
var E=new d,F=A.apply(E,B.concat(AQ.call(arguments)));
return Object(F)===F?F:E
}
};
AR.bindAll=function(A){var B=AQ.call(arguments,1);
B.length==0&&(B=AR.functions(A));
AP(B,function(C){A[C]=AR.bind(A[C],A)
});
return A
};
AR.memoize=function(A,C){var B={};
C||(C=AR.identity);
return function(){var D=C.apply(this,arguments);
return AR.has(B,D)?B[D]:B[D]=A.apply(this,arguments)
}
};
AR.delay=function(B,A){var C=AQ.call(arguments,2);
return setTimeout(function(){return B.apply(null,C)
},A)
};
AR.defer=function(A){return AR.delay.apply(AR,[A,1].concat(AQ.call(arguments,1)))
};
AR.throttle=function(I,H){var G,F,E,D,C,B,A=AR.debounce(function(){C=D=false
},H);
return function(){G=this;
F=arguments;
E||(E=setTimeout(function(){E=null;
C&&I.apply(G,F);
A()
},H));
D?C=true:B=I.apply(G,F);
A();
D=true;
return B
}
};
AR.debounce=function(B,A,D){var C;
return function(){var F=this,E=arguments;
D&&!C&&B.apply(F,E);
clearTimeout(C);
C=setTimeout(function(){C=null;
D||B.apply(F,E)
},A)
}
};
AR.once=function(B){var A=false,C;
return function(){if(A){return C
}A=true;
return C=B.apply(this,arguments)
}
};
AR.wrap=function(B,A){return function(){var C=[B].concat(AQ.call(arguments,0));
return A.apply(this,C)
}
};
AR.compose=function(){var A=arguments;
return function(){for(var B=arguments,C=A.length-1;
C>=0;
C--){B=[A[C].apply(this,B)]
}return B[0]
}
};
AR.after=function(B,A){return B<=0?A():function(){if(--B<1){return A.apply(this,arguments)
}}
};
AR.keys=U||function(A){if(A!==Object(A)){throw new TypeError("Invalid object")
}var C=[],B;
for(B in A){AR.has(A,B)&&(C[C.length]=B)
}return C
};
AR.values=function(A){return AR.map(A,AR.identity)
};
AR.functions=AR.methods=function(A){var C=[],B;
for(B in A){AR.isFunction(A[B])&&C.push(B)
}return C.sort()
};
AR.extend=function(A){AP(AQ.call(arguments,1),function(B){for(var C in B){A[C]=B[C]
}});
return A
};
AR.pick=function(A){var B={};
AP(AR.flatten(AQ.call(arguments,1)),function(C){C in A&&(B[C]=A[C])
});
return B
};
AR.defaults=function(A){AP(AQ.call(arguments,1),function(B){for(var C in B){A[C]==null&&(A[C]=B[C])
}});
return A
};
AR.clone=function(A){return !AR.isObject(A)?A:AR.isArray(A)?A.slice():AR.extend({},A)
};
AR.tap=function(B,A){A(B);
return B
};
AR.isEqual=function(B,A){return AF(B,A,[])
};
AR.isEmpty=function(A){if(A==null){return true
}if(AR.isArray(A)||AR.isString(A)){return A.length===0
}for(var B in A){if(AR.has(A,B)){return false
}}return true
};
AR.isElement=function(A){return !!(A&&A.nodeType==1)
};
AR.isArray=AJ||function(A){return AN.call(A)=="[object Array]"
};
AR.isObject=function(A){return A===Object(A)
};
AR.isArguments=function(A){return AN.call(A)=="[object Arguments]"
};
AR.isArguments(arguments)||(AR.isArguments=function(A){return !(!A||!AR.has(A,"callee"))
});
AR.isFunction=function(A){return AN.call(A)=="[object Function]"
};
AR.isString=function(A){return AN.call(A)=="[object String]"
};
AR.isNumber=function(A){return AN.call(A)=="[object Number]"
};
AR.isFinite=function(A){return AR.isNumber(A)&&isFinite(A)
};
AR.isNaN=function(A){return A!==A
};
AR.isBoolean=function(A){return A===true||A===false||AN.call(A)=="[object Boolean]"
};
AR.isDate=function(A){return AN.call(A)=="[object Date]"
};
AR.isRegExp=function(A){return AN.call(A)=="[object RegExp]"
};
AR.isNull=function(A){return A===null
};
AR.isUndefined=function(A){return A===void 0
};
AR.has=function(B,A){return V.call(B,A)
};
AR.noConflict=function(){AC._=a;
return this
};
AR.identity=function(A){return A
};
AR.times=function(B,A,D){for(var C=0;
C<B;
C++){A.call(D,C)
}};
AR.escape=function(A){return(""+A).replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/"/g,"&quot;").replace(/'/g,"&#x27;").replace(/\//g,"&#x2F;")
};
AR.result=function(A,C){if(A==null){return null
}var B=A[C];
return AR.isFunction(B)?B.call(A):B
};
AR.mixin=function(A){AP(AR.functions(A),function(B){T(B,AR[B]=A[B])
})
};
var S=0;
AR.uniqueId=function(B){var A=S++;
return B?B+A:A
};
AR.templateSettings={evaluate:/<%([\s\S]+?)%>/g,interpolate:/<%=([\s\S]+?)%>/g,escape:/<%-([\s\S]+?)%>/g};
var g=/.^/,AL={"\\":"\\","'":"'",r:"\r",n:"\n",t:"\t",u2028:"\u2028",u2029:"\u2029"},e;
for(e in AL){AL[AL[e]]=e
}var R=/\\|'|\r|\n|\t|\u2028|\u2029/g,Q=/\\(\\|'|r|n|t|u2028|u2029)/g,c=function(A){return A.replace(Q,function(C,B){return AL[B]
})
};
AR.template=function(A,D,C){C=AR.defaults(C||{},AR.templateSettings);
A="__p+='"+A.replace(R,function(E){return"\\"+AL[E]
}).replace(C.escape||g,function(F,E){return"'+\n_.escape("+c(E)+")+\n'"
}).replace(C.interpolate||g,function(F,E){return"'+\n("+c(E)+")+\n'"
}).replace(C.evaluate||g,function(F,E){return"';\n"+c(E)+"\n;__p+='"
})+"';\n";
C.variable||(A="with(obj||{}){\n"+A+"}\n");
var A="var __p='';var print=function(){__p+=Array.prototype.join.call(arguments, '')};\n"+A+"return __p;\n",B=new Function(C.variable||"obj","_",A);
if(D){return B(D,AR)
}D=function(E){return B.call(this,E,AR)
};
D.source="function("+(C.variable||"obj")+"){\n"+A+"}";
return D
};
AR.chain=function(A){return AR(A).chain()
};
var AM=function(A){this._wrapped=A
};
AR.prototype=AM.prototype;
var Z=function(A,B){return B?AR(A).chain():A
},T=function(A,B){AM.prototype[A]=function(){var C=AQ.call(arguments);
X.call(C,this._wrapped);
return Z(B.apply(AR,C),this._chain)
}
};
AR.mixin(AR);
AP("pop,push,reverse,shift,sort,splice,unshift".split(","),function(B){var A=AO[B];
AM.prototype[B]=function(){var D=this._wrapped;
A.apply(D,arguments);
var C=D.length;
(B=="shift"||B=="splice")&&C===0&&delete D[0];
return Z(D,this._chain)
}
});
AP(["concat","join","slice"],function(B){var A=AO[B];
AM.prototype[B]=function(){return Z(A.apply(this._wrapped,arguments),this._chain)
}
});
AM.prototype.chain=function(){this._chain=true;
return this
};
AM.prototype.value=function(){return this._wrapped
}
}).call(this);
!function(I,M){var D=M.prototype.trim,A=M.prototype.trimRight,F=M.prototype.trimLeft,N=function(O){return O*1||0
},C=function(P,O){if(O<1){return""
}var Q="";
while(O>0){O&1&&(Q+=P),O>>=1,P+=P
}return Q
},L=[].slice,K=function(O){return O==null?"\\s":O.source?O.source:"["+B.escapeRegExp(O)+"]"
},H={lt:"<",gt:">",quot:'"',apos:"'",amp:"&"},E={};
for(var J in H){E[H[J]]=J
}var G=function(){function P(R){return Object.prototype.toString.call(R).slice(8,-1).toLowerCase()
}var Q=C,O=function(){return O.cache.hasOwnProperty(arguments[0])||(O.cache[arguments[0]]=O.parse(arguments[0])),O.format.call(null,O.cache[arguments[0]],arguments)
};
return O.format=function(R,W){var h=1,T=R.length,g="",b,X=[],V,Z,S,Y,e,U;
for(V=0;
V<T;
V++){g=P(R[V]);
if(g==="string"){X.push(R[V])
}else{if(g==="array"){S=R[V];
if(S[2]){b=W[h];
for(Z=0;
Z<S[2].length;
Z++){if(!b.hasOwnProperty(S[2][Z])){throw new Error(G('[_.sprintf] property "%s" does not exist',S[2][Z]))
}b=b[S[2][Z]]
}}else{S[1]?b=W[S[1]]:b=W[h++]
}if(/[^s]/.test(S[8])&&P(b)!="number"){throw new Error(G("[_.sprintf] expecting number but found %s",P(b)))
}switch(S[8]){case"b":b=b.toString(2);
break;
case"c":b=M.fromCharCode(b);
break;
case"d":b=parseInt(b,10);
break;
case"e":b=S[7]?b.toExponential(S[7]):b.toExponential();
break;
case"f":b=S[7]?parseFloat(b).toFixed(S[7]):parseFloat(b);
break;
case"o":b=b.toString(8);
break;
case"s":b=(b=M(b))&&S[7]?b.substring(0,S[7]):b;
break;
case"u":b=Math.abs(b);
break;
case"x":b=b.toString(16);
break;
case"X":b=b.toString(16).toUpperCase()
}b=/[def]/.test(S[8])&&S[3]&&b>=0?"+"+b:b,e=S[4]?S[4]=="0"?"0":S[4].charAt(1):" ",U=S[6]-M(b).length,Y=S[6]?Q(e,U):"",X.push(S[5]?b+Y:Y+b)
}}}return X.join("")
},O.cache={},O.parse=function(W){var T=W,Y=[],V=[],S=0;
while(T){if((Y=/^[^\x25]+/.exec(T))!==null){V.push(Y[0])
}else{if((Y=/^\x25{2}/.exec(T))!==null){V.push("%")
}else{if((Y=/^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(T))===null){throw new Error("[_.sprintf] huh?")
}if(Y[2]){S|=1;
var U=[],X=Y[2],R=[];
if((R=/^([a-z_][a-z_\d]*)/i.exec(X))===null){throw new Error("[_.sprintf] huh?")
}U.push(R[1]);
while((X=X.substring(R[0].length))!==""){if((R=/^\.([a-z_][a-z_\d]*)/i.exec(X))!==null){U.push(R[1])
}else{if((R=/^\[(\d+)\]/.exec(X))===null){throw new Error("[_.sprintf] huh?")
}U.push(R[1])
}}Y[2]=U
}else{S|=2
}if(S===3){throw new Error("[_.sprintf] mixing positional and named placeholders is not (yet) supported")
}V.push(Y)
}}T=T.substring(Y[0].length)
}return V
},O
}(),B={VERSION:"2.3.0",isBlank:function(O){return O==null&&(O=""),/^\s*$/.test(O)
},stripTags:function(O){return O==null?"":M(O).replace(/<\/?[^>]+>/g,"")
},capitalize:function(O){return O=O==null?"":M(O),O.charAt(0).toUpperCase()+O.slice(1)
},chop:function(O,P){return O==null?[]:(O=M(O),P=~~P,P>0?O.match(new RegExp(".{1,"+P+"}","g")):[O])
},clean:function(O){return B.strip(O).replace(/\s+/g," ")
},count:function(O,P){return O==null||P==null?0:M(O).split(P).length-1
},chars:function(O){return O==null?[]:M(O).split("")
},swapCase:function(O){return O==null?"":M(O).replace(/\S/g,function(P){return P===P.toUpperCase()?P.toLowerCase():P.toUpperCase()
})
},escapeHTML:function(O){return O==null?"":M(O).replace(/[&<>"']/g,function(P){return"&"+E[P]+";"
})
},unescapeHTML:function(O){return O==null?"":M(O).replace(/\&([^;]+);/g,function(Q,R){var P;
return R in H?H[R]:(P=R.match(/^#x([\da-fA-F]+)$/))?M.fromCharCode(parseInt(P[1],16)):(P=R.match(/^#(\d+)$/))?M.fromCharCode(~~P[1]):Q
})
},escapeRegExp:function(O){return O==null?"":M(O).replace(/([.*+?^=!:${}()|[\]\/\\])/g,"\\$1")
},splice:function(R,P,S,Q){var O=B.chars(R);
return O.splice(~~P,~~S,Q),O.join("")
},insert:function(P,O,Q){return B.splice(P,O,0,Q)
},include:function(O,P){return P===""?!0:O==null?!1:M(O).indexOf(P)!==-1
},join:function(){var P=L.call(arguments),O=P.shift();
return O==null&&(O=""),P.join(O)
},lines:function(O){return O==null?[]:M(O).split("\n")
},reverse:function(O){return B.chars(O).reverse().join("")
},startsWith:function(O,P){return P===""?!0:O==null||P==null?!1:(O=M(O),P=M(P),O.length>=P.length&&O.slice(0,P.length)===P)
},endsWith:function(O,P){return P===""?!0:O==null||P==null?!1:(O=M(O),P=M(P),O.length>=P.length&&O.slice(O.length-P.length)===P)
},succ:function(O){return O==null?"":(O=M(O),O.slice(0,-1)+M.fromCharCode(O.charCodeAt(O.length-1)+1))
},titleize:function(O){return O==null?"":M(O).replace(/(?:^|\s)\S/g,function(P){return P.toUpperCase()
})
},camelize:function(O){return B.trim(O).replace(/[-_\s]+(.)?/g,function(Q,P){return P.toUpperCase()
})
},underscored:function(O){return B.trim(O).replace(/([a-z\d])([A-Z]+)/g,"$1_$2").replace(/[-\s]+/g,"_").toLowerCase()
},dasherize:function(O){return B.trim(O).replace(/([A-Z])/g,"-$1").replace(/[-_\s]+/g,"-").toLowerCase()
},classify:function(O){return B.titleize(M(O).replace(/_/g," ")).replace(/\s/g,"")
},humanize:function(O){return B.capitalize(B.underscored(O).replace(/_id$/,"").replace(/_/g," "))
},trim:function(P,O){return P==null?"":!O&&D?D.call(P):(O=K(O),M(P).replace(new RegExp("^"+O+"+|"+O+"+$","g"),""))
},ltrim:function(O,P){return O==null?"":!P&&F?F.call(O):(P=K(P),M(O).replace(new RegExp("^"+P+"+"),""))
},rtrim:function(O,P){return O==null?"":!P&&A?A.call(O):(P=K(P),M(O).replace(new RegExp(P+"+$"),""))
},truncate:function(P,Q,O){return P==null?"":(P=M(P),O=O||"...",Q=~~Q,P.length>Q?P.slice(0,Q)+O:P)
},prune:function(R,S,Q){if(R==null){return""
}R=M(R),S=~~S,Q=Q!=null?M(Q):"...";
if(R.length<=S){return R
}var O=function(T){return T.toUpperCase()!==T.toLowerCase()?"A":" "
},P=R.slice(0,S+1).replace(/.(?=\W*\w*$)/g,O);
return P.slice(P.length-2).match(/\w\w/)?P=P.replace(/\s*\S+$/,""):P=B.rtrim(P.slice(0,P.length-1)),(P+Q).length>R.length?R:R.slice(0,P.length)+Q
},words:function(P,O){return B.isBlank(P)?[]:B.trim(P,O).split(O||/\s+/)
},pad:function(R,S,Q,O){R=R==null?"":M(R),S=~~S;
var P=0;
Q?Q.length>1&&(Q=Q.charAt(0)):Q=" ";
switch(O){case"right":return P=S-R.length,R+C(Q,P);
case"both":return P=S-R.length,C(Q,Math.ceil(P/2))+R+C(Q,Math.floor(P/2));
default:return P=S-R.length,C(Q,P)+R
}},lpad:function(P,O,Q){return B.pad(P,O,Q)
},rpad:function(P,O,Q){return B.pad(P,O,Q,"right")
},lrpad:function(P,O,Q){return B.pad(P,O,Q,"both")
},sprintf:G,vsprintf:function(P,O){return O.unshift(P),G.apply(null,O)
},toNumber:function(P,Q){if(P==null||P==""){return 0
}P=M(P);
var O=N(N(P).toFixed(~~Q));
return O===0&&!P.match(/^0+$/)?Number.NaN:O
},numberFormat:function(S,P,U,R){if(isNaN(S)||S==null){return""
}S=S.toFixed(~~P),R=R||",";
var O=S.split("."),Q=O[0],T=O[1]?(U||".")+O[1]:"";
return Q.replace(/(\d)(?=(?:\d{3})+$)/g,"$1"+R)+T
},strRight:function(P,Q){if(P==null){return""
}P=M(P),Q=Q!=null?M(Q):Q;
var O=Q?P.indexOf(Q):-1;
return ~O?P.slice(O+Q.length,P.length):P
},strRightBack:function(P,Q){if(P==null){return""
}P=M(P),Q=Q!=null?M(Q):Q;
var O=Q?P.lastIndexOf(Q):-1;
return ~O?P.slice(O+Q.length,P.length):P
},strLeft:function(P,Q){if(P==null){return""
}P=M(P),Q=Q!=null?M(Q):Q;
var O=Q?P.indexOf(Q):-1;
return ~O?P.slice(0,O):P
},strLeftBack:function(P,O){if(P==null){return""
}P+="",O=O!=null?""+O:O;
var Q=P.lastIndexOf(O);
return ~Q?P.slice(0,Q):P
},toSentence:function(S,P,T,R){P=P||", ",T=T||" and ";
var O=S.slice(),Q=O.pop();
return S.length>2&&R&&(T=B.rtrim(P)+T),O.length?O.join(P)+T+Q:Q
},toSentenceSerial:function(){var O=L.call(arguments);
return O[3]=!0,B.toSentence.apply(B,O)
},slugify:function(Q){if(Q==null){return""
}var R="Ä…Ã Ã¡Ã¤Ã¢Ã£Ã¥Ã¦Ä‡Ä™Ã¨Ã©Ã«ÃªÃ¬ÃÃ¯Ã®Å‚Å„Ã²Ã³Ã¶Ã´ÃµÃ¸Ã¹ÃºÃ¼Ã»Ã±Ã§Å¼Åº",P="aaaaaaaaceeeeeiiiilnoooooouuuunczz",O=new RegExp(K(R),"g");
return Q=M(Q).toLowerCase().replace(O,function(T){var S=R.indexOf(T);
return P.charAt(S)||"-"
}),B.dasherize(Q.replace(/[^\w\s-]/g,""))
},surround:function(P,O){return[O,P,O].join("")
},quote:function(O){return B.surround(O,'"')
},exports:function(){var P={};
for(var O in this){if(!this.hasOwnProperty(O)||O.match(/^(?:include|contains|reverse)$/)){continue
}P[O]=this[O]
}return P
},repeat:function(Q,R,P){if(Q==null){return""
}R=~~R;
if(P==null){return C(M(Q),R)
}for(var O=[];
R>0;
O[--R]=Q){}return O.join(P)
},levenshtein:function(S,U){if(S==null&&U==null){return 0
}if(S==null){return M(U).length
}if(U==null){return M(S).length
}S=M(S),U=M(U);
var R=[],P,Q;
for(var T=0;
T<=U.length;
T++){for(var O=0;
O<=S.length;
O++){T&&O?S.charAt(O-1)===U.charAt(T-1)?Q=P:Q=Math.min(R[O],R[O-1],P)+1:Q=T+O,P=R[O],R[O]=Q
}}return R.pop()
}};
B.strip=B.trim,B.lstrip=B.ltrim,B.rstrip=B.rtrim,B.center=B.lrpad,B.rjust=B.lpad,B.ljust=B.rpad,B.contains=B.include,B.q=B.quote,typeof exports!="undefined"?(typeof module!="undefined"&&module.exports&&(module.exports=B),exports._s=B):typeof define=="function"&&define.amd?define("underscore.string",[],function(){return B
}):(I._=I._||{},I._.string=I._.str=B)
}(this,String);
var Hogan={};
(function(J,H){J.Template=function(O,P,N,M){this.r=O||this.r;
this.c=N;
this.options=M;
this.text=P||"";
this.buf=(H)?[]:""
};
J.Template.prototype={r:function(O,N,M){return""
},v:C,t:E,render:function B(O,N,M){return this.ri([O],N||{},M)
},ri:function(O,N,M){return this.r(O,N,M)
},rp:function(O,Q,P,M){var N=P[O];
if(!N){return""
}if(this.c&&typeof N=="string"){N=this.c.compile(N,this.options)
}return N.ri(Q,P,M)
},rs:function(P,O,Q){var M=P[P.length-1];
if(!G(M)){Q(P,O,this);
return 
}for(var N=0;
N<M.length;
N++){P.push(M[N]);
Q(P,O,this);
P.pop()
}},s:function(S,N,Q,O,T,M,P){var R;
if(G(S)&&S.length===0){return false
}if(typeof S=="function"){S=this.ls(S,N,Q,O,T,M,P)
}R=(S==="")||!!S;
if(!O&&R&&N){N.push((typeof S=="object")?S:N[N.length-1])
}return R
},d:function(Q,N,P,R){var S=Q.split("."),T=this.f(S[0],N,P,R),M=null;
if(Q==="."&&G(N[N.length-2])){return N[N.length-1]
}for(var O=1;
O<S.length;
O++){if(T&&typeof T=="object"&&S[O] in T){M=T;
T=T[S[O]]
}else{T=""
}}if(R&&!T){return false
}if(!R&&typeof T=="function"){N.push(M);
T=this.lv(T,N,P);
N.pop()
}return T
},f:function(Q,M,P,R){var T=false,N=null,S=false;
for(var O=M.length-1;
O>=0;
O--){N=M[O];
if(N&&typeof N=="object"&&Q in N){T=N[Q];
S=true;
break
}}if(!S){return(R)?false:""
}if(!R&&typeof T=="function"){T=this.lv(T,M,P)
}return T
},ho:function(S,M,P,R,O){var Q=this.c;
var N=this.options;
N.delimiters=O;
var R=S.call(M,R);
R=(R==null)?String(R):R.toString();
this.b(Q.compile(R,N).render(M,P));
return false
},b:(H)?function(M){this.buf.push(M)
}:function(M){this.buf+=M
},fl:(H)?function(){var M=this.buf.join("");
this.buf=[];
return M
}:function(){var M=this.buf;
this.buf="";
return M
},ls:function(N,T,R,O,M,P,U){var Q=T[T.length-1],S=null;
if(!O&&this.c&&N.length>0){return this.ho(N,Q,R,this.text.substring(M,P),U)
}S=N.call(Q);
if(typeof S=="function"){if(O){return true
}else{if(this.c){return this.ho(S,Q,R,this.text.substring(M,P),U)
}}}return S
},lv:function(Q,O,P){var N=O[O.length-1];
var M=Q.call(N);
if(typeof M=="function"){M=E(M.call(N));
if(this.c&&~M.indexOf("{\u007B")){return this.c.compile(M,this.options).render(N,P)
}}return E(M)
}};
var I=/&/g,D=/</g,A=/>/g,L=/\'/g,K=/\"/g,F=/[&<>\"\']/;
function E(M){return String((M===null||M===undefined)?"":M)
}function C(M){M=E(M);
return F.test(M)?M.replace(I,"&amp;").replace(D,"&lt;").replace(A,"&gt;").replace(L,"&#39;").replace(K,"&quot;"):M
}var G=Array.isArray||function(M){return Object.prototype.toString.call(M)==="[object Array]"
}
})(typeof exports!=="undefined"?exports:Hogan);
(function(N){var F=/\S/,J=/\"/g,O=/\n/g,K=/\r/g,U=/\\/g,A={"#":1,"^":2,"/":3,"!":4,">":5,"<":6,"=":7,_v:8,"{":9,"&":10};
N.scan=function M(g,b){var p=g.length,Y=0,d=1,X=2,Z=Y,c=null,r=null,q="",k=[],f=false,o=0,l=0,h="{{",n="}}";
function m(){if(q.length>0){k.push(new String(q));
q=""
}}function a(){var t=true;
for(var s=l;
s<k.length;
s++){t=(k[s].tag&&A[k[s].tag]<A._v)||(!k[s].tag&&k[s].match(F)===null);
if(!t){return false
}}return t
}function j(v,s){m();
if(v&&a()){for(var t=l,u;
t<k.length;
t++){if(!k[t].tag){if((u=k[t+1])&&u.tag==">"){u.indent=k[t].toString()
}k.splice(t,1)
}}}else{if(!s){k.push({tag:"\n"})
}}f=false;
l=k.length
}function e(w,t){var v="="+n,s=w.indexOf(v,t),u=Q(w.substring(w.indexOf("=",t)+1,s)).split(" ");
h=u[0];
n=u[1];
return s+v.length-1
}if(b){b=b.split(" ");
h=b[0];
n=b[1]
}for(o=0;
o<p;
o++){if(Z==Y){if(W(h,g,o)){--o;
m();
Z=d
}else{if(g.charAt(o)=="\n"){j(f)
}else{q+=g.charAt(o)
}}}else{if(Z==d){o+=h.length-1;
r=A[g.charAt(o+1)];
c=r?g.charAt(o+1):"_v";
if(c=="="){o=e(g,o);
Z=Y
}else{if(r){o++
}Z=X
}f=o
}else{if(W(n,g,o)){k.push({tag:c,n:Q(q),otag:h,ctag:n,i:(c=="/")?f-n.length:o+h.length});
q="";
o+=n.length-1;
Z=Y;
if(c=="{"){if(n=="}}"){o++
}else{R(k[k.length-1])
}}}else{q+=g.charAt(o)
}}}}j(f,true);
return k
};
function R(X){if(X.n.substr(X.n.length-1)==="}"){X.n=X.n.substring(0,X.n.length-1)
}}function Q(X){if(X.trim){return X.trim()
}return X.replace(/^\s*|\s*$/g,"")
}function W(X,b,Z){if(b.charAt(Z)!=X.charAt(0)){return false
}for(var a=1,Y=X.length;
a<Y;
a++){if(b.charAt(Z+a)!=X.charAt(a)){return false
}}return true
}function B(d,a,Y,c){var X=[],b=null,Z=null;
while(d.length>0){Z=d.shift();
if(Z.tag=="#"||Z.tag=="^"||E(Z,c)){Y.push(Z);
Z.nodes=B(d,Z.tag,Y,c);
X.push(Z)
}else{if(Z.tag=="/"){if(Y.length===0){throw new Error("Closing tag without opener: /"+Z.n)
}b=Y.pop();
if(Z.n!=b.n&&!G(Z.n,b.n,c)){throw new Error("Nesting error: "+b.n+" vs. "+Z.n)
}b.end=Z.i;
return X
}else{X.push(Z)
}}}if(Y.length>0){throw new Error("missing closing tag: "+Y.pop().n)
}return X
}function E(a,Y){for(var Z=0,X=Y.length;
Z<X;
Z++){if(Y[Z].o==a.n){a.tag="#";
return true
}}}function G(b,Z,Y){for(var a=0,X=Y.length;
a<X;
a++){if(Y[a].c==b&&Y[a].o==Z){return true
}}}N.generate=function(X,a,Y){var Z='var _=this;_.b(i=i||"");'+T(X)+"return _.fl();";
if(Y.asString){return"function(c,p,i){"+Z+";}"
}return new N.Template(new Function("c","p","i",Z),a,N,Y)
};
function V(X){return X.replace(U,"\\\\").replace(J,'\\"').replace(O,"\\n").replace(K,"\\r")
}function I(X){return(~X.indexOf("."))?"d":"f"
}function T(Y){var b="";
for(var a=0,Z=Y.length;
a<Z;
a++){var X=Y[a].tag;
if(X=="#"){b+=H(Y[a].nodes,Y[a].n,I(Y[a].n),Y[a].i,Y[a].end,Y[a].otag+" "+Y[a].ctag)
}else{if(X=="^"){b+=S(Y[a].nodes,Y[a].n,I(Y[a].n))
}else{if(X=="<"||X==">"){b+=D(Y[a])
}else{if(X=="{"||X=="&"){b+=C(Y[a].n,I(Y[a].n))
}else{if(X=="\n"){b+=L('"\\n"'+(Y.length-1==a?"":" + i"))
}else{if(X=="_v"){b+=P(Y[a].n,I(Y[a].n))
}else{if(X===undefined){b+=L('"'+V(Y[a])+'"')
}}}}}}}}return b
}function H(Y,c,b,a,X,Z){return"if(_.s(_."+b+'("'+V(c)+'",c,p,1),c,p,0,'+a+","+X+',"'+Z+'")){_.rs(c,p,function(c,p,_){'+T(Y)+"});c.pop();}"
}function S(X,Z,Y){return"if(!_.s(_."+Y+'("'+V(Z)+'",c,p,1),c,p,1,0,0,"")){'+T(X)+"};"
}function D(X){return'_.b(_.rp("'+V(X.n)+'",c,p,"'+(X.indent||"")+'"));'
}function C(Y,X){return"_.b(_.t(_."+X+'("'+V(Y)+'",c,p,0)));'
}function P(Y,X){return"_.b(_.v(_."+X+'("'+V(Y)+'",c,p,0)));'
}function L(X){return"_.b("+X+");"
}N.parse=function(Y,Z,X){X=X||{};
return B(Y,"",[],X.sectionTags||[])
},N.cache={};
N.compile=function(a,X){X=X||{};
var Z=a+"||"+!!X.asString;
var Y=this.cache[Z];
if(Y){return Y
}Y=this.generate(this.parse(this.scan(a,X.delimiters),a,X),a,X);
return this.cache[Z]=Y
}
})(typeof exports!=="undefined"?exports:Hogan);
function isAuthor(){return CQURLInfo.runModes=="author"
}function isPublish(){return CQURLInfo.runModes=="publish"
}function isEditMode(){var A=getMode();
return isAuthor()&&(A=="edit"||A==null)
}function isDesignMode(){return isAuthor()&&getMode()=="design"
}function isPreviewMode(){return isAuthor()&&getMode()=="preview"
}function getMode(){return $.cookie("wcmmode")
}function hideEditables(A){$.each(A,function(C,B){var D=CQURLInfo.requestPath+"/jcr:content/"+B;
CQ.WCM.onEditableReady(D,function(E){E.hide()
})
})
}function hideEditables(A,B){$.each(A,function(D,C){var E=CQURLInfo.requestPath+"/jcr:content/"+C;
CQ.WCM.onEditableReady(E,function(F){if(CQURLInfo.requestPath!=B){F.hide()
}})
})
}var intervalId;
var carouselInterval=5000;
$(function(){$("#carousel-previous").click(function(D){D.preventDefault();
clearInterval(intervalId);
var C=($("#slideshow a.show").length?$("#slideshow a.show"):$("#slideshow a:first"));
var B=($("#slideshow_controls a.show").length?$("#slideshow_controls a.show"):$("#slideshow_controls a:nth-child(2)"));
var A=((C.hasClass("first"))?$("#slideshow a:last"):C.prev());
var E=((B.hasClass("first"))?$("#slideshow_controls a:last").prev():B.prev());
C.css({opacity:0,display:"none"}).removeClass("show");
B[0].children[0].src="main/images/bttn_banner_off.jpg"/*tpa=http://www.usfoods.com/etc/designs/usfoods/clientlibs/main/images/bttn_banner_off.jpg*/;
B.removeClass("show");
A.css({opacity:1,display:"block"}).addClass("show");
E[0].children[0].src="main/images/bttn_banner_on.jpg"/*tpa=http://www.usfoods.com/etc/designs/usfoods/clientlibs/main/images/bttn_banner_on.jpg*/;
E.addClass("show");
intervalId=setInterval("changeImage("+true+")",carouselInterval)
});
$("#carousel-next").click(function(D){D.preventDefault();
clearInterval(intervalId);
var C=($("#slideshow a.show").length?$("#slideshow a.show"):$("#slideshow a:first"));
var B=($("#slideshow_controls a.show").length?$("#slideshow_controls a.show"):$("#slideshow_controls a:nth-child(2)"));
var A=((C.hasClass("end"))?$("#slideshow a:first"):C.next());
var E=((B.hasClass("end"))?$("#slideshow_controls a:nth-child(2)"):B.next());
C.css({opacity:0,display:"none"}).removeClass("show");
B[0].children[0].src="main/images/bttn_banner_off.jpg"/*tpa=http://www.usfoods.com/etc/designs/usfoods/clientlibs/main/images/bttn_banner_off.jpg*/;
B.removeClass("show");
A.css({opacity:1,display:"block"}).addClass("show");
E[0].children[0].src="main/images/bttn_banner_on.jpg"/*tpa=http://www.usfoods.com/etc/designs/usfoods/clientlibs/main/images/bttn_banner_on.jpg*/;
E.addClass("show");
intervalId=setInterval("changeImage("+true+")",carouselInterval)
});
$('a[id^="carousel-goto-"]').click(function(E){E.preventDefault();
var G=$(this).attr("id");
var A=parseInt(G[G.length-1]);
clearInterval(intervalId);
var D=($("#slideshow a.show").length?$("#slideshow a.show"):$("#slideshow a:first"));
var C=($("#slideshow_controls a.show").length?$("#slideshow_controls a.show"):$("#slideshow_controls a:nth-child(2)"));
var B=$("#slideshow a:nth-child("+A+")");
var F=$("#slideshow_controls a:nth-child("+(A+1)+")");
D.css({opacity:0,display:"none"}).removeClass("show");
C[0].children[0].src="main/images/bttn_banner_off.jpg"/*tpa=http://www.usfoods.com/etc/designs/usfoods/clientlibs/main/images/bttn_banner_off.jpg*/;
C.removeClass("show");
B.css({opacity:1,display:"block"}).addClass("show");
F[0].children[0].src="main/images/bttn_banner_on.jpg"/*tpa=http://www.usfoods.com/etc/designs/usfoods/clientlibs/main/images/bttn_banner_on.jpg*/;
F.addClass("show");
intervalId=setInterval("changeImage("+true+")",carouselInterval)
})
});
function runSlideShow(B,A){$("#slideshow a").css({opacity:0});
$("#slideshow a:first").css({opacity:1});
carouselInterval=A;
intervalId=setInterval("changeImage("+B+")",carouselInterval)
}function changeImage(B){var D=($("#slideshow a.show")?$("#slideshow a.show"):$("#slideshow a:first"));
var A=((D.hasClass("end"))?$("#slideshow a:first"):D.next());
A.css({opacity:0,display:"block"}).addClass("show").animate({opacity:1},1000);
D.animate({opacity:0},1000,function(){D.css("display","none")
}).removeClass("show");
if(B){var C=($("#slideshow_controls a.show")?$("#slideshow_controls a.show"):$("#slideshow_controls a:nth-child(2)"));
var E=((C.hasClass("end"))?$("#slideshow_controls a:nth-child(2)"):C.next());
C.removeClass("show");
E.addClass("show");
if(C[0]!=null){C[0].children[0].src="main/images/bttn_banner_off.jpg"/*tpa=http://www.usfoods.com/etc/designs/usfoods/clientlibs/main/images/bttn_banner_off.jpg*/
}if(E[0]!=null){E[0].children[0].src="main/images/bttn_banner_on.jpg"/*tpa=http://www.usfoods.com/etc/designs/usfoods/clientlibs/main/images/bttn_banner_on.jpg*/
}}}function initializeTabs(C,A){var B=$.cookie(A);
if(B==null){B=0
}if($.cookie("wcmmode")=="edit"){CQ.WCM.on("editablesready",function(){$("#"+A).tabs({active:B,activate:function(E,F){var G=F.newTab.attr("id");
var D=G.charAt(G.length-1);
$.cookie(A,D,{path:"/"});
toggleTabEditables(C,D)
}});
toggleTabEditables(C,B)
})
}else{$("#"+A).tabs({active:B})
}$("#"+A+" ul#icons li").hover(function(){$(this).addClass("ui-state-hover")
},function(){$(this).removeClass("ui-state-hover")
})
}function toggleTabEditables(C,A){var B=CQ.WCM.getNestedEditables(C);
CQ.Ext.each(B,function(E){var D=CQ.WCM.getEditable(E);
if(E.indexOf("par"+A)>0){D.show()
}else{D.hide()
}})
}window.USF=window.USF||{};
if(!USF.utils){USF.utils={}
}USF.utils=function(){return{getUserId:function(){return CQ.shared.User.data.userID
},isLoggedIn:function(){if(CQURLInfo.runModes=="author"){return USF.utils.getUserId()!="anonymous"
}else{return true
}},removeCookies:function(){$.removeCookie("profile",{path:"/"});
$.removeCookie("employeeId",{path:"/"});
$.removeCookie("filtered-policy-groups",{path:"/"});
$.removeCookie("user-policy-tour-hide",{path:"/"});
$.removeCookie("unfiltered-policy-groups",{path:"/"})
},logout:function(){USF.utils.removeCookies();
if(CQURLInfo.runModes=="author"){$.getJSON("http://www.usfoods.com/etc/usfoods-portal/configuration/_jcr_content/configuration.json",function(json){$.cookie("message",json.logoutMessage,{path:"/"});
window.location.replace("http://www.usfoods.com/bin/logout")
})
}else{var ssoProfile=USF.utils.loadSSOLogoutConfig();
USF.utils.ssoLogout(ssoProfile.logoutUrl,ssoProfile.logoutRedirectUrl)
}},error:function(key){$.getJSON("http://www.usfoods.com/etc/usfoods-portal/configuration/_jcr_content/configuration.json",function(json){USF.utils.removeCookies();
$.cookie("error",json[key],{path:"/"});
window.location.replace("http://www.usfoods.com/usfoods/error.html")
})
},getUserEmployeeId:function(){var employeeId=null;
if(CQURLInfo.runModes=="author"){$.ajax({url:"http://www.usfoods.com/libs/granite/security/currentuser.json",async:false,data:{props:"profile/employeeid"}}).done(function(data){employeeId=data.profile.employeeid
}).fail(function(){USF.utils.error("employeeIdErrorMessage")
})
}return employeeId
},loadSSOLogoutConfig:function(){var ssoConfig=null;
var ssoCookie=$.cookie("logoutUrl");
if(ssoCookie!=null){ssoConfig=JSON.parse(ssoCookie)
}else{$.ajax({url:"/bin/logout"}).done(function(data){ssoConfig=data;
$.cookie("logoutUrl",JSON.stringify(data),{path:"/"})
}).fail(function(){USF.utils.error("employeeIdErrorMessage")
})
}return ssoConfig
},getEmployeeId:function(){var employeeId;
if(CQURLInfo.runModes=="author"){var employeeIdCookie=$.cookie("employeeId");
if(employeeIdCookie==null){employeeId=USF.utils.getUserEmployeeId();
$.cookie("employeeId",employeeId,{path:"/"})
}else{employeeId=employeeIdCookie
}}else{employeeId=USF.utils.getUserEmployeeId()
}return employeeId
},getProfile:function(){var profileData=$("body").data("profile");
var profile;
if(profileData==null){var profileCookie=$.cookie("profile");
if(profileCookie==null){var data;
if(CQURLInfo.runModes=="author"){data={employeeId:USF.utils.getEmployeeId()}
}else{data={}
}var profile=USF.utils.loadProfile(data);
$.cookie("profile",JSON.stringify(profile),{path:"/"})
}else{if(_.isEmpty(JSON.parse(profileCookie))&&USF.utils.isLoggedIn()){profile=USF.utils.loadProfile();
$.cookie("profile",JSON.stringify(profile),{path:"/"})
}else{profile=JSON.parse(profileCookie)
}}$("body").data("profile",profile)
}else{profile=profileData
}return profile
},getProfileValue:function(propertyName){return USF.utils.getProfile()[propertyName]
},loadProfile:function(data){var profile={};
$.ajax({url:"http://www.usfoods.com/bin/profile.json",async:false,cache:false,data:data}).done(function(data){profile=data
}).fail(function(){USF.utils.error("userProfileErrorMessage")
});
return profile
},getAlertsMap:function(empId){var map={peoplesoft:getPeoplesoftAlerts(empId)};
return map
},getPolls:function(){return CQ.HTTP.eval("http://www.usfoods.com/bin/polloptions.json")
},getSSOEmployeeId:function(){var result=null;
$.ajax({url:"http://www.usfoods.com/bin/employeeid.json",async:false,cache:false}).done(function(data){result=data.employeeid
}).fail(function(){USF.utils.error("employeeIdErrorMessage")
});
return result
},ssoLogout:function(serverLogoutURL,serverRedirectURL){window.location.href=serverLogoutURL+"?end_url="+serverRedirectURL
}}
}();
function getPeoplesoftAlerts(B){var A=0;
if(B==null){return 0
}if(B==0){return 0
}$.ajax({url:"/bin/peoplesoftalerts",async:false,cache:false}).done(function(G){if(G==null){return 0
}var C=0;
for(C=0;
C<G.length;
C++){var F=G[C];
var D=F["A.OPRID"];
var E=F["A.TRANSACTIONID"];
if(D==B){A=E;
break
}}}).fail(function(){alert("Error getting peoplesoft notifications")
});
return A
}var sActionKey;
var sTempS;
var sBranch;
var tempVideo;
var $videoDialog;
var videoDialogOptions={autoOpen:false,modal:true,draggable:false,resizable:false,title:"",width:882,height:700,open:function(A,B){$(".ui-dialog").css("padding","0px");
$(".ui-widget-content").css({background:"none",border:"none"})
},close:function(A,B){$("#video").remove()
}};
var addthis_config={data_ga_property:"UA-25705908-1"};
function GetAddThisButtonsDELETE(A,C){var B='<script type="text/javascript">var addthis_share = { url_transforms: { add: { video: '+window.location.href+"?video="+A+", title: "+C+"}}}<\/script>";
return B
}function archiveSelectChanged(){var A=$("#archiveSelect").attr("value");
window.location.hash=A
}function VideosSetupDELETE(){$videoDialog=$("#videoUrlContent").dialog(videoDialogOptions);
$(".videoLink").click(function(){$("#videoUrlContent").html("<div id=\"video\" style=\"width: 882px;\"><iframe frameBorder='0' scrolling='no' class='video_landing_content' src="+this.href+"></iframe></div>");
$videoDialog.dialog("open");
return false
})
}function CheckVideoUrlDELETE(){var A=GetQueryStringValue("video");
if(A!=""){$("#videoUrlContent").html("<div id=\"video\" style=\"width: 882px;\"><iframe frameBorder='0' scrolling='no' class='video_landing_content' src="+unescape(A)+"></iframe></div>");
$videoDialog.dialog("open")
}}function GetQueryStringValue(B){var D="";
if(window.location.search.indexOf(B)!=-1){var C=window.location.search.indexOf(B+"=")+1;
var A=(window.location.search.indexOf("&")!=-1)?window.location.search.indexOf("&"):window.location.search.length;
var D=window.location.search.slice(C+B.length,A)
}return D
}function EmailComments(){$("#commentsDialogTable").css("display","none");
$("#commentsThanks").css("display","block");
var B=$("#name").attr("value");
if($.trim(B)==""){B="no value provided"
}var A=$("#email").attr("value");
if($.trim(A)==""){A="no value provided"
}var C=$("#comments").attr("value");
if($.trim(C)==""){C="no value provided"
}SendEmail("listening,"+B+","+A+","+C,"")
}function ValidateCommentsForm(){var B=$.trim($("#name").attr("value"))!="";
var A=$.trim($("#email").attr("value"))!="";
var C=$.trim($("#comments").attr("value"))!="";
if(B&&A&&C){$("#commentSubmitButton").attr("disabled","")
}else{$("#commentSubmitButton").attr("disabled","disabled")
}}function EmailCallbackDELETE(A,B){}function setUpDialogDELETE(){var A=$("<div id='supplyChainAssessment'></div>").load("http://www.usfoods.com/NatAcctSignUp.html").dialog({autoOpen:false,modal:true,title:"Free Supply Chain Assessment",open:function(B,C){$("#supplyChainAssessment").css("background-image","url('main/images/bg_dialog_boxes.jpg'/*tpa=http://www.usfoods.com/etc/designs/usfoods/clientlibs/main/images/bg_dialog_boxes.jpg*/)")
}});
$("#contact_button").click(function(){A.dialog("open");
return false
})
}function checkQueryStringDELETE(){var A;
if(window.location.search.indexOf("main=true")!=-1){var A=window.location.href.replace("?main=true","");
window.location.href=A
}else{if(window.location.search.indexOf("result=success")!=-1){$("<div></div>").load("http://www.usfoods.com/Success.html").dialog({modal:true,title:"Success"})
}else{if(window.location.search.indexOf("result=error")!=-1){$("<div></div>").load("http://www.usfoods.com/Error.html").dialog({modal:true,title:"Error"})
}}}}function searchDELETE(){var A=$("#searchInput").attr("value");
window.location="http://www.usfoods.com/SearchResults.aspx?term="+A
}function searchBoxEvent(A){if(A.keyCode==13){search()
}}function showNavigation(A){if(navigator.userAgent.search("WebKit")!=-1){$('ul[id*="'+A+'"]').css({display:"inline-block"})
}else{$('ul[id*="'+A+'"]').css({display:"block"})
}}function hideNavigation(A){$('ul[id*="'+A+'"]').css({display:"none"})
}function changeyear(A){$("#year_links a").css({color:""});
A.style.color="#717073"
}function getDivision(A){var B=window.location.href;
var G=B.split("?");
if(G.length==0){return""
}var D=G[1];
var F=D.split("&");
var C="";
for(i=0;
i<F.length;
i++){var E=F[i].split("=");
if(E[0]==A){C=E[1];
break
}}C=unescape(C);
C.replace(/\+/g," ");
return C
}var input_Name;
var input_PhoneNum;
var input_Email;
var input_Bname;
var input_City;
var input_Comment;
var input_Source;
var iZipLen;
var bZipError;
var bFormError;
var errorText;
var sRoute;
var distEmail;
var input_Zipcode;
var divisionPreMsg;
var divisionPostMsg;
var validPhone=false;
var validEmail=false;
var validName=false;
var phoneFormat=/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
function isMemberOf(A){var B=false;
$.ajax({url:"http://www.usfoods.com/libs/granite/security/currentuser.json?props=memberOf",async:false}).done(function(D){for(var C=0;
C<D.memberOf.length;
C++){if(A==D.memberOf[C].name){B=true
}}}).fail(function(){B=false
});
return B
}function setEditable(B,A){if(CQ.utils.WCM.isEditMode()){CQ.WCM.onEditableReady(B,function(C){if(!A){C.hide()
}})
}}function determineEditable(B,A){var C=isMemberOf(B);
setEditable(A,C)
}function getRandomNumber(){return Math.floor((Math.random()*1000)+1)
}function setActions(A){$(A).find("#contactPhone").blur(function(){if($(this).val().length==0){return 
}var B=$(this).val();
if(isValidPhone(B)){var C=B.replace(phoneFormat,"($1) $2-$3");
$(A).find("#contactPhone").val(C);
$(A).find("#phoneError").html("&nbsp;");
validPhone=true
}else{$(A).find("#phoneError").html("Please enter a valid phone number");
validPhone=false
}});
$(A).find("#contactEmail").blur(function(){if($(this).val().length==0){return 
}if(isValidEmail($(this).val())){$(A).find("#emailError").html("&nbsp;");
validEmail=true
}else{$(A).find("#emailError").html("Please enter a valid email address");
validEmail=false
}});
$(A).find("#contactName").blur(function(){if($(this).val().length>0){$(A).find("#nameError").html("&nbsp;");
validName=true
}})
}function getZip(){errorText="";
inputSrc=document.getElementById("zipcode");
input_Zipcode=inputSrc.value;
errorText="The following fields require completion:\n";
errCount=0;
bFormError=false;
iZipLen=0;
bZipError=false;
var A;
if(input_Zipcode.length>1){for(var B=0;
B<=input_Zipcode.length-1;
B++){A=input_Zipcode.charAt(B);
if(A>="0"&&A<="9"){iZipLen++
}else{bFormError=true;
bZipError=true;
errCount=errCount+1;
errorText=errorText+"Please enter a valid 5 digit zip code.\n";
break
}}}if(iZipLen<5&&bZipError!=true){bFormError=true;
errorText=errorText+"Please enter a valid zip 5 digit code.\n"
}if(bFormError){document.getElementById("BAC_correction").innerHTML=errorText
}else{iZip=input_Zipcode.substr(0,3);
sBranch=getBranchCode(iZip);
sTemp=getBranchEmailAndName(sBranch);
sRoute=sTemp.split("|");
document.getElementById("BAC_correction").innerHTML="";
userScreen()
}}function setBACFormDivsionMsgs(B,A){divisionPreMsg=B;
divisionPostMsg=A
}function userScreen(){document.getElementById("comment").value="Comment";
$("#BAC_content").css({display:"none"});
$("#userScreen").css({display:"block"});
document.getElementById("divisioninfo").innerHTML=divisionPreMsg+" "+sRoute[1]+" "+divisionPostMsg
}function getBACDivisionName(){return sRoute[0]
}function submitUserInfo(){if(validateUser()){var A=Array();
A[0]=input_Email;
A[1]=distEmail;
A[2]="Become a Customer Request";
A[3]="The following information was submitted by an operator in your area via the new usfoods.com website. Please forward this to the appropriate member of your team.<br><br>Email: "+input_Email+"<br/><br/>Name: "+input_Name+"<br/><br/>Zipcode: "+input_Zipcode+"<br/><br/>Phone: "+input_PhoneNum+"<br/><br/>Business Name: "+input_Bname+"<br/><br/>City: "+input_City+"<br/><br/>Comment: "+input_Comment+"<br/><br/>Division Email: "+distEmail+"<br/><br/>Source: "+input_Source;
sendEmail(A[0],A[1],A[2],A[3],true);
sendEmail(A[0],"http://www.usfoods.com/etc/designs/usfoods/clientlibs/BecomeACustomer@usfoods.com",A[2],A[3],false);
clearBACForms();
successScreen()
}else{document.getElementById("BAC_correction").innerHTML=errorText
}}function validateUser(){var B=false;
bFormError=false;
errorText="";
inputSrc=document.getElementById("name");
input_Name=inputSrc.value;
inputSrc=document.getElementById("phone");
input_PhoneNum=inputSrc.value;
inputSrc=document.getElementById("email");
input_Email=inputSrc.value;
inputSrc=document.getElementById("bname");
input_Bname=inputSrc.value;
inputSrc=document.getElementById("city");
input_City=inputSrc.value;
inputSrc=document.getElementById("comment");
input_Comment=inputSrc.value;
inputSrc=document.getElementById("source");
input_Source=inputSrc.value;
if(document.getElementById("name").value=="Name"||input_Name.length<2){bFormError=true;
errCount=errCount+1;
errorText=errorText+"Please enter your name in the Name field.<br>"
}if(phoneFormat.test(input_PhoneNum)){var A=input_PhoneNum.replace(phoneFormat,"($1) $2-$3");
document.getElementById("phone").value=A;
input_PhoneNum=A
}else{errorText=errorText+"Please enter a valid phone number.<br>";
bFormError=true;
errCount=errCount+1
}if(isValidEmail(input_Email)){}else{errorText=errorText+"Please enter a valid email address.<br>";
bFormError=true;
errCount=errCount+1
}if(document.getElementById("bname").value=="Business Name"||input_Bname.length<2){bFormError=true;
errCount=errCount+1;
errorText=errorText+"Please enter your business name.<br>"
}if(document.getElementById("city").value=="City"||input_City.length<2){bFormError=true;
errCount=errCount+1;
errorText=errorText+"Please enter the name of your city.<br>"
}if(bFormError){B=false
}else{B=true
}return B
}function successScreen(){$("#userScreen").css({display:"none"});
$("#thankYouScreen").css({display:"block"})
}function isValidPhone(A){if(phoneFormat.test(A)){return true
}else{return false
}}function isValidEmail(B){var A=/^[\w_-]{2,}(.[\w-_]{2,})?@[a-zA-Z0-9.-]{2,}\.[a-zA-Z]{2,4}$/;
return A.test(B)
}function sendEmail(E,A,B,D,C){$.get("/bin/email",{subject:B,toemail:A,fromemail:E,message:D,save:C})
}function validateGateData(A){if(!validPhone||!validName||!validEmail){if(!validPhone){$(A).find("#phoneError").html("Please enter a valid phone number")
}if(!validName){$(A).find("#nameError").html("Please enter your name")
}if(!validEmail){$(A).find("#emailError").html("Please enter a valid email address")
}return false
}else{return true
}}function clearGateDataForm(A){$(A).find("#phoneError").html("&nbsp;");
$(A).find("#nameError").html("&nbsp;");
$(A).find("#emailError").html("&nbsp;");
$(A).find("#contactEmail").val("");
$(A).find("#contactName").val("");
$(A).find("#contactPhone").val("")
}function clearBACForms(){document.getElementById("BAC_correction").innerHTML="";
document.getElementById("name").value=document.getElementById("name").defaultValue;
document.getElementById("phone").value=document.getElementById("phone").defaultValue;
document.getElementById("email").value=document.getElementById("email").defaultValue;
document.getElementById("bname").value=document.getElementById("bname").defaultValue;
document.getElementById("city").value=document.getElementById("city").defaultValue;
document.getElementById("comment").value=document.getElementById("comment").defaultValue
}function validateSupplyChainForm(){var A=true;
if($("#scFirstName").val().length==0){$("#first_name_error").parent().addClass("error");
A=false
}else{$("#first_name_error").parent().removeClass("error");
$("#first_name_error").html("")
}if($("#scLastName").val().length==0){$("#last_name_error").parent().addClass("error");
A=false
}else{$("#last_name_error").parent().removeClass("error");
$("#last_name_error").html("")
}if(!isValidEmail($("#scEmail").val())){$("#email_error").parent().addClass("error");
A=false
}else{$("#email_error").parent().removeClass("error");
$("#email_error").html("")
}if($("#scTitle").val().length==0){$("#title_error").parent().addClass("error");
A=false
}else{$("#title_error").parent().removeClass("error");
$("#title_error").html("")
}if($("#scCompany").val().length==0){$("#company_error").parent().addClass("error");
A=false
}else{$("#company_error").parent().removeClass("error");
$("#company_error").html("")
}if(!isValidPhone($("#scPhone").val())){$("#phone_error").parent().addClass("error");
A=false
}else{$("#phone_error").parent().removeClass("error");
$("#phone_error").html("")
}return A
}function submitSupplyChainAssessment(C,B){if(validateSupplyChainForm()){$.cookie("gate","bypass",{path:"/",expires:365});
var A="name: "+$("#scFirstName").val()+" "+$("#scLastName").val()+"<br/><br/>email: "+$("#scEmail").val()+"<br/><br/>phone: "+$("#scPhone").val()+"<br/><br/>title: "+$("#scTitle").val()+"<br/><br/>company: "+$("#scCompany").val()+"<br/><br/># of locations: "+$("#scLocations").val()+"<br/><br/>website: "+$("#scWebsite").val()+"<br/><br/>input_source: "+$("#scWebsite").val();
sendEmail($("#scEmail").val(),C,B,A,true);
$("#supply_chain_form").hide();
$("#supply_chain_thanks").show()
}}function textCounter(B,A){if(B.value.length>A){B.value=B.value.substring(0,A);
alert("Textarea value can only be 200 characters in length.");
return false
}}function getBranchEmailAndName(B){var D;
var C;
var A;
switch(B){case"2C":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/2CDivWebsiteContact@usfoods.com";
A="Baltimore";
break;
case"2G":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/2GDivWebsiteContact@usfoods.com";
A="Southern New England";
break;
case"2H":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/WesternPADivWebsiteContact@usfoods.com";
A="Altoona";
break;
case"8E":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/WesternPADivWebsiteContact@usfoods.com";
A="Greensburg";
break;
case"2I":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/2IDivWebsiteContact@usfoods.com";
A="New York Metro";
break;
case"2J":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/2JDivWebsiteContact@usfoods.com";
A="Allentown";
break;
case"2L":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/2LDivWebsiteContact@usfoods.com";
A="West Virginia";
break;
case"2D":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/2GDivWebsiteContact@usfoods.com";
A="Southern New England";
break;
case"2N":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/2NDivWebsiteContact@usfoods.com";
A="Pittston";
break;
case"2O":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/2ODivWebsiteContact@usfoods.com";
A="Boston";
break;
case"2R":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/2RDivWebsiteContact@usfoods.com";
A="Buffalo";
break;
case"3D":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/3DDivWebsiteContact@usfoods.com";
A="Milwaukee";
break;
case"3F":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/3FDivWebsiteContact@usfoods.com";
A="Minneapolis";
break;
case"3K":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/3KDivWebsiteContact@usfoods.com";
A="Streator";
break;
case"3L":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/NDDivWebsiteContact@usfoods.com";
A="Grand Forks";
break;
case"3J":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/NDDivWebsiteContact@usfoods.com";
A="Bismarck";
break;
case"3M":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/3MDivWebsiteContact@usfoods.com";
A="Las Vegas";
break;
case"3V":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/3VDivWebsiteContact@usfoods.com";
A="Indianapolis";
break;
case"3W":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/3WDivWebsiteContact@usfoods.com";
A="Cincinnati";
break;
case"3Y":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/3YDivWebsiteContact@usfoods.com";
A="Chicago";
break;
case"3Z":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/3ZDivWebsiteContact@usfoods.com";
A="Cleveland";
break;
case"4C":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/4CDivWebsiteContact@usfoods.com";
A="Los Angelels";
break;
case"4H":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/4HDivWebsiteContact@usfoods.com";
A="Salt Lake City";
break;
case"4I":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/4IDivWebsiteContact@usfoods.com";
A="Phoenix";
break;
case"4J":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/4JDivWebsiteContact@usfoods.com";
A="San Diego";
break;
case"4O":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/4ODivWebsiteContact@usfoods.com";
A="San Francisco";
break;
case"4Q":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/4QDivWebsiteContact@usfoods.com";
A="Seattle";
break;
case"4R":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/4RDivWebsiteContact@usfoods.com";
A="Reno";
break;
case"4U":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/4UDivWebsiteContact@usfoods.com";
A="Corona";
break;
case"4V":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/4VDivWebsiteContact@usfoods.com";
A="Philadelphia";
break;
case"2Z":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/4VDivWebsiteContact@usfoods.com";
A="Swedesboro";
break;
case"5D":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/5DDivWebsiteContact@usfoods.com";
A="Lexington/Columbia";
break;
case"5E":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/5EDivWebsiteContact@usfoods.com";
A="Charlotte";
break;
case"5G":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/5GDivWebsiteContact@usfoods.com";
A="Raleigh";
break;
case"5I":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/5IDivWebsiteContact@usfoods.com";
A="Atlanta";
break;
case"5O":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/5ODivWebsiteContact@usfoods.com";
A="Manassas";
break;
case"5Y":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/5YDivWebsiteContact@usfoods.com";
A="Montgomery";
break;
case"5Z":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/5ZDivWebsiteContact@usfoods.com";
A="Port Orange";
break;
case"6B":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/6BDivWebsiteContact@usfoods.com";
A="Fort Mill";
break;
case"6C":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/5ZDivWebsiteContact@usfoods.com";
A="Port Orange";
break;
case"6D":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/6DDivWebsiteContact@usfoods.com";
A="Little Rock";
break;
case"6F":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/6KDivWebsiteContact@usfoods.com";
A="Iowa City";
break;
case"6G":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/6GDivWebsiteContact@usfoods.com";
A="Roanoke";
break;
case"6H":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/6HDivWebsiteContact@usfoods.com";
A="Knoxville";
break;
case"6I":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/6IDivWebsiteContact@usfoods.com";
A="Kansas City";
break;
case"6J":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/6JDivWebsiteContact@usfoods.com";
A="Oklahoma";
break;
case"6K":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/6KDivWebsiteContact@usfoods.com";
A="Iowa City";
break;
case"6N":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/6NDivWebsiteContact@usfoods.com";
A="Lubbock";
break;
case"6Q":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/6QDivWebsiteContact@usfoods.com";
A="Lakeland";
break;
case"6R":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/8NDivWebsiteContact@usfoods.com";
A="Boca Raton";
break;
case"6U":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/6UDivWebsiteContact@usfoods.com";
A="Jackson";
break;
case"6V":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/6VDivWebsiteContact@usfoods.com";
A="Denver";
break;
case"6W":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/6WDivWebsiteContact@usfoods.com";
A="Dallas";
break;
case"6Y":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/6Ydivwebsitecontact@usfoods.com";
A="Houston";
break;
case"6Z":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/6ZDivWebsiteContact@usfoods.com";
A="Austin";
break;
case"8L":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/8LDivWebsiteContact@usfoods.com";
A="Detroit";
break;
case"8N":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/8NDivWebsiteContact@usfoods.com";
A="Boca Raton";
break;
case"8S":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/8SDivWebsiteContact@usfoods.com";
A="Memphis";
break;
case"8U":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/8UDivWebsiteContact@usfoods.com";
A="Salem, MO";
break;
case"8V":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/8VDivWebsiteContact@usfoods.com";
A="Albuquerque";
break;
case"9B":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/9BDivWebsiteContact@usfoods.com";
A="Albany";
break;
case"9D":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/9DDivWebsiteContact@usfoods.com";
A="Tampa";
break;
case"9I":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/9IDivWebsiteContact@usfoods.com";
A="Omaha";
break;
case"9U":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/9UDivWebsiteContact@usfoods.com";
A="Allen";
break;
case"NA":D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/usfcustsupp@usfood.com";
A="Customer Service";
break;
default:D="http://www.usfoods.com/etc/designs/usfoods/clientlibs/usfcustsupp@usfood.com";
A="Customer Service"
}distEmail=D;
C="http://www.usfoods.com/etc/designs/usfoods/clientlibs/;BecomeACustomer@usfoods.com";
return(D+"|"+A);
return(D+C+"|"+A)
}function getBranchCode(B){var A;
switch(B){case"009":A="8N";
break;
case"010":A="2G";
break;
case"011":A="2G";
break;
case"012":A="9B";
break;
case"103":A="2G";
break;
case"014":A="2O";
break;
case"015":A="2G";
break;
case"016":A="2G";
break;
case"017":A="2O";
break;
case"018":A="2O";
break;
case"019":A="2O";
break;
case"020":A="2G";
break;
case"021":A="2O";
break;
case"022":A="9B";
break;
case"023":A="2G";
break;
case"024":A="2O";
break;
case"025":A="2G";
break;
case"026":A="2G";
break;
case"027":A="2G";
break;
case"028":A="2G";
break;
case"029":A="2G";
break;
case"030":A="2O";
break;
case"031":A="2O";
break;
case"032":A="2O";
break;
case"033":A="2O";
break;
case"034":A="2O";
break;
case"035":A="2O";
break;
case"036":A="9B";
break;
case"037":A="9B";
break;
case"038":A="2O";
break;
case"039":A="2O";
break;
case"040":A="2G";
break;
case"041":A="2O";
break;
case"042":A="2O";
break;
case"043":A="2O";
break;
case"044":A="2O";
break;
case"045":A="2O";
break;
case"046":A="2O";
break;
case"047":A="2O";
break;
case"048":A="2O";
break;
case"049":A="2O";
break;
case"050":A="9B";
break;
case"051":A="9B";
break;
case"052":A="9B";
break;
case"053":A="9B";
break;
case"054":A="9B";
break;
case"055":A="9B";
break;
case"056":A="9B";
break;
case"057":A="9B";
break;
case"058":A="9B";
break;
case"059":A="9B";
break;
case"061":A="2G";
break;
case"062":A="2G";
break;
case"063":A="2G";
break;
case"064":A="2G";
break;
case"065":A="2G";
break;
case"066":A="2G";
break;
case"067":A="2G";
break;
case"068":A="2G";
break;
case"069":A="2G";
break;
case"070":A="2I";
break;
case"071":A="2I";
break;
case"072":A="2I";
break;
case"073":A="2I";
break;
case"074":A="2J";
break;
case"075":A="2J";
break;
case"076":A="2I";
break;
case"077":A="2I";
break;
case"078":A="2J";
break;
case"079":A="2J";
break;
case"080":A="4V";
break;
case"081":A="2Z";
break;
case"082":A="4V";
break;
case"083":A="4V";
break;
case"084":A="4V";
break;
case"085":A="2Z";
break;
case"086":A="4V";
break;
case"087":A="4V";
break;
case"088":A="2I";
break;
case"089":A="2I";
break;
case"090":A="2I";
break;
case"091":A="2I";
break;
case"092":A="2I";
break;
case"093":A="2I";
break;
case"094":A="2I";
break;
case"095":A="2I";
break;
case"096":A="2I";
break;
case"097":A="2I";
break;
case"098":A="2I";
break;
case"099":A="2I";
break;
case"100":A="2I";
break;
case"101":A="2I";
break;
case"102":A="2I";
break;
case"103":A="2I";
break;
case"104":A="2I";
break;
case"105":A="2I";
break;
case"106":A="2I";
break;
case"107":A="2I";
break;
case"108":A="2I";
break;
case"109":A="2I";
break;
case"110":A="2I";
break;
case"111":A="2I";
break;
case"112":A="2I";
break;
case"113":A="2I";
break;
case"114":A="2I";
break;
case"115":A="2I";
break;
case"116":A="2I";
break;
case"117":A="2I";
break;
case"118":A="2I";
break;
case"119":A="2I";
break;
case"120":A="9B";
break;
case"121":A="9B";
break;
case"122":A="9B";
break;
case"123":A="9B";
break;
case"124":A="9B";
break;
case"125":A="9B";
break;
case"126":A="9B";
break;
case"127":A="9B";
break;
case"128":A="9B";
break;
case"129":A="9B";
break;
case"130":A="9B";
break;
case"131":A="9B";
break;
case"132":A="9B";
break;
case"133":A="9B";
break;
case"134":A="9B";
break;
case"135":A="9B";
break;
case"136":A="9B";
break;
case"137":A="9B";
break;
case"138":A="9B";
break;
case"139":A="9B";
break;
case"140":A="2R";
break;
case"141":A="2R";
break;
case"142":A="2R";
break;
case"143":A="2R";
break;
case"144":A="2R";
break;
case"145":A="2R";
break;
case"146":A="2R";
break;
case"147":A="2R";
break;
case"148":A="9B";
break;
case"149":A="2R";
break;
case"150":A="8E";
break;
case"151":A="8E";
break;
case"152":A="8E";
break;
case"153":A="8E";
break;
case"154":A="8E";
break;
case"155":A="2H";
break;
case"156":A="8E";
break;
case"157":A="2H";
break;
case"158":A="2H";
break;
case"159":A="2H";
break;
case"160":A="8E";
break;
case"161":A="8E";
break;
case"162":A="2H";
break;
case"163":A="8E";
break;
case"164":A="8E";
break;
case"165":A="8E";
break;
case"166":A="2H";
break;
case"167":A="8E";
break;
case"168":A="8E";
break;
case"169":A="2H";
break;
case"170":A="2J";
break;
case"171":A="2J";
break;
case"172":A="2H";
break;
case"174":A="2N";
break;
case"175":A="2J";
break;
case"176":A="2J";
break;
case"177":A="2J";
break;
case"178":A="2J";
break;
case"179":A="2J";
break;
case"180":A="2J";
break;
case"181":A="2J";
break;
case"182":A="2J";
break;
case"183":A="2J";
break;
case"184":A="2J";
break;
case"185":A="2J";
break;
case"186":A="2J";
break;
case"187":A="2J";
break;
case"188":A="2J";
break;
case"189":A="2J";
break;
case"190":A="2J";
break;
case"191":A="4V";
break;
case"192":A="4V";
break;
case"193":A="2Z";
break;
case"194":A="2Z";
break;
case"195":A="2J";
break;
case"196":A="2J";
break;
case"197":A="4V";
break;
case"198":A="2Z";
break;
case"199":A="4V";
break;
case"200":A="5O";
break;
case"201":A="5O";
break;
case"202":A="2C";
break;
case"203":A="2C";
break;
case"204":A="5O";
break;
case"205":A="2C";
break;
case"207":A="2C";
break;
case"208":A="2C";
break;
case"209":A="2C";
break;
case"210":A="2C";
break;
case"211":A="2C";
break;
case"212":A="2C";
break;
case"213":A="2C";
break;
case"214":A="2C";
break;
case"215":A="2H";
break;
case"216":A="4V";
break;
case"217":A="2C";
break;
case"218":A="4V";
break;
case"219":A="2Z";
break;
case"220":A="5O";
break;
case"221":A="5O";
break;
case"222":A="5O";
break;
case"223":A="5O";
break;
case"224":A="6G";
break;
case"225":A="6G";
break;
case"226":A="5O";
break;
case"227":A="5O";
break;
case"228":A="6G";
break;
case"229":A="6G";
break;
case"230":A="6G";
break;
case"231":A="6G";
break;
case"232":A="6G";
break;
case"233":A="5G";
break;
case"234":A="5G";
break;
case"235":A="5G";
break;
case"236":A="6G";
break;
case"237":A="5G";
break;
case"238":A="6G";
break;
case"239":A="6G";
break;
case"240":A="6G";
break;
case"241":A="6G";
break;
case"242":A="6G";
break;
case"243":A="6G";
break;
case"244":A="6G";
break;
case"245":A="6G";
break;
case"246":A="6G";
break;
case"247":A="2L";
break;
case"248":A="2L";
break;
case"249":A="2L";
break;
case"250":A="2L";
break;
case"251":A="2L";
break;
case"252":A="2L";
break;
case"253":A="2L";
break;
case"254":A="2H";
break;
case"255":A="2L";
break;
case"256":A="2L";
break;
case"257":A="2L";
break;
case"258":A="2L";
break;
case"259":A="2L";
break;
case"260":A="2L";
break;
case"261":A="2L";
break;
case"262":A="2L";
break;
case"263":A="2L";
break;
case"264":A="2L";
break;
case"265":A="2L";
break;
case"266":A="6G";
break;
case"267":A="2H";
break;
case"268":A="2H";
break;
case"269":A="2H";
break;
case"270":A="6B";
break;
case"271":A="5E";
break;
case"272":A="5E";
break;
case"273":A="5E";
break;
case"274":A="5E";
break;
case"275":A="5E";
break;
case"276":A="5E";
break;
case"277":A="5E";
break;
case"278":A="5G";
break;
case"279":A="5G";
break;
case"280":A="5G";
break;
case"281":A="5E";
break;
case"282":A="5E";
break;
case"283":A="5E";
break;
case"284":A="5E";
break;
case"285":A="5G";
break;
case"286":A="6B";
break;
case"287":A="6B";
break;
case"288":A="5E";
break;
case"289":A="6B";
break;
case"290":A="5D";
break;
case"291":A="5D";
break;
case"292":A="5D";
break;
case"293":A="6B";
break;
case"294":A="5D";
break;
case"295":A="5D";
break;
case"296":A="5D";
break;
case"297":A="5E";
break;
case"298":A="5D";
break;
case"299":A="5D";
break;
case"300":A="9B";
break;
case"301":A="5I";
break;
case"302":A="5I";
break;
case"303":A="5I";
break;
case"304":A="5D";
break;
case"305":A="5I";
break;
case"306":A="5I";
break;
case"307":A="5I";
break;
case"308":A="5D";
break;
case"309":A="5D";
break;
case"310":A="5I";
break;
case"311":A="5I";
break;
case"312":A="5I";
break;
case"313":A="5D";
break;
case"314":A="5D";
break;
case"315":A="5Z";
break;
case"316":A="5Z";
break;
case"317":A="5Z";
break;
case"318":A="5I";
break;
case"319":A="5I";
break;
case"320":A="5Z";
break;
case"321":A="5Z";
break;
case"322":A="5Z";
break;
case"323":A="5Z";
break;
case"324":A="5Y";
break;
case"325":A="5Y";
break;
case"326":A="5Z";
break;
case"327":A="5Z";
break;
case"328":A="5Z";
break;
case"329":A="5Z";
break;
case"330":A="5Z";
break;
case"331":A="8N";
break;
case"332":A="8N";
break;
case"333":A="8N";
break;
case"334":A="8N";
break;
case"335":A="9D";
break;
case"336":A="9D";
break;
case"337":A="9D";
break;
case"338":A="6Q";
break;
case"339":A="9D";
break;
case"340":A="9D";
break;
case"341":A="9D";
break;
case"342":A="9D";
break;
case"343":A="9D";
break;
case"344":A="9D";
break;
case"345":A="9D";
break;
case"346":A="9D";
break;
case"347":A="5Z";
break;
case"349":A="8N";
break;
case"350":A="5Y";
break;
case"351":A="5Y";
break;
case"352":A="5Y";
break;
case"354":A="5Y";
break;
case"355":A="5Y";
break;
case"356":A="5Y";
break;
case"357":A="5Y";
break;
case"358":A="5Y";
break;
case"359":A="5Y";
break;
case"360":A="5Y";
break;
case"361":A="5Y";
break;
case"362":A="5Y";
break;
case"363":A="5Y";
break;
case"364":A="5Y";
break;
case"365":A="5Y";
break;
case"366":A="5Y";
break;
case"367":A="5Y";
break;
case"368":A="5Y";
break;
case"369":A="6U";
break;
case"370":A="8S";
break;
case"371":A="6H";
break;
case"372":A="6H";
break;
case"373":A="6H";
break;
case"374":A="6H";
break;
case"375":A="6H";
break;
case"376":A="6H";
break;
case"377":A="6H";
break;
case"378":A="6H";
break;
case"379":A="6H";
break;
case"380":A="8S";
break;
case"381":A="8S";
break;
case"382":A="8S";
break;
case"383":A="8S";
break;
case"384":A="8S";
break;
case"385":A="6H";
break;
case"386":A="8S";
break;
case"387":A="8S";
break;
case"388":A="8S";
break;
case"389":A="8S";
break;
case"391":A="6U";
break;
case"392":A="6U";
break;
case"393":A="6U";
break;
case"394":A="6U";
break;
case"395":A="6U";
break;
case"396":A="6U";
break;
case"397":A="8S";
break;
case"398":A="5Y";
break;
case"399":A="3V";
break;
case"400":A="3V";
break;
case"401":A="3V";
break;
case"402":A="3V";
break;
case"403":A="3W";
break;
case"404":A="3W";
break;
case"405":A="3W";
break;
case"406":A="3W";
break;
case"407":A="6H";
break;
case"408":A="6H";
break;
case"409":A="6H";
break;
case"410":A="3W";
break;
case"411":A="2L";
break;
case"412":A="2L";
break;
case"413":A="3W";
break;
case"414":A="2L";
break;
case"415":A="2L";
break;
case"416":A="2L";
break;
case"417":A="6H";
break;
case"418":A="6H";
break;
case"419":A="6H";
break;
case"420":A="8S";
break;
case"421":A="8S";
break;
case"422":A="8S";
break;
case"423":A="3V";
break;
case"424":A="8S";
break;
case"425":A="6H";
break;
case"426":A="6H";
break;
case"427":A="3V";
break;
case"428":A="8Y";
break;
case"429":A="8Y";
break;
case"430":A="3W";
break;
case"431":A="3W";
break;
case"432":A="3W";
break;
case"433":A="3W";
break;
case"434":A="8L";
break;
case"435":A="8L";
break;
case"436":A="8L";
break;
case"437":A="2L";
break;
case"438":A="3W";
break;
case"439":A="8E";
break;
case"440":A="3Z";
break;
case"441":A="3Z";
break;
case"442":A="3Z";
break;
case"443":A="3Z";
break;
case"444":A="3Z";
break;
case"445":A="3Z";
break;
case"446":A="3Z";
break;
case"447":A="3Z";
break;
case"448":A="3Z";
break;
case"449":A="3Z";
break;
case"450":A="3W";
break;
case"451":A="3W";
break;
case"452":A="3W";
break;
case"453":A="3W";
break;
case"454":A="3W";
break;
case"455":A="3W";
break;
case"456":A="2L";
break;
case"457":A="2L";
break;
case"458":A="3W";
break;
case"459":A="3W";
break;
case"460":A="3V";
break;
case"461":A="3V";
break;
case"462":A="3V";
break;
case"463":A="3V";
break;
case"464":A="3V";
break;
case"465":A="3V";
break;
case"466":A="3V";
break;
case"467":A="3V";
break;
case"468":A="3V";
break;
case"469":A="3V";
break;
case"470":A="3V";
break;
case"471":A="3V";
break;
case"472":A="3V";
break;
case"473":A="3V";
break;
case"474":A="3V";
break;
case"475":A="3V";
break;
case"476":A="3V";
break;
case"478":A="3V";
break;
case"479":A="3V";
break;
case"480":A="8L";
break;
case"481":A="8L";
break;
case"482":A="8L";
break;
case"483":A="8L";
break;
case"484":A="8L";
break;
case"485":A="8L";
break;
case"486":A="8L";
break;
case"487":A="8L";
break;
case"488":A="8L";
break;
case"489":A="8L";
break;
case"490":A="8L";
break;
case"491":A="8L";
break;
case"492":A="8L";
break;
case"493":A="8L";
break;
case"494":A="8L";
break;
case"495":A="8L";
break;
case"496":A="8L";
break;
case"497":A="8L";
break;
case"498":A="3F";
break;
case"499":A="3F";
break;
case"500":A="6F";
break;
case"501":A="6F";
break;
case"502":A="6F";
break;
case"503":A="9I";
break;
case"504":A="6K";
break;
case"506":A="6F";
break;
case"507":A="6F";
break;
case"508":A="6F";
break;
case"509":A="6K";
break;
case"510":A="6K";
break;
case"511":A="9I";
break;
case"512":A="6K";
break;
case"513":A="6K";
break;
case"514":A="9I";
break;
case"515":A="9I";
break;
case"516":A="9I";
break;
case"520":A="6F";
break;
case"521":A="6F";
break;
case"522":A="6F";
break;
case"523":A="6F";
break;
case"524":A="6F";
break;
case"525":A="6F";
break;
case"526":A="6F";
break;
case"527":A="6F";
break;
case"528":A="6F";
break;
case"529":A="6F";
break;
case"530":A="3D";
break;
case"531":A="3D";
break;
case"532":A="3D";
break;
case"533":A="3D";
break;
case"534":A="3D";
break;
case"535":A="3D";
break;
case"536":A="3D";
break;
case"537":A="3D";
break;
case"538":A="6F";
break;
case"539":A="3D";
break;
case"540":A="3F";
break;
case"541":A="3D";
break;
case"542":A="3D";
break;
case"543":A="3D";
break;
case"544":A="3D";
break;
case"545":A="3D";
break;
case"546":A="3F";
break;
case"547":A="3F";
break;
case"548":A="3F";
break;
case"549":A="3D";
break;
case"550":A="3F";
break;
case"551":A="3F";
break;
case"552":A="3F";
break;
case"553":A="3F";
break;
case"554":A="3F";
break;
case"555":A="3F";
break;
case"556":A="3F";
break;
case"557":A="3F";
break;
case"558":A="3F";
break;
case"559":A="3D";
break;
case"560":A="6K";
break;
case"561":A="6K";
break;
case"562":A="3L";
break;
case"563":A="3F";
break;
case"564":A="3F";
break;
case"565":A="3L";
break;
case"566":A="3L";
break;
case"567":A="3F";
break;
case"570":A="9I";
break;
case"571":A="9I";
break;
case"572":A="3L";
break;
case"573":A="9I";
break;
case"574":A="3L";
break;
case"575":A="3J";
break;
case"576":A="3J";
break;
case"577":A="3J";
break;
case"580":A="3L";
break;
case"581":A="3L";
break;
case"582":A="3L";
break;
case"583":A="3J";
break;
case"584":A="3J";
break;
case"585":A="3J";
break;
case"586":A="3J";
break;
case"587":A="3J";
break;
case"588":A="3J";
break;
case"591":A="4H";
break;
case"593":A="3J";
break;
case"594":A="4H";
break;
case"595":A="4H";
break;
case"596":A="4H";
break;
case"597":A="4H";
break;
case"598":A="4H";
break;
case"599":A="4H";
break;
case"600":A="3Y";
break;
case"601":A="3Y";
break;
case"602":A="3Z";
break;
case"603":A="3Y";
break;
case"604":A="3Y";
break;
case"605":A="3Y";
break;
case"606":A="3Y";
break;
case"607":A="3Y";
break;
case"608":A="3Y";
break;
case"609":A="3K";
break;
case"610":A="3K";
break;
case"612":A="6F";
break;
case"613":A="3K";
break;
case"614":A="6F";
break;
case"615":A="3K";
break;
case"616":A="3K";
break;
case"617":A="3K";
break;
case"618":A="3K";
break;
case"619":A="3K";
break;
case"620":A="9U";
break;
case"621":A="9U";
break;
case"622":A="9U";
break;
case"623":A="9U";
break;
case"624":A="9U";
break;
case"625":A="3K";
break;
case"626":A="3K";
break;
case"627":A="3K";
break;
case"628":A="9U";
break;
case"629":A="9U";
break;
case"630":A="9U";
break;
case"631":A="9U";
break;
case"632":A="9U";
break;
case"633":A="9U";
break;
case"634":A="8U";
break;
case"635":A="8U";
break;
case"636":A="8U";
break;
case"637":A="8U";
break;
case"638":A="8U";
break;
case"639":A="8U";
break;
case"640":A="6I";
break;
case"641":A="6I";
break;
case"642":A="6I";
break;
case"643":A="6I";
break;
case"644":A="6I";
break;
case"645":A="6L";
break;
case"646":A="6I";
break;
case"647":A="6I";
break;
case"648":A="6L";
break;
case"649":A="6I";
break;
case"650":A="6I";
break;
case"651":A="9U";
break;
case"652":A="8U";
break;
case"653":A="6I";
break;
case"654":A="8U";
break;
case"655":A="8U";
break;
case"656":A="8U";
break;
case"657":A="8U";
break;
case"658":A="8U";
break;
case"659":A="8U";
break;
case"660":A="6I";
break;
case"661":A="6I";
break;
case"662":A="6I";
break;
case"663":A="6I";
break;
case"664":A="6I";
break;
case"665":A="6I";
break;
case"666":A="6I";
break;
case"667":A="6J";
break;
case"668":A="6I";
break;
case"669":A="6I";
break;
case"670":A="6J";
break;
case"671":A="6J";
break;
case"672":A="6L";
break;
case"673":A="6J";
break;
case"674":A="6I";
break;
case"675":A="6I";
break;
case"676":A="6I";
break;
case"677":A="6I";
break;
case"678":A="6J";
break;
case"679":A="6J";
break;
case"680":A="9I";
break;
case"681":A="9I";
break;
case"683":A="6I";
break;
case"684":A="9I";
break;
case"685":A="9I";
break;
case"686":A="9I";
break;
case"687":A="9I";
break;
case"688":A="9I";
break;
case"689":A="9I";
break;
case"690":A="6J";
break;
case"691":A="6V";
break;
case"692":A="9I";
break;
case"693":A="6V";
break;
case"700":A="6U";
break;
case"701":A="6U";
break;
case"702":A="6U";
break;
case"703":A="6U";
break;
case"704":A="6U";
break;
case"705":A="6U";
break;
case"706":A="6U";
break;
case"707":A="6U";
break;
case"708":A="6U";
break;
case"710":A="6W";
break;
case"711":A="6W";
break;
case"712":A="6U";
break;
case"713":A="6U";
break;
case"714":A="6U";
break;
case"715":A="6U";
break;
case"716":A="6D";
break;
case"717":A="6D";
break;
case"718":A="6D";
break;
case"719":A="6D";
break;
case"720":A="6D";
break;
case"721":A="6D";
break;
case"722":A="6D";
break;
case"723":A="8S";
break;
case"724":A="8S";
break;
case"725":A="6D";
break;
case"726":A="6D";
break;
case"727":A="6D";
break;
case"728":A="6D";
break;
case"729":A="6D";
break;
case"730":A="6W";
break;
case"731":A="6J";
break;
case"732":A="6J";
break;
case"733":A="6J";
break;
case"734":A="6J";
break;
case"735":A="6J";
break;
case"736":A="6J";
break;
case"737":A="6J";
break;
case"738":A="6J";
break;
case"739":A="6N";
break;
case"740":A="6J";
break;
case"741":A="6L";
break;
case"743":A="6J";
break;
case"744":A="6J";
break;
case"745":A="6J";
break;
case"746":A="6J";
break;
case"747":A="6J";
break;
case"748":A="6J";
break;
case"749":A="6J";
break;
case"750":A="6W";
break;
case"751":A="6W";
break;
case"752":A="6W";
break;
case"753":A="6W";
break;
case"754":A="6W";
break;
case"755":A="6W";
break;
case"756":A="6W";
break;
case"757":A="6W";
break;
case"758":A="6W";
break;
case"759":A="6W";
break;
case"760":A="6W";
break;
case"761":A="6W";
break;
case"762":A="6W";
break;
case"763":A="6N";
break;
case"764":A="6W";
break;
case"765":A="6Z";
break;
case"766":A="6W";
break;
case"767":A="6W";
break;
case"768":A="6N";
break;
case"769":A="6N";
break;
case"770":A="6Y";
break;
case"771":A="6Y";
break;
case"772":A="6Y";
break;
case"773":A="6Y";
break;
case"774":A="6Y";
break;
case"775":A="6Y";
break;
case"777":A="Z3";
break;
case"777":A="6Y";
break;
case"778":A="6Y";
break;
case"779":A="6Y";
break;
case"780":A="6Z";
break;
case"781":A="6Z";
break;
case"782":A="6Z";
break;
case"783":A="6Z";
break;
case"784":A="6Z";
break;
case"785":A="6Z";
break;
case"786":A="6Z";
break;
case"787":A="6Z";
break;
case"788":A="6Z";
break;
case"789":A="6Z";
break;
case"790":A="6N";
break;
case"791":A="6N";
break;
case"792":A="6N";
break;
case"793":A="6N";
break;
case"794":A="6N";
break;
case"795":A="6N";
break;
case"797":A="6N";
break;
case"798":A="8V";
break;
case"799":A="8V";
break;
case"800":A="6V";
break;
case"801":A="6V";
break;
case"802":A="6V";
break;
case"803":A="6V";
break;
case"804":A="6V";
break;
case"805":A="6V";
break;
case"806":A="6V";
break;
case"807":A="6V";
break;
case"808":A="6V";
break;
case"809":A="6V";
break;
case"810":A="6V";
break;
case"811":A="6V";
break;
case"812":A="6V";
break;
case"813":A="6V";
break;
case"814":A="6V";
break;
case"815":A="6V";
break;
case"816":A="6V";
break;
case"817":A="6V";
break;
case"818":A="6V";
break;
case"819":A="6V";
break;
case"820":A="6V";
break;
case"821":A="6V";
break;
case"822":A="6V";
break;
case"823":A="6V";
break;
case"824":A="6V";
break;
case"825":A="6V";
break;
case"826":A="6V";
break;
case"827":A="6V";
break;
case"828":A="6V";
break;
case"829":A="4H";
break;
case"830":A="4H";
break;
case"831":A="4H";
break;
case"832":A="4H";
break;
case"833":A="4H";
break;
case"834":A="4H";
break;
case"835":A="4Q";
break;
case"836":A="4H";
break;
case"837":A="4H";
break;
case"838":A="4Q";
break;
case"839":A="4H";
break;
case"840":A="4H";
break;
case"841":A="4H";
break;
case"842":A="4H";
break;
case"843":A="4H";
break;
case"844":A="4H";
break;
case"845":A="4H";
break;
case"846":A="4H";
break;
case"847":A="3M";
break;
case"850":A="4I";
break;
case"851":A="4I";
break;
case"852":A="4I";
break;
case"853":A="4I";
break;
case"854":A="4I";
break;
case"855":A="4I";
break;
case"856":A="4I";
break;
case"857":A="4I";
break;
case"858":A="4I";
break;
case"859":A="4I";
break;
case"860":A="4I";
break;
case"861":A="4I";
break;
case"862":A="4I";
break;
case"863":A="4I";
break;
case"864":A="4I";
break;
case"865":A="4I";
break;
case"870":A="8V";
break;
case"871":A="8V";
break;
case"872":A="8V";
break;
case"873":A="8V";
break;
case"874":A="8V";
break;
case"875":A="8V";
break;
case"876":A="8V";
break;
case"877":A="6V";
break;
case"878":A="8V";
break;
case"879":A="8V";
break;
case"880":A="8V";
break;
case"881":A="6N";
break;
case"882":A="6N";
break;
case"883":A="6N";
break;
case"884":A="6N";
break;
case"890":A="3M";
break;
case"891":A="3M";
break;
case"892":A="3M";
break;
case"893":A="4R";
break;
case"894":A="4R";
break;
case"895":A="4R";
break;
case"896":A="4R";
break;
case"897":A="4R";
break;
case"898":A="4R";
break;
case"900":A="4C";
break;
case"901":A="4C";
break;
case"902":A="4C";
break;
case"903":A="4C";
break;
case"904":A="4C";
break;
case"905":A="4C";
break;
case"906":A="4C";
break;
case"907":A="4C";
break;
case"908":A="4C";
break;
case"909":A="4C";
break;
case"910":A="4C";
break;
case"911":A="4C";
break;
case"912":A="4C";
break;
case"913":A="4C";
break;
case"914":A="4C";
break;
case"915":A="4C";
break;
case"916":A="4C";
break;
case"917":A="4C";
break;
case"918":A="4C";
break;
case"919":A="4J";
break;
case"920":A="4J";
break;
case"921":A="4J";
break;
case"922":A="4U";
break;
case"923":A="4U";
break;
case"924":A="4U";
break;
case"925":A="4U";
break;
case"926":A="4C";
break;
case"927":A="4C";
break;
case"928":A="4C";
break;
case"929":A="4C";
break;
case"930":A="4C";
break;
case"931":A="4C";
break;
case"932":A="4C";
break;
case"933":A="4C";
break;
case"934":A="4C";
break;
case"935":A="4C";
break;
case"936":A="4O";
break;
case"937":A="4O";
break;
case"938":A="4O";
break;
case"939":A="4O";
break;
case"940":A="4O";
break;
case"941":A="4O";
break;
case"942":A="4O";
break;
case"943":A="4O";
break;
case"944":A="4O";
break;
case"945":A="4O";
break;
case"946":A="4O";
break;
case"947":A="4O";
break;
case"948":A="4O";
break;
case"949":A="4O";
break;
case"950":A="4O";
break;
case"951":A="4O";
break;
case"952":A="4O";
break;
case"953":A="4O";
break;
case"954":A="4O";
break;
case"955":A="4O";
break;
case"956":A="4O";
break;
case"957":A="4O";
break;
case"958":A="4O";
break;
case"959":A="4O";
break;
case"960":A="4O";
break;
case"961":A="4R";
break;
case"967":A="4U";
break;
case"968":A="4U";
break;
case"970":A="4Q";
break;
case"971":A="4Q";
break;
case"972":A="4Q";
break;
case"973":A="4Q";
break;
case"974":A="4Q";
break;
case"975":A="4Q";
break;
case"976":A="4Q";
break;
case"977":A="4Q";
break;
case"978":A="4Q";
break;
case"979":A="4H";
break;
case"980":A="4Q";
break;
case"981":A="4Q";
break;
case"982":A="4Q";
break;
case"983":A="4Q";
break;
case"984":A="4Q";
break;
case"985":A="4Q";
break;
case"986":A="4Q";
break;
case"987":A="4Q";
break;
case"988":A="4Q";
break;
case"990":A="4Q";
break;
case"991":A="4Q";
break;
case"992":A="4Q";
break;
case"993":A="4Q";
break;
case"994":A="4Q";
break;
case"995":A="4Q";
break;
case"996":A="4Q";
break;
case"997":A="4Q";
break;
case"998":A="4Q";
break;
case"999":A="4Q";
break;
default:A="NA"
}return(A)
}var _gaq=_gaq||[];
_gaq.push(["_setAccount","UA-25705908-1"]);
_gaq.push(["_trackPageview"]);
(function(){var B=document.createElement("script");
B.type="text/javascript";
B.async=true;
B.src=("https:"==document.location.protocol?"https://ssl":"http://www/")+".google-analytics.com/ga.js";
var A=document.getElementsByTagName("script")[0];
A.parentNode.insertBefore(B,A)
})();
function trackPrint(){var B=document.title;
var A=B.split(" - ");
if(A.length>2){B=A[1]
}_gaq.push(["_trackEvent",B,"Print ",document.location.href])
}(function(E){E.prettyPhoto={version:"3.1.4"};
E.fn.prettyPhoto=function(G){G=jQuery.extend({hook:"rel",animation_speed:"fast",ajaxcallback:function(){},slideshow:5000,autoplay_slideshow:false,opacity:0.8,show_title:true,allow_resize:true,allow_expand:true,default_width:500,default_height:344,counter_separator_label:"/",theme:"pp_default",horizontal_padding:20,hideflash:false,wmode:"opaque",autoplay:true,modal:false,deeplinking:false,overlay_gallery:true,overlay_gallery_max:30,keyboard_shortcuts:true,changepicturecallback:function(){},callback:function(){},ie6_fallback:true,markup:'<div class="pp_pic_holder">       <div class="pp_top">        <div class="pp_left"></div>        <div class="pp_middle"></div>        <div class="pp_right"></div>       </div>       <div class="pp_content_container">        <div class="pp_left">        <div class="pp_right">         <div class="pp_content">          <div class="pp_loaderIcon"></div>          <div class="pp_fade">           <a href="#" class="pp_expand" title="Expand the image">Expand</a>           <div class="pp_hoverContainer">            <a class="pp_next" href="#">next</a>            <a class="pp_previous" href="#">previous</a>           </div>           <div id="pp_full_res"></div>           <div class="pp_details">            <div class="pp_nav">             <a href="#" class="pp_arrow_previous">Previous</a>             <p class="currentTextHolder">0/0</p>             <a href="#" class="pp_arrow_next">Next</a>            </div>            <p class="pp_description"></p>            <a class="pp_close" href="#">Close</a>           </div>          </div>         </div>        </div>        </div>       </div>       <div class="pp_bottom">        <div class="pp_left"></div>        <div class="pp_middle"></div>        <div class="pp_right"></div>       </div>      </div>      <div class="pp_overlay"></div>',gallery_markup:'<div class="pp_gallery">         <a href="#" class="pp_arrow_previous">Previous</a>         <div>          <ul>           {gallery}          </ul>         </div>         <a href="#" class="pp_arrow_next">Next</a>        </div>',image_markup:'<img id="fullResImage" src="{path}" />',flash_markup:'<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="{width}" height="{height}"><param name="wmode" value="{wmode}" /><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value="{path}" /><embed src="{path}" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="{width}" height="{height}" wmode="{wmode}"></embed></object>',quicktime_markup:'<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab" height="{height}" width="{width}"><param name="src" value="{path}"><param name="autoplay" value="{autoplay}"><param name="type" value="video/quicktime"><embed src="{path}" height="{height}" width="{width}" autoplay="{autoplay}" type="video/quicktime" pluginspage="http://www.apple.com/quicktime/download/"></embed></object>',iframe_markup:'<iframe src ="{path}" width="{width}" height="{height}" frameborder="no"></iframe>',inline_markup:'<div class="pp_inline">{content}</div>',custom_markup:"",social_tools:'<div class="twitter"><a href="http://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a><script type="text/javascript" src="../../../../../platform.twitter.com/widgets-1.js"/*tpa=http://platform.twitter.com/widgets.js*/><\/script></div><div class="facebook"><iframe src="http://www.facebook.com/plugins/like.php?locale=en_US&href={location_href}&layout=button_count&show_faces=true&width=500&action=like&font&colorscheme=light&height=23" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:500px; height:23px;" allowTransparency="true"></iframe></div>'},G);
var N=this,M=false,T,R,S,U,X,Y,I=E(window).height(),b=E(window).width(),J;
doresize=true,scroll_pos=Z();
E(window).unbind("resize.prettyphoto").bind("resize.prettyphoto",function(){Q();
W()
});
if(G.keyboard_shortcuts){E(document).unbind("keydown.prettyphoto").bind("keydown.prettyphoto",function(c){if(typeof $pp_pic_holder!="undefined"){if($pp_pic_holder.is(":visible")){switch(c.keyCode){case 37:E.prettyPhoto.changePage("previous");
c.preventDefault();
break;
case 39:E.prettyPhoto.changePage("next");
c.preventDefault();
break;
case 27:if(!settings.modal){E.prettyPhoto.close()
}c.preventDefault();
break
}}}})
}E.prettyPhoto.initialize=function(){settings=G;
if(settings.theme=="pp_default"){settings.horizontal_padding=16
}if(settings.ie6_fallback&&E.browser.msie&&parseInt(E.browser.version)==6){settings.theme="light_square"
}theRel=E(this).attr(settings.hook);
galleryRegExp=/\[(?:.*)\]/;
isSet=(galleryRegExp.exec(theRel))?true:false;
pp_images=(isSet)?jQuery.map(N,function(d,c){if(E(d).attr(settings.hook).indexOf(theRel)!=-1){return E(d).attr("href")
}}):E.makeArray(E(this).attr("href"));
pp_titles=(isSet)?jQuery.map(N,function(d,c){if(E(d).attr(settings.hook).indexOf(theRel)!=-1){return(E(d).find("img").attr("alt"))?E(d).find("img").attr("alt"):""
}}):E.makeArray(E(this).find("img").attr("alt"));
pp_descriptions=(isSet)?jQuery.map(N,function(d,c){if(E(d).attr(settings.hook).indexOf(theRel)!=-1){return(E(d).attr("title"))?E(d).attr("title"):""
}}):E.makeArray(E(this).attr("title"));
if(pp_images.length>settings.overlay_gallery_max){settings.overlay_gallery=false
}set_position=jQuery.inArray(E(this).attr("href"),pp_images);
rel_index=(isSet)?set_position:E("a["+settings.hook+"^='"+theRel+"']").index(E(this));
H(this);
if(settings.allow_resize){E(window).bind("scroll.prettyphoto",function(){Q()
})
}E.prettyPhoto.open();
return false
};
E.prettyPhoto.open=function(c){if(typeof settings=="undefined"){settings=G;
if(E.browser.msie&&E.browser.version==6){settings.theme="light_square"
}pp_images=E.makeArray(arguments[0]);
pp_titles=(arguments[1])?E.makeArray(arguments[1]):E.makeArray("");
pp_descriptions=(arguments[2])?E.makeArray(arguments[2]):E.makeArray("");
isSet=(pp_images.length>1)?true:false;
set_position=(arguments[3])?arguments[3]:0;
H(c.target)
}if(E.browser.msie&&E.browser.version==6){E("select").css("visibility","hidden")
}if(settings.hideflash){E("object,embed,iframe[src*=youtube],iframe[src*=vimeo]").css("visibility","hidden")
}L(E(pp_images).size());
E(".pp_loaderIcon").show();
if(settings.deeplinking){B()
}if(settings.social_tools){facebook_like_link=settings.social_tools.replace("{location_href}",encodeURIComponent(location.href));
$pp_pic_holder.find(".pp_social").html(facebook_like_link)
}if($ppt.is(":hidden")){$ppt.css("opacity",0).show()
}$pp_overlay.show().fadeTo(settings.animation_speed,settings.opacity);
$pp_pic_holder.find(".currentTextHolder").text((set_position+1)+settings.counter_separator_label+E(pp_images).size());
if(typeof pp_descriptions[set_position]!="undefined"&&pp_descriptions[set_position]!=""){$pp_pic_holder.find(".pp_description").show().html(unescape(pp_descriptions[set_position]))
}else{$pp_pic_holder.find(".pp_description").hide()
}movie_width=(parseFloat(A("width",pp_images[set_position])))?A("width",pp_images[set_position]):settings.default_width.toString();
movie_height=(parseFloat(A("height",pp_images[set_position])))?A("height",pp_images[set_position]):settings.default_height.toString();
M=false;
if(movie_height.indexOf("%")!=-1){movie_height=parseFloat((E(window).height()*parseFloat(movie_height)/100)-150);
M=true
}if(movie_width.indexOf("%")!=-1){movie_width=parseFloat((E(window).width()*parseFloat(movie_width)/100)-150);
M=true
}$pp_pic_holder.fadeIn(function(){(settings.show_title&&pp_titles[set_position]!=""&&typeof pp_titles[set_position]!="undefined")?$ppt.html(unescape(pp_titles[set_position])):$ppt.html("&nbsp;");
imgPreloader="";
skipInjection=false;
switch(a(pp_images[set_position])){case"image":imgPreloader=new Image();
nextImage=new Image();
if(isSet&&set_position<E(pp_images).size()-1){nextImage.src=pp_images[set_position+1]
}prevImage=new Image();
if(isSet&&pp_images[set_position-1]){prevImage.src=pp_images[set_position-1]
}$pp_pic_holder.find("#pp_full_res")[0].innerHTML=settings.image_markup.replace(/{path}/g,pp_images[set_position]);
imgPreloader.onload=function(){T=P(imgPreloader.width,imgPreloader.height);
K()
};
imgPreloader.onerror=function(){alert("Image cannot be loaded. Make sure the path is correct and image exist.");
E.prettyPhoto.close()
};
imgPreloader.src=pp_images[set_position];
break;
case"youtube":T=P(movie_width,movie_height);
movie_id=A("v",pp_images[set_position]);
if(movie_id==""){movie_id=pp_images[set_position].split("youtu.be/");
movie_id=movie_id[1];
if(movie_id.indexOf("?")>0){movie_id=movie_id.substr(0,movie_id.indexOf("?"))
}if(movie_id.indexOf("&")>0){movie_id=movie_id.substr(0,movie_id.indexOf("&"))
}}movie="http://www.youtube.com/embed/"+movie_id;
(A("rel",pp_images[set_position]))?movie+="?rel="+A("rel",pp_images[set_position]):movie+="?rel=1";
if(settings.autoplay){movie+="&autoplay=1"
}toInject=settings.iframe_markup.replace(/{width}/g,T.width).replace(/{height}/g,T.height).replace(/{wmode}/g,settings.wmode).replace(/{path}/g,movie);
break;
case"vimeo":T=P(movie_width,movie_height);
movie_id=pp_images[set_position];
var e=/http:\/\/(www\.)?vimeo.com\/(\d+)/;
var d=movie_id.match(e);
movie="http://player.vimeo.com/video/"+d[2]+"?title=0&amp;byline=0&amp;portrait=0";
if(settings.autoplay){movie+="&autoplay=1;"
}vimeo_width=T.width+"/embed/?moog_width="+T.width;
toInject=settings.iframe_markup.replace(/{width}/g,vimeo_width).replace(/{height}/g,T.height).replace(/{path}/g,movie);
break;
case"quicktime":T=P(movie_width,movie_height);
T.height+=15;
T.contentHeight+=15;
T.containerHeight+=15;
toInject=settings.quicktime_markup.replace(/{width}/g,T.width).replace(/{height}/g,T.height).replace(/{wmode}/g,settings.wmode).replace(/{path}/g,pp_images[set_position]).replace(/{autoplay}/g,settings.autoplay);
break;
case"flash":T=P(movie_width,movie_height);
flash_vars=pp_images[set_position];
flash_vars=flash_vars.substring(pp_images[set_position].indexOf("flashvars")+10,pp_images[set_position].length);
filename=pp_images[set_position];
filename=filename.substring(0,filename.indexOf("?"));
toInject=settings.flash_markup.replace(/{width}/g,T.width).replace(/{height}/g,T.height).replace(/{wmode}/g,settings.wmode).replace(/{path}/g,filename+"?"+flash_vars);
break;
case"iframe":T=P(movie_width,movie_height);
frame_url=pp_images[set_position];
frame_url=frame_url.substr(0,frame_url.indexOf("iframe")-1);
toInject=settings.iframe_markup.replace(/{width}/g,T.width).replace(/{height}/g,T.height).replace(/{path}/g,frame_url);
break;
case"ajax":doresize=false;
T=P(movie_width,movie_height);
doresize=true;
skipInjection=true;
E.get(pp_images[set_position],function(f){toInject=settings.inline_markup.replace(/{content}/g,f);
$pp_pic_holder.find("#pp_full_res")[0].innerHTML=toInject;
K()
});
break;
case"custom":T=P(movie_width,movie_height);
toInject=settings.custom_markup;
break;
case"inline":myClone=E(pp_images[set_position]).clone().append('<br clear="all" />').css({width:settings.default_width}).wrapInner('<div id="pp_full_res"><div class="pp_inline"></div></div>').appendTo(E("body")).show();
doresize=false;
T=P(E(myClone).width(),E(myClone).height());
doresize=true;
E(myClone).remove();
toInject=settings.inline_markup.replace(/{content}/g,E(pp_images[set_position]).html());
break
}if(!imgPreloader&&!skipInjection){$pp_pic_holder.find("#pp_full_res")[0].innerHTML=toInject;
K()
}});
return false
};
E.prettyPhoto.changePage=function(c){currentGalleryPage=0;
if(c=="previous"){set_position--;
if(set_position<0){set_position=E(pp_images).size()-1
}}else{if(c=="next"){set_position++;
if(set_position>E(pp_images).size()-1){set_position=0
}}else{set_position=c
}}rel_index=set_position;
if(!doresize){doresize=true
}if(settings.allow_expand){E(".pp_contract").removeClass("pp_contract").addClass("pp_expand")
}O(function(){E.prettyPhoto.open()
})
};
E.prettyPhoto.changeGalleryPage=function(c){if(c=="next"){currentGalleryPage++;
if(currentGalleryPage>totalPage){currentGalleryPage=0
}}else{if(c=="previous"){currentGalleryPage--;
if(currentGalleryPage<0){currentGalleryPage=totalPage
}}else{currentGalleryPage=c
}}slide_speed=(c=="next"||c=="previous")?settings.animation_speed:0;
slide_to=currentGalleryPage*(itemsPerPage*itemWidth);
$pp_gallery.find("ul").animate({left:-slide_to},slide_speed)
};
E.prettyPhoto.startSlideshow=function(){if(typeof J=="undefined"){$pp_pic_holder.find(".pp_play").unbind("click").removeClass("pp_play").addClass("pp_pause").click(function(){E.prettyPhoto.stopSlideshow();
return false
});
J=setInterval(E.prettyPhoto.startSlideshow,settings.slideshow)
}else{E.prettyPhoto.changePage("next")
}};
E.prettyPhoto.stopSlideshow=function(){$pp_pic_holder.find(".pp_pause").unbind("click").removeClass("pp_pause").addClass("pp_play").click(function(){E.prettyPhoto.startSlideshow();
return false
});
clearInterval(J);
J=undefined
};
E.prettyPhoto.close=function(){if($pp_overlay.is(":animated")){return 
}E.prettyPhoto.stopSlideshow();
$pp_pic_holder.stop().find("object,embed").css("visibility","hidden");
E("div.pp_pic_holder,div.ppt,.pp_fade").fadeOut(settings.animation_speed,function(){E(this).remove()
});
$pp_overlay.fadeOut(settings.animation_speed,function(){if(E.browser.msie&&E.browser.version==6){E("select").css("visibility","visible")
}if(settings.hideflash){E("object,embed,iframe[src*=youtube],iframe[src*=vimeo]").css("visibility","visible")
}E(this).remove();
E(window).unbind("scroll.prettyphoto");
C();
settings.callback();
doresize=true;
R=false;
delete settings
})
};
function K(){E(".pp_loaderIcon").hide();
projectedTop=scroll_pos.scrollTop+((I/2)-(T.containerHeight/2));
if(projectedTop<0){projectedTop=0
}$ppt.fadeTo(settings.animation_speed,1);
$pp_pic_holder.find(".pp_content").animate({height:T.contentHeight,width:T.contentWidth},settings.animation_speed);
$pp_pic_holder.animate({top:projectedTop,left:((b/2)-(T.containerWidth/2)<0)?0:(b/2)-(T.containerWidth/2),width:T.containerWidth},settings.animation_speed,function(){$pp_pic_holder.find(".pp_hoverContainer,#fullResImage").height(T.height).width(T.width);
$pp_pic_holder.find(".pp_fade").fadeIn(settings.animation_speed);
if(isSet&&a(pp_images[set_position])=="image"){$pp_pic_holder.find(".pp_hoverContainer").show()
}else{$pp_pic_holder.find(".pp_hoverContainer").hide()
}if(settings.allow_expand){if(T.resized){E("a.pp_expand,a.pp_contract").show()
}else{E("a.pp_expand").hide()
}}if(settings.autoplay_slideshow&&!J&&!R){E.prettyPhoto.startSlideshow()
}settings.changepicturecallback();
R=true
});
F();
G.ajaxcallback()
}function O(c){$pp_pic_holder.find("#pp_full_res object,#pp_full_res embed").css("visibility","hidden");
$pp_pic_holder.find(".pp_fade").fadeOut(settings.animation_speed,function(){E(".pp_loaderIcon").show();
c()
})
}function L(c){(c>1)?E(".pp_nav").show():E(".pp_nav").hide()
}function P(d,c){resized=false;
V(d,c);
imageWidth=d,imageHeight=c;
if(((Y>b)||(X>I))&&doresize&&settings.allow_resize&&!M){resized=true,fitting=false;
while(!fitting){if((Y>b)){imageWidth=(b-200);
imageHeight=(c/d)*imageWidth
}else{if((X>I)){imageHeight=(I-200);
imageWidth=(d/c)*imageHeight
}else{fitting=true
}}X=imageHeight,Y=imageWidth
}V(imageWidth,imageHeight);
if((Y>b)||(X>I)){P(Y,X)
}}return{width:Math.floor(imageWidth),height:Math.floor(imageHeight),containerHeight:Math.floor(X),containerWidth:Math.floor(Y)+(settings.horizontal_padding*2),contentHeight:Math.floor(S),contentWidth:Math.floor(U),resized:resized}
}function V(d,c){d=parseFloat(d);
c=parseFloat(c);
$pp_details=$pp_pic_holder.find(".pp_details");
$pp_details.width(d);
detailsHeight=parseFloat($pp_details.css("marginTop"))+parseFloat($pp_details.css("marginBottom"));
$pp_details=$pp_details.clone().addClass(settings.theme).width(d).appendTo(E("body")).css({position:"absolute",top:-10000});
detailsHeight+=$pp_details.height();
detailsHeight=(detailsHeight<=34)?36:detailsHeight;
if(E.browser.msie&&E.browser.version==7){detailsHeight+=8
}$pp_details.remove();
$pp_title=$pp_pic_holder.find(".ppt");
$pp_title.width(d);
titleHeight=parseFloat($pp_title.css("marginTop"))+parseFloat($pp_title.css("marginBottom"));
$pp_title=$pp_title.clone().appendTo(E("body")).css({position:"absolute",top:-10000});
titleHeight+=$pp_title.height();
$pp_title.remove();
S=c+detailsHeight;
U=d;
X=S+titleHeight+$pp_pic_holder.find(".pp_top").height()+$pp_pic_holder.find(".pp_bottom").height();
Y=d
}function a(c){if(c.match(/youtube\.com\/watch/i)||c.match(/youtu\.be/i)){return"youtube"
}else{if(c.match(/vimeo\.com/i)){return"vimeo"
}else{if(c.match(/\b.mov\b/i)){return"quicktime"
}else{if(c.match(/\b.swf\b/i)){return"flash"
}else{if(c.match(/\biframe=true\b/i)){return"iframe"
}else{if(c.match(/\bajax=true\b/i)){return"ajax"
}else{if(c.match(/\bcustom=true\b/i)){return"custom"
}else{if(c.substr(0,1)=="#"){return"inline"
}else{return"image"
}}}}}}}}}function Q(){if(doresize&&typeof $pp_pic_holder!="undefined"){scroll_pos=Z();
contentHeight=$pp_pic_holder.height(),contentwidth=$pp_pic_holder.width();
projectedTop=(I/2)+scroll_pos.scrollTop-(contentHeight/2);
if(projectedTop<0){projectedTop=0
}if(contentHeight>I){return 
}$pp_pic_holder.css({top:projectedTop,left:(b/2)+scroll_pos.scrollLeft-(contentwidth/2)})
}}function Z(){if(self.pageYOffset){return{scrollTop:self.pageYOffset,scrollLeft:self.pageXOffset}
}else{if(document.documentElement&&document.documentElement.scrollTop){return{scrollTop:document.documentElement.scrollTop,scrollLeft:document.documentElement.scrollLeft}
}else{if(document.body){return{scrollTop:document.body.scrollTop,scrollLeft:document.body.scrollLeft}
}}}}function W(){I=E(window).height(),b=E(window).width();
if(typeof $pp_overlay!="undefined"){$pp_overlay.height(E(document).height()).width(b)
}}function F(){if(isSet&&settings.overlay_gallery&&a(pp_images[set_position])=="image"&&(settings.ie6_fallback&&!(E.browser.msie&&parseInt(E.browser.version)==6))){itemWidth=52+5;
navWidth=(settings.theme=="facebook"||settings.theme=="pp_default")?50:30;
itemsPerPage=Math.floor((T.containerWidth-100-navWidth)/itemWidth);
itemsPerPage=(itemsPerPage<pp_images.length)?itemsPerPage:pp_images.length;
totalPage=Math.ceil(pp_images.length/itemsPerPage)-1;
if(totalPage==0){navWidth=0;
$pp_gallery.find(".pp_arrow_next,.pp_arrow_previous").hide()
}else{$pp_gallery.find(".pp_arrow_next,.pp_arrow_previous").show()
}galleryWidth=itemsPerPage*itemWidth;
fullGalleryWidth=pp_images.length*itemWidth;
$pp_gallery.css("margin-left",-((galleryWidth/2)+(navWidth/2))).find("div:first").width(galleryWidth+5).find("ul").width(fullGalleryWidth).find("li.selected").removeClass("selected");
goToPage=(Math.floor(set_position/itemsPerPage)<totalPage)?Math.floor(set_position/itemsPerPage):totalPage;
E.prettyPhoto.changeGalleryPage(goToPage);
$pp_gallery_li.filter(":eq("+set_position+")").addClass("selected")
}else{$pp_pic_holder.find(".pp_content").unbind("mouseenter mouseleave")
}}function H(c){if(settings.social_tools){facebook_like_link=settings.social_tools.replace("{location_href}",encodeURIComponent(location.href))
}settings.markup=settings.markup.replace("{pp_social}","");
E("body").append(settings.markup);
$pp_pic_holder=E(".pp_pic_holder"),$ppt=E(".ppt"),$pp_overlay=E("div.pp_overlay");
if(isSet&&settings.overlay_gallery){currentGalleryPage=0;
toInject="";
for(var d=0;
d<pp_images.length;
d++){if(!pp_images[d].match(/\b(jpg|jpeg|png|gif)\b/gi)){classname="default";
img_src=""
}else{classname="";
img_src=pp_images[d]
}toInject+="<li class='"+classname+"'><a href='#'><img src='"+img_src+"' width='50' alt='' /></a></li>"
}toInject=settings.gallery_markup.replace(/{gallery}/g,toInject);
$pp_pic_holder.find("#pp_full_res").after(toInject);
$pp_gallery=E(".pp_pic_holder .pp_gallery"),$pp_gallery_li=$pp_gallery.find("li");
$pp_gallery.find(".pp_arrow_next").click(function(){E.prettyPhoto.changeGalleryPage("next");
E.prettyPhoto.stopSlideshow();
return false
});
$pp_gallery.find(".pp_arrow_previous").click(function(){E.prettyPhoto.changeGalleryPage("previous");
E.prettyPhoto.stopSlideshow();
return false
});
$pp_pic_holder.find(".pp_content").hover(function(){$pp_pic_holder.find(".pp_gallery:not(.disabled)").fadeIn()
},function(){$pp_pic_holder.find(".pp_gallery:not(.disabled)").fadeOut()
});
itemWidth=52+5;
$pp_gallery_li.each(function(e){E(this).find("a").click(function(){E.prettyPhoto.changePage(e);
E.prettyPhoto.stopSlideshow();
return false
})
})
}if(settings.slideshow){$pp_pic_holder.find(".pp_nav").prepend('<a href="#" class="pp_play">Play</a>');
$pp_pic_holder.find(".pp_nav .pp_play").click(function(){E.prettyPhoto.startSlideshow();
return false
})
}$pp_pic_holder.attr("class","pp_pic_holder "+settings.theme);
$pp_overlay.css({opacity:0,height:E(document).height(),width:E(window).width()}).bind("click",function(){if(!settings.modal){E.prettyPhoto.close()
}});
E("a.pp_close").bind("click",function(){E.prettyPhoto.close();
return false
});
if(settings.allow_expand){E("a.pp_expand").bind("click",function(f){if(E(this).hasClass("pp_expand")){E(this).removeClass("pp_expand").addClass("pp_contract");
doresize=false
}else{E(this).removeClass("pp_contract").addClass("pp_expand");
doresize=true
}O(function(){E.prettyPhoto.open()
});
return false
})
}$pp_pic_holder.find(".pp_previous, .pp_nav .pp_arrow_previous").bind("click",function(){E.prettyPhoto.changePage("previous");
E.prettyPhoto.stopSlideshow();
return false
});
$pp_pic_holder.find(".pp_next, .pp_nav .pp_arrow_next").bind("click",function(){E.prettyPhoto.changePage("next");
E.prettyPhoto.stopSlideshow();
return false
});
Q()
}if(!pp_alreadyInitialized&&D()){pp_alreadyInitialized=true;
hashIndex=D();
hashRel=hashIndex;
hashIndex=hashIndex.substring(hashIndex.indexOf("/")+1,hashIndex.length-1);
hashRel=hashRel.substring(0,hashRel.indexOf("/"));
setTimeout(function(){E("a["+G.hook+"^='"+hashRel+"']:eq("+hashIndex+")").trigger("click")
},50)
}return this.unbind("click.prettyphoto").bind("click.prettyphoto",E.prettyPhoto.initialize)
};
function D(){url=location.href;
hashtag=(url.indexOf("#prettyPhoto")!==-1)?decodeURI(url.substring(url.indexOf("#prettyPhoto")+1,url.length)):false;
return hashtag
}function B(){if(typeof theRel=="undefined"){return 
}location.hash=theRel+"/"+rel_index+"/"
}function C(){if(location.href.indexOf("#prettyPhoto")!==-1){location.hash="prettyPhoto"
}}function A(H,G){H=H.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");
var F="[\\?&]"+H+"=([^&#]*)";
var J=new RegExp(F);
var I=J.exec(G);
return(I==null)?"":I[1]
}})(jQuery);
var pp_alreadyInitialized=false;
var popupStatus=0;
function loadPopup(){if(popupStatus==0){$("#backgroundPopup").css({opacity:"0.7"});
$("#backgroundPopup").fadeIn("slow");
$("#popupContent").fadeIn("slow");
popupStatus=1
}}function disablePopup(){if(popupStatus==1){$("#backgroundPopup").fadeOut("slow");
$("#popupContent").fadeOut("slow");
popupStatus=0
}}function centerPopup(){var A=document.documentElement.clientWidth;
var D=document.documentElement.clientHeight;
var C=$("#popupContent").height();
var B=$("#popupContent").width();
$("#popupContent").css({position:"absolute",top:D/2-C/2,left:A/2-B/2});
$("#backgroundPopup").css({height:D})
}$(document).ready(function(){$("#popupButton").click(function(){centerPopup();
loadPopup()
});
$("#popupContentClose").click(function(){disablePopup()
});
$("#backgroundPopup").click(function(){disablePopup()
});
$(document).keypress(function(A){if(A.keyCode==27&&popupStatus==1){disablePopup()
}})
});