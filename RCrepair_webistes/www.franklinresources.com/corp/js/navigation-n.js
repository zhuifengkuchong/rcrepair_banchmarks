var $j = jQuery.noConflict();
$j( function() {
	
	// primary navigation
		$j("ul.secNavDropDwn").hide();
		$j("ul.secLevel li").not("#currentLink").find("ul").hide();
		$j("ul.terLevel li").not("#currentLink").find("ul").hide();

	$j("ul.priNav li > a").append("<b></b>");
	$j("ul#globalNav > li:last a").css("border-right", "0px");
	$j("div#footer ul > li:first a").css("border-right", "0px");
	$j("#priNavBlock ul.priNav > li").hover( function() {
		$j(this).addClass("dropTrigger");
		$j("ul.secNavDropDwn", this).show();
		$j(this).prev("li").find("a b").addClass("border");
	}, function() {
		$j(this).removeClass("dropTrigger");
		$j("ul.secNavDropDwn", this).hide();
		$j(this).prev("li").find("a b").removeClass("border");
	});

	$j("ul.secNavDropDwn li:first").addClass("fc");
	$j("ul.secNavDropDwn li:last").addClass("lc");

	$j("label.LegalCopy").click(
	function(){
	return false;
	});
	$j("label.LegalCopy a").click(
	function(){
	var legalCopyTarget = $j(this).attr("href");
	window.location = legalCopyTarget;
	});
	
	$j("ul.priNav #currentPriNav").prev("li").find("a b").remove();

	jQuery("#sideNav li.focused > a:first").addClass("path");
	jQuery("ul.terLevel li.focused > a:first").addClass("path");
	$j("#sideNav li:last-child").addClass("lc");
	jQuery("#sideNav ul>li.focused>a, #sideNav ul>li#currentLink>span").append("<b></b>");

					
	/*commented for cap_gains :*/  
	/*
	$j("table :first-child").addClass("fc");
	$j("table :last-child").addClass("lc");
	$j("ul li:first-child").addClass("fc");

	$j(":last-child").addClass("lc");
	*/
	$j("table td:last-child, table th:last-child").addClass("lc");

	/* Added for side Navigation - start */

	$j("ul.secLevel li.focused").find("ul").show();
	$j("ul.terLevel li.focused").find("ul").show();
	
	jQuery(".priNav>li>a").click(
			function() {
				var currentURL = jQuery(this).attr("href");
				if (currentURL.indexOf("#") >= 0) {
					var target = $j(this).next("ul").find("li:first-child")
							.find("a").attr("href");
					if (target != null) {
						window.location = target;
					} else {
						window.location = window.location;
					}
				}
			}
	);
	
	$j("ul.terLevel li a").click(
			function() {
				var currentURL = $j(this).attr("href");
				if (currentURL.indexOf("#") >= 0) {
					var target = $j(this).next("ul").find("li:first-child")
							.find("a").attr("href");
					if (target != null) {
						window.location = target;
					} else {
						window.location = window.location;
					}
				}
			}
	);
	
	$j("ul.secLevel li a").click(
			function() {
				var currentURL = $j(this).attr("href");
				if (currentURL.indexOf("#") >= 0) {
					var target = $j(this).next("ul").find("li:first-child")
							.find("a").attr("href");
					if (target != null) {
						window.location = target;
					} else {
						window.location = window.location;
					}
				}
			}
	);


	
	$j('a.restrictedLink').each(function(){
			$j(this).cluetip({
			width: 200,
			arrows: true,
			showTitle: false,
			activation: 'click',
			sticky: true,
			positionBy: 'bottomTop',
			local: true,
			attribute: 'href',
			hideLocal: true,
			closePosition:    'bottom',
			dropShadow: false,
			closeText: '<img style="padding:5px;" src="Unknown_83_filename"/*tpa=http://www.franklinresources.com/retail/new/img/tooltip/tool_tip_close.PNG*//>'
			});
		});	
	
	/* Added for side Navigation - end */
	
	/* Added for actor's Home Page - Start*/
	$j("#leftNavHomePage").nextAll("#fundsearch").addClass("fundsearchhomepage");
		
	/* Added for actor's Home Page - End*/
	
	/* side navigation */
	$j("ul.secLevel > li").not("#currentLink").hover( function() {
		$j(this).addClass('sideLinkHover');
	}, function() {
		$j(this).removeClass('sideLinkHover');
	});

	$j("thead th").not(".ignore").hover( function() {
		$j(this).addClass('hvr');
	}, function() {
		$j(this).removeClass('hvr');
	});

	$j("ul.secLevel > li:last").css("border-bottom", "1px solid #ddd");

	if (parseInt($j("#sideNav").length + $j("#userSideNav").length) == 0) {
		$j("#contentWrapper").addClass("contentOneCol");

		if (parseInt($j("#advisorbar").length) == 1) {
			$j("#advisorbar").prependTo("#mainContent");
			$j("#mainContent h1:first").addClass("offset");
		}

	} else {
		$j("#sideNav").prepend("<b class='sn-shad pngfix'></b>");
		if (parseInt($j("#sideBar").length) == 1) {
			$j("#contentWrapper").addClass("contentThreeCol");
		} else {
			$j("#contentWrapper").addClass("contentTwoCol");
		}
	}

	//LALALA extra stuff

	$j("#ppss ul.tabList li:last, #fundInfo ul.tabList li:last").addClass(
			"shadow");

	$j(".tabContainer ul.tabList > li:last").removeClass("shadow");

	/**/
	$j("input:button, input:submit, input:reset").not(
			".cancel, .go, .inactive, .inactivego, :disabled").wrap(
			"<span class='btn'></span>");

	$j("button").not(".cancel, .go, .inactive, :disabled").addClass("btn")
			.wrapInner("<span></span>");

	$j("input:disabled, input.inactive").not(":radio, :checkbox").wrap(
			"<span class='inactivebtn'></span>");

	$j("button:disabled, button.inactive").addClass("inactiveBtn").wrapInner(
			"<span></span>");

	$j("hr").wrap("<div class='hr'></div>");

	$j.fn.axeHeight = function(px) {
		$j(this).each( function() {
			var myTallest = 0;
			var children = $j(".equal", this);

			children.each( function(i) {
				//alert($j(this).height());
					if ($j(this).height() > myTallest) {
						myTallest = $j(this).height();
					}
				});

			if (jQuery.browser.msie && jQuery.browser.version == 6.0) {
				children.css( {
					'height' : myTallest
				});
			}
			children.css( {
				'height' : myTallest
			});
		});
		return this;
	};

	$j(".equalize").axeHeight();

	$j("#advmenu").hide();
	$j("#advisorbar a.trigger").hover( function() {
		$j(this).addClass("advactive");
	}, function() {
		$j(this).removeClass("advactive");
	}).click( function() {
		$j(this).toggleClass("advclicked");
		$j("#advmenu").css("top", "34px").slideToggle();
	});

	$j("input:text, input:password").each( function() {
		var t = $j(this).attr("title");

		if (t !== "") {
			$j(this).addClass("defval").val(t);

			$j(this).focus( function() {
				$j(this).removeClass("defval");
				if ($j(this).val() == t) {
					$j(this).val("");
				}
			});

			$j(this).blur( function() {
				if ($j(this).val() == "") {
					$j(this).addClass("defval").val(t);
				}
			});
		}
		$j(this).addClass("txt");
	});

	/***************************************************************************
	 * expand Collapse model, apply to a container holding exp/collapse items
	 **************************************************************************/
	jQuery.fn.initExpCollapse = function(settings) {
		settings = jQuery.extend( {
			speed : 'fast',
			ctrlExpand : 'expandAll',
			ctrlCollapse : 'collapseAll'
		}, settings ? settings : {});

		var globalControls = jQuery('li.expControl a', this);
		var expPanes = jQuery('> li', this).not('.expControl');
		var expTriggers = jQuery('> div:first', expPanes);
		var expContents = jQuery('> div:last', expPanes);
		globalControls.click( function() {
			if (jQuery(this).hasClass(settings.ctrlExpand)) {
				expPanes.addClass('active');
				expContents.slideDown();
			}

			if (jQuery(this).hasClass(settings.ctrlCollapse)) {
				expPanes.removeClass('active');
				expContents.slideUp();
			}
			return false;
		});

		expTriggers.click( function() {
			jQuery(this).parent("li").toggleClass('active');

		});

		expTriggers.click( function() {
			jQuery(this).next(expContents).slideToggle();
		});

		expTriggers.hover( function() {
			jQuery(this).addClass("current");
		}, function() {
			jQuery(this).removeClass("current");
		});
		return this;
	};

	function anchorScroll() {
		function filterPath(string) {
			return string.replace(/^\//, '').replace(
					/(index|default).[a-zA-Z]{3,4}$/, '').replace(/\/$/, '');
		}

		var locationPath = filterPath(location.pathname);

		var scrollElement = 'html, body';
		$j('html, body').each( function() {
			var initScrollTop = $j(this).attr('scrollTop');
			$j(this).attr('scrollTop', initScrollTop + 1);
			if ($j(this).attr('scrollTop') == initScrollTop + 1) {
				scrollElement = this.nodeName.toLowerCase();
				$j(this).attr('scrollTop', initScrollTop);
				return false;
			}
		});

		$j('.anchor a[href*=#]')
				.not("ul.tabList a")
				.each(
						function() {
							var thisPath = filterPath(this.pathname)
									|| locationPath;
							if (locationPath == thisPath
									&& (location.hostname == this.hostname || !this.hostname)
									&& this.hash.replace(/#/, '')) {
								if ($j(this.hash).length) {
									$j(this)
											.click(
													function(event) {
														var targetOffset = $j(
																this.hash)
																.offset().top;
														var target = this.hash;
														event.preventDefault();
														$j(scrollElement)
																.animate(
																		{
																			scrollTop : targetOffset
																		},
																		500,
																		function() {
																			location.hash = target;
																		});
													});
								}
							}
						});
	}

	//anchorScroll();

	if ($j("#mainContent .legalinfo").length > 0) {
		$j("#contentWrapper").css("padding-bottom", "0");
	}

	if (!jQuery("#userSideNav").length) {
		jQuery("#sideNav").prepend("<b id='cover'></b>");
	}

	setTimeout(initExternalLinks, 200);

	if ($j("#gp-side .other").length > 0) {
		$j("#gp-side .other .con").prepend("<b class='h'></b>").append("<b class='f'></b>")
	}

});


	/*
	*Handling external 3rd party links
	*/
	function initExternalLinks(){
		var svrDomain = location.hostname;
    var externalLinks = $j("a[href*=leaving_site.jsp], a[rel=#ltBox-leavingSite]");
    
	  if (externalLinks.length > 0 ){
			$j.getScript("jquery.tools.min.js"/*tpa=http://www.franklinresources.com/corp/js/jquery.tools.min.js*/, function(){

					$j("body").append("<div id='ltBox-leavingSite' class='hide'><h2>You are about to leave the Franklin Templeton Website</h2><p>Clicking OK below will take you to an independent site. Information and services provided on this independent site are not reviewed by, guaranteed by, or endorsed by Franklin Templeton or its affiliates. Please keep in mind that this independent site's terms and conditions, privacy and security policies, or other legal information may be different from those of Franklin Templeton's site. Franklin Templeton is not liable for any direct or indirect technical or system issues, consequences, or damages arising from your use of this independent website.</p><div class='close'><a target='_blank' id='btn-leavingRedirect' href='' class='btn'><span>OK</span></a><a class='btn'><span>Cancel</span></a></div></div>");
						externalLinks.each(function(){
 							if (this.href.indexOf(svrDomain) == -1 ) {
								this.rel = "#ltBox-leavingSite";
							}
						}).bind('click', function(e) {
						  var h = this.href;
						  if ( h.indexOf('leaving_site') != -1 ) {
						    h = h.replace(/^java(.*)target=/gi, "").replace(/('\)+)(\D*)$/gi,"");
						    }
						  $j("#btn-leavingRedirect").attr("href", h);

						}).overlay({mask: {color:'#000',opacity: '0.6'}, closeOnClick: false});
			});
		}
	};