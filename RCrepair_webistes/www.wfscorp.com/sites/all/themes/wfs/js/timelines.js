(function ($) {

// Explain link in query log
Drupal.behaviors.timelines = {
  attach: function() {
     if ($("body").hasClass("page-admin")) {
      return;
     }

    var items_to_display = 6;
    if ($("body").hasClass("one-sidebar")) {
      items_to_display = 5;
    }
    if ($("body").hasClass("two-sidebars")) {
      items_to_display = 3;
    }

    var available_buttons = $("#slider .buttons div").length;

    // Display all the available itesm if there are less
    // than the predefined items to display.
    if (available_buttons < items_to_display) {
      items_to_display = available_buttons;
      $('#nextBtn').addClass("button-hidden");
      $('#prevBtn').addClass("button-hidden");
    }

    var slider = $(".buttons").bxSlider({
      controls: false,
      displaySlideQty: items_to_display,
      moveSlideQty: 1,
      infiniteLoop: false
    });

    $('#prevBtn').click(function(){
      slider.goToPreviousSlide();
      checkButtonsStatus();
      return false;
    });

    $('#nextBtn').click(function(){
      if (slider.getSlideCount() > slider.getCurrentSlide() + items_to_display) {
        slider.goToNextSlide();
        checkButtonsStatus();
      }

      return false;
    });


    $("#slider .buttons a").click(function(event) {
      if ($(this).parent().hasClass("selected")) {
        event.preventDefault();
        return;
      }

      var id = this.id;

      var tab = id.replace("button-", "ui-tabs-");

      $("#slider .buttons-bg").removeClass("selected");

      $(this).parent().addClass("selected");
      $("#timeline-tabs .ui-tabs-panel").fadeOut("fast").delay(400);
      $("#" + tab).fadeIn();
      event.preventDefault();
    });

    $("#timeline-tabs .ui-tabs-panel").hide();

    if (slider.getSlideCount() > items_to_display) {
      slider.goToSlide(slider.getSlideCount() - items_to_display);
    }


    checkButtonsStatus();
    /**
     * Determine if the prev and next buttons should be enabled.
     */
    function checkButtonsStatus() {
      if (slider.getSlideCount() < items_to_display) {
        $('#nextBtn').addClass("button-disabled");
        $('#prevBtn').addClass("button-disabled");
      }
      else {
        $('#prevBtn').removeClass("button-disabled");
        $('#nextBtn').removeClass("button-disabled");
      }

      if (slider.getSlideCount() <= slider.getCurrentSlide() + items_to_display) {
        $('#nextBtn').addClass("button-disabled");
      }

      if (slider.getCurrentSlide() <= 0) {
        $('#prevBtn').addClass("button-disabled");
      }
    }
  }
}
})(jQuery);
