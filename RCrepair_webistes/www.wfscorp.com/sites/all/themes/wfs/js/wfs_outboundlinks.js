(function ($) {
  Drupal.behaviors.wfs_outboundlinks = {
    attach: function() {
      $('a.outbound').click(function(){
        _gaq.push(['_trackEvent', $(this).attr('name'), 'Click', $(this).attr('title')]);
      });
    }
  }
}
)(jQuery);