
$(function() {
	/* create overlay background */
	//$('<li id="brandUpdate">Brand Update</li>').insertAfter('#utilityNavContainer ul li.navLast');
	$('<div class="overlayBG"></div>').insertAfter('div#topContentContainer');	
	$('<div class="overlayContent"><div class="closeBtn"></div><img src="../clientcss/images/brandUpdate.jpg"/*tpa=http://www.joyglobal.com/clientcss/images/brandUpdate.jpg*/ /></div>').insertAfter('div.overlayBG');	
	
	var $closeBtn = 'div.closeBtn';
	var $overlayBG = 'div.overlayBG';
	var $overlayContent = 'div.overlayContent';
	
	/* functions declared */
	
	var closeOverlay = function() {
		var $overlayBG = 'div.overlayBG';
		var $overlayContent = 'div.overlayContent';
		
		$($overlayBG).removeClass('visible');
		$($overlayContent).removeClass('visible');
	};
	
	var showOverlay = function() {
		var $overlayBG = 'div.overlayBG';
		var $overlayContent = 'div.overlayContent';
		
		$($overlayBG).addClass('visible');
		$($overlayContent).addClass('visible');
	};
	
	var sizeOverlay = function() {
		var $overlayBG = 'div.overlayBG';
		var $overlayContent = 'div.overlayContent';
		var $overlayContentImg = 'div.overlayContent img';
		var $overlayContentMargin = 30;//in pixels
		var $minWidth = 600;
		var $minHeight = 349;
		
		var $bwidth = $(window).width();
		var $bheight = $(window).height();
		
		//RESET IMAGE SIZE
		$($overlayContentImg).removeAttr('style');
		
		//GET ORIGINAL IMAGE SIZE
		var $contentWidth = $($overlayContentImg).width();
		var $contentHeight = $($overlayContentImg).height();
		
		var heightExceeded = $contentHeight > $bheight - ($overlayContentMargin * 2);
		var widthExceeded = $contentWidth > $bwidth - ($overlayContentMargin * 2);
		
		if (heightExceeded || widthExceeded) {
			if (heightExceeded && !widthExceeded) {
				sizeHeight();
			} else if (widthExceeded && !heightExceeded) {
				sizeWidth();
			} else if (heightExceeded && widthExceeded) {
				if ($bheight >= $bwidth - ($overlayContentMargin * 2)) {
					sizeHeight();
					sizeWidth();
				} else if ($bheight - ($overlayContentMargin * 2) < $bwidth) {
					sizeWidth();
					sizeHeight();
				}
				
			}
		}
		
		function sizeHeight() {
			var newHeight = $bheight - ($overlayContentMargin * 2);
				if (newHeight >= $minHeight) {
					var percentOfOriginal = newHeight / $contentHeight;
					$contentHeight = Math.floor(newHeight);
					var newWidth = $contentWidth * percentOfOriginal;
					$contentWidth = Math.floor(newWidth);
				} else {
					var newHeight = $minHeight;
					var newWidth = $minWidth;
					$contentHeight = Math.floor(newHeight);
					$contentWidth = Math.floor(newWidth);
				}
		}
		
		function sizeWidth() {
			var newWidth = $bwidth - ($overlayContentMargin * 2);
				if (newWidth >= $minWidth) {
					var percentOfOriginal = newWidth / $contentWidth;
					$contentWidth = Math.floor(newWidth);
					var newHeight = $contentHeight * percentOfOriginal;
					$contentHeight = Math.floor(newHeight);
				} else {
					var newHeight = $minHeight;
					var newWidth = $minWidth;
					$contentHeight = Math.floor(newHeight);
					$contentWidth = Math.floor(newWidth);
				}
		}
		
		
		
		
		
		$($overlayContent).css({'width':$contentWidth});
		$($overlayContentImg).width($contentWidth);
		$($overlayContent).css({'height':$contentHeight});
		$($overlayContentImg).height($contentHeight);
		
		
		var $contentXPos = ($bwidth/2) - ($contentWidth/2);
		var $contentYPos = ($bheight/2) - ($contentHeight/2);
		
		var $leftText = 'left:' + $contentXPos + 'px;';
		var $topText = 'top:' + $contentYPos + 'px;';
		
		/* background dimensions */
		$($overlayBG).width($bwidth);
		$($overlayBG).height($bheight);

		/* content centering/positioning */
		$($overlayContent).attr('style', $leftText + $topText);	
	};
	
	/* events for calculating overlay */
	$(window).resize(sizeOverlay);
	$('.brandUpdate').click(showOverlay);
	$('.brandUpdate').click(sizeOverlay);
	$($closeBtn).click(closeOverlay);
	$($overlayBG).click(closeOverlay);
	
	
	
	
});











