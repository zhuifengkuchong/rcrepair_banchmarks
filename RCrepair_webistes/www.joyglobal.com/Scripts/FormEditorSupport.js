/******************* Generic Form and Control Manipulation ***************/

function SFGetControlSet(ctlSet)
{
    return document.getElementsByName(ctlSet) ; 
}

function SFGetControl(ctlName)
{
    return document.getElementById(ctlName) ; 
}


function SFGetLabelValue( ctlName )
{
    ctl = document.getElementById(ctlName) ; 

    return ctl.innerHTML;
}

function SFGetRadioValue(ctlSet)
{
    var radios = SFGetControlSet(ctlSet) ;
    if (radios == null)    
        return null ;  

    if (radios.length == null || radios.length == 0)
       return (radios.checked?radios.value:null) ; 

    for (var ii=0 ; ii<radios.length ; ++ii)
        if (radios[ii].type != 'radio')
            return null ;  
        else if (radios[ii].checked)
            return radios[ii].value ; 

    return null ;
}

function SFGetRadioDefaultValue(ctlSet)
{
    var radios = SFGetControlSet(ctlSet) ;
    if (radios == null)
        return null ;  

    for (var ii=0 ; ii<radios.length ; ++ii)
        if (radios[ii].type != 'radio')
            return null ;  
        else if (radios[ii].defaultChecked)
            return radios[ii].value ; 
            
    return null ;
}

function SFGetSelectListLength(ctlName)
{
    return SFGetControl(ctlName).options.length ;
}

function SFGetSelectValue(ctlSet)
{
    var options = document.getElementById(ctlSet) ;
    if (options == null)
        return null ;  

    var retVal = "" ;
    for (var ii=0 ; ii<options.length ; ++ii)
        if (options[ii].selected)
            retVal += (retVal.length?", ":"") + options[ii].value ; 

    return retVal ;
}

function SFGetSelectDefaultValue(ctlSet)
{
    var options = document.getElementById(ctlSet) ;
    if (options == null)
        return null ;  

    for (var ii=0 ; ii<options.length ; ++ii)
        if (options[ii].defaultSelected)
            return options[ii].value ; 

    return null ;
}

function SFGetCheckboxState(ctlName)
{
    return SFGetControl(ctlName).checked ; 
}

function SFRemoveAllWhitespace(strValue)
{
    return strValue.replace(/[\s|\240]/gi, "") ; 
}

function SFEditFieldValue(ctlName)
{
    return SFRemoveAllWhitespace(SFGetControl(ctlName).value) ;
}

function SFEditFieldHasValue(ctlName)
{
    return (SFEditFieldValue(ctlName) != "") ;
}

function SFEditParseDate(ctlName)
{   
    if (!SFEditFieldIsDate(ctlName))
        return new Date() ;

    var ctl = SFGetControl(ctlName) ;    
    if (!ctl || !ctl.value)
        return new Date() ;
        
    var date = new Date() ;          
	var dateRE = /^ *([01]?\d)[\-|\/\.]([0123]?\d)[\-|\/\.]([12][8901]\d{2}) *$/ ;
	if (dateRE.test(ctl.value))
	{
		var match = ctl.value.match(dateRE) ; 
		if (match.length == 4)
		    return new Date(match[3], Number(match[1])-1, match[2]) ;   
	}
    return date ;             
}

function SFEditFieldIsDate(ctlName)
{   
    var ctl = SFGetControl(ctlName) ;
    if (!ctl || !ctl.value)
        return true ;
  
	var dateRE = /^ *([01]?\d)[\-|\/\.]([0123]?\d)[\-|\/\.]([12][8901]\d{2}) *$/ ;
	if (dateRE.test(ctl.value))
	{
		var match = ctl.value.match(dateRE) ; 
		if (match.length == 4)
		{
		    var testDate = new Date(match[3], Number(match[1])-1, match[2]) ;   

            if (testDate.getFullYear() == match[3] && 
                testDate.getMonth()    == Number(match[1])-1 &&
                testDate.getDate()     == match[2])
                return true ; 
		}
	}
    return false ;             
}

function SFEditFieldIsTime(ctlName)
{
    var ctl = SFGetControl(ctlName) ;
    if (!ctl || !ctl.value)
        return true ;
    
    var timeRE = /^ *([012]?\d)[: ]([0-5]?\d) ?([aApP]?\.?[mM]?\.?) *$/ ;
    if (timeRE.test(ctl.value))
    {
        var match = ctl.value.match(timeRE) ;
        if (match.length == 4)
        {
            var testTime = new Date(1900, 0, 01) ; 
            testTime.setHours(match[1]) ;
            testTime.setMinutes(match[2]) ;
            
            var hours = testTime.getHours() ;
            var minutes = testTime.getMinutes() ;
            
            if ((hours != Number(match[1])) ||
                (minutes != Number(match[2])) ||
                (testTime.getDate() != 1))
                return false ;
            
            var isAM = (match[3].indexOf('a') >= 0 || match[3].indexOf('A') >= 0) ;
            if (isAM && testTime.getHours() > 12)
                return false ;
                
            ctl.value = String(hours) + ((minutes<10)?":0":":") + String(minutes) + ((match[3])?(" " + match[3]):"") ;
            return true ;
        }
    }
    return false ;
}

function SFEditFieldMatchesRegEx(ctlName, regEx)
{
    return (SFGetControl(ctlName).value.match(regEx) != null); 
}

function SFEditFieldIsEmail(ctlName)
{
    return SFEditFieldMatchesRegEx(ctlName, "^ *[\\w._\\-']+@[\\w._\\-]*\\.[\\w._\\-]* *$") ;
}



/********************** Hide and Show Support ****************************/
function internal_ActionByType(type, action, container, suffix)
{
    if (!container)
        container = document ;

    var ctls = container.getElementsByTagName(type) ;
    
    var returnValue = true ; 
    for (var ii=0 ; ii<ctls.length ; ++ii)
        if (ctls[ii].name.indexOf("cmsForms_") == 0 || ctls[ii].name.indexOf("cmsFormS_") == 0)
            switch (action)
            {
                case "disable":
                    SFDisableControl(ctls[ii].name) ; 
                    break ;
                case "enable":
                    SFEnableControl(ctls[ii].name) ; 
                    break ;
                case "validate":
                    returnValue &= SFValidateControl(ctls[ii].name, suffix) ; 
                    break ;   
                case "clearCheck":
                    if (ctls[ii].type == "checkbox")
                        ctls[ii].checked = false ;                   
            }
            
    return returnValue ;            
}
function SFClearCheckBoxes()
{
    internal_ActionByType("input", "clearCheck", document, null) ; 
}

function SFDisableAll(container)
{
    if (!container)
        container = document ;
    internal_ActionByType("input", "disable", container, null) ; 
    internal_ActionByType("textarea", "disable", container, null) ; 
    internal_ActionByType("select", "disable", container, null) ; 
}

function SFEnableAll(container)
{
    if (!container)
        container = document ;

    internal_ActionByType("input", "enable", container, null) ; 
    internal_ActionByType("textarea", "enable", container, null) ; 
    internal_ActionByType("select", "enable", container, null) ; 
}

function SFGeneralValidate(docID, blockID, useCaptcha, specialValidationFunc)
{
    var containerControl = document.getElementById("DataDiv_" + blockID) ;
    if (!containerControl)
    {
        window.alert("Form Container is Missing") ;
        return false ;
    }

    tic_Utilities.ShowHideById("DataDiv_" + blockID, false) ;
    var allPassed = true ; 

    if (specialValidationFunc)
        allPassed &= specialValidationFunc(blockID, docID) ;

    allPassed &= internal_ActionByType("select", "validate", containerControl, blockID) ;
    allPassed &= internal_ActionByType("input", "validate", containerControl, blockID) ;
    allPassed &= internal_ActionByType("textarea", "validate", containerControl, blockID) ;

    if (useCaptcha == "1")
        allPassed &= ClientSideCaptchaValidate(blockID) ;
    
    if (!allPassed)
        tic_Utilities.ShowHideById("DataDiv_" + blockID, true) ;

    return allPassed ;
}

function SFExtraEventValidation(blockID, docID)
{
    var allPassed = true ;
    var ctlName = "cmsForms_EventStartTime_" + blockID ;
    if (document.getElementById(ctlName) && document.getElementById(ctlName).value)
        allPassed &= SFRespondToValidation(SFEditFieldIsTime(ctlName), "message_" + ctlName, "Start Time is invalid.  Accepted format is HH:MM with am, pm or in 24-hour time", ctlName, blockID) ;
    ctlName = "cmsForms_EventEndTime_" + blockID ;
    if (document.getElementById(ctlName) && document.getElementById(ctlName).value)
        allPassed &= SFRespondToValidation(SFEditFieldIsTime(ctlName), "message_" + ctlName, "End Time is invalid.  Accepted format is HH:MM with am, pm or in 24-hour time", ctlName, blockID) ;

    ctlName = "cmsForms_EventStartDate_" + blockID ;
    if (document.getElementById(ctlName) && document.getElementById(ctlName).value)
        allPassed &= SFRespondToValidation(SFEditFieldIsDate(ctlName), "message_" + ctlName, "Start Date is invalid.  Accepted format is MM-dd-yyyy", ctlName, blockID) ;

    ctlName = "cmsForms_EventEndDate_" + blockID ;
    if (document.getElementById(ctlName) && document.getElementById(ctlName).value)
        allPassed &= SFRespondToValidation(SFEditFieldIsDate(ctlName), "message_" + ctlName, "End Date is invalid.  Accepted format is MM-dd-yyyy", ctlName, blockID) ;

    ctlName = "cmsForms_EventContactEmail_" + blockID ;
    if (document.getElementById(ctlName) && document.getElementById(ctlName).value)
        allPassed &= SFRespondToValidation(SFEditFieldIsEmail(ctlName), "message_" + ctlName, "Invalid Contact Email detected", ctlName, blockID) ;

    ctlName = "cmsForms_EventDescription_" + blockID ;
    if (document.getElementById(ctlName) && document.getElementById(ctlName))
        allPassed &= SFRespondToValidation(internal_GetCommentValue(document.getElementById(ctlName)) == document.getElementById(ctlName).value, "message_" + ctlName, "Description field cannot contain HTML", ctlName, blockID) ;

    ctlName = "cmsForms_EventNotes_" + blockID ;
    if (document.getElementById(ctlName) && document.getElementById(ctlName))
        allPassed &= SFRespondToValidation(internal_GetCommentValue(document.getElementById(ctlName)) == document.getElementById(ctlName).value, "message_" + ctlName, "Notes field cannot contain HTML", ctlName, blockID) ;

    return allPassed ;
}

function SFFinalEventSubmit(docID, blockID, useCaptcha)
{
    var attachment = document.getElementById("cmsForms_EventAttachment_" + blockID) ;
    if (attachment && attachment.value)
        document.getElementById("cmsForms_EventHasAttachment_" + blockID).value = "1" ;

    var ctl = document.getElementById("cmsForms_EventName_" + blockID) ;
    ctl.form.submit() ;
}

function SFSubmitEvent(docID, blockID, doConfirm, useCaptcha)
{
    if (!SFGeneralValidate(docID, blockID, useCaptcha, SFExtraEventValidation))
        return ;

    if (doConfirm=="1")
        return SFConfirmForm(blockID) ;

    SFFinalEventSubmit(docID, blockID, useCaptcha) ;
}

function SFSubmitForm(docID, blockID, doConfirm, useCaptcha)
{
    if (!SFGeneralValidate(docID, blockID, useCaptcha, window["FormSpecificValidation"]))
        return ;

    if (doConfirm=="1")
        return SFConfirmForm(blockID) ;

    SFFinalFormSubmit(docID, blockID, useCaptcha) ;
}

function SFReturnToEdit(blockID)
{
    SFEnableAll(document.getElementById("DataDiv_" + blockID)) ;
    tic_Utilities.ShowHideById("VerifyMessage_" + blockID, false) ;
    tic_Utilities.ShowHideById("SubmitButtons_" + blockID, true) ;
    tic_Utilities.ShowHideById("ConfirmButtons_" + blockID, false) ;
    tic_Utilities.ShowHideById("DataDiv_" + blockID, true) ;    
}

function SFConfirmForm(blockID)
{
    SFDisableAll(document.getElementById("DataDiv_" + blockID)) ;
    tic_Utilities.ShowHideById("VerifyMessage_" + blockID, true) ;
    tic_Utilities.ShowHideById("SubmitButtons_" + blockID, false) ;
    tic_Utilities.ShowHideById("ConfirmButtons_" + blockID, true) ;
    tic_Utilities.ShowHideById("cmsForms_DataNotProvided_" + blockID, false) ;
    tic_Utilities.ShowHideById("DataDiv_" + blockID, true) ;    
}

function SFFinalFormSubmit(docID, blockID, useCaptcha)
{
    SFReturnToEdit(blockID) ;
    tic_Utilities.ShowHideById("DataDiv_" + blockID, false) ;     

    var containerControl = document.getElementById("DataDiv_" + blockID) ;
        
    var packagedData = tic_Utilities.MakeXmlStartTag("Data", false) ;    
    packagedData += internal_PackageByType("select", containerControl) ;
    packagedData += internal_PackageByType("input", containerControl) ;
    packagedData += internal_PackageByType("textarea", containerControl) ;

    packagedData += tic_Utilities.MakeXmlStartTag("Captcha", false) ;
    packagedData += ExtractCaptchaInfo(containerControl) ;
    packagedData += tic_Utilities.MakeXmlEndTag("Captcha", false) ;
    
    packagedData += tic_Utilities.MakeXmlEndTag("Data", false) ;

    var ajaxArgs = new Array() ;
    ajaxArgs[ajaxArgs.length] = docID ;
    ajaxArgs[ajaxArgs.length] = blockID ;
    ajaxArgs[ajaxArgs.length] = packagedData ;
    
    TitanDisplayServiceWrapper.MakeWebServiceCall("Forms", NorthwoodsSoftwareDevelopment.Cms.WebServices.FormBlockAjax.ProcessComment, ajaxArgs, SFFormSubmitComplete, [blockID], true) ;            
}

function SFFormSubmitComplete(blockID, results, context, methodName)
{
    if (window["ProcessCaptchaResults"])
        ProcessCaptchaResults(results) ;

    if (!results.status)
    {
        tic_Utilities.ShowHideById("DataDiv_" + blockID, true) ;
        SFRespondToValidation(false, "formsubmit", results.message, "foo", blockID) ;            
    }
    else
        tic_Utilities.ShowHideById("ThankYouDiv_" + blockID, true) ;        
}

function SFValidateAll(ctl)
{
    var allPassed = true ; 
    
    if (window.FormSpecificValidation)
        allPassed &= FormSpecificValidation() ;
    
    allPassed &= internal_ActionByType("select", "validate", document, null) ;
    allPassed &= internal_ActionByType("input", "validate", document, null) ;
    allPassed &= internal_ActionByType("textarea", "validate", document, null) ;

    if (allPassed)
        ctl.form.submit() ;
}

function SFValidateControl(ctlName, suffix)
{
    var messageName = "alert_" + ctlName ; 
    var controlIsValid = true ; 
    
    var ctl = document.getElementById(ctlName) ; 
    var displayName = ctlName.substring(ctlName.indexOf('_')+1); 
    var bIsRequired = false ; 
    if (ctl)
    {
        if (ctl.attributes["errorMessage"] && ctl.attributes["errorMessage"].value != "")
            displayName = ctl.attributes["errorMessage"].value ; 
        else if (ctl.attributes["errormessage"] && ctl.attributes["errormessage"].value != "")
            displayName = ctl.attributes["errormessage"].value ; 
        bIsRequired = ((ctl.attributes["isRequired"]  && (ctl.attributes["isRequired"].value=='true' || ctl.attributes["isRequired"].value=="isRequired")) || 
                       (ctl.attributes["isrequired"]  && (ctl.attributes["isrequired"].value=='true' || ctl.attributes["isrequired"].value=="isrequired")) );             
    }    

    if (ctl && ctl.tagName == "SELECT")
    {
        if (bIsRequired)
        {
            var selectedValue = SFGetSelectValue(ctlName) ; 
            if (!selectedValue || selectedValue == "")
                controlIsValid = false ;             
        }

        SFRespondToValidation(controlIsValid, messageName, displayName, ctlName, suffix) ; 
    }
    else if (ctl && ctl.tagName == 'INPUT' && ctl.type=='checkbox')
    {
        if (bIsRequired)
        {
            controlIsValid = ctl.checked ;
            SFRespondToValidation(controlIsValid, messageName, displayName, ctlName, suffix) ; 
        }
    }
    else if (!ctl || (ctl.tagName == 'INPUT' && ctl.type=='radio'))
    {
        if (!ctl)
        {
            var ctlGroup = document.getElementsByName(ctlName) ; 
            if (ctlGroup && ctlGroup.length > 0)
            {
                bIsRequired =  ((ctlGroup[0].attributes["isRequired"]  && (ctlGroup[0].attributes["isRequired"].value=='true' || ctlGroup[0].attributes["isRequired"].value=="isRequired")) ||
                                (ctlGroup[0].attributes["isrequired"]  && (ctlGroup[0].attributes["isrequired"].value=='true' || ctlGroup[0].attributes["isrequired"].value=="isrequired"))) ;

                var ctl = ctlGroup[0] ;
                if (ctl.attributes["errorMessage"] && ctl.attributes["errorMessage"].value != "")
                    displayName = ctl.attributes["errorMessage"].value ; 
                else if (ctl.attributes["errormessage"] && ctl.attributes["errormessage"].value != "")
                    displayName = ctl.attributes["errormessage"].value ; 
            }                                
        }
    
        if (bIsRequired)
        {
            var selectedValue = SFGetRadioValue(ctlName) ; 
            if (!selectedValue || selectedValue == "")
                controlIsValid = false ;             
        }

        SFRespondToValidation(controlIsValid, messageName, displayName, ctlName, suffix) ; 
    }
    else // input and textareas
    {
        bNeedsValidation = (ctl.attributes["validationType"] && ctl.attributes["validationType"].value != 'None') ; 
        if (!bNeedsValidation)
            bNeedsValidation = (ctl.attributes["validationtype"] && ctl.attributes["validationtype"].value != 'None') ; 
    
        if (bIsRequired && !SFEditFieldHasValue(ctlName))
            controlIsValid = false ; 
        else if (bNeedsValidation && !SFEditFieldMatchesRegEx(ctlName, (ctl.getAttribute("regExp") || ctl.getAttribute("regexp"))))
            controlIsValid = false ; 

        SFRespondToValidation(controlIsValid, messageName, displayName, ctlName, suffix) ; 
    }
    
    return controlIsValid ; 
}

function internal_MaskEdit(ctl)
{
    ctl.readOnly = true ; 
    tic_Utilities.AddStyle(ctl, "hideBorders") ;
}

function internal_UnMaskEdit(ctl)
{
    ctl.readOnly = false ; 
    tic_Utilities.RemoveStyle(ctl, "hideBorders") ;
}

function SFEnableControl(ctlName)
{
    var ctl = document.getElementById(ctlName) ; 
    if (ctl && ctl.tagName == "SELECT")
    {
        ctl.style.display = "block" ;
        
        if (document.getElementById(ctl.id + "_mask"))
            document.getElementById(ctl.id + "_mask").parentNode.removeChild(document.getElementById(ctl.id + "_mask")) ;
    }
    else if (ctl && ctl.tagName == 'INPUT' && ctl.type=='checkbox')
        ctl.disabled = false ; 
    else if (!ctl || (ctl.tagName == 'INPUT' && ctl.type=='radio'))
    {
        var ctlGroup = document.getElementsByName(ctlName) ; 
        for (var ii=0 ; ii<ctlGroup.length ; ++ii)
            ctlGroup[ii].disabled = false ;
    }
    else // input and textareas
        internal_UnMaskEdit(ctl); 
}

function SFDisableControl(ctlName)
{
    var ctl = document.getElementById(ctlName) ; 
    if (ctl && ctl.tagName == "SELECT")
    {
        var parentNode = ctl.parentNode ;
        if (!parentNode)
            return window.alert ("No parent element found for " + ctlName); 
            
        var inputControl = document.createElement("input") ; 
        if (!inputControl)
            return window.alert ("Unable to create element for " + ctlName) ; 
        inputControl.id = ctl.id + "_mask" ;            

        if (!ctl.multiple && ctl.selectedIndex >= 0)
            inputControl.value     = ctl.options[ctl.selectedIndex].text ;   
        else
        {
            var str = "" ;
            for (var ii=0 ; ii<ctl.length ; ++ii)
                if (ctl[ii].selected)
                    str += ((str.length>0)?", ":"") + ctl[ii].text ;
            inputControl.value = str ;
        }            
            
        inputControl.className = ctl.className ;              
        parentNode.insertBefore(inputControl, ctl) ;
        internal_MaskEdit(inputControl); 
        
        ctl.style.display = "none" ;         
    }
    else if (ctl && ctl.tagName == 'INPUT' && ctl.type=='checkbox')
        ctl.disabled = true ; 
    else if (!ctl || (ctl.tagName == 'INPUT' && ctl.type=='radio'))
    {
        var ctlGroup = document.getElementsByName(ctlName) ; 
        for (var ii=0 ; ii<ctlGroup.length ; ++ii)
            ctlGroup[ii].disabled = true ;
    }
    else // input and textareas
        internal_MaskEdit(ctl); 
}

function SFControlIsEnabled(ctlName)
{
    return document.getElementById(ctlName)!=null && !document.getElementById(ctlName).disabled ;
}

function SFControlExists(ctlName)
{
    return SFGetControl(ctlName)!=null ;
}

/************************* Change Detection ******************************/
function SFValueHasChanged(ctlName)
{
    var ctl = SFGetControl(ctlName) ; 
    if (ctl == null)
        return ; 

    var value ;         
    if (!ctl.tagName)
        value = SFGetRadioValue(ctlName) ; 
    else if (ctl.tagName == "SELECT")
        value = SFGetSelectValue(ctlName) ;    
    else if (ctl.tagName == 'INPUT' && ctl.type=='checkbox')
        value = ctl.checked?ctl.value:"" ; 
    else
        value = ((!ctl.value)?"":ctl.value) ; 

    var defaultValue ;         
    if (!ctl.tagName)
        defaultValue = SFGetRadioDefaultValue(ctlName) ; 
    else if (ctl.tagName == "SELECT")
        defaultValue = SFGetSelectDefaultValue(ctlName) ;    
    else if (ctl.tagName == 'INPUT' && ctl.type=='checkbox')
        defaultValue = ctl.defaultChecked?ctl.value:"" ; 
    else
        defaultValue = ((!ctl.defaultValue)?"":ctl.defaultValue) ; 
        
    defaultValue = (defaultValue?defaultValue:"") ; 
    value = (value?value:"") ;        

    return (defaultValue != value) ; 
}

/****************** OnLoad Control Value Setting *************************/
function SFSetRadioValue(ctlName, ctlValue)
{
    var ctl = SFGetControl(ctlName) ; 
    if (ctl == null)
        return ; 
        
    if (ctl.length && ctl.length > 0)
        for (var ii=0 ; ii<ctl.length ; ++ii)
            ctl.checked = (ctl.value == ctlValue) ; 
    else 
        ctl.checked = (ctl.value == ctlValue) ; 
}

function SFClearRadioButton(ctlName, ctlValue)
{
    var ctl = SFGetControl(ctlName) ; 
    if (ctl == null)
        return ;
        
    for (var ii=0 ; clt.length && ii<ctl.length ; ++ii)        
        if (ctl[ii].value == ctlValue)
            ctl[ii].selected = ctl[ii].checked = false ; 
}
function SFSetControlValue(ctlName, ctlValue, bFireChangeEvent)
{
    var ctl = document.getElementById(ctlName) ; 
    if (ctl == null)
    {
        ctl = document.getElementsByName(ctlName) ; 
        if (ctl == null || ctl.length == 0)
            return ;
    }
    
    var arrayOfValues ; 
    if (ctl.tagName == "SELECT" && ctl.multiple)
        arrayOfValues = ctlValue.split(',') ; 
    else
        arrayOfValues = [ctlValue] ;
        
    if (ctl.length && ctl.length > 0)
    {   
	for (var ii=0 ; ii<ctl.length ; ++ii)
	{
            ctl[ii].selected = false ; 
            ctl[ii].checked = false ; 
            ctl[ii].defaultChecked = false ;
            ctl[ii].defaultSelected = false ;
	}
        for (var jj=0 ; jj<arrayOfValues.length ; ++ jj)
        {
            for (var ii=0 ; ii<ctl.length ; ++ii)        
            {
               if (ctl[ii].value.replace(/ /g, '') == arrayOfValues[jj].replace(/ /g, ''))
                {
                    ctl[ii].selected = true ; 
                    ctl[ii].checked = true ; 
                    ctl[ii].defaultChecked = true ;
                    ctl[ii].defaultSelected = true ;
                    if (bFireChangeEvent && ctl[ii].onclick)
                        ctl[ii].onclick() ;
                }                                       
            }
        }
        
        // Handle dropdown's where the method is on the select            
        if (bFireChangeEvent && ctl.onchange)
            ctl.onchange() ;        
    }
    else if (ctl.tagName == 'INPUT' && ctl.type == 'radio')
    {
        var ctlGroup = document.getElementsByName(ctlName) ; 
        for (var ii=0 ; ii<ctlGroup.length ; ++ii)
            if (ctlGroup[ii].value == ctlValue)
            {
                ctlGroup[ii].checked = true ; 
                if (bFireChangeEvent && ctl.onclick)
                    ctl.onclick() ;        
            }
    }
    else if (ctl.tagName == 'INPUT' && ctl.type == 'checkbox')
    {
        ctl.defaultChecked = ctl.checked = (ctlValue != "" && ctlValue != 0 && ctlValue != "No"  && ctlValue != "off" && ctlValue != false) ; 
        if (bFireChangeEvent && ctl.onclick)
            ctl.onclick() ;
    }
    else
    {        
        ctl.value = ctlValue ; 
        ctl.defaultValue = ctlValue ; 
        if (bFireChangeEvent && ctl.onchange)       
            ctl.onchange() ; 
    }
}

/******************* Message Area Manipulation ***************************/
function SFExtractMessageControl(suffix)
{
    var ctlName = "cmsForms_DataNotProvided" ;
    if (suffix)
        ctlName += "_" + suffix ;
    return document.getElementById(ctlName) ; 
}

function SFFindMessage(msgCtl, msgName)
{
    var message = document.getElementById(msgName) ;
    if (message)
        msgCtl.style.display = "block" ;

    return message ;
}

function SFHideMessageArea(suffix)
{
    var msgCtl = SFExtractMessageControl(suffix) ; 
    msgCtl.style.display = "none" ; 
    
}

function SFAddMessage(msgName, msgText, suffix)
{
    var msgCtl = SFExtractMessageControl(suffix) ; 
    if (SFFindMessage(msgCtl, msgName))
        return ; 

    var newMessage = document.createElement("DIV") ; 
    newMessage.className = 'SFMessage' ; 
    newMessage.id = msgName ; 
    newMessage.innerHTML = "&nbsp;&nbsp;&nbsp;" + msgText;
    
    msgCtl.appendChild(newMessage);

    if (msgCtl.getAttribute("useClass") == "1")
        tic_Utilities.AddStyle(msgCtl, "error") ;
    else        
        msgCtl.style.display = "block" ; 
    return true;
}

function SFRemoveMessage(msgName, suffix)
{
    var msgCtl = SFExtractMessageControl(suffix) ; 
    var ctlToRemove = SFFindMessage(msgCtl, msgName) ;
    if (ctlToRemove == null || msgCtl == null)
        return ; 
    msgCtl.removeChild(ctlToRemove) ;
    
    if (msgCtl.getElementsByTagName("DIV").length > 0)
        return ; 

    if (msgCtl.getAttribute("useClass") == "1")
        tic_Utilities.RemoveStyle(msgCtl, "error") ;
    else        
        msgCtl.style.display = "none" ;        
}

function SFDisplayMessage(msgName, message, suffix) 
{
    if (SFFindMessage(SFExtractMessageControl(suffix), msgName))
        return ; 

    SFAddMessage(msgName, message, suffix) ;
}

function SFRespondToValidation(testHasPassed, msgName, message, ctlName, suffix)
{
    var containingDiv = null ;
    var errorCtl = document.getElementById(ctlName + "_Error") ;
    if (errorCtl) 
    {
        containingDiv = errorCtl ; 
        while (containingDiv && containingDiv.tagName != "DIV")
            containingDiv = containingDiv.parentNode ;
            
        if (containingDiv && containingDiv.tagName != "DIV")
            containingDiv = null ;            
    }

    if (testHasPassed)
    {
        SFRemoveMessage(msgName, suffix) ; 
        tic_Utilities.RemoveStyle(containingDiv, "messageOn") ;
        return true ; 
    }
    else
    {
        SFDisplayMessage(msgName, message, suffix) ; 
        tic_Utilities.AddStyle(containingDiv, "messageOn") ;
        return false ; 
    }
}

function IsPhotoDiv(div)
{
    if (!div.id)
        return false ;
        
    if (div.id.indexOf("/") != 0)
        return false ;
        
    return true ;                
}

function ChangeImageSource(ctlName, newSource, smallWidth, largeWidth, blockIndex)
{
    var targetID = newSource.replace("_T" + smallWidth, "_T" + largeWidth) ;
    var ctl = document.getElementById(ctlName) ; 
    var divChildren = ctl.getElementsByTagName("DIV") ;
    for (var ii=0 ; ii<divChildren.length ; ++ii)
    {
        if (!IsPhotoDiv(divChildren[ii]))
            continue ; 

        if (!divChildren[ii].style.display || divChildren[ii].style.display == "block")
            divChildren[ii].style.display = "none" ;
        
        if (divChildren[ii].id == targetID)
        {
            divChildren[ii].style.display = "block" ;
            ResetImageTag(divChildren[ii]) ;
            if (document.getElementById("rightArrow" + String(blockIndex)))
                document.getElementById("rightArrow" + String(blockIndex)).style.visibility = (((ii+1) < divChildren.length)?"visible":"hidden") ; 
            if (document.getElementById("leftArrow" + String(blockIndex)))
                document.getElementById("leftArrow" + String(blockIndex)).style.visibility = (((ii) > 0)?"visible":"hidden") ;            
        }
    }
}

function OpenFullImage(mediumImageLink)
{
    var hwnd = window.open(mediumImageLink.replace(/_T\d\d\d?/gi, "")) ;
    hwnd.focus() ;
}

function ResetImageTag(outerDiv)
{
    var images = outerDiv.getElementsByTagName("IMG") ;
    if (!images || !images.length)
        return ;
    
    var img = images[0] ;
    img.src = img.getAttribute("realSrc") ;
}

function PhotoBlockMove(direction, ctlName, blockIndex)
{
    var ctl = document.getElementById(ctlName) ;
    var divChildren = ctl.getElementsByTagName("DIV") ;
    for (var ii=0 ; ii<divChildren.length ; ++ii)
    {
        if (!IsPhotoDiv(divChildren[ii]))
            continue ;
            
        if (divChildren[ii].style.display && divChildren[ii].style.display == "none")
            continue ;
        if (direction == "right")
        {
            if ((ii+1) < divChildren.length)
            {
                divChildren[ii].style.display = "none" ;
                divChildren[ii+1].style.display = "block" ;
                ResetImageTag(divChildren[ii+1]) ; 
            }            
 
            document.getElementById("rightArrow" + String(blockIndex)).style.visibility = (((ii+2) < divChildren.length)?"visible":"hidden") ; 
            document.getElementById("leftArrow" + String(blockIndex)).style.visibility = "visible" ;
        }            
        else if (direction == "left")
        {
            if ((ii-1) >= 0)
            {
                divChildren[ii].style.display = "none" ;
                divChildren[ii-1].style.display = "block";
                ResetImageTag(divChildren[ii-1]) ;
            }
                
            document.getElementById("leftArrow" + String(blockIndex)).style.visibility = (((ii-1) > 0)?"visible":"hidden") ;            
            document.getElementById("rightArrow" + String(blockIndex)).style.visibility = "visible" ;
        }
        
        break ;            
    }    
}

/* ------------------------  Commenting & Rating ---------------------- */
function internal_ControlHasBeenProcessed (processed, newCtlName)
{
    for (var ii=0 ; ii<processed.length ; ++ii)
        if (processed[ii] == newCtlName)
            return true ;
            
    return false ;            
}

function internal_GetCommentValue(ctl)
{
    switch (ctl.tagName)
    {
        case "INPUT":
            if (ctl.type == "radio")
                return SFGetRadioValue(ctl.name);
            else if (ctl.type == "checkbox") 
                return SFGetCheckboxState(ctl.name)?"1":"0" ;
            // !!!ELSE we're an input FALL THROUGH!!!!!
        case "TEXTAREA":
            return ctl.value.replace(/<|>|&lt;|&gt;|&#60;|&#62;|&#x3c;|&#x3e;|%3c|%3e/gi, "") ; 
        case "SELECT":
            return SFGetSelectValue(ctl.id) ;
    }
}

function internal_PackageByType(type, container)
{
    if (!container)
        container = document ;

    var processedControls = [] ;
    
    var returnValue = "" ; 
    var ctls = container.getElementsByTagName(type) ;
    for (var ii=0 ; ii<ctls.length ; ++ii)
        if (ctls[ii].name.indexOf("cmsForms_") == 0 || ctls[ii].name.indexOf("cmsFormS_") == 0)
        {
           if (internal_ControlHasBeenProcessed (processedControls, ctls[ii].name))
            continue ;
            
            processedControls[processedControls.length] = ctls[ii].name ;
 
            returnValue += tic_Utilities.PackageXml(ctls[ii].name, internal_GetCommentValue(ctls[ii]), true) ;
        }
            
    return returnValue ;            
}

function CRValidateAndSubmit(ctl)
{
    var containerControl = document.getElementById("titanComments_CommentForm") ;
    if (!containerControl)
        return window.alert("Commenting form container is missing") ;

    tic_Utilities.ShowHideById("titanComments_CommentForm", false) ;
    var allPassed = true ; 
    
    if (window["CommentRating_UserSuppliedValidation"])
        allPassed &= CommentRating_UserSuppliedValidation() ;
    
    allPassed &= internal_ActionByType("select", "validate", containerControl, null) ;
    allPassed &= internal_ActionByType("input", "validate", containerControl, null) ;
    allPassed &= internal_ActionByType("textarea", "validate", containerControl, null) ;

    if (window["ClientSideCaptchaValidate"])
        allPassed &= ClientSideCaptchaValidate(null) ;

    if (!allPassed)
        return tic_Utilities.ShowHideById("titanComments_CommentForm", true) ;

    var packagedData = tic_Utilities.MakeXmlStartTag("Data", false) ;
    
    packagedData += internal_PackageByType("select", containerControl) ;
    packagedData += internal_PackageByType("input", containerControl) ;
    packagedData += internal_PackageByType("textarea", containerControl) ;

    packagedData += tic_Utilities.MakeXmlStartTag("Captcha", false) ;
    packagedData += ExtractCaptchaInfo(containerControl) ;
    packagedData += tic_Utilities.MakeXmlEndTag("Captcha", false) ;
    
    packagedData += tic_Utilities.MakeXmlEndTag("Data", false) ;

    var ajaxArgs = new Array() ;
    ajaxArgs[ajaxArgs.length] = document.getElementById("cmsForms_DocID").value ;
    ajaxArgs[ajaxArgs.length] = packagedData ;

    SFRespondToValidation(true, "captcha", "", "cmsForms_TitanRatingReCaptchaZone", null) ;    
    TitanDisplayServiceWrapper.MakeWebServiceCall("Commenting", NorthwoodsSoftwareDevelopment.Cms.WebServices.CommentingAjax.ProcessComment, ajaxArgs, CRSaveComplete, [], true) ;
}

function CRSaveComplete(/*data,*/ results, context, methodName)
{
    if (!results.status)
    {
        tic_Utilities.ShowHideById("titanComments_CommentForm", true) ;
        SFRespondToValidation(false, "formsubmit", results.message, "cmsForms_TitanRatingReCaptchaZone", null) ;        
    }
    else if (document.getElementById("titanComments_Confirmation"))
        tic_Utilities.ShowHideById("titanComments_Confirmation", true) ;
    else
        document.location.href = document.location.href ;

    if (window["ProcessCaptchaResults"])
        ProcessCaptchaResults(results) ;
}

function ClientSideCaptchaValidate(suffix)
{
    if (!document.getElementById("cmsForms_CommentingCaptchaEnabled") ||
         document.getElementById("cmsForms_CommentingCaptchaEnabled").value != "1")
         return true ;

    if (document.getElementById("cmsForms_TitanRatingReCaptchaZone"))
        return SFRespondToValidation(Recaptcha.get_response(), "titan_captcha", "You must respond to the Recaptcha request" , "cmsForms_TitanCaptcha", suffix) ; 

    return true ;        
}

function ProcessCaptchaResults(results)
{
    try
    {
        document.getElementById("recaptcha_response_field").focus() ;
        if (window["Recaptcha"] && Recaptcha.widget)
            Recaptcha.reload() ;    
    }
    catch (e)
    {
        // if it fails, it fails.  We tried
    }            
}

function ExtractCaptchaInfo(containerControl)
{
    var retVal = tic_Utilities.PackageXml("RecaptchaChallenge", Recaptcha.get_challenge(), true) ;
    retVal += tic_Utilities.PackageXml("RecaptchaResponse", Recaptcha.get_response(), true) ;
    
    return retVal ;
}

function CRCommentLimit(ctl, numChars)
{
    var baseID = ctl.id ;
    
    var limitSpanCtl = document.getElementById(baseID + "_Count") ;
    
    if (ctl.value.length > numChars)
        ctl.value = ctl.value.substring(0, numChars) ;
    
    if (limitSpanCtl)
        limitSpanCtl.innerHTML = String(numChars - ctl.value.length) ;
}