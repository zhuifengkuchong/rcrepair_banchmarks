// JavaScript Document
function addLoadEvent(func) {
	var oldonload = window.onload;
	if (typeof window.onload != 'function') {
		window.onload = func;
	} else {
		window.onload = function() {
			oldonload();
			func();
		}
	}
}

startList = function() {
	if (!document.all&&document.getElementById) return false;
	if (!document.getElementById("navigation")) return false;
	
	//if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){ test for MSIE x.x;
 		//var ieversion=new Number(RegExp.$1)  capture x.x portion and store as a number

	 //if (ieversion<=6) {
		var navRoot = document.getElementById("navigation");
		var subNav = navRoot.getElementsByTagName("div");
			for (i=0; i<subNav.length; i++) {
				var node = subNav[i];
				if (node.className =="topNav") {
					node.onmouseover=function() {
						this.className+=" over";
					}
					node.onmouseout=function() {
						this.className=this.className.replace(" over", "");
					}
				}
			}
//		}
//	}
}

addLoadEvent(startList);

// siteMap toggle
function toggleMap() {
	var map = document.getElementById("gridContainer");
	if (!map) return false;
	var toggles = map.getElementsByTagName("li");
	for (var i=0; i<toggles.length; i++) {
		var targetLink = toggles[i];
		if (targetLink.className == "menuTitle collapsed") {
			//alert("match");
			targetLink.submenu = targetLink.getElementsByTagName("ul");
			targetLink.onclick = function() {
				if (this.submenu[0].className == "show") {
					this.submenu[0].className = "hide"
					//this.submenu[0].parentNode.className = "menuTitle collapsed";
					return false;
				} else {
					this.submenu[0].className = "show";	
					//this.submenu[0].parentNode.className = "menuTitle displayed";
					return false;
				}
			}
		}
	}
}
addLoadEvent(toggleMap);

// Enable home page rollovers for IE6
homeRoll = function() {	 
	if (!document.all&&document.getElementById) return false;
	if (!document.getElementById("overviewGrid")) return false;
		var grid = document.getElementById("overviewGrid");
		var boxes = grid.getElementsByTagName("td");
		for (i=0; i<boxes.length; i++) {
			node = boxes[i];			
				node.onmouseover=function() {
					this.className+=" over";
				}
				node.onmouseout=function() {
					this.className=this.className.replace(" over", "");
				}			
		}

}

addLoadEvent(homeRoll);

// Dynamically add alternate table-row background colors. Targeting based on className attribute "standard".
function stripeTables() {
  if (!document.getElementsByTagName) return false;
   		var tables = document.getElementsByTagName("table");  
   			for (var i=0; i<tables.length; i++) { 
   			if (tables[i].className == "standard") {
				var target=tables[i];
    			var odd = false;
   				var rows = target.getElementsByTagName("tr");
    				for (var j=1; j<rows.length; j++) {
      					if (odd == true) {
        				addClass(rows[j],"odd");
        				odd = false;
      			} else {
        			odd = true;
      			}
    		}
  		}
	}
}
function addClass(element,value) {
  if (!element.className) {
    element.className = value;
  } else {
    newClassName = element.className;
    newClassName+= " ";
    newClassName+= value;
    element.className = newClassName;
  }
}
addLoadEvent(stripeTables);


// Dynamically highlight table-rows on rollover. Targeting based on className ============================
function highlightRows() {
  if(!document.getElementsByTagName) return false;
	var tables = document.getElementsByTagName("table"); 
     for (var i=0; i<tables.length; i++) { 
   		if (tables[i].className == "standard") {
		var target = tables[i];	
  		var rows = target.getElementsByTagName("tr");
  			for (var j=0; j<rows.length; j++) {
    			rows[j].oldClassName = rows[j].className
    			rows[j].onmouseover = function() {
      				addClass(this,"highlight");
   				}
    			rows[j].onmouseout = function() {
      				this.className = this.oldClassName
    			}
  			}
		}
	}
}
//addLoadEvent(highlightRows);


// 'You are here' effect on sideNav-subNav items ==================================================
function getUrlString(targetUrl) {
	var beginIndex = targetUrl.lastIndexOf("/")-10;
//	alert(beginIndex);
	var endIndex = targetUrl.lastIndexOf(".");
	targetUrl = targetUrl.substring(beginIndex,endIndex);
	return targetUrl;
}
function subNavHighlight() {
	if (document.getElementById("sideNav")) {
	var subNav = document.getElementById("sideNav").getElementsByTagName("ul");
	var currentpageurl = getUrlString(window.location.href);
	  for (var i=0; i<subNav.length; i++ ) {
		var link = subNav[i].getElementsByTagName("a");

			for(var j=0; j<link.length; j++ ) {			
				var url = getUrlString(link[j].getAttribute("href"));
					if (currentpageurl == url) {
						link[j].className = "highLightLink";
					}
			   }
	 	  }
  	 }
}

//addLoadEvent(subNavHighlight);



function setSizeBtns() {
if (!document.getElementById) return false;
	if (document.getElementById("textSizes")) {
		document.getElementById("textsize_down").onclick = function() {
			changeTextSize(-1);
			return false;
		}
			document.getElementById("textsize_up").onclick = function() {
			changeTextSize(1);
			return false;
		}
	}
}

addLoadEvent(setSizeBtns);


var currTextSize = 0;
function changeTextSize (updown)
{
	currTextSize = Math.max(Math.min((currTextSize + updown),1),-1);
//	alert(currTextSize);
    if (document.getElementById) {
		var boxes = document.getElementById("wrapper").getElementsByTagName("div");
	  	for (var i=0; i<boxes.length; i++ ) {	
	     	//if (boxes[i].className == "box") {
			if (boxes[i].className.indexOf("box") != -1) {
				//styleObj = boxes[i].style;
				styleObj = boxes[i];
        		switch (currTextSize) {
            		case 1: 
                		//styleObj.fontSize =  "http://www.aecom.com/internet1/scripts/1.0em"; break;
						addTextSize(styleObj,"largeText"); break;
            		case 0:
            			//styleObj.fontSize =  "http://www.aecom.com/internet1/scripts/0.75em"; break;
						addTextSize(styleObj,"mediumText"); break;
            		case -1: 
                		//styleObj.fontSize =  "http://www.aecom.com/internet1/scripts/0.68em"; break;
						addTextSize(styleObj,"smallText"); break;
        		}
    		}
		}
	}
}
function addTextSize(element,value) {
    newClassName = "box";
    newClassName+= " ";
    newClassName+= value;
    element.className = newClassName;
//	alert(newClassName);
}

// menu toggle FAQS ===========================================================================
function toggleMenuFAQ() {
	var menu = document.getElementById("localMenu");
	if (!menu) return false;
	var toggles = menu.getElementsByTagName("li");
	for (var i=0; i<toggles.length; i++) {
		var targetLink = toggles[i];
		if (targetLink.className == "questionTitle collapsed") {
			targetLink.submenu = targetLink.getElementsByTagName("ul");
			targetLink.onclick = function() {
				if (this.submenu[0].className == "show") {
					this.submenu[0].className = "hide";		
					this.submenu[0].parentNode.className = "questionTitle collapsed";
					return false;
				} else {
					toggleFAQ(this.submenu[0]);
					return false;
				}
			}
		}
	}
}
addLoadEvent(toggleMenuFAQ);

function toggleFAQ(showHit) {
	var menu = document.getElementById("localMenu");
	var toggles = menu.getElementsByTagName("ul");
		for (var i=0; i<toggles.length; i++) {
		var mytargetLink = toggles[i];
		if (mytargetLink.className == "show") {	
			mytargetLink.className = "hide";
			mytargetLink.parentNode.className = "questionTitle collapsed";
		}
		showHit.className = "show";
		showHit.parentNode.className = "questionTitle displayed";
	}
}

// menu toggle SIDENAV ===========================================================================
function toggleMenu() {
	var menu = document.getElementById("sidenavContainer");
	if (!menu) return false;
	var toggles = menu.getElementsByTagName("li");
	for (var i=0; i<toggles.length; i++) {
		var targetLink = toggles[i];
		if (targetLink.className == "menuTitle collapsed") {
			targetLink.submenu = targetLink.getElementsByTagName("ul");
			targetLink.onclick = function() {
				if (this.submenu[0].className == "show") {
					this.submenu[0].className = "hide";		
					this.submenu[0].parentNode.className = "menuTitle collapsed";
					return false;
				} else {
					toggle(this.submenu[0]);
					return false;
				}
			}
		}
	}
}
//addLoadEvent(toggleMenu);

function toggle(showHit) {
	var menu = document.getElementById("sidenavContainer");
	var toggles = menu.getElementsByTagName("ul");
		for (var i=0; i<toggles.length; i++) {
		var mytargetLink = toggles[i];
		if (mytargetLink.className == "show") {	
			mytargetLink.className = "hide";
			mytargetLink.parentNode.className = "menuTitle collapsed";
		}
		showHit.className = "show";
		showHit.parentNode.className = "menuTitle displayed";
	}
}
// tabbed panels ===================================================================

myPanels = new Array();
myPanels[0] = "panel1";
myPanels[1] = "panel2";


//alert(myPanels[0]);

function init() {
if (!document.getElementsByTagName) return false;
if (!document.getElementById("holder")) return false;	
	for (i=1; i<myPanels.length; i++) {
		document.getElementById(myPanels[i]).style.display = "none";
	}
}

addLoadEvent(init);

function showHide(target) {
	var tabs = document.getElementById("controls").getElementsByTagName("a");
	for (i=0; i<myPanels.length; i++) {
			if(myPanels[i] != target) {
			document.getElementById(myPanels[i]).style.display = "none";
			tabs[i].className = "inActive";

		} else {
			document.getElementById(myPanels[i]).style.display = "block";
			tabs[i].className = "active";
		}
	}
}


/* this function ascribes a particular string (panel name) to each button. Note the attachemnt of a unique property to each itme in "mylinks." Note use of "this" keyword as opposed to that of demo function below.*/
function prepareButtons() {
  if (!document.getElementsByTagName) return false;
  if (!document.getElementById) return false;
  if (!document.getElementById("holder")) return false;
	var mylinks = document.getElementById("controls").getElementsByTagName("a");
  	for (var i=0; i< mylinks.length; i++) {
		myTarget = mylinks[i];
		myTarget.panel = "panel" + (i+1);
		myTarget.onclick = function() {
			showHide(this.panel);
			return false;
		}
	}
}
addLoadEvent(prepareButtons);

// =============================================================================================================

// scrolling thumbnails scripts
function moveElement(elementID,final_x,final_y,interval) {
  if (!document.getElementById) return false;
  if (!document.getElementById(elementID)) return false;
  var elem = document.getElementById(elementID);
  if (elem.movement) {
    clearTimeout(elem.movement);
  }
  if (!elem.style.left) {
    elem.style.left = "0px";
  }
  if (!elem.style.top) {
    elem.style.top = "0px";
  }
  var xpos = parseInt(elem.style.left);
  var ypos = parseInt(elem.style.top);
  if (xpos == final_x && ypos == final_y) {
    return true;
  }
  if (xpos < final_x) {
    xpos+=myIncrement;
  }
  if (xpos > final_x) {
    xpos-=myIncrement;
  }
  if (ypos < final_y) {
    ypos+=myIncrement;
  }
  if (ypos > final_y) {
    ypos-=myIncrement;
  }
  elem.style.left = xpos + "px";
  elem.style.top = ypos + "px";
  var repeat = "moveElement('"+elementID+"',"+final_x+","+final_y+","+interval+")";
  elem.movement = setTimeout(repeat,interval);
}
function stopElement(elementID) {
  var elem = document.getElementById(elementID);
  if (elem.movement) {
    clearTimeout(elem.movement);
  }
}
var myIncrement = 5;

/*final_xleft = 0;
final_xright = -540;*/



function prepareScrollButtons() {
  if (!document.getElementsByTagName) return false;
  if (!document.getElementById) return false;
  if (!document.getElementById("controller")) return false;
  
 	 var final_ytop = 0;
	var final_ybottom = -306;
	
	var mylinks = document.getElementById("controller").getElementsByTagName("a");
  	for (var i=0; i< mylinks.length; i++) {
	var idName = mylinks[i].getAttribute("id");
	if (idName == "leftArrow") {
			mylinks[i].onmouseover = function() {
			moveElement("scrollThumbs",0,final_ytop,10);
		  }
			mylinks[i].onmouseout = function() {
			stopElement("scrollThumbs");
		  }
			mylinks[i].onclick = function() {
			return false;
		  }
		}
			else if (idName == "rightArrow") {
			mylinks[i].onmouseover = function() {
			moveElement("scrollThumbs",0,final_ybottom,10);
		  }
			mylinks[i].onmouseout = function() {
			stopElement("scrollThumbs");
		  }
			mylinks[i].onclick = function() {
			return false;
		  }
		}
	}
}

addLoadEvent(prepareScrollButtons);

function prepareScrollButtons2() {
  if (!document.getElementsByTagName) return false;
  if (!document.getElementById) return false;
  if (!document.getElementById("controller2")) return false;
  
  	var final_ytop = 0;
	var final_ybottom = -240;
  
	var mylinks = document.getElementById("controller2").getElementsByTagName("a");
  	for (var i=0; i< mylinks.length; i++) {
	var idName = mylinks[i].getAttribute("id");
	if (idName == "leftArrow") {
			mylinks[i].onmouseover = function() {
			moveElement("scrollThumbs2",0,final_ytop,10);
		  }
			mylinks[i].onmouseout = function() {
			stopElement("scrollThumbs2");
		  }
			mylinks[i].onclick = function() {
			return false;
		  }
		}
			else if (idName == "rightArrow") {
			mylinks[i].onmouseover = function() {
			moveElement("scrollThumbs2",0,final_ybottom,10);
		  }
			mylinks[i].onmouseout = function() {
			stopElement("scrollThumbs2");
		  }
			mylinks[i].onclick = function() {
			return false;
		  }
		}
	}
}

addLoadEvent(prepareScrollButtons2);


// home page rollover effects ========================================================================
// home page rollover effects
geographyRolls = function() {
	if (document.getElementById && document.getElementById("mapNav")) {
		var navRoot = document.getElementById("mapNav");
		subNav = navRoot.getElementsByTagName("a");	
		for (var i = 0; i<subNav.length; i++ ) {
		targetLink = subNav[i];
		targetLink.roll = "roll_" + i;
			targetLink.onmouseover=function() {
				document.getElementById(this.roll).className="show";
			}
			targetLink.onmouseout=function() {
				document.getElementById(this.roll).className="hide";
			}
		}
	}
}

addLoadEvent(geographyRolls);

serviceRollsTest = function() {
	if (!document.getElementById && !document.getElementById("servicesNav") || document.getElementById("servicesNav") == null) return false;
		var navRoot = document.getElementById("servicesNav");
		var subNav = navRoot.getElementsByTagName("li");
		//alert(subNav.length)
		var subNavLinks = subNav[0].getElementsByTagName("a");
		if (subNavLinks.length > 1) {
			serviceRollsDev();
		} else {
			serviceRollsProd();
		}

}

addLoadEvent(serviceRollsTest);


serviceRollsDev = function() {
//	if (document.getElementById && document.getElementById("servicesNav")) {
		var navRoot = document.getElementById("servicesNav");
		var subNav = navRoot.getElementsByTagName("a");	
		var rollNum = 0;
		for (var i = 1; i<subNav.length; i+=2 ) {
		targetLink = subNav[i];
		targetLink.roll = "roll_" + rollNum;
			targetLink.onmouseover=function() {
				document.getElementById(this.roll).className="show";
				document.getElementById("default").className="hide";
			}
			targetLink.onmouseout=function() {
				document.getElementById(this.roll).className="hide";
				document.getElementById("default").className="show";
			}
			rollNum++;
		}
//	}
}

serviceRollsProd = function() {
//	if (document.getElementById && document.getElementById("servicesNav")) {
		var navRoot = document.getElementById("servicesNav");
		var subNav = navRoot.getElementsByTagName("a");	
		//var rollNum = 0;
		for (var i = 0; i<subNav.length; i++ ) {
		targetLink = subNav[i];
		targetLink.roll = "roll_" + i;
			targetLink.onmouseover=function() {
				document.getElementById(this.roll).className="show";
				document.getElementById("default").className="hide";
			}
			targetLink.onmouseout=function() {
				document.getElementById(this.roll).className="hide";
				document.getElementById("default").className="show";
			}
		}
//	}
}
//addLoadEvent(serviceRolls);


//***Carousel Script***********************************************************

function getPanel(p, h) {
      		document.getElementById("p"+h).style.display = "none";
      		document.getElementById("p"+p).style.display = "block";
    	}
	function getNext() {
		var displayedImages = getPageItems("img", "displayed")
		var hiddenImages = getPageItems("img", "hidden")
		var idMax;
		if(parseFloat(displayedImages[3].id) > parseFloat(hiddenImages[hiddenImages.length-1].id))
			{idMax=parseFloat(displayedImages[3].id);} else {idMax=parseFloat(hiddenImages[hiddenImages.length-1].id);}
		var idLast = parseFloat(displayedImages[3].id);
		var idNew = idLast + 1;
		if(idNew < idMax) {
			document.getElementById("rightArrowDisabled").style.display = "none";
			document.getElementById("rightArrow").style.display = "block";
		} else {
			document.getElementById("rightArrow").style.display = "none";	
			document.getElementById("rightArrowDisabled").style.display = "block";
		}

		if(idMax >= idNew) {
			displayedImages[0].name = "hidden";
			document.getElementById("i"+displayedImages[0].id).style.display = "none";
			document.getElementById(idNew).name= "displayed";
			document.getElementById(idLast).className = "";
			document.getElementById(idNew).className = "last";
			document.getElementById("i" + idNew).style.display = "block";
			document.getElementById("leftArrowDisabled").style.display = "none";
			document.getElementById("leftArrow").style.display = "block";
		}
	}
	function getPrevious() {
		var displayedImages = getPageItems("img", "displayed")
		var idFirst = parseFloat(displayedImages[0].id);
		var idNew = idFirst - 1;
		var idLast = parseFloat(displayedImages[3].id);
		var idNewLast = idLast - 1;
		if(idNew > 1) {
			document.getElementById("leftArrowDisabled").style.display = "none";
			document.getElementById("leftArrow").style.display = "block";
		} else {
			document.getElementById("leftArrow").style.display = "none";
			document.getElementById("leftArrowDisabled").style.display = "block";	
		}
		if(idNew >= 1) {
			displayedImages[3].name = "hidden"; 
			document.getElementById("i"+displayedImages[3].id).style.display = "none";
			document.getElementById(idNew).name= "displayed";
			document.getElementById(idLast).className = "";
			document.getElementById(idNewLast).className = "last";
			document.getElementById("i" + idNew).style.display = "block";
			document.getElementById("rightArrowDisabled").style.display = "none";
			document.getElementById("rightArrow").style.display = "block";

		}
	}
	function getImage(image){
      	    	var mainImages = getPageItems("div","imageDisplay");
	    	for (var i=0; i<mainImages.length; i++) {
		    var targetLink = mainImages[i];
		    targetLink.style.display = "none";
		}
		document.getElementById("m"+image).style.display = "block";
    	}
	function getPageItems(tag, name) {
     
     		var elem = document.getElementsByTagName(tag);
     		var arr = new Array();
     		for(i = 0,iarr = 0; i < elem.length; i++) {
       	   att = elem[i].getAttribute("name");
          	   if(att == name) {
                   arr[iarr] = elem[i];
                   iarr++;
          	   }
    		}
     		return arr;
	}
	function openLink(address, newwindow, title, width, height) {
		if(newwindow == "y"){
			var size;
			if(title == "") {title = "NewWindow";}
			if(width > 0) {size = ",width=" + width;}
			if(height > 0) {size = size + ",height=" + height;}
			window.open (address,title,"menubar=1,resizable=1,toolbar=1,scrollbars=1"+size); 

		} else {
			window.location = address;
		}
	}


//***End Carousel Script*******************************************************

//***BEGIN Printer Friendly Script*********************************************

	function printPage() {
		
		//document.language.printFriendly.value = "yes";
		//win=window.open('','PrintPage','toolbar=1,resizable=1,width=700,scrollbars=1'); 
 		//document.language.target='PrintPage';
 		//document.language.action=window.location;
		//document.language.submit();

		var OLECMDID = 7;
/* OLECMDID values:
* 6 - print
* 7 - print preview
* 1 - open window
* 4 - Save As
*/
var PROMPT = 1; // 2 DONTPROMPTUSER
var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
WebBrowser1.ExecWB(OLECMDID, PROMPT);
WebBrowser1.outerHTML = "";


	}
	function getQueryVariable(variable) 
	{ 
		var query = window.location.search.substring(1); 
		var vars = query.split("&"); 
		for (var i=0;i<vars.length;i++) 
		{ 
			var pair = vars[i].split("="); 
			if (pair[0] == variable) 
			{ 
				return pair[1]; 
			} 
		} 
	} 
	function getPrintFriendly() 
	{ 
		var query = window.location.toString();
		if(query.indexOf('?') > 0) {
			query= query + "&printFriendly=yes";
		} else {
			query= query + "?printFriendly=yes";
		}
		window.open (query,"printFriendly","width=800,scrollbars=1"); 
	} 


//***END Printer Friendly Script************************************************
	function trim (s){ 
	  var l=0; var r=s.length -1; 
	  while(l < s.length && s[l] == ' ') 
	  {     l++; } 
	  while(r > l && s[r] == ' ') 
	  {     r-=1;     } 
  	return s.substring(l, r+1);
}

//*** temporary fix for corp logo ************************************************
corpLogoFix = function() {
	var element;
	var children = document.getElementById('corpId').children; 
	for(var i = 0, length = children.length; i < length; i++) {
      if(children[i].nodeName == 'A') {
            element = children[i];
            element.href = "../../index.htm"/*tpa=http://www.aecom.com/*/
            break;      
      } 
	} 
}

addLoadEvent(corpLogoFix);


//*** social bookmarking variables ************************************************

function displaySocial(smartListURL){
	var variable = smartListURL;
	var url = encodeURIComponent(document.location.href);
	var title = encodeURIComponent(document.title);
	variable = variable.replace(/XTITLEX/g, title);
	variable = variable.replace(/XURLX/g, url);
	variable = variable.replace("XLINKEDIN-LOGOX", "<img src='../images/icons/sm_linkedin-1.png'/*tpa=http://www.aecom.com/internet1/images/icons/sm_linkedin-1.png*/ height='16' width='16'>");
	variable = variable.replace("XFACEBOOK-LOGOX", "<img src='../images/icons/sm_facebook-1.png'/*tpa=http://www.aecom.com/internet1/images/icons/sm_facebook-1.png*/ height='16' width='16'>");
	variable = variable.replace("XTWITTER-LOGOX", "<img src='../images/icons/sm_twitter-2.png'/*tpa=http://www.aecom.com/internet1/images/icons/sm_twitter-2.png*/ height='16' width='16'>");
	variable = variable.replace("XGOOGLEPLUS-LOGOX", "<img src='../images/icons/sm_googlePlus_icn.png'/*tpa=http://www.aecom.com/internet1/images/icons/sm_googlePlus_icn.png*/ height='16' width='16'>");
	return variable;
}

/***********************************************
* opening declaration for jQuery "documant ready" functions
***********************************************/
	$(document).ready(function(){
							   
	$('ul.sb_btn a').live('click',function(e){
	  e.preventDefault();
	 window.open(this.href, "sb_popup", 'width=1000,height=550');
	});	
	
	
	
							   
	});  // NOTE!! closing brackets for jQuery "document ready" functions