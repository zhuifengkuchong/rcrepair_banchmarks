/**
 * Autocomplete Base Class.
 * @function JB.Class.Autocomplete
 * @param {String} container The id or class selector of the input element.
 * @require jQuery.ui.autocomplete
 */

//compares by country name
function compareByCountry(a, b, term) {

    if (a.country.toLowerCase() < b.country.toLowerCase())
        return -1;
    if (a.country.toLowerCase() > b.country.toLowerCase())
        return 1;
    return 0;

}
//compares and sorts city
//compares against city to the 3 letter search term entered
function compareByCity(a, b, term) {
    //parse city name
    var cityA = a.label.substring(0, a.label.indexOf(",")).toLowerCase();
    var cityB = b.label.substring(0, b.label.indexOf(",")).toLowerCase();

    //if some cities dont have , in the label just use label value
    if (cityA == "") cityA = a.label;
    if (cityB == "") cityB = b.label;

    //check if both items start with the same 3 letter search term we entered
    if ((cityA.substring(0, 3).toLowerCase() == term.substring(0, 3).toLowerCase()) && (cityB.substring(0, 3).toLowerCase() == term.substring(0, 3).toLowerCase())) {
        if (cityA < cityB)
            return -1;
        if (cityA > cityB)
            return 1;
        if (cityA == cityB)
           return compareByCountry(a, b, term);
    }
    //if city a equals 3 letter search term keep it in front
    if ((cityA.substring(0, 3).toLowerCase() == term.substring(0, 3).toLowerCase()) && (cityB.substring(0, 3).toLowerCase() != term.substring(0, 3).toLowerCase())) {
        return -1;
    }
    //otherwise move b ahead of a
    else if ((cityA.substring(0, 3).toLowerCase() != term.substring(0, 3).toLowerCase()) && (cityB.substring(0, 3).toLowerCase() == term.substring(0, 3).toLowerCase())) {
        return 1;
    }
    else {
        if (cityA < cityB)
            return -1;
        if (cityA > cityB)
            return 1;
        if (cityA == cityB)
            return compareByCountry(a, b, term);
    }
}
//compares by code - puts actual airport codes ahead
function compareByCode(a, b, term) {

    var searchTermCode = window["o" + term.toUpperCase()];

    if (searchTermCode != undefined) {
        if (a.code == searchTermCode.code && b.code != searchTermCode.code)
            return -1;
        else if (a.code != searchTermCode.code && b.code == searchTermCode.code)
            return 1;
        else
            return compareByCity(a, b, term);
    }
    else 
        return compareByCity(a, b, term);
}

function compareByMac(a, b, term) {
    if (!a.isMac && b.isMac)
        return -1;
    if (a.isMac && !b.isMac)
        return 1;
    if((!a.isMac && !b.isMac) || (a.isMac && b.isMac))
       return compareByCode(a, b, term);

}


function sortResults(searchResults, term) {

    //if MAC city set isMac flag
    $(searchResults).each(function (index) {
        this.isMac = false;
        var match = /\w{3}(?=\)$)/;
        if (!match.exec(this.code)) {
            for (var j = 0; j < MAClist.length; j++) {
                var macCity = window["o" + MAClist[j]];
                if (macCity.name == this.label) {
                    this.isMac = true;
                    break;
                }
            }
        }

    });

    //sort results
    searchResults.sort(function (a, b) {
        if (a.jb && !b.jb)
            return -1;
        if (!a.jb && b.jb)
            return 1;
        if ((a.jb && b.jb) || (!a.jb && !b.jb))
          return compareByMac(a, b, term);
    });

}

//finds a mac city match in results set checking code against mac matrix
function findMacCityMatch(cityResults) {
    var macMatchesArray = [];
    //search result list
    for (i = 0; i < cityResults.length; i++) {
        //look in each mac definition
        for (var key in MACDefinitions) {
            if (MACDefinitions.hasOwnProperty(key)) {
                //check each city defined in a mac definition
                for (k = 0; k < MACDefinitions[key].length; k++) {
                    if (cityResults[i].code.toUpperCase() == MACDefinitions[key][k].toUpperCase()) {

                        macMatchesArray.push(key.toUpperCase());
                    }
                }
            }
        }
    }
    return macMatchesArray;
}

//Checks if source array (from search definition in autocompleter) contains mac cities
function doesSourceSupportMacCity(sourceArray) {

    for (i = 0; i < sourceArray.length; i++) {
        for (j = 0; j < MAClist.length; j++) {
            if (sourceArray[i].code.toUpperCase() == MAClist[j].toUpperCase()) {
                return true;
            }
        }
    }
    return false;
}


JB.Class.Autocomplete = JB.Class.extend({
    /**
    * Library dependencies
    * @type object
    */
    require: {
        "jQuery.ui.autocomplete": "http://www.jetblue.com/js/src/vendor/jquery-ui-1.8.16.custom.min"
    },
    /**
    * @constructor
    */
    init: function (container, options) {
        /**
        * Options
        */
        this.options = {
            source: [],
            minLength: 3,
            autoFocus: true,
            onSelect: function () { },
            onFocusIn: function () { },
            onFocusOut: function () { }
        };
        $.extend(this.options, options || {});
        this.container = $(container);
        this._super();
    },
    /** @ignore */
    _build: function () {

        var ffstatus = this.container.closest("#ffstatus");
        var onlyJBCities = ffstatus.length > 0;
        if (onlyJBCities)
            ffstatus.find(".city-desc").remove();

        //Remove non JB cities - DONE IN THE SOURCE OPTION NOW
        /*
        if (onlyJBCities) {
        var items = this.options.source;
        $.each(items, function (key, value) {
        if (value != undefined) {

        if (!value.jb) {
                    
        //console.log(value.jb + " " + value.label);
                    
        items.splice(key, 1);
        }
        }
        });
        this.options.source = items;
        }
        */

        var self = this;
        this.toggler = this.container.find("span[class*=btn]");
        this.input = this.container.find("input[type=text]");

        this.input.autocomplete({
            source: this.options.source,
            minLength: this.options.minLength,
            autoFocus: this.options.autoFocus,
            appendTo: this.container.find(".inner"),
            create: function (e, ui) {
                self.menu = $(this).data("autocomplete").menu.element.addClass("city-list");
                self.menu.prependTo($(this).data("autocomplete").menu.element.parent());
            },
            search: function (e, ui) {
                var results = $.ui.autocomplete.filter(self.options.source, this.value);
                if (!!results.length) {
                    return;
                }
                setTimeout(function () {
                    self.autocompleter._response([{ label: " ", value: ""}]);
                }, 200);
            },
            focus: function (e, ui) {
                if (!!!(ui.item)) return;
                self.menu.find("a.active").removeClass("active");
                self.menu.find("a[rel=" + ui.item.code + "]").addClass("active");
                self.container.addClass("active");
            },
            change: function (e, ui) {
            },
            open: function (e, ui) {
                self.container.addClass("active");
            },
            close: function (e, ui) {
                self.container.removeClass("active");
            },
            select: function (e, ui) {
                self.options.onSelect(e, ui);
                self.container.removeClass("active");
            }
        });
        // Find and assign autocomplete instance to class
        this.autocompleter = this.input.data("autocomplete");

        // Hijack jQuery.ui.autocomplete._suggest method
        this.autocompleter._suggest = function (items) {
            try {
                var noResults = !!(items[0].label == " ");
                if (noResults) self.container.addClass("no-results");
                else self.container.removeClass("no-results");


                //JB MX1.6 Add autocomplete check to inject MAC if term is city code belonging to mac city group
                if (!noResults) {

                    //Do we support mac cities in the source of this autocomplete
                    var macCityExistsInSource = doesSourceSupportMacCity(self.options.source);

                    /*
                    console.log("**************");
                    console.log("does mac cities exist: " + macCityExistsInSource);
                    */


                    /*
                    if we support mac cities in our respective source then we can check
                    and inject the respective mac city if a city code is entered that is part of a mac city group
                    EX - FLL is entered - this will also add South Florida Area to the autocomplete   
                    */

                    if (macCityExistsInSource == true) {

                        /*
                        console.log("I entered this: " + this.term);
                        console.log("items has : " + items.length);
                        */


                        //Lets find mac city needed to be added based on our search results
                        var macCityAddition = findMacCityMatch(items);

                        /*
                        console.log("Add this mac: " + macCityAddition);
                        */

                        //Check if macCityAddition is not undefined and its not in our search items already lets add it
                        if (macCityAddition.length > 0) {

                            //We may have multiple macs found
                            for (z = 0; z < macCityAddition.length; z++) {

                                var macCityExistsInResults = false;
                                /*
                                console.log("macCityExistsInResults: before check " + macCityExistsInResults);
                                */

                                //Does our mac city exist in our search results already
                                for (i = 0; i < items.length; i++) {
                                    if (items[i].code.toUpperCase() == macCityAddition[z].toString()) {
                                        macCityExistsInResults = true;
                                    }
                                }

                                /*
                                console.log("macCityExistsInResults: after check " + macCityExistsInResults);
                                */

                                //if its not in there - lets add it
                                if (macCityExistsInResults == false) {

                                    /*
                                    console.log("start adding mac: " + macCityAddition[z].toString());
                                    */

                                    //need to find the index in our source to identify which item to add
                                    var index = -1;
                                    for (j = 0; j < self.options.source.length; j++) {
                                        if (self.options.source[j].code.toUpperCase() == macCityAddition[z].toString()) {
                                            index = j;
                                        }
                                    }

                                    /*
                                    console.log("index found is: " + index);
                                    */

                                    //If we have a valid index lets push the mac city into our items
                                    if (index != -1) {

                                        /*
                                        console.log("adding mac w code to results: " + self.options.source[index].code);
                                        */
                                        var itemToAdd = self.options.source[index];
                                        itemToAdd.value = itemToAdd.label;
                                        items.push(itemToAdd);

                                    } //else console.log("no index found for: " + macCityAddition[z].toString());
                                } //else console.log("mac already exists: not adding it");
                            }
                        } //else console.log("mac undefined from search term");


                    }

                    /*
                    console.log("items has : " + items.length);
                    console.log("**************");
                    */

                } //else console.log("items is empty");

                sortResults(items, this.term);
                var ul = this.menu.element.empty().zIndex(this.element.zIndex() + 1);
                if (!noResults) this._renderMenu(ul, items);
                this.menu.refresh();
                ul.show();
                if (!noResults) this._resizeMenu();
                if (this.options.autoFocus) this.menu.next(new $.Event("mouseover"));
            } catch (e) { }
        };
        // Hijack jQuery.ui.autocomplete._renderItem method
        this.autocompleter._renderItem = function (ul, item) {
            if (self.container.selector.search("to-wrap") != -1) {
                try {
                    var fromCode = "";
                    var fromValue = "";
                    if (self.container.selector.search("fvacation") != -1) {
                        var fromValue = $('#vac_from_field').val();
                    }
                    else if (self.container.selector.search("fstatus") != -1) {
                        var fromValue = $('#fs_from_field').val();
                    }
                    else {
                        var fromValue = $('#from_field').val();
                    }
                    var toCode = item.code.substring(item.code.length - 3, item.code.length);
                    //Below is breaking flightstatus (fromValue undefined) - madan
                    if (fromValue != "") {
                        //if MAC city add 3 letter airport code.
                        var match = /\w{3}(?=\)$)/;
                        if (!match.exec(fromValue)) {
                            for (var j = 0; j < MAClist.length; j++) {
                                var macCity = window["o" + MAClist[j]];
                                if (macCity.name == fromValue) {
                                    fromCode = macCity.code;
                                    break;
                                }
                            }
                        }
                        else {

                            fromCode = fromValue.substring(fromValue.length - 4, fromValue.length - 1);
                        }
                        if ($.inArray(fromCode, window['r' + toCode]) == -1) return;
                    }
                } catch (e) { }
            }
            var partner = (item.jb) ? "" : " class='partner'";

            var cityListItem = $("<li" + partner + "></li>")
				.data("item.autocomplete", item)
				.append("<a rel='" + item.code + "'>" + item.label + "</a>");


            if (item.isMac) 
                cityListItem.addClass("MAC");
    
            if (!item.duplicate)
                cityListItem.appendTo(ul);
    
            return cityListItem;
        };

        // Input Key Event Handler
        this.input.bind("keydown keyup keypress", function (e) {
            // Disable Tab Through
            if (!!(self.menu.find("a.active").length) && (e.which === 9)) return false;
        });
        // Input Focus Events Handlers
        this.input.focusin(function (e) {
            try {
                if (this.value.length >= self.options.minLength) {
                    self.autocompleter.search()
                }
                else {
                    self.autocompleter._response([{ label: " ", value: ""}])
                };
                self.options.onFocusIn(e);
            } catch (e) { }
        });
        // Focus Out Event Handler
        this.input.focusout(function (e) {
            try {
                var results = $.ui.autocomplete.filter(self.options.source, this.value);
                if (!!!results.length || this.value.length < self.options.minLength) this.value = this.defaultValue;
                self.options.onFocusOut(e);
            } catch (e) { }
        });
    }
});
