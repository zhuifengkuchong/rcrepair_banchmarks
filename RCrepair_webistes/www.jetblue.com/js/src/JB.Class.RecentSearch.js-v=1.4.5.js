JB.Class.RecentSearch = JB.Class.extend({
    /**
    * Library dependencies
    * @type object
    */
    require: {
        "amplify": "http://www.jetblue.com/js/src/vendor/amplify.min"
    },
    /**
    * @Constructor
    */
    init: function (target, form, options) {
        /**
        * Options
        */
        this.options = {
            /*
            pageloadResetRun: true,
            pageloadResetData: { 
            "drop_down_value": "",
            "search_type" : "find_flights",
            "flight_type" : "round_trip",
            "from_field" : "",
            "departure_field" : "",
            "adult_count" : "1",
            "kid_count" : "0",
            "kid_vacation" : "0",
            "kid_age_1" : "0",
            "kid_age_2" : "0",
            "kid_age_3" : "0",
            "infant_count" : "0",
            "to_field" : "",
            "return_field" : "",
            "hotel_room_count" : "1",
            "rental_car" : "no",
            "fare_display" : "lowest"
            }
            */
        };
        $.extend(this.options, options || {});
        this.form = $(form);
        this.target = $(target);
        this._build();

        //if(window.console) console.log('target', target, 'form', form);

    },
    /** @ignore */
    _build: function () {
        var self = this;

        /*
        if(this.options.pageloadResetRun){
        this.fillForm();
        }
        */

        if (this.form.hasClass('fvacation-form')) {
            //if(window.console) console.log('vacation form');
            //this.searchArray = amplify.store("fvacation-form-data");
            //this.searchArray = readFVCookie();
            this.searchArray = getHistory("fvacation-form-data");
        } else if (this.form.hasClass('fflight-form')) {
            //if(window.console) console.log('flight form');
            //this.searchArray = amplify.store("fflight-form-data");
            this.searchArray = getHistory("fflight-form-data");
        }

        var listInject = '';
        var numOfSearches = this.searchArray ? this.searchArray.length : 0;

        //if(window.console) console.log('this.searchArray', this.searchArray);

        if (numOfSearches === 0) {
            this.target.siblings('span').html(numOfSearches + ' recent searches').addClass('disabled-span');
            this.target.remove();
        } else {
            this.target.siblings('span').html(numOfSearches + ' recent searches<span class="dwn-arrow"></span>');
            this.target.children('div.inner').children('span').html(numOfSearches + ' recent searches<span class="dwn-arrow"></span>');

            for (var i = 0, ii = numOfSearches; i < ii; i++) {
                listInject += '<li class="loc' + i + '">' + this.searchArray[i].drop_down_value + '</li>'
            }

            this.target.find('div.inner ol').html(listInject);

            this.target.find('div.inner ol li').bind('click', function (e) {
                self.fillForm($(this).attr('class'));
            });
        }


    },
    fillForm: function (arrPos) {

        var self = this;

        if (!!!arrPos) {
            var recentSearchObj = this.options.pageloadResetData;
        } else {
            var pos = parseInt(arrPos.substr(3));
            var recentSearchObj = this.searchArray[pos];
        }

        var oneWayTrip = false,
			triggerArray = [];

        for (var key in recentSearchObj) {

            var value = recentSearchObj[key];

            var target = this.form.find('#' + key);
            if (!!!(target.length)) target = this.form.find('input[name="' + key + '"][value="' + value + '"]');
            if (!!!(target.length)) continue;

            var type = target.attr('type');
            if (!!!type) {
                if (target.is("select")) {
                    type = 'select';
                };
            };

            // if(window.console) console.log('target: ', target, '\n type: ', type, '\n value: ', value );	

            if (type == 'text') {

                if (key == 'departure_field' || key == 'return_field' || key == 'vac_departure_field' || key == 'vac_return_field') {
                    if (oneWayTrip && key == 'return_field') {
                        target.attr('value', 'No Data');
                        target.siblings('span.placeholder').text('Return date');
                    } else {
                        target.attr('value', value);
                        target.siblings('span.placeholder').text(value);
                    }

                    if (key == 'departure_field' || key == 'vac_departure_field') {
                        // if(window.console) console.log('depart-cal', value);
                        $('#depart-cal').datepicker('setDate', value);
                        //$('#depart-cal').datepicker('refresh');
                        triggerArray.push($('#depart-cal').find('td.ui-datepicker-current-day'));
                    } else if (key == 'return_field' || key == 'vac_return_field') {
                        // if(window.console) console.log('return-cal', value);
                        $('#return-cal').datepicker('setDate', value);
                        // $('#return-cal').datepicker('refresh');
                        if (!oneWayTrip) {
                            triggerArray.push($('#return-cal').find('td.ui-datepicker-current-day'));
                        }
                    }

                } else if (key == 'kid_age_1' || key == 'kid_age_2' || key == 'kid_age_3') {
                    if (value === "0") {
                        target.attr('value', '');
                    } else {
                        target.attr('value', value);
                    }
                } else {
                    target.attr('value', value);
                }
            } else if (type == 'radio') {
                if (value == 'one_way') {
                    oneWayTrip = true;
                } else if (value == 'round_trip') {
                    oneWayTrip = false;
                }

                var radioStyleTrigger = target.attr('checked', 'checked').siblings('span.radio-styled');
                triggerArray.push(radioStyleTrigger);
            } else if (type == 'select' || type == 'select-one') {
                var targetOption = target.children('option[value="' + value + '"]'),
					targetOptionPos = targetOption.index();
                //if(window.console) console.log(target, 'select option index', targetOptionPos, target.siblings('div').find('ol').children('li:eq('+targetOptionPos+')'));

                targetOption.attr('selected', 'selected').siblings('option').removeAttr('selected');
                triggerArray.push(target.parent());

                if (key == 'kid_vacation') {
                    target.parent('li').attr('class', 'kid-vacation-dropdown');
                    triggerArray.push(target.siblings('div.kid-count').find('ol li:eq(' + targetOptionPos + ')'));
                } else {
                    triggerArray.push(target.siblings('div').find('ol li:eq(' + targetOptionPos + ')'));
                }
            } else if (type == 'checkbox') {
                //flexible date.
                if (value == true) {
                    $('#flexible').attr('checked', true);
                    $('.bookerFlexibleCheckbox').addClass('bookerFlexible_active');
                } else {
                    $('#flexible').attr('checked', false);
                    $('.bookerFlexibleCheckbox').removeClass('bookerFlexible_active');
                }
            }
        }

        //if(window.console) console.log('triggerArray \n', triggerArray);		

        $.each(triggerArray, function (index, value) {
            var self = $(value);
            //if(window.console) console.log('click array for loop item: ', index, value);
            setTimeout(function () { self.trigger('click') }, 1);
        });

        // if(window.console) console.log('clear all values in booker form on pageload');
        /*
        if(this.options.pageloadResetRun){
        this.options.pageloadResetRun = false;
        }
        */
    }
});