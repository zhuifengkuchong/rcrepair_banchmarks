//Detect if device has touch capability.
//Helpful if you need no hover states on element. Precede the selector with ".notouchonly"

function is_touch_device() {
	return (('ontouchstart' in window)
	  || (navigator.MaxTouchPoints > 0)
	  || (navigator.msMaxTouchPoints > 0));
}

$(function(){
	var htmlEL = $('html');
	if(!is_touch_device()) {
		htmlEL.addClass('notouchonly');
	}
	else {
		 htmlEL.addClass('devicehastouch');
	}
});
