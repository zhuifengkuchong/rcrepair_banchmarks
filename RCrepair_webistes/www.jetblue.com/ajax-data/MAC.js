var MAClist = ['NYC', 'WAS', 'XBO', 'XDR', 'XFL', 'XSF', 'ZLA'];


var MACDefinitions = {
    'NYC': ["LGA", "JFK", "EWR", "HPN", "SWF"],
    'XFL': ["PBI", "FLL"],
    'ZLA': ["BUR", "LAX", "LGB"],
    'XSF': ["SFO", "SJC", "OAK"],
    'WAS': ["IAD", "DCA", "BWI"],
    'XBO': ["BOS", "PVD", "ORH"],
    'XDR': ["SDQ", "LRM"]
};