// Seasonal route data.
var seasonalRoutes = new Array(['BDA','BOS'], ['BOS','MBJ'], ['BOS','NAS'], ['BOS','PLS'], ['BOS','SXM'], ['SEA','ANC'], ['SEA','ANC'], ['LGB','ANC']);

//Day of the week route data
var dayOfWeekRoutes = new Array(['BOS','PUJ'], ['JFK','LIR'], ['JFK','LRM'], ['JFK','PLS'], ['JFK','UVF'], ['JFK','AZS'], ['JFK','CTG'], ['BOS','GCM'], ['JFK','GCM']);