function simpleModal(){
    $('.openModalRef').on('click', function(event){
        
        var mediaType = $(this).attr("data-type");//solo para videos en mobile
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && mediaType === "video") {
        }
        else{// sino el contenido no es video y dispositivo es distinto de mobile
            event.preventDefault();
            
            if( mediaType === "defaultModal"){
                var destino = $(this).attr("data-modal");
                var contenido = $('*[data-modal-id="' + destino + '"]');
                var html  = '<div class="modalBlock modalRef"><div class="modalCloseArea closeModalRef"></div><div class="modalContent"><div class="modalInnerContent"><div class="showData"><a href="#" class="closeModal closeModalRef">x</a><div class="mainModal">';
                html += '</div></div></div></div></div>';
                $(".layoutWrapper").append(html);
                contenido = contenido.clone( contenido );
                $(".mainModal").append( contenido );
                $(".modalRef").find('*[data-modal-id="' + destino + '"]').show();

            }
            else{
                var element = $(this).parents(".containerRef");
                var srcVideo = $(this).find("a").attr("href");
                if (typeof srcVideo == "undefined") {
                    srcVideo = $(this).attr("href");
                    if (typeof srcVideo == "undefined") {
                        srcVideo = "";
                    }
                }
 
                var srcImage = $(this).find("img").attr("src");
                var title = element.find(".titleRef").html();

                if (typeof title == "undefined") {
                    var title = "";
                }
       
                    //var srcVideoIframe = element.find(".iframevideo iframe").attr("src");

                    var html = '<div class="modalBlock modalRef">';
                    html += '<div class="modalCloseArea closeModalRef"></div>';
                    html +=    '<div class="modalContent">';
                    html +=        '<div class="modalInnerContent">';
                    html +=            '<div class="showData">';
                    html +=                '<div class="headerModal">';
                    html +=                    '<h2>'+ title +'</h2>';
                    //html +=                    '<a href="#" class="shareModal">Share</a>';
                    html +=                '</div>';
                    html +=                '<a href="#" class="closeModal closeModalRef">x</a>';
                    html +=                '<div class="mainModal">';
                    if (mediaType === "video") {
                    html += '<div class="videoModal">';
                    html +=                        '<iframe width="560" height="315" src="'+ srcVideo+ '" frameborder="0">'
                    }
                    else {
                    html += '<div class="imageModal">';
                    html +=                        '<img src="'+ srcImage+ '" alt="" />'
                    }
                    html +=                    '</div>';
                    html +=                    '</div>';
                    html +=            '</div>';
                    html +=        '</div>';
                    html +=    '</div>';
                    html +='</div>';
                
                    $(".layoutWrapper").append(html);
                    
            }
            $("body").css("overflow", "hidden");
            $(".modalRef").fadeIn( "slow", function() {});
        }
    });


    $("body").on( "click", ".closeModalRef", function(event) {
        event.preventDefault();
        $(".modalRef").fadeOut( "slow", function() {
            $("body").css("overflow", "auto");
            $(".modalRef").remove();
        });
    });
}

