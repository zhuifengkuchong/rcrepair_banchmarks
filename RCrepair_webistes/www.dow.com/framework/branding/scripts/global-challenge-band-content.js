$(document).ready(function () {
    $('.globalChallengeBand').each(function () {
        var band = $(this);
        band.find(".bodyCarousel li.itemRef").eq(0).addClass("selected");
        band.find(".bodyCarousel li.itemRef").on("click", function (e) {
            e.preventDefault();
            band.find(".bodyCarousel li.itemRef").removeClass("selected");
            $(this).addClass("selected");
            var itemId = $(this).find("a").attr("data-itemid");
            band.find(".haderCarouselContent li.item").hide();
            band.find(".haderCarouselContent li.item").filter('[data-itemid="' + itemId + '"]').show();
        });
    });
});