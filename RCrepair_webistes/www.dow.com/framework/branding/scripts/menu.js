var isTouchDevice;
$(document).ready(function () {
    isTouchDevice = 'ontouchstart' in document.documentElement;
    setHeightMenu();
    heightDinamicMenuHeader();
    heightDinamicMenuSecondary()
    eventMenu();
    //menu responsive
    $(window).resize(function () {
        setHeightMenu();
        heightDinamicMenuHeader();
        heightDinamicMenuSecondary();
        eventMenu();
    });
    $(".globalHeaderContent .collapsed").on("click", function (event) {
        $(".globalHeaderContent .menuContent").toggle();
    });
});

$(window).load(function () {
    setHeightMenu();
    heightDinamicMenuHeader();
    heightDinamicMenuSecondary();
});


function heightDinamicMenuSecondary() {
    $('.sectionNavigation.menuRef ul').each(function () {
        var topDinamic = $(this).outerHeight();
        $(this).find("li > ul").css({ "top": topDinamic });
    });
}
function heightDinamicMenuHeader() {
    $('.menuContentNav.menuRef ul').each(function () {
        $(this).children().each(function () {
            var topDinamic = $(this).outerHeight();
            $(this).children("ul").first().css({ "top": topDinamic });
        });
    });
}
function setHeightMenu() {
    var windowWidth = $(window).width();
    if (windowWidth <= 767 || isTouchDevice === true) {
        var heightMenu = ($(window).height()) - $(".globalHeader .globalHeaderContent .logo").height();
        $(".globalHeader .menuContent .menuContentNav").css({ "height": heightMenu });
        $(".globalHeader").removeClass("desktopMenu");
        $(".globalHeader").addClass("deviceMenu")
        $("body").removeClass("desktopMenu");
        $("body").addClass("deviceMenu");
    }
    else {
        $(".globalHeader .menuContent .menuContentNav").css({ "height": "" });
        $(".globalHeader").removeClass("deviceMenu");
        $(".globalHeader").addClass("desktopMenu");
        $("body").removeClass("deviceMenu");
        $("body").addClass("desktopMenu");

    }
}
function eventMenu() {
    var windowWidth = $(window).width();
    $('.menuRef ul li').unbind('mouseenter'); //reset
    $('.menuRef ul li').unbind('mouseleave'); //reset
    $('.menuRef ul li .arrow').unbind("click"); //reset
    if (windowWidth <= 767 || isTouchDevice === true) {

        $('.menuRef ul li .arrow').on("click", function (event) {
            if ($(this).parent().children("ul").is(":hidden")) {// si el ul(submenu) no esta visible lo muestro
                $(this).parent().parent().find("ul").hide();
                $(this).parent().children("ul").stop().stop().fadeIn(300, function () {
                });
            } else {
                $(this).parent().children("ul").stop().stop().fadeOut(100, function () {
                });
            }
        });
    }
    else {
        $('.menuRef ul li ul').hide();
        $('.menuRef ul li').on("mouseenter", function () {
            if ($(this).children("ul").is(":hidden")) {// si esta cerrado lo muestro
                $(this).parent().find("ul").hide();
                $(this).children("ul").stop().stop().fadeIn(300, function () {
                });
            }
        });
        $('.menuRef ul li').on("mouseleave", function () {
                  $(this).children("ul").stop().stop().fadeOut(100, function () {
                  });
        });
}
}