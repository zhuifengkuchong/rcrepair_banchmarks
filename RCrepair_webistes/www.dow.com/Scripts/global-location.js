$(document).ready(function () {

    var regionParameter = getUrlParameter("region");

    if (typeof regionParameter === 'undefined' || regionParameter === null || regionParameter === "") {
        $(".dropdownCountry select").prop("disabled", true);
        $(".dropdownCountryGo input").prop("disabled", true).addClass("disable");
    }

    $(".dropdownRegion").on("change", function (event) {

        var parentform = $(this).parent();
        var regionId = $(this).find("option:selected").val();
        var dataJson = { 'itemId': regionId };

        $.ajax({
            type: 'POST',
            url: '/api/sitecore/GlobalLocation/Countries',
            dataType: 'json',
            data: JSON.stringify(dataJson),
            contentType: "application/json; charset=utf-8",
            context: this,
            success: function (response) {
                parentform.find(".dropdownCountry select").empty();
                parentform.find(".dropdownCountry select").append('<option value="" selected="selected">Country</option>');
                $.each($.parseJSON(response), function (index, data) {
                    parentform.find(".dropdownCountry select").append('<option value="' + data.Value + '">' + data.Text + '</option>');
                });
                parentform.find(".dropdownCountry select").prop("disabled", false);
                parentform.find(".dropdownCountryGo input").prop("disabled", false).removeClass("disable");
                parentform.find(".dropdownCountry select").change();
                var countryParameter = getUrlParameter("country");
                if (typeof countryParameter !== 'undefined' && countryParameter !== null) {
                    if (parentform.find(".dropdownCountry select").find("[value='" + countryParameter + "']").length > 0) {
                        parentform.find(".dropdownCountry select").val(countryParameter).removeAttr("selected").find("[value='" + countryParameter + "']").attr("selected", true).change();
                    }
                    $(".dropdownCountry .inputUL p").text(parentform.find(".dropdownCountry select option:selected").text());
                }
                regionParameter = "";
            },
            error: function () {
                alert('Countries could not be loaded.');
            }
        });

    });

});
