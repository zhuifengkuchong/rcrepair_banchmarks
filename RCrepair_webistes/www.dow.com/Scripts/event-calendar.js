$(document).ready(function () {

    var eventCategorySelected = undefined;
    var eventDateTimeSelected = undefined;

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0
    var yyyy = today.getFullYear();
    var hh = today.getHours();
    var min = today.getMinutes();

    if (dd < 10) {
        dd = '0' + dd  //Force 2 digit by adding 0 to the left
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    if (hh < 10) {
        hh = '0' + hh
    }

    if (min < 10) {
        min = '0' + min
    }

    today = yyyy + '/' + mm + '/' + dd + " " + hh + ":" + min;



    $("#eventShowMore").hide();

    $("#eventCategoryFilter").on("change", function (event) {
        eventCategorySelected = "";
        listEvents(event, true);
    });

    $("#eventDateTimeFilter").on("change", function (event) {
        listEvents(event, true);
    });

    $("#eventShowMore").on("click", function (event) {
        listEvents(event, false);
    });

    $("#eventSearchInput").on("input", function (event) {
        filterListedEvents(event);
    });

    $("#eventSearch").on("click", function (event) {
        filterListedEvents(event);
    });

    function listEvents(event, first) {

        if (event != null) {
            event.preventDefault();
        }

        var iterationNumber;
        if (first) {
            iterationNumber = 0;
        } else {
            iterationNumber = $("#eventList").data("iterationnumber");
            iterationNumber++;
        }
        $("#eventList").data("iterationnumber", iterationNumber);

        var eventDateTime = $("#eventDateTimeFilter").val();
        if (!eventDateTime || 0 === eventDateTime.trim().length) {
            return;
        }

        var eventCategory = $("#eventCategoryFilter option:selected").val();

        if (eventCategory == eventCategorySelected && eventDateTime == eventDateTimeSelected && iterationNumber == 0) {
            return;
        }

        eventCategorySelected = eventCategory;
        eventDateTimeSelected = eventDateTime;

        var categoryItemsId = $("#eventCategoryFilter").data("item-id");

        var dataJson = { 'categoryItemsId': categoryItemsId, 'eventCategory': eventCategory, 'eventDateTime': eventDateTime, 'iterationNumber': iterationNumber };

        $.ajax({
            type: 'POST',
            url: '/api/sitecore/EventCalendar/ListEvents',
            dataType: 'json',
            data: JSON.stringify(dataJson),
            contentType: "application/json; charset=utf-8",
            context: this,
            success: function (response) {

                // List of Events
                var arrayResponse = $.parseJSON(response);

                // Create nodes of each year and month
                var eventsMonth = [];

                if (first) {
                    $("#eventList").empty();
                }

                var arrayResponseLast = arrayResponse[arrayResponse.length - 1];

                var eventYear = eventDateTime.substr(0, 4);
                var eventMonth = eventDateTime.substr(5, 2);
                var lastYear = arrayResponseLast ? arrayResponseLast.Year : new Date().getFullYear();
                var lastMonth = arrayResponseLast ? arrayResponseLast.Month : new Date().getMonth() + 1;

                for (var indexYear = eventYear; indexYear <= lastYear; indexYear++) {
                    var minimumMonth = (indexYear == eventYear) ? eventMonth : 1;
                    var maximumMonth = (indexYear < lastYear) ? 12 : lastMonth;
                    for (var indexMonth = minimumMonth; indexMonth <= maximumMonth; indexMonth++) {
                        var date = new Date(indexYear, indexMonth - 1, 1);
                        var eventsListedId = "eventsListed" + indexYear + pad(indexMonth, 2);
                        if (!$("#" + eventsListedId).length) {
                            eventsMonth.push(
                                "<h4>" +
                                    date.getMonthName().toUpperCase() + " " + date.getFullYear() +
                                "</h4>" +
                                "<ul id='" + eventsListedId + "' class='monthEventsList'>" +
                                "</ul>"
                            );
                        }
                    }
                }

                // Add year-months to list node
                $("#eventList").append(eventsMonth);

                // Loop through each Event and add to the corresponding year and month node
                $.each(arrayResponse, function (index, event) {
                    $("#eventsListed" + event.Year + pad(event.Month, 2)).append(
                        "<li class='eventListed' data-title='" + event.Title + "'>" +
                        "    <div class='date'><p>" + event.Day + "</p></div>" +
                        "    <div class='info'>" +
                        "        <h5>" + event.Title + "</h5>" +
                        "        <span>" + event.DateTime + "</span>" +
                        "        <span>" + event.Location + "</span>" +
                        "        <p>" + event.Description + "</p>" +
                        "    </div>" +
                        "<a href='" + event.Url + "'></a>" +
                        "</li>"
                    );
                });

                // Remove months with no events
                $(".monthEventsList").each(function () {
                    var elem = $(this);
                    if (elem.children().length == 0) {
                        elem.prev("h4").remove();
                        elem.remove();
                    }
                });

                // Set initial number of events to show
                var eventsToShowAtLoad = 0;
                eventsToShowAtLoad += $(".monthEventsList").eq(0).children().length;
                eventsToShowAtLoad += $(".monthEventsList").eq(1).children().length;

                // Show or hide the Show More button
                if (arrayResponse.length == eventsToShowAtLoad) {
                    $("#eventShowMore").show();
                } else {
                    $("#eventShowMore").hide();
                }
              
            },
            error: function () {
                alert('The events could not be loaded.');
            }
        });

    } // End function listEvents()

    function filterListedEvents(event) {

        event.preventDefault();

        var eventSearchInput = $("#eventSearchInput").val().trim();

        if (!eventSearchInput || 0 === eventSearchInput.length || $("#eventSearchInput").data("value") === eventSearchInput) {
            $(".eventListed").show();
        } else {
            $(".eventListed").hide();
            $(".eventListed").filter(function () { return $(this).data("title").toLowerCase().indexOf(eventSearchInput.toLowerCase()) > -1; }).show();
        }

    } // End function filterListedEvents(event)    



    window.setTimeout(showEvents, 0001);
    function showEvents() {
        $("#eventDateTimeFilter").val(today);
        $("#eventDateTimeFilter").blur();
        //$("#eventSearchInput").focus();
        $("#eventSearchInput").blur();
        listEvents(null, true);
       
    }



}); // End $(document).ready(...)

