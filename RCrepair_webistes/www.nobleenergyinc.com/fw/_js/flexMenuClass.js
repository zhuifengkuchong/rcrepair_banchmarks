//---------------------------------------------------------------------------------------------------------
//Menu Class Object
//---------------------------------------------------------------------------------------------------------

var zIndex=1000;
function CreateMenuClass(){
	return new clsMenu();
}

function clsMenu(){
	this.Name="";
	this.RootName="";
	this.hv="|";
	this.ItemCount=0;
	this.MenuClass="";
	this.ItemSeparatorClass="";
	this.sHTML="";
	this.bIsFirst=true;
	this.CreateMenu=CreateMenu;
	this.CreateSubMenu=CreateSubMenu;
	this.OpenSubMenu=OpenSubMenu;
	this.AddItem=AddItem;
	this.MakeItemSeparator=MakeItemSeparator;
	this.WrapMenu=WrapMenu;
	this.CloseMenu=CloseMenu;
	this.CloseSubMenu=CloseSubMenu;
	this.ExpanderArrow="V";
	this.AbsolutePosition=false;
	this.Attributes="";
	
	if(navigator.userAgent.toLowerCase().indexOf("touch")>0)this.Attributes+=" aria-haspopup='true' ";
}
 
function CreateMenu(sName,sDirection,sClass,sExpanderArrow,sItemSeparatorClass){
	this.Name=sName;
	this.RootName=sName;
	this.hv=sDirection;
	this.MenuClass=sClass;
	this.ItemSeparatorClass=sItemSeparatorClass;
	this.ExpanderArrow=sExpanderArrow;
	
	
	if((!this.AbsolutePosition)&&(this.hv=="|")){var sWidth=" width=\"100%\" "}else{var sWidth=""}	
	
	if(this.AbsolutePosition){var sAbs="position:absolute;top:0px;left:0px;visibility:hidden;"}else{var sAbs=""}
	
	this.sHTML+="<table "+this.Attributes+sWidth+" onselectstart=\"return false\" class=\""+ this.MenuClass +"\" style=\"border-collapse:separate;"+sAbs+"\" hv=\""+ 
				sDirection +"\" id="+ sName +" rid="+ this.RootName +" onmousemove=\"pw.doClearHideTimer('"+ sName +"')\" onmouseout=\"pw.doSetHideTimer('"+ sName +"')\" border=0 cellPadding=0 cellSpacing=0>\n";
	if(this.hv=="-"){
		this.sHTML+="<tr><td><table width=\"100%\" onselectstart=\"return false\" class=\""+ this.MenuClass +"\" style=\"border-collapse:separate;margin:0px;padding:0px;border:0px;background:none;\" border=0 cellPadding=0 cellSpacing=0><tr>\n";
	}
}

function CreateSubMenu(sDirection,sLink,sTitle,sClass,sItemClass,sItemClassOver,sItemSeparatorClass,sID){
	var sText="";
	sText=sTitle;
	zIndex+=1;
	
	sName=this.Name + "_" + zIndex;
	if(!sID)sID="it"+sName;

	if (this.hv=="|"){
		//--- expander arrow vertical
		if (this.ExpanderArrow.indexOf("V")>-1){
			if (this.ExpanderArrow.indexOf("C")>-1) { 
				sText="<img class=mnuArrowV width=12 height=12 border=0 align=right src=\"../_icons/pt.gif\"/*tpa=http://www.nobleenergyinc.com/fw/_icons/pt.gif*/>"+ sText;
			}else{
				sText="<img width=10 height=10 border=0 style=\"margin-left:0px;margin-top:2px;padding:0px;\" align=right src=\"../_icons/ico_arrow_right_small.gif\"/*tpa=http://www.nobleenergyinc.com/fw/_icons/ico_arrow_right_small.gif*/>"+ sText;
			}
		}
	}else{
		//--- expander arrow horizontal
		if (this.ExpanderArrow.indexOf("H")>-1) {
			if (this.ExpanderArrow.indexOf("C")>-1) { 
				sText=sText +"&nbsp;<img class=mnuArrowH width=12 height=12 border=0 align=absmiddle src=\"../_icons/pt.gif\"/*tpa=http://www.nobleenergyinc.com/fw/_icons/pt.gif*/>";
			}else{
				sText=sText +"&nbsp;<img width=10 height=10 border=0 align=absmiddle src=\"../_icons/ico_arrow_down_small.gif\"/*tpa=http://www.nobleenergyinc.com/fw/_icons/ico_arrow_down_small.gif*/>";
			}
		}
	}
	
	this.AddItem(sLink,sText,sItemClass,sItemClassOver,sID,sName);

	var rCreateSubMenu=new clsMenu();
	rCreateSubMenu.ExpanderArrow=this.ExpanderArrow;
	rCreateSubMenu.ItemSeparatorClass=sItemSeparatorClass;
	rCreateSubMenu.OpenSubMenu(sName,this.RootName,sDirection,sClass);
	return rCreateSubMenu;
}

function DisableLink(){
	return false;
}

function OpenSubMenu(sName,sRootName,sDirection,sClass){
	this.hv=sDirection;
	this.Name=sName;
	this.RootName=sRootName;
	this.sHTML+="<table "+this.Attributes+" width=150 id="+ this.Name +" hv=\""+ this.hv +"\" rid="+ this.RootName +" class=\""+ sClass +"\" border=0 cellPadding=0 cellSpacing=0 style=\"z-index:"+ 
				zIndex +";position:absolute;top:0px;left:0px;visibility:hidden;border-collapse:separate;\" "+
				" onmousemove=\"pw.doClearHideTimer('"+ this.Name +"')\" onmouseout=\"pw.doSetHideTimer('"+ this.Name +"')\" >\n";
	if(this.hv=="-") {this.sHTML+="<tr>\n"}
}

function AddItem(sLink,sTitle,sClass,sClassOver,sID,sSubName){

	var sTarget="";
	var aLink=sLink.split("^*^");sLink=aLink[0];if(aLink.length>1){sTarget=" target=\""+aLink[1]+"\" "} else {if(document.all) sTarget=" target=\"_parent\" "};
	if(sID){var strID=" id="+sID+" "}else{var strID=""}
	if(!sSubName)sSubName="";
	var sText=sTitle;
	this.ItemCount+=1;
	this.MakeItemSeparator(this.ItemSeparatorClass);
	if(this.hv=="|") {this.sHTML+="<tr"+strID+">\n";strID="";}
	if(sSubName){var MobileClick="pw.doEnableLink('"+sID+"',false);"}else{var MobileClick=""}

	if(sLink.indexOf("onclick:")==0){
		this.sHTML+="<td"+this.Attributes+strID+" mid="+ this.Name +" onmouseover=\"pw.doMouseOverItem(this,'"+ sClassOver +"','"+ sSubName +"')\" class=\""+ sClass +"\" style=\"cursor:pointer\" onClick=\"pw."+ sLink.substr(8) +"\">"+ sText +"</td>\n";
	}else{
		this.sHTML+="<td"+this.Attributes+strID+" mid="+ this.Name +" onmouseover=\""+MobileClick+"pw.doMouseOverItem(this,'"+ sClassOver +"','"+ sSubName +"')\" class=\""+ sClass +"\"><a href=\""+ sLink +"\" "+sTarget+"><div>"+ sText +"</div></a></td>\n";
	}
 	if(this.hv=="|") {this.sHTML+="</tr>\n"}
}

//default separator
function MakeItemSeparator(sClass,sID){
	if(sID){sID=" id="+sID+" "}else{sID=""}

 	//make default item separator
	if(!this.bIsFirst) {
		if(this.hv=="-") {
	 		this.sHTML+="<td"+sID+" height=100% valign=middle class=\""+ sClass +"\" style=\"margin:0px;padding:0px;border:0px;\"><img style=\"display:none;\" width=\"1\" height=\"100%\" alt=\"\" src=\"../_icons/pt.gif\"/*tpa=http://www.nobleenergyinc.com/fw/_icons/pt.gif*/></td>";
		}else{
	 		this.sHTML+="<tr"+sID+" class=\""+ sClass +"\" style=\"margin:0px;padding:0px;border:0px;\"><td align=center><img style=\"display:none;\" width=\"100%\" height=\"1\" alt=\"\" src=\"../_icons/pt.gif\"/*tpa=http://www.nobleenergyinc.com/fw/_icons/pt.gif*/></td></tr>";
		}
	}else{
		this.bIsFirst=false;
	}
}

function WrapMenu(){
 	if(this.hv=="-") {
 		this.sHTML+="</tr></table><table width=\"100%\" onselectstart=\"return false\" class=\""+ this.MenuClass +"\" style=\"border-collapse:separate;margin:0px;padding:0px;border:0px;background:none;\" border=0 cellPadding=0 cellSpacing=0><tr>\n";
		this.bIsFirst=true;
	}
}
 
function CloseMenu(){
	if(this.hv=="-") {this.sHTML+="</tr></table></td></tr>\n"}
	this.sHTML +="</table>\n";
	return this.sHTML;
}

function CloseSubMenu(){
	if(this.hv=="-") {this.sHTML+="</tr>\n"}
	this.sHTML+="</table>\n";
	return this.sHTML;
}
