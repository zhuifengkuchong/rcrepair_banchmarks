//---------------------------------------------------------------------------------------------------------
// flexMenu V8
// JavaScript DHTML Hierarchical Menu Class 
// Currently supported browsers: 
// IE5+, Mozilla/5 (e.g. Netscape 6+, FireBird, FireFox), Safari, Camino, Konqueror, Opera 
// 
// (C) FlexScape, LLC, Torsten Heinze 1999-2007, All rights reserved.
//
// Copying or using this code (or parts of it) without explicit permission by FlexScape, LLC is prohibited 
// and will result in proscecution and severe penalties.
//---------------------------------------------------------------------------------------------------------
var timers=new Array();
var currentRID=null; //current selected root menu
var currentMNU=null; //current selected root menu
var usePopContainer=false;
//parent window containing this script (e.g. for IFRAME)
var pw=self;
//var mnuSlideDelay=10;
var ua=navigator.userAgent;

var b_Pattern="android|ipad|iphone|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|IEMobile|msiemobile|MSIEMobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino"
var v_Pattern="1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-"
var b_Regexp=new RegExp(b_Pattern,"ig");
var v_Regexp=new RegExp(v_Pattern,"ig");
var isMobile= ((b_Regexp.test(ua))||(v_Regexp.test(ua.substr(ua.length-4))));
//console.log(ua+":"+isMobile);

//IE only --------------------------------------------------------------------------
if(document.all){var checkedForControls=false;document.onreadystatechange=checkForControls;}else{var checkedForControls=true;}
function checkForControls(){
	if(document.body&&(ua.indexOf("MSIE")>1)){
		if(document.body.readyState=="complete"){
			var rv = 0;
			var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
			if (re.exec(ua) != null)rv = parseFloat( RegExp.$1 );
			
			if(rv>=8.0) return;
			if(checkForControls&&usePopContainer) return;

			usePopContainer=(document.body.getElementsByTagName("IFRAME").length+
							 document.body.getElementsByTagName("SELECT").length+
							 document.body.getElementsByTagName("OBJECT").length+
							 (typeof(window.flexWriteActiveX)!="undefined"))>0;

			//if(usePopContainer)console.log(" POP CONTAINER "); //DEBUG
			checkedForControls=true;
		}
	}
}
//-----------------------------------------------------------------------------------

function doFalse(){
	return false;
}

function doLink(el,sLink){
	if((!sLink)||(sLink=="")){return}
	if(el.isdisabled) return;
	if(sLink.indexOf("javascript:")>-1){ 
		eval(sLink);
	}else{
		pw.location.href=sLink;
	}
}

function doEnableLink(sID,bEnableLink){
	if(!sID)return;
	var oTD=document.getElementById(sID);
	if(!oTD)return;
	if(oTD.tagName=="TR"){
		for (var c=0;c<oTD.childNodes.length;c++){
			if(oTD.childNodes[c].tagName=="TD"){oTD=oTD.childNodes[c];break;}
		}
	}

	var oLink=oTD.childNodes[0];
	if(!oLink)return;
	
	if(bEnableLink){
		oLink.onclick = null;
	}else{
   		oLink.onclick = DisableLink;
   		iTimeout=(isMobile)?500:0;
		setTimeout("doEnableLink('"+sID+"',true)",iTimeout)
	}
}

function doMouseOverItem(el,sOverClass,subid){
	if(!checkedForControls) checkForControls();
	doSelect(el,sOverClass,subid);
	el.onselectstart=doFalse;
}

function doSelect(el,sOverClass,subid){
	if(!el){return}
	var mid=el.getAttribute("mid");
	var mnu=document.getElementById(mid);
	//window.status+=mnu;
	if(!mnu) return;
	var rid=mnu.getAttribute("rid");
	
	//if moved to another menu, then hide last menu
	if(currentRID) if(currentRID!=rid) unSelect(document.getElementById(currentRID));		
	currentRID=rid;
	if(el!=mnu.current){
		unSelect(mnu);
		el.classNameOrg=el.className;
		el.className=sOverClass;
		mnu.current=el;
	}else{
		doClearHideTimer(mid);
		return;
	}
	
	if(typeof(subid)!="undefined") doShowSubMenu(el,subid);
}

function unSelect(mnu){
	doHideSubMenuChildren(mnu);
	resetClass(mnu);
}

function resetClass(mnu){
	if(mnu.current){
		mnu.current.className=mnu.current.classNameOrg;
		mnu.current=null
	}
}

function doShowMenu(id,x,y){
	var mnu=document.getElementById(id);	//the menu
	if(!mnu){return}
	var mhv=mnu.getAttribute("hv");	
	if(usePopContainer) mnu=doCreateContainer(id,id,mhv,mnu)
	mnu.style.left=x+"px";
	mnu.style.top=y+"px";
	mnu.style.visibility="visible";
	currentMNU=id;
}

function doShowSubMenu(el,id){
	if(!id){return}
	var mnu=document.getElementById(id);	//the (sub) menu
	if(!mnu){return}

	var mid=el.getAttribute("mid");
	
	var parentMenu=document.getElementById(mid); 		// parent menu
	var rid = mnu.getAttribute("rid");					// root menu id
	
	var mhv=mnu.getAttribute("hv");						// direction of menu (horizontal or vertical)
	var phv=parentMenu.getAttribute("hv");				// direction of parent menu (horizontal or vertical)
	
	var w=mnu.offsetWidth;
	var h=mnu.offsetHeight;
	
	//determine positioning parent
	if(mhv=="|"){
		var pel=el;
		//make it look better if parent item width > submenu width
		if((phv=="-")&&(pel.offsetWidth>(w))){mnu.style.width=pel.offsetWidth;w=pel.offsetWidth;}
	}else{
		var pel=parentMenu;
		//horizontal, so same width as parentMenu
		mnu.style.width=parentMenu.offsetWidth;
	}
	
	bRelativeContainer=false;

	//determine new position based on parent
	if(phv=="|"){
		var x=mnuGetLeft(pel)+pel.offsetWidth;
		var y=mnuGetTop(pel);
	}else{
		var x=mnuGetLeft(pel);
		var y=mnuGetTop(pel)+ pel.offsetHeight;
	}

	if((typeof(parentMenu)!="undefined")||(parentMenu.style.position=="absolute")){
		if(typeof(parentMenu.w)!="undefined"){
		var parw=parentMenu.w;
		var parh=parentMenu.h;
		}else{
		var parw=parentMenu.offsetWidth;
		var parh=parentMenu.offsetHeight;
		}
	}else{
		var parw=0;
		var parh=0;
	}	

	var w=mnu.offsetWidth;
	var h=mnu.offsetHeight;
	
	//for vertical menus, adjust scrolling and body width, so it fits always in visible window
	if(mhv=="|"){
		if((parentMenu.className.indexOf("_LEFT")>0)||((x+w)>=(document.body.clientWidth+document.body.scrollLeft))){
			if(phv!="-"){
				x-=w+parw-2;
			}else{
				x=document.body.clientWidth+document.body.scrollLeft-w;
			}
			//window.status=phv+":"+w+":"+parw+":"+x+":"+document.body.clientWidth+":"+document.body.scrollLeft;
		}
		
		//if((parentMenu.className.indexOf("_TOP")>0)||((y+h)>=(document.body.clientHeight+document.body.scrollTop))){
		if(parentMenu.className.indexOf("_TOP")>0){
			if(phv!="|"){
				y-=h+parh-2;
			}else{
				y=document.body.clientHeight+document.body.scrollTop-h-2;
			}
			//window.status=phv+":"+h+":"+parh+":"+y+":"+document.body.clientTop+":"+document.body.scrollTop;
		}
		
	}

	if(usePopContainer){
		//1st level sub menu is an IFRAME so apply correct position to 2nd level one
		//since el in sub menu 1 is now positioned relative to it's IFRAME document, 
		//must add parentMenu offset (only if vertical)
		if((mhv=="|")&&(parentMenu.style.position=="absolute")){
	 			x+=parentMenu.offsetLeft;
	 			y+=parentMenu.offsetTop;
	 	}

	 	mnu=doCreateContainer(id,rid,mhv,mnu)
	}
	
	//console.log(usePopContainer+":"+x+":"+y);
	mnu.style.left=x+"px";
	mnu.style.top=y+"px";
	mnu.style.visibility="visible";
	parentMenu.childMenu=mnu;	//link with parent
	mnu.parentMenu=parentMenu;
	
	if(typeof(mnuSlideDelay)!="undefined"){
		mnu.style.clip="rect(0px,"+w+"px,0px,0px)";
		mnu.style.opacity=0;mnu.style.KhtmlOpacity=0;mnu.style.MozOpacity=0;
  		mnu.style.filter = 'alpha(opacity=0)';
  		mnu.maxwidth=w;
  		mnu.maxheight=h;
		mnu.currentheight=0;
		doMenuSlide(id,-1);
		mnu.slideid=setInterval("doMenuSlide('"+id+"',1)",15);
	} 

}

function doMenuSlide(id,dir){
	var mnu=document.getElementById(id);
	var w=mnu.maxwidth;
	var h=mnu.maxheight;
	var i=mnu.currentheight;

	i=i+dir*(Math.round((h - i) / mnuSlideDelay)+10);
	var ip=Math.round(i * 100 / h);if(ip>100)ip=100;

	if(i>=h){
		clearInterval(mnu.slideid);i=h;ip=100;
	}else if(i<=0){
		clearInterval(mnu.slideid);i=0;ip=0;
	}
	
	//window.status=i;
	mnu.style.opacity=ip/100;mnu.style.KhtmlOpacity=ip/100;mnu.style.MozOpacity=ip/100;
	mnu.style.filter = 'alpha(opacity=' + ip+ ')';
	mnu.style.clip="rect(0px,"+w+"px,"+i+"px,0px)";
	mnu.currentheight=i;
}


//IE4+ and ActiveX, Frames, Flash put everything in IFRAME now, so zIndex is correct
//tricky: move existing mnu (outer TABLE) into a new created IFRAME
function doCreateContainer(id,rid,mhv,mnu){
	//already created?
	if(mnu.tagName=="IFRAME"){
		//in case size changed
		var mnuDoc=document.frames[id];
		if(mnuDoc){
			mnuX=mnuDoc.document.getElementById(id);
			mnu.style.width=mnuX.offsetWidth;
			mnu.style.height=mnuX.offsetHeight;
		}
		return mnu;
	}

	//remember properties of mnu
	var mnu_margin=mnu.currentStyle.margin;
	mnu.style.margin="0px 0px 0px 0px";
	var sMenuHTML=mnu.outerHTML;
	var w=mnu.offsetWidth;
	var h=mnu.offsetHeight;
	//destroy mnu original, because IFRAME will become new mnu
	mnu.outerHTML="";

	// remove inline styles for A tags (added by IE, while fetching innerHTML)
	var re = new RegExp("<A style=\"([^\"]*)\"([^>]*)","ig");
	sMenuHTML = sMenuHTML.replace(re, "<A $2");

	//IFRAME container
	var sHTML='<IFRAME id="'+id+'" src="javascript:false" style="background:none;position:absolute;border:0px;padding:0px;margin:'+mnu_margin+';top:0px;left:0px;width:'+w+'px;height:'+h+'px;visibility:hidden;" frameborder=0 scrolling=no allowTransparency="true"></IFRAME>';
	document.body.insertAdjacentHTML("AfterBegin",sHTML);
	var sty="";
	//insert styles into iframe as well
	for (var i=0; i<document.styleSheets.length;i++){
		var styleSrc=document.styleSheets[i].href;
		if((styleSrc)&&(styleSrc!="")) sty+='<link REL="stylesheet" TYPE="text/css" HREF="'+styleSrc+'">\n';
	}
	sty+="<style type=\"text/css\">\n";
	sty+="#"+mnu.id+" A {display:block;cursor:pointer;border:0;padding:0;margin:0;text-decoration:expression(this.parentNode.currentStyle.textDecoration);text-decoration:inherit;color:expression(this.parentNode.currentStyle.color);color:inherit}\n";
	sty+="#"+mnu.id+" A:HOVER {display:block;cursor:pointer;border:0;padding:0;margin:0;text-decoration:expression(this.parentNode.currentStyle.textDecoration);text-decoration:inherit;color:expression(this.parentNode.currentStyle.color);color:inherit}\n";
	sty+="</style>\n";

	var scr='<script language="JavaScript">var pw=parent;onload=doShow;function doShow(){'+id+'.style.visibility="visible";'+id+'.onmousemove=null;'+id+'.onmouseout=null;};setTimeout("doShow()",10);</script>';
	var t=eval(id+".document");
	t.open();
	t.writeln(sty+scr+'\n<body style="background:none;" id=ifrmnu topmargin=0 leftmargin=0 rightmargin=0 marginheight=0 marginwidth=0 onmouseover="pw.doClearHideTimer(\''+id+'\')" onmousemove="pw.doClearHideTimer(\''+id+'\')" onmouseout="pw.doSetHideTimer(\''+id+'\')">'+sMenuHTML+'</body>');
	t.close();
	var cnt=document.getElementById(id);

	cnt.rid=rid;
	cnt.hv=mhv;
	mnu=cnt;
	return mnu;
} 
	
function doClearHideTimer(id){
	var mnu=document.getElementById(id);
	if(!mnu) return;
	var rid=mnu.getAttribute("rid");
	//window.status=mnu.id+":"+rid+" hide timer cleared";
	if(timers[rid]){clearTimeout(timers[rid]);timers[rid]=null}
}

function doSetHideTimer(id){
	var mnu=document.getElementById(id);
	if(!mnu) return;
	var rid=mnu.getAttribute("rid");
	doClearHideTimer(id);
	timers[rid]=setTimeout("doHideSubMenuRecurse('"+id+"')",450);
}
	
function doHideSubMenuChildren(mnu){
	if(!mnu) return;
	//close all children
	while(mnu.childMenu){
		resetClass(mnu.childMenu);
		pot=document.getElementById(mnu.childMenu.id);
		if(pot){
			if(typeof(pot.slideid)!="undefined"){
				clearInterval(pot.slideid);pot.slideid=setInterval("doMenuSlide('"+pot.id+"',-1)",15);
			}else{
				pot.style.visibility="hidden";
			}
		}

		closedMenu=mnu;
		mnu=mnu.childMenu;
		closedMenu.childMenu=null;
	}
	
}
 
function doHideSubMenuRecurse(id){
	var mnu=document.getElementById(id);
	doHideSubMenuChildren(mnu);
	if(mnu.childMenu){return}
	//close menus all the way up to root-parent
	while(mnu){
		resetClass(mnu);
		mnu.childMenu=null; //unlink
		pot=document.getElementById(mnu.id);

		if(pot){
		 if(pot.style.position=="absolute"){
			if(typeof(pot.slideid)!="undefined"){
				clearInterval(pot.slideid);pot.slideid=setInterval("doMenuSlide('"+pot.id+"',-1)",15);
			}else{
				pot.style.visibility="hidden";
			}
		 }
		}

		mnu=mnu.parentMenu;
	}
}
	 
/* ------------- get Element Absolute Left ------------------------------------------------------ */
function mnuGetLeft(el) {
	var x=0;
	while (el!= null){
		if(el.tagName!="TR"){
			var bWidth=0;if(el.currentStyle)if(el.className.indexOf("mnu")!=0)if(el.currentStyle.borderLeftWidth)bWidth=parseInt(el.currentStyle.borderLeftWidth);
			if(isNaN(bWidth)) bWidth=0;
			var sPosition=mnuGetStyle(el,"position");
			if(ua.indexOf("MSIE")<0)if(el.tagName=="TABLE")sPosition="";
			if(sPosition=="relative"){
				x += bWidth;
			}else{
				x += el.offsetLeft+bWidth;
				//console.log(sPosition+":"+el.tagName+":"+el.id+":"+el.className+":"+el.offsetLeft+":"+bWidth);
			}			
		}
		el = el.offsetParent;
	}
	return x;
}

/* ------------- get Element Absolute Top	------------------------------------------------------ */
function mnuGetTop(el) {
	var y=0;
	var pel=el.offsetParent;
	if(pel.currentStyle)if(pel.currentStyle.borderTopWidth) y=parseInt(pel.currentStyle.borderTopWidth);
	if(isNaN(y)) y=0;
	y= -1 * y;

	while (el!= null) {
		if(el.tagName!="TR"){
			var bWidth=0;if(el.currentStyle)if(el.className.indexOf("mnu")!=0)if(el.currentStyle.borderTopWidth)bWidth=parseInt(el.currentStyle.borderTopWidth);
			if(isNaN(bWidth)) bWidth=0;
			var sPosition=mnuGetStyle(el,"position");
			if(ua.indexOf("MSIE")<0)if(el.tagName=="TABLE")sPosition="";
			//if(bWidth>0) console.log(el.tagName+":"+el.className+":"+bWidth);
			if(sPosition=="relative"){
				y += bWidth;
			}else{
				y += el.offsetTop+bWidth;
			}	
		};
		el = el.offsetParent;
	}
	
	//console.log(y);
	return y;
}

//only for MakeMenuVerticalList Navigation Module ---------------------------------------------
var openListItems=new Array();
function toggleListMenu(id,depth,bShow){
	if(!document.getElementById) return;
	
	if(document.getElementById(openListItems[depth])){
	 	if(openListItems[depth]!=id) document.getElementById(openListItems[depth]).style.display="none";
	}
	
	if(document.getElementById(id)){
		if(bShow){
			document.getElementById(id).style.display="";
			openListItems[depth]=id;
		}else{
	 		if(document.getElementById(id).style.display=="none"){
				document.getElementById(id).style.display="";
				openListItems[depth]=id;
			}else{
				document.getElementById(id).style.display="none";
			}
		}
	}
}

function mnuGetStyle(el, prop) {
  var y;
  if (el.currentStyle) 
    y = el.currentStyle[prop];
  else if (window.getComputedStyle)
    y = window.getComputedStyle( el, '').getPropertyValue(prop);
  return y;
}
