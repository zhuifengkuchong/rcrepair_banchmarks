function playWePutPeopleToWork(){
	if ( !m_abBannerReady[m_nCurrentBanner]) setBannerReady(m_nCurrentBanner);

	//console.log("playing banner5");
	TweenMax.to($('.bnr_WePutPeopleToWork div.bg1'), 1.0, {css:{autoAlpha:1}});
	TweenMax.to($('.bnr_WePutPeopleToWork .frame1'), 0.5, {css:{autoAlpha:1}, delay:1.0});
	
	TweenMax.to($('.bnr_WePutPeopleToWork div.bg2'), 2, {css:{autoAlpha:0.66}, delay:4.0});
	TweenMax.to($('.bnr_WePutPeopleToWork .frame1'), 2, {css:{autoAlpha:0}, delay:4.5, onComplete: frame2});	
	
	function frame2(){
		//console.log("starting frame 2 sequence!");
		TweenMax.to($('.bnr_WePutPeopleToWork .frame2'), 2, {css:{autoAlpha:1}});
	}
}

function stopWePutPeopleToWork(){
	//console.log("stopping banner5");
	TweenMax.killAll();
}

function resetWePutPeopleToWork(){
	//console.log("resetting banner5");
	TweenMax.set($('.bnr_WePutPeopleToWork div.bg1'), {css:{autoAlpha:0}});
	TweenMax.set($('.bnr_WePutPeopleToWork div.bg2'), {css:{autoAlpha:0}});
	//TweenMax.set($('.bnr_WePutPeopleToWork .frame1').children(), {css:{autoAlpha:0}});
	TweenMax.set($('.bnr_WePutPeopleToWork .frame1'), {css:{autoAlpha:0}});
	TweenMax.set($('.bnr_WePutPeopleToWork .frame2'), {css:{autoAlpha:0}});
}
