var __BannerTimer = null;
var homebanner = {
    effect: $(".banner_effect").text().toLowerCase(),
    slide_effect_timing: $(".sl_timing").text(),
    slide_effect_slidetiming: $(".sl_slidetiming").text(),
    slide_effect_macrotiming: $(".sl_fademacrotiming").text(),
    slide_effect_texttiming: $(".sl_fadetiming").text(),
    fade_effect_timing: $(".fd_timing").text(),
    fade_effect_fadetiming: $(".fd_fadetiming").text(),
    fade_effect_macrotiming: $(".fd_slidemacrotiming").text(),
    fade_effect_texttiming: $(".fd_slidetiming").text(),
    loading: $(".home_banner_loading"),
    previousbanner: 0,
    currentbanner: 0,
    nextbanner: 1,
    bannercount: 0,
    loadnav: function () {
        $(".banner_navigation").empty();
        for (i = 0; i < homebanner.bannercount; i++) {
            if (i == homebanner.currentbanner) {
                $(".banner_navigation").append("<div class='nav_item_active' id='banner_nav" + i + "'>&nbsp;<!-- IE Fix --></div>");
            } else {
                $(".banner_navigation").append("<div class='nav_item_base' id='nav" + i + "'>&nbsp;<!-- IE Fix --></div>");
            }
        }
        $(".banner_navigation div").mouseenter(function () {
            if (!$(this).hasClass("nav_item_active")) {
                $(this).removeClass("nav_item_base");
                $(this).addClass("nav_item_hover");
            }
        }).mouseleave(function () {
            if (!$(this).hasClass("nav_item_active")) {
                $(this).removeClass("nav_item_hover");
                $(this).addClass("nav_item_base");
            }
        }).click(function () {
            if (!$(this).hasClass("nav_item_active")) {
                //alert($(this).attr("id").match(/\d+/g));
                homebanner.banner_timer("stop");
                homebanner.nextbanner = $(this).attr("id").match(/\d+/g);
                //TrackEvent("HP-slider-Nav-Click", "click", $(".banner" + homebanner.nextbanner + " .home_banner_content .banner_text_content .banner_title").text(), "0");
                if (homebanner.effect == "slide") { homebanner.slidebanner(); } else { homebanner.fadebanner(); }
            }
        });
        $(".home_banner_content .banner_text_content .banner_link").each(function () {
            $(this).mouseenter(function () {
                $(".banner_button", this).css("background-color", "#333333");
            }).mouseleave(function () {
                $(".banner_button", this).css("background-color", "#1c242c");
     
            });
        });

    },
    banner_timer: function (action) {
        if (action == "start") {
            switch (homebanner.effect) {
                case "slide":
                    __BannerTimer = setInterval("homebanner.slidebanner()", parseInt(homebanner.slide_effect_timing));
                    break;
                default:
                    __BannerTimer = setInterval("homebanner.fadebanner()", parseInt(homebanner.fade_effect_timing));
            }
        } else {
            try {
                clearInterval(__BannerTimer);
            } catch (er) {
                //Do nothing with the error.
            }
            __BannerTimer = null;
        }
    },
    slidebanner: function () {
        // This is the function that slides the background image from left to right
        $(".home_banner_wrapper").find(".banner" + homebanner.nextbanner).css("left", "825px");
        $(".home_banner_wrapper").find(".banner" + homebanner.nextbanner).show();
        $(".home_banner_wrapper").find(".banner" + homebanner.currentbanner).animate({ left: "-825px" }, { "queue": false, "duration": parseInt(homebanner.slide_effect_slidetiming), 'easing': 'linear', complete: function () {
            $('.home_banner_wrapper').find('.banner' + homebanner.previousbanner).hide();
            homebanner.resetslidebanner();
            homebanner.fadecontent();
        }
        });
        $(".home_banner_wrapper").find(".banner" + homebanner.nextbanner).animate({ left: "0px" }, { "queue": false, "duration": parseInt(homebanner.slide_effect_slidetiming), 'easing': 'linear' });
        homebanner.previousbanner = homebanner.currentbanner;
        homebanner.currentbanner = homebanner.nextbanner;
        homebanner.nextbanner = parseInt(homebanner.nextbanner) + 1 <= homebanner.bannercount - 1 ? parseInt(homebanner.nextbanner) + 1 : 0;
        homebanner.loadnav();
    },
    fadecontent: function () {
        // This is the function that fades in the content on the banner
        $(".home_banner_wrapper").find(".banner" + homebanner.currentbanner + " .home_banner_content .banner_macro").animate({ opacity: 1 }, { "queue": false, "duration": parseInt(homebanner.slide_effect_macrotiming), 'easing': 'linear', complete: function () {
            $(".home_banner_wrapper").find(".banner" + homebanner.currentbanner + " .home_banner_content .banner_text_content").animate({ opacity: 1 }, { "queue": false, "duration": parseInt(homebanner.slide_effect_texttiming), 'easing': 'linear' });
        }
        });
    },
    resetslidebanner: function () {
        // This is the function that hides the content on the silde so it can be faded in.
        $(".home_banner_wrapper").find(".banner" + homebanner.previousbanner + " .home_banner_content .banner_macro").animate({ opacity: 0 }, { "queue": false, "duration": 100, 'easing': 'linear' });
        $(".home_banner_wrapper").find(".banner" + homebanner.previousbanner + " .home_banner_content .banner_text_content").animate({ opacity: 0 }, { "queue": false, "duration": 100, 'easing': 'linear' });
    },
    fadebanner: function () {
        $(".home_banner_wrapper").find(".banner" + homebanner.currentbanner).animate({ opacity: 0 }, { "queue": false, "duration": parseInt(homebanner.fade_effect_fadetiming), 'easing': 'linear', complete: function () {
            $('.home_banner_wrapper').find('.banner' + homebanner.previousbanner).hide();
            homebanner.resetfadebanner();
            homebanner.slidecontent();
            homebanner.loadnav();
        }
        });
        $(".home_banner_wrapper").find(".banner" + homebanner.nextbanner).show();
        $(".home_banner_wrapper").find(".banner" + homebanner.nextbanner).animate({ opacity: 1 }, { "queue": false, "duration": parseInt(homebanner.fade_effect_fadetiming), 'easing': 'linear' });
        homebanner.previousbanner = homebanner.currentbanner;
        homebanner.currentbanner = homebanner.nextbanner;
        homebanner.nextbanner = parseInt(homebanner.nextbanner) + 1 <= homebanner.bannercount - 1 ? parseInt(homebanner.nextbanner) + 1 : 0;
    },
    resetfadebanner: function () {
        // This is the function moves the content on the banner so it can be slid in.
        $(".home_banner_wrapper").find(".banner" + homebanner.previousbanner + " .home_banner_content .banner_macro").css("left", "825px");
        $(".home_banner_wrapper").find(".banner" + homebanner.previousbanner + " .home_banner_content .banner_text_content").css("left", "825px");
    },
    slidecontent: function () {
        $(".home_banner_wrapper").find(".banner" + homebanner.currentbanner + " .home_banner_content .banner_macro").animate({ left: "625px" }, { "queue": false, "duration": parseInt(homebanner.fade_effect_texttiming), 'easing': 'linear', complete: function () {
            $(".home_banner_wrapper").find(".banner" + homebanner.currentbanner + " .home_banner_content .banner_text_content").animate({ left: "465px"}, { "queue": false, "duration": parseInt(homebanner.fade_effect_texttiming), 'easing': 'linear' });
        }
        });
    },
    init: function () {
        var ctr = 0;
        $(".home_banner").each(function () {
            $(this).addClass("banner" + ctr);
            if (ctr != 0) {
                $(this).hide();
                if (homebanner.effect != "slide") {
                    $(this).animate({ opacity: 0 }, { "queue": false, "duration": 100, 'easing': 'linear' });
                }
            }
            if (homebanner.effect == "slide") {
                $(".home_banner_wrapper").find(".banner" + ctr + " .home_banner_content .banner_macro").animate({ opacity: 0 }, { "queue": false, "duration": 100, 'easing': 'linear' });
                $(".home_banner_wrapper").find(".banner" + ctr + " .home_banner_content .banner_text_content").animate({ opacity: 0 }, { "queue": false, "duration": 100, 'easing': 'linear' });
            } else {
                $(".home_banner_wrapper").find(".banner" + ctr + " .home_banner_content .banner_macro").css("left", "825px");
                $(".home_banner_wrapper").find(".banner" + ctr + " .home_banner_content .banner_text_content").css("left", "825px");
            }
            ctr++;
        });
        homebanner.bannercount = ctr;
    }
}

$(document).ready(function () 
{
    homebanner.init();
    if(homebanner.bannercount==1){return true; }
    else{ homebanner.loadnav();  }
    
});
$(window).ready(function () {
    homebanner.loading.animate({ opacity: 0 }, { "queue": false, "duration": 1000, 'easing': 'linear', complete: function () 
    {
        if (homebanner.effect == "slide") { homebanner.fadecontent(); } else { homebanner.slidecontent(); }
        
        if(homebanner.bannercount == 1) { homebanner.banner_timer("stop"); }
        else { homebanner.banner_timer("start"); }
        homebanner.loading.hide();
    }
    });
});

function TrackEvent(category, verb_actions, label, bln_noninteraction) {
    try {
        //staging
         //var pageTracker = _gat._getTracker("UA-12340078-22");
         var pageTracker = _gat._createTracker("UA-32049141-1");
        _gaq.push(['_trackEvent', category, verb_actions, label, bln_noninteraction]);
    } catch (e) {
        alert(e);
    }
}

/*Start Honeywell Event Tracking System*/
jQuery(document).ready(function() 
{

     jQuery('.home_banner_content .banner_text_content .banner_title a').click(function(event) 
     {
      recordLinkEvent(this, 'Hon-HP-slider-Headline-Link', document.URL, event);

     });
	
	 jQuery('.home_banner_content .banner_text_content .banner_link').click(function(event) 
     {
     recordLinkEvent(this, 'Hon-HP-slider-CTA-Button', document.URL, event);

     });
});


/*End Honeywell Event Tracking System*/