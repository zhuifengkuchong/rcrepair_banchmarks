var urlparts = _hw_baseURL.split('//');
var protocolBaseURL = ('https:' == document.location.protocol ? 'https://' : 'http://') +urlparts[1];

/*****************************************************************************/
/*Variable   : _hw_sFooter_Div_YearHoneywellInternationalInc  */
/*Description: This variable is assigned with dynamically change the year based on the present year */
/*****************************************************************************/

var splitYear=_hw_sFooter_Div_YearHoneywellInternationalInc.substr(0,4);

var splitText=_hw_sFooter_Div_YearHoneywellInternationalInc.substr(4);

var isNumber =  /^\d+$/.test(splitYear);

if(isNumber==true)
{
var date = new Date();
var currentYear = date.getFullYear();

_hw_sFooter_Div_YearHoneywellInternationalInc=currentYear+splitText;


}

/*****************************************************************************/
/*Plugin  : Create */
/*Description: adds functionality to Jquery to add a element in a nicer fashion */
/*****************************************************************************/
(function($){
     $.fn.create = function(element, attributes){     
     return $(document.createElement(element)).attr(attributes);
    };
    })(jQuery);

/*****************************************************************************/
/*Function   : HW_ToggleDiv  */
/*Description: This function hides and shows sitemap on click */
/*****************************************************************************/

var bSiteMapOpen_Flag = 0;
function HW_ToggleDiv(){
    //Creating Sitemap on Click of the Sitemap button
    if(bSiteMapOpen_Flag == 0)
    {
        HW_CreateSiteMap();
    }
    bSiteMapOpen_Flag = 1;
    var sDivId ='div1';
    if(document.getElementById(sDivId).style.display == 'none')
    {
      document.getElementById("sitemap").style.display='none';
      document.getElementById(sDivId).style.display = 'block';   
      document.getElementById("sitemap1").style.display='inline';    

    }else{
    document.getElementById("sitemap").style.display='inline';
    document.getElementById(sDivId).style.display = 'none';
    document.getElementById("sitemap1").style.display='none';   
    }
  }

/*****************************************************************************/
/*Function   : HW_ChangeGoImage  */
/*Description: Changes the search button image on mouse over */ 
/*****************************************************************************/     
function HW_ChangeGoImage(psImagePath,piFlag)
{
     if(piFlag==1)
         psImagePath.src = ""+_hw_baseURL+"/_layouts/InternetFramework/Images/submitbutton_mouseover.jpg";
     else
         psImagePath.src = ""+_hw_baseURL+"/_layouts/InternetFramework/Images/submitbutton_normal.jpg";
}
/*****************************************************************************/
/*Function   : HW_CreateMenu() */
/*Description: Creates the menu on document ready */ 
/*****************************************************************************/
function HW_CreateMenu()
{
	     
							 
	 var sMenu_Color_A81C24	= '#EE3124';					 
	 var sMenu_Color_DFDFDF = '#E7E7E7';
	 var sMenu_Color_7E7E7E	= '#7E7E7E';
	 var sMenu_Color_FFFFFF = '#FFFFFF';
	 var sMenu_Color_CFCFCF = '#CFCFCF';
	 var sMenu_Color_B3B3B3 = '#b3b3b3';
	 var sMenu_Color_E5E5E5 = '#e5e5e5';    



    var hw_logo_html;

    if(typeof _hw_headerlogo_image !='undefined')
    {
        
        if(typeof _hw_headerlogo_link !='undefined')
        {
            hw_logo_html= "<a href="+ _hw_headerlogo_link +" ><image src="+ _hw_headerlogo_image  +"/></a>";
            
        }
        else
        {
            hw_logo_html= "<image src="+ _hw_headerlogo_image  +"/>";
        }
    }
    else
    {
        hw_header_logo =  protocolBaseURL+'/_layouts/InternetFramework/Images/honeywell_logo.png'; 
        hw_logo_html = "<a href='../../../Pages/Home.aspx.htm'/*tpa=http://honeywell.com/*/><image src='" + protocolBaseURL + "/_layouts/InternetFramework/Images/honeywell_logo.png'/></a>";
    }
    


		//defining logo, and right hand side links					 
	 var oHeaderHTML= $(this).create('div', {'id':'header'})
                       .append($(this).create('div', {'class':'hw-header-t-space'}).html("&nbsp;"))
                       .append($(this).create('div', {'class':'hw-logo'})
                               .append(hw_logo_html)
                                );
                                
                                
                               
    var headerhtml=$(this).create('div', {'class':'hw-top_rht_area hw-top_link'});
    var count=0;
    $.each(_hw_header_right,function(k,v){
    count=count+1;
    });                               
                                
                                    var countinside=0;
                                    $.each(_hw_header_right,function(k,v){
                                    
                                    countinside++;                                   
                                    if(countinside==count)
                                    {
                                       if(v[1]==true && v[2]==true)
                                       {
                                       var imageHtml=$(this).create('a',{'href':v[0],'id':'idWorldWide','target':'_blank'}).html(k);
                                       imageHtml.append('&nbsp;&nbsp;');
                                       imageHtml.append($(this).create('img', {'src':''+protocolBaseURL+'/_layouts/InternetFramework/Images/world_map.gif','style':'vertical-align:middle'}));                                      
                                       headerhtml.append(imageHtml);
                                       }
                                       else if(v[1]==false && v[2]==true)
                                       {
                                       var imageHtml=$(this).create('a',{'href':v[0],'id':'idWorldWide'}).html(k);
                                       imageHtml.append('&nbsp;&nbsp;');
                                       imageHtml.append($(this).create('img', {'src':''+protocolBaseURL+'/_layouts/InternetFramework/Images/world_map.gif','style':'vertical-align:middle'}));                                      
                                       headerhtml.append(imageHtml);
                                       }
                                       else if(v[1]==true && v[2]==false)
                                       {
                                       headerhtml.append($(this).create('a',{'href':v[0],'target':'_blank'}).html(k));
                                       }
                                       else
                                       {
                                       headerhtml.append($(this).create('a',{'href':v[0]}).html(k));
                                       }

                                    }
                                    else
                                    {
                                    if(v[1]==true)
                                       {
                                       headerhtml.append($(this).create('a',{'href':v[0],'target':'_blank'}).html(k));
                                       headerhtml.append('&nbsp;|&nbsp;');
                                       }
                                       else
                                         {
                                       headerhtml.append($(this).create('a',{'href':v[0]}).html(k));
                                       headerhtml.append('&nbsp;|&nbsp;');
                                       }
                                    }
                                    });
                                    
        oHeaderHTML.append(headerhtml);
                               


       var oMenuUL = $(this).create('ul', {'class':'hw-mnu-nav','id':'hw-mnu-nav-one'});          
        
       $.each(_hw_aMenuTree_Complete, function(k, v) {

               var oItemHTML ='';
               //Checking whether the Menu is text based or image based
               if(v[1] == "text")
               {
               //Creating textbased menu HTML
                oItemHTML = HW_CreateTextRow(k,v[2],v[0],v[3]); 
               }
               else if(v[1] =="twocolumn")
               {
                oItemHTML = HW_CreateColumnRow(k,v[2],v[0]); 
               }
               else
               {
                //Creating Imagebased Menu HTML
                oItemHTML = HW_CreateImageRow(k,v[2],v[0]);
               }
              oMenuUL.append(oItemHTML);

         });           
            
          var oMenu_Bg_Left = $(this).create('div', {'class':'hw-mnu-bg-left'})
                                   .append(oMenuUL);
            
            //defining search input box
          //var oSearch_Input = $(this).create('input', {'class':'hw-mnu-search-box','id':'q','type':'text','name':'q','autocomplete':'on'});
           var oSearch_Input =  $(this).create('form', {'name':'cse-search-box1','id':'cse-search-box','type':'text'})
                                .append($(this).create('input', {'class':'hw-mnu-search-box','id':'q','type':'text','name':'q','autocomplete':'off'})
                                       );
          //HW_attachAutoComplete(oSearch_Input);  
          oSearch_Input.click(HW_OnSearchClick);
            //defining search button
          var oSearch_Button=  $(this).create('input', {'id':'imgSearch','type':'image','src':''+protocolBaseURL+'/_layouts/InternetFramework/Images/submitbutton_normal.jpg'});
          
          
            
          oSearch_Button.click(HW_RedirectToSearchPage);
            //attching hover function to the text box
           $(oSearch_Button).hover(function () {
           
               HW_ChangeGoImage(this,1);
                
              }, 
                 function () {
               HW_ChangeGoImage(this,0);
                  }
            );
      
            
           var oMenu_Bg_Right =   $(this).create('div', {'class':'hw-mnu-bg-right'})
                                    .append($(this).create('table', {'border':'0','cellpadding':'0','cellspacing':'0','style':'height:37px'})
                                            
                                             .append($(document.createElement('tr'))
                                                     .append($(document.createElement('td')).append(oSearch_Input))
                                                     .append($(document.createElement('td')).append(oSearch_Button))                                               
                                                    )
                                    
                                            );                                             
                                                          
                                                            
           var oMenu_Height = $(this).create('div', {'class':'hw-mnu-height'})
                                .append($(this).create('div', {'class':'hw-mnu-bg-div'})
                                        .append(oMenu_Bg_Left)
                                        .append(oMenu_Bg_Right)                                        
                                           
                                        );       
							
							
			//Adding the header	
			$('#hw-internet-header').append(oHeaderHTML);	
            $('#hw-internet-header').append(oMenu_Height);
			//Added for Image section hover
	      $(".hw-mnu-img-dim-1").hover(function() {
                      
              $(this).next().find('a').css('color', sMenu_Color_A81C24);
              $(this).next().find('a').css('background',sMenu_Color_DFDFDF);               
            
            },
	  function() {
	  	         
	         $(this).next().find('a').css('color', sMenu_Color_7E7E7E); 
	   	     $(this).next().find('a').css('background',sMenu_Color_FFFFFF);
	     	    
	      });
           //For the text to hover
 	  $('.hw-mnu-txt-1 a').hover(function()
  	   {
  	 		$(this).css('background-color', sMenu_Color_DFDFDF);
     		$(this).css('color', sMenu_Color_A81C24);  		
     				
       }, 
      function()
      {
    		$(this).css('background-color', sMenu_Color_FFFFFF);
			$(this).css('color', sMenu_Color_7E7E7E);			
 	  }); 

    $('#idWorldWide').hover(function()
  	   {
  	  $(this).find('img')[0].src=""+protocolBaseURL+"/_layouts/InternetFramework/Images/world_map_mouseover.gif";	
  	 	
     		 		
     				
       }, 
      function()
      {
    	$(this).find('img')[0].src=""+protocolBaseURL+"/_layouts/InternetFramework/Images/world_map.gif";	
 	  }); 
   
     //Attaching hover to the menu
         $('.hw-parent-mnu-item').hover(function()
  	          {
  	           $(this).prev().find('a').css("border-right-color", sMenu_Color_B3B3B3); 	 		          
  	 		                		
			   $(this).find('a').css("border-right-color", sMenu_Color_B3B3B3); 
			   $('.hw-mnu-bg-div').css('border-left-color',sMenu_Color_B3B3B3);
                      }, 
                function()
                    {
                    
                $(this).prev().find('a').css("border-right-color", sMenu_Color_E5E5E5);
                $(this).find('a').css("border-right-color", sMenu_Color_E5E5E5); 
                $('.hw-mnu-bg-div').css('border-left-color',sMenu_Color_CFCFCF);
                    
 	           });
 	           
 	            $("#hw-mnu-nav-one li a").hover(function(){                                           
                                                      
                                                                                                     
                $("ul", this).fade("fast");                                                
                                                
                      }, 
          function() {}                                                      
                                             
                      
                    );
                    
                    
           if (document.all) {                                   
                                            
                 $("#hw-mnu-nav-one li").hoverClass ("sfHover");
                            }           
 	           
 	           
}
/*****************************************************************************/
/*Function   : HW_CreateTextRow  */
/*Description: Creates Text Based Navigation */ 
/*****************************************************************************/

function HW_CreateTextRow(psMenuTitle,psMenuId,psMenuArray,psDropDown)      
{

if(psDropDown)
{
var Hlink;
$.each(psMenuArray, function(k, v) {
Hlink=v[0];
});
  var oMenu_Li= $(this).create('li', {'class':'hw-parent-mnu-item',id:psMenuId})
              .append($(this).create('a', {'href':Hlink}).html(psMenuTitle).click(HW_ReturnFalse));
              //.append($(document.createElement('a')).html(psMenuTitle));              
  //var oMenuInside_Ul= $(document.createElement('ul'));
}
else
{
//Creating Parent Li
  var oMenu_Li= $(this).create('li', {'class':'hw-parent-mnu-item',id:psMenuId})
              .append($(this).create('a', {'href':'#'}).html(psMenuTitle).click(HW_ReturnFalse));
              //.append($(document.createElement('a')).html(psMenuTitle));              
  var oMenuInside_Ul= $(document.createElement('ul'));
//Adding child items

  $.each(psMenuArray, function(k, v) {
  //If v[1] is true link to open in a new broswer window else in the same broswer window
 
  if(v[1])
  {
         oMenuInside_Ul.append($(document.createElement('li'))
                    .append($(this).create('a',{'href':v[0],'target':'_blank'}).html(k))
                    );
  }          
  else
  {   
       oMenuInside_Ul.append($(document.createElement('li'))
                    .append($(this).create('a',{'href':v[0]}).html(k))
                    );
          

}

   });
   }
  oMenu_Li.append(oMenuInside_Ul);
   return oMenu_Li;
              
}      
                  
 /*****************************************************************************/
/*Function   : HW_CreateImageRow  */
/*Description: Creates Image Based Navigation */ 
/*****************************************************************************/

function HW_CreateImageRow(psMenuTitle,psMenuId,psMenuArray)
{
  var oMenu_Li= $(this).create('li', {'class':'hw-parent-mnu-item',id:psMenuId})
              .append($(this).create('a', {'href':'#'}).html('<span style="text-transform:uppercase">'+psMenuTitle+'</span>'));

  var oMenuInside_Ul    = $(document.createElement('ul')).attr('class','hw-mnu-image-drpdown').attr('id','hw-mnu-image-drpdown');
  var oMenu_Parent_Li   = $(document.createElement('li'));  
  var oParent_Image_Tr   = $(document.createElement('tr'));                      
                      
  //Adding Child Elements
  $.each(psMenuArray, function(k, v) {

  var oImage_Td  = $(document.createElement('td')).attr('valign','top')
                     .append($(this).create('div', {'class':'hw-mnu-img-dim-1'}).html('<a href="' + '' + v[1]+'"><img src="' + protocolBaseURL + v[0] + '" alt="" class="hw-mnu-img1" /></a>'))
                     .append($(this).create('div', {'class':'hw-mnu-txt-1','style':'height=20px'}).html('<a href="'+ '' +v[1]+'" style="clear:both; height:20px;" >'+ k + '</a>'));       
                     

  oParent_Image_Tr.append(oImage_Td);
            
 });
 
  var oMenuImage_Table =$(this).create('table', {'border':'0','cellpadding':'0','cellspacing':'0','class':'hw-mnu-img'}) 
                     .append(oParent_Image_Tr);
  oMenu_Li.append(oMenuInside_Ul.append(oMenu_Parent_Li.append(oMenuImage_Table )));
  return oMenu_Li;                  

}
function HW_CreateColumnRow(psMenuTitle,psMenuId,psMenuArray)
{
  var oMenu_Li= $(this).create('li', {'class':'hw-parent-mnu-item',id:psMenuId})
              .append($(this).create('a', {'href':'#'}).html(psMenuTitle));

  var oMenuInside_Ul    = $(document.createElement('ul')).attr('class','hw-nav-products').attr('id','hw-nav-products');
  var oMenu_Parent_Li   = $(document.createElement('li'));  
  var oParent_Image_Tr   = $(document.createElement('tr'));
  
  
  
  var _hw_MnuCol1 =   $(document.createElement('td')).attr('valign','top').attr('class','hw-nav-products-td1');
  var _hw_MnuCol2 =   $(document.createElement('td')).attr('valign','top').attr('class','hw-nav-products-td2'); 
  var _hw_MnuCol3 =   $(document.createElement('td')).attr('valign','top').attr('class','hw-nav-products-td3');
  

                    
                   
  //Adding Child Elements
  $.each(psMenuArray, function(k, v) {
  
   
 
  var oRowDiv= $(this).create('div', {'class':'hw-nav-products-div'}).html('<a href="'+ '' +v[0]+'" class="hw-nav-products-link" >'+ k + '</a>');
         
  if(v[1] == '_hw_MnuCol1' )
  {
  _hw_MnuCol1.append(oRowDiv);
  }
  else if(v[1] == '_hw_MnuCol2')
  {
  _hw_MnuCol2.append(oRowDiv);
  }
  else if(v[1] == '_hw_MnuCol3')
  {
  _hw_MnuCol3.append(oRowDiv);
  }              
        
 });
 
 oParent_Image_Tr.append(_hw_MnuCol1);
 oParent_Image_Tr.append(_hw_MnuCol2);
 //oParent_Image_Tr.append(_hw_MnuCol3);
 
  var oMenuImage_Table =$(this).create('table', {'border':'0','cellpadding':'0','cellspacing':'0','class':'hw-mnu-img'}) 
                     .append(oParent_Image_Tr);
  oMenu_Li.append(oMenuInside_Ul.append(oMenu_Parent_Li.append(oMenuImage_Table )));
  return oMenu_Li;                  

}    




       
                  
$.fn.hoverClass = function(c) {
   return this.each(function(){
       $(this).hover( 
             function() { $(this).addClass(c);  },
             function() { $(this).removeClass(c); }
                   );
                      });              
                                                                                    
      }; 
                
  /*****************************************************************************/
/*Function   : HW_RedirectToSearchPage()  */
/*Description: Creates Image Based Navigation */ 
/*****************************************************************************/                  
   function  HW_CreateHeaderMenu()
   {
   
   //Create header             
     HW_CreateMenu();
 
 
     if(document.getElementsByName('MenuHighlight').length != 0)
     {

     HW_SetNavMenu(document.getElementsByName("MenuHighlight")[0].content);           
     }

     //Redirecting to the search page
     var oQueryStr =  HW_GetQueryString('q');
     if(oQueryStr!= null && oQueryStr!='')
     {
    
      window.location = _hw_baseURL + "/Pages/Search.aspx?k=" + oQueryStr ;
     }
     else
     {
     document.getElementById('q').value= _hw_sSearch_Search;
     }   
   
   }       
   
 /*****************************************************************************/
/*Function   : HW_RedirectToSearchPage()  */
/*Description: Creates Image Based Navigation */ 
/*****************************************************************************/
  function HW_RedirectToSearchPage()
  {

  var sInputValue = document.getElementById('q').value;
  
 

 
  if(sInputValue == '' || sInputValue == _hw_sSearch_Search )
  {
  alert(_hw_sSearch_PleaseEnterSearchText);
  document.getElementById('q').focus();
 
  }
  else
  {
    var inputValue = sInputValue.replace(/\%/g," ");
    window.location = _hw_baseURL + "/Pages/Search.aspx?k=" + encodeURIComponent(inputValue) ;
  }
  
  }
  /*****************************************************************************/
/*Function   : HW_OnSearchClick()  */
/*Description: clears text on click of the textbox */ 
/*****************************************************************************/
 function HW_OnSearchClick() 
 {
 if(document.getElementById('q').value == _hw_sSearch_Search)
 document.getElementById('q').value = '';
 }
/*****************************************************************************/
/*Function   : HW_CreateFooter()  */
/*Description: Creates Image Based Navigation */ 
/*****************************************************************************/
function HW_CreateFooter()
{

 var oParent_SitetMap_Tr= $(this).create('tr', {'valign':'top'})
                               .append($(this).create('td', {'class':'hw-sitemap_tdf','id':'_hw_SiteMap_Col1'}))
                               .append($(this).create('td', {'class':'hw-sitemap_td','id':'_hw_SiteMap_Col2'}))
                               .append($(this).create('td', {'class':'hw-sitemap_td','id':'_hw_SiteMap_Col3'}))
                               .append($(this).create('td', {'class':'hw-sitemap_td','id':'_hw_SiteMap_Col4'}))
                               .append($(this).create('td', {'class':'hw-sitemap_td1','id':'_hw_SiteMap_Col5'}));                                                      
                                                            
                               
                               
var oSiteMap_Table =$(this).create('table', {'border':'0','cellpadding':'0','cellspacing':'0','style':'height:165px;'}) 					                        
                            .append(oParent_SitetMap_Tr); 

var hw_imgCntrl ="<div/>"
if(typeof _hw_footer_logoId !='undefined')
{
    var hw_imgPath = HW_GetFooterLogoImage(_hw_footer_logoId);
    if(hw_imgPath!="")
    {
        hw_imgCntrl = "<img src='" + protocolBaseURL  + hw_imgPath +"' class='hw-footer-logoimg'/>";
    }
}

else
{
    //hw_imgCntrl = "<div id='hwSocialMedia-Cont'><a title='Facebook' href='#'id='hwFacebook' class='hwSocialMedia' onclick='SocialMedia(1)' target='_blank'>Facebook</a><a title='Twitter' href='#'id='hwTwitter' class='hwSocialMedia' onclick='SocialMedia(2)' target='_blank'>Twitter</a><a title='Google Plus' href='#'id='hwGooglePlus' class='hwSocialMedia' onclick='SocialMedia(4)' target='_blank'>Google Plus</a><a title='Linkedin' href='#'id='hwLinkedIn' class='hwSocialMedia' onclick='SocialMedia(3)' target='_blank'>Linkedin</a></div>";

hw_imgCntrl = $(this).create('div', {'id':'hwSocialMedia-Cont'}).html('<font style="font-size:11px;float: left; font-weight: bold; margin-top:2px; color:#919191">SHARE&nbsp;</font><a title="Facebook" href="#" id="hwFacebook" class="hwSocialMedia" onclick="SocialMedia(1)" target="_blank">Facebook</a><a title="Twitter" href="#" id="hwTwitter" class="hwSocialMedia" onclick="SocialMedia(2)" target="_blank">Twitter</a><a title="Google Plus" href="#" id="hwGooglePlus" class="hwSocialMedia" onclick="SocialMedia(4)" target="_blank">Google Plus</a><a title="Linkedin" href="#" id="hwLinkedIn" class="hwSocialMedia" onclick="SocialMedia(3)" target="_blank">Linkedin</a>');

}



var oSiteMap_Div =  $(this).create('div', {'class':'hw-sitemap_main_div'})
                      .append($(this).create('div', {'class':'spacethirtypix'}))
                      .append($(this).create('div', {'id':'sitemap'})
                              .append($(this).create('a', {'class':'hw-sitemap-txt','title':_hw_sSiteMap}).bind('click',HW_ToggleDiv)
                                       .append($(this).create('img', {'style':'vertical-align:baseline;','src':''+protocolBaseURL+'/_layouts/InternetFramework/images/button_closed.gif'}))
                                       .append('<strong>&nbsp;'+_hw_sSiteMap_Div_Sitemap +'</strong>')
                                      )                                      
                                      .append(hw_imgCntrl)
                              )   
                       .append($(this).create('div', {'id':'sitemap1','style':'display:none'})
                              .append($(this).create('a', {'class':'hw-sitemap-txt','title':_hw_sSiteMap}).bind('click',HW_ToggleDiv)
                                       .append($(this).create('img', {'style':'vertical-align:baseline;','src':''+protocolBaseURL+'/_layouts/InternetFramework/images/button_open.gif'}))
                                       .append('<strong>&nbsp;'+_hw_sSiteMap_Div_Sitemap +'</strong>')
                                      )                                      
                                      
                              )
                        .append($(this).create('div', {'class':'spacetenpix'})       
                        .append(hw_imgCntrl));
                        var oFooter_Div = $(this).create('div', {'id':'hw-footer'});
                       
                    
   var footerhtml=$(this).create('div', {'class':'hw-footer-link','style':'float:left;'});
                                var countFooter=0;
                                $.each(_hw_uFooter,function(k,v){
                                countFooter=countFooter+1;
                                 });                               
                                
                                    var countinsideFooter=0;
                                    $.each(_hw_uFooter,function(k,v){
                                    countinsideFooter++;                                   
                                    if(countinsideFooter==countFooter)
                                    {
                                        if(v[2]==true)
                                       {
                                       var footerFeed=$(this).create('span', {'class':'hw-footer-link','style':'padding-left:0px'});
                                       footerFeed.append($(this).create('a',{'class':'hw-footerbox','href':v[0]+''}).html(k));
                                       footerhtml.append(footerFeed);
                                       }
                                       else if(v[1]==true)
                                       {                                       
                                       footerhtml.append($(this).create('a',{'href':v[0],'target':'_blank'}).html(k));
                                       }
                                       else
                                       {
                                       footerhtml.append($(this).create('a',{'href':v[0]}).html(k));
                                       }
                                    }
                                    else
                                    {
                                       if(v[2]==true)
                                       {
                                       var footerFeed=$(this).create('span', {'class':'hw-footer-link','style':'padding-left:0px'});
                                       footerFeed.append($(this).create('a',{'class':'hw-footerbox','href':v[0]+''}).html(k));
                                       footerhtml.append(footerFeed);
                                       footerhtml.append('&nbsp;|&nbsp;');
                                       }
                                       else if(v[1]==true)
                                       {                                       
                                       footerhtml.append($(this).create('a',{'href':v[0],'target':'_blank'}).html(k));
                                       footerhtml.append('&nbsp;|&nbsp;');                                       
                                       }
                                       else
                                       {
                                       footerhtml.append($(this).create('a',{'href':v[0]}).html(k));
                                       footerhtml.append('&nbsp;|&nbsp;');
                                       
                                       }                                    }
                                    });
                                    
                               oFooter_Div.append(footerhtml);  
                               oFooter_Div.append($(this).create('div', {'style':'float: right;'}).html('&copy;&nbsp;'+ _hw_sFooter_Div_YearHoneywellInternationalInc));            
                       
  
var oFinal_Div = $(this).create('div', {'style':'clear:both;'})
               .append(oSiteMap_Div)
               .append($(this).create('div', {'id':'div1','class':'hw-sitemap_hide_div','style':'display: none;'})
                              .append(oSiteMap_Table)
                               );
           

//var oGoogle_Translator = $(this).create('div', {'class':'gspace'})
//                            .append($(this).create('div', {'id':'google_translate_element'}));

                      
  $('#hw-internet-footer').append(oFinal_Div );                     
  $('#hw-internet-footer').append(oFooter_Div);
//  $('#hw-internet-footer').append(oGoogle_Translator);  
$('#hw-internet-footer').append('<div class="spacethirtypix"/>');                       
  
                       
	}
/*****************************************************************************/
/*Function   : HW_CreateSiteMap() */
/*Description: Creates SiteMap Items */ 
/*****************************************************************************/							
function HW_CreateSiteMap()
{	
				 
	    							 		 
      $.each(_hw_oSiteMapTree_Complete, function(k, v) {
     
       var oSiteMapHeader_Div  = $(this).create('div', {'class':'hw-sitemap-head'}).html(k);
       //creating sitemap column id
       var oSiteMap_ColumnId= '#' + v[1];


      
       $(oSiteMap_ColumnId).append(oSiteMapHeader_Div);

       //Creating child elements
        $.each(v[0], function(k, v) {        
                
          //If v[1] is true link to open in a new broswer window else in the same broswer window
          if(v[1])
          {
          $(oSiteMap_ColumnId).append(      
          $(this).create('div', {'class':'hw-sitemap_linktxt'})
                 .append($(this).create('a', {'href':v[0],'target':'_blank'}).html(k))
                                 );
          }
          else
          {
          $(oSiteMap_ColumnId).append(      
          $(this).create('div', {'class':'hw-sitemap_linktxt'})
                 .append($(this).create('a', {'href':v[0]}).html(k))
                                 );
           }                      
                            //.append('<br/>');                            
                            
        
            
         });  
         
        
       $(oSiteMap_ColumnId).append($(this).create('div', {'class':'hw-sitemap-space'}));           
            
       });       
}

/*****************************************************************************/
/*Function   : HW_SetNavMenu */
/*Description: Highlighs the Menu */ 
/*****************************************************************************/
function HW_SetNavMenu(psNavigation)
{
        var oHightId= "'[id=" + psNavigation +']' ;

   
        if(document.getElementById(psNavigation) !=null)
        {
         $(oHightId).addClass('hw-mnu-active-page');
         $(oHightId +" "+ "a:first").attr('id','hw-mnu-active-fc');
        }
    
}


/*****************************************************************************/
/*Function   : HW_GetFooterLogoImage */
/*Description: Return the footer logo image based on Image Id*/ 
/*****************************************************************************/
function HW_GetFooterLogoImage(logoId)
{
        switch (logoId)
        {
        case 0:
          return "../Images/KCP_footer_logo.jpg"/*tpa=http://honeywell.com/_layouts/InternetFramework/Images/KCP_footer_logo.jpg*/;
          break;
        
        default:
          return "";
            
        }
}

/*****************************************************************************/
/*Function   : HW_GetQueryString */
/*Description: Fetchs the value of query string */ 
/*****************************************************************************/
function HW_GetQueryString(pskey, psdefault_)
{
  if (psdefault_==null) psdefault_=""; 
  pskey = pskey.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regex = new RegExp("[\\?&]"+pskey+"=([^&#]*)");
  var qs = regex.exec(window.location.href);
  if(qs == null)
    return psdefault_;
  else
    return qs[1];
}
function HW_ReturnFalse()
{
    return false;
} 

/* Social Media */
function SocialMedia(sm)
{
  
	var _ShareLink = window.location;
    if (sm==1)
	{
		var _fb = document.getElementById('hwFacebook');
		_fb.setAttribute("href", "http://www.facebook.com/share.php?u="+_ShareLink);
	}
	if (sm==2)
	{
		var _tweet = document.getElementById('hwTwitter');
		_tweet.setAttribute("href", "http://twitter.com/home?status="+_ShareLink);
	}

	if (sm==3)
	{
		var _linkedin = document.getElementById('hwLinkedIn');
		_linkedin.setAttribute("href", "http://www.linkedin.com/shareArticle?mini=true&url="+_ShareLink);
	}
	if (sm==4)
	{
		var _gplus = document.getElementById('hwGooglePlus');
		_gplus.setAttribute("href", "https://plus.google.com/share?url="+_ShareLink);
	}
   

}

/*Social Media */
