var dr = document.referrer;

if (dr.indexOf("http://www.cablevision.com/js/google.com") != -1 || dr.indexOf("http://www.cablevision.com/js/google.ca") != -1 || dr.indexOf("http://www.cablevision.com/js/google.co.jp") != -1 || dr.indexOf("http://www.cablevision.com/js/google.co.uk") != -1 || dr.indexOf("http://www.cablevision.com/js/yahoo.com") != -1 || dr.indexOf("http://www.cablevision.com/js/bing.com") != -1 || dr.indexOf("http://www.cablevision.com/js/askjeeves.com") != -1 || dr.indexOf("http://www.cablevision.com/js/aol.com") != -1 || dr.indexOf("http://www.cablevision.com/js/myway.com") != -1 || dr.indexOf("http://www.cablevision.com/js/mywebsearch.com") != -1)
{
(document.location.href.indexOf("http://www.cablevision.com/js/aitrk.com") != -1) ? addQueryStringValues("visitType","searchPaidFromCVC","optimum.com|optimumbusiness.com|optimumstore.com|optimumrewards.com") : addQueryStringValues("visitType","searchNaturalFromCVC","optimum.com|optimumbusiness.com|optimumstore.com|optimumrewards.com");
}

function addQueryStringValues(queryParam, queryValue, includeDomainsArray)
{

   var links = document.getElementsByTagName('a');
   var includeDomains = new Array();
   
   if(arguments.length == 3) //has include domains
   {
       //Links will be updated only if they point to one of the following domains:
       //includeDomains = includeDomainsArray.split("|");
       includeDomains = includeDomainsArray;
   }
   else
   {
       //Links will be updated only if they point to one the current domain only:
       includeDomains[0] = self.location.host;
   }
   
   for (var i=0;i<links.length;i++)
   {
       if(links[i].href != "#" && links[i].href != "/" && links[i].href != "" && links[i].href != window.location) //Ignore links with empty src attribute, linking to site root, or anchor tags (#)
       {
           var updateLink = false;
           for(k=0;k<includeDomains.length;k++)
           {
               if(links[i].href.toLowerCase().indexOf(includeDomains[k].toLowerCase()) != -1) //Domain of current link is inlcluded i the includeDomains array.  Update Required...
                   updateLink = true;
           }
       
           if(!updateLink)
           {
               //Do nothing - link not is includeDomains array
           }
           else
           {
               var queryStringComplete = "";
               var paramCount = 0;
           
               var linkParts = links[i].href.split("?");
               
               if(linkParts.length > 1) // Has Query String Params
               {
                   queryStringComplete = "?";
               
                   var fullQString = linkParts[1];
                   var paramArray = fullQString.split("&");    
                   var found = false;
                   
                   for (j=0;j<paramArray.length;j++)
                   {
                       
                       var currentParameter = paramArray[j].split("=");
                       
                       if(paramCount > 0)
                           queryStringComplete = queryStringComplete + "&";
                       
                       if(currentParameter[0] == queryParam) //Parameter exists in url, refresh value
                        {
                            queryStringComplete = queryStringComplete + queryParam + "=" + queryValue;
                            found = true;
                        }
                        else
                        {
                            queryStringComplete = queryStringComplete + paramArray[j]; //Not related parameter - re-include in url
                        }
                       
                        paramCount++;
                   }
                   
                   if(!found) //Add new param to end of query string
                       queryStringComplete = queryStringComplete + "&" + queryParam + "=" + queryValue;
               }
               else
               {
                   queryStringComplete = "?" + queryParam + "=" + queryValue;
               }
                   
               links[i].href = links[i].href.split("?")[0] + queryStringComplete;        
           }
       }
       else
       {
           //Do nothing - link not is includeDomains array
       }
   }
}