
$(document).ready(function () {

    $('.rotator').show();

    $('.rotator').cycle({
        fx: 'fade',
        pause: 1,
        speed:2000,
        timeout: 6000,
        pager: '#pager',
        pagerAnchorBuilder: function (idx, slide) {
            return '<div class="white-circle"><a href="#"></a></div>';
        }
    });

});


