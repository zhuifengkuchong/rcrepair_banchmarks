function submitcheck() {

  document.cookie = 'SMSESSION=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' + '; path=/; domain=.ingrammicro.com';

  document.cookie = 'SMTRYNO=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' + '; path=/; domain=.ingrammicro.com';  

  document.cookie = 'SMCHALLENGE=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' + '; path=/; domain=.ingrammicro.com';

  document.cookie = 'SMCREDS=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' + '; path=/; domain=.ingrammicro.com';

  document.cookie = 'SMONDENIEDREDIR=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' +'; path=/; domain=.ingrammicro.com'; 



  capitalize();

  return true;

}

function submitcheckWithCheckCapsIDLength() {
	if(document.login_form.USER.value.length != 6)
	{
		alert("The User ID you entered is not a valid Ingram Micro DC/POS ID.  Please contact eSolutions Customer Support at (800) 616-4665 for further assistance.");
		return false;
	}
	document.cookie = 'SMSESSION=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' + '; path=/; domain=.ingrammicro.com';
	document.cookie = 'SMTRYNO=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' + '; path=/; domain=.ingrammicro.com';  
	document.cookie = 'SMCHALLENGE=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' + '; path=/; domain=.ingrammicro.com';
	document.cookie = 'SMCREDS=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' + '; path=/; domain=.ingrammicro.com';
	document.cookie = 'SMONDENIEDREDIR=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' +'; path=/; domain=.ingrammicro.com'; 
	capitalize();
	return true;
}

function capitalize() {

  document.login_form.USER.value = document.login_form.USER.value.toUpperCase();

}


// modified by infosys 24/4/07 . Chnaged to clear the session on click on Log Out Link. Change Start
function logout() {
  document.cookie = 'SMSESSION=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' + '; path=/; domain=.ingrammicro.com';

  document.cookie = 'SMTRYNO=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' + '; path=/; domain=.ingrammicro.com';  

  document.cookie = 'SMCHALLENGE=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' + '; path=/; domain=.ingrammicro.com';

  document.cookie = 'SMCREDS=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' + '; path=/; domain=.ingrammicro.com';

  document.cookie = 'SMONDENIEDREDIR=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' +'; path=/; domain=.ingrammicro.com'; 

  document.cookie = 'smUserType=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' +'; path=/; domain=.ingrammicro.com'; 

  document.cookie = 'IM_SITE=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' +'; path=/; domain=.ingrammicro.com'; 

  document.cookie = 'catalogID=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' +'; path=/; domain=.ingrammicro.com';
  
//  location.href=document.URL;

  location.href = "http://www.ingrammicro.com/js/IMD_WASWeb/jsp/login/logout.jsp";  

}
// modified by infosys 24/4/07 . Chnaged to clear the session on click on Log Out Link. Change End


function checkCookie(){



  offset = document.cookie.indexOf("NGUserID");

  

  if (offset == -1) { // if cookie does not exist

    alert("Your browser is not set up to accept cookies. Please change the setting of your browser to accept cookies and then reload the page again");

  }

}



function checkBrowser() {

  

  // Code for detecting the browser on a WIN32 platform

  if((navigator.platform=="Win32") && (navigator.appName.indexOf("Microsoft")!=-1)) {

    var ver = navigator.appVersion;

    if (ver.charAt(0)<"4") {

      alert("Your Browser is not supported by the current Ingrammicro site and as a result you may experience difficulties in loading of some of the pages. Please upgrade your browser to a 4 or higher version of IE");

    }

  }

  

  if((navigator.platform=="Win32") && (navigator.appName.indexOf("Netscape")!=-1)) {

    var ver = navigator.appVersion;

    if (ver.charAt(0)<"4") {

      alert("Your Browser is not supported by the current Ingrammicro site and as a result you may experience difficulties in loading of some of the pages. Please upgrade your browser to a 4.03 or higher version of Netscape");

      }

  }

  

  // Code for detecting browser on a Mac Platform

  if((navigator.platform!="Win32") && (navigator.appName.indexOf("Microsoft")!=-1))

  {

    var ver = navigator.appVersion;

    if (ver.charAt(0)<"4") {

      alert("Your Browser is not supported by the current Ingrammicro site and as a result you may experience difficulties in loading of some of the pages. Please upgrade your browser to a 4.01 or higher version of IE");

    }

  }

  

  if((navigator.platform!="Win32") && (navigator.appName.indexOf("Netscape")!=-1)) {

    var ver = navigator.appVersion;

    if (ver.charAt(0)<"4") {

      alert("Your Browser is not supported by the current Ingrammicro site and as a result you may experience difficulties in loading of some of the pages. Please upgrade your browser to a 4.03 or higher version of Netscape");

    }

  }

}



function checkUserName() {



  if(document.login_form.USER.value != "") {



    document.cookie = 'SMSESSION=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' + '; path=/; domain=.ingrammicro.com';

    document.cookie = 'SMTRYNO=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' + '; path=/; domain=.ingrammicro.com';  

    document.cookie = 'SMCHALLENGE=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' + '; path=/; domain=.ingrammicro.com';

    document.cookie = 'SMCREDS=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' + '; path=/; domain=.ingrammicro.com';

    document.cookie = 'SMONDENIEDREDIR=; expires=' + 'Mon, 01-Jan-90 00:00:01 GMT' +'; path=/; domain=.ingrammicro.com';

    document.login_form.submit();

    

  } else {

    alert("Please enter a username ");

  }

  return;

}







function MM_jumpMenu(targ,selObj,restore){ //v3.0

          eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");

          if (restore) selObj.selectedIndex=0;

        }

