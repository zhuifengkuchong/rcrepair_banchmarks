/*
* jQuery vScroll
* Copyright (c) 2011 Simon Hibbard
* 
* Permission is hereby granted, free of charge, to any person
* obtaining a copy of this software and associated documentation
* files (the "Software"), to deal in the Software without
* restriction, including without limitation the rights to use,
* copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following
* conditions:

* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
* OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE. 
*/

/*
* Version: V1.2.0
* Release: 10-02-2011
* Based on jQuery 1.4.2
*/



(function ($) {
    $.fn.vScroll = function (options) {
        var options = $.extend({}, { speed: 500, height: 300, upID: "#up-arrow", downID: "#bottom-arrow", debug: false }, options), $moduleWrap = this;
        return $moduleWrap.each(function () {
            $obj = $(this);
            $obj.itemCount = 0;
            $obj.divH = 0;
            $obj.nextCount = 0;
            $obj.prevCount = 0;
            $obj.containerHeight = 0;
            $obj.dynSkipCount = 0;
            $obj.totalCount = 1;
            $obj.arrayMove = new Array();
            $obj.clickCount = 0;
            $obj.nextdivH = 0;
            $obj.nextDivCount = 0;
            $obj.totalHeight = 0;
            $obj.css("overflow", "hidden");
            $obj.css("position", "relative");
            $obj.css("height", options.height + "px");

            $(options.upID).hide();
            $(options.downID).hide();

            if ($obj.find(".vscroller").length) {
                //$obj.find(".vscroller").css("top", "0");
            }
            else {
                $obj.children().wrapAll("<div style='position: relative; top: 0' class='vscroller'></div>");
            }

            $obj.find(".vscroller").children().attr("class", "newsTabArticle");

            $obj.find(".vscroller").children().each(function (intIndex) {
                $(this).addClass("vscroll-" + intIndex);
                $obj.totalHeight += $(this).outerHeight();
                if (options.debug && console) {
                    console.log("outerHeight ", $(this).outerHeight());
                }
                $obj.totalCount++;
            });

            if ($obj.totalHeight > $obj.outerHeight()) {
                $obj.find(".vscroller").css("top", "0");
                $(options.upID).hide();
                $(options.downID).show();
            }
            //else {
            //    $obj.children().wrapAll("<div style='position: relative; top: 0' class='vscroller'></div>");
            //}

            if (options.debug && console) {
                console.log("totalHeight " + $obj.totalHeight);
                console.log("nextdivH " + $obj.nextdivH);
            }

            if ($obj.nextdivH < $obj.containerHeight) {
                $(options.upID).hide();
                $(options.downID).hide();
                $obj.nextdivH = $obj.containerHeight;
            }

            $obj.containerHeight = $obj.outerHeight();

            while ($obj.nextdivH < $obj.totalHeight) {
                $obj.nextdivH += $obj.find('.vscroller .vscroll-' + $obj.nextDivCount).outerHeight();
                $obj.nextDivCount++;
            }

            $obj.nextDivCount = 10;

            $obj.nextCount = $obj.nextDivCount;

            //need to calculate total height to scroll
            for (var i = $obj.itemCount; i < $obj.nextCount; i++) {
                $obj.divH += $obj.find('.vscroller .vscroll-' + i).outerHeight();
            }

            while ($obj.divH > $obj.containerHeight) {
                $obj.nextCount--;
                $obj.divH = 0;
                for (var i = $obj.itemCount; i < $obj.nextCount; i++) {
                    $obj.divH += $obj.find('.vscroller .vscroll-' + i).outerHeight();
                }
            }

            $obj.dynSkipCount = $obj.nextCount;
            if (options.debug && console) {
                console.log("containerHeight: " + $obj.containerHeight);
                console.log("nextCount " + $obj.nextCount);
                console.log("divH " + $obj.divH);
                console.log("skipcount " + $obj.dynSkipCount);
            }
            if (!($(options.downID).data("events"))) {
                $(options.downID).click(function () {
                    //$obj.nextCount = $obj.itemCount + options.skipCount;
                    var handler = arguments.callee;
                    var $this = $(this).off("click", handler);
                    $obj.nextdivH = 0;
                    $obj.nextDivCount = 0;
                    $obj.nextDivCount = $obj.itemCount;
                    while ($obj.nextdivH < $obj.containerHeight) {

                        //for (var i = $obj.itemCount; i < $obj.nextDivCount; i++) {
                        $obj.nextdivH += $obj.find(".vscroller").find('.vscroll-' + $obj.nextDivCount).outerHeight();
                        $obj.nextDivCount++;
                        //}
                    }

                    $obj.nextCount = $obj.nextDivCount;

                    while (!($obj.find(".vscroller").find('.vscroll-' + $obj.nextCount).length) && $obj.nextCount > $obj.itemCount) {
                        $obj.nextCount--;
                    }
                    if ($obj.find(".vscroller").find('.vscroll-' + $obj.nextCount).length) {

                        //need to calculate total height to scroll
                        $obj.divH = 0;
                        for (var i = $obj.itemCount; i < $obj.nextCount; i++) {
                            $obj.divH += $obj.find(".vscroller").find('.vscroll-' + i).outerHeight();
                        }
                        while ($obj.divH > $obj.containerHeight) {
                            $obj.nextCount--;
                            $obj.divH = 0;
                            for (var i = $obj.itemCount; i < $obj.nextCount; i++) {
                                $obj.divH += $obj.find(".vscroller").find('.vscroll-' + i).outerHeight();
                            }
                        }
                        $obj.dynSkipCount = $obj.nextCount - $obj.itemCount;
                        $obj.itemCount += $obj.dynSkipCount;

                        $obj.arrayMove[$obj.clickCount] = $obj.dynSkipCount;

                        if (options.debug && console) {
                            console.log("arrayMove " + $obj.arrayMove[$obj.clickCount]);
                            console.log("Down click event fired");
                            console.log("divH " + $obj.divH);
                            console.log("dynSkipCount " + $obj.dynSkipCount);
                            console.log("itemCount " + $obj.itemCount);
                            console.log("nextCount " + $obj.nextCount);
                        }

                        $obj.clickCount++;

                        $obj.find(".vscroller").animate({
                            top: "-=" + $obj.divH + "px"
                        }, options.speed, function () {
                            $(options.upID).show();
                            $this.click(handler);
                            onSlideDownComplete();
                        });
                    }
                });
            }

            if (!($(options.upID).data("events"))) {
                $(options.upID).click(function () {
                    var handler = arguments.callee;
                    var $this = $(this).off("click", handler);

                    $obj.clickCount--;
                    var moveUp = $obj.arrayMove[$obj.clickCount];
                    $obj.prevCount = $obj.itemCount - moveUp;
                    if (options.debug && console) {
                        console.log("clickCount " + $obj.clickCount);
                        console.log("arrayMove " + $obj.arrayMove[$obj.clickCount]);
                        console.log("prevCount " + $obj.prevCount);
                    }

                    if ($obj.find(".vscroller").find('.vscroll-' + $obj.prevCount).length) {
                        $obj.divH = 0;
                        for (var i = $obj.prevCount; i < $obj.itemCount; i++) {
                            $obj.divH += $obj.find(".vscroller").find('.vscroll-' + i).outerHeight();
                        }

                        $obj.itemCount -= moveUp;


                        if (options.debug && console) {
                            console.log("divH " + $obj.divH);
                            console.log("itemCount " + $obj.itemCount);
                        }
                        $obj.find(".vscroller").animate({
                            top: "+=" + $obj.divH + "px"
                        }, options.speed, function() {
                            $this.click(handler); 
                            onSlideUpComplete();
                        });
                    }
                    if ($obj.prevCount == 0) {
                        $(options.upID).hide();
                        onSlideUpComplete();
                    }
                });
            }

            function onSlideUpComplete() {
                if (options.debug && console) {
                    console.log("Slide Up Complete");
                }
                $(options.downID).show();
            }

            function onSlideDownComplete() {
                if (options.debug && console) {
                    console.log("Slide Down Complete");
                }

                var remainingDivHeights = 0;
                for (var i = $obj.itemCount; i < $obj.totalCount; i++) {

                    remainingDivHeights += $obj.find(".vscroller").find('.vscroll-' + i).outerHeight();
                }
                if (options.debug && console) {
                    console.log("remainingDivHeights " + remainingDivHeights);
                }

                if (remainingDivHeights < $obj.containerHeight) {
                    $(options.downID).hide();
                }
            };
        });


    };

})(jQuery);
