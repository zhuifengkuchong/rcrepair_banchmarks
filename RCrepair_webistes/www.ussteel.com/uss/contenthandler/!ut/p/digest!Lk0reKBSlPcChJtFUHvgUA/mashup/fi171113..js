(function(){
var _1=ibmCfg.portalConfig.contentHandlerURI+"?uri=menu:${id}";
var _2=false;
var _3=function(){
return i$.hasClass(document.getElementsByTagName("body")[0],"edit-mode");
};
if(typeof wptheme==="undefined"||!wptheme){
wptheme={};
}
i$.mash(wptheme,{getWindowIDFromSkin:function(_4){
while((_4=_4.parentNode)!=null){
if(i$.hasClass(_4,"component-control")){
var m=_4&&(_4.className||"").match(/id-([\S]+)/);
return m&&m[1];
}
}
return null;
},getPortletState:function(_5){
var _6=i$.byId("portletState");
if(_6){
if(!_6._cache){
_6._cache=i$.fromJson(_6.innerHTML);
_6._cache._defaults={"windowState":"normal","portletMode":"view"};
}
if(_6._cache[_5]){
return _6._cache[_5];
}else{
return _6._cache._defaults;
}
}
return {};
},isValidOp:function(_7){
if(_7.visibility===false){
return false;
}
var _8=_7.metadata||{};
switch(_7.id){
case "ibm.portal.operations.changePortletMode":
return wptheme.getPortletState(_8.wid).portletMode!=_8.portletMode;
case "ibm.portal.operations.changeWindowState":
return wptheme.getPortletState(_8.wid).windowState!=_8.windowState;
default:
}
return true;
},operation:{changeToHelpMode:function(_9){
var _a=window.location.href;
if(_9.actionUrl){
if(_9.actionUrl.indexOf("?")==0){
var _b=_a.indexOf("#");
if(_b!=-1){
var _c=_a.substring(0,_b);
var _d=_a.substring(_b);
_a=_c+(_c.indexOf("?")==-1?"?":"&")+_9.actionUrl.substring(1);
_a+=_d;
}else{
_a+=(_a.indexOf("?")==-1?"?":"&")+_9.actionUrl.substring(1);
}
}else{
_a=_9.actionUrl;
}
}
window.open(_a,"","resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=no,width=800,height=600,screenX=10,screenY=10,top=10,left=10");
}},contextMenu:{cache:{},css:{focus:"wpthemeMenuFocus",disabled:"wpthemeMenuDisabled",show:"wpthemeMenuShow",error:"wpthemeMenuError",menuTemplate:"wpthemeTemplateMenu",submenuTemplate:"wpthemeTemplateSubmenu",loadingTemplate:"wpthemeTemplateLoading"},init:function(_e,_f,_10){
_e._contextMenu=_e._contextMenu||{};
_e._contextMenu.id=_e._contextMenu.id||Math.round(Math.random()*1000000000);
_e.setAttribute("id",_e._contextMenu.id);
_e._contextMenu.menuId=_f;
_e._contextMenu.jsonQuery=_10;
var _11=_e._contextMenu;
var _12=function(_13){
if(_13.displayMenu){
i$.addClass(i$.byId(_11.id),wptheme.contextMenu.css.show);
var _14=i$.byId(_11.id)._firstSelectable;
if(_14){
_14.focus();
i$.byId(_11.id)._currentSelected=_14;
}
}
};
wptheme.contextMenu._initialize(_e).then(_12,_12);
_e=null;
},initSubmenu:function(_15,_16,_17){
_15._contextMenu=_15._contextMenu||{};
_15._contextMenu._submenu=true;
_15._contextMenu._menuitemTemplate=_17._menuitemTemplate;
_15._contextMenu._subMenuTemplate=_17._subMenuTemplate;
_15._contextMenu._loadingTemplate=_17._loadingTemplate;
wptheme.contextMenu.init(_15,_16,_17.jsonQuery);
},_findFocusNode:function(_18){
var _19,i,_1a;
var _1b=function(_1c,_1d){
var l=_1c.childNodes.length;
for(i=0;i<l;i++){
if(_19){
break;
}
_1a=_1c.childNodes[i];
if(i$.hasClass(_1a,wptheme.contextMenu.css.focus)){
_19=_1a;
break;
}
if(_1a.childNodes){
i=_1b(_1a,i);
}
}
return _1d;
};
_1b(_18);
return _19;
},_findNodes:function(_1e){
var _1f,_20,_21,i,_22;
var _23=function(_24,_25){
for(i=_24.childNodes.length-1;i>=0;i--){
_22=_24.childNodes[i];
if(i$.hasClass(_22,wptheme.contextMenu.css.menuTemplate)){
_1f=_22;
continue;
}
if(i$.hasClass(_22,wptheme.contextMenu.css.submenuTemplate)){
_20=_22;
continue;
}
if(i$.hasClass(_22,wptheme.contextMenu.css.loadingTemplate)){
_21=_22;
continue;
}
if(_22.childNodes){
i=_23(_22,i);
}
}
return _25;
};
_23(_1e);
return {"menu":_1f,"submenu":_20,"loading":_21};
},_invalidateCallback:function(){
wptheme.contextMenu.cache={};
},_initialize:function(_26){
var _27=true;
var _28=_26._contextMenu;
if(wptheme.contextMenu.cache[_28.id]||_28._inProgress){
return i$.promise.resolved({displayMenu:_27});
}
_28._inProgress=true;
i$.addListener("wptheme/contextMenu/invalidate/all",wptheme.contextMenu._invalidateCallback);
var _29,_2a,tmp=i$.createDom("div");
if(_28._submenu){
tmp.innerHTML=_28._subMenuTemplate.replace(/\$\{submenu-id\}/g,_28.id+"_menu");
_26.appendChild(tmp.firstChild);
_29=i$.byId(_28.id+"_menu");
_2a=i$.createDom("div");
_2a.innerHTML=_28._loadingTemplate;
}else{
var _2b=wptheme.contextMenu._findNodes(_26);
_29=_2b.menu;
if(!_28._menuitemTemplate){
_28._menuitemTemplate=i$.trim(_29.innerHTML);
}
if(!_28._loadingTemplate){
_2a=i$.createDom("div");
_2a.appendChild(_2b.loading);
_28._loadingTemplate=i$.trim(_2a.innerHTML);
_2a=null;
}
_2a=i$.createDom("div");
_2a.innerHTML=_28._loadingTemplate;
tmp.appendChild(_2b.submenu.cloneNode(true));
if(!_28._subMenuTemplate){
_28._subMenuTemplate=i$.trim(tmp.innerHTML);
}
}
while(_29.firstChild){
_29.removeChild(_29.firstChild);
}
_29.appendChild(_2a);
i$.addClass(_26,wptheme.contextMenu.css.show);
i$.bindDomEvt(i$.byId(_28.id),"onmouseleave",function(){
if(_28._inProgress){
_27=false;
}
var _2c=i$.byId(_28.id);
i$.removeClass(_2c,wptheme.contextMenu.css.show);
if(!_28.activeAction){
var _2d=_2c._currentSelected;
if(_2d){
_2d.blur();
}
var _2e=wptheme.contextMenu._findFocusNode(_2c);
((_2e)?_2e:_2c).focus();
}
});
return wptheme.contextMenu._load(_28).then(function(_2f){
var _30=wptheme.contextMenu._parseData(_2f).then(function(_31){
_31=wptheme.contextMenu._filterMenu(_31);
if(!_31||_31.length==0){
var tmp=i$.createDom("div");
tmp.innerHTML=wptheme.contextMenu._fromTemplate(_28._menuitemTemplate,wptheme.contextMenu.css.error,"No items to display.");
while(_29.firstChild){
_29.removeChild(_29.firstChild);
}
_29.appendChild(tmp);
}else{
wptheme.contextMenu._buildMenu(_28,_29,_31);
}
_28._inProgress=false;
wptheme.contextMenu.cache[_28.id]=true;
return {displayMenu:_27};
});
return _30;
},function(){
var tmp=i$.createDom("div");
tmp.innerHTML=wptheme.contextMenu._fromTemplate(_28._menuitemTemplate,wptheme.contextMenu.css.error,"Error happened while loading the menu.");
while(_29.firstChild){
_29.removeChild(_29.firstChild);
}
_29.appendChild(tmp);
_28._inProgress=false;
wptheme.contextMenu.cache[_28.id]=true;
return {displayMenu:_27};
});
},_load:function(_32){
var _33=_1.replace(/\$\{id\}/g,_32.menuId);
if(_32.jsonQuery){
_33+=(_33.indexOf("?")==-1?"?":"&")+i$.toQuery(_32.jsonQuery);
}
return i$.xhrGet({url:_33,headers:{"X-IBM-XHR":"true"},responseType:"json"}).then(function(_34){
return _34.data;
},function(_35){
var _36=_35.xhr.getResponseHeader("Content-Type")||"";
if((_36.indexOf("text/html")==0)||(_35.xhr.status==401)){
window.setTimeout(function(){
document.location.reload();
},0);
}
console.log("Error trying to load the context menu feed for '"+_32.menuId+"': "+_35);
return null;
});
},_parseData:function(_37){
var _38=[];
i$.each(_37,function(_39){
var _3a=i$.fromPath("moduleInfo.deferred",false,_39)?i$.modules.loadDeferred():i$.promise.resolved(true);
_38.push(_3a.then(function(){
var _3b=wptheme.contextMenu._checkFunction(_39,_39.visibilityFn,_39,(typeof _39.visibility!="undefined")?_39.visibility:true);
var _3c=wptheme.contextMenu._checkFunction(_39,_39.enableFn,_39,(typeof _39.enabled!="undefined")?_39.enabled:true);
return i$.whenAll(_3b,_3c).then(function(_3d){
_39._visible=_3d[0];
_39._enabled=_3d[1];
return _39;
});
}));
});
return i$.whenAll.apply(i$,_38);
},_filterMenu:function(_3e){
var _3f=[],_40,_41={"type":"Separator"};
for(var i=_3e.length-1;i>=0;i--){
_40=_3e[i];
if(!_40._visible){
continue;
}
if(_40.type=="Separator"){
if(_41.type=="Separator"){
continue;
}
}else{
if(_40.type=="Header"){
if((_41.type=="Separator")||(_41.type=="Header")){
continue;
}
}
}
_41=_40;
_3f.unshift(_40);
}
while(_3f.length>0&&_3f[0].type=="Separator"){
_3f=_3f.slice(1);
}
return _3f;
},_buildMenu:function(_42,_43,_44){
var _45=document.createDocumentFragment(),tmp=i$.createDom("div"),_46,_47,_48,_49;
for(var i=0,l=_44.length;i<l;i++){
_46=_44[i];
tmp.innerHTML=wptheme.contextMenu._fromTemplate(_42._menuitemTemplate,_46);
while(_47=tmp.firstChild){
if(_47.nodeType==1){
if(_46.type=="Submenu"){
_47._menuitem=_46;
_47._jsonData=_42;
i$.bindDomEvt(_47,"onmouseover",wptheme.contextMenu._applySubmenu);
}else{
if(_46._enabled){
_47.links={previous:_48,next:null,sub:null};
if(_48){
_48.links.next=_47;
}
if(!_49&&_46.type!="Header"){
_49=_47;
}
_47._menuitem=_46;
_48=_47;
i$.bindDomEvt(_47,"onclick",function(evt){
wptheme.contextMenu._stopEventPropagation(evt);
wptheme.contextMenu._applyAction(evt);
setTimeout(function(){
i$.removeClass(i$.byId(_42.id),wptheme.contextMenu.css.show);
},0);
});
i$.bindDomEvt(_47,"onkeydown",function(evt){
return wptheme.contextMenu._applyKeyAction(evt);
});
i$.bindDomEvt(_47,"onmouseover",function(evt){
return wptheme.contextMenu._applyFocusAction(evt);
});
}
}
if((_46.title)&&(i$.isRTL(_46.title.lang))){
i$.addClass(_47,"rtl");
_47.setAttribute("dir","RTL");
}
}
_45.appendChild(_47);
}
}
while(_43.firstChild){
_43.removeChild(_43.firstChild);
}
_43.appendChild(_45);
i$.byId(_42.id)._firstSelectable=_49;
i$.byId(_42.id)._currentSelected=null;
},_fromTemplate:function(_4a,_4b,_4c){
var _4d,_4e,_4f;
if(typeof (_4b)=="string"){
_4d=_4b;
_4e=_4c;
_4f="";
}else{
_4d="type"+_4b.type;
if(_4b.itemClass){
_4d+=" "+_4b.itemClass;
}
if(!_4b._enabled){
_4d+=" "+wptheme.contextMenu.css.disabled;
}
_4e=(_4b.title)?_4b.title.value:"";
_4f=((_4b.description)?_4b.description.value:"");
}
return _4a.replace(/\$\{title\}/g,_4e).replace(/"\$\{css-class\}"/g,"\""+(_4d)+"\"").replace(/\$\{css-class\}/g,"\""+(_4d)+"\"").replace(/"\$\{description\}"/g,"\""+_4f+"\"").replace(/\$\{description\}/g,"\""+_4f+"\"");
},_checkFunction:function(_50,fn,arg,_51){
if(fn){
if(!_50.fromPath){
_50.fromPath={};
}
var _52=_50.fromPath[fn]||i$.fromPath(fn);
_50.fromPath[fn]=_52;
if(i$.isFunction(_52)){
try{
return _52(arg);
}
catch(exc){
console.log("error executing function "+fn+" - "+exc);
}
}
}
return i$.promise.resolved(_51);
},_stopEventPropagation:function(evt){
if(evt){
if(evt.stopPropagation){
evt.stopPropagation();
}else{
evt.cancelBubble=true;
}
}
},_applyKeyAction:function(evt){
var _53=evt.target||evt.srcElement;
var _54=_53;
var _55=null;
while(!_55){
_54=_54.parentNode;
if(_54._contextMenu){
_55=_54;
}
}
var _56=_55._contextMenu;
switch(evt.keyCode){
case 13:
wptheme.contextMenu._stopEventPropagation(evt);
i$.removeClass(i$.byId(_56.id),wptheme.contextMenu.css.show);
var _57=wptheme.contextMenu._findFocusNode(_55);
((_57)?_57:_55).focus();
window.setTimeout(function(){
wptheme.contextMenu._applyAction(evt);
},0);
return false;
case 9:
case 27:
i$.removeClass(i$.byId(_56.id),wptheme.contextMenu.css.show);
var _57=wptheme.contextMenu._findFocusNode(_55);
((_57)?_57:_55).focus();
break;
case 40:
wptheme.contextMenu._moveFocus(evt,_56,_53,"next");
return false;
case 38:
wptheme.contextMenu._moveFocus(evt,_56,_53,"previous");
return false;
}
return true;
},_moveFocus:function(evt,_58,_59,_5a){
var _5b=_59.links[_5a];
if(_5b&&(_5b._menuitem.type=="Header"||_5b._menuitem.type=="Separator")){
var _5c=false;
var _5d=null;
while(!_5d&&!_5c){
_5b=_5b.links[_5a];
if(!_5b){
_5c=true;
}else{
if(_5b._menuitem.type!="Header"&&_5b._menuitem.type!="Separator"){
_5d=_5b;
}
}
}
_5b=_5d;
}
if(_5b){
var _5e=i$.byId(_58.id)._currentSelected;
if(_5e){
_5e.blur();
}
i$.byId(_58.id)._currentSelected=_5b;
_5b.focus();
}
if(evt.preventDefault){
evt.preventDefault();
}
},_applyFocusAction:function(evt){
var _5f=evt.target||evt.srcElement;
var _60=_5f;
var _61=null;
var _62=_5f._menuitem;
while(!_61){
_60=_60.parentNode;
if(_60._contextMenu){
_61=_60;
}
if(!_62){
_5f=_5f.parentNode;
_62=_5f._menuitem;
}
}
var _63=_61._contextMenu;
var _64=i$.byId(_63.id)._currentSelected;
if(_64!=_5f){
if(_64){
_64.blur();
i$.byId(_63.id)._currentSelected=null;
}
if(_62.type!="Header"&&_62.type!="Separator"){
i$.byId(_63.id)._currentSelected=_5f;
_5f.focus();
}
}
return false;
},_applyAction:function(evt){
var _65=evt.target||evt.srcElement;
var _66=_65;
var _67=null;
var _68=_65._menuitem;
while(!_67){
_66=_66.parentNode;
if(_66._contextMenu){
_67=_66;
}
if(!_68){
_65=_65.parentNode;
_68=_65._menuitem;
}
}
var _69=_67._contextMenu;
_69.activeAction=true;
var p=wptheme.contextMenu._checkFunction(_68,_68.actionFn,_68,_68.actionUrl);
if(p){
p.then(function(_6a){
if(_6a&&i$.isString(_6a)){
var _6b=_68.actionHttpMethod||"GET";
if(_6b!="GET"){
var _6c=i$.createDom("form");
_6c.setAttribute("action",_6a);
_6b=_6b.toLowerCase();
switch(_6b){
case "get":
_6c.setAttribute("method","GET");
break;
case "delete":
case "put":
var _6d=i$.createDom("input",{"type":"hidden","name":"x-method-override","value":_6b.toUpperCase()});
_6c.appendChild(_6d);
case "post":
_6c.setAttribute("method","POST");
_6c.setAttribute("enctype","multipart/form-data");
break;
default:
}
document.body.appendChild(_6c);
_6c.submit();
}else{
window.location.href=_6a;
}
}
});
}
},_applySubmenu:function(evt){
var _6e=evt.target||evt.srcElement;
if(!_6e._jsonData){
_6e=_6e.parentNode;
}
if(_6e._jsonData){
_6e.setAttribute("id",_6e._jsonData.id+"_"+_6e._menuitem.id);
wptheme.contextMenu.initSubmenu(_6e,_6e._menuitem.id,_6e._jsonData);
}
}}});
})();

(function(){
var _1=false;
if(typeof wptheme==="undefined"||!wptheme){
wptheme={};
}
i$.mash(wptheme,{togglePageMode:function(_2,_3,_4,_5){
var _6=i$.byId("wpthemeModeToggle");
i$.addClass(_6,"wpthemeEditControlLoading");
return i$.modules.loadDeferred().then(function(){
var _7=i$.fromPath("com.ibm.mashups"),_8=_7?com.ibm.mashups.builder.model.Factory.getRuntimeModel():null,_9=_7?com.ibm.mashups.enabler.user.Factory.getUserModel():null,_6=i$.byId("wpthemeModeToggle"),_a=i$.byId("wpthemeModeToggleImg"),_b=i$.byId("wpthemeModeToggleAccess"),_c=i$.byId("wpthemeModeToggleAltText"),_d=document.getElementsByTagName("body")[0],_e=function(_f){
if(_7){
com.ibm.mashups.services.ServiceManager.getService("eventService").broadcastEvent("com.ibm.mashups.builder.changePageMode",_f);
_8.getCurrentPage().setPageMode(_f);
}
i$.fireEvent("wptheme/contextMenu/invalidate/all");
i$.removeClass(_6,"wpthemeEditControlLoading");
};
if((!_7&&!i$.hasClass(_d,"edit-mode"))||(_7&&_9.getAnonymousMode()!=com.ibm.mashups.enabler.user.AnonymousMode.ANONYMOUS&&_8.getCurrentPage().getPageMode()!="edit")){
_6.title=_a.alt=_3;
_b.innerHTML=_4;
_c.innerHTML=_3;
_e("edit");
i$.addClass(_d,"edit-mode");
if(!_1){
if(!i$.isIE&&!i$.isOpera&&_7){
window.onbeforeunload=function(){
if(com.ibm.mashups.builder.model.Factory.getRuntimeModel().getCurrentPage().isDirty()){
return com.ibm.mm.builder.coreWidgetsStrings.I_PAGE_SAVE_WARNING;
}
};
}
_1=true;
}
}else{
_6.title=_a.alt=_2;
_b.innerHTML=_5;
_c.innerHTML=_2;
_e("view");
i$.removeClass(_d,"edit-mode");
}
},function(err){
console.log("Error: "+err);
});
}});
})();

// create a test node off the browser screen to calculate high contrast mode
var testNode = document.createElement("div");
testNode.className = "highContrastTestNode";
document.body.appendChild(testNode);
// look at the computed style for the test node
var styles = null;
try {
	styles = document.defaultView.getComputedStyle(testNode, "");
} catch(e) {
	styles = testNode.currentStyle;
}
var testImg = styles.backgroundImage;
if ((styles.borderTopColor == styles.borderRightColor) || (testImg != null && (testImg == "none" || testImg == "url(invalid-url:)" ))) {
	document.getElementsByTagName("body")[0].className+=" lotusImagesOff";
}
document.body.removeChild(testNode);
