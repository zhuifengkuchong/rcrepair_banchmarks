function getSummary(the_field) {
	try {
		var browserName = navigator.appName;
		var theField = document.getElementById(the_field);
		var theVal = theField.innerHTML;
		theVal = theVal.replace(/(<([^>]+)>)/ig, "");
		theVal = theVal.replace("&nbsp;", "");
		var char_count = theVal.length;
		var fullStr = theVal + " ";
		var initial_whitespace_rExp = /^[^A-Za-z0-9]+/gi;
		var left_trimmedStr = fullStr.replace(initial_whitespace_rExp, "");
		var non_alphanumerics_rExp = rExp = /[^A-Za-z0-9]+/gi;
		var cleanedStr = left_trimmedStr.replace(non_alphanumerics_rExp, " ");
		if (cleanedStr.length >= 90) {
			cleanedStr = cleanedStr.substring(0, 90);
		}
		var splitString = cleanedStr.split(" ");
		var summary = "";
		for (i = 0; i < splitString.length; i++) {
			if (i == 15) {
				break;
			}
			if (splitString[i] + "" != "") {
				summary += splitString[i] + " ";
			}
		}
		if (summary != "") {
			summary = summary + "..."
		}

		if (browserName == 'Netscape') {
			theField.innerHTML = summary;
		} else {
			theField.innerText = summary;
		}

		theField.style.visibility = 'visible';
		theField.className = 'navigation';
	} catch (err) {
	}
}

function checkRequestParameter(name, value) {
	// this function parses the url and returns true if
	// the 'name' and 'value' pair are found in the url
	// as request parameters

	var paramFound = false;

	var query = window.location.search.substring(1);
	var vars1 = query.split("&");
	var vars2 = query.split("?");
	var vars = vars1.concat(vars2)

	for (var i in vars) {
  	var pair = vars[i].split("=");
  		if (pair[0] == name) {
    			if (pair[1] == value) {
        			paramFound = true;
    			}
  		}
	}

return paramFound;
}

function parseURL(url) {
    var a =  document.createElement('a');
    a.href = url;
    return {
        source: url,
        protocol: a.protocol.replace(':',''),
        host: a.hostname,
        port: a.port,
        query: a.search,
        params: (function(){
            var ret = {},
                seg = a.search.replace(/^\?/,'').split('&'),
                len = seg.length, i = 0, s;
            for (;i<len;i++) {
                if (!seg[i]) { continue; }
                s = seg[i].split('=');
                ret[s[0]] = s[1];
            }
            return ret;
        })(),
        file: (a.pathname.match(/\/([^\/?#]+)$/i) || [,''])[1],
        hash: a.hash.replace('#',''),
        path: a.pathname.replace(/^([^\/])/,'/$1'),
        relative: (a.href.match(/tps?:\/\/[^\/]+(.+)/) || [,''])[1],
        segments: a.pathname.replace(/^\//,'').split('/')
    };
}

function showNav(){
   var breadCrumbs = $("#breadcrumbNav").children();
   if(breadCrumbs.length == 1){
       $("#navLevel1").css('display','block');
    }else if(breadCrumbs.length == 2){
         $("#navLevel2").css('display','block');
    }else{
        $("#navLevel3").css('display','block');
    }
	
	var fourthLevel = $("navHdr3-4").find("div");
	if(fourthLevel){
		var thirdLevel = $("navHdr3-3").find("div");
		thirdLevel.each(function (i) {
			curDiv = $(this);
			curDiv.css('margin-left','0');
			curDiv.css('font-size', '12px');
		});
	}
	
}

function buildLinks(whichMenu){
	var theLinks = $("#"+whichMenu).find("div");
	theLinks.each(function (i) {
        curDiv = $(this);
		if(curDiv.attr('class') != 'menuMore'){
			curAnchor = $(curDiv).find('a');
		
			var curHref = curAnchor.attr('href');
			var newHref = curHref.replace('/wps/PA_WCMLRingPortJSR286', '').replace('/uss/PA_WCMLRingPortJSR286', '').replace('/myportal', '');
			var locArray = newHref.replace(/^\//,'').split('/');
		
			if(locArray.length > 5){
				newHref = newHref.replace('&page=pressreleases&','&page='+locArray[4].replace(/\+/g, '')+'&');
			}else{
				newHref = newHref.replace('&page=pressreleases&','&page='+locArray[locArray.length - 1].replace(/\+/g, '')+'&');
			}
			curAnchor.attr('href', newHref);
		}
	});
}