/**
 * IE7 -8 JavaScript to handle the new navigation requirements.
 * 8/5/2011 - Liberty Mutual
 */

var ieNav = (function($){
	//Fixes an IE bug where the megamenu and global menu stay open on browser back button - if they were open on page change.
	$('#megaMenuContainer a').click(function(){
		$('#megaMenuContainer li.sfHover a').blur();
	});
	
	$('#globalContainer a').click(function(){
		$('#globalContainer li.sfHover a').blur();
	});
})(jQuery);

if(navigator.userAgent.indexOf('MSIE') != -1 && (document.documentMode == 7 || document.documentMode == 8)){
	var paperSlips = (function($){
		var orangeHeight = 30 + $('#centerTop .centerBackground').height();
		var blueHeight1 = 10 + $('.twoByCenterNum1 .twoByText').height();
		var blueHeight2 = 10 + $('.twoByCenterNum2 .twoByText').height();
		var greenHeight = 30 + $('#centerBottom .centerBackground').height();
		
		$('.orangeSlip').append('<img src="../../images/yellowSlip.png"/*tpa=https://www.libertymutual.com/images/yellowSlip.png*/ height="'+orangeHeight+'px" width="19px"/>');
		$('.twoByCenterNum1 .blueSlip').append('<img src="../../images/blueSlip.png"/*tpa=https://www.libertymutual.com/images/blueSlip.png*/ height="'+blueHeight1+'px" width="19px"/>');
		$('.twoByCenterNum2 .blueSlip').append('<img src="../../images/blueSlip.png"/*tpa=https://www.libertymutual.com/images/blueSlip.png*/ height="'+blueHeight2+'px" width="19px"/>');
		$('.greenSlip').append('<img src="../../images/greenSlip.png"/*tpa=https://www.libertymutual.com/images/greenSlip.png*/ height="'+greenHeight+'px" width="19px"/>');
	})(jQuery);
}