  // BRANDS MAP NAV
  
          $(function(){
                    $(brands).each(function(idx,item) {
                        var anchor = $('a#' + item.slug);
                        if(anchor) {
                                anchor.data('slug', item.slug);
                                anchor.data('color', item.color);
                                anchor.data('countries', item.countries);
                                anchor.data('locations', item.locations);
                        }
                    });
                    
                
                $('body').on('mouseenter mouseleave', '.marker', function(event){
                    if(event.type == "mouseenter") {
                        $(this).find('.marker-info').show();
                        $(this).addClass('marker-info-above');
                    } else { 
                       $(this).find('.marker-info').hide();
                       $(this).removeClass('marker-info-above');
                    }
                });
                
                
                
                $('body').on('click', '#brands-map-nav li > a', function(){
                
                    // Slide down
                    var slug = $(this).attr('id');
                    
                     var mapp = $('.' + slug + '-area').vectorMap('get', 'mapObject');
                     mapp.setFocus(0.9, 0, 0);
                     
                    
                    var hasCountries = $(this).data('countries') != 'undefined'  || $(this).data('countries').length > 0;
                    var $clickedItem = $(this).parent();
                    var navHeight = 0;
                    var $clone = $clickedItem.clone();
                    $clickedItem.css({overflow: 'hidden'})
                    $('.map-name').html($(this).text());
                    
                    if(hasCountries) { 
                        $('.map-area').not('.map-empty').fadeOut('slow');
                        $('.map-area.' + slug + '-area').fadeIn('slow');
                    }
                    
                    $('.markers').fadeOut();
                    $('#' + slug + '-markers').fadeIn();
                    

                    if (!$clickedItem.hasClass('open')) {
                        
                        $('#brands-map-nav li').removeClass('open');
                        
                        if($(window).width() >= 768) {
                          
                          $clickedItem.slideUp(300, 'linear');
                    
                          $clone.appendTo($('#brands-map-nav ul')).hide().slideDown(300, 'linear', function(){
                        
                            $clone.replaceWith($clickedItem);
                            $clickedItem.show().css({overflow: 'visible'});
                            
                            // Slide left/right
                            var iconsWidth = $clickedItem.find('.icons').outerWidth();
                            $('.icons-wrap').stop().animate({
                              width: 0
                            }, 500, function() {  $('#brands-map-nav li').not($clickedItem).css({minWidth: 25});  });
                            $clickedItem.css({minWidth: 25 + iconsWidth});
                            $clickedItem.find('.icons-wrap').stop().animate({
                              width: iconsWidth
                            }, 500);
                            
                            
                          });
                          
                        } else { 
                            $clickedItem.animate({width: 0}, 300);
                            $clone = $clickedItem.clone();
                            $clone.css({ width : 0});
                            
                            $clone.appendTo($('#brands-map-nav ul')).animate({width: '25px'}, 300, function(){
                                $clone.replaceWith($clickedItem);
                                $clickedItem.css({width: 25, overflow: 'visible'});
                            });
                            
                            
                        }   
                        
                        $clickedItem.addClass('open');
                  
                     }

                    return false;
                  });
                        
          });
        
        function prepairMaps(selected) {
                $('.map-area').html('');
            
                $(brands).each(function(idx, item){
                    var $container = $('.map-area.' + item.slug + '-area');
                    $container.show();
                    
                    
                    var m = new jvm.WorldMap({
                        map: 'world_merc_en',
                        container: $container,
                        backgroundColor: '#000',
                        zoomMin: 0.9,
                        zoomMax: 30,
                        focusOn: {
                            x: 0,
                            y: 0,
                            scale: 0.9
                        },
                        regionStyle: {
                            initial: {
                                fill: '#313539',
                                stroke : '#313539',
                                'stroke-width': 1.5,
                                'stroke-opacity': 1
                            }
                        },
                        series: {
                            regions: [{
                                    values: item.countries,
                                    attribute: { stroke: item.color, fill: item.color, 'stroke-opacity': 1, 'stroke-width': 1.5 },
                                    normalizeFunction: 'polynomial'
                                }]
                        },
                        onViewportChange: function() { drawLocations(item.slug, item.locations); },
                        onRegionOver: function(e) { return false; }
                    });  
                    
                    $container.hide();
                
                });
                
            
            if(selected) $('#' + selected).click();
            else $('#brands-map-nav .brands-nav #priceline').last().click();
        }
        
        function drawLocations(slug,locations, m) {
                var $container = $('.map-area.' + slug + '-area');
                
                var m = $container.vectorMap('get', 'mapObject');
                $('#' + slug + '-markers').html('');
                    $(locations).each(function(index, marker){
                        var $newMarker = $('#brands-map > .marker').clone();
                        $newMarker.addClass('marker-' + marker.type);
                        $newMarker.addClass('marker-lines' + marker.lineCount);
                        $newMarker.find('.marker-info').html(marker.customData).hide();
                        
                        var cords = m.latLngToPoint(marker.latitude, marker.longitude);
                        $newMarker.css({left: cords.x, top: cords.y });
                        $newMarker.appendTo('#' + slug + '-markers');
                        
                });

        }