/* 
 * Global JS for General Dynamics
 * Phillip Salfity (psalfity@matrixgroup.net) 
 * 	-Equal Height Cloumns/Elements
 * 	-Autoscrolling
 *  -FAQ Table of Contents
 */
 document.documentElement.className = document.documentElement.className.replace(/\bno-js\b/,'js');
(function($) {
	/** Equal Heights **/
	$.fn.equalHeights = function(minHeight, maxHeight) {
		tallest = (minHeight) ? minHeight : 0;
		this.each(function() {
			if($(this).height() > tallest) {
				tallest = $(this).height();
			}
		});
		if((maxHeight) && tallest > maxHeight) tallest = maxHeight;
		return this.each(function() {
			$(this).height(tallest); //.css("overflow","auto")
		});
	}
	
	/** Automatic Scrolling **/
	$.fn.doScroll = function() {
		$('html,body').animate({
			scrollTop: $(this).offset().top
		}, 0);	
	}
	/** Add class to second item **/
	$.fn.addClassSecond = function() {
		this.each(function () {
			$(this).children(':eq(1)').addClass('second');
		});
	}
	
	
	/** Add class to even item **/
	$.fn.addClassEven = function() {
		this.each(function () {
			$(this).children(':even').addClass('even');
		});
	}

})(jQuery);
 
$(document).ready(function() { 
	
	var thePath = location.pathname;	
	
/** Fade in Branding Image **/
	$('img#branding').hide().fadeIn("slow");

/** Make Sure Page Is At Least As Tall As SubNav **/
	var subnavheight = 10 + $('.subnav').height();
	var mainheight = $('#main').height();
	var anchorsheight = $('.anchors').height();
	var leftSide = subnavheight + anchorsheight;
	if (subnavheight>mainheight) {$(document).ready(function() {$('#main').css("height", subnavheight);});}
	if (leftSide>mainheight) {$(document).ready(function() {$('#main').css("height", leftSide);});}
	
/** FAQ Table of Contents **/	
	$('<a href="javascript:void(0);" class="backTop" style="margin: 0 0 20px 0; text-align: right;">Return to top</a>').appendTo('div.faq #main .section div');
	
	var doingB = $("#doing-business h4").text();
	$('#toc').append('<h4>'+doingB+'</h4>');
	$('#doing-business h5').each(function(i) {
		$(this).attr('id','doing-business'+i);
		$('#toc').append('<a href="javascript:void(0)" class="doing-business'+i+'">'+$(this).text()+'</a>');
			var theName = 'doing-business';
			var num = i;
			var almostVal = theName + num;
			var idVal = "#" + almostVal;
			var classVal = "#toc a." + almostVal;
			$(classVal).each(function(){
				$(this).click(function(){
					$(idVal).doScroll();
				});
			});
	});
	
	var reqS = $("#requirements-selection h4").text();
	$('#toc').append('<h4>'+reqS+'</h4>');
	$('#requirements-selection h5').each(function(i) {	
		$(this).attr('id','requirements-selection'+i);
		$('#toc').append('<a href="javascript:void(0)" class="requirements-selection'+i+'">'+$(this).text()+'</a>');
			var theName = 'requirements-selection';
			var num = i;
			var almostVal = theName + num;
			var idVal = "#" + almostVal;
			var classVal = "#toc a." + almostVal;
			$(classVal).each(function(){
				$(this).click(function(){
					$(idVal).doScroll();
				});
			});
	});
		
	
	var regN = $("#registration h4").text();
	$('#toc').append('<h4>'+regN+'</h4>');
	$('#registration h5').each(function(i) {
		$(this).attr('id','registration'+i);
		$('#toc').append('<a href="javascript:void(0)" class="registration'+i+'">'+$(this).text()+'</a>');
			var theName = 'registration';
			var num = i;
			var almostVal = theName + num;
			var idVal = "#" + almostVal;
			var classVal = "#toc a." + almostVal;
			$(classVal).each(function(){
				$(this).click(function(){
					$(idVal).doScroll();
				});
			});
	});
	
	//Stripe the rows
	$('#toc a:odd, div.faq #main .section div:odd').addClass('odd');
	
/** Automatic Scrolling **/
	//To Top
	$('.backTop').click(function(event){event.preventDefault();$('html, body').animate({scrollTop: 0}, 50);});
	//SkipLinks
	$('#skipLinks a').click(function(event){event.preventDefault();var theLink = $(this).attr('href');$(theLink).doScroll();});
	
/** Run Equal Heights **/
	//Footer
	$('#contentinfo .footer-block').equalHeights();

	//Home Page Content Area
	$('#main .block, #main .four-block, #main .four-col, #main .grid').equalHeights();

	//Monopoly Cards
	$('#main .halfCol, #main .card .bottomCard').equalHeights();

	//Contact Lists
	$('#main ul.contactListX').equalHeights();
	
/** Classes **/
	//External Links
	$("#main a[href*='http://']:not([href*='"+window.location.hostname+"'])").addClass("ExternalLink");
	
	$("#main a[href*='http://www.generaldynamics.com/scripts/gdcareers.com']").addClass("exit-text");
	$("#main a[href*='http://www.generaldynamics.com/scripts/cvmas13.cvmsolutions.com']").addClass("exit-text");
	
	//Internal Links
  	$("#main a[href*='/.*'], #main a[href*='"+window.location.hostname+"']").addClass("InternalLink");
	
	//Tables
	$('table').each(function(){$('tbody tr:odd').addClass('alt');});
	
	//Other
	$(".businessgroup").addClassEven();
	$(".businessgroup").addClassSecond();	

/** Push anchors below subnav **/
	var subH = $(".subnav").height();+"px";
	$('#main .anchors').css('top',subH);

/** Form Handling **/
	
	//Clear on first focus
	$('.footer-search input[type=text], #contact-form input[type=text]').one("focus", function(){$(this).attr('value','');});
	$('#contact-form textarea').one("focus", function(){$(this).empty();});
	
	
/** Press Releases Image Resizing with contrained proportions **/
	var isPressRelease = thePath.indexOf('http://www.generaldynamics.com/news/press-releases/detail.cfm');
	if(isPressRelease>=0){
		$('#pressrelease .item img').each(function(){
			var iHeight = $(this).height();
			var iWidth = $(this).width();
			var mainWidth = $('#main').width();
			var mainPads = mainWidth - 10;
			if(iWidth >= mainPads){
				$(this).each(function(){
					var ratio = iWidth/iHeight;
					var resizeW = mainPads * 0.95;
					var resizeH = resizeW / ratio;
					$(this).attr('width',resizeW).attr('height',resizeH);					
				});
			}
			
		});
	}
	
/** Generics **/
	//Turn up the fx FPS to run smoother
	jQuery.fx.interval = 100;
	
	/* Working copy of superscript trademark */
	var testing = $('#main h1:first').html();
	if (testing && testing.match(/strength.*/i)){
		var supp = '<h1>Strength On Your Side<sup>&reg;</sup></h1>';
		var rgx = testing.replace(/strength.*/i, supp);
		$('#main h1').replaceWith(rgx);
	} else { return false; }

	
	
		
	
});

	function validateForm() {
		if( (document.getElementById('phone').value == '') && (document.getElementById('email').value == '') ){
			var theW = $('#error').width();
			$('#error').css({'display' : 'block', 'width' : theW});
			return false;
		}
		return true;
	}
