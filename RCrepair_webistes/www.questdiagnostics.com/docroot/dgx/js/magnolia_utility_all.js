// --------------Start Video Code ------------------//

$(document).ready(function(){
	try{
		var d = $('video');
		for(i=0;i<d.length;i++){
			d[i].id="video_"+i;
		}
		setTimeout( function(){
			var k = $('video');
			for(j=0;j<k.length;j++){
				jwplayer(k[j].id).setup({
					'height': '480', 
					'width': '640', 
					'autostart':'true',
					'modes': [
						{ type: "flash", src: pageContextPath+"/docroot/dgx/js/player.swf" },
						{ type: "html5" },
						{ type: "download" }
					] 
				}); 
			}
		}, 500 );
	}catch(err){}
});

$(document).ready(function(){
	var url = document.location.pathname;
	var uri = url.substring(url.indexOf("/home")+6,url.length);

	if(uri.indexOf('/')>0)
	{
		uri = uri.substring(0,uri.indexOf("/"))
	}
	else if (uri.indexOf('.')>0)
	{
		uri=uri.substring(0,uri.indexOf("."))
	}
	else
	{
		uri=uri.substring(0,url.length)
	}
	
	
	if(uri=="patients"){$('#nav-patients a').addClass('active');}
	else if(uri=="physicians"){$('#nav-physicians a').addClass('active');}
	else if(uri=="companies"){$('#nav-companies a').addClass('active');}
	else if(uri=="contact"){$('#nav-contact a').addClass('active');}
	else{$('#nav-home a').addClass('active');}
});

$(document).ready(function(){
	var objHeight=0,objWidth=0;
	$('.jwbox').children().each(function(index, element){
		if(this.className=='jwboxPosterImg'){
			objHeight = $(this).height();
			objWidth = $(this).width();
		}
		if(this.className=='jwboxPlayButton'){
			if(objHeight>0){
				var topPoint = ((objHeight+$(this).height())/2)*(-1);
				$(this).css('top',topPoint+'px');}
			if(objWidth>0){
				var leftPoint = ((objWidth-$(this).width())/2);
				$(this).css('left',leftPoint+'px');}
		}
		if(this.className=='jwboxPlayButtonNew'){
			if(objHeight>0){
				var topPoint = ((objHeight-$(this).height())/2);
				$(this).css('top',topPoint+'px');}
			if(objWidth>0){
				var leftPoint = ((objWidth+$(this).width())/2)*(-1);
				$(this).css('left',leftPoint+'px');}
		}
	});
	setTimeout( function() {
		$('.jwboxPlayButton').css('display','block');
		$('.jwboxPlayButtonNew').css('display','block');
		$('.vidText').css('display','block');
	}, 500 );
});


// --------------Start Search Code ------------------//
function redirectToSearch(val){
	window.location=pageContextPath+"/home/search.html?keyWord="+encodeURIComponent(val);
}
function searchResult(){
	var ifr = document.getElementById('iframe-searchresults');
	var keyword = document.getElementById('header-search-text').value;
	ifr.src = pageContextPath+"/search/magnolia.action?keyWord="+keyword;
}
function parseURL(){
	var uri = document.location.search;
	uri=decodeURI(uri);
	var urlparam = (uri).replace("?","");
	if(urlparam.indexOf('&')>0){
	urlparam = urlparam.substring(0,urlparam.indexOf("&"));
	}
	if(uri.indexOf('?')<0){
		urlparam="keyWord=";
	}
	if(uri.indexOf('?keyWord=')>=0){
		document.getElementById('header-search-text').value = (urlparam).replace("keyWord=","");
	}
	var ifr = document.getElementById('iframe-searchresults');
	var keyword = document.getElementById('header-search-text').value;
	ifr.src = pageContextPath+"/search/magnolia.action?keyWord="+encodeURIComponent(keyword);
}

// --------------Start Cookie Code ------------------//
function parseCookieURL(){
	var url = document.location.pathname;
	var uri = url.substring(url.indexOf("/home")+6,url.length);
	uri = (uri.indexOf('/')>0)?uri.substring(0,uri.indexOf("/")):uri.substring(0,uri.indexOf("."));
	if(uri=="patients" ||uri=="physicians")
	initDefaultHomeCookie("smartDefaultHomePage",uri);
}
function parseSpecialityURL(){
	var url = document.location.pathname;
	if(url.indexOf("/testing-services/condition")>=0){
		var uri = url.substring(url.indexOf("/testing-services/condition")+28,url.length);
		var spe = (uri.indexOf('/')>0)?uri.substring(0,uri.indexOf("/")):uri.substring(0,uri.indexOf("."));
		if(spe.length>0){createCookie("specialityIdentified",spe,365);}
	}
}
function createCookie(name,value,days){
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}
function readCookie(name) {
	var patientHome=0, physicianHome=0;
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}
function initDefaultHomeCookie(Cname,Pname) {
	var cookData = readCookie(Cname);
	if(!cookData){
		if(Pname=="patients")
			createCookie(Cname,"PhysicianHome-0_#_PatientHome-1",365);
		else if(Pname=="physicians")
			createCookie(Cname,"PhysicianHome-1_#_PatientHome-0",365);
		else
			createCookie(Cname,"PhysicianHome-0_#_PatientHome-0",365);
		return false;
	}
	var cookieData = cookData.split('_#_');
	for(var j=0; j < cookieData.length; j++) {
		var homeCookieData = cookieData[j].split('-');

		if(cookieData[j].indexOf("PhysicianHome")==0){physicianHome=parseInt(homeCookieData[1]);}
		if(cookieData[j].indexOf("PatientHome")==0){patientHome=parseInt(homeCookieData[1]);}
	}
	if(Pname=="patients" && patientHome < 5){
		patientHome ++;
		if((patientHome + physicianHome) > 5){physicianHome--;}
	}
	if(Pname=="physicians" && physicianHome < 5){
		physicianHome ++;
		if((physicianHome + patientHome) > 5){patientHome--;}
	}
	cookData = "";
	cookData = "PhysicianHome-"+physicianHome+"_#_PatientHome-"+patientHome;
	createCookie(Cname,cookData,365);
	return true;
}

parseCookieURL();
parseSpecialityURL();

// --------------Start Mail Code ------------------//

function sendmail(){
	var eml="";
	var bodyContent="Hello, %0D%0A\n%0D%0A\nHere is some great information I wanted to share with you from the Quest Diagnostics website. To read it, visit \n"
	var bod="&body="+bodyContent+document.location;
	var subj="?subject=I thought you might be interested in this information from the Quest Diagnostics website. \n" +document.location;
	location.href="mailto:"+eml+subj+bod;
}

// --------------Start External Link Code -----------//

 function extConfirmation(url){ 
   var answer = confirm("You are now leaving the Quest Diagnostics web site. Quest Diagnostics does not control the site you are about to enter and accepts no responsibility for its content.");
   if (answer){
      var WinTest = window.open(url);
	  if (! WinTest){window.location.href=url;}
	}
 }

$('a').click(function(e){
	if($(this).attr('href')){
		var link=$(this).attr('href').toLowerCase();
		var hostname=window.location.hostname;
		if(!$(this).parents('div').hasClass('actions')){
			if(!(link.indexOf(hostname)>0) && lookInQuestFamily(link)){
				var status=link.indexOf("http");
				if(status==0){
					e.preventDefault();
					extConfirmation($(this).attr('href'));
				}
			}
		}
	}
});

$('a').click(function(e){
	if($(this).attr('href')){
		var link=$(this).attr('href').toLowerCase();
		if((link.indexOf('dms')>0) && (link.indexOf('.pdf')>0)){
			$(this).attr('target','_blank');	
		}
	}
});

var questFamily = ['http://www.questdiagnostics.com/docroot/dgx/js/questdiagnostics.com','http://www.questdiagnostics.com/docroot/dgx/js/questdiagnostics1.webex.com','http://www.questdiagnostics.com/docroot/dgx/js/questdiagnostics2.webex.com','http://www.questdiagnostics.com/docroot/dgx/js/care360ehr.webex.com','http://www.questdiagnostics.com/docroot/dgx/js/questdiagnosticsfeedback.com','http://www.questdiagnostics.com/docroot/dgx/js/care360.com','http://www.questdiagnostics.com/docroot/dgx/js/zynite.com','http://www.questdiagnostics.com/docroot/dgx/js/examone.com','http://www.questdiagnostics.com/docroot/dgx/js/employersolutions.com','http://www.questdiagnostics.com/docroot/dgx/js/mygazelleapp.com','http://www.questdiagnostics.com/docroot/dgx/js/quest.airhealthgroup.com','http://www.questdiagnostics.com/docroot/dgx/js/questdiagnostics.co.uk','http://www.questdiagnostics.com/docroot/dgx/js/questdiagnostics.ie','http://www.questdiagnostics.com/docroot/dgx/js/questdiagnostics.mediaroom.com','http://www.questdiagnostics.com/docroot/dgx/js/survey1.burke.com','http://www.questdiagnostics.com/docroot/dgx/js/ameripath.com','http://www.questdiagnostics.com/docroot/dgx/js/associatedclinicallabs.com','http://www.questdiagnostics.com/docroot/dgx/js/athenadiagnostics.com','http://www.questdiagnostics.com/docroot/dgx/js/bhlinc.com','http://www.questdiagnostics.com/docroot/dgx/js/blueprintforwellness.com','http://www.questdiagnostics.com/docroot/dgx/js/care360tour.com','http://www.questdiagnostics.com/docroot/dgx/js/colovantage.com','http://www.questdiagnostics.com/docroot/dgx/js/compunetlab.com','http://www.questdiagnostics.com/docroot/dgx/js/corporate-ir.net','http://www.questdiagnostics.com/docroot/dgx/js/dermpath.com','http://www.questdiagnostics.com/docroot/dgx/js/dermpathdiagnostics.com','http://www.questdiagnostics.com/docroot/dgx/js/dlolab.com','http://www.questdiagnostics.com/docroot/dgx/js/doyouhavetheguts.com','http://www.questdiagnostics.com/docroot/dgx/js/drcsurveys.com','http://www.questdiagnostics.com/docroot/dgx/js/enterix.com.au','http://www.questdiagnostics.com/docroot/dgx/js/focusdx.com','http://www.questdiagnostics.com/docroot/dgx/js/hemocue.com','http://www.questdiagnostics.com/docroot/dgx/js/hemocue.se','http://www.questdiagnostics.com/docroot/dgx/js/insuretest.com','http://www.questdiagnostics.com/docroot/dgx/js/labcard.com','http://www.questdiagnostics.com/docroot/dgx/js/labcardselect.com','http://www.questdiagnostics.com/docroot/dgx/js/labone.com','http://www.questdiagnostics.com/docroot/dgx/js/labreference.com','http://www.questdiagnostics.com/docroot/dgx/js/macl1.com','http://www.questdiagnostics.com/docroot/dgx/js/medplus.com','http://www.questdiagnostics.com/docroot/dgx/js/MedPlusCentergy.com','http://www.questdiagnostics.com/docroot/dgx/js/mycare360health.com','http://www.questdiagnostics.com/docroot/dgx/js/mylabisquest.com','http://www.questdiagnostics.com/docroot/dgx/js/nicholsinstitute.com','http://www.questdiagnostics.com/docroot/dgx/js/omega-labs.com','http://www.questdiagnostics.com/docroot/dgx/js/oraldna.com','http://www.questdiagnostics.com/docroot/dgx/js/pathwaydx.com','http://www.questdiagnostics.com/docroot/dgx/js/quest4health.com','http://www.questdiagnostics.com/docroot/dgx/js/questcentralab.com','http://www.questdiagnostics.com/docroot/dgx/js/questdiagnostics.ie','http://www.questdiagnostics.com/docroot/dgx/js/questdiagnostics.in','http://www.questdiagnostics.com/docroot/dgx/js/sonoraquest.com','http://www.questdiagnostics.com/docroot/dgx/js/specialtylabs.com','http://www.questdiagnostics.com/docroot/dgx/js/vitamindtestfacts.com','http://www.questdiagnostics.com/docroot/dgx/js/careers-ext.questcareersite.com','http://www.questdiagnostics.com/docroot/dgx/js/celera.com','http://www.questdiagnostics.com/docroot/dgx/js/gazelleapp.com','http://www.questdiagnostics.com/docroot/dgx/js/myoraldna.com','http://www.questdiagnostics.com/docroot/dgx/js/questdiagnostics-ccf.com','http://www.questdiagnostics.com/docroot/dgx/js/questdiagnostics-qis.com','http://www.questdiagnostics.com/docroot/dgx/js/questdiagnostics-randoms.com','http://www.questdiagnostics.com/docroot/dgx/js/questdiagnostics-supplies.com','hwoshg.healthwise.net/questdiag','http://www.questdiagnostics.com/docroot/dgx/js/4myheart.com','www.pages02.net/questdiagnosticsinc/es/','http://www.questdiagnostics.com/docroot/dgx/js/www.employer-solutions-resources.com','http://www.questdiagnostics.com/docroot/dgx/js/zmags.com','http://www.questdiagnostics.com/docroot/dgx/js/questconnect.com','http://www.questdiagnostics.com/docroot/dgx/js/brcavantage.com','http://www.questdiagnostics.com/docroot/dgx/js/celiacanswers.com','http://www.questdiagnostics.com/docroot/dgx/js/questdiagnosticssurveys.com','http://www.questdiagnostics.com/docroot/dgx/js/itsbesttotest.com'];
function lookInQuestFamily(link){
  for(var i=0;i<questFamily.length;i++){
    var ret = link.indexOf(questFamily[i]);
	if(ret>0)return false;
  }
  return true;
}
// --------------End External Link Code -----------//

// -------------- Start Google Analytics Code -----------//

  var _gaq = _gaq || [];
  var gaDomain;
  gaDomain =((document.domain.indexOf("http://www.questdiagnostics.com/docroot/dgx/js/qdx.com")) != -1)?"http://www.questdiagnostics.com/docroot/dgx/js/qdx.com":"http://www.questdiagnostics.com/docroot/dgx/js/questdiagnostics.com";
  _gaq.push(['_setAccount', 'UA-921392-12']);
  _gaq.push(['_setDomainName', gaDomain]);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www/') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

// -------------- End Google Analytics Code -----------//


// Updated xdomain.js script -- deals with matching mixed lower/upper case links for Quest Family sites - Lunametrics

(function($) {

function listenToClicks()
{
	var fileTypes=[".doc", ".xls", ".exe", ".zip", ".pdf", ".mov", ".mp3"];

	$('a[href]').each(function(index) {
 		var link = $(this);
		var href = link.attr('href');
		
		$.each(fileTypes, function(i) {
			if($(link).attr('href').indexOf(this)!=-1){
				valid = false;
				$(link).bind('click', function(c) {
					c.preventDefault();
	                _gat._getTrackerByName()._trackEvent('Download', 'Click - ' + $(link).attr('href'));
	                setTimeout('document.location = "' + $(link).attr('href') + '"', 100);
	            });
			}
		});

		var valid = false;
		$.each(questFamily, function(j) {
				var lowerLink = $(link).attr('href').toLowerCase();
				if((lowerLink.indexOf(this)!=-1)&&(window.location.href.indexOf(this)==-1)){	
					valid = true;

					if (valid)
					{
						$(link).bind('click', function(l) {
							l.preventDefault();
							_gat._getTrackerByName()._trackEvent('Quest Family Link', href);
							setTimeout('document.location = "' + href + '"', 100);
						});
					}
				}			
		});

		var rootDomain = document.domain.split(".")[document.domain.split(".").length - 2] + "." + document.domain.split(".")[document.domain.split(".").length - 1];

		if ( (href.match(/^http/)) && (href.indexOf(rootDomain) == -1) && !valid) {
			$(link).bind('click', function(d) {
					d.preventDefault();
			      	_gat._getTrackerByName()._trackEvent('Outbound Link', href);
			    });			   
		}
	});
	
}

$(document).ready(function() {
	listenToClicks();
});

} ) (jQuery);