var isMinIE4 = (document.all) ? 1 : 0;
var isDOM = (document.getElementById) ? 1 : 0;
var hintText = "Enter reference number";

/** Get elements from the document by ID **/
function get(elementName){
	if (isDOM == 1){		
		if(document.getElementById(elementName)){
			return document.getElementById(elementName);
		}else{
			return null;
		}		
	}else if (isMinIE4 == 1){
		if(document.all(elementName)[0]){
			return document.all(elementName)[0];
			}
		else{
			return document.all(elementName);
		}		
	}
}

/** Set the focus to the Track Quick Search input **/
function pageInit(){
	document.GuestForm.TrackingNumber.focus();
}

/** Remove illegal characters from the form and if the
 *  value isn't empty make call to submit the form
**/
function doSubmitSearch(aForm, anAction) {
	var aValue = aForm.TrackingNumber.value;
	aValue = aValue.replace(/^\s+/,'')
	aValue = aValue.replace(/\s+$/,'');
	var okay = (aValue != '');
	if (okay) {
		fwdSubmit(aForm, anAction);
	}
}	

/** Make sure that the Track Quick Search has a value search value
 *  1. User has entered a valid search value. 
 * 		value isn't the hint text
 * 		value isn't empty
 * 		value doesn't contains script characters that could be a security risk
 * If the value is invalid display an error by changeing the sytle of the guestSearchError
 * 
**/
function validateGuest(){
	var errorContainer = get('guestSearchError');
	var tNum = get('TrackingNumber');
	// Reset the styles for the backgournd and border of guestTrack input
	tNum.style.backgroundColor = "";
	tNum.style.borderColor = "";
	
	
	if(tNum.value == "" || tNum.value == hintText || (!stripScripts(tNum))){
		if(errorContainer.style.display!='block') errorContainer.style.display = 'block';
		tNum.style.backgroundColor = "#FFFFCC";
		tNum.style.borderColor = "#FF0000";
		return;
	}
	doSubmitSearch(get('GuestForm'),'SQGuest');
}

/** Submit the form **/
function fwdSubmit(form, action){
	var correctAction = getObjectInForm(form, "action");
	correctAction.setAttribute("value", action);
	form.submit();
}

/** Get a element from a form. In particular the action **/
function getObjectInForm(form, elementName){
	if (isDOM == 1){
		for(x=0; x< form.elements.length; x++){
			var element = form.elements[x];
			if(element.getAttribute("name") == elementName || element.getAttribute("id") == elementName) return element;
		}
		return null;
	}
}
