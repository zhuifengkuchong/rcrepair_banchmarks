(function($) {
	$.fn.hovercontent = function(options) {

		var defaults = {
			
		};

		var opts = jQuery.extend (defaults, options);

		this.each( function() {
			var element = $(this);
			
			//element.click(function(){
			//	$(this).find('.help-content').css('display', 'block');
			//});
			
			config = {
				over: function(){
			    	$(this).find('.help-content').css('display', 'block');
			    }, // function = onMouseOver callback (REQUIRED)    
			    timeout: 500, // number = milliseconds delay before onMouseOut    
			    out: function(){
			    	$(this).find('.help-content').css('display', 'none');
			    } // function = onMouseOut callback (REQUIRED) 
			}
			
			element.hoverIntent(config);

		});
		return this;
	};

})(jQuery);