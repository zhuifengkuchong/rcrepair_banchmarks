/** Determine if the value to be submitted to a form contains any
 	 characters that could be used to execute a script. This funtion is used
 	 for security. If any illegal characters are found remove them.
**/
function stripScripts(toStrip){
	var cleanString = "Clean String";
	var cleanFlag = true;	
	var stBadChar = "\"<>;{}():";
	var arrBadChar = stBadChar.split("");
	var string1;
	var string2;
	var startVal = toStrip.value;
	for (var x = 0; x < arrBadChar.length; x++){
		do {
			string1 = toStrip.value.substring(0,toStrip.value.indexOf(arrBadChar[x]));		
			string2 = toStrip.value.substring(toStrip.value.indexOf(arrBadChar[x])+1,toStrip.value.length);
			toStrip.value = string1 + string2;
		}while (!toStrip.value.indexOf(arrBadChar[x]) == -1)
		cleanString = toStrip.value;
	} // this loop is done twice
	for (var x = 0; x < arrBadChar.length; x++){
		do {
			string1 = toStrip.value.substring(0,toStrip.value.indexOf(arrBadChar[x]));			
			string2 = toStrip.value.substring(toStrip.value.indexOf(arrBadChar[x])+1,toStrip.value.length);
			toStrip.value = string1 + string2;
		}while (!toStrip.value.indexOf(arrBadChar[x]) == -1)
		cleanString = toStrip.value;
	}
	if(!startVal == cleanString){
		cleanFlag = false;
	}
	toStrip.value = cleanString;
	return cleanFlag;	
}