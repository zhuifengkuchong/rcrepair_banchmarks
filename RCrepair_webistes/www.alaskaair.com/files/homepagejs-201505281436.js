//version [201505270728] cached at [5/28/2015 3:55:39 PM]
//combined files: widget.js ascalendarwide.js cities.js shopping.js home.js sitenewsletter.js cookie.js 
//starts: widget.js
var KeyCodes = {
    DownArrow: 40,
    UpArrow: 38,
    Enter: 13,
    Tab: 9,
    End: 35,
    Home: 36,
    Shift: 16
};

//FormHelper acts like a static object with various methods for working with HTML forms on the client
var FormHelper = {
    CreateDynamicForm: function(postToUrl) {
        //alert("CreateDynamicForm: " + postToUrl);
        var dynamicForm = document.createElement("FORM");
        document.body.appendChild(dynamicForm);
        dynamicForm.method = "POST";
        dynamicForm.action = postToUrl;
        return dynamicForm;
    },
    AppendHiddenInputField: function(dynamicForm, fieldName, fieldValue) {
        //alert("AppendHiddenInputField: " + dynamicForm + ", " + fieldName + ", " + fieldValue);
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", fieldName);
        hiddenField.setAttribute("value", fieldValue);
        dynamicForm.appendChild(hiddenField);
    }
};


var Widget = {
    ErrorClass: "input-validation-error",
    QueueAriaMessage: function(ariaLog, msg, isImmediate) {
        this.Debounce(function() {
            var ariaLiveId = ariaLog[0].id;
//            alert(ariaLiveId);
            var jqueryAriaLiveId = '#' + ariaLiveId;
//            alert($(jqueryAriaLiveId)[0].outerHTML);
//            alert(msg);
            $(jqueryAriaLiveId).text(msg);
//            ariaLog.text(msg);

//            alert($(jqueryAriaLiveId).text());
        }, 500, isImmediate)();
    },
    Debounce:
        function(func, wait, immediate) {
            var timeout;
            return function() {
                var context = this, args = arguments;
                var later = function() {
                    timeout = null;
                    if (!immediate) func.apply(context, args);
                };
                var callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) func.apply(context, args);
            };
        },
    GetRandomId: function() {
        var d = new Date();
        var curr_hour = d.getHours();
        var curr_min = d.getMinutes();
        var curr_sec = d.getSeconds();

        return curr_hour + "_" + curr_min + "_" + curr_sec + Math.floor(Math.random() * 1111);
    },
    CancelBubbleAs: function(e) {
        if (!e) {
            e = window.event;
        }
        e.cancelBubble = true;
        if (e.stopPropagation) {
            e.stopPropagation();
        }
    },
    HideDD: function() {
        $(".AsDD").hide();
    },
    CurrentProtocol: function() {
        var protocol = "http:";
        return (top.location.protocol ? top.location.protocol : protocol) + "//";
    },
    PopulateDropdownFromList: function(dd, list, defaultValue) {
        for (i = 0; i < list.length; i++) {
            $(dd).append("<option>" + list[i] + "</option>");
        }
        if (defaultValue != null) {
            try {
                $(dd).val(defaultValue);
            } catch (e) {
            }
        }
    },
    IsIE6: function() {
        return $.browser.msie && $.browser.version == 6;
    },
    IsIE: function() {
        return $.browser.msie;
    },
    GetQuerystring: function(key, default_) {
        if (default_ == null) default_ = "";
        key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
        var qs = regex.exec(window.location.href);
        if (qs == null)
            return default_;
        else
            return qs[1];
    }

};

Widget.Settings = {
    ImagePath: "/icons/"
};

jQuery.fn.selfDescribeTxtBox3 = function(options) {
    options = options || {};
    options.selectTextOnClick = false;
    return $(this).selfDescribeTxtBox2(options);
};
jQuery.fn.selfDescribeTxtBox2 = function(options) {

    var description = options.description || "Enter Text Here";
    var title = options.title;
    var selectTextOnClick = options != null && options.selectTextOnClick != null ? options.selectTextOnClick : true;
    var clearOnFocus = options != null && options.clearOnFocus != null ? options.clearOnFocus : true;
    var currentTextBox = $(this);
    var placeholderColor = options.placeholderColor || '#696969';
    var fontColor = options.fontColor || '#000';

    if ($("#lblBehindTxtBx").length == 0) {
        $("body").append("<span style='color:silver; position:absolute; visibility:hidden; display:none;z-index:0;' disabled id='lblBehindTxtBx'></span>");
        $("#lblBehindTxtBx").bind("click", function(e) {
            currentTextBox.focus();
        });
    }

    currentTextBox.addClass("selfdescribe").attr({ description: description });
    if (currentTextBox.val() == "") {
        currentTextBox.val(title);
    }

    currentTextBox.bind("focus", function () {
        if (currentTextBox.val() == title && clearOnFocus) {
            currentTextBox.val("");
            $("#lblBehindTxtBx").show().html(description).css({ top: currentTextBox.offset().top + 4, left: currentTextBox.offset().left + 5 })
        }
    });

    currentTextBox.bind("keydown", function() {
        if (currentTextBox.val() == title) {
            currentTextBox.val('');
            $("#lblBehindTxtBx").show().html(description).css({ top: currentTextBox.offset().top + 4, left: currentTextBox.offset().left + 5 });
        }
    });

    currentTextBox.bind("click", function(e) {
        if ($(this).val() != "") {
            $(this).focus();
            if (selectTextOnClick == true) {
                $(this).select();

                //This fixes a missing/bugged feature in iOS webkit that doesn't select the text with .select().
                //As of iOS7, the .select() still does not select the text. This can be removed if iOS ever does support it.
                var isiOS = /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
                if (isiOS) {
                    this.setSelectionRange(0, 999);
                }
            }
        }
        resetStyles();
    });
    currentTextBox.bind("keyup", function() {
        if ($(this).val() != "") {
            $("#lblBehindTxtBx").hide();
        } else {
            $("#lblBehindTxtBx").show();
        }
        resetStyles();
    });
    currentTextBox.bind("blur", function() {
        if ($(this).val() == "") {
            $(this).val(title);
            $("#lblBehindTxtBx").hide();
        }
        resetStyles();
    });

    function resetStyles() {
        if (!currentTextBox.hasClass(Widget.ErrorClass)) {
            var color = fontColor;
            if (currentTextBox.val() === title) {
                color = placeholderColor;
            }
            currentTextBox.css({ color: color });
        }
    }

    resetStyles();
    return this;
};

jQuery.fn.selfDescribeTxtBox = function(options) {
    var description = options.description || "";
    var clearOnFocus = options.clearOnFocus || true;
    var fontColor = options.fontColor || "#696969";
    var currentTextBox = $(this);
    var showCloseBtn = options != null && options.showCloseBtn != null ? options.showCloseBtn : false;
    var onclose = options.onclose;
    var attachLabel = options.attachLabel != null ? options.attachLabel : true;
    var showLabel = options.showLabel != null ? options.showLabel : true;

    currentTextBox.addClass("selfdescribe").attr({ description: description });

    var idSuffix = Widget.GetRandomId();
    currentTextBox.css({ color: fontColor, "z-index": 1 });
    if (clearOnFocus) {
        if (showLabel) {
            currentTextBox.before("<div id=nobr" + idSuffix + " style='display:inline; position: relative;'></div>");
            $("#nobr" + idSuffix).append(currentTextBox);
        }
        if (attachLabel) {
            currentTextBox.after("<label id=spn" + idSuffix + " for=" + $(currentTextBox).attr("id") + " >" + description + "</label>");
        } else {
            currentTextBox.after("<span id=spn" + idSuffix + ">" + description + "</span>");
        }
        if (!showLabel) {
            $("#spn" + idSuffix).css({ color: "#696969", "z-index": 100, "-moz-opacity": "0.0", "-khtml-opacity": "0.0", "opacity": "0.0", position: "fixed", right: 0, bottom: 0, "font-size": "0px" });
        }
        if (showCloseBtn && !($.browser.msie && $.browser.version <= 6)) {
            currentTextBox.after("<img id=clr" + idSuffix + " src='" + Widget.Settings.ImagePath + "clear.gif' style='cursor:pointer; position:absolute; display:none; right:3px;' />");
            if ($.browser.msie) {
                $("#clr" + idSuffix).css({ "margin-top": 7 });
            } else {
                $("#clr" + idSuffix).css({ "top": 2 });
            }
            $("#clr" + idSuffix).bind("click", function(e) {
                currentTextBox.val("");
                currentTextBox.focus();
                resetText(e, true);
                if (onclose != null) {
                    try {
                        onclose();
                    } catch (e) {
                    }
                }
            });
        }
        $("#spn" + idSuffix).css({ color: "#696969", "z-index": 100 });
        var positionLabel = function() {
            if ($.browser.msie) {
                var marginTop = 5;
                $("#spn" + idSuffix).css({ left: 3, "margin-top": marginTop, position: "absolute" });
            } else {
                $("#spn" + idSuffix).css({ "margin-left": -1 * currentTextBox.width() + 4, top: 2 });
            }
        };
        positionLabel();
        $("#spn" + idSuffix).bind("click", function() {
            currentTextBox.focus();
        });
        $(window).bind("resize", function() {
            positionLabel();
        });
        $(document).bind("scroll", function() {
            positionLabel();
        });
    }
    $(this).bind(
    {
        focus: function(e) {
            resetText(e, true);
        },
        blur: function(e) {
            resetText(e, false);
        },
        keyup: function(e) {
            resetText(e, true);
        },
        click: function(e) {
            if ($(this).val() != "") {
                $(this).focus();
                $(this).select();
            }
        }
    });

    function resetText(e, hasFocus) {
        if (clearOnFocus && currentTextBox.val() === "") {
            $("#spn" + idSuffix).show();
            $("#clr" + idSuffix).hide();

            if (hasFocus) {
                $("#spn" + idSuffix).css({ color: "silver" });
            } else {
                $("#spn" + idSuffix).css({ color: "#696969" });
            }
            //currentTextBox.css({ "border-color": "#BBB" });
        } else {
            $("#spn" + idSuffix).hide();
            $("#clr" + idSuffix).show();
            currentTextBox.css({ color: "black" });
        }
        Widget.CancelBubbleAs(e);
    }

    return this;
};

jQuery.fn.trueWidth = function() {
    var width = $(this).width();
    if (!isNaN(parseInt($(this).css("border-left-width")))) {
        width += parseInt($(this).css("border-left-width")) * 2;
    }
    width += 4;
    return width;
};

jQuery.fn.addAsDropdown = function(param) {
    return makeDropdown($(this), param);
};

makeDropdown = function(src, param) {

    var currentTextBox = $(src);
    currentTextBox.attr('aria-invalid', false);
    var options = {
        Width: param.width,
        Height: param.height,
        DivId: param.divId,
        SearchText: param.searchText,
        FormatText: param.formatText,
        List: param.list,
        ListAlt: [],
        ShowArrow: param != null && param.showArrow != null ? param.showArrow : false,
        AcceptCharInput: param != null && param.acceptCharInput != null ? param.acceptCharInput : false,
        ZIndex: param != null && param.zIndex != null ? param.zIndex : 1,
        IdSuffix: Widget.GetRandomId(),
        UseBackgroundImage: true,
        DefaultHtml: param != null && param.defaultHtml != null ? param.defaultHtml : "",
        CityCodeID: param.citycodeid
    };

    var currentCityCodeID = options.CityCodeID;
    $("#" + currentCityCodeID).val('');

    var returnValue = false;

    options.DivId = options.DivId == null ? ('divAsDD' + options.IdSuffix) : options.DivId;
    param.divId = options.DivId;

    if (false && $("#" + options.DivId).length == 0) {
        currentTextBox.before("<div id=nobr" + options.IdSuffix + " style='display:inline; position:relative; z-index:" + options.ZIndex + "; text-align:left;margin:0px; padding:0px;'></div>");
        $("#nobr" + options.IdSuffix).append(currentTextBox);
    }

    if (options.ShowArrow) {

        var placeArrowInTextbox = function() {
            var arrowId = "spn" + options.IdSuffix;

            var imgHtml = "";
            if (($.browser.msie && $.browser.version <= 8) || $.browser.mozilla) {
                options.UseBackgroundImage = false;
                imgHtml = "<img style='display:inline; position: absolute' id='" + arrowId + "' src='" + Widget.Settings.ImagePath + "utility-arrow2.gif' />";
            } else {
                imgHtml = "<div style='z-index:99999999; display:inline; position: absolute;height:20px; width:20px; background:url(" + Widget.Settings.ImagePath + "utility-arrow2.gif)no-repeat;' id='" + arrowId + "'></div>";
            }
            currentTextBox.after(imgHtml);
            var marginTop = parseInt($(currentTextBox).css("padding-top"));
            if ($.browser.msie) {
                marginTop = 3;
            }
            $("#" + arrowId).css({ "z-index": "500", cursor: "pointer", "margin-top": marginTop });
            if (!options.UseBackgroundImage) {
                $("#" + arrowId).css({ "margin-left": $(currentTextBox).trueWidth() - 18, "z-index": "500", left: 0 });
            } else {
                $("#" + arrowId).css({ "right": -1 });
            }
            $("#" + arrowId).click(function() {
                Widget.HideDD();
                showDD();
//                currentTextBox.focus();
                return false;
            });
            $(currentTextBox).bind("click", function() {
                Widget.HideDD();
                showDD();
                return false;
            });
        };
        placeArrowInTextbox();
    }

    var divAsDD = $("#" + options.DivId);
    if (divAsDD.length == 0) {
        var html = "<div id='" + options.DivId + "' class='AsDD'></div>";
        $("body").append(html);
        divAsDD = $("#" + options.DivId);
        var divAsDDTop = 19;
        if ($.browser.msie) {
            divAsDDTop = 23;
            if ($.browser.version >= 8) {
                divAsDDTop = 8;
            }
        }
        $(divAsDD).css({ "z-index": 9999, "text-align": "left", "display": "none", "position": "absolute", "background-color": "white", "border": "solid 1px #BBB", left: 0, top: divAsDDTop });

        if (options.Height != null) {
            $(divAsDD).css({ "height": options.Height, "overflow": "auto" });
        }


        $(divAsDD).scroll(function() {
//            currentTextBox.focus();
        });
    } else {
        $(divAsDD).empty();
    }

    if (options.Width != null && !isNaN(options.Width)) {
        $(divAsDD).css({ "width": options.Width });
    }

    html = "";
    if (options.List != null && options.List.length > 0) {
        var ulListId = "ul" + options.IdSuffix,
            liI = 0,
            defaultSelect = -1,
            isSpecificInstanceFound = false; // if search by code has more variation, choose first one.

        divAsDD.append("<ul id='" + ulListId + "'></ul>");

        for (var i = 0; i < options.List.length; i++) {
            var liId = "li" + options.IdSuffix + "-" + liI,
                liIdS = "li" + options.IdSuffix + "-",
                findInstance = new RegExp(options.SearchText, 'gi'),
                findSpecificInstance = new RegExp(options.SearchText.toUpperCase() + "-", 'g'),
                findAndReplaceInstance = new RegExp("(" + options.SearchText + ")", 'gi'),
                formattedText = (options.FormatText) ? options.List[i].N.replace(findAndReplaceInstance, '<b>$1</b>') : options.List[i].N,
                l = options.List[i].S.length,
                itemStyle = "background-color: #fff;color: #696969;";

            options.ListAlt.push(options.List[i].N);

            // find index of default select or reset if first instance of code is found.
            if (((defaultSelect < 0) && findInstance.test(options.List[i].N)) || (!isSpecificInstanceFound && options.SearchText.length === 3 && findSpecificInstance.test(options.List[i].N))) {
                if (!isSpecificInstanceFound && options.SearchText.length === 3 && findSpecificInstance.test(options.List[i].N)) {
                    isSpecificInstanceFound = true;
                }
                defaultSelect = liI;
            }

            if (l > 0) {
                $("#" + ulListId).append("<li id='" + liId + "' style='border-top:1px solid #BBB; padding:0 3px;white-space:nowrap;" + itemStyle + "' title=\"" + options.List[i].N + "\">" + formattedText + "</li>");

                for (var j = 0; j < l; j++) {
                    liIdS = liIdS + (++liI);
                    itemStyle = "background-color: #fff;color: #696969;";

                    // find index of default select or reset if first instance of code is found.
                    if (((defaultSelect < 0) && findInstance.test(options.List[i].S[j].N)) || (!isSpecificInstanceFound && options.SearchText.length === 3 && findSpecificInstance.test(options.List[i].S[j].N))) {
                        if (!isSpecificInstanceFound && options.SearchText.length === 3 && findSpecificInstance.test(options.List[i].S[j].N)) {
                            isSpecificInstanceFound = true;
                        }

                        defaultSelect = liI;
                    }

                    formattedText = (options.FormatText) ? options.List[i].S[j].N.replace(findAndReplaceInstance, '<b>$1</b>') : options.List[i].S[j].N;
                    options.ListAlt.push(options.List[i].S[j].N);

                    if (j == l - 1) {
                        $("#" + ulListId).append("<li id='" + liIdS + "' style='border-bottom:1px solid #BBB;padding:0 3px 0 25px;white-space:nowrap;" + itemStyle + "' title='" + options.List[i].S[j].N + "' citycode='" + options.List[i].S[j].C + "'>" + formattedText + "</li>");
                    } else {
                        $("#" + ulListId).append("<li id='" + liIdS + "' style='padding:0 3px 0 25px;white-space:nowrap;" + itemStyle + "' title='" + options.List[i].S[j].N + "' citycode='" + options.List[i].S[j].C + "'>" + formattedText + "</li>");
                    }
                }
            } else {
                $("#" + ulListId).append("<li id='" + liId + "' style='padding:0 3px;white-space:nowrap;" + itemStyle + "' title='" + options.List[i].N + "' citycode='" + options.List[i].C + "'>" + formattedText + "</li>");
            }

            liI++;
        }

        var swap = options.List.slice();
        options.List = options.ListAlt.slice();
        options.ListAlt = swap;

        // highlight item appropriately
        if (options.List.length > 0) {
            $("#" + ulListId + " li").eq((defaultSelect > -1) ? defaultSelect : 0).addClass('selected_li').css({ "background-color": "#696969", "color": "#fff" });
            Widget.QueueAriaMessage(
                param.$ariaLog,
                $("#" + ulListId + " li")
                .eq((defaultSelect > -1) ? defaultSelect : 0).text() + " One of " + options.List.length,
                true); //TODO: Test tomorrow3
            //You should insert your announcement logic here for the existing drop selection.
            //Should happen only o nce
//            Widget.QueueAriaMessage(param.$ariaLog, , true);
            param.defaultSelect = defaultSelect;
        }
        currentTextBox.removeClass(Widget.ErrorClass);
        $(divAsDD).css({ background: "#ffffff" });
    }
    //No matching city was returned.
    else {
        setTimeout(function() {
            currentTextBox.attr('aria-invalid', true);
        }, 0);
        Widget.QueueAriaMessage(param.$ariaLog,$(options.DefaultHtml).text(), false);


        html = options.DefaultHtml;
        $(divAsDD).css({ background: "#dedede" });
        currentTextBox.addClass(Widget.ErrorClass);
        divAsDD.append(html).show();
        positionAcDropdown(currentTextBox, divAsDD);
        $(divAsDD).css({ "top": $(divAsDD).offset().top + 4 });

        var keyEvent = "keydown";
        $(currentTextBox).unbind(keyEvent);
        var lastKey = '';
        var selectedIndex = -1;
        $(currentTextBox).bind(keyEvent, function(e) {
            var keyID = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
            if (keyID == KeyCodes.Enter || keyID == KeyCodes.Tab) {
                Widget.HideDD();
            }
        });
        return this;
    }

    $("ul", $(divAsDD)).css({ "padding": "0px", "color": "#696969", "margin": "0px", "cursor": "pointer", "padding-left": "0px", "margin-left": "0px", "list-style-type": "none", "font-size": "11px", "font-family": "Arial" });
    $("li", $(divAsDD)).mouseover(function() {
        $("li", $(divAsDD)).css({ "background-color": "#ffffff", "color": "#696969" });
        $(this).css({ "background-color": "#696969", "color": "white" });
    });

    $("li", $(divAsDD)).click(function() {
        $(divAsDD).hide();
        var currentValue = $(this).attr("title");
        var currentCityCode = $(this).attr("citycode");
        $(currentTextBox).val(currentValue);
        $("#" + currentCityCodeID).val(currentCityCode);
//        $(currentTextBox).focus();
        currentTextBox.removeClass(Widget.ErrorClass);
    });

    $(divAsDD).keypress(function() {
        $(".selected_li", this).click();
    });


    var keyEvent = "keydown";
    $(currentTextBox).unbind(keyEvent);
    var lastKey = '';
    var selectedIndex = -1;
    $(currentTextBox).bind(keyEvent, function(e) {
        checkKeyPressed(e);
    });

    if ($(currentTextBox).attr("blurbound") != "true") {
        $(currentTextBox).bind("blur", function(e) {
            if ($(divAsDD).css("display") != "none" && $("li", $(divAsDD)).length == 1) {
                $(currentTextBox).val($("li", $(divAsDD)).eq(0).attr("title"));
                $("#" + currentCityCodeID).val($("li", $(divAsDD)).eq(0).attr("citycode"));
            }
        });
        $(currentTextBox).attr("blurbound", "true");
    }

    if (Widget.IsIE6()) {
        var keyEvent = "keypress";
        $(currentTextBox).unbind(keyEvent);
        var lastKey = '';
        var selectedIndex = -1;
        $(currentTextBox).bind(keyEvent, function(e) {
            var keyID = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
            if (keyID == KeyCodes.Enter) {
                checkKeyPressed(e);
            }
        });
    }

    function checkKeyPressed(e) {
        var keyID = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;

        //if there are no items on the list, enter should trigger form submit
        if (keyID == KeyCodes.Enter) {
            if ($("li", $(divAsDD)).length == 0) {
                return;
            }
        }

        if (keyID >= 96 && keyID <= 105) {
            keyID = keyID - 48;
        }
        var letter = String.fromCharCode(keyID);
        var letterFound = false;
        if (!options.AcceptCharInput) {
            for (i = selectedIndex + 1; i < options.List.length; i++) {
                if (options.List[i].substring(0, 1).toLowerCase() === letter.toLowerCase()) {
                    selectedIndex = i;
                    letterFound = true;
                    break;
                }
            }

            if (!letterFound) {
                for (i = 0; i <= selectedIndex; i++) {
                    if (options.List[i].substring(0, 1).toLowerCase() === letter.toLowerCase()) {
                        selectedIndex = i;
                        letterFound = true;

                        break;
                    }
                }
            }
        }
        var selectedFound = false;
        if (!letterFound) {
            $("li", divAsDD).each(function(i) {
                if ($(this).hasClass("selected_li") || currentTextBox.val() == $(this).attr("title")) {
                    selectedIndex = i;
                    selectedFound = true;
                }
            });
        }

        var listIsVisible = $(divAsDD).length > 0 && $(divAsDD).css("display").toLowerCase() != "none";
        if (listIsVisible) {
            var highlightedValue = "";
            $("li", $(divAsDD)).each(function(i) {
                if ($(this).hasClass("selected_li")) {
                    highlightedValue = $(this).attr("title");
                }
            });
        }

        var noOfElements = options.List.length;
        if (letterFound || keyID == KeyCodes.DownArrow || keyID == KeyCodes.UpArrow) {
            if (!letterFound && listIsVisible) {
                switch (keyID) {
                case KeyCodes.DownArrow:
                    if (selectedIndex + 1 < options.List.length) {
                        selectedIndex++;
                    }
                    Widget.QueueAriaMessage(param.$ariaLog, (selectedIndex + 1) + " of " + options.List.length, true);
                    break;
                case KeyCodes.UpArrow:
                    if (selectedIndex >= 0) {
                        selectedIndex--;
                        if (selectedIndex >= 0) {
                            Widget.QueueAriaMessage(param.$ariaLog, (selectedIndex + 1) + " of " + options.List.length, true);
                        }
                    }
                    break;
                default:
                    console.log(letterFound + " " + keyID);
                    break;
                }
            }
            if (selectedIndex > noOfElements) {
                selectedIndex = 0;
            } else if (selectedIndex < -1) {
                selectedIndex = noOfElements - 1;
            } else if (selectedIndex == -1 || selectedIndex == noOfElements) {
                selectedIndex = -1;
            }

            Widget.HideDD();
            showDD();
            $("li", $(divAsDD)).css({ "background-color": "white" });
            $("li", $(divAsDD)).css({ "color": "#696969" });
            $("li", $(divAsDD)).removeClass("selected_li");

            if (selectedIndex > -1) {
                $("li", $(divAsDD)).eq(selectedIndex).css({ "background-color": "#696969" });
                $("li", $(divAsDD)).eq(selectedIndex).css({ "color": "white" });
                $("li", $(divAsDD)).eq(selectedIndex).addClass("selected_li");
                currentTextBox.val(options.List[selectedIndex]);
            } else if (currentTextBox.param.searchText != "") {
                currentTextBox.val(currentTextBox.param.searchText);
            }

            $("#" + currentCityCodeID).val($("li", $(divAsDD)).eq(selectedIndex).attr("citycode"));
            setTimeout(function() {
                $(currentTextBox)[0].setSelectionRange(0, 999999);
            }, 0);

            if (options.ShowArrow) {
                if (listIsVisible) {
                    var highlightedValue = "";
                    $("li", $(divAsDD)).each(function(i) {
                        if ($(this).hasClass("selected_li")) {
                            highlightedValue = $(this).attr("title");
                        }
                    });
                    currentTextBox.val(highlightedValue);
                    currentTextBox.removeClass(Widget.ErrorClass);
                }
                $(divAsDD).scrollTop(0);
                if ((selectedIndex + 1) * $("li:eq(0)", $(divAsDD)).height() > options.Height) {
                    $(divAsDD).scrollTop((selectedIndex + 1) * $("li:eq(0)", $(divAsDD)).height() - options.Height);
                }
            }
            returnValue = false;
        } else if (keyID >= 48 && keyID <= 48 + options.List.length) {
            selectedIndex = keyID - 49;
            $(this).val(options.List[selectedIndex]);
            $(divAsDD).hide();
        } else {
            if (keyID == KeyCodes.Enter || keyID == KeyCodes.Tab) {
                if (listIsVisible) {
                    var highlightedValue = "";
                    var highlightedCityCode = "";
                    if (currentTextBox.param.defaultSelect >= 0 && currentTextBox.param.defaultSelect <= noOfElements) {
                        highlightedValue = $("li", $(divAsDD)).eq(currentTextBox.param.defaultSelect).attr("title");
                        highlightedCityCode = $("li", $(divAsDD)).eq(currentTextBox.param.defaultSelect).attr("cityCode");
                    } else if ($("li", $(divAsDD)).length >= 1) {
                        highlightedValue = $("li", $(divAsDD)).eq(0).attr("title");
                        highlightedCityCode = $("li", $(divAsDD)).eq(0).attr("citycode");
                    }
                    $("li", $(divAsDD)).each(function(i) {
                        if ($(this).hasClass("selected_li")) {
                            highlightedValue = $(this).attr("title");
                            highlightedCityCode = $(this).attr("cityCode");
                            currentTextBox.removeClass(Widget.ErrorClass);
                        }
                    });
                    if (highlightedValue != "") {
                        currentTextBox.val(highlightedValue);
                        currentTextBox.removeClass(Widget.ErrorClass);
                        if ($(currentTextBox).attr("id") == "checkInDestination") {
                            $("#" + currentCityCodeID).val(highlightedCityCode);
                        }
                    }
                    $(divAsDD).hide();
                }
                returnValue = true;
            } else {
                returnValue = options.AcceptCharInput;
            }
        }
        return returnValue;
    }

//end checkKeyPressed
    $(currentTextBox).attr("autocomplete", "off");
    currentTextBox.param = param;


    function showDD() {
        //ie6 div-under-select iframe fix
        if ($.browser.msie && $.browser.version == 6) {
            $('.AsDD').bgiframe();
        }
        $(divAsDD).show();
        //Calendar.CloseCalPopup();
    }

    function positionAcDropdown(txtBox, divAutocompleContainer) {
        var top = txtBox.offset().top + txtBox.height();
        var borderTop = isNaN(parseInt(txtBox.css("border-top"))) ? 0 : parseInt(txtBox.css("border-top"));
        var borderBottom = isNaN(parseInt(txtBox.css("border-bottom"))) ? 0 : parseInt(txtBox.css("border-bottom"));
        var marginTop = isNaN(parseInt(txtBox.css("margin-top"))) ? 0 : parseInt(txtBox.css("margin-top"));
        var paddingTop = isNaN(parseInt(txtBox.css("padding-top"))) ? 0 : parseInt(txtBox.css("padding-top"));
        top += borderTop + borderBottom + marginTop + paddingTop + 3;
        var left = txtBox.offset().left;
        $(divAutocompleContainer).css({ "top": top });
        $(divAutocompleContainer).css({ "left": left });
    }

    positionAcDropdown(currentTextBox, divAsDD);
    if ($(document.activeElement).attr("id") == currentTextBox.attr("id")) {
        showDD();
    } else {
        $(divAsDD).hide();
    }

    return this;
    //END OF MakeDropDown
};

$(document).ready(function() {
    $(window).bind("click", function() {
        Widget.HideDD();
    });
    $(document).bind("click", function() {
        Widget.HideDD();
    });
});

//ends: widget.js
//starts: ascalendarwide.js
//this javascript file has a dependency on asglobal.js
var Calendar = {
    DivCalContainerId: "divAsCalCntl12345",
    currentMonth1: 0,
    currentYear1: 0,
    currentMonth2: 0,
    currentYear2: 0,
    calTextboxCurrent: null,
    calDaysUpperLimit: 332,
    calDaysLowerLimit: 0,
    calTxtBoxD: null,
    calOnselectdate: null,
    BindMonth: function (txtBox, month, year, daysUpperLimit, daysLowerLimit, txtBoxD, onselectdate) {

        if(txtBox != null)
        {
            Calendar.calTextboxCurrent = txtBox;
        }
        else
        {
            txtBox = Calendar.calTextboxCurrent;
        }

        if(txtBoxD != null)
        {
            calTxtBoxD = txtBoxD;
        }
        else if(typeof(calTxtBoxD) != 'undefined' && calTxtBoxD != null)
        {
            txtBoxD = calTxtBoxD;
        }

		// Stay on the last displayed calendar if the date is still able to be displayed
		if (month == Calendar.currentMonth2 && year == Calendar.currentYear2)
		{
			month = Calendar.currentMonth1;
			year = Calendar.currentYear1;
		}

        calOnselectdate = onselectdate;

        $("#tblAsCalDays .day").html("&nbsp;");
        $("#tblAsCalDays .day").unbind("click");
        $("#tblAsCalDays .day").unbind("mouseover");
        $("#tblAsCalDays .day").unbind("mouseout");
        $("#tblAsCalDays .day").css({ cursor: "default" });
        $("#tblAsCalDays .day").css({ "text-decoration": "none" });
        $("#tblAsCalDays .day").css({ "background-color": "white" });
        $("#tblAsCalDays .day").css({ "color": "rgb(154,98, 32)" }); 
        $("#tblAsCalDays .day").css({ "border": "solid 1px white" });
        var day = 1;
        var row = 1;
        var col = 0;
        year = year > 100 ? year : (2000 + year);
        var date = new Date(year, month, day, 0, 0, 0, 0);

        var todayPlus330 = Calendar.GetUpperLimitDate(daysUpperLimit);
        var lowerLimitDate = new Date(Calendar.GetLowerLimitDate(daysLowerLimit));

        var thisDate = null;
        $("#tdAsCalNextMonth").addClass("nextMonth").removeClass("nextMonthDisabled");

        //Reset month selector to textbox value
        $("#monthNavigatorDropdown").val(date.getFullYear() + '/' + (date.getMonth()+1) + '/1').attr("selected", "selected");


        var calNumber = 1;
        while (date.getMonth() == month) {

            col = date.getDay() + 1;

			var dayElement = $("#td" + row + "_" + col + "_" + calNumber);
			dayElement.html(day);
            dayElement.attr("year", year);
            dayElement.attr("month", month);

            thisDate = new Date(year, month, day, 24, 0, 0, 0);
            if (thisDate >= lowerLimitDate && thisDate < todayPlus330) {
				dayElement.unbind();
                dayElement.bind("click", function () {
                    txtBox.val(Calendar.DateToText(new Date($(this).attr("year"), $(this).attr("month"), $(this).html(), 0, 0, 0, 0)));
					txtBox.change();
					txtBox.focus();
					txtBox.css({ color: "black" });
                    Calendar.CloseCalPopup();
                    if (onselectdate != null) {
                        try {
                            onselectdate();
                        }
                        catch (e) {
                        }
                    }
                });
                dayElement.bind("mouseover", function () {
                    $(this).css({ "background-color": "#9fb069" });
                    $(this).css({ "border": "solid 1px rgb(154,98, 32)" });
                });
                dayElement.bind("mouseout", function () {
                    $(this).css({ "background-color": "white" });
                    $(this).css({ "color": "rgb(154,98, 32)" });
                    $(this).css({ "border": "solid 1px white" });
                });
                dayElement.css({ cursor: "pointer" });
                dayElement.css({ color: "rgb(154,98, 32)" });
                dayElement.css({ "font-weight": "bold" });
                dayElement.css({ "text-decoration": "underline" });
            }
            else {
                dayElement.css({ cursor: "auto" });
                dayElement.css({ color: "gray" });
                dayElement.css({ "font-weight": "normal" });
            }

            var currentDate = new Date();
            var thisYear = year;
            if (txtBox.val() != "") {
                currentDate = new Date(Date.parse(txtBox.val()));
                if (currentDate.getFullYear() >= year) {
                    thisYear = currentDate.getFullYear();
                }
            }

            if (currentDate != null && currentDate.getMonth() != null && !isNaN(currentDate.getMonth())) {
                if (currentDate.getMonth() == month && thisYear == year && currentDate.getDate() == day) {
                    dayElement.css({ "background-color": "#9fb069" });
                    dayElement.css({ "border": "solid 1px rgb(154,98, 32)" });
                }
                else {
                    dayElement.css({ "background-color": "white" });
                }
            }


            if (col == 7) {
                row++;
            }
            day++;
            date = new Date(date.setDate(date.getDate() + 1));
            
            if((date.getMonth() > month || date.getFullYear() > year) && calNumber == 1)
            {
                Calendar.currentMonth1 = month;
                Calendar.currentYear1 = year;
                month = date.getMonth();
                year = date.getFullYear();
                calNumber = 2;
                day = 1;
                row = 1;
                col = 0;

                //Clear calendar day HTLM, IE7 bug
                for (i = 1; i <= 6; i++) 
                {
                        for (j = 1; j <= 7; j++) 
                        {
                            $("#td" + i + "_" + j + "_" + calNumber).html("");
                            $("#td" + i + "_" + j + "_" + calNumber).unbind();
                            $("#td" + i + "_" + j + "_" + calNumber).css({ cursor: "auto" });
                            $("#td" + i + "_" + j + "_" + calNumber).css({ color: "gray" });
                            $("#td" + i + "_" + j + "_" + calNumber).css({ "font-weight": "normal" });
                            $("#td" + i + "_" + j + "_" + calNumber).css({ "text-decoration": "none" });
                            $("#td" + i + "_" + j + "_" + calNumber).css({ "background-color": "white" });
                            $("#td" + i + "_" + j + "_" + calNumber).css({ "border": "solid 1px white" });
                        }
                }
            }
        }

        Calendar.currentMonth2 = month;
        Calendar.currentYear2 = year;

        $("#tdCurrentMonth1").html(Calendar.GetMonthName(Calendar.currentMonth1) + " " + Calendar.currentYear1);
        $("#tdCurrentMonth2").html(Calendar.GetMonthName(Calendar.currentMonth2) + " " + Calendar.currentYear2);

        $("#tblAsCalMonths td").removeClass("current");
        $("#tblAsCalMonths td").eq(Calendar.currentMonth1).addClass("current");


        Calendar.calTextboxCurrent = txtBox;

        if (new Date(Calendar.currentYear1, Calendar.currentMonth1, 1, 24, 0, 0, 0) < lowerLimitDate) {
            $("#tdAsCalPrevMonth").removeClass("prevMonth");
            $("#tdAsCalPrevMonth").addClass("prevMonthDisabled");
        }
        else {
            $("#tdAsCalPrevMonth").removeClass("prevMonthDisabled");
            $("#tdAsCalPrevMonth").addClass("prevMonth");
        }
        if (thisDate.setDate(thisDate.getDate() - 1) >= todayPlus330) {
            if ($("#tdAsCalNextMonth").hasClass("nextMonth")) {
                $("#tdAsCalNextMonth").removeClass("nextMonth");
            }
            if (!$("#tdAsCalNextMonth").hasClass("nextMonthDisabled")) {
                $("#tdAsCalNextMonth").addClass("nextMonthDisabled");
            }
        }
    }
	,
    GotoNextMonth: function () {
        if ($("#tdAsCalNextMonth").hasClass("nextMonthDisabled")) {
            return;
        }
        if (Calendar.currentMonth1 < 11) {
            Calendar.currentMonth1++;
        }
        else {
            Calendar.currentMonth1 = 0;
            Calendar.currentYear1++;
        }
        Calendar.BindMonth(Calendar.calTextboxCurrent, Calendar.currentMonth1, Calendar.currentYear1, calDaysUpperLimit, calDaysLowerLimit, Calendar.calTxtBoxD, calOnselectdate);
    }
	,
    GotoPreviousMonth: function () {
        if ($("#tdAsCalPrevMonth").hasClass("prevMonthDisabled")) {
            return;
        }
        if (Calendar.currentMonth1 == 0) {
            Calendar.currentMonth1 = 11;
            Calendar.currentYear1--;
        }
        else {
            Calendar.currentMonth1--;
        }
        Calendar.BindMonth(Calendar.calTextboxCurrent, Calendar.currentMonth1, Calendar.currentYear1, calDaysUpperLimit, calDaysLowerLimit, Calendar.calTxtBoxD, calOnselectdate);
    }
	,
    GetUpperLimitDate: function(daysUpperLimit){
        if (daysUpperLimit == null || isNaN(daysUpperLimit)) {
            daysUpperLimit = 332;
        }
        calDaysUpperLimit = daysUpperLimit;

        return new Date().setDate(new Date().getDate() + daysUpperLimit);
    }
        ,
    GetLowerLimitDate: function(daysLowerLimit){
        if (daysLowerLimit == null || isNaN(daysLowerLimit)) {
            daysLowerLimit = 0;
        }
        calDaysLowerLimit = daysLowerLimit;
        return new Date().setDate(new Date().getDate() - daysLowerLimit);
    }
    ,
    GetMonthName: function (month) {
        switch (month) {
            case 0:
                return "January";
                break;
            case 1:
                return "February";
                break;
            case 2:
                return "March";
                break;
            case 3:
                return "April";
                break;
            case 4:
                return "May";
                break;
            case 5:
                return "June";
                break;
            case 6:
                return "July";
                break;
            case 7:
                return "August";
                break;
            case 8:
                return "September";
                break;
            case 9:
                return "October";
                break;
            case 10:
                return "November";
                break;
            case 11:
                return "December";
                break;
        }
    }
	,
    CloseCalPopup: function () {
        $("#" + Calendar.DivCalContainerId).hide();
    }
    ,
    GetCalRandomId: function () {
        var d = new Date();
        var curr_hour = d.getHours();
        var curr_min = d.getMinutes();
        var curr_sec = d.getSeconds();

        return curr_hour + "_" + curr_min + "_" + curr_sec + Math.floor(Math.random() * 1111);
    }
	,
    DateToText: function (date) {
        return (date.getMonth() <= 8 ? "0" : "") + (date.getMonth() + 1) + "/" + (date.getDate() <= 9 ? "0" : "") + date.getDate() + "/" + date.getFullYear();
    }
};

jQuery.fn.ascalendar = function (param) {
	var CALENDAR_TRIGGER_CLASS = "ascalendar-trigger";
	var TAB_KEY = 9;
    param = param || {};
    var useCalIcon = param.useCalIcon || true;
    var showOnFocus = param.showOnFocus || false;
    var iconInside = param.iconInside || true;
    var isPopup = param.isPopup || false;
    var DEFAULT_NUM = -9999;
    var top = param.top || DEFAULT_NUM;
    var left = param.left || DEFAULT_NUM;
    var rightPos = param.rightPos || 0;
    var endDateCtl = param.endDateCtl;
    var startDateCtl = param.startDateCtl;
    var daysUpperLimit = param.daysUpperLimit || 332;   //when this number is added to today, it returns the first invalid day, so 332 will allow today + 331 days
    var daysLowerLimit = param.daysLowerLimit || 0;
    var openEnded = param.openEnded || false;
    var onselectdate = param.onselectdate;
    var onblur = param.onblur;
    var calculatePosition = param.calculatePosition != null ? param.calculatePosition : true;
    var isRequired = param.isRequired != null ? param.isRequired : true;
    var alignRight = param.alignRight || false;
    if (openEnded) {
        daysUpperLimit = 100000;
        daysLowerLimit = 100000;
    }
    $(this).each(function (i) {
        var idSuffix = Calendar.GetCalRandomId();
        var imgId = "img" + idSuffix;

        var txtBox = $(this);
        //format textbox, set background
        var calIconUrl = "Unknown_83_filename"/*tpa=http://www.alaskaair.com/files/www.alaskaair.com/icons/cal-icon.gif*/;
        if (typeof as === "undefined" || typeof as.Protocol === "undefined")
            calIconUrl = "https://" + calIconUrl;
        else
            calIconUrl = as.Protocol + calIconUrl;

        $(txtBox).css({ background: "white url(" + calIconUrl + ") no-repeat right center" }).attr({ maxlength: 11 });
		txtBox.addClass(CALENDAR_TRIGGER_CLASS);

        //put textbox in a relatively position container
        txtBox.before("<div id=nobr" + idSuffix + " style='display:inline; position: relative; margin:0px; padding:0px;clear:both'></div>");
        $("#nobr" + idSuffix).append(txtBox);
        //ascalendaricongenerated class is used on retro credit to remove it. this gets rebuilt and layered underneath every time .ascalendar is called and prevents new calendar icon from firing correct binded event.
        txtBox.after("<div class='ascalendaricongenerated " + CALENDAR_TRIGGER_CLASS + "' style='display:inline; position: absolute; margin:0px; padding:0px;clear:both; width:20px;height:20px; cursor: pointer;' id='" + imgId + "'>&nbsp;</div>")

        if (calculatePosition) {
            left = txtBox.offset().left - txtBox.offsetParent().offset().left + txtBox.width() - 15 + parseInt(txtBox.css("padding-left")) * 2;
            top = 0;
            if ($.browser.msie && $.browser.version <= 6) {
                left -= 8;
            }

        }
        else {
            //check for top, left overrides
            top = parseInt(top);
            left = parseInt(left);
            if (isNaN(top) || top == DEFAULT_NUM || isNaN(left) || left == DEFAULT_NUM) {
                left = -15;
                top = 15;
            }

        }

		var imageZIndex = 1;
		if (txtBox.css("z-index") != 'auto' && txtBox.css("z-index") != '0') {
			imageZIndex = parseInt(txtBox.css("z-index")) + 1;
		}
        $("#" + imgId).css({ position: "absolute", left: left, top: top, "z-index": imageZIndex });
        //work-around to ie8 z-index bug
        if ($.browser.msie) {
            $("#" + imgId).css({ background: "url(//data:image/gif)"});
        }

        formatDate(txtBox);

		txtBox.keyup(function (e) {
			if (e.which == TAB_KEY) {
				showCalendar(e, onselectdate);
			} else if (calendarIsVisible()) {
				var dfvalidator = new ASCOMDateFieldValidator(txtBox.val());
				if (dfvalidator.validate()) {
					showCalendar(e, onselectdate);
				}
			}
		});

		// An IE bug prevents us from using focus instead of click/keyup. See http://stackoverflow.com/questions/14410075/jquery-receiving-two-focusin-events-on-focus
		txtBox.click(function (e) {
			showCalendar(e, onselectdate);
		});

		txtBox.keydown(function (e) {
			if (e.which == TAB_KEY) {
				Calendar.CloseCalPopup();
			}
		});

		txtBox.change(function (e) {
			var errorClass = "input-validation-error";
			var dfvalidator = new ASCOMDateFieldValidator(txtBox.val());
			if (dfvalidator.validate()) {
				txtBox.val(dfvalidator.sd_target);
				txtBox.removeClass(errorClass);
				setDependentDate(txtBox, $(endDateCtl));
			}
			else
			{
				var errmsg = dfvalidator.validationerrormsg;
				// txtBox.val("mm/dd/yyyy"); <-- leave invalid text displayed
				setFontSize(txtBox);                
				txtBox.addClass(errorClass);
			}
		});

		function calendarIsVisible() {
			return $("#" + Calendar.DivCalContainerId).length > 0 && $("#" + Calendar.DivCalContainerId).is(":visible");
		}

		function setDependentDate(sourceInput, dependentInput) {
			if (sourceInput.length > 0 && dependentInput.length > 0 && dependentInput.val() != "") {
				//change dependent date only if dependent date is earlier
				var isEarlier = false;

				try {
					isEarlier = Date.parse(sourceInput.val()) > Date.parse(dependentInput.val());
				}
				catch (e) {
				}

				if (isEarlier) {
					dependentInput.val(sourceInput.val());
					dependentInput.change();
				}
			}
		}

        function setFontSize(txtBox) {
            if (txtBox.val() == "mm/dd/yyyy") {
                txtBox.css({ "font-size": "11px" });
            }
            else {
                txtBox.css({ "font-size": "13px" }); //restore font size
            }
        }

        // M40297,M41054 / start
		// -------------------------------------------
		//  start of the updated date field validator
		// -------------------------------------------
		/*
        here are the use cases supported

        UCase    Source           Transformation
        1        10/14            10/14/2012 - use current year (ucase 23 applies)
        2        1/45             Error  there aren't 45 day in jan (for feb check for leap years)
        3        14oct            10/14/2012 - use current year(ucase 23 applies)
        4        14oct12          10/14/2012
        5        10/14/12         10/14/2012
        6        10/1412          Error
        7        10/14/201        Error
        8        10/14/20123      Error - truncate to 4 digit year and validate
        9        12               10/12/2012 - assume mth day is provided, use current year (ucase 23 applies)
        10       123              Error
        11       Blank            Error
        12       14/10            Error
        13       14/10/12         Error
        14       10-14            10/14/2012 - use current year (ucase 23 applies)
        15       10-14-12         10/14/2012
        16       14-oct           10/14/2012 - use current year (ucase 23 applies)
        17       14-oct-12        10/14/2012
        18       10-1412          Error
        19       4/6/12           04/06/2012
        20       4/2/2020         Error - over 330 days in the future
        21       other            Error
        22       pastdate         Dates in the past are an error
        23       special case     Where the year is not provided, if the mth or mth/day plus the current year would
                                  result in a past date, then apply next year. (ucase 20 applies) 
        24       period seperator In ucase 1 .. 23, where the seperator is a '.', convert it to '/' and validate
        inter-date behavior
        25       depart<=return   If departure date is greater than the return date, set the return date to the departure date
        26       1-january-2012   Handle fully month text, optionally surrounded by the following delimiters space,dash,slash 
                 1-janoarry-2012  Handle short month text, optionally followed by misspellings with alpha characters 
		*/

		function ASCOMDateFieldValidator(sdate){
			this.sd_target = sdate.toLowerCase();
//console.log("sdate: " + sdate);				
            this.now = new Date();
            this.months = ["jan", "feb", "mar", "apr", "may", "jun", "jul","aug", "sep", "oct", "nov", "dec"];
            //this.fullmonths = ["january","february","march","april","may","june","july","august","september","october","november","december"];
			this.bkslash = "/";
			this.blank = false;
			this.allnumbers = false;
			this.hasbkslashes = true;
			this.hasmonthtext = false;
			this.specialcaseadvanceyearapplies = false;		// ucase 23
			this.monthtext = "";
			this.monthnumber = -1;
			this.hasvalidationerror = false;
			this.validationerrormsg = "";
			this.seterror = function(errors_index){
				if(this.validationerrormsg.length === 0)
				{
					this.validationerrormsg = this.errors[errors_index];
				}
				this.hasvalidationerror = true;
			}
			this.errors = ["Invalid characters","Missing value","Number exceed days in the current month","Invalid date format","Not a date value","Date is in the past","Exceeds maximum date","Date is in the past but incrementing the year would exceed the maximum date"];
			this.lastdayofmonth = function(mm,yy) {
                return new Date(yy, mm, 0).getDate()
            };
            this.isnullorblank = function(s){
                return (s === null || s === "");
            };
			this.exceedsmaxdate = function(sdate){
				var d = new Date(sdate);
				if(isNaN(d))
				{
					return false;
				}
				return (d > this.current.maxd);
			};
			this.advanceyear = function(){
//console.log("here in this.advanceyear");				
				// do last in validate() so we assume that this.sd_target is in the final form, mm/dd/yyyy, 
				// and therefore no need to do error checking. check that the new date in the future doesnt exceed the max date
				var temp    = this.sd_target;
				var newtemp = "";
				if(this.isinthepast(temp))
				{
//console.log("adding 1 to the year");				
					var tokens = temp.split("/");
//console.log("pre - mm|dd|yy: " + tokens[0] + "|" + tokens[1] + "|" + tokens[2]);				
					newtemp = tokens[0] + "/" + tokens[1] + "/" + ((tokens[2] * 1) + 1);
					if(this.exceedsmaxdate(newtemp))
					{
						// do nothing, leave this.sd_target unchanged
						this.seterror(7);
					}
					else
					{
						this.sd_target = newtemp;
					}
				}
			};
			this.removeemptyarrayelements = function(ray){
				var raynew = [];
				for(var i = 0; i < ray.length; i += 1)
				{
					if(ray[i] !== "")
					{
						raynew.push(ray[i]);
					}
				}
				return raynew;
			};
            this.current = {
				mm: this.now.getMonth() + 1,
				dd: this.now.getDate(),
				yy: this.now.getFullYear(),
				mmddyy: new Date( (this.now.getMonth() + 1) + "/" + this.now.getDate() + "/" + this.now.getFullYear() ),
				raw: this.now,
				maxd: new Date( (this.now.getMonth() + 1) + "/" + (this.now.getDate() + daysUpperLimit - 1) + "/" + this.now.getFullYear() ),
				lastdd: new Date(this.now.getFullYear(), this.now.getMonth(), 0).getDate()
				/*
				lastdd: The way the above code works is that the month passed to the Date constructor is actually 0 based
				(i.e. 0 is January, 1 is February etc) so it is in effect creating a date for the day 0 of the next month. 
				Because day 0 equates to the last day of the previous month the number returned is effectively the number 
				of days for the month we want.
				*/
            };            
            this.isinthepast = function(sdate){
                var d = new Date(sdate);
                if(d == undefined || isNaN(d))
				{
                   return undefined;
				}
                var mm = d.getMonth()+1;
                var dd = d.getDate();
                var yy = d.getFullYear();
                
                var dt = new Date();                
                dt.setDate(dt.getDate() - daysLowerLimit); 
                
                // compare only the date part not the time                   
                return ( new Date(mm + "/" + dd + "/" + yy) < new Date(dt.getMonth()+1 + "/" +  dt.getDate() + "/" + dt.getFullYear()));

            };
			this.precheck = function(){
				// is there a month spelled out like "12-oct-2012" (ucase 3,4,16,17)
				this.sd_target = this.sd_target.toLowerCase();
				var temp       = this.sd_target;
				var matchlist  = temp.match(/[a-zA-Z]{3}/gi);
				if(matchlist !== null && matchlist.length > 0)
				{
					var text          = matchlist[0].toLowerCase();
					var smonths       = this.months.join('').toLowerCase();			// "janfebmaraprmayjunjulaugsepoctnovdec"
					var pos           = smonths.indexOf(text);
					this.hasmonthtext = (pos !== -1);
					if(this.hasmonthtext)
					{
                        // ucase 26: "12sepksdjfkasdff-12".replace(/sep[a-zA-Z]{1,}/i,"sep") -> "12sep-12"
                        var badmonthspellingpattern = new RegExp(text + "[a-zA-Z]{1,}","i");
                        this.sd_target = temp.replace(badmonthspellingpattern,text);

						// "janfebmaraprmayjunjulaugsepoctnovdec".indexOf("dec") / 3 -> 11
						this.monthtext   = text;
						this.monthnumber = (pos / 3) + 1;
					}
					else
					{
						// the text doesn't match a valid month string
						this.seterror(0);
					}
				}
//console.log("this.monthtext: " + this.monthtext);				
					
				var temp1 = "";
				// 1st pass get rid off any escaped or non-printable characters or spaces
				for(var i = 0; i < this.sd_target.length; i += 1)
				{
				    var c       = this.sd_target.charAt(i);
				    var c_ascii = this.sd_target.charCodeAt(i);
					
					if(c_ascii > 31 && c_ascii !== 92 && c_ascii !== 32)  // refer to an ascii chart
					{
						temp1 += c;
					}
//console.log("temp1|c_ascii: " + temp1 + "|" + c_ascii);				
				}
				this.sd_target = temp1;
//console.log("this.sd_target: " + this.sd_target);				

				// convert these -. to / 
				// easier to do in a Regexp than in the loop above (ucase 14 thru 18)
				var temp2 = "";
				temp2 = temp1.replace(/[-.]+/g,"/")
//console.log("temp2: " + temp2);				
				this.sd_target = temp2;
//console.log("this.sd_target: " + this.sd_target);				

				// ok what are we left with
				this.allnumbers = !isNaN(this.sd_target);
				this.blank = this.isnullorblank(this.sd_target);
				if(this.allnumbers && !this.blank)
				{
					this.allnumberstransform();
				}

				if(isRequired && this.blank)
				{
					this.seterror(1);  // (ucase 11)
				}
			};
			this.allnumberstransform = function(){	// ucase 9 and 10
				var daynumber = this.sd_target * 1; //coerce to a number
				if(daynumber > 0 && daynumber <= this.current.lastdd)
				{
					this.sd_target = this.current.mm + "/" + daynumber + "/" + this.current.yy;
				}
				else
				{
					this.seterror(2);
				}
			};
			this.bkslashtransform = function(){
				// ucase 1,2,5,6,7,8,12,13,19,20 
				// "/b/".split('/') -> ["", "b", ""]
				var tokens = this.sd_target.split('/');
				tokens = this.removeemptyarrayelements(tokens);
                                
				switch (tokens.length) {
					case 0:		// weird case - invalid ... error
//console.log("mp7: " + this.sd_target);
						this.seterror(3);
						return;
					case 1:
//console.log("mp8: " + this.sd_target);
						// 12/ or /12  also weird, assume this is the day of the current month
						var sdaynumber = tokens[0];
						if(!isNaN(sdaynumber))
						{
							var daynumber = sdaynumber * 1; //coerce to a number
							if(daynumber > 0 && daynumber <= this.current.lastdd)
							{	// continue: its a valid day for the current month
								this.sd_target = this.current.mm + "/" + daynumber + "/" + this.current.yy;
								this.specialcaseadvanceyearapplies = true;
							}
						}
						else
						{	// weird case - invalid ... error
							this.seterror(3);
							return;
						}
						break;
					case 2:					// ucase 1,2,12
//console.log("mp9: " + this.sd_target);
						var smm = tokens[0];
						var sdd = tokens[1];
						if(this.isnullorblank(smm) || this.isnullorblank(sdd)) 
						{
							this.seterror(3);
							return;
						}
							
						var mm = smm * 1;		// coerce to a number    
						var dd = sdd * 1;
						if(mm < 1 || mm > 12 || dd < 1 || dd > this.lastdayofmonth(mm,this.current.yy))
						{
							this.seterror(3);
							return;
						}
							
						this.sd_target = mm + "/" + dd + "/" + this.current.yy;
						this.specialcaseadvanceyearapplies = true;
						break;
					case 3:				// // ucase 5,7,8,13,19,20,22, normal case, something/something/something
//console.log("mp10: " + this.sd_target);
						var smm      = tokens[0];
						var sdd      = tokens[1];
						var syy      = tokens[2];
						var yylength = syy.length;
						if(this.isnullorblank(smm) || this.isnullorblank(sdd) || this.isnullorblank(syy))
						{
							this.seterror(3);
							return;
						}
						
//console.log("mp10-1: " + this.sd_target);
						if(yylength > 4)    // ucase 8
						{
							syy = syy.slice(0,4)
							yylength = 4;
						}
						if(yylength == 1 || yylength == 3)    
						{
							this.seterror(3);
							return;
						}

//console.log("mp10-2: " + this.sd_target);
						var mm = smm * 1;		// coerce to a number    
						var dd = sdd * 1;
						var yy = syy * 1;
						if(yylength == 2)    
							yy = 2000 + yy;
						
						var sdate = mm + "/" + dd + "/" + yy;
						var d     = new Date(sdate);
						if(isNaN(d))
						{
							this.seterror(4);
							return;
						}
						
//console.log("mp10-3: " + this.sd_target);
//console.log("mm dd yy: " + mm + " " + dd + " " + yy);
//console.log("lastdayofmonth(mm,yy): " + this.lastdayofmonth(mm,yy));
						if(mm < 1 || mm > 12 || dd < 1 || dd > this.lastdayofmonth(mm,yy))
						{
							this.seterror(4);
							return;
						}
						if(this.exceedsmaxdate(sdate))
						{
							this.seterror(6);
							return;
						}
							
//console.log("mp10-4: " + this.sd_target);
						// we should have a good date now
						this.sd_target = (mm < 10 ? "0" + mm : mm) + "/" + (dd < 10 ? "0" + dd : dd) + "/" + yy;
//console.log("yea!: " + this.sd_target);
						break;
					default : 
//console.log("*** SHOULDN GET HERE *** mp11: " + this.sd_target);
						this.seterror(3);
				}
			};
			this.monthtexttransform = function(){ 		// ucase 3,4,16,17
				/*		3   14oct           10/14/2012 - use current year
						4   14oct12         10/14/2012
						16  14-oct          10/14/2012 - use current year
						17  14-oct-12       10/14/2012
						note: delimiters have already been converted to '/'
				*/                            
				var smm    = this.monthtext;
				var imm    = this.monthnumber;
				var tokens = this.sd_target.replace(/[/]+/g,"").split(smm);  // " 14/oct/12".replace(/[/]+/g,"").split("oct") -> ["14", "12"]
				tokens = this.removeemptyarrayelements(tokens);

				switch(tokens.length)
				{
					case 0:		// "oct" invalid
						{
							this.seterror(3);
							return;
						}
					case 1:		// "14oct" 
						var sdd = tokens[0];
						if(this.isnullorblank(sdd) || isNaN(sdd))
						{
							this.seterror(3);
							return;
						}
						var idd = sdd * 1;		// coerce to a number
						if(idd < 1 || idd > this.lastdayofmonth(imm,this.current.yy))
						{
							this.seterror(3);
							return;
						}
						this.sd_target = imm + "/" + tokens[0] + "/" + this.current.yy;
						this.specialcaseadvanceyearapplies = true;
						break;
					case 2:		// "14oct12"
						var sdd = tokens[0];
						var syy = tokens[1];
						if(this.isnullorblank(sdd) || isNaN(sdd) || this.isnullorblank(syy) || isNaN(syy) )
						{
							this.seterror(3);
							return;
						}
						var idd = sdd * 1;		// coerce to a number
						var iyy = (syy.length === 2 ? 2000 + (syy * 1) : syy * 1);
						
						if(idd < 1 || idd > this.lastdayofmonth(imm,iyy))
						{
							this.seterror(3);
							return;
						}
						this.sd_target = imm + "/" + idd + "/" + iyy;
						break;
					default:
						return false;
					}
			};
			this.validate = function(){
				this.precheck();
				if(!this.hasvalidationerror && !this.blank)
				{
					if(this.hasmonthtext)
					{
						this.monthtexttransform();
					}
					this.bkslashtransform();
					if(this.specialcaseadvanceyearapplies)
					{
						this.advanceyear();
					}
					var inthepasttest = this.isinthepast(this.sd_target);
					if(inthepasttest === undefined || inthepasttest)
					{
						this.seterror(5);
					} 
				}
//console.log("done! - this.sd_target: " + this.sd_target);		
//console.log(this.validationerrormsg);		
				return !this.hasvalidationerror;
			};
			
		};
		// -----------------------------------------
		//  end of the updated date field validator
		// -----------------------------------------
        // M40297,M41054 / end

        //users can enter date in different formats, e.g, 1/1, Jan 1, January 1, format the date to mm/dd/yyyy for consistency
        function formatDate(dateCtl) {
            var dateVal = dateCtl.val();
            var dateWasFixed = false;
            if (isNaN(Date.parse(dateVal))) {
                dateVal = fixDate(dateVal);
                dateWasFixed = true;
            }
            if (dateWasFixed == false) {
                //check if year was entered
                if (!isNaN(Date.parse(dateVal + ' ' + new Date().getFullYear()))) {
                    //year was not entered, fix date
                    dateVal = fixDate(dateVal);
                    dateWasFixed = true;
                }
            }
            if (!isNaN(Date.parse(dateVal))) {
                var thisDate = new Date(dateVal);
                if (!openEnded && thisDate < new Date()) {
                    //check century first, some browser will interpret 4/1/12 as 4/1/1912
                    var datValYear = thisDate.getFullYear() % 100;
                    var dateValFullYear = thisDate.getFullYear();
                    var datValCentury = dateValFullYear - datValYear;

                    var today = new Date();
                    var thisYear = today.getFullYear() % 100;
                    var thisFullYear = today.getFullYear();
                    var currentCentury = thisFullYear - thisYear;

                    var correctFullYear = 0;
                    if (dateWasFixed == false) {
                        var diff1 = Math.abs(thisFullYear - dateValFullYear);
                        var diff2 = Math.abs(thisFullYear - (currentCentury + datValYear));
                        if (diff1 > diff2) {
                            correctFullYear = currentCentury + datValYear;
                        }
                        else {
                            correctFullYear = dateValFullYear;
                        }
                    }
                    else {
                        correctFullYear = thisFullYear;
                    }
                    thisDate.setYear(correctFullYear);
                }
                else {
                    // M40142 / start *** Do this only on the Bag Service Guarantee page ***
                    var hasBSG_H1tag = ($('h1:contains("Bag Service Guarantee")').length > 0);
                    if (hasBSG_H1tag == true) {
                        //alert('hasBSG_H1tag is true');
                        // ticket description:  Travel occurred date must not be in the future, reset year to current year if this happens.
                        // If user enter only mm/dd, the year defaults to next year which is ok on other screens but not on the 
                        // baggage claim screen where the date establishes the date that some incident happened.
                        var rnow = new Date();
                        //alert('formatDate.here5: thisDate = ' + thisDate + ', rnow = ' + rnow + ', thisDate > rnow = ' + (thisDate > rnow) );                        
                        if (thisDate > rnow) {
                            var mm = thisDate.getMonth();
                            var dd = thisDate.getDate();
                            var yy = rnow.getFullYear() - 1;
                            //alert('formatDate.here6: mm = ' + mm + ', dd = ' + dd + ', yy = ' + yy);
                            thisDate = new Date(yy, mm, dd);
                            //alert('formatDate.here7: thisDate = ' + thisDate);                           
                        }
                        // M40142 / end
                    }
                }
                $(dateCtl).val(Calendar.DateToText(thisDate));
            }

        }

        //When users omit year in a date, most browsers won't recognize it as date, adding year to the value will fix it
        function fixDate(dateVal) {
            if (dateVal == null || dateVal == "") {
                return "";
            }

            var numChars = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
            var noNum = true;
            for (var i = 0; i < numChars.length; i++) {
                if (dateVal.indexOf(numChars[i]) >= 0) {
                    noNum = false;
                    break;
                }
            }

            var months = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
            if (noNum == true) {
                var isMonth = false;
                for (var i = 0; i < months.length; i++) {
                    if (dateVal.toLowerCase().indexOf(months[i]) == 0) {
                        dateVal = months[i] + " 1";
                        isMonth = true;
                        break;
                    }
                }
                if (!isMonth) {
                    return dateVal;
                }
            }
            else {
                for (var i = 0; i < months.length; i++) {
                    var monthIndex = dateVal.toLowerCase().indexOf(months[i]);
                    if (monthIndex > 0) {
                        var month = months[i];
                        var day = -1;
                        var year = new Date().getFullYear();
                        var num1 = parseInt(dateVal.substring(0, monthIndex));
                        if (!isNaN(num1)) {
                            if (num1 <= 31) {
                                day = num1;
                            }
                            else {
                                year = num1 < 100 ? 2000 + num1 : num1;
                            }
                        }
                        var num2 = "";
                        j = dateVal.length - 1;
                        while (j > 0 && !isNaN(parseInt(dateVal.charAt(j)))) {
                            num2 = dateVal.charAt(j) + num2;
                            j--;
                        }
                        num2 = parseInt(num2);
                        if (!isNaN(num2)) {
                            if (day == -1) {
                                if (num2 <= 31) {
                                    day = num2;
                                }
                                else {
                                    year = num2 < 100 ? 2000 + num2 : num2;
                                }
                            }
                            else {
                                year = num2 < 100 ? 2000 + num2 : num2;
                            }
                        }
                        if (day == -1) {
                            day = 1;
                        }
                        dateVal = month + " " + day + ", " + year;
                        break;
                    }
                }

            }
            var date = Date.parse(dateVal);

            if (isNaN(date)) {
                var dateChars = [",", ".", " ", "~", "-"];
                for (var i = 0; i < dateChars.length; i++) {
                    if (dateVal.indexOf(dateChars[i]) > 0) {
                        dateVal = dateVal.replace(dateChars[i], "/");
                        break;
                    }
                }
            }

            if (isNaN(Date.parse(dateVal)) || !isNaN(Date.parse(dateVal + " " + new Date().getFullYear()))) {
                var date1 = dateVal + " " + new Date().getFullYear();
                var date2 = dateVal + " " + (new Date().getFullYear() + 1);

                if (isNaN(Date.parse(date1))) {
                    return dateVal;
                }
                var date1Date = new Date(Date.parse(date1));
                var today = new Date();
                if (date1Date > today || (today.getMonth() == date1Date.getMonth() && today.getDate() == date1Date.getDate() && today.getYear() == date1Date.getYear())) {
                    return date1;
                }
                else {
                    return date2;
                }
            }
            else {
                return dateVal;
            }
        }
        //end fixdate

        $("#" + imgId).bind("click", function (e) {
            //format date in textbox to mm/dd/yyyy
            formatDate(txtBox);
            showCalendar(e, onselectdate);
        });

        function showCalendar(e, onselectdate) {

            if ($(startDateCtl).length > 0 && $(startDateCtl).val() != "" && !isNaN(Date.parse($(startDateCtl).val()))) {
				var startDateVal = new Date($(startDateCtl).val());
				var today = new Date();
                if (startDateVal < today) {
                    startDateVal.setYear(today.getFullYear());
                }
               if (!openEnded) {
                    daysLowerLimit = -Math.ceil(((startDateVal - today) / (24 * 60 * 60 * 1000)));
                }
            }

            var currentDate = new Date();

            if ($(startDateCtl).length > 0 && $(startDateCtl).val() != "" && !isNaN(Date.parse($(startDateCtl).val()))) {
                if (txtBox.val() != "") {
                    currentDate = new Date(Date.parse(txtBox.val()));
                    if (currentDate < new Date(Date.parse($(startDateCtl).val()))) {
                        currentDate = new Date(Date.parse($(startDateCtl).val()));
                    }
                }
                else {
                    currentDate = new Date(Date.parse($(startDateCtl).val()));
                }
            }
            else {

                if (txtBox.val() != "") {
                    currentDate = new Date(Date.parse(txtBox.val()));
                }
            }


            if (currentDate == null || currentDate.getMonth() == null || isNaN(currentDate.getMonth())) {
                currentDate = new Date();
            }
            var month = currentDate.getMonth();
            var monthday = currentDate.getDate();
            var year = currentDate.getFullYear();
            if (!openEnded && daysLowerLimit <= 0) {
                year = currentDate.getFullYear() >= new Date().getFullYear() ? currentDate.getFullYear() : new Date().getFullYear();
            }

            if ($("#" + Calendar.DivCalContainerId).length <= 0) {
                var domainUrl = "http://www.alaskaair.com/files/www.alaskaair.com";
                try {
                    if (asglobal != null && asglobal.domainUrl != null && asglobal.domainUrl != "") {
                        domainUrl = asglobal.domainUrl
                    }
                    else {
                        if (parent.asglobal != null && parent.asglobal.domainUrl != null && parent.asglobal.domainUrl != "") {
                            domainUrl = asglobal.domainUrl
                        }
                    }
                }
                catch (e) {
                }
                var calHtml = "<div id='" + Calendar.DivCalContainerId + "' style=' width:365px;z-index:9999999;box-sizing:content-box;'>";
                calHtml += "<div id='divCalInner'>";
                if ($.browser.msie && $.browser.version == 6) {
                    calHtml += "<iframe src='javascript:false;' frameborder='no' id='iCalFiller' name='iCalFiller' style='position:absolute; z-index:-1; width:365px; border:solid 0px; height:180px;'></iframe>";
                }
                calHtml += "<div><div style='margin-left:3px;right:-15px; width:26px; height:26px; background:url(https://www.alaskaair.com/images/Popup_Close_X.png) no-repeat;position:absolute; margin-top:-18px; cursor: pointer; z-index:9999999'  id='divCloseCal'></div></div>";

                calHtml += "<div style='background-color:#EFF5f9; margin-top:0px;padding:5px 0px 0px 0px;'>";
                
                //Set styles
                calHtml += "<style>";
                calHtml += "#" + Calendar.DivCalContainerId + "{position:absolute;font-size:12px; font-family:Arial,Verdana;background:white;border:solid 5px #5f87bb; z-index: -1;}";
                calHtml += "#" + Calendar.DivCalContainerId + " * {margin:0px; padding:0px;}";
                calHtml += "#" + Calendar.DivCalContainerId + " table{margin-bottom:0px;}";
                calHtml += "td.prevMonth{height:20px; width:10px; background: url(https://" + domainUrl + "/icons/calendar_chevrons.jpg) 0px 0px no-repeat; overflow:hidden;cursor:pointer} ";
                calHtml += "td.prevMonth:hover {background: url(https://" + domainUrl + "/icons/calendar_chevrons.jpg) -0px -20px no-repeat;overflow:hidden;}";
                calHtml += "td.prevMonthDisabled {height:20px; width:10px; background: url(https://" + domainUrl + "/icons/calendar_chevrons.jpg) 0px -40px no-repeat; overflow:hidden; cursor:default;}";
                calHtml += "td.nextMonth {height:20px; width:10px; background: url(https://" + domainUrl + "/icons/calendar_chevrons.jpg) -10px -0px no-repeat; overflow:hidden; cursor:pointer;}";
                calHtml += "td.nextMonth:hover {background: url(https://" + domainUrl + "/icons/calendar_chevrons.jpg) -10px -20px no-repeat;overflow:hidden;}";
                calHtml += "td.nextMonthDisabled {height:20px; width:10px; background: url(https://" + domainUrl + "/icons/calendar_chevrons.jpg) -10px -40px no-repeat; overflow:hidden; cursor:default;}";
                calHtml += "#tblAsCalMonths, #tblMonthNavigation{width:170px;}";
                calHtml += "#tblAsCalMonths td{width:16%; font-weight:bold; cursor:pointer;text-align:center;color:#01194C;}";
                calHtml += "#tblAsCalMonths td:hover, #tblAsCalMonths td.current{background-color:#9fb069; color:maroon;}";
                calHtml += "#tblPreviousMonthNavigation td{color:#01194C;vertical-align:middle;}";
                calHtml += "#tblPreviousMonthNavigation td.spacer{width:10px}";
                calHtml += "#tblPreviousMonthNavigation #tdAsCalNextMonth{text-align:right;}";
                calHtml += "#tblPreviousMonthNavigation #tdCurrentMonth1{font-weight:bold;width:130px; text-align:center;}";
                calHtml += "#tblNextMonthNavigation td{color:#01194C;vertical-align:middle;}";
                calHtml += "#tblNextMonthNavigation td.spacer{width:10px;height:2px;}";
                calHtml += "#tblNextMonthNavigation #tdAsCalNextMonth{text-align:right;}";
                calHtml += "#tblNextMonthNavigation #tdCurrentMonth2{font-weight:bold;width:130px; text-align:center;}";
                calHtml += "#tblAsCalDays{width:170px; height:120px;}";
                calHtml += "#tblAsCalDays td{width:14%; text-align:center; font-weight:bold; vertical-align:middle}";
                calHtml += "#trDOW td{font-weight:bold; font-size:11px;}";
                calHtml += "</style>";

                //Month navigator
                calHtml += "<div id='monthNavigator' style='font-weight:bold;font-size:13px;width:100%;text-align:center;height:30px;background-color:#EFF5f9'>";
                calHtml += "<table style='width:100%;'>";  
                calHtml += "<tr>";
                calHtml += "<td style='width:50%;text-align:right;font: bold 12px Verdana, Arial, Helvetica, sans-serif;color: #01194c;padding:4px 5px 0 0;'>";
                calHtml += "Go to another month:";
                calHtml += "</td>";
                calHtml += "<td style='width:50%;text-align:left;'>";

                calHtml += "<select id='monthNavigatorDropdown' style='font-weight: normal;'>";
                calHtml += "</select>";

                calHtml += "</td>";
                calHtml += "</tr>";
                calHtml += "</table>";
                calHtml += "</div>";
                calHtml += "</div>";

                calHtml += "<div style='background-color:white; margin-top:0px;padding:0px 5px 0px 5px;'>";

                //Previous month navigator
                calHtml += "<div style='float:left;padding:0 0 0 0;margin:0 0 0 0;'>";
                calHtml += "<table id='tblPreviousMonthNavigation' cellspacing='0' cellpadding='0' style='padding:0 0 0 0;><tr>";
                calHtml += "<td class='spacer'>&nbsp;</td>";
                calHtml += "<td id='tdAsCalPrevMonth' class='prevMonth'>&nbsp;</td>";
                calHtml += "<td id='tdCurrentMonth1' align='center'>October 2010</td>";
                calHtml += "<td class='spacer'>&nbsp;</td>";
                calHtml += "</tr></table>";

                //Start of first calendar
                calHtml += "<table cellspacing=0 cellpadding=1 id='tblAsCalDays' style='float:left'>";
                calHtml += "<tr><td colspan='7' height='12' style='margin: 0 0 0 0;padding:0 0 0 0;'><div style='border-top:solid 1px #dedede; margin:5px 0 0 0; height:12px;'></div></td></tr>";
                calHtml += "<tr id=trDOW><td>Su</td><td>Mo</td><td>Tu</td><td>We</td><td>Th</td><td>Fr</td><td>Sa</td></tr>";

                for (i = 1; i <= 6; i++) {
                    calHtml += "<tr>";
                    for (j = 1; j <= 7; j++) {
                        calHtml += "<td class='day' month='0' year='0' id='td" + i + "_" + j + "_1" + "'>&nbsp;</td>";
                    }/// <reference path="../common/" />

                    calHtml += "</tr>";
                }
                calHtml += "</table>";
                calHtml += "</div>";

                //Vertical separator
                calHtml += "<div style='float:left;margin:0 5px 0 5px;' >";
                calHtml += "<table><tr><td width='1'>";
                calHtml += "<div style='border-left:solid 1px #dedede; margin:0 0 0 0; height:185px;'></div>";
                calHtml += "</td></tr></table></div>";
                
                //Next month navigator
                calHtml += "<div style='float:left;padding:16px 0 0 0;'>";
                calHtml += "<table id='tblNextMonthNavigation' cellspacing='0' cellpadding='0' style='padding:0 0 0 0;width:100%;'><tr>";
                calHtml += "<td class='spacer'>&nbsp;</td>";
                calHtml += "<td id='tdCurrentMonth2' align='center'>October 2010</td>";
                calHtml += "<td id='tdAsCalNextMonth'  class='nextMonth'>&nbsp;</td>";
                //calHtml += "<td class='spacer'>&nbsp;</td>";
                calHtml += "</tr></table>";
                
                //Start of second calendar
                calHtml += "<table cellspacing=0 cellpadding=1 id='tblAsCalDays' style='float:left'>";
                calHtml += "<tr><td colspan='7' height='12' style='margin: 0 0 0 0;padding:0 0 0 0;'><div style='border-top:solid 1px #dedede; margin:5px 0 0 0; height:12px;'></div></td></tr>";
                calHtml += "<tr id=trDOW><td>Su</td><td>Mo</td><td>Tu</td><td>We</td><td>Th</td><td>Fr</td><td>Sa</td></tr>";

                for (i = 1; i <= 6; i++) {
                    calHtml += "<tr>";
                    for (j = 1; j <= 7; j++) {
                        calHtml += "<td class='day' month='0' year='0' id='td" + i + "_" + j + "_2" + "'>&nbsp;</td>";
                    }
                    calHtml += "</tr>";
                }
                calHtml += "</table>";
                calHtml += "</div>";
                calHtml += "</div>";
                calHtml += "</div>";
                calHtml += "</div>";


                $("body").append(calHtml);
                var cornerRadius = 8;
                var boxShadowStyle = "3px 3px 3px #999";
                $("#" + Calendar.DivCalContainerId).css({ padding: "0 0 5px 0", "-moz-border-radius": cornerRadius }).css({ "-webkit-border-radius": cornerRadius }).css({ "border-radius": cornerRadius });
                $("#divCalInner").css({ "-moz-border-radius": cornerRadius }).css({ "-webkit-border-radius": cornerRadius }).css({ "border-radius": cornerRadius });
                $("#" + Calendar.DivCalContainerId).css({ "box-shadow": boxShadowStyle }).css({ "-webkit-box-shadow": boxShadowStyle }).css({ "-moz-box-shadow": boxShadowStyle });
                $("#" + Calendar.DivCalContainerId).click(function (e) {
                    Widget.CancelBubbleAs(e);
                });
                if ($.browser.msie) {
                    $("#divCloseCal").css({ "margin-top": "-15px" });
                }

                //Populate the month picker using lower and upper limit
                var todayPlus330 = new Date(Calendar.GetUpperLimitDate(daysUpperLimit));
                var lowerLimitDate = new Date(Calendar.GetLowerLimitDate(daysLowerLimit));
                var pickerDate = new Date(lowerLimitDate); 
                pickerDate.setDate(1);

                if(pickerDate.getMonth() == currentDate.getMonth())
                {
                    $("#monthNavigatorDropdown").append('<option selected=selected value=' + pickerDate.getFullYear() + '/' + (pickerDate.getMonth()+1) + '/1' + '>' + Calendar.GetMonthName(pickerDate.getMonth()) + ' ' + pickerDate.getFullYear() + '</option>');
                }
                else
                {
                    $("#monthNavigatorDropdown").append('<option value=' + pickerDate.getFullYear() + '/' + (pickerDate.getMonth()+1) + '/1' + '>' + Calendar.GetMonthName(pickerDate.getMonth()) + ' ' + pickerDate.getFullYear() + '</option>');
                }
                todayPlus330.setMonth((todayPlus330.getMonth()-1))
                while(pickerDate < todayPlus330)
                {
                    pickerDate.setMonth(pickerDate.getMonth()+1);
                    if(pickerDate.getMonth() == currentDate.getMonth())
                    {
                        $("#monthNavigatorDropdown").append('<option selected=selected value=' + pickerDate.getFullYear() + '/' + (pickerDate.getMonth()+1) + '/1' + '>' + Calendar.GetMonthName(pickerDate.getMonth()) + ' ' + pickerDate.getFullYear() + '</option>');
                    }
                    else
                    {
                        $("#monthNavigatorDropdown").append('<option value=' + pickerDate.getFullYear() + '/' + (pickerDate.getMonth()+1) + '/1' + '>' + Calendar.GetMonthName(pickerDate.getMonth()) + ' ' + pickerDate.getFullYear() + '</option>');
                    }
                }

                $("#tdAsCalPrevMonth").bind("click", function () {
                    Calendar.GotoPreviousMonth();
                });

                $("#tdAsCalNextMonth").bind("click", function () {
                    Calendar.GotoNextMonth();
                });
                
                $("#divCloseCal").bind("click", function () {
                    Calendar.CloseCalPopup();
                });

                $("#monthNavigatorDropdown").change(function () {
                    var d;
                    $("#monthNavigatorDropdown option:selected").each(function() {
                        d = new Date($(this).val());
                    });
                    Calendar.BindMonth(Calendar.calTextboxCurrent, d.getMonth(), d.getFullYear(), daysUpperLimit, daysLowerLimit, Calendar.calTxtBoxD, onselectdate);
                });
            }

            //This function used to be in ScreenPosition.js. Removing dependency on that file for performance reasons, and its really outdated.
            function GetObjectPosition(obj) 
            {
	            var position = {x:0, y:0};
	            if (obj.offsetParent)
	            { 
		            while (obj.offsetParent)
		            { 
			            position.x += obj.offsetLeft;
			            position.y += obj.offsetTop;
			            obj = obj.offsetParent;
		            }
	            }
	            else if (obj.y || obj.x)
	            {
		            position.x += obj.x;
		            position.y += obj.y; 
	            }
	            return position;
            }

            function positionCalendar() {
                if (!isPopup) {
                    var position = GetObjectPosition(document.getElementById(txtBox[0].id));
                    top = position.y + txtBox.height() + 9;
                    left = txtBox.offset().left - $(window).scrollLeft();
                    if (alignRight) {
                        left = left + txtBox.width() - $("#" + Calendar.DivCalContainerId).width();
                    }
                }
                $("#" + Calendar.DivCalContainerId).css({ top: top, left: left });
                $("#" + Calendar.DivCalContainerId).show();
                Widget.HideDD();

            }
            positionCalendar();
            $(window).resize(function () {
                if ($("#" + Calendar.DivCalContainerId).css("display").toLowerCase() != "none") {
                    positionCalendar();
                }
            });
            Calendar.BindMonth(txtBox, month, year, daysUpperLimit, daysLowerLimit, endDateCtl, onselectdate);
            Widget.CancelBubbleAs(e);
        }

    });

    $(document).click(function (e) {    
		if (window.event) {
			e = window.event;
		}
		var srcEl = e.srcElement? e.srcElement : e.target;      
    
		if (!$(srcEl).hasClass(CALENDAR_TRIGGER_CLASS)) {
			Calendar.CloseCalPopup();
		}
    });

};

//ends: ascalendarwide.js
//starts: cities.js

var isImageRes = false;

jQuery.fn.getCities = function(city, inputDivId, contextKey, shoppingFormlet, retry, $ariaLog) {
    $ariaLog = $ariaLog || $('#' + this[0].id + "-log");
    var divId = inputDivId;
    var currentTextBox = $(this);
    //retrieve cities only if user is done typing
    if (city == $(this).val()) {
        var domainUrl = "http://www.alaskaair.com/files/www.alaskaair.com";
        try {
            if (asglobal != null && asglobal.domainUrl != null && asglobal.domainUrl != "") {
                domainUrl = asglobal.domainUrl;
            } else {
                if (parent.asglobal != null && parent.asglobal.domainUrl != null && parent.asglobal.domainUrl != "") {
                    domainUrl = asglobal.domainUrl;
                }
            }
        } catch (e) {
        }
        $.ajax({
            type: "POST",
            url: Widget.CurrentProtocol() + domainUrl + "/HomeWidget/GetCities",
            dataType: 'json',
            data: { prefixText: city, contextKey: contextKey },
            complete: function(response, textStatus) {
                list = $.parseJSON(response.responseText);
                var width = "auto";
                // do not show previous list
                if (city != $(currentTextBox).val()) {
                    return;
                }
                var defaultHTML = "<div style='padding:8px;'>Enter the first 3 characters of:<div>&bull;&nbsp;&nbsp;City Name</div><div>&bull;&nbsp;&nbsp;Airport Name or Code</div>";
                if (contextKey != null && contextKey.toLowerCase() == "partner") {
                    if (retry && list.length > 0) {
                        defaultHTML = "<div style='padding:8px;'>Select the Use Miles check box to search award destinations.</div>";
                        list = new Array;
                    } else {
                        defaultHTML += "</div>";
                    }
                } else {
                    defaultHTML += "<div>&bull;&nbsp;&nbsp;U.S. state, province, or Mexico</div></div>";
                }

                if (shoppingFormlet && list.length == 0 && contextKey != null && contextKey.toLowerCase() != "partner") {
                    contextKey = "partner";
                    currentTextBox.getCities(city, inputDivId, contextKey, shoppingFormlet, true, $ariaLog);
                } else {
                    makeDropdown($(currentTextBox), {
                        formatText: true,
                        searchText: city,
                        list: list,
                        divId: divId,
                        showArrow: false,
                        acceptCharInput: true,
                        width: width,
                        defaultHtml: defaultHTML,
                        selectTextOnClick: true,
                        $ariaLog: $ariaLog, 
			citycodeid: 'checkinorigin'
                    });
                    if (list.length > 0 && $(document.activeElement).attr("id") == currentTextBox.attr("id")) {
                        $("#" + divId).show();
                    }
                }
            }
        });
    }

    return this;
};

jQuery.fn.autoCompleteCities = function(contextKey, shoppingFormlet) {
    var myContextKey = contextKey;

    function _getKeyCode(e) {
        return e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
    }

    function _isEnterKey(e) {
        return _getKeyCode(e) == KeyCodes.Enter;
    }

    var $ariaLog = $('<div role="log" aria-atomic="true" aria-relevant="additions" aria-live="polite"></div>')
        .addClass('hiddenInLink')
        .attr('id', this.attr('id') + '-log');

    this.before($ariaLog);

    $(this).keypress(function(e) {
        if (isImageRes) {
            var currentTextBox = $(this);
            var city = $(this).val() + String.fromCharCode(_getKeyCode(e));
            var minChars = 1;
            if (city.length >= minChars) {

                $.ajax({
                    type: "POST",
                    url: Widget.CurrentProtocol() + "www.alaskaair.com" + "/HomeWidget/GetCities",
                    dataType: 'json',
                    data: { prefixText: city, contextKey: myContextKey() },
                    complete: function(response, textStatus) {

                        list = $.parseJSON(response.responseText);

                        var width = "auto";
                        // do not show previous list
                        try {
                            makeDropdown($(currentTextBox), {
                                formatText: true,
                                searchText: city,
                                list: list,
                                divId: "div0",
                                showArrow: false,
                                acceptCharInput: true,
                                width: width,
                                selectTextOnClick: true,
                                defaultHtml: "<div style='padding:8px;'>Enter the first 3 characters of:<div>&bull;&nbsp;&nbsp;City Name</div><div>&bull;&nbsp;&nbsp;Airport Name or Code</div><div>&bull;&nbsp;&nbsp;U.S. state, province, or Mexico</div></div>",
                                $ariaLog: $ariaLog, 
				citycodeid: 'checkinorigin'
                            });
                            $("#").show();
                        } catch (e) {
                        }
                    }
                });
            }
        }
        return !_isEnterKey(e);
    });

    //Is this iterating over 1 element?
    $(this).each(function(i) {
        var currentTextBox = $(this);
        if (currentTextBox.is("input:text")) {
            var divId = "div" + i + Widget.GetRandomId();
            $(currentTextBox).bind("keyup", function(e) {
                var minChars = 1;
                if ($(this).val().length >= minChars) {
                    var inputCity = $(this).val();
                    var keyID = _getKeyCode(e);
                    var index = $.inArray(keyID, [KeyCodes.DownArrow, KeyCodes.UpArrow, KeyCodes.Enter, KeyCodes.Tab, KeyCodes.End, KeyCodes.Home, KeyCodes.Shift]);
                    if (index == -1) {
                        var calContextKey = "codeshare";
                        if (myContextKey != null) {
                            if (typeof myContextKey == 'function') {
                                calContextKey = myContextKey();
                            } else {
                                calContextKey = myContextKey;
                            }
                        }
                        //Shopping formlet might in the wrong position... retry is supposed to be a boolean.
                        currentTextBox.getCities(inputCity, divId, calContextKey, shoppingFormlet, false, $ariaLog);
                    }
                }
            });
            $(currentTextBox).attr({ "autocomplete": "off", maxlength: 60 });
        }
    });
};

//Special jquery extension for left on board page see backend action for more info..
jQuery.fn.autoCompleteCitiesLeftOnBoard = function(contextKey, shoppingFormlet) {

    var $ariaLog = $('<div role="log" aria-atomic="true" aria-relevant="additions" aria-live="polite"></div>')
        .addClass('hiddenInLink')
        .attr('id', this.attr('id') + '-log');
    var myContextKey = contextKey;

    function _getKeyCode(e) {
        return e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
    }

    function _isEnterKey(e) {
        return _getKeyCode(e) == KeyCodes.Enter;
    }


    $(this).keypress(function(e) {

        if (isImageRes) {
            var currentTextBox = $(this);
            var city = $(this).val() + String.fromCharCode(_getKeyCode(e));
            var minChars = 1;
            if (city.length >= minChars) {

                $.ajax({
                    type: "POST",
                    url: Widget.CurrentProtocol() + "www.alaskaair.com" + "/HomeWidget/GetCitiesLeftOnBoard",
                    dataType: 'json',
                    data: { prefixText: city, contextKey: myContextKey() },
                    complete: function(response, textStatus) {

                        list = $.parseJSON(response.responseText);

                        var width = "auto";
                        // do not show previous list
                        try {
                            makeDropdown($(currentTextBox), {
                                formatText: true,
                                searchText: city,
                                list: list,
                                divId: "div0",
                                showArrow: false,
                                acceptCharInput: true,
                                width: width,
                                selectTextOnClick: true,
                                defaultHtml: "<div style='padding:8px;'>Enter the first 3 characters of:<div>&bull;&nbsp;&nbsp;City Name</div><div>&bull;&nbsp;&nbsp;Airport Name or Code</div><div>&bull;&nbsp;&nbsp;U.S. state, province, or Mexico</div></div>", 
				                citycodeid: 'checkinorigin',
                                $ariaLog: $ariaLog, 
                            });
                            $("#").show();
                        } catch (e) {
                        }
                    }
                });
            }
        }
        return !_isEnterKey(e);
    });

    $(this).each(function(i) {
        var currentTextBox = $(this);
        if (currentTextBox.is("input:text")) {
            var divId = "div" + i + Widget.GetRandomId();
            $(currentTextBox).bind("keyup", function(e) {
                var minChars = 1;
                if ($(this).val().length >= minChars) {
                    var inputCity = $(this).val();
                    var keyID = _getKeyCode(e);
                    var index = $.inArray(keyID, [KeyCodes.DownArrow, KeyCodes.UpArrow, KeyCodes.Enter, KeyCodes.Tab, KeyCodes.End, KeyCodes.Home, KeyCodes.Shift]);
                    if (index == -1) {
                        var calContextKey = "codeshare";
                        if (myContextKey != null) {
                            if (typeof myContextKey == 'function') {
                                calContextKey = myContextKey();
                            } else {
                                calContextKey = myContextKey;
                            }
                        }
                        currentTextBox.getCitiesLeftOnBoard(inputCity, divId, calContextKey, shoppingFormlet);
                    }
                }
            });
            $(currentTextBox).attr({ "autocomplete": "off", maxlength: 60 });
        }
    });
};

jQuery.fn.getCitiesLeftOnBoard = function(city, inputDivId, contextKey, shoppingFormlet, retry) {
    var divId = inputDivId;
    var currentTextBox = $(this);
    var $ariaLog = $('<div role="log" aria-atomic="true" aria-relevant="additions" aria-live="polite"></div>')
    .addClass('hiddenInLink')
    .attr('id', this.attr('id') + '-log');
    //retrieve cities only if user is done typing
    if (city == $(this).val()) {
        var domainUrl = "http://www.alaskaair.com/files/www.alaskaair.com";
        try {
            if (asglobal != null && asglobal.domainUrl != null && asglobal.domainUrl != "") {
                domainUrl = asglobal.domainUrl;
            } else {
                if (parent.asglobal != null && parent.asglobal.domainUrl != null && parent.asglobal.domainUrl != "") {
                    domainUrl = asglobal.domainUrl;
                }
            }
        } catch (e) {
        }
        $.ajax({
            type: "POST",
            url: Widget.CurrentProtocol() + domainUrl + "/HomeWidget/GetCitiesLeftOnBoard",
            dataType: 'json',
            data: { prefixText: city, contextKey: contextKey },
            complete: function(response, textStatus) {
                list = $.parseJSON(response.responseText);
                var width = "auto";
                // do not show previous list
                if (city != $(currentTextBox).val()) {
                    return;
                }
                var defaultHTML = "<div style='padding:8px;'>Enter the first 3 characters of:<div>&bull;&nbsp;&nbsp;City Name</div><div>&bull;&nbsp;&nbsp;Airport Name or Code</div>";
                if (contextKey != null && contextKey.toLowerCase() == "partner") {
                    if (retry && list.length > 0) {
                        defaultHTML = "<div style='padding:8px;'>Select the Use Miles check box to search award destinations.</div>";
                        list = new Array;
                    } else {
                        defaultHTML += "</div>";
                    }
                } else {
                    defaultHTML += "<div>&bull;&nbsp;&nbsp;U.S. state, province, or Mexico</div></div>";
                }

                if (shoppingFormlet && list.length == 0 && contextKey != null && contextKey.toLowerCase() != "partner") {
                    contextKey = "partner";
                    currentTextBox.getCities(city, inputDivId, contextKey, shoppingFormlet, true);
                } else {
                    makeDropdown($(currentTextBox), {
                        formatText: true,
                        searchText: city,
                        list: list,
                        divId: divId,
                        showArrow: false,
                        acceptCharInput: true,
                        width: width,
                        selectTextOnClick: true,
                        defaultHtml: defaultHTML, 
			            citycodeid: 'checkinorigin',
                        $ariaLog: $ariaLog 
                    });
                    if (list.length > 0 && $(document.activeElement).attr("id") == currentTextBox.attr("id")) {
                        $("#" + divId).show();
                    }
                }
            }
        });
    }

    return this;
};


jQuery.fn.GetCitiesASQXNotAllAirports = function(city, inputDivId, contextKey) {
    var divId = inputDivId;
    var currentTextBox = $(this);
    var $ariaLog = $('<div role="log" aria-atomic="true" aria-relevant="additions" aria-live="polite"></div>')
    .addClass('hiddenInLink')
    .attr('id', this.attr('id') + '-log');
    //retrieve cities only if user is done typing
    if (city == $(this).val()) {
        var domainUrl = "http://www.alaskaair.com/files/www.alaskaair.com";
        try {
            if (asglobal != null && asglobal.domainUrl != null && asglobal.domainUrl != "") {
                domainUrl = asglobal.domainUrl;
            } else {
                if (parent.asglobal != null && parent.asglobal.domainUrl != null && parent.asglobal.domainUrl != "") {
                    domainUrl = asglobal.domainUrl;
                }
            }
        } catch (e) {
        }
        $.ajax({
            type: "POST",
            url: Widget.CurrentProtocol() + domainUrl + "/HomeWidget/GetCitiesASQXNotAllAirports",
            dataType: 'json',
            data: { prefixText: city, contextKey: contextKey },
            complete: function(response, textStatus) {
                list = $.parseJSON(response.responseText);
                var width = "auto";
                // do not show previous list
                if (city != $(currentTextBox).val()) {
                    return;
                }
                makeDropdown($(currentTextBox), {
                    formatText: true,
                    searchText: city,
                    list: list,
                    divId: divId,
                    showArrow: false,
                    acceptCharInput: true,
                    width: width,
                    selectTextOnClick: true,
                    defaultHtml: "<div style='padding:8px;'>Enter the first 3 characters of:<div>&bull;&nbsp;&nbsp;City Name</div><div>&bull;&nbsp;&nbsp;Airport Name or Code</div><div>&bull;&nbsp;&nbsp;U.S. state, province, or Mexico</div></div>", 
		    citycodeid: 'checkinorigin',
                    $ariaLog: $ariaLog
                });
                if (list.length > 0 && $(document.activeElement).attr("id") == currentTextBox.attr("id")) {
                    $("#" + divId).show();
                }
            }
        });
    }

    return this;
};

jQuery.fn.autoCompleteCitiesASQXNotAllAirports = function(contextKey) {
    var myContextKey = contextKey;
    $(this).each(function(i) {
        var currentTextBox = $(this);
        if (currentTextBox.is("input:text")) {
            var divId = "div" + i + Widget.GetRandomId();
            $(currentTextBox).bind("keyup", function(e) {
                var minChars = 1;
                if ($(this).val().length >= minChars) {
                    var inputCity = $(this).val();
                    var keyID = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
                    var index = $.inArray(keyID, [KeyCodes.DownArrow, KeyCodes.UpArrow, KeyCodes.Enter, KeyCodes.Tab, KeyCodes.End, KeyCodes.Home, KeyCodes.Shift]);
                    if (index == -1) {
                        var calContextKey = "none";
                        if (myContextKey != null) {
                            if (typeof myContextKey == 'function') {
                                calContextKey = myContextKey();
                            } else {
                                calContextKey = myContextKey;
                            }
                        }
                        currentTextBox.GetCitiesASQXNotAllAirports(inputCity, divId, calContextKey);
                    }
                }
            });
            $(currentTextBox).attr({ "autocomplete": "off", maxlength: 60 });
        }
    });
};

//ends: cities.js
//starts: shopping.js
var shoppingVar = new ShoppingParams();

var shoppingEnum = {
    WidgetType : { STANDARD: 'standard', VERTICAL: 'vertical', VERTICALIFRAME: 'vertical-iframe' }
};

//IMPORTANT! the ShoppingParams Javascript class properties MUST CORRESPOND EXACTLY to the ShoppingRequestModel C# class or shopping will fail.
//           Refer to ShoppingRequestModel.cs
function ShoppingParams() {
    this.IsRoundTrip = { Value: false, ControlId: "roundTrip", Type: "bool" };
    this.IsOneWay = { Value: false, ControlId: "oneWay", Type: "bool" };
    this.IsMultiCity = { Value: false, ControlId: "multiCity", Type: "bool" };
    this.IsAwardReservation = { Value: false, ControlId: "awardReservation", Type: "bool" };
    this.AdultCount = { Value: "", ControlId: "adultCount" };
    this.ChildrenCount = { Value: "", ControlId: "childrenCount" };
    this.UMNRAnswer = { Value: "", ControlId: "umnrAnswer" };
    this.DepartureCity1 = { Value: "", ControlId: "fromCity1", Type: "city" };
    this.DepartureCity2 = { Value: "", ControlId: "fromCity2", Type: "city" };
    this.DepartureCity3 = { Value: "", ControlId: "fromCity3", Type: "city" };
    this.DepartureCity4 = { Value: "", ControlId: "fromCity4", Type: "city" };
    this.ArrivalCity1 = { Value: "", ControlId: "toCity1", Type: "city" };
    this.ArrivalCity2 = { Value: "", ControlId: "toCity2", Type: "city" };
    this.ArrivalCity3 = { Value: "", ControlId: "toCity3", Type: "city" };
    this.ArrivalCity4 = { Value: "", ControlId: "toCity4", Type: "city" };
    this.DepartureDate1 = { Value: "", ControlId: "departureDate1" };
    this.DepartureDate2 = { Value: "", ControlId: "departureDate2" };
    this.DepartureDate3 = { Value: "", ControlId: "departureDate3" };
    this.DepartureDate4 = { Value: "", ControlId: "departureDate4" };
    this.DepartureTime1 = { Value: "", ControlId: "departureTime1" };
    this.DepartureTime2 = { Value: "", ControlId: "departureTime2" };
    this.DepartureTime3 = { Value: "", ControlId: "departureTime3" };
    this.DepartureTime4 = { Value: "", ControlId: "departureTime4" };
    this.ReturnDate = { Value: "", ControlId: "returnDate" };
    this.ReturnTime = { Value: "", ControlId: "returnTime" };
    this.IncludeNearbyDepartureAirports = { Value: "", ControlId: "includeNearbyDepartureAirports", Type: "bool" };
    this.IncludeNearbyArrivalAirports = { Value: "", ControlId: "includeNearbyArrivalAirports", Type: "bool" };
    this.ShopAwardCalendar = { Value: "", ControlId: "awardCalendar", Type: "bool" };
    this.ShopLowFareCalendar = { Value: "", ControlId: "lowFareCalendar", Type: "bool" };
    this.CabinType = { Value: "", ControlId: "cabinType" };
    this.FareType = { Value: "", ControlId: "fareType" };
    this.AwardOption = { Value: "", ControlId: "awardOption" };
    this.DiscountCode = { Value: "", ControlId: "discountCode" };
    this.ContractFareTypeKey = { Value: "", ControlId: "contractFareType" };
    this.ShowOnlyContractFares = { Value: "", ControlId: "showOnlyContractFares", Type: "bool" };
    this.ShowContractAndAllFares = { Value: "", ControlId: "showContractAndAllFares", Type: "bool" };
}

function ShoppingWidget(param) {
    var me = this, debug = false;
    var FlightType = { RoundTrip: 1, OneWay: 2, MultiCity: 3 },
        WidgetType = (param === undefined || param.WidgetType === undefined) ? shoppingEnum.WidgetType.STANDARD : param.WidgetType;

    var DestinationControlIds = { toCity: "toCity", divReturnDate: "divReturnDate", divReturnTime: "divReturnTime", divLowFareCalendar: "divLowFareCalendar" };
    var AwardsControlIds = { divFareType: "divFareType", divCabinType: "divCabinType", divAwardOption: "divAwardOption", divAboutAOIcon: "divAboutAOIcon", divDiscountCode: "divDiscountCode", divAboutDCIcon: "divAboutDCIcon", divLowFareCalendar: "divLowFareCalendar", divAwardCalendar: "divAwardCalendar", divIncludeNearbyDepartureAirports: "divIncludeNearbyDepartureAirports", divIncludeNearbyArrivalAirports: "divIncludeNearbyArrivalAirports", multicityLink: "multicityLink" };
    var oneWayCalContainerId = param != null && param.oneWayCalContainerId != null ? param.oneWayCalContainerId : "divCalOptionContainer1";

    this.ErrorClass = "input-validation-error";
    this.WidgetMainDivCssSelector = "";
    //This object corresponds to the ShoppingRequestModel C# class -- any changes here must also be made in the C# class
    //This object is used when invoking shopping on the server -- do not use for UI-related processing
    this.ShoppingRequest = function () {
        var shoppingRequest = new ShoppingParams();
        var fieldName = "";
        var fieldValue = null;
        for (var obj in shoppingVar) {
            fieldName = obj.toString();
            fieldValue = "";
            var prop = shoppingVar[fieldName];
            var title = "";
            if (prop.ControlId) {
                var ctl = $("#" + shoppingVar[fieldName].ControlId, me.WidgetMainDivCssSelector);
                if (ctl.length > 0) {
                    if (prop.Type != null && prop.Type == "bool") {
                        fieldValue = $("#" + shoppingVar[fieldName].ControlId + ":checked").length > 0;
                    }
                    else {
                        fieldValue = $("#" + shoppingVar[fieldName].ControlId).val();
                        title = $("#" + shoppingVar[fieldName].ControlId).attr("title");

                        if (fieldValue == title) {
                            fieldValue = "";
                        }
                    }

                    //if ($.inArray(fieldValue, ["From", "To", "mm/dd/yyyy", "Discount Code"]) != -1) {
                    //    fieldValue = "";
                    //}

                    shoppingRequest[fieldName] = fieldValue;
                }
                else {
                    if (prop.Type != null && prop.Type == "bool") {
                        fieldValue = false;
                        shoppingRequest[fieldName] = false;
                    }
                    else {
                        shoppingRequest[fieldName] = "";
                        fieldValue = "";
                    }
                }
                try {
                    $("input[name=" + fieldName + "]", me.WidgetMainDivCssSelector).val(fieldValue);
                }
                catch (e) {
                }
                $("#" + fieldName, me.WidgetMainDivCssSelector).val(fieldValue);
            }

        }

        return shoppingRequest;
    };

    this.saveShoppingRequest = function (shoppingRequest) {
        if (shoppingRequest && window.localStorage && window.JSON) {
            shoppingRequest.timestamp = new Date();
            window.localStorage.setItem('shoppingParams', JSON.stringify(shoppingRequest));
        }
    };

    this.BindFromShoppingRequest = function (shoppingRequest) {
        var fieldName = "";
        for (var obj in shoppingVar) {
            fieldName = obj.toString();
            var prop = shoppingVar[fieldName];
            if (prop.ControlId) {
                var ctl = $("#" + shoppingVar[fieldName].ControlId);
                if (ctl.length > 0) {
                    if (prop.Type != null && prop.Type == "bool") {
                        $("#" + shoppingVar[fieldName].ControlId).attr({ checked: shoppingRequest[fieldName] });
                    }
                    else {
                        $("#" + shoppingVar[fieldName].ControlId).val(shoppingRequest[fieldName]);
                    }
                }
            }
        }
    };

    this.SelectFlightType = function () {
        var flightType = $("input[name=flightType]:checked").val();
        $("#" + DestinationControlIds.divReturnDate).css({ visibility: "visible" });
        $("#" + DestinationControlIds.divReturnTime).css({ visibility: "visible" });
        if (flightType == FlightType.OneWay) {
            $("#" + DestinationControlIds.divReturnDate).css({ visibility: "hidden" });
            $("#" + DestinationControlIds.divReturnTime).css({ visibility: "hidden" });
            if (typeof homeWidgets === 'undefined') {
                $("#" + oneWayCalContainerId).append($("#divCalendarOption"));
            }

            $("#divReturningLabel").hide();

        } else {
        	if (typeof homeWidgets === 'undefined') {
                $("#divCalOptionContainer2").append($("#divCalendarOption"));
            }
            $("#divReturningLabel").show();

            var departureDate = Date.parse($("#departureDate1").val());
            var returnDate = Date.parse($("#returnDate").val());
            if (!isNaN(departureDate) && !isNaN(returnDate) && returnDate < departureDate) {
                $("#returnDate").val($("#departureDate1").val());
            }
        }
        me.ResetSDTexts();
        me.RaiseParentCallback();
    };

    this.ToggleAwardsReservation = function () {
        $("#" + AwardsControlIds.divFareType).show();
        $("#" + AwardsControlIds.divCabinType).show();
        $("#" + AwardsControlIds.divCabinType).css({ visibility: "visible" });
        $("#" + AwardsControlIds.divAwardOption).show();
        $("#" + AwardsControlIds.divAboutAOIcon).show();
        $("#" + AwardsControlIds.divDiscountCode).show();
        $("#" + AwardsControlIds.divAboutDCIcon).show();
        $("#" + AwardsControlIds.divLowFareCalendar).show();
        $("#" + AwardsControlIds.divAwardCalendar).show();
        if ($("#awardReservation").attr("checked")) {
            $("#" + AwardsControlIds.divFareType).hide();
            $("#" + AwardsControlIds.divDiscountCode).hide();
            $("#" + AwardsControlIds.divAboutDCIcon).hide();
            $("#" + AwardsControlIds.divLowFareCalendar).hide();
            if ($('#awardOption').length > 0 && $("#awardOption").val().indexOf("AS50") >= 0) {
                $("#" + AwardsControlIds.divAwardCalendar).hide();
            }
            else {
                $("#" + AwardsControlIds.divCabinType).css({ visibility: "hidden" });
            }
            if ($("#" + AwardsControlIds.multicityLink).length > 0) {
                var val = $("#" + AwardsControlIds.multicityLink).attr("href").replace("awards=no", "awards=yes");
                $("#" + AwardsControlIds.multicityLink).attr("href", val);
            }
        } else {
            $("#" + AwardsControlIds.divCabinType).hide();
            $("#" + AwardsControlIds.divAwardOption).hide();
            $("#" + AwardsControlIds.divAboutAOIcon).hide();
            $("#" + AwardsControlIds.divAwardCalendar).hide();
            if ($("#" + AwardsControlIds.multicityLink).length > 0) {
                var val = $("#" + AwardsControlIds.multicityLink).attr("href").replace("awards=yes", "awards=no");
                $("#" + AwardsControlIds.multicityLink).attr("href", val);
            }
        }
        me.ToggleNearbyAirportsVisibility();
    };

    this.ToggleNearbyAirportsVisibility = function () {
        var hide = false;
        if ($("#awardReservation").attr("checked")) {
            if ($('#awardOption').length > 0 && $("#awardOption").val().indexOf("AS50") >= 0) {
                hide = true;
            } else {
                hide = $("#awardCalendar").attr("checked");
            }
        } else {
            hide = $("#lowFareCalendar").attr("checked");
        }

        if (hide) {
            $("#" + AwardsControlIds.divIncludeNearbyDepartureAirports).css({ visibility: "hidden" });
            $("#" + AwardsControlIds.divIncludeNearbyArrivalAirports).css({ visibility: "hidden" });
        } else {
            $("#" + AwardsControlIds.divIncludeNearbyDepartureAirports).css({ visibility: "visible" });
            $("#" + AwardsControlIds.divIncludeNearbyArrivalAirports).css({ visibility: "visible" });
        }
    };

    this.PopulateFareTypeDD = function () {
        //Need to remove the content of the lightbox because it is different for multi-city
        $("#aboutCT_LB").remove();

        $("#fareType").html("");
        var flightType = $("input[name=flightType]:checked").val();
        if (flightType == 3) {
            $("#fareType").html($("#fareTypeMultiCity").html());
            $("#fareOptions").val($("#fareOptionsMultiCity").val());
        }
        else {
            $("#fareType").html($("#fareTypeOneWayRoundTrip").html());
            $("#fareOptions").val($("#fareOptionsOneWayRoundTrip").val());
        }
    };

    this.ShowErrors = function (validationResponse) {
        $("#findFlights").val("FIND FLIGHTS");

        me.ResetSDTexts();

        if (debug) console.log("ShoppingWidget.ShowErrors function started.");

        if (validationResponse == null || validationResponse.ErrorMessages == null) {
            $("#divValidationMessage").html("A system error occurred while shopping for flights. Please try again.").show();
            return;
        }

        var validationMessage = "";
        for (i = 0; i < validationResponse.ErrorMessages.length; i++) {
            if (validationResponse.ErrorMessages[i] != null) {
                validationMessage += "<div class='DC_Container_Parent'>" + validationResponse.ErrorMessages[i] + "<br></div>";
            }
        }
        $("#divValidationMessage").html(validationMessage).css({ visibility: "visible" }).show();

        if (validationResponse.ErrorFields != null) {
            if (debug) console.log("validationResponse.ErrorFields is not null");

            $("." + me.ErrorClass).removeClass(me.ErrorClass);
            for (i = 0; i < validationResponse.ErrorFields.length; i++) {
                if (debug) console.log("ErrorFields item " + i + " = " + validationResponse.ErrorFields[i]);
                if (shoppingVar[validationResponse.ErrorFields[i]] != null) {
                    if (debug) console.log("Setting error on field: " + validationResponse.ErrorFields[i]);
                    if (debug) console.log(shoppingVar[validationResponse.ErrorFields[i]].ControlId);
                    if (debug) console.log($("#" + shoppingVar[validationResponse.ErrorFields[i]].ControlId).length);
                    $("#" + shoppingVar[validationResponse.ErrorFields[i]].ControlId).addClass(me.ErrorClass);
                    if (debug) console.log($("#" + shoppingVar[validationResponse.ErrorFields[i]].ControlId).hasClass(me.ErrorClass));
                }
            }
        }
        else {
            if (debug) console.log("ErrorFields is null");
        }

        if (validationResponse.ErrorFields.length > 0) {
            $("#divValidationMessage").focus();
        }

        me.RaiseParentCallback();
    };

    this.RaiseParentCallback = function () {
        try {
            var height = $(me.WidgetMainDivCssSelector).height();
            parent.shoppingErrorCallback(height, me); //opportunity for parent(iframe host) to react to error such as adjusting iframe height,
        }
        catch (e) {
        }
    };

    this.ClearValidationMessage = function () {
        $("#divValidationMessage").css({ visibility: "hidden" });
    };

    this.ToggleWaitStatus = function (isWaiting) {
        if (isWaiting == true) {
            $("#findFlights").attr({ disabled: true });
        } else {
            $("#findFlights").attr({ disabled: false });
        }
    };

    this.ValidateUmnr = function (answer) {
        //Ajax call to get the error message.
        var shoppingRequest = me.ShoppingRequest();
        //Set this var since it is an ajax call.
        shoppingRequest.UMNRAnswer = answer;
        $("input[name=UMNRAnswer]").val(answer);
        var validateUrl = Widget.CurrentProtocol() + asglobal.domainUrl + "/Shopping/Flights/ValidateShoppingRequest";
        me.ClearValidationMessage();
        me.ToggleWaitStatus(true);

        //DO ajax call to validate the shopping request input
        $.ajax({
            type: "POST",
            url: validateUrl,
            dataType: "json",
            data: shoppingRequest,
            complete: function (response, textStatus) {

                var hasValidationError = true;
                var msg = response.responseText;

                if (msg != null && msg != "") {

                    var validationResponse = $.parseJSON(response.responseText);

                    if (validationResponse != null) {
                        if (!validationResponse.IsValidRequest) {
                            me.ShowErrors(validationResponse);
                        }
                        else {
                            me.SubmitRequest();
                        }
                    }
                }

                me.ToggleWaitStatus(false);
            }
        });
    };

    this.FindFlights = function (src, on_error) {
        if (debug) console.log("ShoppingWidget.FindFlights function started.");

        if (me.IsUmnr()) {
            // to avoid double validation request and await for yes/no light box response shown up front
            return;
        }

        var shoppingRequest = me.ShoppingRequest();

        var validateUrl = Widget.CurrentProtocol() + asglobal.domainUrl + "/Shopping/Flights/ValidateShoppingRequest";
        if (debug) console.log(validateUrl);

        me.ClearValidationMessage();

        me.ToggleWaitStatus(true);
        me.shoppingParams = shoppingRequest;
        //DO ajax call to validate the shopping request input
        $.ajax({
            type: "POST",
            url: validateUrl,
            dataType: "json",
            data: shoppingRequest,
            complete: function (response, textStatus) {

                var hasValidationError = true;
                var msg = response.responseText;
                if (debug) console.log("FindFlights validaterequest ajax call complete...");

                if (msg != null && msg != "") {
                    if (debug) console.log("FindFlights validaterequest ajax call returned something...");
                    if (debug) console.log(response.responseText);

                    var validationResponse = $.parseJSON(response.responseText);

                    if (debug) console.log("Json IsValidRequest = " + validationResponse.IsValidRequest);

                    if (validationResponse != null) {
                        if (validationResponse.IsValidRequest) {
                            me.saveShoppingRequest(me.shoppingParams);
                            me.SubmitRequest();
                        }
                        else {
                            me.ShowErrors(validationResponse);
                            if (on_error != null) {
                                try {
                                    on_error();
                                }
                                catch (e) {
                                }
                            }
                        }
                    }
                    else {
                        me.ShowErrors(null);
                        if (on_error != null) {
                            try {
                                on_error();
                            }
                            catch (e) {
                            }
                        }
                    }
                }
                me.ToggleWaitStatus(false);
            }
        });
    };

    this.IsUmnr = function () {
        var adtCount = parseInt($("#adultCount").val());
        var chdCount = parseInt($("#childrenCount").val());
        if (adtCount == 0 && chdCount > 0) {
            this.SetUMNRQuestion(chdCount);
            this.ShowUMNROptions();
        }
        return (parseInt($("#adultCount").val()) == 0 && parseInt($("#childrenCount").val()) > 0);
    };

    this.IsUmnrReissue = function () {
        var chdCount = 0;
        var adtCount = 0;
        $("#paxType0, #paxType1, #paxType2, #paxType3, #paxType4, #paxType5, #paxType6, #paxType7").each(function (i) {
            var travelerID = "#Travelers_" + i.toString() + "__IsSelected";
            if ($(travelerID).attr("checked")) {
                if ($(this).val() == "CHD") { chdCount++; }
                if ($(this).val() == "ADT") { adtCount++; }
            }
        });
        if (chdCount > 0 && adtCount == 0) {
            this.SetUMNRQuestion(chdCount);
            this.ShowUMNROptions();
            $("#submitUMNRRequest").attr('onclick', '');
            $("#submitUMNRRequest").bind('click', this.SubmitUMNRRequestReissue);
        } else {
            ShowInterstitial();
            $("#ShoppingForm").submit();
        }
    };

    this.SubmitUmnrRequest = function () {
        if (this.UMNRTypeSelected()) {
            if (WidgetType !== shoppingEnum.WidgetType.VERTICALIFRAME) {
                $.closeEzPopoups(true);
            }
            else {
                parent.$.closeEzPopoups(true);
            }
            if (debug) { console.log("UMNRAnswer is : " + $("input[name=UMNRAnswer]").val()); }
            me.ValidateUmnr($("input[name=UMNRAnswer]").val());
        }
    };

    this.SubmitUMNRRequestReissue = function () {
        if (shoppingWidget.UMNRTypeSelected()) {
            $.closeEzPopoups(true);
            ShowInterstitial();
            $("#ShoppingForm").submit();
        }
    };

    this.SetUMNRQuestion = function (chdCount) {
        var DIV_CHILD_QUESTION = "#divChildrenQ";
        if (chdCount > 1) {
                $(DIV_CHILD_QUESTION).html("Are these children traveling alone?");
        } else {
            $(DIV_CHILD_QUESTION).html("Is your child traveling alone?");
        }
    };

    this.UMNRTypeSelected = function () {
        var yesChecked = $("#umnrYes").get(0).checked;
        var noChecked = $("#umnrNo").get(0).checked;
        var NO_ANSWER_ERR = "#divNoUmnrAnswer";
        if (!yesChecked && !noChecked) {
            $(NO_ANSWER_ERR).show();
            return false;
        } else {
            $(NO_ANSWER_ERR).hide();
            $("input[name=UMNRAnswer]").val($("#umnrYes").get(0).checked ? "Yes" : "No");
            return true;
        }
    };

    this.ShowUMNROptions = function () {
        if (WidgetType !== shoppingEnum.WidgetType.VERTICALIFRAME) {
            $('#divUMNR').showLightBox({ width: 460 });
        }
        else {
            parent.flightWidgetAF.ShowUMNROptions();
        }
    };

    this.ResetSDTexts = function () {
        $(".selfdescribe").each(function (i) {
            if ($(this).val() == "") {
                if ($(this).attr("description") != null && $(this).attr("description") != "") {
                    $(this).val($(this).attr("description"))
                }
            }
        });
    };

    this.SubmitRequest = function () {
        $(".arrow").hide();
        //for low fare calendar, nearby airports should not be included
        //they do not get unchecked while user is entering shopping info
        if ($("#lowFareCalendar").attr("checked") == true) {
            $("input[name=IncludeNearbyDepartureAirports]", me.WidgetMainDivCssSelector).val("false");
            $("input[name=IncludeNearbyArrivalAirports]", me.WidgetMainDivCssSelector).val("false");
        }
        try {
            if (typeof (ShowInterstitial) == "function") {
                ShowInterstitial();
            }
        } catch (e) {
            //error to display insterstital should not prevent the form from getting submitted to the server
        }

        //unbind keyboard events
        $(document).unbind("keypress").unbind("keyup").unbind("keydown");
        $("#frmShopping").submit();
    };

    this.ContextKey = function () {
        var isAward = $("#awardReservation").attr("checked") == true && $("#awardOption").val().indexOf("AS50") < 0;
        return isAward ? "partner" : "codeshare";
    };

    this.ContractFareTypeChange = function () {
        var contractFareType = $("#contractFareType").val();
        if (contractFareType != null && contractFareType.toLowerCase() == "none") {
            $("#divContractFaresOption").hide();
            $("#divDiscountCode").show();
        }
        else {
            $("#divContractFaresOption").show();
            $("#lblShowOnlyContractFares").html("Show only " + $("#contractFareType option:selected").html());
            $("#divDiscountCode").hide();
        }
    };

    this.ShowAboutDC = function (discountCode) {
        $.showAboutDiscountCode($("#discountCode").val().toLowerCase() == "discount code" ? "" : $("#discountCode").val());
    };

    this.SelectCity = function (src) {

        var enteredCity = $(".DC_EnteredCity", $(src).parents(".DC_Container")).html();
        var cityName = $(".DC_CityName", $(src).parents(".DC_ThisCity")).html();
        $("#fromCity, #toCity, #fromCity1, #toCity1, #fromCity2, #toCity2, #fromCity3, #toCity3, #fromCity4, #toCity4").each(function (i) {
            if ($(this).val() == enteredCity) {
                $(this).val(cityName);
                $(this).removeClass(me.ErrorClass);
            }
        });
        var errordiv = $(src).parents("#divValidationMessage");
        $(src).parents(".DC_Container").hide();
        $(src).parents(".DC_Container_Parent").remove();
        var errordivHtml = errordiv.html();
        if (errordivHtml.length == 0) {
            errordiv.hide();    //hide empty div, otherwise the red error icon displays by itself on PB
        }
        me.RaiseParentCallback();
    };
}

//ends: shopping.js
//starts: home.js
var shoppingWidget = new ShoppingWidget();
shoppingWidget.WidgetMainDivCssSelector = "#divHome";
var homeWidgets = new HomeWidgets();
var searchDays = 332; //today+331 //361; //361 allows today + 360 days
var labelText = {
    checkInConfirmationCode: '6 letters from Alaska',
    checkInETicketNumber: '10 or 13 numbers',
    checkInMileagePlanNumber: 'Alaska Mileage Plan #'
};

$(document).ready(function () {

	$("input[type=submit]").attr({ disabled: false });
	//attach labels to textboxes
	$("#fromCity1").selfDescribeTxtBox2({ description: $("#fromCity1").val(), clearOnFocus: true, title: $("#fromCity1").attr("title") });
	$("#toCity1").selfDescribeTxtBox2({ description: $("#toCity1").val(), clearOnFocus: true, title: $("#toCity1").attr("title") });
	$("#departureDate1").selfDescribeTxtBox3({ description: "Departure Date", showCloseBtn: false });
	$("#returnDate").selfDescribeTxtBox3({ description: "Return Date", showCloseBtn: false });
	$("#departureDate1").ascalendar({ showOnFocus: true, rightPos: -6, endDateCtl: "#returnDate", daysUpperLimit: searchDays });
	$("#returnDate").ascalendar({ showOnFocus: true, rightPos: -6, startDateCtl: "#departureDate1", daysUpperLimit: searchDays });
	$("#fromCity1, #toCity1").autoCompleteCities(shoppingWidget.ContextKey, true);
	$("#checkInDestination").selfDescribeTxtBox2({ description: $("#checkInDestination").val(), clearOnFocus: false, title: $("#checkInDestination").attr("title") });
	$("#checkInDestination").autoCompleteCitiesASQXNotAllAirports(shoppingWidget.ContextKey, true);

	//Flight schedule code
	$("#Origin").selfDescribeTxtBox2({ description: $("#Origin").val(), clearOnFocus: true, title: $("#Origin").attr("title") });
	$("#Destination").selfDescribeTxtBox2({ description: $("#Destination").val(), clearOnFocus: true, title: $("#Destination").attr("title") });
	$("#DepartureDate").selfDescribeTxtBox3({ description: "mm/dd/yyyy", showCloseBtn: false });

	$('#Origin').autoCompleteCities();
	$('#Destination').autoCompleteCities();
	$('#DepartureDate').ascalendar({ showOnFocus: true });

	// Tab controls
	$('#homelet-nav a').live('click', function (e) {
		e.preventDefault();
		$('.homelet-content > .active').removeClass('active').hide();
		var href = $(this).attr('href');
		$(href).addClass('active').show();
		var $firstElement = $(href + ' select, ' + href + ' input').eq(0);
		if (!$firstElement.hasClass('no-auto-focus')) {
			$firstElement.focus();
		}

		$('#homelet-nav li').removeClass('active');
		$(this).parent().addClass('active');

		$('#lastElementClicked').val($(this).attr('id'));
	});

	$('#book-nav a').live('click', function (e) {
		e.preventDefault();
		$('.book-content > .active').removeClass('active').hide();
		var href = $(this).attr('href');
		$(href).addClass('active').show();
		$(href + ' select, ' + href + ' input').eq(0).focus();

		$('#book-nav li').removeClass('active');
		$(this).parent().addClass('active');

		$('#lastElementClicked').val($(this).attr('id'));
	});

	$('.nav-tabs a').live('click', function (e) {
		e.preventDefault();
		var name = $(this).attr('name');
		homeWidgets.trackNavClick(name);
	});

	$('#tab-hotels').live('click', function (e) {
		e.preventDefault();
		assignHotelURl();
	});
	$('#tab-cars').live('click', function (e) {
		e.preventDefault();
		assignCarURl();
	});
	$('#tab-packages').live('click', function (e) {
		e.preventDefault();
		homeWidgets.LoadVacationWidget();
	});

	$('#flightTypeOptions input').live('change', function () {
		shoppingWidget.SelectFlightType();
	});

	$('#oneWay').live('change', function () {
		if ($(this).attr('checked')) {
			$('#roundTrip').removeAttr('checked');
		} else {
			$('#roundTrip').attr('checked', 'checked');
		}
	});

	$('#_hotelIframe').load(function () {
		var doc = homeWidgets.getIframeRef('/dl/hotels/search_form');
		if (doc) {
			homeWidgets.styleHotels(doc);
		}
	});

	$('#_carIframe').load(function () {
		var doc = homeWidgets.getIframeRef('/dl/cars/search_form');
		if (doc) {
			homeWidgets.styleCars(doc);
		}
	});

	$('#divPackageType input').live('change', function () {
		if ($(this).attr('id') === 'hotelOnly') {
			$('#divRVacationMain').addClass('hotel-car');
		} else {
			$('#divRVacationMain').removeClass('hotel-car');
		}
	});



	// append modals to body for proper display
	$('#interstitial').appendTo('body');
	$('#divUMNR-container').appendTo('body');

	//force submit on enter key in IE8 - bug in IE8 where multiple forms on page using display:none can only submit first form
	if ($.browser.msie && $.browser.version === 8) {
		$("#frmViewPnr input").keydown(function (e) {
			if (e.keyCode === 13) {
				submitToViewPnr();
				return false;
			}
		});
		$("#checkinForm input").keydown(function (e) {
			if (e.keyCode === 13) {
				$("#checkinForm").submit();
				return false;
			}
		});
	}
    
	$("#checkInConfirmationCode").selfDescribeTxtBox2({ description: labelText.checkInConfirmationCode, clearOnFocus: true, title: '6 letters from Alaska'});
	$('#checkInETicketNumber').selfDescribeTxtBox2({description: labelText.checkInETicketNumber, clearOnFocus: true, title: '10 or 13 numbers'});
	$('#checkInMileagePlanNumber').selfDescribeTxtBox2({description: labelText.checkInMileagePlanNumber, clearOnFocus: true, title: 'Alaska Mileage Plan #'});
	$("#checkinOption").change(homeWidgets.ShowCheckinInputbox);
	$("#checkinForm").submit(homeWidgets.ValidateCheckinData);

	$('#txtFlightNumber').selfDescribeTxtBox2({description: 'Flight Number', clearOnFocus: true, title: 'Flight Number'});
	$('#Origin').selfDescribeTxtBox2({description: 'From', clearOnFocus: true, title: 'From'});
	$('#Destination').selfDescribeTxtBox2({description: 'To', clearOnFocus: true, title: 'To'});
	$('#DepartureDate').selfDescribeTxtBox2({description: 'Departure Date', clearOnFocus: true, title: 'Departure Date'});

	$('#txtTLastName').selfDescribeTxtBox({description: 'Traveler\'s Last Name', clearOnFocus: true});
	$('#tripModConfirmationCode').selfDescribeTxtBox({description:  'Confirmation Code/E-ticket #', clearOnFocus: true});

	$('#status').height();
	$('#manage').height(); // Just touch it. It fixes a Safari bug

	$("#frmFlightSchedule").submit(function () {
		if ($('#Origin').val() === $('#Origin').attr("title")) { $('#Origin').val(""); }
		if ($('#Destination').val() === $('#Destination').attr("title")) { $('#Destination').val(""); }
	});
});
//end document ready


function HomeWidgets() {

	this.trackNavClick = function (name) {
		if (window.s_gi) {
			var s = window.s_gi('alaskacom');
			s.linkTrackVars   = 'prop16';
			s.linkTrackEvents = 'None';
			s.prop16          = 'home-page:formlet::' + name;
			s.tl(this, 'o', 'home-page:formlet::' + name);
			s.prop16 = '';
		}
	};

	var $formElements = $('.homelet-content input[type=text], .homelet-content select');
	$formElements.live('blur', function () {
		if ($(this).val() === '') {
			$(this).removeClass('has-content');
		} else {
			$(this).addClass('has-content');
		}
	});
	$formElements.each(function (i, el) {
		var id = $(el).attr('id'),
		    val = $(el).val();
		if (val !== '' && val !== $('label[for=' + id + ']').text()) {
			$(el).addClass('has-content');
		}
	});


	this.getIframeRef = function (pathname) {
		for (var i = 0, len = window.frames.length; i < len; i++) {
			try {
				if (window.frames[i].location.pathname === pathname) {
					return window.frames[i].document;
				}
			} catch (e) {
				//
			}
		}
		return null;
	};

	// How fun is it styling the contents of an iframe?
	this.styleHotels = function (doc) {
		homeWidgets.styleCommonIframe(doc);
		homeWidgets.addCssRule(doc.styleSheets[0], '#start_location', 'width: 378px !important');
	};

	// How fun is it styling the contents of an iframe?
	this.styleCars = function (doc) {
		homeWidgets.styleCommonIframe(doc);
		$(doc).find('#end_location').closest('.column').appendTo($(doc).find('.row:eq(0)'));
		$(doc).find('#start_date').closest('.column').prependTo($(doc).find('.row:eq(1)'));
		var $div = $('<div id="times" class="row"></div>');
		$(doc).find('#StartTime').closest('.column').appendTo($div);
		$(doc).find('#EndTime').closest('.column').appendTo($div);
		$div.insertBefore($(doc).find('.button'));
		$(doc).find('select').css('width', '188px');
		$(doc).find('#start_location').prev().html('Pick-up Airport');
		$(doc).find('#end_location').prev().html('Drop-off Airport').removeClass('black');
	};

	this.styleCommonIframe = function (doc) {
		var css = doc.styleSheets[0];
		homeWidgets.addCssRule(css, 'input[type=text]', 'background-color: #fff; border: 1px solid #1d2a58; color: #1d2a58; height: 20px; line-height: 20px; padding: 3px !important; width: 180px !important;');
		homeWidgets.addCssRule(css, 'input[type=text]:hover', 'background-color: #fff; border-color: #6b6b6b; color: #4f4f4f;');
		homeWidgets.addCssRule(css, '#submit_button', 'background: none; border-radius: 0; background-color: #40639b; padding: 6px; height: auto; padding-top: 8px;');
		homeWidgets.addCssRule(css, 'body .input span', 'padding-top: 6px');
		homeWidgets.addCssRule(css, 'select', 'height: 28px; width: 70px;');
		homeWidgets.addCssRule(css, 'input.error', 'border-color: #a00 !important');
		homeWidgets.addCssRule(css, '.date_field', 'width: 188px !important;');
		homeWidgets.addCssRule(css, '.row', 'margin-bottom: 10px !important;');
		homeWidgets.addCssRule(css, '.column', 'width: 188px !important; margin-right: 10px !important;');
		homeWidgets.addCssRule(css, '#submit_button', 'width: 188px; font-size: 14px; font-weight: normal; letter-spacing: 0.5px; padding: 8px 6px 6px !important;');
		$(doc).find('.bold').hide();
		$(doc).find('#small_formlet').css({
			fontFamily: 'open-sans, sans-serif',
			fontSize: '12px',
			margin: '0',
			padding: '0'
		});

		$(doc).find('.date_field').parent().css({
			display: 'inline-block',
			width: '173px'
		});
		$(doc).find('.date_field ~ div').css({
			background: 'transparent url(https://www.alaskaair.com/content/~/media/Images/onSite/icons/calendar.ashx) 100% 50% no-repeat',
			width: '20px',
			height: '20px',
			top: '2px',
			right: '-10px'
		});

		$(doc).find('input[type=text], select').each(function (i, el) {
			if ($(el).val() !== '') {
				$(el).addClass('has-content');
			}
		});
		$(doc).find('#start_date').attr('placeholder', 'Check-in Date');
		$(doc).find('#end_date').attr('placeholder', 'Check-out Date');
	};

	this.addCssRule = function (css, sel, rule) {
		if (css.insertRule) {
			css.insertRule(sel + ' {' + rule + '}', 0);
		} else if (css.addRule) {
			css.addRule(sel, rule, 0);
		}
	};




	var cruiseWIsLoaded = false;
	this.LoadCruiseWidget = function () {
		if (!cruiseWIsLoaded) {
			$("#cruises").loadCruise(function () {
				//set focus to first input
				$("input, select", "#cruises").eq(0).focus();
				cruiseWIsLoaded = true;
			});
		}
	};

	var vacationWIsLoaded = false;
	this.LoadVacationWidget = function () {
		var city = $('#toCity1').val();
		if (!vacationWIsLoaded) {
			$('#packages').loadHVacation(function () {
				vacationWIsLoaded = true;
				//set focus to first input
				$('input, select', '#packages').eq(0).focus();

				$('#divBook8orMore').appendTo('body');
			}, city);
		}
	};

	this.ShowCheckinInputbox = function () {
		$('#sectionConfirmationCode, #sectionETicketNumber, #sectionMileagePlanNumber').hide();
		var selectedSectionId = '';
		switch ($('#checkinOption').val()) {
			case '2':
				selectedSectionId = 'sectionConfirmationCode';
				break;
			case '3':
				selectedSectionId = 'sectionETicketNumber';
				break;
			case '4':
				selectedSectionId = 'sectionMileagePlanNumber';
				break;
		}

		if (selectedSectionId !== '') {
			var selectedSection = $('#' + selectedSectionId);
			selectedSection.show();
		}
	};

	this.ShowTripModInputbox = function () {
		$("#sectionTripModConfirmationCode, #sectionTripModETicketNumber, #sectionTripModMileagePlanNumber, #tripmodForm #btnViewPnrSubmit").hide();
		var selectedSectionId = "";
		switch ($("#tripModOption").val()) {
			case "Confirmation Code":
				selectedSectionId = "sectionTripModConfirmationCode";
				break;
			case "E-ticket Number":
				selectedSectionId = "sectionTripModETicketNumber";
				break;
		}

		if (selectedSectionId !== "") {
			var selectedSection = $("#" + selectedSectionId);
			selectedSection.show();
			$("#divViewPnr #btnViewPnrSubmit").show();
			$("input:eq(0)", selectedSection).focus();
		}
	};

	this.ValidateCheckinData = function () {
	    $("#checkinErrors1, #checkinErrors2").empty();

	    var inputErrorClass = "input-validation-error";
	    var requiredText = "The following fields are required: ";
	    var invalidCityText = " is an invalid city or is unavailable through this Web site. Check the spelling or try entering a different city.";
	    var matchText = "";
	    var errorCount = 0;
	    var reErrorCount = 0;

	    var reclocRegex = /^[A-Za-z]{6}$/;
	    var mpNumRegex = /^[0-9]{1,12}$/;
	    var ticketNumRegex = /^[0-9]{10}$|^[0-9]{13}$/;

	    function addComma() {
	        if (errorCount > 0) {
	            requiredText += ", ";
	        }
	    }

	    function addErrorClass() { $("#checkinOption").addClass(inputErrorClass); }

	    function checkDest() {


	        if ($("#checkInDestination").val().trim() == '' || $("#checkInDestination").val().trim() == $("#checkInDestination").attr('title')) {
	            errorCount++;
	            requiredText += $("#checkInDestination").attr('title');
	            $("#checkInDestination").addClass(inputErrorClass);
	        }
	        else if ($("#checkinorigin").val() == '') {
	            errorCount++;
	            requiredText = $("#checkInDestination").val() + invalidCityText;
	            $("#checkInDestination").addClass(inputErrorClass);
	        }
	        else {
	            $("#checkInDestination").removeClass(inputErrorClass);
	        }
	    }

	    function checkValues(controlID, regex, requiredMsg, regexMsg) {
	        if ($(controlID).val() == "") {
	            $(controlID).addClass(inputErrorClass);
	            addErrorClass();
	            addComma();
	            errorCount++;
	            requiredText += requiredMsg;
	        } else if (!regex.test($(controlID).val())) {
	            reErrorCount++;
	            addErrorClass();
	            $(controlID).addClass(inputErrorClass);
	            matchText = regexMsg;
	        }
	    }

	    function clearValuesHelper(controlName) {
	        var controlId = '#' + controlName;
	            $(controlId).val('');
	            return false;
	    }


	    checkDest();

	    switch ($("#checkinOption").val()) {
	        case "2":
	            checkValues("#checkInConfirmationCode", reclocRegex, "Confirmation Code", "The confirmation code is invalid. Only Alaska-issued 6-letter confirmation codes can be accepted.");
	            break;
	        case "3":
	            checkValues("#checkInETicketNumber", ticketNumRegex, "E-Ticket Number", "The e-ticket number entered is invalid. E-ticket numbers consist of 10 or 13 numeric characters.");
	            break;
	        case "4":
	            checkValues("#checkInMileagePlanNumber", mpNumRegex, "Mileage Plan&trade; Number", "Please enter your Alaska Airlines Mileage Plan&trade; number or select another option to look up your reservation.");
	            break;
	        default:
                checkValues("#checkinOption", "", "Look Up Reservation", "");
                break;
	    }

	    if (errorCount + reErrorCount > 0) {
	        if (errorCount > 0) {
	            $("#checkinErrors1").html(requiredText);
	        }
	        if (reErrorCount > 0) {
	            $("#checkinErrors2").html(matchText);
	        }
	        $("#divCheckin").focus();
	        return false;
	    }

	    switch ($("#checkinOption").val()) {
	        case "2":
	            clearValuesHelper('checkInETicketNumber');
	            clearValuesHelper('checkInMileagePlanNumber');
	            break;
	        case "3":
	            clearValuesHelper('checkInConfirmationCode');
	            clearValuesHelper('checkInMileagePlanNumber');
	            break;
	        case "4":
	            clearValuesHelper('checkInConfirmationCode');
	            clearValuesHelper('checkInETicketNumber');
	            break;
	    }

	    return true;
	};

	this.SubmitFlightStatusOrSchedule = function () {
		if ($('#txtFlightNumber').val() !== '' && $('#txtFlightNumber').val() !== 'Flight Number') {
			$('#frmFlightStatus').submit();
		} else {
			$('#frmFlightSchedule').submit();
		}
	};

	this.SetFocusTo = function (cssSelector) {
		try {
			$(cssSelector).focus();
		}
		catch (e) {
		}
	};

}

//ends: home.js
//starts: sitenewsletter.js
function SiteNewsletter() {
    var _me = this;
    var _emailDefaultVal = "Email Address";
    var _newsletterId = "#newsletter_form";
    var _emailAddressId = "#newsletter_emailAddress";
    var _submitId = "#newsletter_submit";
    var _errorMessageId = "#newsletter_errorMessage";
    var _confirmMessageId = "#newsletter_confirmMessage";
    var _campaignId = "#newsletter_campaign";

    this.Init = function () {
        if (!$(_newsletterId)) return;   // if newsletter widget doesn't exist, skip 
        
        if ($(_emailAddressId).val() == "") {
            $(_emailAddressId).val(_emailDefaultVal);
        }
        $(_emailAddressId).bind('focus', function () {
            if ($(this).val().toLowerCase() == _emailDefaultVal.toLowerCase()) { $(this).val(""); }
        });
        $(_emailAddressId).bind('blur', function () {
            if ($(this).val() == "") { $(this).val(_emailDefaultVal); }
        });


        $(_submitId).click(function () { _me.SignUp(); });
        $(_emailAddressId).keypress(function (e) {
            var code = e.which ? e.which : e.keyCode;
            if (code == 13) {
                _me.SignUp();
            }
        });
    };

    this.SignUp = function (on_error) {
        if ($(_emailAddressId).val().toLowerCase() == _emailDefaultVal.toLowerCase()) { $(_emailAddressId).val(""); }
        var request = { EmailAddress: $(_emailAddressId).val(), Campaign: $(_campaignId).val() };
        var submitUrl = "/widgets/newsletter/ajaxsignup";

        //DO ajax call to validate and sign up the email address
        $.ajax({
            type: "POST",
            url: submitUrl,
            dataType: "json",
            data: request,
            complete: function (response, textStatus) {
                var msg = response.responseText;
                if (msg == null || msg == "") {
                    _showErrors("");
                    return;
                }
                if (msg == "OK") {
                    _showSuccess();
                }
                else {
                    _showErrors(msg);
                }
            }
        });
    };

    function _showErrors(errorMessage) {
        $(_emailAddressId).addClass("input-validation-error");
        if ($(_errorMessageId))
            $(_errorMessageId).html("<span class='field-validation-error'>" + errorMessage + "</span>");
    };

    function _showSuccess() {
        $(_emailAddressId).removeClass("input-validation-error");
        if ($(_errorMessageId))
            $(_errorMessageId).html("");
        $(_newsletterId).hide();
        if ($(_confirmMessageId))
            $(_confirmMessageId).show();
    }
};

var siteNewsletter = new SiteNewsletter();

$(document).ready(function () {
    siteNewsletter.Init();
});    

//ends: sitenewsletter.js
//starts: cookie.js
function deleteCookieJS(strName, strDomain, strPath)
{ var dtmExpiry = new Date("January 1, 1970"); document.cookie = strName + "=" + ((strPath) ? "; path=" + strPath : "") + ((strDomain) ? "; domain=" + strDomain : "") + "; expires=" + dtmExpiry.toGMTString(); }


function Get_Cookie(check_name) {
    // first we'll split this cookie up into name/value pairs
    // note: document.cookie only returns name=value, not the other components
    var a_all_cookies = document.cookie.split(';');
    var a_temp_cookie = '';
    var cookie_name = '';
    var cookie_value = '';
    var b_cookie_found = false; // set boolean t/f default f

    for (i = 0; i < a_all_cookies.length; i++) {
        // now we'll split apart each name=value pair
        a_temp_cookie = a_all_cookies[i].split('=');


        // and trim left/right whitespace while we're at it
        cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

        // if the extracted name matches passed check_name
        if (cookie_name == check_name) {
            b_cookie_found = true;
            // we need to handle case where cookie has no value but exists (no = sign, that is):
            if (a_temp_cookie.length > 1) {
                cookie_value = unescape(a_temp_cookie[1].replace(/^\s+|\s+$/g, ''));
            }
            // note that in cases where cookie is initialized but no value, null is returned
            return cookie_value;
            break;
        }
        a_temp_cookie = null;
        cookie_name = '';
    }
    if (!b_cookie_found) {
        return null;
    }
}



//ends: cookie.js
