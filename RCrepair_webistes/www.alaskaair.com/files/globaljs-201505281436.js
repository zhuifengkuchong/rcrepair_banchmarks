//version [201505270728] cached at [5/28/2015 3:31:03 PM]
//combined files: AppInsights.js require.js require.config.js windowLocation.svc.js pixelCaller.js yepnope.js modernizr.js FrameworkUtils.js Promise.js tagUtil.js PixelBootstrap.js BasePerPagePixel.js AdaraPixel.js AdaraPixelInitializer.js json2Loader.js preventXSF.js asglobal.js topNav.js authentication.js visitor.js testUtil.js maxymiserRepository.js asdropdownnav.js lightbox.js loadwidgets.js bubblenew.js adChoicesLoader.js cake.js superPixel.js wdcw.js spanish.js adready.js intentMediaPixel.js bing.js kenshoo.js sojernPixel.js foresee.js facebooklike.js deferredLoader.js responsysTWRS.js googleanalytics.js omnitureUtils.js PreventDoubleClicks.js 
//starts: AppInsights.js
var appInsights = window.appInsights || function (config) {
    function s(config) { t[config] = function () { var i = arguments; t.queue.push(function () { t[config].apply(t, i) }) } } var t = { config: config }, r = document, f = window, e = "script", o = r.createElement(e), i, u; for (o.src = config.url || "../../az416426.vo.msecnd.net/scripts/a/ai.0.js"/*tpa=http://az416426.vo.msecnd.net/scripts/a/ai.0.js*/, r.getElementsByTagName(e)[0].parentNode.appendChild(o), t.cookie = r.cookie, t.queue = [], i = ["Event", "Exception", "Metric", "PageView", "Trace"]; i.length;) s("track" + i.pop()); return config.disableExceptionTracking || (i = "onerror", s("_" + i), u = f[i], f[i] = function (config, r, f, e, o) { var s = u && u(config, r, f, e, o); return s !== !0 && t["_" + i](config, r, f, e, o), s }), t
}({
    instrumentationKey: as.AppInsightsKey
});

window.appInsights = appInsights;
appInsights.trackPageView();
//ends: AppInsights.js
//starts: require.js
/** vim: et:ts=4:sw=4:sts=4
* @license RequireJS 2.1.15 Copyright (c) 2010-2014, The Dojo Foundation All Rights Reserved.
* Available via the MIT or new BSD license.
* see: http://github.com/jrburke/requirejs for details
*/
//Not using strict: uneven strict support in browsers, #392, and causes
//problems with requirejs.exec()/transpiler plugins that may not be strict.
/*jslint regexp: true, nomen: true, sloppy: true */
/*global window, navigator, document, importScripts, setTimeout, opera */

var requirejs, require, define;
(function (global) {
    var req, s, head, baseElement, dataMain, src,
        interactiveScript, currentlyAddingScript, mainScript, subPath,
        version = 'http://www.alaskaair.com/files/2.1.15',
        commentRegExp = /(\/\*([\s\S]*?)\*\/|([^:]|^)\/\/(.*)$)/mg,
        cjsRequireRegExp = /[^.]\s*require\s*\(\s*["']([^'"\s]+)["']\s*\)/g,
        jsSuffixRegExp = /\.js$/,
        currDirRegExp = /^\.\//,
        op = Object.prototype,
        ostring = op.toString,
        hasOwn = op.hasOwnProperty,
        ap = Array.prototype,
        apsp = ap.splice,
        isBrowser = !!(typeof window !== 'undefined' && typeof navigator !== 'undefined' && window.document),
        isWebWorker = !isBrowser && typeof importScripts !== 'undefined',
    //PS3 indicates loaded and complete, but need to wait for complete
    //specifically. Sequence is 'loading', 'loaded', execution,
    // then 'complete'. The UA check is unfortunate, but not sure how
    //to feature test w/o causing perf issues.
        readyRegExp = isBrowser && navigator.platform === 'PLAYSTATION 3' ?
                      /^complete$/ : /^(complete|loaded)$/,
        defContextName = '_',
    //Oh the tragedy, detecting opera. See the usage of isOpera for reason.
        isOpera = typeof opera !== 'undefined' && opera.toString() === '[object Opera]',
        contexts = {},
        cfg = {},
        globalDefQueue = [],
        useInteractive = false;

    function isFunction(it) {
        return ostring.call(it) === '[object Function]';
    }

    function isArray(it) {
        return ostring.call(it) === '[object Array]';
    }

    /**
    * Helper function for iterating over an array. If the func returns
    * a true value, it will break out of the loop.
    */
    function each(ary, func) {
        if (ary) {
            var i;
            for (i = 0; i < ary.length; i += 1) {
                if (ary[i] && func(ary[i], i, ary)) {
                    break;
                }
            }
        }
    }

    /**
    * Helper function for iterating over an array backwards. If the func
    * returns a true value, it will break out of the loop.
    */
    function eachReverse(ary, func) {
        if (ary) {
            var i;
            for (i = ary.length - 1; i > -1; i -= 1) {
                if (ary[i] && func(ary[i], i, ary)) {
                    break;
                }
            }
        }
    }

    function hasProp(obj, prop) {
        return hasOwn.call(obj, prop);
    }

    function getOwn(obj, prop) {
        return hasProp(obj, prop) && obj[prop];
    }

    /**
    * Cycles over properties in an object and calls a function for each
    * property value. If the function returns a truthy value, then the
    * iteration is stopped.
    */
    function eachProp(obj, func) {
        var prop;
        for (prop in obj) {
            if (hasProp(obj, prop)) {
                if (func(obj[prop], prop)) {
                    break;
                }
            }
        }
    }

    /**
    * Simple function to mix in properties from source into target,
    * but only if target does not already have a property of the same name.
    */
    function mixin(target, source, force, deepStringMixin) {
        if (source) {
            eachProp(source, function (value, prop) {
                if (force || !hasProp(target, prop)) {
                    if (deepStringMixin && typeof value === 'object' && value &&
                        !isArray(value) && !isFunction(value) &&
                        !(value instanceof RegExp)) {

                        if (!target[prop]) {
                            target[prop] = {};
                        }
                        mixin(target[prop], value, force, deepStringMixin);
                    } else {
                        target[prop] = value;
                    }
                }
            });
        }
        return target;
    }

    //Similar to Function.prototype.bind, but the 'this' object is specified
    //first, since it is easier to read/figure out what 'this' will be.
    function bind(obj, fn) {
        return function () {
            return fn.apply(obj, arguments);
        };
    }

    function scripts() {
        return document.getElementsByTagName('script');
    }

    function defaultOnError(err) {
        throw err;
    }

    //Allow getting a global that is expressed in
    //dot notation, like 'a.b.c'.
    function getGlobal(value) {
        if (!value) {
            return value;
        }
        var g = global;
        each(value.split('.'), function (part) {
            g = g[part];
        });
        return g;
    }

    /**
    * Constructs an error with a pointer to an URL with more information.
    * @param {String} id the error ID that maps to an ID on a web page.
    * @param {String} message human readable error.
    * @param {Error} [err] the original error, if there is one.
    *
    * @returns {Error}
    */
    function makeError(id, msg, err, requireModules) {
        var e = new Error(msg + '\nhttp://requirejs.org/docs/errors.html#' + id);
        e.requireType = id;
        e.requireModules = requireModules;
        if (err) {
            e.originalError = err;
        }
        return e;
    }

    if (typeof define !== 'undefined') {
        //If a define is already in play via another AMD loader,
        //do not overwrite.
        return;
    }

    if (typeof requirejs !== 'undefined') {
        if (isFunction(requirejs)) {
            //Do not overwrite an existing requirejs instance.
            return;
        }
        cfg = requirejs;
        requirejs = undefined;
    }

    //Allow for a require config object
    if (typeof require !== 'undefined' && !isFunction(require)) {
        //assume it is a config object.
        cfg = require;
        require = undefined;
    }

    function newContext(contextName) {
        var inCheckLoaded, Module, context, handlers,
            checkLoadedTimeoutId,
            config = {
                //Defaults. Do not set a default for map
                //config to speed up normalize(), which
                //will run faster if there is no default.
                waitSeconds: 7,
                baseUrl: './',
                paths: {},
                bundles: {},
                pkgs: {},
                shim: {},
                config: {}
            },
            registry = {},
        //registry of just enabled modules, to speed
        //cycle breaking code when lots of modules
        //are registered, but not activated.
            enabledRegistry = {},
            undefEvents = {},
            defQueue = [],
            defined = {},
            urlFetched = {},
            bundlesMap = {},
            requireCounter = 1,
            unnormalizedCounter = 1;

        /**
        * Trims the . and .. from an array of path segments.
        * It will keep a leading path segment if a .. will become
        * the first path segment, to help with module name lookups,
        * which act like paths, but can be remapped. But the end result,
        * all paths that use this function should look normalized.
        * NOTE: this method MODIFIES the input array.
        * @param {Array} ary the array of path segments.
        */
        function trimDots(ary) {
            var i, part;
            for (i = 0; i < ary.length; i++) {
                part = ary[i];
                if (part === '.') {
                    ary.splice(i, 1);
                    i -= 1;
                } else if (part === '..') {
                    // If at the start, or previous value is still ..,
                    // keep them so that when converted to a path it may
                    // still work when converted to a path, even though
                    // as an ID it is less than ideal. In larger point
                    // releases, may be better to just kick out an error.
                    if (i === 0 || (i == 1 && ary[2] === '..') || ary[i - 1] === '..') {
                        continue;
                    } else if (i > 0) {
                        ary.splice(i - 1, 2);
                        i -= 2;
                    }
                }
            }
        }

        /**
        * Given a relative module name, like ./something, normalize it to
        * a real name that can be mapped to a path.
        * @param {String} name the relative name
        * @param {String} baseName a real name that the name arg is relative
        * to.
        * @param {Boolean} applyMap apply the map config to the value. Should
        * only be done if this normalization is for a dependency ID.
        * @returns {String} normalized name
        */
        function normalize(name, baseName, applyMap) {
            var pkgMain, mapValue, nameParts, i, j, nameSegment, lastIndex,
                foundMap, foundI, foundStarMap, starI, normalizedBaseParts,
                baseParts = (baseName && baseName.split('/')),
                map = config.map,
                starMap = map && map['*'];

            //Adjust any relative paths.
            if (name) {
                name = name.split('/');
                lastIndex = name.length - 1;

                // If wanting node ID compatibility, strip .js from end
                // of IDs. Have to do this here, and not in nameToUrl
                // because node allows either .js or non .js to map
                // to same file.
                if (config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {
                    name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, '');
                }

                // Starts with a '.' so need the baseName
                if (name[0].charAt(0) === '.' && baseParts) {
                    //Convert baseName to array, and lop off the last part,
                    //so that . matches that 'directory' and not name of the baseName's
                    //module. For instance, baseName of 'one/two/three', maps to
                    //'Unknown_83_filename'/*tpa=http://www.alaskaair.com/files/one/two/three.js*/, but we want the directory, 'one/two' for
                    //this normalization.
                    normalizedBaseParts = baseParts.slice(0, baseParts.length - 1);
                    name = normalizedBaseParts.concat(name);
                }

                trimDots(name);
                name = name.join('/');
            }

            //Apply map config if available.
            if (applyMap && map && (baseParts || starMap)) {
                nameParts = name.split('/');

                outerLoop: for (i = nameParts.length; i > 0; i -= 1) {
                    nameSegment = nameParts.slice(0, i).join('/');

                    if (baseParts) {
                        //Find the longest baseName segment match in the config.
                        //So, do joins on the biggest to smallest lengths of baseParts.
                        for (j = baseParts.length; j > 0; j -= 1) {
                            mapValue = getOwn(map, baseParts.slice(0, j).join('/'));

                            //baseName segment has config, find if it has one for
                            //this name.
                            if (mapValue) {
                                mapValue = getOwn(mapValue, nameSegment);
                                if (mapValue) {
                                    //Match, update name to the new value.
                                    foundMap = mapValue;
                                    foundI = i;
                                    break outerLoop;
                                }
                            }
                        }
                    }

                    //Check for a star map match, but just hold on to it,
                    //if there is a shorter segment match later in a matching
                    //config, then favor over this star map.
                    if (!foundStarMap && starMap && getOwn(starMap, nameSegment)) {
                        foundStarMap = getOwn(starMap, nameSegment);
                        starI = i;
                    }
                }

                if (!foundMap && foundStarMap) {
                    foundMap = foundStarMap;
                    foundI = starI;
                }

                if (foundMap) {
                    nameParts.splice(0, foundI, foundMap);
                    name = nameParts.join('/');
                }
            }

            // If the name points to a package's name, use
            // the package main instead.
            pkgMain = getOwn(config.pkgs, name);

            return pkgMain ? pkgMain : name;
        }

        function removeScript(name) {
            if (isBrowser) {
                each(scripts(), function (scriptNode) {
                    if (scriptNode.getAttribute('data-requiremodule') === name &&
                            scriptNode.getAttribute('data-requirecontext') === context.contextName) {
                        scriptNode.parentNode.removeChild(scriptNode);
                        return true;
                    }
                });
            }
        }

        function hasPathFallback(id) {
            var pathConfig = getOwn(config.paths, id);
            if (pathConfig && isArray(pathConfig) && pathConfig.length > 1) {
                //Pop off the first array value, since it failed, and
                //retry
                pathConfig.shift();
                context.require.undef(id);

                //Custom require that does not do map translation, since
                //ID is "absolute", already mapped/resolved.
                context.makeRequire(null, {
                    skipMap: true
                })([id]);

                return true;
            }
        }

        //Turns a plugin!resource to [plugin, resource]
        //with the plugin being undefined if the name
        //did not have a plugin prefix.
        function splitPrefix(name) {
            var prefix,
                index = name ? name.indexOf('!') : -1;
            if (index > -1) {
                prefix = name.substring(0, index);
                name = name.substring(index + 1, name.length);
            }
            return [prefix, name];
        }

        /**
        * Creates a module mapping that includes plugin prefix, module
        * name, and path. If parentModuleMap is provided it will
        * also normalize the name via require.normalize()
        *
        * @param {String} name the module name
        * @param {String} [parentModuleMap] parent module map
        * for the module name, used to resolve relative names.
        * @param {Boolean} isNormalized: is the ID already normalized.
        * This is true if this call is done for a define() module ID.
        * @param {Boolean} applyMap: apply the map config to the ID.
        * Should only be true if this map is for a dependency.
        *
        * @returns {Object}
        */
        function makeModuleMap(name, parentModuleMap, isNormalized, applyMap) {
            var url, pluginModule, suffix, nameParts,
                prefix = null,
                parentName = parentModuleMap ? parentModuleMap.name : null,
                originalName = name,
                isDefine = true,
                normalizedName = '';

            //If no name, then it means it is a require call, generate an
            //internal name.
            if (!name) {
                isDefine = false;
                name = '_@r' + (requireCounter += 1);
            }

            nameParts = splitPrefix(name);
            prefix = nameParts[0];
            name = nameParts[1];

            if (prefix) {
                prefix = normalize(prefix, parentName, applyMap);
                pluginModule = getOwn(defined, prefix);
            }

            //Account for relative paths if there is a base name.
            if (name) {
                if (prefix) {
                    if (pluginModule && pluginModule.normalize) {
                        //Plugin is loaded, use its normalize method.
                        normalizedName = pluginModule.normalize(name, function (name) {
                            return normalize(name, parentName, applyMap);
                        });
                    } else {
                        // If nested plugin references, then do not try to
                        // normalize, as it will not normalize correctly. This
                        // places a restriction on resourceIds, and the longer
                        // term solution is not to normalize until plugins are
                        // loaded and all normalizations to allow for async
                        // loading of a loader plugin. But for now, fixes the
                        // common uses. Details in #1131
                        normalizedName = name.indexOf('!') === -1 ?
                                         normalize(name, parentName, applyMap) :
                                         name;
                    }
                } else {
                    //A regular module.
                    normalizedName = normalize(name, parentName, applyMap);

                    //Normalized name may be a plugin ID due to map config
                    //application in normalize. The map config values must
                    //already be normalized, so do not need to redo that part.
                    nameParts = splitPrefix(normalizedName);
                    prefix = nameParts[0];
                    normalizedName = nameParts[1];
                    isNormalized = true;

                    url = context.nameToUrl(normalizedName);
                }
            }

            //If the id is a plugin id that cannot be determined if it needs
            //normalization, stamp it with a unique ID so two matching relative
            //ids that may conflict can be separate.
            suffix = prefix && !pluginModule && !isNormalized ?
                     '_unnormalized' + (unnormalizedCounter += 1) :
                     '';

            return {
                prefix: prefix,
                name: normalizedName,
                parentMap: parentModuleMap,
                unnormalized: !!suffix,
                url: url,
                originalName: originalName,
                isDefine: isDefine,
                id: (prefix ?
                        prefix + '!' + normalizedName :
                        normalizedName) + suffix
            };
        }

        function getModule(depMap) {
            var id = depMap.id,
                mod = getOwn(registry, id);

            if (!mod) {
                mod = registry[id] = new context.Module(depMap);
            }

            return mod;
        }

        function on(depMap, name, fn) {
            var id = depMap.id,
                mod = getOwn(registry, id);

            if (hasProp(defined, id) &&
                    (!mod || mod.defineEmitComplete)) {
                if (name === 'defined') {
                    fn(defined[id]);
                }
            } else {
                mod = getModule(depMap);
                if (mod.error && name === 'error') {
                    fn(mod.error);
                } else {
                    mod.on(name, fn);
                }
            }
        }

        function onError(err, errback) {
            var ids = err.requireModules,
                notified = false;

            if (errback) {
                errback(err);
            } else {
                each(ids, function (id) {
                    var mod = getOwn(registry, id);
                    if (mod) {
                        //Set error on module, so it skips timeout checks.
                        mod.error = err;
                        if (mod.events.error) {
                            notified = true;
                            mod.emit('error', err);
                        }
                    }
                });

                if (!notified) {
                    req.onError(err);
                }
            }
        }

        /**
        * Internal method to transfer globalQueue items to this context's
        * defQueue.
        */
        function takeGlobalQueue() {
            //Push all the globalDefQueue items into the context's defQueue
            if (globalDefQueue.length) {
                //Array splice in the values since the context code has a
                //local var ref to defQueue, so cannot just reassign the one
                //on context.
                apsp.apply(defQueue,
                           [defQueue.length, 0].concat(globalDefQueue));
                globalDefQueue = [];
            }
        }

        handlers = {
            'require': function (mod) {
                if (mod.require) {
                    return mod.require;
                } else {
                    return (mod.require = context.makeRequire(mod.map));
                }
            },
            'exports': function (mod) {
                mod.usingExports = true;
                if (mod.map.isDefine) {
                    if (mod.exports) {
                        return (defined[mod.map.id] = mod.exports);
                    } else {
                        return (mod.exports = defined[mod.map.id] = {});
                    }
                }
            },
            'module': function (mod) {
                if (mod.module) {
                    return mod.module;
                } else {
                    return (mod.module = {
                        id: mod.map.id,
                        uri: mod.map.url,
                        config: function () {
                            return getOwn(config.config, mod.map.id) || {};
                        },
                        exports: mod.exports || (mod.exports = {})
                    });
                }
            }
        };

        function cleanRegistry(id) {
            //Clean up machinery used for waiting modules.
            delete registry[id];
            delete enabledRegistry[id];
        }

        function breakCycle(mod, traced, processed) {
            var id = mod.map.id;

            if (mod.error) {
                mod.emit('error', mod.error);
            } else {
                traced[id] = true;
                each(mod.depMaps, function (depMap, i) {
                    var depId = depMap.id,
                        dep = getOwn(registry, depId);

                    //Only force things that have not completed
                    //being defined, so still in the registry,
                    //and only if it has not been matched up
                    //in the module already.
                    if (dep && !mod.depMatched[i] && !processed[depId]) {
                        if (getOwn(traced, depId)) {
                            mod.defineDep(i, defined[depId]);
                            mod.check(); //pass false?
                        } else {
                            breakCycle(dep, traced, processed);
                        }
                    }
                });
                processed[id] = true;
            }
        }

        function checkLoaded() {
            var err, usingPathFallback,
                waitInterval = config.waitSeconds * 1000,
            //It is possible to disable the wait interval by using waitSeconds of 0.
                expired = waitInterval && (context.startTime + waitInterval) < new Date().getTime(),
                noLoads = [],
                reqCalls = [],
                stillLoading = false,
                needCycleCheck = true;

            //Do not bother if this call was a result of a cycle break.
            if (inCheckLoaded) {
                return;
            }

            inCheckLoaded = true;

            //Figure out the state of all the modules.
            eachProp(enabledRegistry, function (mod) {
                var map = mod.map,
                    modId = map.id;

                //Skip things that are not enabled or in error state.
                if (!mod.enabled) {
                    return;
                }

                if (!map.isDefine) {
                    reqCalls.push(mod);
                }

                if (!mod.error) {
                    //If the module should be executed, and it has not
                    //been inited and time is up, remember it.
                    if (!mod.inited && expired) {
                        if (hasPathFallback(modId)) {
                            usingPathFallback = true;
                            stillLoading = true;
                        } else {
                            noLoads.push(modId);
                            removeScript(modId);
                        }
                    } else if (!mod.inited && mod.fetched && map.isDefine) {
                        stillLoading = true;
                        if (!map.prefix) {
                            //No reason to keep looking for unfinished
                            //loading. If the only stillLoading is a
                            //plugin resource though, keep going,
                            //because it may be that a plugin resource
                            //is waiting on a non-plugin cycle.
                            return (needCycleCheck = false);
                        }
                    }
                }
            });

            if (expired && noLoads.length) {
                //If wait time expired, throw error of unloaded modules.
                err = makeError('timeout', 'Load timeout for modules: ' + noLoads, null, noLoads);
                err.contextName = context.contextName;
                return onError(err);
            }

            //Not expired, check for a cycle.
            if (needCycleCheck) {
                each(reqCalls, function (mod) {
                    breakCycle(mod, {}, {});
                });
            }

            //If still waiting on loads, and the waiting load is something
            //other than a plugin resource, or there are still outstanding
            //scripts, then just try back later.
            if ((!expired || usingPathFallback) && stillLoading) {
                //Something is still waiting to load. Wait for it, but only
                //if a timeout is not already in effect.
                if ((isBrowser || isWebWorker) && !checkLoadedTimeoutId) {
                    checkLoadedTimeoutId = setTimeout(function () {
                        checkLoadedTimeoutId = 0;
                        checkLoaded();
                    }, 50);
                }
            }

            inCheckLoaded = false;
        }

        Module = function (map) {
            this.events = getOwn(undefEvents, map.id) || {};
            this.map = map;
            this.shim = getOwn(config.shim, map.id);
            this.depExports = [];
            this.depMaps = [];
            this.depMatched = [];
            this.pluginMaps = {};
            this.depCount = 0;

            /* this.exports this.factory
            this.depMaps = [],
            this.enabled, this.fetched
            */
        };

        Module.prototype = {
            init: function (depMaps, factory, errback, options) {
                options = options || {};

                //Do not do more inits if already done. Can happen if there
                //are multiple define calls for the same module. That is not
                //a normal, common case, but it is also not unexpected.
                if (this.inited) {
                    return;
                }

                this.factory = factory;

                if (errback) {
                    //Register for errors on this module.
                    this.on('error', errback);
                } else if (this.events.error) {
                    //If no errback already, but there are error listeners
                    //on this module, set up an errback to pass to the deps.
                    errback = bind(this, function (err) {
                        this.emit('error', err);
                    });
                }

                //Do a copy of the dependency array, so that
                //source inputs are not modified. For example
                //"shim" deps are passed in here directly, and
                //doing a direct modification of the depMaps array
                //would affect that config.
                this.depMaps = depMaps && depMaps.slice(0);

                this.errback = errback;

                //Indicate this module has be initialized
                this.inited = true;

                this.ignore = options.ignore;

                //Could have option to init this module in enabled mode,
                //or could have been previously marked as enabled. However,
                //the dependencies are not known until init is called. So
                //if enabled previously, now trigger dependencies as enabled.
                if (options.enabled || this.enabled) {
                    //Enable this module and dependencies.
                    //Will call this.check()
                    this.enable();
                } else {
                    this.check();
                }
            },

            defineDep: function (i, depExports) {
                //Because of cycles, defined callback for a given
                //export can be called more than once.
                if (!this.depMatched[i]) {
                    this.depMatched[i] = true;
                    this.depCount -= 1;
                    this.depExports[i] = depExports;
                }
            },

            fetch: function () {
                if (this.fetched) {
                    return;
                }
                this.fetched = true;

                context.startTime = (new Date()).getTime();

                var map = this.map;

                //If the manager is for a plugin managed resource,
                //ask the plugin to load it now.
                if (this.shim) {
                    context.makeRequire(this.map, {
                        enableBuildCallback: true
                    })(this.shim.deps || [], bind(this, function () {
                        return map.prefix ? this.callPlugin() : this.load();
                    }));
                } else {
                    //Regular dependency.
                    return map.prefix ? this.callPlugin() : this.load();
                }
            },

            load: function () {
                var url = this.map.url;

                //Regular dependency.
                if (!urlFetched[url]) {
                    urlFetched[url] = true;
                    context.load(this.map.id, url);
                }
            },

            /**
            * Checks if the module is ready to define itself, and if so,
            * define it.
            */
            check: function () {
                if (!this.enabled || this.enabling) {
                    return;
                }

                var err, cjsModule,
                    id = this.map.id,
                    depExports = this.depExports,
                    exports = this.exports,
                    factory = this.factory;

                if (!this.inited) {
                    this.fetch();
                } else if (this.error) {
                    this.emit('error', this.error);
                } else if (!this.defining) {
                    //The factory could trigger another require call
                    //that would result in checking this module to
                    //define itself again. If already in the process
                    //of doing that, skip this work.
                    this.defining = true;

                    if (this.depCount < 1 && !this.defined) {
                        if (isFunction(factory)) {
                            //If there is an error listener, favor passing
                            //to that instead of throwing an error. However,
                            //only do it for define()'d  modules. require
                            //errbacks should not be called for failures in
                            //their callbacks (#699). However if a global
                            //onError is set, use that.
                            if ((this.events.error && this.map.isDefine) ||
                                req.onError !== defaultOnError) {
                                try {
                                    exports = context.execCb(id, factory, depExports, exports);
                                } catch (e) {
                                    err = e;
                                }
                            } else {
                                exports = context.execCb(id, factory, depExports, exports);
                            }

                            // Favor return value over exports. If node/cjs in play,
                            // then will not have a return value anyway. Favor
                            // module.exports assignment over exports object.
                            if (this.map.isDefine && exports === undefined) {
                                cjsModule = this.module;
                                if (cjsModule) {
                                    exports = cjsModule.exports;
                                } else if (this.usingExports) {
                                    //exports already set the defined value.
                                    exports = this.exports;
                                }
                            }

                            if (err) {
                                err.requireMap = this.map;
                                err.requireModules = this.map.isDefine ? [this.map.id] : null;
                                err.requireType = this.map.isDefine ? 'define' : 'require';
                                return onError((this.error = err));
                            }

                        } else {
                            //Just a literal value
                            exports = factory;
                        }

                        this.exports = exports;

                        if (this.map.isDefine && !this.ignore) {
                            defined[id] = exports;

                            if (req.onResourceLoad) {
                                req.onResourceLoad(context, this.map, this.depMaps);
                            }
                        }

                        //Clean up
                        cleanRegistry(id);

                        this.defined = true;
                    }

                    //Finished the define stage. Allow calling check again
                    //to allow define notifications below in the case of a
                    //cycle.
                    this.defining = false;

                    if (this.defined && !this.defineEmitted) {
                        this.defineEmitted = true;
                        this.emit('defined', this.exports);
                        this.defineEmitComplete = true;
                    }

                }
            },

            callPlugin: function () {
                var map = this.map,
                    id = map.id,
                //Map already normalized the prefix.
                    pluginMap = makeModuleMap(map.prefix);

                //Mark this as a dependency for this plugin, so it
                //can be traced for cycles.
                this.depMaps.push(pluginMap);

                on(pluginMap, 'defined', bind(this, function (plugin) {
                    var load, normalizedMap, normalizedMod,
                        bundleId = getOwn(bundlesMap, this.map.id),
                        name = this.map.name,
                        parentName = this.map.parentMap ? this.map.parentMap.name : null,
                        localRequire = context.makeRequire(map.parentMap, {
                            enableBuildCallback: true
                        });

                    //If current map is not normalized, wait for that
                    //normalized name to load instead of continuing.
                    if (this.map.unnormalized) {
                        //Normalize the ID if the plugin allows it.
                        if (plugin.normalize) {
                            name = plugin.normalize(name, function (name) {
                                return normalize(name, parentName, true);
                            }) || '';
                        }

                        //prefix and name should already be normalized, no need
                        //for applying map config again either.
                        normalizedMap = makeModuleMap(map.prefix + '!' + name,
                                                      this.map.parentMap);
                        on(normalizedMap,
                            'defined', bind(this, function (value) {
                                this.init([], function () { return value; }, null, {
                                    enabled: true,
                                    ignore: true
                                });
                            }));

                        normalizedMod = getOwn(registry, normalizedMap.id);
                        if (normalizedMod) {
                            //Mark this as a dependency for this plugin, so it
                            //can be traced for cycles.
                            this.depMaps.push(normalizedMap);

                            if (this.events.error) {
                                normalizedMod.on('error', bind(this, function (err) {
                                    this.emit('error', err);
                                }));
                            }
                            normalizedMod.enable();
                        }

                        return;
                    }

                    //If a paths config, then just load that file instead to
                    //resolve the plugin, as it is built into that paths layer.
                    if (bundleId) {
                        this.map.url = context.nameToUrl(bundleId);
                        this.load();
                        return;
                    }

                    load = bind(this, function (value) {
                        this.init([], function () { return value; }, null, {
                            enabled: true
                        });
                    });

                    load.error = bind(this, function (err) {
                        this.inited = true;
                        this.error = err;
                        err.requireModules = [id];

                        //Remove temp unnormalized modules for this module,
                        //since they will never be resolved otherwise now.
                        eachProp(registry, function (mod) {
                            if (mod.map.id.indexOf(id + '_unnormalized') === 0) {
                                cleanRegistry(mod.map.id);
                            }
                        });

                        onError(err);
                    });

                    //Allow plugins to load other code without having to know the
                    //context or how to 'complete' the load.
                    load.fromText = bind(this, function (text, textAlt) {
                        /*jslint evil: true */
                        var moduleName = map.name,
                            moduleMap = makeModuleMap(moduleName),
                            hasInteractive = useInteractive;

                        //As of 2.1.0, support just passing the text, to reinforce
                        //fromText only being called once per resource. Still
                        //support old style of passing moduleName but discard
                        //that moduleName in favor of the internal ref.
                        if (textAlt) {
                            text = textAlt;
                        }

                        //Turn off interactive script matching for IE for any define
                        //calls in the text, then turn it back on at the end.
                        if (hasInteractive) {
                            useInteractive = false;
                        }

                        //Prime the system by creating a module instance for
                        //it.
                        getModule(moduleMap);

                        //Transfer any config to this other module.
                        if (hasProp(config.config, id)) {
                            config.config[moduleName] = config.config[id];
                        }

                        try {
                            req.exec(text);
                        } catch (e) {
                            return onError(makeError('fromtexteval',
                                             'fromText eval for ' + id +
                                            ' failed: ' + e,
                                             e,
                                             [id]));
                        }

                        if (hasInteractive) {
                            useInteractive = true;
                        }

                        //Mark this as a dependency for the plugin
                        //resource
                        this.depMaps.push(moduleMap);

                        //Support anonymous modules.
                        context.completeLoad(moduleName);

                        //Bind the value of that module to the value for this
                        //resource ID.
                        localRequire([moduleName], load);
                    });

                    //Use parentName here since the plugin's name is not reliable,
                    //could be some weird string with no path that actually wants to
                    //reference the parentName's path.
                    plugin.load(map.name, localRequire, load, config);
                }));

                context.enable(pluginMap, this);
                this.pluginMaps[pluginMap.id] = pluginMap;
            },

            enable: function () {
                enabledRegistry[this.map.id] = this;
                this.enabled = true;

                //Set flag mentioning that the module is enabling,
                //so that immediate calls to the defined callbacks
                //for dependencies do not trigger inadvertent load
                //with the depCount still being zero.
                this.enabling = true;

                //Enable each dependency
                each(this.depMaps, bind(this, function (depMap, i) {
                    var id, mod, handler;

                    if (typeof depMap === 'string') {
                        //Dependency needs to be converted to a depMap
                        //and wired up to this module.
                        depMap = makeModuleMap(depMap,
                                               (this.map.isDefine ? this.map : this.map.parentMap),
                                               false,
                                               !this.skipMap);
                        this.depMaps[i] = depMap;

                        handler = getOwn(handlers, depMap.id);

                        if (handler) {
                            this.depExports[i] = handler(this);
                            return;
                        }

                        this.depCount += 1;

                        on(depMap, 'defined', bind(this, function (depExports) {
                            this.defineDep(i, depExports);
                            this.check();
                        }));

                        if (this.errback) {
                            on(depMap, 'error', bind(this, this.errback));
                        }
                    }

                    id = depMap.id;
                    mod = registry[id];

                    //Skip special modules like 'require', 'exports', 'module'
                    //Also, don't call enable if it is already enabled,
                    //important in circular dependency cases.
                    if (!hasProp(handlers, id) && mod && !mod.enabled) {
                        context.enable(depMap, this);
                    }
                }));

                //Enable each plugin that is used in
                //a dependency
                eachProp(this.pluginMaps, bind(this, function (pluginMap) {
                    var mod = getOwn(registry, pluginMap.id);
                    if (mod && !mod.enabled) {
                        context.enable(pluginMap, this);
                    }
                }));

                this.enabling = false;

                this.check();
            },

            on: function (name, cb) {
                var cbs = this.events[name];
                if (!cbs) {
                    cbs = this.events[name] = [];
                }
                cbs.push(cb);
            },

            emit: function (name, evt) {
                each(this.events[name], function (cb) {
                    cb(evt);
                });
                if (name === 'error') {
                    //Now that the error handler was triggered, remove
                    //the listeners, since this broken Module instance
                    //can stay around for a while in the registry.
                    delete this.events[name];
                }
            }
        };

        function callGetModule(args) {
            //Skip modules already defined.
            if (!hasProp(defined, args[0])) {
                getModule(makeModuleMap(args[0], null, true)).init(args[1], args[2]);
            }
        }

        function removeListener(node, func, name, ieName) {
            //Favor detachEvent because of IE9
            //issue, see attachEvent/addEventListener comment elsewhere
            //in this file.
            if (node.detachEvent && !isOpera) {
                //Probably IE. If not it will throw an error, which will be
                //useful to know.
                if (ieName) {
                    node.detachEvent(ieName, func);
                }
            } else {
                node.removeEventListener(name, func, false);
            }
        }

        /**
        * Given an event from a script node, get the requirejs info from it,
        * and then removes the event listeners on the node.
        * @param {Event} evt
        * @returns {Object}
        */
        function getScriptData(evt) {
            //Using currentTarget instead of target for Firefox 2.0's sake. Not
            //all old browsers will be supported, but this one was easy enough
            //to support and still makes sense.
            var node = evt.currentTarget || evt.srcElement;

            //Remove the listeners once here.
            removeListener(node, context.onScriptLoad, 'load', 'onreadystatechange');
            removeListener(node, context.onScriptError, 'error');

            return {
                node: node,
                id: node && node.getAttribute('data-requiremodule')
            };
        }

        function intakeDefines() {
            var args;

            //Any defined modules in the global queue, intake them now.
            takeGlobalQueue();

            //Make sure any remaining defQueue items get properly processed.
            while (defQueue.length) {
                args = defQueue.shift();
                if (args[0] === null) {
                    return onError(makeError('mismatch', 'Mismatched anonymous define() module: ' + args[args.length - 1]));
                } else {
                    //args are id, deps, factory. Should be normalized by the
                    //define() function.
                    callGetModule(args);
                }
            }
        }

        context = {
            config: config,
            contextName: contextName,
            registry: registry,
            defined: defined,
            urlFetched: urlFetched,
            defQueue: defQueue,
            Module: Module,
            makeModuleMap: makeModuleMap,
            nextTick: req.nextTick,
            onError: onError,

            /**
            * Set a configuration for the context.
            * @param {Object} cfg config object to integrate.
            */
            configure: function (cfg) {
                //Make sure the baseUrl ends in a slash.
                if (cfg.baseUrl) {
                    if (cfg.baseUrl.charAt(cfg.baseUrl.length - 1) !== '/') {
                        cfg.baseUrl += '/';
                    }
                }

                //Save off the paths since they require special processing,
                //they are additive.
                var shim = config.shim,
                    objs = {
                        paths: true,
                        bundles: true,
                        config: true,
                        map: true
                    };

                eachProp(cfg, function (value, prop) {
                    if (objs[prop]) {
                        if (!config[prop]) {
                            config[prop] = {};
                        }
                        mixin(config[prop], value, true, true);
                    } else {
                        config[prop] = value;
                    }
                });

                //Reverse map the bundles
                if (cfg.bundles) {
                    eachProp(cfg.bundles, function (value, prop) {
                        each(value, function (v) {
                            if (v !== prop) {
                                bundlesMap[v] = prop;
                            }
                        });
                    });
                }

                //Merge shim
                if (cfg.shim) {
                    eachProp(cfg.shim, function (value, id) {
                        //Normalize the structure
                        if (isArray(value)) {
                            value = {
                                deps: value
                            };
                        }
                        if ((value.exports || value.init) && !value.exportsFn) {
                            value.exportsFn = context.makeShimExports(value);
                        }
                        shim[id] = value;
                    });
                    config.shim = shim;
                }

                //Adjust packages if necessary.
                if (cfg.packages) {
                    each(cfg.packages, function (pkgObj) {
                        var location, name;

                        pkgObj = typeof pkgObj === 'string' ? { name: pkgObj} : pkgObj;

                        name = pkgObj.name;
                        location = pkgObj.location;
                        if (location) {
                            config.paths[name] = pkgObj.location;
                        }

                        //Save pointer to main module ID for pkg name.
                        //Remove leading dot in main, so main paths are normalized,
                        //and remove any trailing .js, since different package
                        //envs have different conventions: some use a module name,
                        //some use a file name.
                        config.pkgs[name] = pkgObj.name + '/' + (pkgObj.main || 'main')
                                     .replace(currDirRegExp, '')
                                     .replace(jsSuffixRegExp, '');
                    });
                }

                //If there are any "waiting to execute" modules in the registry,
                //update the maps for them, since their info, like URLs to load,
                //may have changed.
                eachProp(registry, function (mod, id) {
                    //If module already has init called, since it is too
                    //late to modify them, and ignore unnormalized ones
                    //since they are transient.
                    if (!mod.inited && !mod.map.unnormalized) {
                        mod.map = makeModuleMap(id);
                    }
                });

                //If a deps array or a config callback is specified, then call
                //require with those args. This is useful when require is defined as a
                //config object before require.js is loaded.
                if (cfg.deps || cfg.callback) {
                    context.require(cfg.deps || [], cfg.callback);
                }
            },

            makeShimExports: function (value) {
                function fn() {
                    var ret;
                    if (value.init) {
                        ret = value.init.apply(global, arguments);
                    }
                    return ret || (value.exports && getGlobal(value.exports));
                }
                return fn;
            },

            makeRequire: function (relMap, options) {
                options = options || {};

                function localRequire(deps, callback, errback) {
                    var id, map, requireMod;

                    if (options.enableBuildCallback && callback && isFunction(callback)) {
                        callback.__requireJsBuild = true;
                    }

                    if (typeof deps === 'string') {
                        if (isFunction(callback)) {
                            //Invalid call
                            return onError(makeError('requireargs', 'Invalid require call'), errback);
                        }

                        //If require|exports|module are requested, get the
                        //value for them from the special handlers. Caveat:
                        //this only works while module is being defined.
                        if (relMap && hasProp(handlers, deps)) {
                            return handlers[deps](registry[relMap.id]);
                        }

                        //Synchronous access to one module. If require.get is
                        //available (as in the Node adapter), prefer that.
                        if (req.get) {
                            return req.get(context, deps, relMap, localRequire);
                        }

                        //Normalize module name, if it contains . or ..
                        map = makeModuleMap(deps, relMap, false, true);
                        id = map.id;

                        if (!hasProp(defined, id)) {
                            return onError(makeError('notloaded', 'Module name "' +
                                        id +
                                        '" has not been loaded yet for context: ' +
                                        contextName +
                                        (relMap ? '' : '. Use require([])')));
                        }
                        return defined[id];
                    }

                    //Grab defines waiting in the global queue.
                    intakeDefines();

                    //Mark all the dependencies as needing to be loaded.
                    context.nextTick(function () {
                        //Some defines could have been added since the
                        //require call, collect them.
                        intakeDefines();

                        requireMod = getModule(makeModuleMap(null, relMap));

                        //Store if map config should be applied to this require
                        //call for dependencies.
                        requireMod.skipMap = options.skipMap;

                        requireMod.init(deps, callback, errback, {
                            enabled: true
                        });

                        checkLoaded();
                    });

                    return localRequire;
                }

                mixin(localRequire, {
                    isBrowser: isBrowser,

                    /**
                    * Converts a module name + .extension into an URL path.
                    * *Requires* the use of a module name. It does not support using
                    * plain URLs like nameToUrl.
                    */
                    toUrl: function (moduleNamePlusExt) {
                        var ext,
                            index = moduleNamePlusExt.lastIndexOf('.'),
                            segment = moduleNamePlusExt.split('/')[0],
                            isRelative = segment === '.' || segment === '..';

                        //Have a file extension alias, and it is not the
                        //dots from a relative path.
                        if (index !== -1 && (!isRelative || index > 1)) {
                            ext = moduleNamePlusExt.substring(index, moduleNamePlusExt.length);
                            moduleNamePlusExt = moduleNamePlusExt.substring(0, index);
                        }

                        return context.nameToUrl(normalize(moduleNamePlusExt,
                                                relMap && relMap.id, true), ext, true);
                    },

                    defined: function (id) {
                        return hasProp(defined, makeModuleMap(id, relMap, false, true).id);
                    },

                    specified: function (id) {
                        id = makeModuleMap(id, relMap, false, true).id;
                        return hasProp(defined, id) || hasProp(registry, id);
                    }
                });

                //Only allow undef on top level require calls
                if (!relMap) {
                    localRequire.undef = function (id) {
                        //Bind any waiting define() calls to this context,
                        //fix for #408
                        takeGlobalQueue();

                        var map = makeModuleMap(id, relMap, true),
                            mod = getOwn(registry, id);

                        removeScript(id);

                        delete defined[id];
                        delete urlFetched[map.url];
                        delete undefEvents[id];

                        //Clean queued defines too. Go backwards
                        //in array so that the splices do not
                        //mess up the iteration.
                        eachReverse(defQueue, function (args, i) {
                            if (args[0] === id) {
                                defQueue.splice(i, 1);
                            }
                        });

                        if (mod) {
                            //Hold on to listeners in case the
                            //module will be attempted to be reloaded
                            //using a different config.
                            if (mod.events.defined) {
                                undefEvents[id] = mod.events;
                            }

                            cleanRegistry(id);
                        }
                    };
                }

                return localRequire;
            },

            /**
            * Called to enable a module if it is still in the registry
            * awaiting enablement. A second arg, parent, the parent module,
            * is passed in for context, when this method is overridden by
            * the optimizer. Not shown here to keep code compact.
            */
            enable: function (depMap) {
                var mod = getOwn(registry, depMap.id);
                if (mod) {
                    getModule(depMap).enable();
                }
            },

            /**
            * Internal method used by environment adapters to complete a load event.
            * A load event could be a script load or just a load pass from a synchronous
            * load call.
            * @param {String} moduleName the name of the module to potentially complete.
            */
            completeLoad: function (moduleName) {
                var found, args, mod,
                    shim = getOwn(config.shim, moduleName) || {},
                    shExports = shim.exports;

                takeGlobalQueue();

                while (defQueue.length) {
                    args = defQueue.shift();
                    if (args[0] === null) {
                        args[0] = moduleName;
                        //If already found an anonymous module and bound it
                        //to this name, then this is some other anon module
                        //waiting for its completeLoad to fire.
                        if (found) {
                            break;
                        }
                        found = true;
                    } else if (args[0] === moduleName) {
                        //Found matching define call for this script!
                        found = true;
                    }

                    callGetModule(args);
                }

                //Do this after the cycle of callGetModule in case the result
                //of those calls/init calls changes the registry.
                mod = getOwn(registry, moduleName);

                if (!found && !hasProp(defined, moduleName) && mod && !mod.inited) {
                    if (config.enforceDefine && (!shExports || !getGlobal(shExports))) {
                        if (hasPathFallback(moduleName)) {
                            return;
                        } else {
                            return onError(makeError('nodefine',
                                             'No define call for ' + moduleName,
                                             null,
                                             [moduleName]));
                        }
                    } else {
                        //A script that does not call define(), so just simulate
                        //the call for it.
                        callGetModule([moduleName, (shim.deps || []), shim.exportsFn]);
                    }
                }

                checkLoaded();
            },

            /**
            * Converts a module name to a file path. Supports cases where
            * moduleName may actually be just an URL.
            * Note that it **does not** call normalize on the moduleName,
            * it is assumed to have already been normalized. This is an
            * internal API, not a public one. Use toUrl for the public API.
            */
            nameToUrl: function (moduleName, ext, skipExt) {
                var paths, syms, i, parentModule, url,
                    parentPath, bundleId,
                    pkgMain = getOwn(config.pkgs, moduleName);

                if (pkgMain) {
                    moduleName = pkgMain;
                }

                bundleId = getOwn(bundlesMap, moduleName);

                if (bundleId) {
                    return context.nameToUrl(bundleId, ext, skipExt);
                }

                //If a colon is in the URL, it indicates a protocol is used and it is just
                //an URL to a file, or if it starts with a slash, contains a query arg (i.e. ?)
                //or ends with .js, then assume the user meant to use an url and not a module id.
                //The slash is important for protocol-less URLs as well as full paths.
                if (req.jsExtRegExp.test(moduleName)) {
                    //Just a plain path, not module name lookup, so just return it.
                    //Add extension if it is included. This is a bit wonky, only non-.js things pass
                    //an extension, this method probably needs to be reworked.
                    url = moduleName + (ext || '');
                } else {
                    //A module that needs to be converted to a path.
                    paths = config.paths;

                    syms = moduleName.split('/');
                    //For each module name segment, see if there is a path
                    //registered for it. Start with most specific name
                    //and work up from it.
                    for (i = syms.length; i > 0; i -= 1) {
                        parentModule = syms.slice(0, i).join('/');

                        parentPath = getOwn(paths, parentModule);
                        if (parentPath) {
                            //If an array, it means there are a few choices,
                            //Choose the one that is desired
                            if (isArray(parentPath)) {
                                parentPath = parentPath[0];
                            }
                            syms.splice(0, i, parentPath);
                            break;
                        }
                    }

                    //Join the path parts together, then figure out if baseUrl is needed.
                    url = syms.join('/');
                    url += (ext || (/^data\:|\?/.test(url) || skipExt ? '' : '.js'));
                    url = (url.charAt(0) === '/' || url.match(/^[\w\+\.\-]+:/) ? '' : config.baseUrl) + url;
                }

                return config.urlArgs ? url +
                                        ((url.indexOf('?') === -1 ? '?' : '&') +
                                         config.urlArgs) : url;
            },

            //Delegates to req.load. Broken out as a separate function to
            //allow overriding in the optimizer.
            load: function (id, url) {
                req.load(context, id, url);
            },

            /**
            * Executes a module callback function. Broken out as a separate function
            * solely to allow the build system to sequence the files in the built
            * layer in the right sequence.
            *
            * @private
            */
            execCb: function (name, callback, args, exports) {
                return callback.apply(exports, args);
            },

            /**
            * callback for script loads, used to check status of loading.
            *
            * @param {Event} evt the event from the browser for the script
            * that was loaded.
            */
            onScriptLoad: function (evt) {
                //Using currentTarget instead of target for Firefox 2.0's sake. Not
                //all old browsers will be supported, but this one was easy enough
                //to support and still makes sense.
                if (evt.type === 'load' ||
                        (readyRegExp.test((evt.currentTarget || evt.srcElement).readyState))) {
                    //Reset interactive script so a script node is not held onto for
                    //to long.
                    interactiveScript = null;

                    //Pull out the name of the module and the context.
                    var data = getScriptData(evt);
                    context.completeLoad(data.id);
                }
            },

            /**
            * Callback for script errors.
            */
            onScriptError: function (evt) {
                var data = getScriptData(evt);
                if (!hasPathFallback(data.id)) {
                    return onError(makeError('scripterror', 'Script error for: ' + data.id, evt, [data.id]));
                }
            }
        };

        context.require = context.makeRequire();
        return context;
    }

    /**
    * Main entry point.
    *
    * If the only argument to require is a string, then the module that
    * is represented by that string is fetched for the appropriate context.
    *
    * If the first argument is an array, then it will be treated as an array
    * of dependency string names to fetch. An optional function callback can
    * be specified to execute when all of those dependencies are available.
    *
    * Make a local req variable to help Caja compliance (it assumes things
    * on a require that are not standardized), and to give a short
    * name for minification/local scope use.
    */
    req = requirejs = function (deps, callback, errback, optional) {

        //Find the right context, use default
        var context, config,
            contextName = defContextName;

        // Determine if have config object in the call.
        if (!isArray(deps) && typeof deps !== 'string') {
            // deps is a config object
            config = deps;
            if (isArray(callback)) {
                // Adjust args if there are dependencies
                deps = callback;
                callback = errback;
                errback = optional;
            } else {
                deps = [];
            }
        }

        if (config && config.context) {
            contextName = config.context;
        }

        context = getOwn(contexts, contextName);
        if (!context) {
            context = contexts[contextName] = req.s.newContext(contextName);
        }

        if (config) {
            context.configure(config);
        }

        return context.require(deps, callback, errback);
    };

    /**
    * Support require.config() to make it easier to cooperate with other
    * AMD loaders on globally agreed names.
    */
    req.config = function (config) {
        return req(config);
    };

    /**
    * Execute something after the current tick
    * of the event loop. Override for other envs
    * that have a better solution than setTimeout.
    * @param  {Function} fn function to execute later.
    */
    req.nextTick = typeof setTimeout !== 'undefined' ? function (fn) {
        setTimeout(fn, 4);
    } : function (fn) { fn(); };

    /**
    * Export require as a global, but only if it does not already exist.
    */
    if (!require) {
        require = req;
    }

    req.version = version;

    //Used to filter out dependencies that are already paths.
    req.jsExtRegExp = /^\/|:|\?|\.js$/;
    req.isBrowser = isBrowser;
    s = req.s = {
        contexts: contexts,
        newContext: newContext
    };

    //Create default context.
    req({});

    //Exports some context-sensitive methods on global require.
    each([
        'toUrl',
        'undef',
        'defined',
        'specified'
    ], function (prop) {
        //Reference from contexts instead of early binding to default context,
        //so that during builds, the latest instance of the default context
        //with its config gets used.
        req[prop] = function () {
            var ctx = contexts[defContextName];
            return ctx.require[prop].apply(ctx, arguments);
        };
    });

    if (isBrowser) {
        head = s.head = document.getElementsByTagName('head')[0];
        //If BASE tag is in play, using appendChild is a problem for IE6.
        //When that browser dies, this can be removed. Details in this jQuery bug:
        //http://dev.jquery.com/ticket/2709
        baseElement = document.getElementsByTagName('base')[0];
        if (baseElement) {
            head = s.head = baseElement.parentNode;
        }
    }

    /**
    * Any errors that require explicitly generates will be passed to this
    * function. Intercept/override it if you want custom error handling.
    * @param {Error} err the error object.
    */
    req.onError = defaultOnError;

    /**
    * Creates the node for the load command. Only used in browser envs.
    */
    req.createNode = function (config, moduleName, url) {
        var node = config.xhtml ?
                document.createElementNS('http://www.w3.org/1999/xhtml', 'html:script') :
                document.createElement('script');
        node.type = config.scriptType || 'text/javascript';
        node.charset = 'utf-8';
        node.async = true;
        return node;
    };

    /**
    * Does the request to load a module for the browser case.
    * Make this a separate function to allow other environments
    * to override it.
    *
    * @param {Object} context the require context to find state.
    * @param {String} moduleName the name of the module.
    * @param {Object} url the URL to the module.
    */
    req.load = function (context, moduleName, url) {
        var config = (context && context.config) || {},
            node;
        if (isBrowser) {
            //In the browser so use a script tag
            node = req.createNode(config, moduleName, url);

            node.setAttribute('data-requirecontext', context.contextName);
            node.setAttribute('data-requiremodule', moduleName);

            //Set up load listener. Test attachEvent first because IE9 has
            //a subtle issue in its addEventListener and script onload firings
            //that do not match the behavior of all other browsers with
            //addEventListener support, which fire the onload event for a
            //script right after the script execution. See:
            //https://connect.microsoft.com/IE/feedback/details/648057/script-onload-event-is-not-fired-immediately-after-script-execution
            //UNFORTUNATELY Opera implements attachEvent but does not follow the script
            //script execution mode.
            if (node.attachEvent &&
            //Check if node.attachEvent is artificially added by custom script or
            //natively supported by browser
            //read https://github.com/jrburke/requirejs/issues/187
            //if we can NOT find [native code] then it must NOT natively supported.
            //in IE8, node.attachEvent does not have toString()
            //Note the test for "[native code" with no closing brace, see:
            //https://github.com/jrburke/requirejs/issues/273
                    !(node.attachEvent.toString && node.attachEvent.toString().indexOf('[native code') < 0) &&
                    !isOpera) {
                //Probably IE. IE (at least 6-8) do not fire
                //script onload right after executing the script, so
                //we cannot tie the anonymous define call to a name.
                //However, IE reports the script as being in 'interactive'
                //readyState at the time of the define call.
                useInteractive = true;

                node.attachEvent('onreadystatechange', context.onScriptLoad);
                //It would be great to add an error handler here to catch
                //404s in IE9+. However, onreadystatechange will fire before
                //the error handler, so that does not help. If addEventListener
                //is used, then IE will fire error before load, but we cannot
                //use that pathway given the connect.microsoft.com issue
                //mentioned above about not doing the 'script execute,
                //then fire the script load event listener before execute
                //next script' that other browsers do.
                //Best hope: IE10 fixes the issues,
                //and then destroys all installs of IE 6-9.
                //node.attachEvent('onerror', context.onScriptError);
            } else {
                node.addEventListener('load', context.onScriptLoad, false);
                node.addEventListener('error', context.onScriptError, false);
            }
            node.src = url;

            //For some cache cases in IE 6-8, the script executes before the end
            //of the appendChild execution, so to tie an anonymous define
            //call to the module name (which is stored on the node), hold on
            //to a reference to this node, but clear after the DOM insertion.
            currentlyAddingScript = node;
            if (baseElement) {
                head.insertBefore(node, baseElement);
            } else {
                head.appendChild(node);
            }
            currentlyAddingScript = null;

            return node;
        } else if (isWebWorker) {
            try {
                //In a web worker, use importScripts. This is not a very
                //efficient use of importScripts, importScripts will block until
                //its script is downloaded and evaluated. However, if web workers
                //are in play, the expectation that a build has been done so that
                //only one script needs to be loaded anyway. This may need to be
                //reevaluated if other use cases become common.
                importScripts(url);

                //Account for anonymous modules
                context.completeLoad(moduleName);
            } catch (e) {
                context.onError(makeError('importscripts',
                                'importScripts failed for ' +
                                    moduleName + ' at ' + url,
                                e,
                                [moduleName]));
            }
        }
    };

    function getInteractiveScript() {
        if (interactiveScript && interactiveScript.readyState === 'interactive') {
            return interactiveScript;
        }

        eachReverse(scripts(), function (script) {
            if (script.readyState === 'interactive') {
                return (interactiveScript = script);
            }
        });
        return interactiveScript;
    }

    //Look for a data-main script attribute, which could also adjust the baseUrl.
    if (isBrowser && !cfg.skipDataMain) {
        //Figure out baseUrl. Get it from the script tag with require.js in it.
        eachReverse(scripts(), function (script) {
            //Set the 'head' where we can append children by
            //using the script's parent.
            if (!head) {
                head = script.parentNode;
            }

            //Look for a data-main attribute to set main script for the page
            //to load. If it is there, the path to data main becomes the
            //baseUrl, if it is not already set.
            dataMain = script.getAttribute('data-main');
            if (dataMain) {
                //Preserve dataMain in case it is a path (i.e. contains '?')
                mainScript = dataMain;

                //Set final baseUrl if there is not already an explicit one.
                if (!cfg.baseUrl) {
                    //Pull off the directory of data-main for use as the
                    //baseUrl.
                    src = mainScript.split('/');
                    mainScript = src.pop();
                    subPath = src.length ? src.join('/') + '/' : './';

                    cfg.baseUrl = subPath;
                }

                //Strip off any trailing .js since mainScript is now
                //like a module name.
                mainScript = mainScript.replace(jsSuffixRegExp, '');

                //If mainScript is still a path, fall back to dataMain
                if (req.jsExtRegExp.test(mainScript)) {
                    mainScript = dataMain;
                }

                //Put the data-main script in the files to load.
                cfg.deps = cfg.deps ? cfg.deps.concat(mainScript) : [mainScript];

                return true;
            }
        });
    }

    /**
    * The function that handles definitions of modules. Differs from
    * require() in that a string for the module should be the first argument,
    * and the function to execute after dependencies are loaded should
    * return a value to define the module corresponding to the first argument's
    * name.
    */
    define = function (name, deps, callback) {
        var node, context;

        //Allow for anonymous modules
        if (typeof name !== 'string') {
            //Adjust args appropriately
            callback = deps;
            deps = name;
            name = null;
        }

        //This module may not have dependencies
        if (!isArray(deps)) {
            callback = deps;
            deps = null;
        }

        //If no name, and callback is a function, then figure out if it a
        //CommonJS thing with dependencies.
        if (!deps && isFunction(callback)) {
            deps = [];
            //Remove comments from the callback string,
            //look for require calls, and pull them into the dependencies,
            //but only if there are function args.
            if (callback.length) {
                callback
                    .toString()
                    .replace(commentRegExp, '')
                    .replace(cjsRequireRegExp, function (match, dep) {
                        deps.push(dep);
                    });

                //May be a CommonJS thing even without require calls, but still
                //could use exports, and module. Avoid doing exports and module
                //work though if it just needs require.
                //REQUIRES the function to expect the CommonJS variables in the
                //order listed below.
                deps = (callback.length === 1 ? ['require'] : ['require', 'exports', 'module']).concat(deps);
            }
        }

        //If in IE 6-8 and hit an anonymous define() call, do the interactive
        //work.
        if (useInteractive) {
            node = currentlyAddingScript || getInteractiveScript();
            if (node) {
                if (!name) {
                    name = node.getAttribute('data-requiremodule');
                }
                context = contexts[node.getAttribute('data-requirecontext')];
            }
        }

        //Always save off evaluating the def call until the script onload handler.
        //This allows multiple modules to be in a file without prematurely
        //tracing dependencies, and allows for anonymous module support,
        //where the module name is not known until the script onload event
        //occurs. If no context, use the global queue, and get it processed
        //in the onscript load callback.
        (context ? context.defQueue : globalDefQueue).push([name, deps, callback]);
    };

    define.amd = {
        jQuery: true
    };


    /**
    * Executes the text. Normally just uses eval, but can be modified
    * to use a better, environment-specific call. Only used for transpiling
    * loader plugins, not for plain JS modules.
    * @param {String} text the text to execute/evaluate.
    */
    req.exec = function (text) {
        /*jslint evil: true */
        return eval(text);
    };

    //Set up with config info.
    req(cfg);
} (this));
//ends: require.js
//starts: require.config.js
//The configuration for require js.

//SHIMS
(function () { define('as', [], function () { return as; }); })();

(function() {
    define('common/tagUtil', ['svc/windowLocation'], function(windowLocator) {
        return new TagUtil(windowLocator.getLocation());
    });
})();
//ends: require.config.js
//starts: windowLocation.svc.js
(function() {
    define('svc/windowLocation',
    [],
    function() {
        var windowLocationSingleton = function() {
            var self = this;
            self.getLocation = function() {
                return window.location;
            };
        };

        return new windowLocationSingleton();
    });
})();
//ends: windowLocation.svc.js
//starts: pixelCaller.js
(function() {
    define('common/pixelCaller',
        ['common/pixel', 'common/adaraPixelInitializer', 'as'],
        function (pixel, adaraPixelInit) {
            return function() {
                if (typeof (pixel) != "undefined" && pixel.runAll) {
                    pixel.runAll();
                }
            };
        }
    );
})();

//ends: pixelCaller.js
//starts: yepnope.js
// yepnope.js
// Version - 1.5.4pre
//
// by
// Alex Sexton - @SlexAxton - AlexSexton[at]gmail.com
// Ralph Holzmann - @ralphholzmann - ralphholzmann[at]gmail.com
//
// http://yepnopejs.com/
// https://github.com/SlexAxton/yepnope.js/
//
// Tri-license - WTFPL | MIT | BSD
//
// Please minify before use.
// Also available as Modernizr.load via the Modernizr Project
//
(function (window, doc, undef) {

    var docElement = doc.documentElement,
    sTimeout = window.setTimeout,
    firstScript = doc.getElementsByTagName("script")[0],
    toString = {}.toString,
    execStack = [],
    started = 0,
    noop = function () { },
    // Before you get mad about browser sniffs, please read:
    // https://github.com/Modernizr/Modernizr/wiki/Undetectables
    // If you have a better solution, we are actively looking to solve the problem
    isGecko = ("MozAppearance" in docElement.style),
    isGeckoLTE18 = isGecko && !!doc.createRange().compareNode,
    insBeforeObj = isGeckoLTE18 ? docElement : firstScript.parentNode,
    // Thanks to @jdalton for showing us this opera detection (by way of @kangax) (and probably @miketaylr too, or whatever...)
    isOpera = window.opera && toString.call(window.opera) == "[object Opera]",
    isIE = !!doc.attachEvent && !isOpera,
    // isOlderWebkit fix for #95 - https://github.com/SlexAxton/yepnope.js/issues/95
    isOlderWebkit = ('webkitAppearance' in docElement.style) && !('async' in doc.createElement('script')),
    strJsElem = isGecko ? "object" : (isIE || isOlderWebkit) ? "script" : "img",
    strCssElem = isIE ? "script" : (isOlderWebkit) ? "img" : strJsElem,
    isArray = Array.isArray || function (obj) {
        return toString.call(obj) == "[object Array]";
    },
    isObject = function (obj) {
        return Object(obj) === obj;
    },
    isString = function (s) {
        return typeof s == "string";
    },
    isFunction = function (fn) {
        return toString.call(fn) == "[object Function]";
    },
    readFirstScript = function () {
        if (!firstScript || !firstScript.parentNode) {
            firstScript = doc.getElementsByTagName("script")[0];
        }
    },
    globalFilters = [],
    scriptCache = {},
    prefixes = {
        // key value pair timeout options
        timeout: function (resourceObj, prefix_parts) {
            if (prefix_parts.length) {
                resourceObj['timeout'] = prefix_parts[0];
            }
            return resourceObj;
        }
    },
    handler,
    yepnope;

    /* Loader helper functions */
    function isFileReady(readyState) {
        // Check to see if any of the ways a file can be ready are available as properties on the file's element
        return (!readyState || readyState == "loaded" || readyState == "complete" || readyState == "uninitialized");
    }


    // Takes a preloaded js obj (changes in different browsers) and injects it into the head
    // in the appropriate order
    function injectJs(src, cb, attrs, timeout, /* internal use */err, internal) {

        var script = doc.createElement("script"),
        done, i;

        timeout = timeout || yepnope['errorTimeout'];

        script.src = src;

        // Add our extra attributes to the script element
        for (i in attrs) {
            script.setAttribute(i, attrs[i]);
        }

        cb = internal ? executeStack : (cb || noop);

        // Bind to load events
        script.onreadystatechange = script.onload = function () {

            if (!done && isFileReady(script.readyState)) {

                // Set done to prevent this function from being called twice.
                done = 1;
                cb();

                // Handle memory leak in IE
                script.onload = script.onreadystatechange = null;
            }
        };

        // 404 Fallback
        sTimeout(function () {
            if (!done) {
                done = 1;
                // Might as well pass in an error-state if we fire the 404 fallback
                cb(1);
            }
        }, timeout);

        // Inject script into to document
        // or immediately callback if we know there
        // was previously a timeout error
        readFirstScript();
        err ? script.onload() : firstScript.parentNode.insertBefore(script, firstScript);
    }

    // Takes a preloaded css obj (changes in different browsers) and injects it into the head
    function injectCss(href, cb, attrs, timeout, /* Internal use */err, internal) {

        // Create stylesheet link
        var link = doc.createElement("link"),
        done, i;

        timeout = timeout || yepnope['errorTimeout'];

        cb = internal ? executeStack : (cb || noop);

        // Add attributes
        link.href = href;
        link.rel = "stylesheet";
        link.type = "text/css";

        // Add our extra attributes to the link element
        for (i in attrs) {
            link.setAttribute(i, attrs[i]);
        }

        if (!err) {
            readFirstScript();
            firstScript.parentNode.insertBefore(link, firstScript);
            sTimeout(cb, 0);
        }
    }

    function executeStack() {
        // shift an element off of the stack
        var i = execStack.shift();
        started = 1;

        // if a is truthy and the first item in the stack has an src
        if (i) {
            // if it's a script, inject it into the head with no type attribute
            if (i['t']) {
                // Inject after a timeout so FF has time to be a jerk about it and
                // not double load (ignore the cache)
                sTimeout(function () {
                    (i['t'] == "c" ? yepnope['injectCss'] : yepnope['injectJs'])(i['s'], 0, i['a'], i['x'], i['e'], 1);
                }, 0);
            }
            // Otherwise, just call the function and potentially run the stack
            else {
                i();
                executeStack();
            }
        }
        else {
            // just reset out of recursive mode
            started = 0;
        }
    }

    function preloadFile(elem, url, type, splicePoint, dontExec, attrObj, timeout) {

        timeout = timeout || yepnope['errorTimeout'];

        // Create appropriate element for browser and type
        var preloadElem = doc.createElement(elem),
        done = 0,
        firstFlag = 0,
        stackObject = {
            "t": type,     // type
            "s": url,      // src
            //r: 0,        // ready
            "e": dontExec, // set to true if we don't want to reinject
            "a": attrObj,
            "x": timeout
        };

        // The first time (common-case)
        if (scriptCache[url] === 1) {
            firstFlag = 1;
            scriptCache[url] = [];
        }

        function onload(first) {
            // If the script/css file is loaded
            if (!done && isFileReady(preloadElem.readyState)) {

                // Set done to prevent this function from being called twice.
                stackObject['r'] = done = 1;

                !started && executeStack();

                if (first) {
                    if (elem != "img") {
                        sTimeout(function () { insBeforeObj.removeChild(preloadElem) }, 50);
                    }

                    for (var i in scriptCache[url]) {
                        if (scriptCache[url].hasOwnProperty(i)) {
                            scriptCache[url][i].onload();
                        }
                    }

                    // Handle memory leak in IE
                    preloadElem.onload = preloadElem.onreadystatechange = null;
                }
            }
        }


        // Setting url to data for objects or src for img/scripts
        if (elem == "object") {
            preloadElem.data = url;

            // Setting the type attribute to stop Firefox complaining about the mimetype when running locally.
            // The type doesn't matter as long as it's real, thus text/css instead of text/javascript.
            preloadElem.setAttribute("type", "text/css");
        } else {
            preloadElem.src = url;

            // Setting bogus script type to allow the script to be cached
            preloadElem.type = elem;
        }

        // Don't let it show up visually
        preloadElem.width = preloadElem.height = "0";

        // Attach handlers for all browsers
        preloadElem.onerror = preloadElem.onload = preloadElem.onreadystatechange = function () {
            onload.call(this, firstFlag);
        };
        // inject the element into the stack depending on if it's
        // in the middle of other scripts or not
        execStack.splice(splicePoint, 0, stackObject);

        // The only place these can't go is in the <head> element, since objects won't load in there
        // so we have two options - insert before the head element (which is hard to assume) - or
        // insertBefore technically takes null/undefined as a second param and it will insert the element into
        // the parent last. We try the head, and it automatically falls back to undefined.
        if (elem != "img") {
            // If it's the first time, or we've already loaded it all the way through
            if (firstFlag || scriptCache[url] === 2) {
                readFirstScript();
                insBeforeObj.insertBefore(preloadElem, isGeckoLTE18 ? null : firstScript);

                // If something fails, and onerror doesn't fire,
                // continue after a timeout.
                sTimeout(onload, timeout);
            }
            else {
                // instead of injecting, just hold on to it
                scriptCache[url].push(preloadElem);
            }
        }
    }

    function load(resource, type, dontExec, attrObj, timeout) {
        // If this method gets hit multiple times, we should flag
        // that the execution of other threads should halt.
        started = 0;

        // We'll do 'j' for js and 'c' for css, yay for unreadable minification tactics
        type = type || "j";
        if (isString(resource)) {
            // if the resource passed in here is a string, preload the file
            preloadFile(type == "c" ? strCssElem : strJsElem, resource, type, this['i']++, dontExec, attrObj, timeout);
        } else {
            // Otherwise it's a callback function and we can splice it into the stack to run
            execStack.splice(this['i']++, 0, resource);
            execStack.length == 1 && executeStack();
        }

        // OMG is this jQueries? For chaining...
        return this;
    }

    // return the yepnope object with a fresh loader attached
    function getYepnope() {
        var y = yepnope;
        y['loader'] = {
            "load": load,
            "i": 0
        };
        return y;
    }

    /* End loader helper functions */
    // Yepnope Function
    yepnope = function (needs) {

        var i,
        need,
        // start the chain as a plain instance
        chain = this['yepnope']['loader'];

        function satisfyPrefixes(url) {
            // split all prefixes out
            var parts = url.split("!"),
      gLen = globalFilters.length,
      origUrl = parts.pop(),
      pLen = parts.length,
      res = {
          "url": origUrl,
          // keep this one static for callback variable consistency
          "origUrl": origUrl,
          "prefixes": parts
      },
      mFunc,
      j,
      prefix_parts;

            // loop through prefixes
            // if there are none, this automatically gets skipped
            for (j = 0; j < pLen; j++) {
                prefix_parts = parts[j].split('=');
                mFunc = prefixes[prefix_parts.shift()];
                if (mFunc) {
                    res = mFunc(res, prefix_parts);
                }
            }

            // Go through our global filters
            for (j = 0; j < gLen; j++) {
                res = globalFilters[j](res);
            }

            // return the final url
            return res;
        }

        function getExtension(url) {
            //The extension is always the last characters before the ? and after a period.
            //The previous method was not accounting for the possibility of a period in the query string.
            var b = url.split('?')[0];
            return b.substr(b.lastIndexOf('.') + 1);
        }

        function loadScriptOrStyle(input, callback, chain, index, testResult) {
            // run through our set of prefixes
            var resource = satisfyPrefixes(input),
          autoCallback = resource['autoCallback'],
          extension = getExtension(resource['url']);

            // if no object is returned or the url is empty/0 just exit the load
            if (resource['bypass']) {
                return;
            }

            // Determine callback, if any
            if (callback) {
                callback = isFunction(callback) ?
          callback :
          callback[input] ||
          callback[index] ||
          callback[(input.split("/").pop().split("?")[0])];
            }

            // if someone is overriding all normal functionality
            if (resource['instead']) {
                return resource['instead'](input, callback, chain, index, testResult);
            }
            else {
                // Handle if we've already had this url and it's completed loaded already
                if (scriptCache[resource['url']] && resource['reexecute'] !== true) {
                    // don't let this execute again
                    resource['noexec'] = true;
                }
                else {
                    scriptCache[resource['url']] = 1;
                }

                // Throw this into the queue
                input && chain.load(resource['url'], ((resource['forceCSS'] || (!resource['forceJS'] && "css" == getExtension(resource['url'])))) ? "c" : undef, resource['noexec'], resource['attrs'], resource['timeout']);

                // If we have a callback, we'll start the chain over
                if (isFunction(callback) || isFunction(autoCallback)) {
                    // Call getJS with our current stack of things
                    chain['load'](function () {
                        // Hijack yepnope and restart index counter
                        getYepnope();
                        // Call our callbacks with this set of data
                        callback && callback(resource['origUrl'], testResult, index);
                        autoCallback && autoCallback(resource['origUrl'], testResult, index);

                        // Override this to just a boolean positive
                        scriptCache[resource['url']] = 2;
                    });
                }
            }
        }

        function loadFromTestObject(testObject, chain) {
            var testResult = !!testObject['test'],
            group = testResult ? testObject['yep'] : testObject['nope'],
            always = testObject['load'] || testObject['both'],
            callback = testObject['callback'] || noop,
            cbRef = callback,
            complete = testObject['complete'] || noop,
            needGroupSize,
            callbackKey;

            // Reusable function for dealing with the different input types
            // NOTE:: relies on closures to keep 'chain' up to date, a bit confusing, but
            // much smaller than the functional equivalent in this case.
            function handleGroup(needGroup, moreToCome) {
                if ('' !== needGroup && !needGroup) {
                    // Call the complete callback when there's nothing to load.
                    !moreToCome && complete();
                }
                // If it's a string
                else if (isString(needGroup)) {
                    // if it's a string, it's the last
                    if (!moreToCome) {
                        // Add in the complete callback to go at the end
                        callback = function () {
                            var args = [].slice.call(arguments);
                            cbRef.apply(this, args);
                            complete();
                        };
                    }
                    // Just load the script of style
                    loadScriptOrStyle(needGroup, callback, chain, 0, testResult);
                }
                // See if we have an object. Doesn't matter if it's an array or a key/val hash
                // Note:: order cannot be guaranteed on an key value object with multiple elements
                // since the for-in does not preserve order. Arrays _should_ go in order though.
                else if (isObject(needGroup)) {
                    // I hate this, but idk another way for objects.
                    needGroupSize = (function () {
                        var count = 0, i
                        for (i in needGroup) {
                            if (needGroup.hasOwnProperty(i)) {
                                count++;
                            }
                        }
                        return count;
                    })();

                    for (callbackKey in needGroup) {
                        // Safari 2 does not have hasOwnProperty, but not worth the bytes for a shim
                        // patch if needed. Kangax has a nice shim for it. Or just remove the check
                        // and promise not to extend the object prototype.
                        if (needGroup.hasOwnProperty(callbackKey)) {
                            // Find the last added resource, and append to it's callback.
                            if (!moreToCome && !(--needGroupSize)) {
                                // If this is an object full of callbacks
                                if (!isFunction(callback)) {
                                    // Add in the complete callback to go at the end
                                    callback[callbackKey] = (function (innerCb) {
                                        return function () {
                                            var args = [].slice.call(arguments);
                                            innerCb && innerCb.apply(this, args);
                                            complete();
                                        };
                                    })(cbRef[callbackKey]);
                                }
                                // If this is just a single callback
                                else {
                                    callback = function () {
                                        var args = [].slice.call(arguments);
                                        cbRef.apply(this, args);
                                        complete();
                                    };
                                }
                            }
                            loadScriptOrStyle(needGroup[callbackKey], callback, chain, callbackKey, testResult);
                        }
                    }
                }
            }

            // figure out what this group should do
            handleGroup(group, !!always || !!testObject['complete']);

            // Run our loader on the load/both group too
            // the always stuff always loads second.
            always && handleGroup(always);

            // If complete callback is used without loading anything
            !always && !!testObject['complete'] && handleGroup('');

        }

        // Someone just decides to load a single script or css file as a string
        if (isString(needs)) {
            loadScriptOrStyle(needs, 0, chain, 0);
        }
        // Normal case is likely an array of different types of loading options
        else if (isArray(needs)) {
            // go through the list of needs
            for (i = 0; i < needs.length; i++) {
                need = needs[i];

                // if it's a string, just load it
                if (isString(need)) {
                    loadScriptOrStyle(need, 0, chain, 0);
                }
                // if it's an array, call our function recursively
                else if (isArray(need)) {
                    yepnope(need);
                }
                // if it's an object, use our modernizr logic to win
                else if (isObject(need)) {
                    loadFromTestObject(need, chain);
                }
            }
        }
        // Allow a single object to be passed in
        else if (isObject(needs)) {
            loadFromTestObject(needs, chain);
        }
    };

    // This publicly exposed function is for allowing
    // you to add functionality based on prefixes on the
    // string files you add. 'css!' is a builtin prefix
    //
    // The arguments are the prefix (not including the !) as a string
    // and
    // A callback function. This function is passed a resource object
    // that can be manipulated and then returned. (like middleware. har.)
    //
    // Examples of this can be seen in the officially supported ie prefix
    yepnope['addPrefix'] = function (prefix, callback) {
        prefixes[prefix] = callback;
    };

    // A filter is a global function that every resource
    // object that passes through yepnope will see. You can
    // of course conditionally choose to modify the resource objects
    // or just pass them along. The filter function takes the resource
    // object and is expected to return one.
    //
    // The best example of a filter is the 'autoprotocol' officially
    // supported filter
    yepnope['addFilter'] = function (filter) {
        globalFilters.push(filter);
    };

    // Default error timeout to 10sec - modify to alter
    yepnope['errorTimeout'] = 1e4;

    // Webreflection readystate hack
    // safe for jQuery 1.4+ ( i.e. don't use yepnope with jQuery 1.3.2 )
    // if the readyState is null and we have a listener
    if (doc.readyState == null && doc.addEventListener) {
        // set the ready state to loading
        doc.readyState = "loading";
        // call the listener
        doc.addEventListener("DOMContentLoaded", handler = function () {
            // Remove the listener
            doc.removeEventListener("DOMContentLoaded", handler, 0);
            // Set it to ready
            doc.readyState = "complete";
        }, 0);
    }

    // Attach loader &
    // Leak it
    window['yepnope'] = getYepnope();

    // Exposing executeStack to better facilitate plugins
    window['yepnope']['executeStack'] = executeStack;
    window['yepnope']['injectJs'] = injectJs;
    window['yepnope']['injectCss'] = injectCss;

})(this, document);
//ends: yepnope.js
//starts: modernizr.js
/*!
* Modernizr v2.7.1
* www.modernizr.com
*
* Copyright (c) Faruk Ates, Paul Irish, Alex Sexton
* Available under the BSD and MIT licenses: www.modernizr.com/license/
*/

/*
* Modernizr tests which native CSS3 and HTML5 features are available in
* the current UA and makes the results available to you in two ways:
* as properties on a global Modernizr object, and as classes on the
* <html> element. This information allows you to progressively enhance
* your pages with a granular level of control over the experience.
*
* Modernizr has an optional (not included) conditional resource loader
* called Modernizr.load(), based on Yepnope.js (yepnopejs.com).
* To get a build that includes Modernizr.load(), as well as choosing
* which tests to include, go to www.modernizr.com/download/
*
* Authors        Faruk Ates, Paul Irish, Alex Sexton
* Contributors   Ryan Seddon, Ben Alman
*/

window.Modernizr = (function (window, document, undefined) {

    var version = '2.7.1',

    Modernizr = {},

    /*>>cssclasses*/
    // option for enabling the HTML classes to be added
    enableClasses = true,
    /*>>cssclasses*/

    docElement = document.documentElement,

    /**
    * Create our "modernizr" element that we do most feature tests on.
    */
    mod = 'modernizr',
    modElem = document.createElement(mod),
    mStyle = modElem.style,

    /**
    * Create the input element for various Web Forms feature tests.
    */
    inputElem /*>>inputelem*/ = document.createElement('input') /*>>inputelem*/,

    /*>>smile*/
    smile = ':)',
    /*>>smile*/

    toString = {}.toString,

    // TODO :: make the prefixes more granular
    /*>>prefixes*/
    // List of property values to set for css tests. See ticket #21
    prefixes = ' -webkit- -moz- -o- -ms- '.split(' '),
    /*>>prefixes*/

    /*>>domprefixes*/
    // Following spec is to expose vendor-specific style properties as:
    //   elem.style.WebkitBorderRadius
    // and the following would be incorrect:
    //   elem.style.webkitBorderRadius

    // Webkit ghosts their properties in lowercase but Opera & Moz do not.
    // Microsoft uses a lowercase `ms` instead of the correct `Ms` in IE8+
    //   erik.eae.net/archives/2008/03/10/21.48.10/

    // More here: github.com/Modernizr/Modernizr/issues/issue/21
    omPrefixes = 'Webkit Moz O ms',

    cssomPrefixes = omPrefixes.split(' '),

    domPrefixes = omPrefixes.toLowerCase().split(' '),
    /*>>domprefixes*/

    /*>>ns*/
    ns = { 'svg': 'http://www.w3.org/2000/svg' },
    /*>>ns*/

    tests = {},
    inputs = {},
    attrs = {},

    classes = [],

    slice = classes.slice,

    featureName, // used in testing loop


    /*>>teststyles*/
    // Inject element with style element and some CSS rules
    injectElementWithStyles = function (rule, callback, nodes, testnames) {

        var style, ret, node, docOverflow,
          div = document.createElement('div'),
        // After page load injecting a fake body doesn't work so check if body exists
          body = document.body,
        // IE6 and 7 won't return offsetWidth or offsetHeight unless it's in the body element, so we fake it.
          fakeBody = body || document.createElement('body');

        if (parseInt(nodes, 10)) {
            // In order not to give false positives we create a node for each test
            // This also allows the method to scale for unspecified uses
            while (nodes--) {
                node = document.createElement('div');
                node.id = testnames ? testnames[nodes] : mod + (nodes + 1);
                div.appendChild(node);
            }
        }

        // <style> elements in IE6-9 are considered 'NoScope' elements and therefore will be removed
        // when injected with innerHTML. To get around this you need to prepend the 'NoScope' element
        // with a 'scoped' element, in our case the soft-hyphen entity as it won't mess with our measurements.
        // msdn.microsoft.com/en-us/library/ms533897%28VS.85%29.aspx
        // Documents served as xml will throw if using &shy; so use xml friendly encoded version. See issue #277
        style = ['&#173;', '<style id="s', mod, '">', rule, '</style>'].join('');
        div.id = mod;
        // IE6 will false positive on some tests due to the style element inside the test div somehow interfering offsetHeight, so insert it into body or fakebody.
        // Opera will act all quirky when injecting elements in documentElement when page is served as xml, needs fakebody too. #270
        (body ? div : fakeBody).innerHTML += style;
        fakeBody.appendChild(div);
        if (!body) {
            //avoid crashing IE8, if background image is used
            fakeBody.style.background = '';
            //Safari 5.13/5.1.4 OSX stops loading if ::-webkit-scrollbar is used and scrollbars are visible
            fakeBody.style.overflow = 'hidden';
            docOverflow = docElement.style.overflow;
            docElement.style.overflow = 'hidden';
            docElement.appendChild(fakeBody);
        }

        ret = callback(div, rule);
        // If this is done after page load we don't want to remove the body so check if body exists
        if (!body) {
            fakeBody.parentNode.removeChild(fakeBody);
            docElement.style.overflow = docOverflow;
        } else {
            div.parentNode.removeChild(div);
        }

        return !!ret;

    },
    /*>>teststyles*/

    /*>>mq*/
    // adapted from matchMedia polyfill
    // by Scott Jehl and Paul Irish
    // gist.github.com/786768
    testMediaQuery = function (mq) {

        var matchMedia = window.matchMedia || window.msMatchMedia;
        if (matchMedia) {
            return matchMedia(mq).matches;
        }

        var bool;

        injectElementWithStyles('@media ' + mq + ' { #' + mod + ' { position: absolute; } }', function (node) {
            bool = (window.getComputedStyle ?
                  getComputedStyle(node, null) :
                  node.currentStyle)['position'] == 'absolute';
        });

        return bool;

    },
    /*>>mq*/


    /*>>hasevent*/
    //
    // isEventSupported determines if a given element supports the given event
    // kangax.github.com/iseventsupported/
    //
    // The following results are known incorrects:
    //   Modernizr.hasEvent("webkitTransitionEnd", elem) // false negative
    //   Modernizr.hasEvent("textInput") // in Webkit. github.com/Modernizr/Modernizr/issues/333
    //   ...
    isEventSupported = (function () {

        var TAGNAMES = {
            'select': 'input', 'change': 'input',
            'submit': 'form', 'reset': 'form',
            'error': 'img', 'load': 'img', 'abort': 'img'
        };

        function isEventSupported(eventName, element) {

            element = element || document.createElement(TAGNAMES[eventName] || 'div');
            eventName = 'on' + eventName;

            // When using `setAttribute`, IE skips "unload", WebKit skips "unload" and "resize", whereas `in` "catches" those
            var isSupported = eventName in element;

            if (!isSupported) {
                // If it has no `setAttribute` (i.e. doesn't implement Node interface), try generic element
                if (!element.setAttribute) {
                    element = document.createElement('div');
                }
                if (element.setAttribute && element.removeAttribute) {
                    element.setAttribute(eventName, '');
                    isSupported = is(element[eventName], 'function');

                    // If property was created, "remove it" (by setting value to `undefined`)
                    if (!is(element[eventName], 'undefined')) {
                        element[eventName] = undefined;
                    }
                    element.removeAttribute(eventName);
                }
            }

            element = null;
            return isSupported;
        }
        return isEventSupported;
    })(),
    /*>>hasevent*/

    // TODO :: Add flag for hasownprop ? didn't last time

    // hasOwnProperty shim by kangax needed for Safari 2.0 support
    _hasOwnProperty = ({}).hasOwnProperty, hasOwnProp;

    if (!is(_hasOwnProperty, 'undefined') && !is(_hasOwnProperty.call, 'undefined')) {
        hasOwnProp = function (object, property) {
            return _hasOwnProperty.call(object, property);
        };
    }
    else {
        hasOwnProp = function (object, property) { /* yes, this can give false positives/negatives, but most of the time we don't care about those */
            return ((property in object) && is(object.constructor.prototype[property], 'undefined'));
        };
    }

    // Adapted from ES5-shim https://github.com/kriskowal/es5-shim/blob/master/es5-shim.js
    // es5.github.com/#x15.3.4.5

    if (!Function.prototype.bind) {
        Function.prototype.bind = function bind(that) {

            var target = this;

            if (typeof target != "function") {
                throw new TypeError();
            }

            var args = slice.call(arguments, 1),
            bound = function () {

                if (this instanceof bound) {

                    var F = function () { };
                    F.prototype = target.prototype;
                    var self = new F();

                    var result = target.apply(
                  self,
                  args.concat(slice.call(arguments))
              );
                    if (Object(result) === result) {
                        return result;
                    }
                    return self;

                } else {

                    return target.apply(
                  that,
                  args.concat(slice.call(arguments))
              );

                }

            };

            return bound;
        };
    }

    /**
    * setCss applies given styles to the Modernizr DOM node.
    */
    function setCss(str) {
        mStyle.cssText = str;
    }

    /**
    * setCssAll extrapolates all vendor-specific css strings.
    */
    function setCssAll(str1, str2) {
        return setCss(prefixes.join(str1 + ';') + (str2 || ''));
    }

    /**
    * is returns a boolean for if typeof obj is exactly type.
    */
    function is(obj, type) {
        return typeof obj === type;
    }

    /**
    * contains returns a boolean for if substr is found within str.
    */
    function contains(str, substr) {
        return !! ~('' + str).indexOf(substr);
    }

    /*>>testprop*/

    // testProps is a generic CSS / DOM property test.

    // In testing support for a given CSS property, it's legit to test:
    //    `elem.style[styleName] !== undefined`
    // If the property is supported it will return an empty string,
    // if unsupported it will return undefined.

    // We'll take advantage of this quick test and skip setting a style
    // on our modernizr element, but instead just testing undefined vs
    // empty string.

    // Because the testing of the CSS property names (with "-", as
    // opposed to the camelCase DOM properties) is non-portable and
    // non-standard but works in WebKit and IE (but not Gecko or Opera),
    // we explicitly reject properties with dashes so that authors
    // developing in WebKit or IE first don't end up with
    // browser-specific content by accident.

    function testProps(props, prefixed) {
        for (var i in props) {
            var prop = props[i];
            if (!contains(prop, "-") && mStyle[prop] !== undefined) {
                return prefixed == 'pfx' ? prop : true;
            }
        }
        return false;
    }
    /*>>testprop*/

    // TODO :: add testDOMProps
    /**
    * testDOMProps is a generic DOM property test; if a browser supports
    *   a certain property, it won't return undefined for it.
    */
    function testDOMProps(props, obj, elem) {
        for (var i in props) {
            var item = obj[props[i]];
            if (item !== undefined) {

                // return the property name as a string
                if (elem === false) return props[i];

                // let's bind a function
                if (is(item, 'function')) {
                    // default to autobind unless override
                    return item.bind(elem || obj);
                }

                // return the unbound function or obj or value
                return item;
            }
        }
        return false;
    }

    /*>>testallprops*/
    /**
    * testPropsAll tests a list of DOM properties we want to check against.
    *   We specify literally ALL possible (known and/or likely) properties on
    *   the element including the non-vendor prefixed one, for forward-
    *   compatibility.
    */
    function testPropsAll(prop, prefixed, elem) {

        var ucProp = prop.charAt(0).toUpperCase() + prop.slice(1),
            props = (prop + ' ' + cssomPrefixes.join(ucProp + ' ') + ucProp).split(' ');

        // did they call .prefixed('boxSizing') or are we just testing a prop?
        if (is(prefixed, "string") || is(prefixed, "undefined")) {
            return testProps(props, prefixed);

            // otherwise, they called .prefixed('requestAnimationFrame', window[, elem])
        } else {
            props = (prop + ' ' + (domPrefixes).join(ucProp + ' ') + ucProp).split(' ');
            return testDOMProps(props, prefixed, elem);
        }
    }
    /*>>testallprops*/


    /**
    * Tests
    * -----
    */

    // The *new* flexbox
    // dev.w3.org/csswg/css3-flexbox

    tests['flexbox'] = function () {
        return testPropsAll('flexWrap');
    };

    // The *old* flexbox
    // www.w3.org/TR/2009/WD-css3-flexbox-20090723/

    tests['flexboxlegacy'] = function () {
        return testPropsAll('boxDirection');
    };

    // On the S60 and BB Storm, getContext exists, but always returns undefined
    // so we actually have to call getContext() to verify
    // github.com/Modernizr/Modernizr/issues/issue/97/

    tests['canvas'] = function () {
        var elem = document.createElement('canvas');
        return !!(elem.getContext && elem.getContext('2d'));
    };

    tests['canvastext'] = function () {
        return !!(Modernizr['canvas'] && is(document.createElement('canvas').getContext('2d').fillText, 'function'));
    };

    // webk.it/70117 is tracking a legit WebGL feature detect proposal

    // We do a soft detect which may false positive in order to avoid
    // an expensive context creation: bugzil.la/732441

    tests['webgl'] = function () {
        return !!window.WebGLRenderingContext;
    };

    /*
    * The Modernizr.touch test only indicates if the browser supports
    *    touch events, which does not necessarily reflect a touchscreen
    *    device, as evidenced by tablets running Windows 7 or, alas,
    *    the Palm Pre / WebOS (touch) phones.
    *
    * Additionally, Chrome (desktop) used to lie about its support on this,
    *    but that has since been rectified: crbug.com/36415
    *
    * We also test for Firefox 4 Multitouch Support.
    *
    * For more info, see: modernizr.github.com/Modernizr/touch.html
    */

    tests['touch'] = function () {
        var bool;

        if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
            bool = true;
        } else {
            injectElementWithStyles(['@media (', prefixes.join('touch-enabled),('), mod, ')', '{#modernizr{top:9px;position:absolute}}'].join(''), function (node) {
                bool = node.offsetTop === 9;
            });
        }

        return bool;
    };


    // geolocation is often considered a trivial feature detect...
    // Turns out, it's quite tricky to get right:
    //
    // Using !!navigator.geolocation does two things we don't want. It:
    //   1. Leaks memory in IE9: github.com/Modernizr/Modernizr/issues/513
    //   2. Disables page caching in WebKit: webk.it/43956
    //
    // Meanwhile, in Firefox < 8, an about:config setting could expose
    // a false positive that would throw an exception: bugzil.la/688158

    tests['geolocation'] = function () {
        return 'geolocation' in navigator;
    };


    tests['postmessage'] = function () {
        return !!window.postMessage;
    };


    // Chrome incognito mode used to throw an exception when using openDatabase
    // It doesn't anymore.
    tests['websqldatabase'] = function () {
        return !!window.openDatabase;
    };

    // Vendors had inconsistent prefixing with the experimental Indexed DB:
    // - Webkit's implementation is accessible through webkitIndexedDB
    // - Firefox shipped moz_indexedDB before FF4b9, but since then has been mozIndexedDB
    // For speed, we don't test the legacy (and beta-only) indexedDB
    tests['indexedDB'] = function () {
        return !!testPropsAll("indexedDB", window);
    };

    // documentMode logic from YUI to filter out IE8 Compat Mode
    //   which false positives.
    tests['hashchange'] = function () {
        return isEventSupported('hashchange', window) && (document.documentMode === undefined || document.documentMode > 7);
    };

    // Per 1.6:
    // This used to be Modernizr.historymanagement but the longer
    // name has been deprecated in favor of a shorter and property-matching one.
    // The old API is still available in 1.6, but as of 2.0 will throw a warning,
    // and in the first release thereafter disappear entirely.
    tests['history'] = function () {
        return !!(window.history && history.pushState);
    };

    tests['draganddrop'] = function () {
        var div = document.createElement('div');
        return ('draggable' in div) || ('ondragstart' in div && 'ondrop' in div);
    };

    // FF3.6 was EOL'ed on 4/24/12, but the ESR version of FF10
    // will be supported until FF19 (2/12/13), at which time, ESR becomes FF17.
    // FF10 still uses prefixes, so check for it until then.
    // for more ESR info, see: mozilla.org/en-US/firefox/organizations/faq/
    tests['websockets'] = function () {
        return 'WebSocket' in window || 'MozWebSocket' in window;
    };


    // css-tricks.com/rgba-browser-support/
    tests['rgba'] = function () {
        // Set an rgba() color and check the returned value

        setCss('background-color:rgba(150,255,150,.5)');

        return contains(mStyle.backgroundColor, 'rgba');
    };

    tests['hsla'] = function () {
        // Same as rgba(), in fact, browsers re-map hsla() to rgba() internally,
        //   except IE9 who retains it as hsla

        setCss('background-color:hsla(120,40%,100%,.5)');

        return contains(mStyle.backgroundColor, 'rgba') || contains(mStyle.backgroundColor, 'hsla');
    };

    tests['multiplebgs'] = function () {
        // Setting multiple images AND a color on the background shorthand property
        //  and then querying the style.background property value for the number of
        //  occurrences of "url(" is a reliable method for detecting ACTUAL support for this!

        setCss('background:url(https://),url(https://),red url(https://)');

        // If the UA supports multiple backgrounds, there should be three occurrences
        //   of the string "url(" in the return value for elemStyle.background

        return (/(url\s*\(.*?){3}/).test(mStyle.background);
    };



    // this will false positive in Opera Mini
    //   github.com/Modernizr/Modernizr/issues/396

    tests['backgroundsize'] = function () {
        return testPropsAll('backgroundSize');
    };

    tests['borderimage'] = function () {
        return testPropsAll('borderImage');
    };


    // Super comprehensive table about all the unique implementations of
    // border-radius: muddledramblings.com/table-of-css3-border-radius-compliance

    tests['borderradius'] = function () {
        return testPropsAll('borderRadius');
    };

    // WebOS unfortunately false positives on this test.
    tests['boxshadow'] = function () {
        return testPropsAll('boxShadow');
    };

    // FF3.0 will false positive on this test
    tests['textshadow'] = function () {
        return document.createElement('div').style.textShadow === '';
    };


    tests['opacity'] = function () {
        // Browsers that actually have CSS Opacity implemented have done so
        //  according to spec, which means their return values are within the
        //  range of [0.0,1.0] - including the leading zero.

        setCssAll('opacity:.55');

        // The non-literal . in this regex is intentional:
        //   German Chrome returns this value as 0,55
        // github.com/Modernizr/Modernizr/issues/#issue/59/comment/516632
        return (/^0.55$/).test(mStyle.opacity);
    };


    // Note, Android < 4 will pass this test, but can only animate
    //   a single property at a time
    //   daneden.me/2011/12/putting-up-with-androids-bullshit/
    tests['cssanimations'] = function () {
        return testPropsAll('animationName');
    };


    tests['csscolumns'] = function () {
        return testPropsAll('columnCount');
    };


    tests['cssgradients'] = function () {
        /**
        * For CSS Gradients syntax, please see:
        * webkit.org/blog/175/introducing-css-gradients/
        * developer.mozilla.org/en/CSS/-moz-linear-gradient
        * developer.mozilla.org/en/CSS/-moz-radial-gradient
        * dev.w3.org/csswg/css3-images/#gradients-
        */

        var str1 = 'background-image:',
            str2 = 'gradient(linear,left top,right bottom,from(#9f9),to(white));',
            str3 = 'linear-gradient(left top,#9f9, white);';

        setCss(
        // legacy webkit syntax (FIXME: remove when syntax not in use anymore)
              (str1 + '-webkit- '.split(' ').join(str2 + str1) +
        // standard syntax             // trailing 'background-image:'
              prefixes.join(str3 + str1)).slice(0, -str1.length)
        );

        return contains(mStyle.backgroundImage, 'gradient');
    };


    tests['cssreflections'] = function () {
        return testPropsAll('boxReflect');
    };


    tests['csstransforms'] = function () {
        return !!testPropsAll('transform');
    };


    tests['csstransforms3d'] = function () {

        var ret = !!testPropsAll('perspective');

        // Webkit's 3D transforms are passed off to the browser's own graphics renderer.
        //   It works fine in Safari on Leopard and Snow Leopard, but not in Chrome in
        //   some conditions. As a result, Webkit typically recognizes the syntax but
        //   will sometimes throw a false positive, thus we must do a more thorough check:
        if (ret && 'webkitPerspective' in docElement.style) {

            // Webkit allows this media query to succeed only if the feature is enabled.
            // `@media (transform-3d),(-webkit-transform-3d){ ... }`
            injectElementWithStyles('@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}', function (node, rule) {
                ret = node.offsetLeft === 9 && node.offsetHeight === 3;
            });
        }
        return ret;
    };


    tests['csstransitions'] = function () {
        return testPropsAll('transition');
    };


    /*>>fontface*/
    // @font-face detection routine by Diego Perini
    // javascript.nwbox.com/CSSSupport/

    // false positives:
    //   WebOS github.com/Modernizr/Modernizr/issues/342
    //   WP7   github.com/Modernizr/Modernizr/issues/538
    tests['fontface'] = function () {
        var bool;

        injectElementWithStyles('@font-face {font-family:"font";src:url("https://")}', function (node, rule) {
            var style = document.getElementById('smodernizr'),
              sheet = style.sheet || style.styleSheet,
              cssText = sheet ? (sheet.cssRules && sheet.cssRules[0] ? sheet.cssRules[0].cssText : sheet.cssText || '') : '';

            bool = /src/i.test(cssText) && cssText.indexOf(rule.split(' ')[0]) === 0;
        });

        return bool;
    };
    /*>>fontface*/

    // CSS generated content detection
    tests['generatedcontent'] = function () {
        var bool;

        injectElementWithStyles(['#', mod, '{font:0/0 a}#', mod, ':after{content:"', smile, '";visibility:hidden;font:3px/1 a}'].join(''), function (node) {
            bool = node.offsetHeight >= 3;
        });

        return bool;
    };



    // These tests evaluate support of the video/audio elements, as well as
    // testing what types of content they support.
    //
    // We're using the Boolean constructor here, so that we can extend the value
    // e.g.  Modernizr.video     // true
    //       Modernizr.video.ogg // 'probably'
    //
    // Codec values from : github.com/NielsLeenheer/html5test/blob/9106a8/index.html#L845
    //                     thx to NielsLeenheer and zcorpan

    // Note: in some older browsers, "no" was a return value instead of empty string.
    //   It was live in FF3.5.0 and 3.5.1, but fixed in 3.5.2
    //   It was also live in Safari 4.0.0 - 4.0.4, but fixed in 4.0.5

    tests['video'] = function () {
        var elem = document.createElement('video'),
            bool = false;

        // IE9 Running on Windows Server SKU can cause an exception to be thrown, bug #224
        try {
            if (bool = !!elem.canPlayType) {
                bool = new Boolean(bool);
                bool.ogg = elem.canPlayType('video/ogg; codecs="theora"').replace(/^no$/, '');

                // Without QuickTime, this value will be `undefined`. github.com/Modernizr/Modernizr/issues/546
                bool.h264 = elem.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/, '');

                bool.webm = elem.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/, '');
            }

        } catch (e) { }

        return bool;
    };

    tests['audio'] = function () {
        var elem = document.createElement('audio'),
            bool = false;

        try {
            if (bool = !!elem.canPlayType) {
                bool = new Boolean(bool);
                bool.ogg = elem.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, '');
                bool.mp3 = elem.canPlayType('audio/mpeg;').replace(/^no$/, '');

                // Mimetypes accepted:
                //   developer.mozilla.org/En/Media_formats_supported_by_the_audio_and_video_elements
                //   bit.ly/iphoneoscodecs
                bool.wav = elem.canPlayType('audio/wav; codecs="1"').replace(/^no$/, '');
                bool.m4a = (elem.canPlayType('audio/x-m4a;') ||
                              elem.canPlayType('audio/aac;')).replace(/^no$/, '');
            }
        } catch (e) { }

        return bool;
    };


    // In FF4, if disabled, window.localStorage should === null.

    // Normally, we could not test that directly and need to do a
    //   `('localStorage' in window) && ` test first because otherwise Firefox will
    //   throw bugzil.la/365772 if cookies are disabled

    // Also in iOS5 Private Browsing mode, attempting to use localStorage.setItem
    // will throw the exception:
    //   QUOTA_EXCEEDED_ERRROR DOM Exception 22.
    // Peculiarly, getItem and removeItem calls do not throw.

    // Because we are forced to try/catch this, we'll go aggressive.

    // Just FWIW: IE8 Compat mode supports these features completely:
    //   www.quirksmode.org/dom/html5.html
    // But IE8 doesn't support either with local files

    tests['localstorage'] = function () {
        try {
            localStorage.setItem(mod, mod);
            localStorage.removeItem(mod);
            return true;
        } catch (e) {
            return false;
        }
    };

    tests['sessionstorage'] = function () {
        try {
            sessionStorage.setItem(mod, mod);
            sessionStorage.removeItem(mod);
            return true;
        } catch (e) {
            return false;
        }
    };


    tests['webworkers'] = function () {
        return !!window.Worker;
    };


    tests['applicationcache'] = function () {
        return !!window.applicationCache;
    };


    // Thanks to Erik Dahlstrom
    tests['svg'] = function () {
        return !!document.createElementNS && !!document.createElementNS(ns.svg, 'svg').createSVGRect;
    };

    // specifically for SVG inline in HTML, not within XHTML
    // test page: paulirish.com/demo/inline-svg
    tests['inlinesvg'] = function () {
        var div = document.createElement('div');
        div.innerHTML = '<svg/>';
        return (div.firstChild && div.firstChild.namespaceURI) == ns.svg;
    };

    // SVG SMIL animation
    tests['smil'] = function () {
        return !!document.createElementNS && /SVGAnimate/.test(toString.call(document.createElementNS(ns.svg, 'animate')));
    };

    // This test is only for clip paths in SVG proper, not clip paths on HTML content
    // demo: srufaculty.sru.edu/david.dailey/svg/newstuff/clipPath4.svg

    // However read the comments to dig into applying SVG clippaths to HTML content here:
    //   github.com/Modernizr/Modernizr/issues/213#issuecomment-1149491
    tests['svgclippaths'] = function () {
        return !!document.createElementNS && /SVGClipPath/.test(toString.call(document.createElementNS(ns.svg, 'clipPath')));
    };

    /*>>webforms*/
    // input features and input types go directly onto the ret object, bypassing the tests loop.
    // Hold this guy to execute in a moment.
    function webforms() {
        /*>>input*/
        // Run through HTML5's new input attributes to see if the UA understands any.
        // We're using f which is the <input> element created early on
        // Mike Taylr has created a comprehensive resource for testing these attributes
        //   when applied to all input types:
        //   miketaylr.com/code/input-type-attr.html
        // spec: www.whatwg.org/specs/web-apps/current-work/multipage/the-input-element.html#input-type-attr-summary

        // Only input placeholder is tested while textarea's placeholder is not.
        // Currently Safari 4 and Opera 11 have support only for the input placeholder
        // Both tests are available in feature-detects/forms-placeholder.js
        Modernizr['input'] = (function (props) {
            for (var i = 0, len = props.length; i < len; i++) {
                attrs[props[i]] = !!(props[i] in inputElem);
            }
            if (attrs.list) {
                // safari false positive's on datalist: webk.it/74252
                // see also github.com/Modernizr/Modernizr/issues/146
                attrs.list = !!(document.createElement('datalist') && window.HTMLDataListElement);
            }
            return attrs;
        })('autocomplete autofocus list placeholder max min multiple pattern required step'.split(' '));
        /*>>input*/

        /*>>inputtypes*/
        // Run through HTML5's new input types to see if the UA understands any.
        //   This is put behind the tests runloop because it doesn't return a
        //   true/false like all the other tests; instead, it returns an object
        //   containing each input type with its corresponding true/false value

        // Big thanks to @miketaylr for the html5 forms expertise. miketaylr.com/
        Modernizr['inputtypes'] = (function (props) {

            for (var i = 0, bool, inputElemType, defaultView, len = props.length; i < len; i++) {

                inputElem.setAttribute('type', inputElemType = props[i]);
                bool = inputElem.type !== 'text';

                // We first check to see if the type we give it sticks..
                // If the type does, we feed it a textual value, which shouldn't be valid.
                // If the value doesn't stick, we know there's input sanitization which infers a custom UI
                if (bool) {

                    inputElem.value = smile;
                    inputElem.style.cssText = 'position:absolute;visibility:hidden;';

                    if (/^range$/.test(inputElemType) && inputElem.style.WebkitAppearance !== undefined) {

                        docElement.appendChild(inputElem);
                        defaultView = document.defaultView;

                        // Safari 2-4 allows the smiley as a value, despite making a slider
                        bool = defaultView.getComputedStyle &&
                              defaultView.getComputedStyle(inputElem, null).WebkitAppearance !== 'textfield' &&
                        // Mobile android web browser has false positive, so must
                        // check the height to see if the widget is actually there.
                              (inputElem.offsetHeight !== 0);

                        docElement.removeChild(inputElem);

                    } else if (/^(search|tel)$/.test(inputElemType)) {
                        // Spec doesn't define any special parsing or detectable UI
                        //   behaviors so we pass these through as true

                        // Interestingly, opera fails the earlier test, so it doesn't
                        //  even make it here.

                    } else if (/^(url|email)$/.test(inputElemType)) {
                        // Real url and email support comes with prebaked validation.
                        bool = inputElem.checkValidity && inputElem.checkValidity() === false;

                    } else {
                        // If the upgraded input compontent rejects the :) text, we got a winner
                        bool = inputElem.value != smile;
                    }
                }

                inputs[props[i]] = !!bool;
            }
            return inputs;
        })('search tel url email datetime date month week time datetime-local number range color'.split(' '));
        /*>>inputtypes*/
    }
    /*>>webforms*/


    // End of test definitions
    // -----------------------



    // Run through all tests and detect their support in the current UA.
    // todo: hypothetically we could be doing an array of tests and use a basic loop here.
    for (var feature in tests) {
        if (hasOwnProp(tests, feature)) {
            // run the test, throw the return value into the Modernizr,
            //   then based on that boolean, define an appropriate className
            //   and push it into an array of classes we'll join later.
            featureName = feature.toLowerCase();
            Modernizr[featureName] = tests[feature]();

            classes.push((Modernizr[featureName] ? '' : 'no-') + featureName);
        }
    }

    /*>>webforms*/
    // input tests need to run.
    Modernizr.input || webforms();
    /*>>webforms*/


    /**
    * addTest allows the user to define their own feature tests
    * the result will be added onto the Modernizr object,
    * as well as an appropriate className set on the html element
    *
    * @param feature - String naming the feature
    * @param test - Function returning true if feature is supported, false if not
    */
    Modernizr.addTest = function (feature, test) {
        if (typeof feature == 'object') {
            for (var key in feature) {
                if (hasOwnProp(feature, key)) {
                    Modernizr.addTest(key, feature[key]);
                }
            }
        } else {

            feature = feature.toLowerCase();

            if (Modernizr[feature] !== undefined) {
                // we're going to quit if you're trying to overwrite an existing test
                // if we were to allow it, we'd do this:
                //   var re = new RegExp("\\b(no-)?" + feature + "\\b");
                //   docElement.className = docElement.className.replace( re, '' );
                // but, no rly, stuff 'em.
                return Modernizr;
            }

            test = typeof test == 'function' ? test() : test;

            if (typeof enableClasses !== "undefined" && enableClasses) {
                docElement.className += ' ' + (test ? '' : 'no-') + feature;
            }
            Modernizr[feature] = test;

        }

        return Modernizr; // allow chaining.
    };


    // Reset modElem.cssText to nothing to reduce memory footprint.
    setCss('');
    modElem = inputElem = null;

    /*>>shiv*/
    /**
    * @preserve HTML5 Shiv prev3.7.1 | @afarkas @jdalton @jon_neal @rem | MIT/GPL2 Licensed
    */
    ; (function (window, document) {
        /*jshint evil:true */
        /** version */
        var version = '3.7.0';

        /** Preset options */
        var options = window.html5 || {};

        /** Used to skip problem elements */
        var reSkip = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i;

        /** Not all elements can be cloned in IE **/
        var saveClones = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i;

        /** Detect whether the browser supports default html5 styles */
        var supportsHtml5Styles;

        /** Name of the expando, to work with multiple documents or to re-shiv one document */
        var expando = '_html5shiv';

        /** The id for the the documents expando */
        var expanID = 0;

        /** Cached data for each document */
        var expandoData = {};

        /** Detect whether the browser supports unknown elements */
        var supportsUnknownElements;

        (function () {
            try {
                var a = document.createElement('a');
                a.innerHTML = '<xyz></xyz>';
                //if the hidden property is implemented we can assume, that the browser supports basic HTML5 Styles
                supportsHtml5Styles = ('hidden' in a);

                supportsUnknownElements = a.childNodes.length == 1 || (function () {
                    // assign a false positive if unable to shiv
                    (document.createElement)('a');
                    var frag = document.createDocumentFragment();
                    return (
                typeof frag.cloneNode == 'undefined' ||
                typeof frag.createDocumentFragment == 'undefined' ||
                typeof frag.createElement == 'undefined'
              );
                } ());
            } catch (e) {
                // assign a false positive if detection fails => unable to shiv
                supportsHtml5Styles = true;
                supportsUnknownElements = true;
            }

        } ());

        /*--------------------------------------------------------------------------*/

        /**
        * Creates a style sheet with the given CSS text and adds it to the document.
        * @private
        * @param {Document} ownerDocument The document.
        * @param {String} cssText The CSS text.
        * @returns {StyleSheet} The style element.
        */
        function addStyleSheet(ownerDocument, cssText) {
            var p = ownerDocument.createElement('p'),
          parent = ownerDocument.getElementsByTagName('head')[0] || ownerDocument.documentElement;

            p.innerHTML = 'x<style>' + cssText + '</style>';
            return parent.insertBefore(p.lastChild, parent.firstChild);
        }

        /**
        * Returns the value of `html5.elements` as an array.
        * @private
        * @returns {Array} An array of shived element node names.
        */
        function getElements() {
            var elements = html5.elements;
            return typeof elements == 'string' ? elements.split(' ') : elements;
        }

        /**
        * Returns the data associated to the given document
        * @private
        * @param {Document} ownerDocument The document.
        * @returns {Object} An object of data.
        */
        function getExpandoData(ownerDocument) {
            var data = expandoData[ownerDocument[expando]];
            if (!data) {
                data = {};
                expanID++;
                ownerDocument[expando] = expanID;
                expandoData[expanID] = data;
            }
            return data;
        }

        /**
        * returns a shived element for the given nodeName and document
        * @memberOf html5
        * @param {String} nodeName name of the element
        * @param {Document} ownerDocument The context document.
        * @returns {Object} The shived element.
        */
        function createElement(nodeName, ownerDocument, data) {
            if (!ownerDocument) {
                ownerDocument = document;
            }
            if (supportsUnknownElements) {
                return ownerDocument.createElement(nodeName);
            }
            if (!data) {
                data = getExpandoData(ownerDocument);
            }
            var node;

            if (data.cache[nodeName]) {
                node = data.cache[nodeName].cloneNode();
            } else if (saveClones.test(nodeName)) {
                node = (data.cache[nodeName] = data.createElem(nodeName)).cloneNode();
            } else {
                node = data.createElem(nodeName);
            }

            // Avoid adding some elements to fragments in IE < 9 because
            // * Attributes like `name` or `type` cannot be set/changed once an element
            //   is inserted into a document/fragment
            // * Link elements with `src` attributes that are inaccessible, as with
            //   a 403 response, will cause the tab/window to crash
            // * Script elements appended to fragments will execute when their `src`
            //   or `text` property is set
            return node.canHaveChildren && !reSkip.test(nodeName) && !node.tagUrn ? data.frag.appendChild(node) : node;
        }

        /**
        * returns a shived DocumentFragment for the given document
        * @memberOf html5
        * @param {Document} ownerDocument The context document.
        * @returns {Object} The shived DocumentFragment.
        */
        function createDocumentFragment(ownerDocument, data) {
            if (!ownerDocument) {
                ownerDocument = document;
            }
            if (supportsUnknownElements) {
                return ownerDocument.createDocumentFragment();
            }
            data = data || getExpandoData(ownerDocument);
            var clone = data.frag.cloneNode(),
          i = 0,
          elems = getElements(),
          l = elems.length;
            for (; i < l; i++) {
                clone.createElement(elems[i]);
            }
            return clone;
        }

        /**
        * Shivs the `createElement` and `createDocumentFragment` methods of the document.
        * @private
        * @param {Document|DocumentFragment} ownerDocument The document.
        * @param {Object} data of the document.
        */
        function shivMethods(ownerDocument, data) {
            if (!data.cache) {
                data.cache = {};
                data.createElem = ownerDocument.createElement;
                data.createFrag = ownerDocument.createDocumentFragment;
                data.frag = data.createFrag();
            }


            ownerDocument.createElement = function (nodeName) {
                //abort shiv
                if (!html5.shivMethods) {
                    return data.createElem(nodeName);
                }
                return createElement(nodeName, ownerDocument, data);
            };

            ownerDocument.createDocumentFragment = Function('h,f', 'return function(){' +
                                                          'var n=f.cloneNode(),c=n.createElement;' +
                                                          'h.shivMethods&&(' +
            // unroll the `createElement` calls
                                                          getElements().join().replace(/[\w\-]+/g, function (nodeName) {
                                                              data.createElem(nodeName);
                                                              data.frag.createElement(nodeName);
                                                              return 'c("' + nodeName + '")';
                                                          }) +
            ');return n}'
                                                         )(html5, data.frag);
        }

        /*--------------------------------------------------------------------------*/

        /**
        * Shivs the given document.
        * @memberOf html5
        * @param {Document} ownerDocument The document to shiv.
        * @returns {Document} The shived document.
        */
        function shivDocument(ownerDocument) {
            if (!ownerDocument) {
                ownerDocument = document;
            }
            var data = getExpandoData(ownerDocument);

            if (html5.shivCSS && !supportsHtml5Styles && !data.hasCSS) {
                data.hasCSS = !!addStyleSheet(ownerDocument,
                // corrects block display not defined in IE6/7/8/9
                                          'article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}' +
                // adds styling not present in IE6/7/8/9
                                            'mark{background:#FF0;color:#000}' +
                // hides non-rendered elements
                                            'template{display:none}'
                                         );
            }
            if (!supportsUnknownElements) {
                shivMethods(ownerDocument, data);
            }
            return ownerDocument;
        }

        /*--------------------------------------------------------------------------*/

        /**
        * The `html5` object is exposed so that more elements can be shived and
        * existing shiving can be detected on iframes.
        * @type Object
        * @example
        *
        * // options can be changed before the script is included
        * html5 = { 'elements': 'mark section', 'shivCSS': false, 'shivMethods': false };
        */
        var html5 = {

            /**
            * An array or space separated string of node names of the elements to shiv.
            * @memberOf html5
            * @type Array|String
            */
            'elements': options.elements || 'abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video',

            /**
            * current version of html5shiv
            */
            'version': version,

            /**
            * A flag to indicate that the HTML5 style sheet should be inserted.
            * @memberOf html5
            * @type Boolean
            */
            'shivCSS': (options.shivCSS !== false),

            /**
            * Is equal to true if a browser supports creating unknown/HTML5 elements
            * @memberOf html5
            * @type boolean
            */
            'supportsUnknownElements': supportsUnknownElements,

            /**
            * A flag to indicate that the document's `createElement` and `createDocumentFragment`
            * methods should be overwritten.
            * @memberOf html5
            * @type Boolean
            */
            'shivMethods': (options.shivMethods !== false),

            /**
            * A string to describe the type of `html5` object ("default" or "default print").
            * @memberOf html5
            * @type String
            */
            'type': 'default',

            // shivs the document according to the specified `html5` object options
            'shivDocument': shivDocument,

            //creates a shived element
            createElement: createElement,

            //creates a shived documentFragment
            createDocumentFragment: createDocumentFragment
        };

        /*--------------------------------------------------------------------------*/

        // expose html5
        window.html5 = html5;

        // shiv the document
        shivDocument(document);

    } (this, document));
    /*>>shiv*/

    // Assign private properties to the return object with prefix
    Modernizr._version = version;

    // expose these for the plugin API. Look in the source for how to join() them against your input
    /*>>prefixes*/
    Modernizr._prefixes = prefixes;
    /*>>prefixes*/
    /*>>domprefixes*/
    Modernizr._domPrefixes = domPrefixes;
    Modernizr._cssomPrefixes = cssomPrefixes;
    /*>>domprefixes*/

    /*>>mq*/
    // Modernizr.mq tests a given media query, live against the current state of the window
    // A few important notes:
    //   * If a browser does not support media queries at all (eg. oldIE) the mq() will always return false
    //   * A max-width or orientation query will be evaluated against the current state, which may change later.
    //   * You must specify values. Eg. If you are testing support for the min-width media query use:
    //       Modernizr.mq('(min-width:0)')
    // usage:
    // Modernizr.mq('only screen and (max-width:768)')
    Modernizr.mq = testMediaQuery;
    /*>>mq*/

    /*>>hasevent*/
    // Modernizr.hasEvent() detects support for a given event, with an optional element to test on
    // Modernizr.hasEvent('gesturestart', elem)
    Modernizr.hasEvent = isEventSupported;
    /*>>hasevent*/

    /*>>testprop*/
    // Modernizr.testProp() investigates whether a given style property is recognized
    // Note that the property names must be provided in the camelCase variant.
    // Modernizr.testProp('pointerEvents')
    Modernizr.testProp = function (prop) {
        return testProps([prop]);
    };
    /*>>testprop*/

    /*>>testallprops*/
    // Modernizr.testAllProps() investigates whether a given style property,
    //   or any of its vendor-prefixed variants, is recognized
    // Note that the property names must be provided in the camelCase variant.
    // Modernizr.testAllProps('boxSizing')
    Modernizr.testAllProps = testPropsAll;
    /*>>testallprops*/


    /*>>teststyles*/
    // Modernizr.testStyles() allows you to add custom styles to the document and test an element afterwards
    // Modernizr.testStyles('#modernizr { position:absolute }', function(elem, rule){ ... })
    Modernizr.testStyles = injectElementWithStyles;
    /*>>teststyles*/


    /*>>prefixed*/
    // Modernizr.prefixed() returns the prefixed or nonprefixed property name variant of your input
    // Modernizr.prefixed('boxSizing') // 'MozBoxSizing'

    // Properties must be passed as dom-style camelcase, rather than `box-sizing` hypentated style.
    // Return values will also be the camelCase variant, if you need to translate that to hypenated style use:
    //
    //     str.replace(/([A-Z])/g, function(str,m1){ return '-' + m1.toLowerCase(); }).replace(/^ms-/,'-ms-');

    // If you're trying to ascertain which transition end event to bind to, you might do something like...
    //
    //     var transEndEventNames = {
    //       'WebkitTransition' : 'webkitTransitionEnd',
    //       'MozTransition'    : 'transitionend',
    //       'OTransition'      : 'oTransitionEnd',
    //       'msTransition'     : 'MSTransitionEnd',
    //       'transition'       : 'transitionend'
    //     },
    //     transEndEventName = transEndEventNames[ Modernizr.prefixed('transition') ];

    Modernizr.prefixed = function (prop, obj, elem) {
        if (!obj) {
            return testPropsAll(prop, 'pfx');
        } else {
            // Testing DOM property e.g. Modernizr.prefixed('requestAnimationFrame', window) // 'mozRequestAnimationFrame'
            return testPropsAll(prop, obj, elem);
        }
    };
    /*>>prefixed*/


    /*>>cssclasses*/
    // Remove "no-js" class from <html> element, if it exists:
    docElement.className = docElement.className.replace(/(^|\s)no-js(\s|$)/, '$1$2') +

    // Add the new classes to the <html> element.
                            (enableClasses ? ' js ' + classes.join(' ') : '');
    /*>>cssclasses*/

    return Modernizr;

})(this, this.document);


(function(modernizr) {
    modernizr.cssNthChildSupport = (function isNthChildSupported() {
        try {
            var result = false,
                test = document.createElement('ul'),
                style = document.createElement('style');
            test.setAttribute('id', 'nth-child-test');
            style.setAttribute('type', 'text/css');
            style.setAttribute('rel', 'stylesheet');
            style.setAttribute('id', 'nth-child-test-style');
            style.innerHTML = "#nth-child-test li:nth-child(even){height:10px;}";
            for (var i = 0; i < 3; i++) {
                test.appendChild(document.createElement('li'));
            }
            document.body.appendChild(test);
            document.head.appendChild(style);
            if (document.getElementById('nth-child-test').getElementsByTagName('li')[1].offsetHeight == 10) {
                result = true;
            }
            document.body.removeChild(document.getElementById('nth-child-test'));
            document.head.removeChild(document.getElementById('nth-child-test-style'));
            return result;
        } catch(ex)  {
            return false;
        }
    })();

    modernizr.load = function(arg) {
        window.yepnope(arg);
    };
})(Modernizr);
//ends: modernizr.js
//starts: FrameworkUtils.js
asFrameworkUtils = {};
//ends: FrameworkUtils.js
//starts: Promise.js
(
function (utils) {
    utils.SyncPromise = function(data) {
        var self = this;
        self.then = function(action) {
             if (action) {
                action(data);
            }
        };
        self.each = function() {
            return new utils.SyncAllPromise(data);
        };
    };

    utils.SyncAllPromise = function (data) {
        var self = this;
        self.then = function (action) {
            if (action) {
                for (var i in data) {
                    action(data[i]);
                }
            }
        };

        self.each = function () {
            var tempArray = new Array();
            for (var i in data) {
                for (var j in data[i]) {
                    tempArray.push(data[i][j]);
                }
            }
            return new utils.SyncAllPromise(tempArray);
        };
    };
})
(asFrameworkUtils);
//ends: Promise.js
//starts: tagUtil.js
function TagUtil(location) {
    var _me = this;
    _me.location = location ? location : window.location;

    this.isPage_BookingConfirm = function() {
        return this.pathStartsWith("//www.alaskaair.com/booking/payment")
            || this.pathStartsWith("//alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/booking/payment");
    };

    // no spanish version
    this.isPage_EasybizSignupConfirm = function() {
        return this.pathStartsWith("//easybiz.alaskaair.com/Enrollment/WelcomeConfirm");
    };

    this.isPage_MileagePlanSignupConfirm = function() {
        return this.pathStartsWith("http://www.alaskaair.com/www2/ssl/myalaskaair/MyAlaskaAir.aspx?isNewMember=true")
            || this.pathStartsWith("http://alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/www2/ssl/myalaskaair/MyAlaskaAir.aspx?isNewMember=true");
    };

    this.pathStartsWith = function(url) {
        return _me.isTesting || _me.startsWith(_me.location.href.replace(_me.location.protocol, "").toLowerCase(), url.toLowerCase());
    };

    _me.isPage_Home = function() {
        return _me.isPath('//www.alaskaair.com/') || _me.isPath('//alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com');
    };

    _me.isPage_Loyalty = function() {
        return ((_me.isPath('http://www.alaskaair.com/www2/ssl/myalaskaair/myalaskaair.aspx?CurrentForm=UCSignInStart', true)
                || _me.isPath('http://alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/www2/ssl/myalaskaair/myalaskaair.aspx?CurrentForm=UCSignInStart', true))
        //needed because the landing page has the same URL as the sign in page, this is the only way to differentiate.
                    && as.Page && as.Page.pageid != 'UCSignInStart'  
                    || _me.isPath('http://www.alaskaair.com/www2/ssl/myalaskaair/myalaskaair.aspx', true)
                    || _me.isPath('http://alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/www2/ssl/myalaskaair/myalaskaair.aspx', true)
                    );
    };

    _me.isPage_Loyalty_Booking = function () {
        return (_me.isPath('//www.alaskaair.com/Booking/SignIn') ||
            _me.isPath('//alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/Booking/SignIn'));
    };

    _me.isPage_ShoppingPath_Flight = function () {
        return (_me.isPath('//www.alaskaair.com/Shopping/Flights/Shop') ||
                _me.isPath('//www.alaskaair.com/Shopping/Flights/Price') ||
                _me.isPath('//www.alaskaair.com/Shopping/Flights/Calendar') ||                
            _me.isPath('//alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/Shopping/Flights/Shop') ||
            _me.isPath('//alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/Shopping/Flights/Price') ||
            _me.isPath('//alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/Shopping/Flights/Calendar'));
    };

    _me.isPage_ShoppingPath_Cart_Flight = function () {
        return (_me.isPath('//www.alaskaair.com/Shopping/Cart/AddFlight') ||
            _me.isPath('//alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/Shopping/Cart/AddFlight'));
    };

    _me.isPage_ShoppingPath_Confirmation = function() {
        return (_me.isPath('//www.alaskaair.com/booking/payment') ||
            _me.isPath('//alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/booking/payment'));
    };

    _me.isPath = function (url, isComparePath) {
        if (!isComparePath) {
            return _me.isTesting || (("//" + _me.location.hostname + _me.location.pathname).toLowerCase() == url.toLowerCase());
        }
        return _me.isTesting || (("//" + _me.location.hostname + _me.location.pathname + _me.location.search).toLowerCase() == url.toLowerCase());
    };


    this.startsWith = function(thisStr, str) {
        return str.length > 0 && thisStr.substring(0, str.length) === str;
    };

    this.insertImgBasedOnHttps = function(sslUrl, url) {
        if (_me.location.protocol == "https:")
            _me.insertImg(sslUrl);
        else
            _me.insertImg(url);
    };



    this.insertImg = function(url) {
        var img = document.createElement('img');
        img.setAttribute('alt', '');
        img.setAttribute('height', '1');
        img.setAttribute('width', '1');
        img.setAttribute('style', 'display: none;');
        img.setAttribute('src', url);
        document.body.appendChild(img);
    };
}

if (typeof (as) != "undefined") {
    as.tagUtil = new TagUtil();
}
//ends: tagUtil.js
//starts: PixelBootstrap.js


(function() {
    define('common/pixel', [], function() {
        var pixel = {};
        pixel.runAll = function() {
            for (var idx in pixel) {
                if (typeof (pixel[idx]) == "object" && pixel[idx].conditionalInsertPixel) {
                    try {
                        pixel[idx].conditionalInsertPixel();
                    } catch (ex) {
                        debugger;
                        //TODO: Should ajaxify the call to a logging system.
                    }
                }
            }
        };
        return pixel;
    });
})();
//ends: PixelBootstrap.js
//starts: BasePerPagePixel.js
(function() {
    define('common/basePerPagePixel', ['common/pixel', 'as'],
        function(pixel, as) {
            var insertPixel = function(url) {
                var img = document.createElement('img');
                img.setAttribute('alt', '');
                img.setAttribute('height', '1');
                img.setAttribute('width', '1');
                img.setAttribute('style', 'display: none;');
                img.setAttribute('src', url);
                document.body.appendChild(img);
            };

            pixel.BasePagePixel = function(pixelAndConditions, vendorName, defaultPreconditions) {
                var self = this;
                self.pixelAndConditions = pixelAndConditions;
                self.vendorName = vendorName;
                self.defaultPreconditions = defaultPreconditions;

                self.isPath =
                    // url -> no querystring
                    function(url, isStrict) {
                        if (!isStrict) {
                            return self.isTesting || (("//" + location.hostname + location.pathname).toLowerCase() == url.toLowerCase());
                        }
                        return self.isTesting || (("//" + location.hostname + location.pathname + location.search).toLowerCase() == url.toLowerCase());
                    };


                self.conditionalInsertPixel = function() {
                    var pixelLogic = pixelAndConditions;

                    //Check to make sure that the vendor site is up.
                    if (as != "undefined" && !as['Is' + vendorName + 'Down'] && (!defaultPreconditions || defaultPreconditions())) {
                        for (var i in pixelLogic) {
                            if (pixelLogic[i].condition instanceof Function) {
                                if (pixelLogic[i].condition.call(self, self)) {
                                    self.unpackUrl(pixelLogic[i].pixelUrl).then(insertPixel);
                                }
                            } else if (typeof pixelLogic[i].condition === 'string') {
                                if (self.isPath(pixelLogic[i].condition)) {
                                    self.unpackUrl(pixelLogic[i].pixelUrl).then(insertPixel);
                                }
                            }
                        }
                    }
                };

                self.unpackUrl = function(pixelUrl) {
                    var asUtils = asFrameworkUtils;
                    if (pixelUrl instanceof Function) {
                        var result = pixelUrl.call(self);
                        if (result instanceof Array)
                            return new asUtils.SyncPromise(result).each();
                        return new asUtils.SyncPromise();
                    } else if (pixelUrl instanceof Array) {
                        return new asUtils.SyncPromise(pixelUrl).each();
                    } else {
                        return new asUtils.SyncPromise(pixelUrl);
                    }
                };

            };

            return pixel.BasePagePixel;
        });
})();
//ends: BasePerPagePixel.js
//starts: AdaraPixel.js
(function() {
    define('common/adaraPixel', ['common/basePerPagePixel'], function(basePixel) {
        var AdaraPagePixelClass = function(pixelAndConditions, defaultPreconditions) {
            this.prototype.constructor.call(this, pixelAndConditions, 'Adara', defaultPreconditions);

            var self = this;

            self.generateSyncPixels = function(n) {
                var asUtils = asFrameworkUtils;
                var tempArray = new Array();
                for (var i = 1; i <= n; i++) {
                    tempArray.push('//tag.yieldoptimizer.com/ps/sync?t=i&p=1194&w=false&r=' + i);
                }
                return new asUtils.SyncPromise(tempArray).each();
            };

            self.isVisitorObjectAvailable = function() {
                return typeof (visitor) !== "undefined"
                    && visitor != null;
            };

            self.reformatStringDate4Adara = function(sDate) {
                var date = new Date(sDate);
                return sDate ? date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() : '';
            };

            self.isFlightInfoAvailable = function() {
                return as.Page
                    && (
                        as.Page.Cart
                            && as.Page.Cart.Itinerary
                            && as.Page.Cart.Itinerary.HasFlight
                            && as.Page.Cart.Itinerary.ItinerarySlices
                            && as.Page.Cart.Itinerary.ItinerarySlices.length > 0
                            && as.Page.Cart.Itinerary.ItinerarySlices[0].SliceSegments
                            && as.Page.Cart.Itinerary.ItinerarySlices[0].SliceSegments.length > 0
                            || as.Page.ShoppingSearch
                            && as.Page.ShoppingSearch.CityPairSlices
                            && as.Page.ShoppingSearch.CityPairSlices.length > 0);
            };

            self.isBookedServicesAvailable = function() {
                return as.Page
                    && as.Page.CP
                    && as.Page.CP.inCart;
            };

            self.isFormOfPaymentAvailable = function() {
                return as.Page
                    && as.Page.PP
                    && as.Page.PP.fop;
            };

            self.isRevenueAvailable = function() {
                return as.Page
                    && as.Page.Cart
                    && as.Page.Cart.Itinerary
                    && as.Page.Cart.Itinerary.Revenue;
            };

            self.getFlightInfo = function() {
                var departureDate = null;
                var returnDate = null;
                var flightInfo = null;
                if (self.isFlightInfoAvailable() && as.Page.Cart) {
                    var departureStartingSegment = as.Page.Cart.Itinerary.ItinerarySlices[0].SliceSegments[0];
                    var departureEndingSegment = as.Page.Cart.Itinerary.ItinerarySlices[0].SliceSegments[as.Page.Cart.Itinerary.ItinerarySlices[0].SliceSegments.length - 1];
                    var returningFlightSegment = as.Page.Cart.Itinerary.ItinerarySlices.length > 1 ?
                        as.Page.Cart.Itinerary.ItinerarySlices[as.Page.Cart.Itinerary.ItinerarySlices.length - 1]
                        .SliceSegments[as.Page.Cart.Itinerary.ItinerarySlices[as.Page.Cart.Itinerary.ItinerarySlices.length - 1].SliceSegments.length - 1] : null;
                    departureDate = departureStartingSegment ? self.reformatStringDate4Adara(departureStartingSegment.DepartureStationDate) : '';
                    returnDate = returningFlightSegment ? self.reformatStringDate4Adara(returningFlightSegment.DepartureStationDate) : '';
                    flightInfo = {
                        DepartureAirportCode: departureStartingSegment.DepartureStationCode,
                        ArrivalAirportCode: departureEndingSegment.ArrivalStationCode,
                        DepartureDate: departureDate,
                        ReturnDate: returnDate,
                        TravelersCount: as.Page.Cart.Itinerary.PassengersCount
                    };
                } else if (self.isFlightInfoAvailable() && as.Page.ShoppingSearch) {
                    var outgoingFlightInfo = as.Page.ShoppingSearch.CityPairSlices[0];
                    var lastSliceIdx = as.Page.ShoppingSearch.CityPairSlices.length - 1;
                    var returningFlightInfo = as.Page.ShoppingSearch.CityPairSlices.length > 1 ? as.Page.ShoppingSearch.CityPairSlices[lastSliceIdx] : {};

                    departureDate = outgoingFlightInfo ? self.reformatStringDate4Adara(outgoingFlightInfo.Date) : '';
                    returnDate = returningFlightInfo ? self.reformatStringDate4Adara(returningFlightInfo.Date) : '';
                    flightInfo = {
                        DepartureAirportCode: outgoingFlightInfo.DepartureShort,
                        ArrivalAirportCode: outgoingFlightInfo.ArrivalShort,
                        DepartureDate: departureDate,
                        ReturnDate: returnDate,
                        TravelersCount: as.Page.ShoppingSearch.TravelersCount
                    };
                }
                if (flightInfo) {
                    flightInfo.bookedService = self.isBookedServicesAvailable() ? escape(as.Page.CP.inCart) : '';
                    flightInfo.formOfPayment = self.isFormOfPaymentAvailable() ? escape(as.Page.PP.fop) : '';
                    flightInfo.revenue = self.isRevenueAvailable() ? as.Page.Cart.Itinerary.Revenue : '';
                }
                return flightInfo;
            };
        };


        AdaraPagePixelClass.prototype = basePixel;

        return AdaraPagePixelClass;
    });
})();
//ends: AdaraPixel.js
//starts: AdaraPixelInitializer.js
(function() {
    define('common/adaraPixelInitializer', ['common/adaraPixel', 'common/pixel', 'common/tagUtil'], 
    function(AdaraPagePixelClass,pixel,tagUtil) {

        pixel.AdaraPixel = new AdaraPagePixelClass(
            [
                {
                    condition: tagUtil.isPage_Home,
                    pixelUrl: [
                        '//tag.yieldoptimizer.com/ps/ps?t=i&p=1194&pg=hm&u=' + as.ResponsysCustContactId,
                        '//tag.yieldoptimizer.com/ps/sync?t=i&p=1194&w=false&r=1',
                        '//tag.yieldoptimizer.com/ps/sync?t=i&p=1194&w=false&r=2',
                        '//tag.yieldoptimizer.com/ps/sync?t=i&p=1194&w=false&r=3'
                    ]
                }, {
                    condition: function() {
                        return tagUtil.isPage_Loyalty()
                            && this.isVisitorObjectAvailable();
                    },
                    pixelUrl: function() {
                        var pixelUrls =
                        [
                            '//tag.yieldoptimizer.com/ps/ps?t=i&p=1194&pg=lyl&mlplv='
                            + (visitor.tier != null ? visitor.tier : '')
                            + '&sub='
                            + (visitor.hasInsiderSubscription != null ? visitor.hasInsiderSubscription : '')
                            + '&u='
                            + (as.ResponsysCustContactId != null ? as.ResponsysCustContactId : '')
                        ];
                        this.generateSyncPixels(3).then(function(url) { pixelUrls.push(url); });
                        return pixelUrls;
                    }
                },
                {
                    condition: function() {
                        return tagUtil.isPage_Loyalty_Booking()
                            && this.isVisitorObjectAvailable();
                    },
                    pixelUrl: function() {
                        var pixelUrls =
                        [
                            '//tag.yieldoptimizer.com/ps/ps?t=i&p=1194&pg=lyl_bk&mlplv='
                            + (visitor.tier != null ? visitor.tier : '')
                            + '&sub='
                            + (visitor.hasInsiderSubscription != null ? visitor.hasInsiderSubscription : '')
                            + '&u='
                            + (as.ResponsysCustContactId != null ? as.ResponsysCustContactId : '')
                        ];
                        this.generateSyncPixels(3).then(function(url) { pixelUrls.push(url); });
                        return pixelUrls;
                    }
                }, {
                    condition: function() {
                        return tagUtil.isPage_ShoppingPath_Flight()
                            && this.isVisitorObjectAvailable()
                            && this.isFlightInfoAvailable();
                    }, //TO
                    pixelUrl: function() {
                        var flightInfo = this.getFlightInfo();
                        var pixelUrls =
                        [
                            '//tag.yieldoptimizer.com/ps/ps?t=i&p=1194&pg=spth'
                            + '&soac='
                            + flightInfo.DepartureAirportCode //Origin Airport
                            + '&sdac='
                            + flightInfo.ArrivalAirportCode //Destination Airport
                            + '&sdpdt='
                            + flightInfo.DepartureDate //Searched departure date
                            + '&srdt='
                            + flightInfo.ReturnDate //Searched return date (If it exists)
                            + '&snopas='
                            + flightInfo.TravelersCount //Searched number of passengers
                            + '&mlplv='
                            + (visitor.tier != null ? visitor.tier : '') //Searched mileage plan tier
                            + '&u='
                            + (as.ResponsysCustContactId != null ? as.ResponsysCustContactId : '') //Hashed user id.
                            + '&mlpbal='
                            + visitor.mileagePlanBalance
                            //TODO: Do we need mileage plan balance?
                        ];
                        this.generateSyncPixels(3).then(function(url) { pixelUrls.push(url); });
                        return pixelUrls;
                    }
                }, {
                    condition: function() {
                        return tagUtil.isPage_ShoppingPath_Cart_Flight()
                            && this.isVisitorObjectAvailable()
                            && this.isFlightInfoAvailable();
                    }, //TO
                    pixelUrl: function() {
                        var flightInfo = this.getFlightInfo();
                        var pixelUrls =
                        [
                            '//tag.yieldoptimizer.com/ps/ps?t=i&p=1194&pg=spth_crt'
                            + '&soac='
                            + flightInfo.DepartureAirportCode //Origin Airport
                            + '&sdac='
                            + flightInfo.ArrivalAirportCode //Destination Airport
                            + '&sdpdt='
                            + flightInfo.DepartureDate //Searched departure date
                            + '&srdt='
                            + flightInfo.ReturnDate //Searched return date (If it exists)
                            + '&snopas='
                            + flightInfo.TravelersCount //Searched number of passengers
                            + '&mlplv='
                            + (visitor.tier != null ? visitor.tier : '') //Searched mileage plan tier
                            + '&u='
                            + (as.ResponsysCustContactId != null ? as.ResponsysCustContactId : '') //Hashed user id.
                            + '&mlpbal='
                            + visitor.mileagePlanBalance
                            //TODO: Do we need mileage plan balance?
                        ];
                        this.generateSyncPixels(3).then(function(url) { pixelUrls.push(url); });
                        return pixelUrls;
                    }
                }, {
                    condition: function() {
                        return tagUtil.isPage_ShoppingPath_Confirmation()
                            && this.isVisitorObjectAvailable()
                            && this.isFlightInfoAvailable();
                    }, //TO
                    pixelUrl: function() {

                        var flightInfo = this.getFlightInfo();
                        var pixelUrls =
                        [
                            '//tag.yieldoptimizer.com/ps/ps?t=i&p=1194&pg=cfm'
                            + '&boac='
                            + flightInfo.DepartureAirportCode //Origin Airport
                            + '&bdac='
                            + flightInfo.ArrivalAirportCode //Destination Airport
                            + '&bdpdt='
                            + flightInfo.DepartureDate //Searched departure date
                            + '&brdt='
                            + flightInfo.ReturnDate //Searched return date (If it exists)
                            + '&bnopas='
                            + flightInfo.TravelersCount //Searched number of passengers
                            + '&bsv='
                            + as.Page.CP.inCart //TODO: Add the booked services here.
                            + '&fopmt='
                            + as.Page.PP.fop //TODO: Add the form of payment here.
                            + '&bamt='
                            + as.Page.Cart.Itinerary.Revenue //TODO: Add the Revenue HERE
                            + '&mlplv='
                            + (visitor.tier != null ? visitor.tier : '') //Searched mileage plan tier
                            + '&u='
                            + (as.ResponsysCustContactId != null ? as.ResponsysCustContactId : '')
                            + '&mlpbal='
                            + visitor.mileagePlanBalance //Hashed user id.
                        ];
                        this.generateSyncPixels(3).then(function(url) { pixelUrls.push(url); });
                        return pixelUrls;
                    }
                }
            ],
            function() {
                var x = new VisitorRepository();
                x.PopulateVisitor();
                return as && as.tagUtil;
            }
        );
    });
})();
//ends: AdaraPixelInitializer.js
//starts: json2Loader.js
(function () {
    var JSON = window.JSON;
    if (JSON===undefined) {
        var ht = document.getElementsByTagName("head")[0],
            st = document.createElement('script');

        st.type = 'text/javascript';
        st.src = '../javascripts/common/json2.js'/*tpa=http://www.alaskaair.com/javascripts/common/json2.js*/;
        ht.appendChild(st);
    }
})();
//ends: json2Loader.js
//starts: preventXSF.js
if (top != self) { top.location = self.location; } //Prevents XSF
//ends: preventXSF.js
//starts: asglobal.js
var KeyCodeEnter = 13;
var KeyCodeSpace = 32;

function IsEnterKey(e) {
    var isEnter = false;
    if (GetKeyCode(e) == KeyCodeEnter) {
        isEnter = true;
    }
    return isEnter;     
} 

function GetKeyCode(e) {
    var keyID = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
    return keyID;
}

if (asglobal == null) {
   var asglobal = new Object();
}
SetDomainVariables();

function SetDomainVariables() {
    SetDomainUrl();
    //asglobal.domainIsSet is assigned value in an inline javascript code in Header.xslt
    if(asglobal.domainIsSet != null && asglobal.domainIsSet == false){
        SetDomain(); 
    }

    function SetDomain() {
        var domain = window.location.hostname.replace('www.', '');
        try {
            document.domain = domain; // This is for cross domain communication for Jenn and iSeatz's iFrames
            asglobal.domain = domain; //remember valid domain
        }
        catch (e) {
        //some iframe communication with parent window might fail but should not prevent other javascript code from working
        }
    }

    function SetDomainUrl() {

        asglobal.domainUrl = GetDomainUrl(GetHomePageUrl());

        function GetDomainUrl(homePageUrl) {
            var domainUrl = 'http://www.alaskaair.com/files/www.alaskaair.com'; //default, could different if spanish or another language
            var http = 'http://';
            var https = 'https://';
            //get domain url, it is home page url without the protocol
            if (homePageUrl != null && homePageUrl != '') {
                if (homePageUrl.indexOf(http) == 0) {
                    domainUrl = homePageUrl.replace(http, '');
                }
                else if (homePageUrl.indexOf(https) == 0) {
                    domainUrl = homePageUrl.replace(https, '');
                }
                else {
                    domainUrl = homePageUrl;
                }
            }
            return domainUrl;
        }

        function GetHomePageUrl() {
            var ENGLISH_MAINSITE_URL = 'http://www.alaskaair.com/files/www.alaskaair.com';
            var EASYBIZ_URL = 'http://www.alaskaair.com/files/easybiz.alaskaair.com';
            var SPANISH_SITE_URL = 'alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com';
            var homePageUrl = '';

            //get from logo
            var $homeLink = $('#aslogo a');
            if ($homeLink.length > 0) {
                var logoUrl = $homeLink.eq(0).attr('href').toLowerCase();
                if (logoUrl.indexOf(ENGLISH_MAINSITE_URL) > -1) {
                    homePageUrl = ENGLISH_MAINSITE_URL;
                }
                else if (logoUrl.indexOf(EASYBIZ_URL) > -1) {
                    homePageUrl = EASYBIZ_URL;
                }
                else if (logoUrl.indexOf(SPANISH_SITE_URL) > -1) {
                    homePageUrl = SPANISH_SITE_URL;
                }
                else {
                    homePageUrl = logoUrl.split("?")[0];
                }
                asglobal.logoUrl = logoUrl;
            }
            else {
                if (window.location.toString().indexOf(SPANISH_SITE_URL) > -1) {
                    homePageUrl = SPANISH_SITE_URL + '/'; 
                }
            }
            if(homePageUrl == null || homePageUrl == ''){
                homePageUrl = ENGLISH_MAINSITE_URL;
            }
            asglobal.homePageUrl = homePageUrl; //remember home page url, could be handy in some other places
            return homePageUrl;
        }
    }
}


$(document).ready(function () {

    // Setting focus on error message - fix for global Accessibility issue.
    var validationSummary = $('.errorTextSummary:first').eq(0);
    if (validationSummary) {                        
        if(validationSummary.css('display') != 'none')
            validationSummary.focus();
                }

    // Change href for "skip to main content" link for pages having left navigation
    var infoContentMain = $('.infoContentMain:first').get(0);
    if (infoContentMain) {
        var skipLink = $('#skip a');
        skipLink.attr('href', '#infoContentMain');
    }
});

/*
$(document).ready(function () {
    var cookieName = "alaskaairbrowsernotification";
    var cookieExists = readCookie(cookieName);
    //if cookie exists (meaning user has seen the notification today at least once already and closed it), then don't display the message again
    if (!cookieExists) {
        var ua = window.navigator.userAgent;

        //Check if browser is IE or not
        if (ua.search("MSIE") >= 0 && parseInt(getVersion("MSIE")) < 8) {
            //retrieve update message
            var message = $("#browserUpdateText").html();

            //create messagebox
            var messageBox = $("<div>").addClass("advisoryBanner").html(message);

            var closeIcon = $("<div id=\"buorgclose\">&times;</div>").css({
                position: "absolute",
                right: "18px",
                top: "12px",
                height: "27px",
                width: "19px",
                fontWeight: "bold",
                fontSize: "25px",
                padding: "0",
                cursor: "pointer"
            });

            messageBox.append(closeIcon);
            closeIcon.click(function () {
                messageBox.hide();
                //create cookie      
                createCookie(cookieName, "I EXIST", 1); //1 = 1 day = 1440 mins
            });
            //insert into the body of the webpage as the first element (so it'll appear at the top)
            $("body").prepend(messageBox);
        }
    }
});
*/

function getVersion(browserName) {
    var ua = window.navigator.userAgent;
    var nameIndex = ua.search(browserName);

    if (browserName === "MSIE") {
        var slashAfterNameIndex = ua.indexOf(" ", nameIndex); //find the index of the space directly after the browser name for IE
    } else {
        var slashAfterNameIndex = ua.indexOf("/", nameIndex); //find the index of the slash directly after the browser name
    }

    return ua.substring(slashAfterNameIndex + 1, ua.indexOf(".", nameIndex)); //take the substring between that slash and the next dot
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}
//ends: asglobal.js
//starts: topNav.js
function TopNav() {
    var _me = this;

    this.init = function () {
        _showLanguageLink();  // show english or spanish link
    }

    this.jumpToSpanishUrl = function () {
        location.href = _me.toSpanishUrl(location.href);
    }

    this.jumpToEnglishUrl = function () {
        location.href = _me.toEnglishUrl(location.href);
    }

    this.toEnglishUrl = function (spanishUrl) {
        if (!spanishUrl)
            return "../index.htm"/*tpa=http://www.alaskaair.com/*/;
        return spanishUrl.replace("//alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com", "../index.htm"/*tpa=http://www.alaskaair.com/*/);
    }

    this.toSpanishUrl = function (englishUrl) {
        if (!englishUrl)
            return "http://alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com";
        return englishUrl.replace("../index.htm"/*tpa=http://www.alaskaair.com/*/, "//alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com");
    }

    function _showLanguageLink() {
        if (location.hostname.toString() == 'http://www.alaskaair.com/files/alaskaair.convertlanguage.com') {
            if ($('#enEspanol'))
                $('#enEspanol').css('display', 'none');
            if ($('#english'))
                $('#english').css('display', 'block');
        }
    }
}

var as = window.as || {};

as.topNav = new TopNav();

$(document).ready(function () {
    as.topNav.init();

    // Set tabindex on target of Skip To Content link so IE knows it are focusable, and so Webkit browsers will focus() them (see below)
    $('#content').attr('tabindex', -1);

    // If there is a '#' in the URL (someone linking directly to a page with an anchor), set focus to that section (needed by Webkit browsers)
    if (document.location.hash && document.location.hash !== '#' && document.location.hash[1] !== '?') {
        var myAnchor = document.location.hash;
        setTimeout(function () {
            $(myAnchor).focus();
        }, 100);
    }
    // Set focus to targets of in-page links when clicked (needed by Webkit browsers)
    $("a[href^='#']").click(function (event) {
        var clickAnchor = this.href.split('#')[1];
        if (clickAnchor !== undefined && clickAnchor !== '') {
            setTimeout(function () {
                $("#" + clickAnchor).focus();
            }, 100);
        }
    });
});

//ends: topNav.js
//starts: authentication.js
function BoxDropDown(containerId, labelId, dropDownId, arrowId) {
    var _me = this;
    var _containerId = containerId;
    var _dropDownId = dropDownId;
    var _labelId = labelId;
    var _arrowId = arrowId;
    var _boxClass = "myAccountLabelWhiteBoxVisible";

    this.initForClick = function () {
        if ($('#' + _labelId).length) {
            $('#' + _labelId).click(function (e) {
                _me.toggle();
            });
        }
        if ($('#' + _containerId).length) {
            $('#' + _containerId).click(function (e) {
                e.stopPropagation();
            });
        }

        $(document).click(function (e) {
            _me.close();
        });

        // set up for sign in interstitial
        if ($('#myAccoutnSignInButton').length) {
            $('#myAccoutnSignInButton').click(function (e) {
                if ($('#greyInterstitialCover').length) {
                    $("#greyInterstitialCover").css("display", "block")
                }
            });
        }
    }

    this.initForHover = function () {
        if ($('#' + _containerId).length) {
            $('#' + _containerId).hover(function (e) {
                _me.toggle();
            });
        }
    }

    this.toggle = function () {
        var display = $('#' + _dropDownId).css('display');

        if (display == 'block') {
            display = 'none';
            if ($('#' + _arrowId).length) {
                $('#' + _arrowId).html('&#x25ba;&nbsp;');
            }
        }
        else {
            display = 'block';
            if ($('#' + _arrowId).length) {
                $('#' + _arrowId).html('&#x25bc;&nbsp;');
            }
            if ($('#greyInterstitialCover').length) {
                $("#greyInterstitialCover").css("display", "none");
            }
        }

        $('#' + _dropDownId).css('display', display);

        if ($('#' + _labelId).hasClass(_boxClass)) {
            $('#' + _labelId).removeClass(_boxClass);
        }
        else {
            $('#' + _labelId).addClass(_boxClass);
            $('#UserId').focus();
        }
    }

    this.close = function () {        
        $('#' + _dropDownId).css('display', 'none');
        if ($('#' + _arrowId).length) {
            $('#' + _arrowId).html('&#x25ba;&nbsp;');
        }
        $('#' + _labelId).removeClass(_boxClass);
    }
}

function Authentication() {
    this.init = function () {
        if ($('#myAccount').length) {
            new BoxDropDown("myAccount", "myAccountLabel", "myAccountDropDownDiv", "myAccountArrow").initForClick();
        }
        else if ($('#myAccountMenu').length) {
            new BoxDropDown("myAccountMenu", "myAccountLabel", "myAccountDropDownDiv", null).initForHover();
        }

        // need to set this so that the sign in webform won't throw up for dynamic browser check
        _setCookie("ASDBD", "J1C1", 1);        
    }

    this.forgotUserId = function (url) {
        _openWindowCenter(url, 'forgotuserid', '475', '425');
    }

    this.forgotPassword = function (url) {
        _openWindowCenter(url, 'forgotpassword', '475', '425');
    }

    function _setCookie(c_name, value, exdays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = escape(value) + ((exdays == null) ? "" : "; path=/; expires=" + exdate.toUTCString());
        document.cookie = c_name + "=" + c_value;
    }

    function _openWindowCenter(url, title, w, h) {
        var left = (screen.width / 2) - (w / 2);
        var top = (screen.height / 2) - (h / 2);
        var features = 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, '
        + 'copyhistory=no, ' + 'width=' + w + ', height=' + h + ', top=' + top + ', left=' + left;

        var targetWin = open(url, title, features);
    }
}

as.authentication = new Authentication();

$(document).ready(function () {
    as.authentication.init();
});


$(function () {
    $('#myAccount input').keydown(function (e) {
        if (e.keyCode == 13) {           
            if ($('#greyInterstitialCover').length) {
                $("#greyInterstitialCover").css("display", "block")
            }
            $(this).parents('form').submit();
            return false;
        }
    });
});

//ends: authentication.js
//starts: visitor.js
var Tier = { UNKNOWN: 0, STANDARD: 1, MVP: 2, GOLD: 3, GOLD75: 4 };
var Country = { UNKNOWN: "unknown", US: "united states", CA: "canada" };
var CreditCardType = { UNKNOWN: "unknown", VISA: "BAAS" };

var visitor = {
    country: Country.UNKNOWN,
    tier: Tier.UNKNOWN,
    creditCard: CreditCardType.UNKNOWN, // unknown, BAAS 
    destination: "unknown", // unknown, SEA, LAS,  PDX

    referrerUrl: "",  // http://www.google.com/search?q=alaska+airline+cheap&ie=utf-8&oe=utf-8&aq=t&rls=org.mozilla:en-US:official&client=firefox-a&safe=active    
    isMileagePlanMember: false,
    hasInsiderSubscription: false,
    isBoardroomMember: false,
    familySize: 0
};

function VisitorRepository() {
    var _v = visitor;

    this.PopulateVisitor = function() {
        _v.referrerUrl = (typeof document.referrer != "undefined") ? document.referrer : "";

        // parse cookies
        var cookie = unescape(document.cookie);
        _v.destination = _parseDestination(cookie);
        _v.tier = _parseTier(cookie);
        _v.isMileagePlanMember = _v.tier > 0;
        _v.familySize = _parseFamilySize(cookie);
        _v.creditCard = _parseCreditCard(cookie);
        _v.hasInsiderSubscription = _parseHasInsiderSubscription(cookie);
        _v.isBoardroomMember = _parseIsBoardroomMember(cookie);
        _v.mileagePlanBalance = _parseForMileagePlanBalance(cookie);

        if (typeof testAndTargetUser == "object") {
            _overwriteObject(_v, testAndTargetUser); // set country using test and target geo location service
        }

        return _v;
    };

    function _parseForMileagePlanBalance(cookieString) {
        return _extract(cookieString, 'MemberMpBalance=', ';','');
    }

    function _overwriteObject(oldJson, newJson) {
        for (prop in newJson) {
            if (newJson.hasOwnProperty(prop) && oldJson.hasOwnProperty(prop)) {
                oldJson[prop] = newJson[prop];
            }
        }
    };

    function _parseDestination(cookie) {
        return _parseCookie(cookie, "AS_dest=");
    }

    function _parseTier(cookie) {
        var tier = _parseCookie(cookie, "AS_mbr=");
        if (tier.indexOf("GOLD75K") >= 0)
            return Tier.GOLD75;
        if (tier.indexOf("GOLD") >= 0)
            return Tier.GOLD;
        if (tier.indexOf("MVP") >= 0)
            return Tier.MVP;
        if (tier.indexOf("STANDARD") >= 0)
            return Tier.STANDARD;
        return Tier.UNKNOWN;
    }

    function _parseFamilySize(cookie) {
        var size = _parseCookie(cookie, "AS_fam=");

        if (size == "ZERO" || size == "unknown")
            size = "0";
        return parseInt(size);
    }

    function _parseCreditCard(cookie) {
        return _parseCookie(cookie, "AS_card=");
    }

    function _parseHasInsiderSubscription(cookie) {
        var subscriptions = _parseCookie(cookie, "AS_Subscrx=");
        return subscriptions.indexOf("InsiderNewsletter") >= 0;
    }

    function _parseIsBoardroomMember(cookie) {
        var subscriptions = _parseCookie(cookie, "AS_BR=");
        return subscriptions.indexOf("ACTIVE") == 0;
    }

    function _parseCookie(cookie, indexString) {
        return _extract(cookie, indexString, "|", "unknown");
    }

    function _extract(cookie, indexString, endString, defaultValue) {
        var x = cookie.indexOf(indexString);
        if (x < 0)
            return defaultValue;

        var result = cookie.substring(x + indexString.length);

        var y = result.indexOf(endString);
        if (y <= 0)
            return defaultValue;

        result = result.substring(0, y);
        if (!result || result == "")
            return defaultValue;

        return result;
    }
}

//ends: visitor.js
//starts: testUtil.js
function TestUtil() {
    var _me = this;
    this.debugging = false;

    // for testing: overwrite visitor properties based on URL query string
    this.overwriteVisitor = function (visitor) {
        if (window.location.search.indexOf("visitor=") > 0) {
            var params = this.getQuerystringParams(window.location.search);
            var newVisitor = jQuery.parseJSON(params["visitor"]);

            for (prop in newVisitor) {
                if (newVisitor.hasOwnProperty(prop) && visitor.hasOwnProperty(prop)) {
                    visitor[prop] = newVisitor[prop];
                }
            }
        }
    };

    this.debugJson = function (jsonObj) {
        if (_me.debugging || window.location.search.indexOf("debug=1") > 0) {
            this.debug(this.jsonToString(jsonObj) + "\r\n\r\n");
        }
    };

    this.debug = function (message) {
        if (_me.debugging || window.location.search.indexOf("debug=1") > 0) {
            if ($("#debugText").length == 0) {
                $("body").append("<textarea id='debugText' rows='20' cols='175' readonly='yes'></textarea>");
            }
                        
            $("#debugText").text($("#debugText").text() + message);
        }
    };

    this.jsonToString = function (jsonObj) {
        var s = "{"
        for (var p in jsonObj) {
            if (!jsonObj.hasOwnProperty(p)) {
                continue;
            }
            if (s.length > 1) {
                s += ',';
            }
            s += '' + p + ':' + jsonObj[p] + '';
        }

        return s + "}";
    }

    this.getQuerystringParams = function(querystring) {
        var qs = unescape(querystring);

        // document.location.search is empty if no query string
        if (!qs) {
            return {};
        }

        // Remove the '?' via substring(1)
        if (qs.substring(0, 1) == "?") {
            qs = qs.substring(1);
        }

        // Load the key/values of the return collection
        var qsDictionary = {};

        // '&' seperates key/value pairs
        var pairs = qs.split("&");
        for (var i = 0; i < pairs.length; i++) {
            var keyValuePair = pairs[i].split("=");
            qsDictionary[keyValuePair[0]] = keyValuePair[1];
        }

        // Return the key/value dictionary
        return qsDictionary;
    }
}
//ends: testUtil.js
//starts: maxymiserRepository.js
function MaxymiserRepository() {
    this.SetPersCriterions = function () {
        var testUtil = new TestUtil();
        var visitorRepository = new VisitorRepository();
        var v = visitorRepository.PopulateVisitor();
        testUtil.overwriteVisitor(v);
        testUtil.debugJson(v);
        
        mmcore.SetPersCriterion("tier", v.tier.toString());
        mmcore.SetPersCriterion("hasInsider", v.hasInsiderSubscription.toString().toUpperCase());
        mmcore.SetPersCriterion("hasBoardroom", v.isBoardroomMember.toString().toUpperCase());
        mmcore.SetPersCriterion("creditCard", v.creditCard);
    };
}
//ends: maxymiserRepository.js
//starts: asdropdownnav.js
var navigationMenu = new NavigationMenu();
function NavigationMenu() {
    var me = this;
    var currentFlexMenuId = "";
    this.SetupMenuDone = false;
    this.SelectTab = function (navTab) {
        $(navTab).removeClass('navTab');
        $(navTab).addClass('navWhiteBox');
        me.ShowMenu(navTab, $("http://www.alaskaair.com/files/a.nav", navTab).attr("data-flexmenu"));
    }
    this.SelectTabNoSubNav = function (navTab) {
        $(navTab).removeClass('navTab');
        $(navTab).addClass('navWhiteBox');
        me.ShowMenuNoSubNav(navTab, $("http://www.alaskaair.com/files/a.nav", navTab).attr("data-flexmenu"));

        $(".tabContainer").css("position", "relative");
        $(".tabContainer").css("z-index", "10000");
    }
    this.DeSelectTab = function (navTab) {
        if (!$(navTab).hasClass("selectedTab")) {
            if ($(navTab).hasClass("navWhiteBox")) $(navTab).removeClass('navWhiteBox');
            $(navTab).addClass('navTab');
        }

        me.ToggleTab($(".navBarTab.toggled"));

        $(".flexdropdownmenu").css({ display: "none" });

        $(".tabContainer").css("position", "");
       }

    this.DeSelectAllTabs = function () {
       	$(".navBarTab").each(function () {
       		me.DeSelectTab(this);
       	});
    }

    this.ToggleTab = function (navTab) {
        
        if ($(navTab).hasClass("selectedTab")) {
            $(navTab).removeClass('selectedTab');
            if ($(navTab).hasClass("navWhiteBox")) $(navTab).removeClass('navWhiteBox');
            $(navTab).addClass('navTab');
            $(navTab).addClass('toggled');
        }
        else {
            $(navTab).removeClass('navTab');
            $(navTab).addClass('navWhiteBox');
            $(navTab).addClass('selectedTab');
            $(navTab).removeClass('toggled');
        }

    }

    this.ShowMenu = function (navTab, flexMenuId) {
        var flexMenu = $("#" + flexMenuId);
        if ($(navTab).length > 0) {
            $(flexMenu).css({ position: "absolute" });
            $(flexMenu).css({ "z-index": 10000 });
            var addToTop = 1;
            $(flexMenu).css({ top: $(navTab).offset().top + $(navTab).height() + addToTop });
            $(flexMenu).css({ left: $(navTab).offset().left - $(navTab).offsetParent().offset().left - 1 });
        }
        var duration = 0;

        if (currentFlexMenuId == flexMenuId) {
            duration = 0;
        }
        if ($(flexMenu).css("display") == "none") {
            //$("#divLog").append("<div>" + flexMenuId + "</div>");
            $(flexMenu).slideDown(duration);
        }
        currentFlexMenuId = flexMenuId;
    }


    this.ShowMenuNoSubNav = function (navTab, flexMenuId) {
    	var flexMenu = $("#" + flexMenuId);
    	var navBar = $(".navBar");
    	var advBanner = $("#Advisories");
    	if ($(navTab).length > 0) {
    		$(flexMenu).css({ position: "absolute" });
    		$(flexMenu).css({ "z-index": 9999999 });
    		$(flexMenu).css({ top: $(navBar).outerHeight() - 5, left: $(navBar).offset().left - $(navTab).offset().left + 8 });
    		$(flexMenu).css("width", $(".contentMain").width() - 60);
    	}
    	var duration = 0;

    	if (currentFlexMenuId == flexMenuId) {
    		duration = 0;
    	}

    	var flexMenuLinks = $("[id^=" + flexMenuId + "]");

    	if ($(flexMenu).css("display") == "none") {
    		//$("#divLog").append("<div>" + flexMenuId + "</div>");
    		$(flexMenuLinks).show();
    		$(flexMenu).slideDown(duration);
    	}
    	currentFlexMenuId = flexMenuId;

    	if ($(".navBarTab.selectedTab").attr("id") != $(navTab).attr("id")) {
    		me.ToggleTab($(".navBarTab.selectedTab"));
    	}

    }


    this.CancelBubble = function (e) {
        if (!e) var e = window.event;
        e.cancelBubble = true;
        if (e.stopPropagation) e.stopPropagation();
    }

    $(document).ready(function () {

    	// Fix for hoverstate causing users to double tap on nav links using IOS devices. 
    	// See http://stackoverflow.com/questions/3038898/ipad-iphone-hover-problem-causes-the-user-to-double-click-a-link
    	$(".navBar .listitem a").bind("touchend", function (e) {
    		window.location = $(this).attr("href");
    	});

    	// Show menus with touch events.
    	$(".navBar .tabContainer").bind("touchstart", function (e) {
    		var menuId = $("http://www.alaskaair.com/files/a.nav", this).attr("data-flexmenu");
    		if (menuId != currentFlexMenuId && $("#" + menuId).length > 0) {
    			e.preventDefault();
    			me.DeSelectAllTabs();
    			me.SelectTabNoSubNav($(".navBarTab", this));
    		}
    	});
    });

}

//ends: asdropdownnav.js
//starts: lightbox.js

//start widget containers
var WidgetContainers = { Selectors: { V: "#vshopping-container", H: "#hshopping-container", P: "#pshopping-container"} };
//end widget containers

//Start Lightbox Code
var POPUP_OUTER_DIV_CLASS = "EzPopopOuter";
var POPUP_INNER_DIV_CLASS = "EzPopopInner";
var POPUP_CLOSE_BTN_CLASS = "EzPopopClsBtn";
var CLOSE_ANCHOR_ID = "closeAnchorId";
var lastFocus;

if (typeof $.browser == 'undefined') {
    $.browser = {};
    (function () {
        $.browser.msie = false;
        $.browser.version = 0;
        if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
            $.browser.msie = true;
            $.browser.version = RegExp.$1;
        }
    })();
}

window.onresize = function () {
    if ($.browser.msie) {
        if ($("#" + FORM_FILLER_ID).length != 0 && $("#" + FORM_FILLER_ID).css("display").toLowerCase() != "none") {
            $.showFormFiller();
        }
        if ($("#" + PROCESSING_DIV_ID).length != 0 && $("#" + PROCESSING_DIV_ID).css("display").toLowerCase() != "none") {
            $.showProcessingBar();
        }
    }
};

jQuery.fn.positionElement = function (param) {
    var top = (param != null && param.top != null && !isNaN(param.top)) ? param.top : 0;
    var left = (param != null && param.left != null && !isNaN(param.left)) ? param.left : 0;
    var duration = param != null && param.duration != null ? param.duration : "slow";
    this.css({ left: left }).css({ top: top });
    this.show(duration);
};

var FORM_FILLER_ID = "divFormFiller";
jQuery.showFormFiller = function (closeOnClick, onClose) {
    if ($("#" + FORM_FILLER_ID).length == 0) {
        var parentElement = $("body");
        if (parentElement.length == 0) {
            parentElement = $("div").first();
        }


        parentElement.append("<iframe id='iFiller' src='javascript:false;' name='iFiller'></iframe><div id='" + FORM_FILLER_ID + "'></div>");

        $("#" + FORM_FILLER_ID).css({ "background-color": "silver" }).css({ "display": "none" }).css({ "z-index": "500" }).css({ "filter": "alpha(opacity = 70)" });
        $("#" + FORM_FILLER_ID).css({ "-moz-opacity": "0.7" }).css({ "-khtml-opacity": "0.7" }).css({ "opacity": "0.7" });
        $("#iFiller").css({ "display": "none", "z-index": "49", border: 0, margin: 0, "-moz-opacity": "0", "-khtml-opacity": "0", "opacity": "0" });
        if (!$.browser.msie) {
            $("#" + FORM_FILLER_ID).css({ "position": "fixed", "top": "0px", "bottom": "0px", "left": "0px", "right": "0px" });
            $("#iFiller").css({ "position": "fixed", "top": "0px", "bottom": "0px", "left": "0px", "right": "0px" });
        }
        else {
            $("#iFiller").css({ "position": "absolute" });
            $("#" + FORM_FILLER_ID).css({ "position": "absolute" });
        }
    }
    var height = lightboxHelper.GetTotalHeight();
    var width = lightboxHelper.GetTotalWidth();
    if ($.browser.msie) {
        $("#" + FORM_FILLER_ID).css({ height: height, width: width - 17, top: 0, left: 0 });
    }
    $("#iFiller").css({ height: height - 100, width: width - 100, top: 0, left: 0 });
    $("#" + FORM_FILLER_ID).show();
    $("#iFiller").show();
    $("#" + FORM_FILLER_ID).unbind("click");
    $("#" + FORM_FILLER_ID).bind("click", function () {
        if (closeOnClick == null || closeOnClick == true) {
            $.closeEzPopoups();
            if (onClose != null && typeof onClose == "function") {
                try {
                    onClose();
                }
                catch (e) {
                }
            }
        }
    });

};

jQuery.showInterstitialFormFiller = function (closeOnClick) {
    var fillerTop = $(window).scrollTop();
    if ($("#" + FORM_FILLER_ID).length == 0) {
        var parentElement = $("body");
        if (parentElement.length == 0) {
            parentElement = $("div").first();
        }
        parentElement.append("<iframe id='iFiller' src='javascript:false;' name='iFiller'></iframe><div id='" + FORM_FILLER_ID + "'></div>");

        $("#" + FORM_FILLER_ID).css({ "background": "#f2f5fa", "z-index": "500" });

        $("#iFiller").css({ "display": "none", "z-index": "49", border: 0, margin: 0, "-moz-opacity": "0", "-khtml-opacity": "0", "opacity": "0" });
        if (!$.browser.msie) {
            $("#" + FORM_FILLER_ID).css({ "position": "fixed", "top": "0px", "bottom": "0px", "left": "0px", "right": "0px" });
            $("#iFiller").css({ "position": "fixed", "top": "0px", "bottom": "0px", "left": "0px", "right": "0px" });
        }
        else {
            $("#iFiller").css({ "position": "absolute", "top": fillerTop + "px" });
            $("#" + FORM_FILLER_ID).css({ "position": "absolute", "top": fillerTop + "px" });
        }
    }

    var height = lightboxHelper.GetTotalHeight();
    var width = lightboxHelper.GetTotalWidth();
    if ($.browser.msie) {
        $("#" + FORM_FILLER_ID).css({ height: height, width: width, top: fillerTop, left: 0 });
    }
    $("#iFiller").css({ height: height - 100, width: width - 100, top: fillerTop, left: 0 });
    $("#" + FORM_FILLER_ID).show();
    $("#iFiller").show();
    $("#" + FORM_FILLER_ID).unbind("click");
    $("#" + FORM_FILLER_ID).bind("click", function () {
        if (closeOnClick == null || closeOnClick == true) {
            $.closeEzPopoups();
        }
    });

};

var lightboxHelper = new LightboxHelper();
function LightboxHelper() {
    this.KeyPressedEventBound = false;
    this.GetVisibleHeight = function () {
        return $(document).height() > $(window).height() ? $(window).height() : $(document).height();
    };

    this.GetTotalHeight = function () {
        return $(document).height() < $(window).height() ? $(window).height() : $(document).height();
    };

    this.GetVisibleWidth = function () {
        return $(document).width() > $(window).width() ? $(window).width() : $(document).width();
    };

    this.GetTotalWidth = function () {
        return $(document).width() < $(window).width() ? $(window).width() : $(document).width();
    };
    this.GetIdUniqueSuffix = function () {
        var d = new Date();
        var curr_hour = d.getHours();
        var curr_min = d.getMinutes();
        var curr_sec = d.getSeconds();

        return curr_hour + "_" + curr_min + "_" + curr_sec + Math.floor(Math.random() * 1111);
    };
}

jQuery.hideFormFiller = function () {
    $("#iFiller").hide();
    $("#" + FORM_FILLER_ID).hide();
};

jQuery.fn.makeRoundCorner = function (param) {
    var cornerRadius = param != null && param.cornerRadius != null ? param.cornerRadius : 5;
    var showShadow = param != null && param.showShadow != null ? param.showShadow : false;
    var boxShadowStyle = param != null && param.boxShadowStyle != null ? param.boxShadowStyle : "3px 3px 3px #999";
    this.css({ "-moz-border-radius": cornerRadius }).css({ "-webkit-border-radius": cornerRadius }).css({ "border-radius": cornerRadius });
    if (showShadow == true) {
        this.css({ "box-shadow": boxShadowStyle }).css({ "-webkit-box-shadow": boxShadowStyle }).css({ "-moz-box-shadow": boxShadowStyle });
    }
    return this;
};

jQuery.fn.formatEzTable = function (param) {
    var tables = this;
    tables.each(function (i) {
        $(this).attr({ cellpadding: 6 }).attr({ cellspacing: 0 });
        $(this).css("border-left", "solid 1px #656565");
        $(this).css("border-top", "solid 1px #656565");
        $("td", $(this)).css("border-right", "solid 1px #656565");
        $("td", $(this)).css("border-bottom", "solid 1px #656565");
        $("th", $(this)).css("border-right", "solid 1px #656565");
        $("th", $(this)).css("border-bottom", "solid 1px #656565");
        var visibleRowNumber = 0;
        $("tbody > tr", this).each(function (j) {
            if ($(this).css("display").toLowerCase() != "none") {
                visibleRowNumber++;
                if (visibleRowNumber == 1) {
                    $(this).css("background-color", "#E6EDF6");
                }
                else {
                    if ((visibleRowNumber % 2) == 1) {
                        $(this).css("background-color", "#E6EDF6");
                    }
                    else {
                        $(this).css("background-color", "white");
                    }
                }
            }
        });

    });
};


jQuery.closeEzPopoups = function (noDelay) {
    noDelay = noDelay == null ? false : noDelay;
    if (noDelay === true) {
        $(".EzPopopOuter").hide();
        $.hideFormFiller();
    }
    else {
        $(".EzPopopOuter").hide("slow", function () {
            $.hideFormFiller();
        });
    }
};

jQuery.fn.centerEz = function (param) {
    var baseWidth = (param != null && param.baseWidth != null && !isNaN(param.baseWidth)) ? param.baseWidth : $(document).width();
    var duration = param.duration;
    if (isNaN(baseWidth) || baseWidth == null || baseWidth == 0) {
        baseWidth = $(document).width();
    }
    var baseHeight = lightboxHelper.GetVisibleHeight();
    var top = (baseHeight - this.height()) / 2 + $(document).scrollTop();
    var left = (baseWidth - this.width()) / 2 + $(document).scrollLeft();
    var minTop = 7;
    if (top < minTop) top = minTop;
    var minLeft = 0;
    if (left < minLeft) left = minLeft;
    this.css({ position: "absolute" });
    this.positionElement({ top: top, left: left, duration: duration });
    return this;
};

jQuery.fn.showEzPopup = function (param) {

    var top = (param != null && param.top != null && !isNaN(param.top)) ? param.top : 0;
    var left = (param != null && param.left != null && !isNaN(param.left)) ? param.left : 0;
    var centerOnPage = (param != null && param.centerOnPage != null) ? param.centerOnPage : false;
    var duration = 0; //ignore for now, param != null && param.duration != null ? param.duration : "slow";
    var baseWidth = param.baseWidth;
    if (centerOnPage == true) {
        if (baseWidth != null && baseWidth > 0) {
            this.centerEz({ baseWidth: baseWidth, duration: duration });
        }
        else {
            this.centerEz({ duration: duration });
        }
    }
    else {
        if (top != 0 && left != 0) {
            $(this).positionElement({ left: left, top: top, duration: duration });
        }
        else {
            $(this).css({ position: "absolute" }).show(duration);
        }
    }
};

jQuery.fn.hideEzPopup = function () {
    this.hide();
    $.hideFormFiller();
};

// postify.js
// Converts an object to an ASP.NET MVC  model-binding-friendly format
// Author: Nick Riggs
// http://www.nickriggs.com

$.postify = function (value) {
    var result = {};

    var buildResult = function (object, prefix) {
        for (var key in object) {

            var postKey = isFinite(key)
                ? (prefix != "" ? prefix : "") + "[" + key + "]"
                : (prefix != "" ? prefix + "." : "") + key;

            switch (typeof (object[key])) {
                case "number": case "string": case "boolean":
                    result[postKey] = object[key];
                    break;

                case "object":
                    if (object[key].toUTCString)
                        result[postKey] = object[key].toUTCString().replace("UTC", "GMT");
                    else {
                        buildResult(object[key], postKey != "" ? postKey : key);
                    }
            }
        }
    };

    buildResult(value, "");

    return result;
};

jQuery.fn.disableEnableInputControls = function (willDisable) {
    if (this == null) {
        $("input").attr({ disabled: willDisable });
        $("select").attr({ disabled: willDisable });
        $("textarea").attr({ disabled: willDisable });
    }
    else {
        $("input", this).attr({ disabled: willDisable });
        $("select", this).attr({ disabled: willDisable });
        $("textarea", this).attr({ disabled: willDisable });
    }
};

jQuery.showValidationMessages = function () {
    $(".redtext").show();
    //$(".redtext").hide().toggle("pulsate", null, 750);
};

jQuery.hideValidationMessages = function () {
    $(".redtext").hide();
};

jQuery.showActionMessage = function (message, isError, param) {
    if (message == '') {
        $.hideActionMessage();
    }
    else {
        var top = param != null && param.top != null ? param.top : 150;
        var left = param != null && param.left != null ? param.left : 170;
        if (isError == null) {
            isError = true;
        }
        if (isError) {
            $("#" + MESSAGE_TEXT_DIV_ID).css({ "color": 'red' }).html(message);
        }
        else {
            $("#" + MESSAGE_TEXT_DIV_ID).css({ "color": '#54112b' }).html(message).addClass('successTextSummary');
        }
        $("#" + MESSAGE_CONTAINER_DIV_ID).positionElement({ top: top, left: left, duration: "fast" });
    }
};

jQuery.hideActionMessage = function () {
    $("#" + MESSAGE_CONTAINER_DIV_ID).hide();
};

jQuery.showProcessingBar = function () {
    var baseWidth = lightboxHelper.GetVisibleWidth();
    var baseHeight = lightboxHelper.GetVisibleHeight();
    var top = (baseHeight - $("#" + PROCESSING_DIV_ID).height()) / 2 + $(document).scrollTop();
    var left = (baseWidth - $("#" + PROCESSING_DIV_ID).width()) / 2 + $(document).scrollLeft();

    var divProcessing = $("#" + PROCESSING_DIV_ID);
    var parentElement = $("body");
    if ($(divProcessing).length == 0) {
        var html = "<div style='box-sizing: content-box; border:solid 2px #00245A; height:100px; width:100px; display:none; z-index:10000;background-color:white; padding:5px;' id='" + PROCESSING_DIV_ID + "'>";
        html += "<div style='font-family:Arial; font-size:12px;color: #000066;font-weight:bold;'><img src='https://www.alaskaair.com/images/ajax-loader_b.gif' style='position: absolute; margin-top: -2px;z-index:10002;' /><div style='padding:35px 0 0 20px;z-index:10001;'>Loading...</div></div>";
        html += "</div>";

        if (parentElement.length == 0) {
            parentElement = $("div").first();
        }
        parentElement.append(html);

        divProcessing = $("#" + PROCESSING_DIV_ID);
        $(divProcessing).css({ "position": "absolute" }).css({ top: 0 }).css({ left: 0 });
        $(divProcessing).makeRoundCorner(10);
    }

    $.showFormFiller(false);
    $("#" + PROCESSING_DIV_ID).css({ left: left }).css({ top: top }).show();


    $(document).scrollTop($(document).scrollTop() + 1);
};

jQuery.scroll1pxDown = function () {
    $(document).scrollTop($(document).scrollTop() + 1);
};

jQuery.hideProcessingBar = function () {
    $.hideFormFiller();
    $("#" + PROCESSING_DIV_ID).hide();
};

var PROCESSING_DIV_ID = "divProcessing";
var MESSAGE_CONTAINER_DIV_ID = "divMessageContainer";
var MESSAGE_TEXT_DIV_ID = "divMessageText";
var CLOSE_MESSAGE_BAR_DIV_ID = "divCloseMessageBar";
var SESSION_TIMEDOUT_DIV_ID = "divSessionTimedOut";
var MY_ACCOUNT_TIMEDOUT_DIV_ID = "divMyAccountSessionTimedOut";
jQuery.createMessageElements = function () {
    //alert('>Lightbox.otheres.js jQuery.createMessageElements');
    var parentElement = $("body");

    var divProcessing = $("#" + PROCESSING_DIV_ID);
    if ($(divProcessing).length == 0) {
        var html = "<div style='border:solid 2px #00245A; height:100px; width:100px; display:none; z-index:10000;background-color:white; padding:5px;' id='" + PROCESSING_DIV_ID + "'>";
        html += "<div style='font-family:Arial; font-size:10px;'><img src='https://www.alaskaair.com/images/ajax-loader.gif' style='position: absolute; margin-top: -2px;' /></div>";
        html += "</div>"

        if (parentElement.length == 0) {
            parentElement = $("div").first();
        }
        parentElement.append(html);

        divProcessing = $("#" + PROCESSING_DIV_ID);
        $(divProcessing).css({ "position": "absolute" }).css({ top: 0 }).css({ left: 0 });
        $(divProcessing).makeRoundCorner(10);
    }

    var divMessageContainer = $("#" + MESSAGE_CONTAINER_DIV_ID);
    if ($(divMessageContainer).length == 0) {
        var html = "";
        html += "<div id='" + MESSAGE_CONTAINER_DIV_ID + "' style='display:none;position:absolute;'>";
        html += "<div id='" + MESSAGE_TEXT_DIV_ID + "' style='float: left; text-align: center;padding-top:5px; font-weight:bold;'></div>";
        html += "<div id='" + CLOSE_MESSAGE_BAR_DIV_ID + "' style='float: left;z-index:20000; cursor:pointer; padding-left: 10px;'>";
        html += "	<img src='https://www.alaskaair.com/images/Popup_Close_X.png' alt='Close' tabindex='0' />";
        html += "</div>";

        if (parentElement.length == 0) {
            parentElement = $("div").first();
        }
        parentElement.append(html);

        var divCloseMessageBar = $("#" + CLOSE_MESSAGE_BAR_DIV_ID);
        $(divCloseMessageBar).click(function () {
            $.hideActionMessage();
        });
        $(divCloseMessageBar).css({ "padding-left": "10px" }).hide(); //don't show for now
    }

    var divSessionTimedOut = $("#" + SESSION_TIMEDOUT_DIV_ID);
    if ($(divSessionTimedOut).length == 0) {
        var html = "";
        html += "<div id='divSessionTimedOut' style='display:none;'>";
        html += "	Your session has timed out. You will be redirected to the signin page.<br />";
        html += "	<br />";
        html += "	Please wait...<img alt='' src='../images/interstitial-animated-dots.gif'/*tpa=https://www.alaskaair.com/images/interstitial-animated-dots.gif*/ />";
        html += "</div>";
        if (parentElement.length == 0) {
            parentElement = $("div").first();
        }
        parentElement.append(html);
    }

    if ($.browser.msie) {
        var screenTop = $(document).scrollTop();
        divProcessing.css({ top: screenTop }).css({ left: 0 });
    }


};

var SIGNIN_REQUIRED = "SigninRequired";
var ERROR_ENCOUNTERED = "Error was encountered, please try again.";
var REQUEST_SUCCESSFUL = "Your request was successfully processed.";
var REQUEST_TIMED_OUT = "Your request timed out, please try again.";

function afterPost(msg) {
    if (msg == SIGNIN_REQUIRED || msg.indexOf(SESSION_TIMEDOUT_DIV_ID) > 0 || msg.indexOf(MY_ACCOUNT_TIMEDOUT_DIV_ID) > 0) {
        redirectToSignInPage();
        return false;
    }
    else {
        return true;
    }
}

function redirectToSignInPage() {
    $.hideLightBoxes();
    $("#divSessionTimedOut").showLightBox({ width: 300, hideCloseBtn: true });

    window.setTimeout("window.location.href = 'https://easybiz.alaskaair.com/ssl/signin/cosignin.aspx?CurrentForm=UCCoSignInStart&action=timedout';", 1000);
}
function redirectToMyAccountSignInPage(url) {
    $.hideLightBoxes();
    $("#divMyAccountSessionTimedOut").showLightBox({ width: 300, hideCloseBtn: true });

    var urlString = "https://www.alaskaair.com/www2/ssl/myalaskaair/MyAlaskaAir.aspx?CurrentForm=UCSignInStart";
    if (url != null) {
        urlString = urlString + "&url=" + url;
    }

    window.setTimeout("window.location.href = '" + urlString + "';", 1000);
}
var ESCAPE_KEY_CODE = 27;
var ENTER_KEY_CODE = 13;
function clickBtnOnTargetKey(targetKey, btnId) {
    if (event.keyCode == targetKey) {
        $("#" + btnId).click();
    }
}

function focusOnFirstInput(container) {
    $("input:eq(0)", container).focus();
}

jQuery.toggleAjaxProcessingImage = function (show) {
    if (show) {
        $.showProcessingBar();
    }
    else {
        $.hideProcessingBar();
    }
};


jQuery.fn.showLightBox = function (param) {
    //show only one lightbox at a time. Close other lightboxes.
    $.closeEzPopoups(true);

    param = param || {};
    var width = param != null && param.width != null ? param.width : 200;
    var maxWidth = param != null && param.maxWidth != null ? param.maxWidth : 585;
    var maxWidthOverride = param != null && param.maxWidthOverride != null ? param.maxWidthOverride : false;
    var isDraggable = param != null && param.isDraggable != null ? param.isDraggable : false;
    var src = param != null && param.src != null ? param.src : null;
    var setParentZIndex = param != null && param.setParentZIndex != null ? param.setParentZIndex : true;
    var show = param != null && param.show != null ? param.show : true;
    var centerOnPage = param != null && param.centerOnPage != null ? param.centerOnPage : true;
    var btnCloseElement = param != null && param.btnCloseElement != null ? $(param.btnCloseElement) : null;
    var btnSubmitElement = param != null && param.btnSubmitElement != null ? param.btnSubmitElement : null;
    var submitOnEnter = param != null && param.submitOnEnter != null ? param.submitOnEnter : true;
    var animation = param != null && param.animation != null ? param.animation : "easeOutBounce";
    var hideProcessingBar = param != null && param.hideProcessingBar != null ? param.hideProcessingBar : true;
    var hideValidationMessages = param != null && param.hideValidationMessages != null ? param.hideValidationMessages : true;
    var hideActionMessage = param != null && param.hideActionMessage != null ? param.hideActionMessage : true;
    var cornerRadius = param != null && param.cornerRadius != null ? param.cornerRadius : 5;
    var borderThickness = param != null && param.borderThickness != null ? param.borderThickness : 5;
    var borderColor = param != null && param.borderColor != null ? param.borderColor : "#5f87bb";
    var boxShadowStyle = param != null && param.boxShadowStyle != null ? param.boxShadowStyle : "3px 3px 3px #999";
    var parentElement = param.parentElement;
    var height = param.height;
    var duration = param.duration;
    var hideCloseBtn = param != null && param.hideCloseBtn != null ? param.hideCloseBtn : false;
    var hideCloseSecondaryBtn = param != null && param.hideCloseSecondaryBtn != null ? param.hideCloseSecondaryBtn : true;
    var createNew = param != null && param.createNew != null ? param.createNew : false;
    var skipFocus = param != null && param.skipFocus != null ? param.skipFocus : false;
    var onClose = param.onClose;
    var focusid = param.focusid;
    if (width == null || isNaN(width) || width <= 0) {
        width = 200;
    }

    var mainContainer = $(this).parents("." + POPUP_OUTER_DIV_CLASS);
    var innerContainer;

    var minWidth = 200;
    var maxHeight = 520;

    if (mainContainer.length == 0 || createNew == true) {
        parentId = "divEzPopup" + lightboxHelper.GetIdUniqueSuffix() + Math.floor(Math.random() * 11);
        imgId = "img" + lightboxHelper.GetIdUniqueSuffix();

        mainContainer = jQuery("<div/>", { id: parentId, "class": POPUP_OUTER_DIV_CLASS + ' popup', width: width, title: "", tabindex: 0 });

        var divImgContainer = jQuery("<div/>").css({ position: "absolute", right: "-15px", top: "-13px", "z-index": "9999999" });
        var imgCloseBtn;
        if ($.browser.msie && $.browser.version == 6) {
            imgCloseBtn = jQuery("<div />", { id: imgId, "style": "cursor: pointer;padding:1px 6px 3px 6px; border:solid 1px silver; margin-top:-4px" });
            imgCloseBtn.html("x");
            imgCloseBtn.css({ "font-size": "20px", color: "darkblue", "font-weight": "bold" });
        }
        else {
            aCloseBtn = jQuery("<a />", { id: CLOSE_ANCHOR_ID, href: "#" });
            imgCloseBtn = jQuery("<img />", { id: imgId, src: "https://www.alaskaair.com/images/Popup_Close_X.png", "alt": "Close" });
            aCloseBtn.append(imgCloseBtn);
        }
        imgCloseBtn.attr("class", POPUP_CLOSE_BTN_CLASS);
        if (hideCloseBtn == true) {
            imgCloseBtn.hide(); //hide button to IE6 users, button doesn't render nicely for the browser
        }

        divImgContainer.append(aCloseBtn);

        var innerContainer = jQuery("<div/>", { "class": POPUP_INNER_DIV_CLASS + ' containerx' });
        mainContainer.append(innerContainer);
        mainContainer.append(divImgContainer);

        if (parentElement == null || parentElement.length == 0) {
            parentElement = $(this).parent();
        }
        if (parentElement.length == 0) {
            parentElement = $("body");
        }
        if (parentElement.length == 0) {
            parentElement = $("div").first();
        }


        parentElement.append(mainContainer);
        //mainContainer.css({ background: "#5f87bb", padding: borderThickness + "px", "z-index": "9999999", float: "left" });
        mainContainer.css({ background: "white", "border": "solid " + borderThickness + "px #5f87bb", "z-index": "9999998", float: "left" });
        if (setParentZIndex) {
            mainContainer.offsetParent().css({ "z-index": "9999997" });
        }
        //, "max-height": "520px", "max-width":"585px", "overflow":"auto" 
        // var maxWidth = 585;
        $("." + POPUP_INNER_DIV_CLASS, mainContainer).css({ "background-color": "white" });
        if (height != null) {
            if (height > maxHeight) {
                mainContainer.css({ "max-height": maxHeight, overflow: "auto" });
                mainContainer.css({ "height": maxHeight });
            }
            else {
                mainContainer.css({ "height": height });
            }
        }
        if (width > maxWidth && !maxWidthOverride) {
            mainContainer.css({ width: maxWidth, "max-width": maxWidth, overflow: "auto" });
        }
        else {
            mainContainer.css({ width: width });
        }
        $("." + POPUP_INNER_DIV_CLASS, mainContainer).css({ "padding": "15px" });


        $("#" + CLOSE_ANCHOR_ID).live("click", function () {
            $(this).parents("." + POPUP_OUTER_DIV_CLASS).hide();
            var withVisiblePopups = false;
            $("." + POPUP_OUTER_DIV_CLASS).each(function (i) {
                if ($(this).css("display").toLowerCase() != "none") {
                    withVisiblePopups = true;
                }
            });
            if (!withVisiblePopups) {
                $.hideFormFiller();
            }
            if (hideProcessingBar) {
                $.hideProcessingBar();
            }
            if (hideValidationMessages) {
                $.hideValidationMessages();
            }
            if (hideActionMessage) {
                $.hideActionMessage();
            }

            if (onClose != null && typeof onClose == "function") {
                try {
                    onClose();
                }
                catch (e) {
                }
            }
            lastFocus.focus();

        });


    }
    else {
        //mainContainer = $(this).parents("." + POPUP_OUTER_DIV_CLASS);
        innerContainer = $("." + POPUP_INNER_DIV_CLASS, mainContainer);
        innerContainer.find("#closeLink").remove();
        if (height != null) {
            if (height > maxHeight) {
                mainContainer.css({ "max-height": maxHeight, overflow: "auto" });
                mainContainer.css({ "height": maxHeight });
            }
            else {
                mainContainer.css({ "height": height });
            }
        }
        if (width > maxWidth && !maxWidthOverride) {
            mainContainer.css({ width: maxWidth, "max-width": maxWidth, overflow: "auto" });
        }
        else {
            mainContainer.css({ width: width });
        }
    }

    innerContainer.append(this);

    if (isDraggable || isDraggable == null) {
        try {
            //lightboxes should not be movable for now
            //mainContainer.draggable();
        }
        catch (e) {
            //JQuery UI must be referenced in order for this to work
        }
    }

    if ($(btnCloseElement).length > 0) {
        //hide close button, according to spec from Amanda, users can still close lightbox by clicking area outside
        if (hideCloseSecondaryBtn) {
            $(btnCloseElement).hide();
        }
        $(btnCloseElement).bind("click", function () {
            $("." + POPUP_CLOSE_BTN_CLASS, mainContainer).click();
            return false;
        });
    }

    lastFocus = document.activeElement;
    this.show();
    innerContainer.makeRoundCorner({ cornerRadius: 3 });
    mainContainer.makeRoundCorner({ showShadow: true });

    if (src != null) {
        $(src).after(mainContainer);
    }
    mainContainer.hide().showEzPopup({ centerOnPage: true, duration: duration, centerOnPage: centerOnPage });
    mainContainer.css({ overflow: "" });
    mainContainer.show(0, function () {
        $.showFormFiller(hideCloseBtn == false, onClose);
    });

    var keyboardEvent = "keyup";
    if ($.browser.msie && $.browser.version == 6) {
        keyboardEvent = "keypress";
    }
    $(this).unbind(keyboardEvent);
    $(this).bind(keyboardEvent, function (e) {
        if (submitOnEnter && e.keyCode == ENTER_KEY_CODE && $(e.target).attr("id") != $(btnSubmitElement).attr("id")) {
            $(btnSubmitElement).click();
        }
    });

    if (lightboxHelper.KeyPressedEventBound == false) {
        lightboxHelper.KeyPressedEventBound = true;
        $(document).bind(keyboardEvent, function (e) {
            if (e.keyCode == ESCAPE_KEY_CODE && hideCloseBtn == false) {
                $("." + POPUP_CLOSE_BTN_CLASS).each(function (i) {
                    if ($(this).parents("." + POPUP_OUTER_DIV_CLASS).css("display").toLowerCase() == "block") {
                        $(this).click();
                    }
                });
            }
        });
    }

    //for accessibility, focus on first element that can receive focus
    if (skipFocus === false) {
        var focusElements = "input, a, textarea, select";
        if (jQuery.ui) {
            // the :focusable selector is part of the jquery UI library, which is only included on some of the pages
            focusElements = "input, a, textarea, select, :focusable";
        }
        if ($(focusElements, this).length > 0) {
            $(focusElements, this).eq(0).focus();
            try {
                $(this).scrollTop(0);
            }
            catch (e) {
            }
        }
        else {
            focusElements = "h1,h2,h3,h4,h5,div";
            if ($(focusElements, this).length > 0) {
                $(focusElements, this).eq(0).focus();
                try {
                    $(this).scrollTop(0);
                }
                catch (e) {
                }
            }
        }
    }
    else if (focusid) {
        $("#" + focusid).focus();
    }


    //Trap tabbing within lightbox
    $(mainContainer).bind('keydown', function (event) {
        if (event.keyCode !== $.ui.keyCode.TAB) {
            return;
        }

        var tabbables = $(':tabbable', this);
        var first = tabbables.filter(':first');
        var last = tabbables.filter(':last');

        var iFrame;

        //this is necessary because of how browsers tab through iFrames for the case where the content of the lightbox is in an iFrame.
        if (first[0].nodeName == 'IFRAME' && !event.shiftKey) {
            iFrame = first;
            var tabbables2 = $(':tabbable', tabbables[0].contentDocument);
            first = tabbables2.filter(':first');
        } else if (first[0].nodeName == 'IFRAME' && event.shiftKey) {
            iFrame = first;
            first = mainContainer;
        }


        if (event.target === last[0] && !event.shiftKey) {
            first.focus(1);
            return false;
        } else if ((event.target === first[0] || event.target === iFrame[0]) && event.shiftKey) {
            last.focus(1);
            return false;
        }

    });


    return mainContainer;
};


//end showLightBox function

jQuery.hideLightBoxes = function () {
    $("." + POPUP_OUTER_DIV_CLASS).hide();
};

jQuery.getHtml = function getHtml(param) {
    param = param || {};
    var width = param.width;
    var url = param.url;
    var container = param.container;
    var btnCloseElement = param.btnCloseElement;
    var btnCloseElementId = param.btnCloseElementId;
    var btnSubmitElement = param.btnSubmitElement;
    var submitOnEnter = param.submitOnEnter;
    var tables_forformatting = param.tables_forformatting;
    var popup = param.popup;
    var completeCallback = param.completeCallback;
    var afterCompleteCallback = param.afterCompleteCallback;
    var attachTo = param.attachTo;
    var height = param.height;
    var duration = param.duration;
    var centerOnPage = param.centerOnPage;
    var isDraggable = param != null && param.isDraggable != null ? param.isDraggable : true;
    var hideCloseBtn = param != null && param.hideCloseBtn != null ? param.hideCloseBtn : false;
    var hideCloseSecondaryBtn = param != null && param.hideCloseSecondaryBtn != null ? param.hideCloseSecondaryBtn : true;
    $.ajax({
        type: "POST",
        url: url,
        complete: function (response, textStatus) {
            switch (textStatus) {
                case "timeout":
                    $.showActionMessage(REQUEST_TIMED_OUT, true);
                    break;
                case "error":
                    $.showActionMessage(ERROR_ENCOUNTERED, true);
                    break;
                default:
                    if (completeCallback == null || completeCallback(response, textStatus)) {
                        if ($(container).length > 0) {
                            $(container).replaceWith(response.responseText);
                        }
                        else {
                            if (attachTo == null || attachTo.length == 0) {
                                attachTo = $("div").first();
                            }
                            if (attachTo == null || attachTo.length == 0) {
                                attachTo = $("body");
                            }
                            attachTo.after(response.responseText);
                            container = $(container);
                        }
                        if (btnCloseElement == null || $(btnCloseElement).length == 0) {
                            if (btnCloseElementId != null && btnCloseElementId != "") {
                                btnCloseElement = $("#" + btnCloseElementId);
                            }
                        }
                        if (popup == true) {
                            $(container).showLightBox({ width: width, btnCloseElement: btnCloseElement, btnSubmitElement: btnSubmitElement, height: height, duration: duration, src: attachTo, centerOnPage: centerOnPage, isDraggable: isDraggable, hideCloseBtn: hideCloseBtn, submitOnEnter: submitOnEnter, hideCloseSecondaryBtn: hideCloseSecondaryBtn });
                        }
                        $(tables_forformatting).formatEzTable({ isHeaderTextBold: true });

                        if (afterCompleteCallback != null) {
                            afterCompleteCallback();
                        }
                    }
            }
        }
    });
};

jQuery.showAboutDiscountCode = function (discountCode) {
    var divId = "divAboutDC_IF";
    var divIdJQ = "#" + divId;
    var ifrmId = "ifrmAboutDC";
    var ifrmIdJQ = "#" + ifrmId;
    if ($(divIdJQ).length == 0) {
        var html = "<div id='" + divId + "' style='display:none;'><iframe id='" + ifrmId + "' title='About Discount Codes' height='500' tabindex='0' frameborder='0' width='560' scrolling='auto' src='javascript:false;'></iframe></div>";
        $("body").append(html);
        $(divIdJQ).css({ width: 560, height: 500 });
    }
    var protocol = (top.location.protocol ? top.location.protocol : "http:") + "//";
    $(ifrmIdJQ).attr({ src: protocol + asglobal.domainUrl + "/shared/tips/AboutDiscountCodes.aspx?popup=true&referrer=lightbox&code=" + discountCode });
    $(divIdJQ).showLightBox({ width: 585, maxWidthOverride: true });
};

jQuery.showCodeOrNumberLB = function (src) {
    $.showLB(src, "www.alaskaair.com/help/CodeOrNumber", 380, 145);
};

jQuery.showLB = function (src, url, width, height) {
	// width is required, but height is not
	// url has to point to the same domain as the page
	// Use this if close buttons (elements with id "Close") need to be hidden
	// Do not include protocol in url, but include domain (e.g. "www.alaskaair.com/content/...")
	var divId = "divLB";
	var divIdJQ = "#" + divId;
	if ($(divIdJQ).length == 0) {
		var html = "<div id='" + divId + "' style='display:none;overflow:auto;'></div>";
		$("body").append(html);
	}
	if (!isNaN(height)) { $(divIdJQ).css({ height: height }); } else { $(divIdJQ).css({ height: "auto" }); }
	url = top.location.protocol + '//' + url;
	$.ajax({ url: url, success: function (data) { $(divIdJQ).html(data); $(divIdJQ + ' #Close').hide(); } });
	$(divIdJQ).showLightBox({ src: src, width: width, maxWidthOverride: true, centerOnPage: true });
};

jQuery.showLB_IF = function (src, url, width, height) {
	// both width and height are required
	// Use this if the url points to another domain
	// Close buttons cannot be hidden because of the cross-domain issue
	// url may include protocol, but not required.  Always include domain.
	var divId = "divLB_IF";
   	var divIdJQ = "#" + divId;
   	var ifrmId = "ifrmLB_IF";
   	var ifrmIdJQ = "#" + ifrmId;
   	if ($(divIdJQ).length == 0) {
   		var html = "<div id='" + divId + "' style='display:none;'><iframe id='" + ifrmId + "' frameborder='0' tabindex='0' scrolling='auto' src='javascript:false;'></iframe></div>";
   		$("body").append(html);
   	}
   	$(divIdJQ).css({ width: width-40, height: height });
   	$(ifrmIdJQ).css({ width: width-40, height: height });

   	if (url.substring(4, 0).toLowerCase() != "http") {
   		url = top.location.protocol + '//' + url;
   	}
   	$(ifrmIdJQ).attr({ src: url });
   	$(divIdJQ).showLightBox({ src: src, width: width, maxWidthOverride: true, centerOnPage: true });
};
//end Lightbox Code

//ends: lightbox.js
//starts: loadwidgets.js
jQuery.fn.loadWidget = function (param) {
    var url = param.url;
    var containerSelector = param.containerSelector;
    var callback = param.callback;
    var localCallback = param.localCallback;
    var doPost = param.doPost != null ? param.doPost : false;
    var getOnce = param.getOnce != null ? param.getOnce : true;
    if (!getOnce || $(containerSelector).length == 0) {
        var me = $(this);
        if (doPost) {
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                complete: function (response, textStatus) {
                    $(me).html(response.responseText);
                    if (callback != null) {
                        callback();
                    }
                    if (localCallback != null) {
                        localCallback();
                    }
                }
            });
        }
        else {
            $.get(url, function (data) {
                $(me).html(data);
                if (callback != null) {
                    callback();
                }
            });
        }
    }
    else {
        $(this).append($(containerSelector));
        callback();
    }
    return this;
};

jQuery.fn.loadVShoppingI = function (city) {
    var qs = BuildVShoppingQueryString(city, "?");
    var protocol = (top.location.protocol ? top.location.protocol : "http:") + "//";
    var url = protocol + asglobal.domainUrl + "/deals/flightformlet" + qs;
    this.html("<iframe scrolling='no' frameborder='0' src='" + url + "'" + " title='Shopping' id='flightsFormletIframe' style='width:205px;'></iframe>");
    adjustVShoppingParentHeight();
    return this;
};

jQuery.fn.loadAuctionPartialI = function (auctionParams) {
    var qs = BuildAuctionQueryString(auctionParams, "?");
    var protocol = (top.location.protocol ? top.location.protocol : "http:") + "//";
    var url = protocol + asglobal.domainUrl + "/auctions/auctionformlet" + qs;
    this.html("<iframe scrolling='no' frameborder='0' src='" + url + "'" + " title='Auctions' id='auctionFormletIframe' style='width:930px;height:450px'></iframe>");

    if ($.browser.msie && $.browser.version < 7) {
        $(".contentWrapNoSubNav").css({ height: 1450 });
    }

    return this;
};


jQuery.fn.loadVAwardShoppingI = function (city) {
    var qs = BuildVShoppingQueryString(city, "&");
    var protocol = (top.location.protocol ? top.location.protocol : "http:") + "//";
    var url = protocol + asglobal.domainUrl + "/deals/flightformlet?shoppingmethod=onlineaward" + qs;
    this.html("<iframe scrolling='no' frameborder='0' src='" + url + "' title='Award Shopping' id='awardsFormletIframe' style='width:205px;'></iframe>");
    adjustVShoppingParentHeight();
    return this;
};

jQuery.fn.loadVShopping = function (onload, city) {
    var qs = BuildVShoppingQueryString(city, "&");
    var protocol = (top.location.protocol ? top.location.protocol : "http:") + "//";
    var url = protocol + asglobal.domainUrl + "/deals/flightformlet?njqry=yes" + qs;
    adjustVShoppingParentHeight();
    return $(this).loadWidget({ url: url, containerSelector: WidgetContainers.Selectors.V, callback: onload, doPost: true, localCallback: function () {
        try {
            vShopping.InitializeControls(); //vShopping is in VShopping.js
        }
        catch (e) {
            alert("reference to VShopping.js could be missing");
        }
    }
    });
};

function adjustVShoppingParentHeight() {
    if ($.browser.msie && $.browser.version < 7) {
        $(".dealContentRight").css({ height: 700 });
    }
}

function BuildVShoppingQueryString(city, prefix) {
    var qs = (city != null && city != "" ? "D=" + city : "");
    if (qs == "") {
        qs = window.location.search.substring(1);
        if (qs == null) { qs = ""; }
    }
    if (qs != "") { qs = prefix + qs; }
    return qs;
}


function BuildAuctionQueryString(params, prefix) {

    var qs = "";
    if (params != null && params != "") {

        qs = "ID=" + params.AuctionId;
        qs += "&SD=" + params.StartDate;
        qs += "&ED=" + params.EndDate;
        qs += "&MB=" + params.MinimumBid;
        qs += "&T=" + params.Title;
    }

    if (qs == "") {
        qs = window.location.search.substring(1);
        if (qs == null) { qs = ""; }
    }
    if (qs != "") { qs = prefix + qs; }
    return qs;
}

jQuery.fn.loadPBShopping = function (onload) {
    var protocol = (top.location.protocol ? top.location.protocol : "http:") + "//";
    return $(this).loadWidget({ url: protocol + asglobal.domainUrl + "/planbook/ShoppingWidget?njqry=yes", containerSelector: WidgetContainers.Selectors.P, callback: onload, doPost: true });
};

jQuery.fn.loadPBHotel = function (onload) {
    var protocol = (top.location.protocol ? top.location.protocol : "http:") + "//";
    return $(this).loadWidget({ url: protocol + asglobal.domainUrl + "/deals/hoteloffers/HotelFormlet?njqry=yes", doPost: false, getOnce: true, containerSelector: WidgetContainers.Selectors.H, callback: onload });
};

jQuery.fn.loadPBCar = function (onload) {
    var protocol = (top.location.protocol ? top.location.protocol : "http:") + "//";
    return $(this).loadWidget({ url: protocol + asglobal.domainUrl + "/planbook/CarWidget?njqry=yes", doPost: true, getOnce: false, containerSelector: WidgetContainers.Selectors.H, callback: onload });
};

jQuery.fn.loadHVacation = function (onload, city) {
    var protocol = (top.location.protocol ? top.location.protocol : "http:") + "//";
    return $(this).loadWidget({ url: protocol + asglobal.domainUrl + "/vacation/hwidget?njqry=yes&city=" + city, containerSelector: WidgetContainers.Selectors.V, callback: onload, doPost: true });
};

function vacWidgetCallback(height) {
    $("#vacationFormletIframe").css({ height: height + 20 });
    $(document).scrollTop($(document).scrollTop());
    if ($.browser.msie && $.browser.version < 7) {
        if ($(vacationFormlet).parent().height() < height + 40) {
            $(vacationFormlet).css({ height: height + 30 });
            $(vacationFormlet).parent().css({ height: height + 40 });
        }
    }
}

var vacationFormlet = null;
jQuery.fn.loadVVacation = function (onload, city) {
    var protocol = (top.location.protocol ? top.location.protocol : "http:") + "//";
    var url = protocol + asglobal.domainUrl + "/vacation/vwidget";
    var qs = BuildVVacationQS(city);
    this.append("<iframe scrolling='no' frameborder='0' src='" + url + qs + "' title='Shopping' id='vacationFormletIframe' style='width:205px;padding:0;margin:0;'></iframe>");

    if (onload != null) {
        onload();
    }
    vacationFormlet = this;
    return this;
};

function BuildVVacationQS(city) {
    var qs = (city != null && city != "" ? "D=" + city : "");
    if (qs == "") {
        qs = window.location.search.substring(1);
        if (qs == null) { qs = ""; }
    }
    if (qs != "") { qs = "?&" + qs; }
    return qs;
}

jQuery.fn.loadCruise = function (onload) {
    var protocol = (top.location.protocol ? top.location.protocol : "http:") + "//";
    return $(this).loadWidget({ url: protocol + asglobal.domainUrl + "/vacation/cruise?njqry=yes", containerSelector: WidgetContainers.Selectors.V, callback: onload, doPost: true });
};

function showTips(url, width) {
    var divId = "divLB";
    var divIdJQ = "#" + divId;
    if ($(divIdJQ).length == 0) {
        var html = "<div id='" + divId + "' style='display:none;overflow:auto;'></div>";
        $("body").append(html);
    }
    var protocol = (top.location.protocol ? top.location.protocol : "http:") + "//";
    $.get(protocol + url, function (data) {
        $(divIdJQ).html(data);
        $(divIdJQ + " img").hide();
        $(divIdJQ).showLightBox({ width: width }).show();
    });
};

var flightWidgetAF = new FlightWidgetAF();
function FlightWidgetAF() {
    this.ShowAboutCT = function () {
        var aboutCT_LB_Id = "aboutCT_LB";
        var aboutCT_LB_CSS = "#" + aboutCT_LB_Id;
        if ($(aboutCT_LB_CSS).length == 0) {
            var fareOptions = ",MILEAGEUPG,GOLDUPG,MVPUPG,GUESTUPG"
            if ($("#fareOptions").length > 0) {
                fareOptions = $("#fareOptions").val();
            }
            $("body").append("<div id=" + aboutCT_LB_Id + "></div>");
            var protocol = "http:";
            protocol = (top.location.protocol ? top.location.protocol : protocol) + "//";
            $.get(protocol + asglobal.domainUrl + "/shopping/flights/AboutFareOptions?options=" + fareOptions, function (data) {
                $(aboutCT_LB_CSS).html(data);
                showLightBox();
            });
        }
        else {
            showLightBox();
        }
        function showLightBox() {
            $(aboutCT_LB_CSS + " #Close").hide();
            $(aboutCT_LB_CSS).showLightBox({ width: 550 }).show();
        }
    };

    this.ShowAwardOptions = function () {
        var awardOptions_LB_Id = "awardOptions_LB";
        var awardOptions_LB_CSS = "#" + awardOptions_LB_Id;
        if ($(awardOptions_LB_CSS).length == 0) {
            $("body").append("<div id=" + awardOptions_LB_Id + "></div>");
            var protocol = "http:";
            protocol = (top.location.protocol ? top.location.protocol : protocol) + "//";
            $.get(protocol + asglobal.domainUrl + "/AwardAdvisory/AboutAwardOptions", function (data) {
                $(awardOptions_LB_CSS).html(data);
                showLightBox();
            });
        }
        else {
            showLightBox();
        }
        function showLightBox() {
            $(awardOptions_LB_CSS).showLightBox({ width: 550 }).show();
        }
    };

    this.ShowAboutContractFares = function () {
        var contractFares_LB_Id = "contractFares_LB";
        var contractFares_LB_CSS = "#" + contractFares_LB_Id;
        if ($(contractFares_LB_CSS).length == 0) {
            $("body").append("<div id=" + contractFares_LB_Id + "></div>");
            var protocol = "http:";
            protocol = (top.location.protocol ? top.location.protocol : protocol) + "//";
            $.get(protocol + asglobal.domainUrl + "/shared/tips/aboutcompanyfares.aspx", function (data) {
                $(contractFares_LB_CSS).html(data);
                showLightBox();
            });
        }
        else {
            showLightBox();
        }
        function showLightBox() {
            $(contractFares_LB_CSS).showLightBox({ width: 300 }).show();
        }
    };

    this.ShowUMNROptions = function () {
        var umnrOptions_LB_Id = "umnrOptions_LB",
            umnrOptions_LB_CSS = "#" + umnrOptions_LB_Id;

        $(umnrOptions_LB_CSS).remove();
        
        $("body").append("<div id=" + umnrOptions_LB_Id + "></div>");
        $(umnrOptions_LB_CSS).html($("#flightsFormletIframe").contents().find("#divUMNR-container").clone().html());
        $("#divUMNR").show();
        $("#submitUMNRRequest").attr('onclick', '').undelegate();
        $("#divUMNR").delegate("#submitUMNRRequest", "click", wireUp);

        showLightBox();

        function showLightBox() {
            $(umnrOptions_LB_CSS).showLightBox({ width: 460 }).show();
        }

        function wireUp() {
            if (!$("#umnrYes").get(0).checked && !$("#umnrNo").get(0).checked) {
                $("#divNoUmnrAnswer").show();
                return;
            }
            $("#flightsFormletIframe").contents().find("#umnrYes").get(0).checked = $("#umnrYes").get(0).checked;
            $("#flightsFormletIframe").contents().find("#umnrNo").get(0).checked = $("#umnrNo").get(0).checked;

            $("#flightsFormletIframe").contents().find("#submitUMNRRequest").click();
        }
    };
}


//ends: loadwidgets.js
//starts: bubblenew.js
//start bubble code

var closeOnClickBound = false;

jQuery.fn.hideBubble = function () {
    $(this).parents('.bubble-new').hide();
    return this;
}

jQuery.fn.showBubble = function (param, e) {
    param = param || {};
    param.orientation = param.orientation || 1; //1 - pointup, 2 - pointdown, 3 - pointleft, 4 pointright

    if (param.orientation == 1 || param.orientation == 2) {
        param.pointDown = param.orientation == 2;
        $(this).showBubbleV(param, e);
    }
    else if (param.orientation == 3 || param.orientation == 4) {
        param.pointLeft = param.orientation == 3;
        $(this).showBubbleH(param, e);
    }
    else {
        param.pointDown = false;
        $(this).showBubbleV(param, e);
    }
    return this;
}

jQuery.fn.showBubbleV = function (param, e) {
    var me = this;

    param = param || {};
    param.width = param.width != null ? param.width : 200;
    param.offsetLeft = param.offsetLeft != null ? param.offsetLeft : 0;
    param.pointDown = param.pointDown != null ? param.pointDown : false;
    if (param.src == null) param.src = event;

    $('.bubble-new').hide(); //hide all bubbles first
    //calculate widths
    var padding = 10;
    var borderWidth = 3;
    var pointerWidth = 28;
    var leftWidth = Math.round((param.width - pointerWidth) / 2 + (padding + borderWidth));
    var rightWidth = leftWidth;
    var bubbleWidth = leftWidth + rightWidth + pointerWidth;
    var pointerHeight = 18;
    var bottomPointerHeight = 28;
    var contentWidth = bubbleWidth - (padding * 2) - borderWidth;

    //end calculate widths
    var borderStyle = borderWidth + 'px solid #82A3D1';
    var borderRadius = 8;
    if ($.browser.msie && $.browser.version < 7) {
        pointerHeight = 19;
        contentWidth -= 3;
        bottomPointerHeight = 33;
    }
    var fillerMarginTop = -16;
    if ($.browser.msie && $.browser.version <= 7) {
        fillerMarginTop = -1;
    }

    function protocol() {
        var protocol = "http:";
        protocol = (parent.location.protocol ? parent.location.protocol : protocol) + "//";
        return protocol;
    }
    var alaskaUrl = protocol() + 'www.alaskaair.com';

    if ($(this).parents('.bubble-new').length == 0) {
        var suffix = GetRandomId();
        var html = '<style type="text/css">.containerx .bubble-new, .containerx .bubble-new *{margin:0;padding:0;z-index:999999999}</style>';
        html = '<div class="bubble-new" id="bubble' + suffix + '" style="position:absolute;">';
        if ($.browser.msie && $.browser.version < 7) {
            html += '	<iframe id="ifrm"' + suffix + '" height="0" frameborder="0" width="0" scrolling="no" src="javascript:void(0);" style="position:absolute;z-index:-1"></iframe>';
        }
        html += '   <div id="close' + suffix + '" class="bubble-close" style="cursor:pointer; display:none; margin-left:' + (bubbleWidth - 10) + 'px;position:absolute; width:30px; height:30px; background:url(/images/Popup_Close_X.png) no-repeat"></div>';
        html += '	<div style="clear:both;width:' + bubbleWidth + 'px;" class="bubble-top" id="bubbletop' + suffix + '">';
        html += '		<div style="float:left; width:' + leftWidth + 'px;" class="bubble-top-left">';
        html += '           <div style="clear:both;height:15px;"></div><div style="background-color:white;width:' + leftWidth + 'px; height:10px; border-left:' + borderStyle + ';border-top:' + borderStyle + ';border-top-left-radius: ' + borderRadius + 'px;-moz-border-radius-topleft:' + borderRadius + 'px;"></div>';
        html += '		</div>';
        html += '		<div style="float:left; width:' + pointerWidth + 'px;">';
        html += '		    <div class="bubble-pointer" style="float:left;background:no-repeat bottom; background-image:url(' + alaskaUrl + '/content/~/media/Images/onSite/backgrounds/clippy_pointer_up);width:' + pointerWidth + 'px; height:' + pointerHeight + 'px;"></div><div style="clear:both;height:10px;background-color:white;"></div>';
        html += '		</div>';
        html += '		<div style="float:left; width:' + leftWidth + 'px;" class="bubble-top-right">';
        html += '           <div style="clear:both;height:15px;"></div><div style="background-color:white;width:' + leftWidth + 'px; height:10px; border-right:' + borderStyle + ';border-top:' + borderStyle + ';border-top-right-radius: ' + borderRadius + 'px;-moz-border-radius-topright:' + borderRadius + 'px;"></div>';
        html += '		</div>';
        html += '	</div>';
        html += '	<div class="bubble-content" style="-webkit-box-shadow: #999 3px 3px 3px;-moz-box-shadow:3px 3px 3px #999999;background-color:white;width:' + contentWidth + 'px;border-left:' + borderStyle + '; border-right:' + borderStyle + '; border-bottom:' + borderStyle + '; clear:both;border-bottom-left-radius: ' + borderRadius + 'px;border-bottom-right-radius: ' + borderRadius + 'px; padding:' + padding + 'px;" id="bubblecontent' + suffix + '">';
        html += '	</div>';
        html += '	<div style="display:none;clear:both;width:' + bubbleWidth + 'px;" class="bubble-bottom" id="bubblebottom' + suffix + '">';
        html += '		<div style="float:left; width:' + leftWidth + 'px; height:15px;" class="bubble-bottom-left">';
        html += '           <div style="background-color:white;width:' + leftWidth + 'px; height:10px; border-left:' + borderStyle + ';border-bottom:' + borderStyle + ';border-bottom-left-radius: ' + borderRadius + 'px;-moz-border-radius-bottomleft:' + borderRadius + 'px;"></div>';
        html += '		</div>';
        html += '		<div class="bubble-pointer" style="float:left;background-color:white;background:no-repeat bottom; background-image:url(' + alaskaUrl + '/content/~/media/Images/onSite/backgrounds/clippy_pointer_down);width:' + pointerWidth + 'px; height:' + (bottomPointerHeight - 1) + 'px;">&nbsp;<div style="position:absolute; margin-top:' + fillerMarginTop + 'px; background:white;width:42px;height:10px;"></div></div>';
        html += '		<div style="float:left; width:' + leftWidth + 'px; height:15px;" class="bubble-bottom-right">';
        html += '           <div style="background-color:white;width:' + leftWidth + 'px; height:10px; border-right:' + borderStyle + ';border-bottom:' + borderStyle + ';border-bottom-right-radius: ' + borderRadius + 'px;-moz-border-radius-bottomright:' + borderRadius + 'px;"></div>';
        html += '		</div>';
        html += '	</div>';
        html += '</div>';
        $(this).before(html);
        $(this).css({ position: 'static' });
        $('#bubblecontent' + suffix).attr({ totalWidth: (leftWidth * 2) });
        $('#bubblecontent' + suffix).append('<div class="clear"></div>').append(this).append('<div class="clear"></div>');
        if ($.browser.msie && $.browser.version < 7) {
            var frameHeight = $(this).height() < $('#bubble' + suffix).height() ? $(this).height() : $('#bubble' + suffix).height();
            $('iframe', '#bubble' + suffix).css({ width: $('#bubble' + suffix).width(), height: frameHeight, 'margin-top': pointerHeight });
        }
        $(this).show().css({ 'z-index': 999 });
    }
    $(this).parents('.bubble-new').show();
    $('.bubble-new').css('z-index', '99999999');

    //calculate position
    var offsetTop = 5;
    var srcTop = $(param.src).offset().top;
    var top = srcTop + $(param.src).height() + offsetTop;
    if (top < 0) top = 0;
    //check to see if bubble is partially hidden, if yes adjust top

    var bubbleDiv = $(this).parents('.bubble-new');
    var bubbleClose = $(".bubble-close", bubbleDiv);
    var bubbleTop = $(".bubble-top", bubbleDiv);
    var bubbleBottom = $(".bubble-bottom", bubbleDiv);
    var bubbleContent = $(".bubble-content", bubbleDiv);
    var bubbleTopLeft = $(".bubble-top-left", bubbleDiv);
    var bubbleTopRight = $(".bubble-top-right", bubbleDiv);
    var bubbleBottomLeft = $(".bubble-bottom-left", bubbleDiv);
    var bubbleBottomRight = $(".bubble-bottom-right", bubbleDiv);
    var bubbleHeight = bubbleDiv.height();
    var bubbleBottomPos = top + bubbleHeight;
    var bubbleTopPos = srcTop - bubbleHeight;
    var bottomPagePos = $(document).scrollTop() + getVisibleHeight();
    var leftPagePos = $(document).scrollLeft() + getVisibleWidth();
    var pointDown = (param.pointDown && bubbleTopPos > $(document).scrollTop()) || bubbleBottomPos > bottomPagePos;
    if (pointDown) {
        top = srcTop - bubbleHeight - offsetTop;
        $(bubbleContent).css({ 'border-bottom': 'none', 'border-top': borderStyle });
        $(bubbleContent).css({ 'border-top-left-radius': borderRadius, 'border-top-right-radius': borderRadius });
        $(bubbleContent).css({ 'border-bottom-left-radius': 0, 'border-bottom-right-radius': 0 });
        $(bubbleContent).css({ '-moz-border-radius-topleft': borderRadius, '-moz-border-radius-topright': borderRadius });
        $(bubbleContent).css({ '-moz-border-radius-bottomleft': 0, '-moz-border-radius-bottomright': 0 });
        $(bubbleBottom).show();
        $(bubbleTop).hide();
        $(bubbleClose).css({ 'margin-top': -10 });
    }
    else {
        $(bubbleContent).css({ 'border-top': 'none', 'border-bottom': borderStyle });
        $(bubbleContent).css({ 'border-bottom-left-radius': borderRadius, 'border-bottom-right-radius': borderRadius });
        $(bubbleContent).css({ 'border-top-left-radius': 0, 'border-top-right-radius': 0 });
        $(bubbleContent).css({ '-moz-border-radius-bottomleft': borderRadius, '-moz-border-radius-bottomright': borderRadius });
        $(bubbleContent).css({ '-moz-border-radius-topleft': 0, '-moz-border-radius-topright': 0 });
        $(bubbleBottom).hide();
        $(bubbleTop).show();
        $(bubbleClose).css({ 'margin-top': 5 });
    }
    var left = Math.round($(param.src).offset().left + $(param.src).width() / 2 - (bubbleWidth) / 2 + borderWidth) + param.offsetLeft;
    if (left < 0) left = 0;
    if (left < $(document).scrollLeft()) {
        left = $(document).scrollLeft();
    }
    if (left + bubbleDiv.width() > leftPagePos) {
        left = Math.round(leftPagePos - bubbleDiv.width());
    }
    //end calculate position
    adjustPointerPosition();

    function adjustPointerPosition() {
        var totalWidth = parseInt($(bubbleContent).attr('totalWidth'));
        var newLeftWidth = Math.round($(param.src).offset().left - left + $(param.src).width() / 2 - pointerWidth / 2);
        var newRightWidth = totalWidth - newLeftWidth;
        if ($.browser.msie && $.browser.version < 7) {
            newLeftWidth -= borderWidth;
            newRightWidth -= borderWidth;
        }

        $(bubbleTopLeft).css({ width: newLeftWidth });
        $(bubbleBottomLeft).css({ width: newLeftWidth });
        $('div', bubbleTopLeft).css({ width: newLeftWidth });
        $('div', bubbleBottomLeft).css({ width: newLeftWidth });

        $(bubbleTopRight).css({ width: newRightWidth });
        $(bubbleBottomRight).css({ width: newRightWidth });
        $('div', bubbleTopRight).css({ width: newRightWidth });
        $('div', bubbleBottomRight).css({ width: newRightWidth });
    }

    $(bubbleDiv).css({ top: top, left: left }).show();
    generateBubbleEvents(e, param, this, bubbleDiv, bubbleClose);


    function GetRandomId() {
        var d = new Date();
        var curr_hour = d.getHours();
        var curr_min = d.getMinutes();
        var curr_sec = d.getSeconds();

        return curr_hour + "_" + curr_min + "_" + curr_sec + Math.floor(Math.random() * 1111);
    }
    function getVisibleHeight() {
        return $(document).height() > $(window).height() ? $(window).height() : $(document).height();
    }
    function getVisibleWidth() {
        return $(document).width() > $(window).width() ? $(window).width() : $(document).width();
    }

    return this;
}

jQuery.fn.showBubbleH = function (param, e) {
    param = param || {};
    param.width = param.width != null ? param.width : 200;
    param.height = param.height != null ? param.height : 200;
    param.offsetLeft = param.offsetLeft != null ? param.offsetLeft : 0;
    param.pointLeft = param.pointLeft != null ? param.pointLeft : false;
    if (param.src == null) param.src = event;

    var isIE6 = $.browser.msie && $.browser.version < 7;

    var pointerWidth = 18;
    var width15 = 15;
    var width10 = 15;

    $('.bubble-new').hide(); //hide all bubbles first
    if ($(this).parents('.bubble-ltr').length == 0) {
        var suffix = GetRandomId();
        var html = '';
        html += '<style type="text/css">';
        html += '    .containerx .left{float:left;}';
        html += '    .containerx .bgwhite{background:white;}';
        html += '    .containerx .bubble-new{position:absolute; z-index: 9999999;}';
        html += '    .containerx .bubble-width15{width:15px;}';
        html += '    .containerx .bubble-width10{width:10px;}';
        html += '    .containerx .bubble-r_topleft{border-left:solid 3px #82A3D1;border-top:solid 3px #82A3D1;border-top-left-radius: 6px;-moz-border-radius-topleft:6px;}';
        html += '    .containerx .bubble-r_bottomleft{border-left:solid 3px #82A3D1;border-bottom:solid 3px #82A3D1;border-bottom-left-radius: 6px;-moz-border-radius-bottomleft:6px;}';
        html += '    .containerx .bubble-r_topright{border-right:solid 3px #82A3D1;border-top:solid 3px #82A3D1;border-top-right-radius: 6px;-moz-border-radius-topright:6px;}';
        html += '    .containerx .bubble-r_bottomright{border-right:solid 3px #82A3D1;border-bottom:solid 3px #82A3D1;border-bottom-right-radius: 6px;-moz-border-radius-bottomright:6px;}';
        html += '    .containerx .bubble-content{border-top:solid 3px #82A3D1;border-bottom:solid 3px #82A3D1; padding:0px;}';
        html += '    .containerx .bubble-shadow{-webkit-box-shadow: #999 3px 3px 3px;-moz-box-shadow:3px 3px 3px #999999}';
        html += '</style>';
        html += '<div class="bubble-new bubble-ltr">';
        html += '    <div id="close' + suffix + '" class="bubble-close" style="text-align:left; clear:both; cursor:pointer; display:none; margin-top:-10px; width:' + (parseInt(param.width) + 54) + 'px;position:absolute; left:0;height:30px; background:url(/images/Popup_Close_X.png) no-repeat center right"></div>';
        html += '    <div class="left buble-left">';
        html += '        <div style="clear:both;" class="bubble_lt" id="bubble_lt' + suffix + '">';
        html += '	        <div class="bubble-width15 left" style="height:50px;">&nbsp;</div>';
        html += '	        <div class="bubble-width10 bubble-r_topleft left bgwhite" style="height:50px;">&nbsp;</div>';
        html += '        </div>';
        html += '        <div style="clear:both;" class="bubble_lc" id="bubble_lc' + suffix + '">';
        html += '	        <div class="left" style="background:url(/content/~/media/Images/onSite/backgrounds/clippy_pointer_left) no-repeat center right; width:18px; height:30px;"></div>';
        html += '	        <div class="bubble-width10 left bgwhite"></div>';
        html += '        </div>';
        html += '        <div style="clear:both;" class="bubble_lb" id="bubble_lb' + suffix + '">';
        html += '	        <div class="bubble-width15 left" style="height:50px;">&nbsp;</div>';
        html += '	        <div class="bubble-width10 bubble-r_bottomleft left bgwhite" style="height:50px;">&nbsp;</div>';
        html += '        </div>';
        html += '    </div>';
        html += '    <div class="left bubble-content bgwhite bubble-shadow" style="height:130px; width:100px;" id="bubble_content' + suffix + '">';
        html += '    </div>';
        html += '    <div class="left buble-right">';
        html += '        <div style="clear:both;" class="bubble_rt" id="bubble_rt' + suffix + '">';
        html += '	        <div class="bubble-width10 bubble-r_topright left bgwhite bubble-shadow" style="height:50px;">&nbsp;</div>';
        html += '	        <div class="bubble-width15 left" style="height:50px;">&nbsp;</div>';
        html += '        </div>';
        html += '        <div style="clear:both;" class="bubble_rc" id="bubble_rc' + suffix + '">';
        html += '	        <div class="bubble-width10 left bgwhite">&nbsp;</div>';
        if (isIE6) {
            html += '	        <div class="left bgwhite" style="background:url(/content/~/media/Images/onSite/backgrounds/clippy_pointer_right.ashx) no-repeat center right; width:18px; height:30px;"></div>';
        }
        else {
            html += '	        <div class="left bgwhite" style="background:url(/content/~/media/Images/onSite/backgrounds/clippy_pointer_left) no-repeat center right; width:18px; height:30px;-moz-transform: scaleX(-1);-o-transform: scaleX(-1);-webkit-transform: scaleX(-1);transform: scaleX(-1);filter: FlipH;-ms-filter: \'FlipH\';"></div>';
        }
        html += '        </div>';
        html += '        <div style="clear:both;" class="bubble_rb" id="bubble_rb' + suffix + '">';
        html += '	        <div class="bubble-width10 bubble-r_bottomright left bgwhite bubble-shadow" style="height:50px;">&nbsp;</div>';
        html += '	        <div class="bubble-width15 left" style="height:50px;">&nbsp;</div>';
        html += '        </div>';
        html += '    </div>';
        html += '</div>';
        $(this).before(html);
        $(this).css({ position: 'static' });
        $('#bubble_content' + suffix).append('<div class="clear">&nbsp;</div>').append(this).append('<div class="clear">&nbsp;</div>');
        $(this).show().css({ 'z-index': 999 });
        if (isIE6) {
            var frameHeight = $(this).height() < $('#bubble' + suffix).height() ? $(this).height() : $('#bubble' + suffix).height();
            $('iframe', '#bubble' + suffix).css({ width: $('#bubble' + suffix).width(), height: frameHeight, 'margin-top': pointerHeight });
        }
        var $bubble_ltr = $(this).parents('.bubble-ltr');
        if (isIE6) {
            $bubble_ltr.css({ width: param.width + (width10 + width15) * 2 + 20 });
        }
        else {
            $bubble_ltr.css({ width: param.width + (width10 + width15) * 2 });
        }
        $bubble_ltr.show().attr({ height: $bubble_ltr.height() });
    }

    var pointLeft = param.pointLeft;

    var $bubble_new = $(this).parents('.bubble-ltr');
    var $bubble_close = $('.bubble-close', $bubble_new);
    var $bubble_lt = $('.bubble_lt', $bubble_new);
    var $bubble_lc = $('.bubble_lc', $bubble_new);
    var $bubble_lb = $('.bubble_lb', $bubble_new);
    var $bubble_left = $('.bubble-left', $bubble_new);
    var $bubble_content = $('.bubble-content', $bubble_new);
    var $bubble_right = $('.bubble-left', $bubble_new);
    var $bubble_rt = $('.bubble_rt', $bubble_new);
    var $bubble_rc = $('.bubble_rc', $bubble_new);
    var $bubble_rb = $('.bubble_rb', $bubble_new);

    var lt_height = 0;
    var lc_height = 0;
    var lb_height = 0;
    var rt_height = 0;
    var rc_height = 0;
    var rb_height = 0;
    var offsetLeft = 10;
    var pointerHeight = 28;
    var borderWidth = 3;
    var bubbleComputedHeight = param.height + 2 * borderWidth;
    var srcTop = $(param.src).offset().top + Math.round($(param.src).height() / 2);
    var srcLeft = $(param.src).offset().left;
    var left = 0;
    //start calculate top
    var top = srcTop - Math.round(param.height / 2) - 2 * borderWidth;
    if (top < 0) top = 0;
    if (top < $(document).scrollTop()) top = $(document).scrollTop();
    if (top + bubbleComputedHeight > $(document).scrollTop() + getVisibleHeight()) {
        top = $(document).scrollTop() + getVisibleHeight() - bubbleComputedHeight;
    }
    if (top < 0) top = 0;
    if (top + Math.round(pointerHeight / 2) > srcTop) {
        top -= Math.round(pointerHeight / 2);
    }
    if (srcTop + Math.round(pointerHeight / 2) > $(document).scrollTop() + getVisibleHeight()) {
        top += pointerHeight;
    }
    //end calculate top

    if (pointLeft == true) {
        if (srcLeft + $(param.src).width() + $bubble_new.width() > $(document).scrollLeft() + getVisibleWidth()) {
            pointLeft = false;
        }
    }

    if ((srcLeft - param.width - $(param.src).width()) < $(document).scrollLeft()) {
        pointLeft = true;
    }

    if (isIE6) {
        $bubble_lc.show();
        $bubble_rc.show();
    }
    if (pointLeft == true) {
        left = srcLeft + $(param.src).width();
        lc_height = pointerHeight;
        rc_height = 0;
        if (isIE6) {
            $bubble_rc.hide();
        }
    }
    else {
        lc_height = 0;
        rc_height = pointerHeight;
        left = srcLeft - $bubble_new.width();
        if (isIE6) {
            $bubble_lc.hide();
        }
    }

    var paramHeight = param.height;

    lt_height = srcTop - top - lc_height / 2;
    lb_height = paramHeight - lt_height - lc_height;

    rt_height = srcTop - top - rc_height / 2;
    rb_height = paramHeight - rt_height - rc_height;

    $bubble_content.css({ height: paramHeight, width: param.width });
    $('div', $bubble_lt).css({ height: lt_height });
    $('div', $bubble_lb).css({ height: lb_height });
    $('div', $bubble_lc).css({ height: lc_height });
    $('div', $bubble_rt).css({ height: rt_height });
    $('div', $bubble_rb).css({ height: rb_height });
    $('div', $bubble_rc).css({ height: rc_height });

    $bubble_new.css({ top: top, left: left });
    $(this).parents('.bubble-new').show();

    generateBubbleEvents(e, param, this, $bubble_new, $bubble_close, 2);
    function GetRandomId() {
        var d = new Date();
        var curr_hour = d.getHours();
        var curr_min = d.getMinutes();
        var curr_sec = d.getSeconds();

        return curr_hour + "_" + curr_min + "_" + curr_sec + Math.floor(Math.random() * 1111);
    }
    function getVisibleHeight() {
        return $(document).height() > $(window).height() ? $(window).height() : $(document).height();
    }
    function getVisibleWidth() {
        return $(document).width() > $(window).width() ? $(window).width() : $(document).width();
    }
}

function generateBubbleEvents(e, param, content, bubbleDiv, bubbleClose, view) {
    if (e != null && e.type != null && e.type == 'click') {
        e.cancelBubble = true;
        if (e.stopPropagation) {
            e.stopPropagation();
        }
        param.mouseOutHide = false;
    }
    else {
    	//enable click for touch users
    	if ($(param.src).attr("bubble_click") == "true") {
    		$(param.src).unbind("click");
    	}
        $(param.src).bind("click", function (e) {
            $(param.src).unbind("mouseout");
            if (view == null || view == 1) {
                $(content).showBubble({ width: param.width, src: $(this), offsetLeft: param.offsetLeft, mouseOutHide: false }, e)
            }
            else {
                $(content).showBubbleH({ width: param.width, height: param.height, src: $(this), pointLeft: param.pointLeft, mouseOutHide: false }, e)
            }
        });
        $(param.src).attr({ "bubble_click": "true" });
    }

    if (param.mouseOutHide == null || param.mouseOutHide != false) {
        $(bubbleClose).hide();
        $(param.src).bind("mouseout", function () {
            $(bubbleDiv).hide();
        });
    }
    else {
        if (!$.browser.msie || $.browser.version > 6) {
            $(bubbleClose).show().bind('click', function () {
                $(bubbleDiv).hide();
            });
        }
    }

    if (closeOnClickBound == false) {
        $(document).bind('click', function (e) {
            $('.bubble-new').hide();
        });
        $(window).bind('resize', function (e) {
            $('.bubble-new').hide();
        });
        closeOnClickBound = true;
    }
    $(bubbleDiv).bind('click', function (e) {
        if (!e) {
            e = window.event;
        }
        e.cancelBubble = true;
        if (e.stopPropagation) {
            e.stopPropagation();
        }
    });
    $(param.src).bind('click', function (e) {
        if (!e) {
            e = window.event;
        }
        e.cancelBubble = true;
        if (e.stopPropagation) {
            e.stopPropagation();
        }
    });
}

//end bubble code

//ends: bubblenew.js
//starts: adChoicesLoader.js
function AdChoices() {
    this.insertAdChoicesScript = function () {

        if ($('#AdChoicesContainer').length && !($('#_bapw-link').length)) {
            try {
                $('#AdChoicesContainer').append(
                '<a id="_bapw-link" href="#" target="_blank" style="text-decoration:none !important">' +
                    '<img id="_bapw-icon" alt="" style="border:0 !important;display:inline !important;vertical-align:middle !important;padding-right:5px !important;"/>' +
                    '<span style="vertical-align:middle !important">AdChoices</span>' +
                '</a>');                
            }
            catch (err) { }
        }

        if ($('#adChoicesUrl').length) {
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = $('#adChoicesUrl').attr('href');
            try {
                document.body.appendChild(script);
                $('#AdChoicesContainer').attr('style', 'display:block');
            }
            catch (err) { }
        }
    };
}

as.adChoices = new AdChoices();
//ends: adChoicesLoader.js
//starts: cake.js
// this is for Cake, starting Aug, 2013
function Cake() {
    this.init = function () {
        var params = _getQuerystringParams(window.location.search);
        // cake param found, then store it in cookie to create pixel and/or to perform postback
        if (params.hasOwnProperty('cake')) {
            _setCookie("cake", params.cake, 7);
        }
    }

    // only called when transaction is completed successfully
    // https://alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/booking/payment
    // https://www.alaskaair.com/booking/payment
    this.insertCakeConversionPixels = function () {
        var cake = JSON.parse(unescape(as.CakeTag.SourceCookie)),
            cakePixelUrl = "//astrks.com/p.ashx?f=img&r=" + cake.ri + "&o=" + cake.oi + "&t=" + as.CakeTag.RecordLocator + "&p=" + as.CakeTag.TotalBaseFare;

        _insertPixel(cakePixelUrl);
    }

    function _insertPixel(url) {
        var img = document.createElement('img');
        img.setAttribute('alt', '');
        img.setAttribute('height', '1');
        img.setAttribute('width', '1');
        img.setAttribute('style', 'display: none;');
        img.setAttribute('src', url);
        document.body.appendChild(img);
    }

    function _setCookie(c_name, value, exdays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = escape(value) + ((exdays == null) ? "" : ";domain=" + document.domain + ";path=/; expires=" + exdate.toGMTString());
        document.cookie = c_name + "=" + c_value;
    }

    function _getQuerystringParams(querystring) {
        var qs = unescape(querystring);

        // document.location.search is empty if no query string
        if (!qs) {
            return {};
        }

        // Remove the '?' via substring(1)
        if (qs.substring(0, 1) == "?") {
            qs = qs.substring(1);
        }

        // Load the key/values of the return collection
        var qsDictionary = {};

        // '&' seperates key/value pairs
        var pairs = qs.split("&");
        for (var i = 0; i < pairs.length; i++) {
            var keyValuePair = pairs[i].split("=");
            qsDictionary[keyValuePair[0]] = keyValuePair[1];
        }

        // Return the key/value dictionary
        return qsDictionary;
    }
}

if (typeof (as) != "undefined") {
    as.cake = new Cake();
    as.cake.init();
}
//ends: cake.js
//starts: superPixel.js
'use strict'

function SuperPixelController(opts) {
    var repo = (typeof (opts) != "undefined" && opts.hasOwnProperty("repo")) ? opts.repo : undefined,
        log = (typeof (window.as) != "undefined" && window.as.hasOwnProperty("Environment") && window.as.Environment != "prod") ? true : false,
        superPixelPageMapping = {
            pageNameOriginal: ["Home:Home",
                                "Shopping:MatrixAvailability",
                                "Shopping:CalendarAvailability",
                                "Shopping:BundledAvailability",
                                "Shopping:cart",
                                "booking:reservation",
                                "206:about-easybiz-test",
                                "EasyBiz:/Enrollment/WelcomeConfirm^EasyBiz",
                                "MainMileagePlan:UCMyAccountCreate",
                                "MyASSignedIn:Profile:Overview & Tier Status"],
            pageNameToBeSubstituted: ["Home",
                                        "Search",
                                        "Search",
                                        "Search",
                                        "Cart",
                                        "Purchase",
                                        "EBInfo",
                                        "EBEnroll",
                                        "MPInfo",
                                        "MPEnroll"],
            conditionMet: function (key) {
                var result = false;

                if ($.inArray(key, superPixelPageMapping.pageNameOriginal) != -1) {
                    result = true;

                    if (key == "Shopping:BundledAvailability") {
                        result = (repo.hasOwnProperty("formstate") && repo.formstate != "/Shopping/ReissueFlights") ? true : false;
                    }
                    if (key == "MyASSignedIn:Profile:Overview & Tier Status") {
                        result = (repo.hasOwnProperty("MAAP") && repo.MAAP.isNewMember == "True") ? true : false;
                    }
                    if (key == "booking:reservation") {
                        result = (repo.hasOwnProperty("formstate") && repo.formstate == "reservation^NewPurchase") ? true : false;
                    }
                }

                return result;
            }
        };

    var console = window.console || { log: function () { }, dir: function () { } };

    this.createSuperPixel = function () {
        var url = "//googleads.g.doubleclick.net/pagead/viewthroughconversion/1054000976/?value=0&label=Pf7ICLiz5wMQ0I7L9gM&guid=ON&script=0&data=";

        if (typeof (repo) == "undefined") {
            repo = {};
            if (typeof (window.as) != "undefined" && window.as.hasOwnProperty("Page")) {
                repo = window.as.Page;
            }
        }

        if (typeof (repo) != "undefined" && repo.hasOwnProperty("pagename") && superPixelPageMapping.conditionMet(repo.pagename)) {
            _insertPixel(url);
        }
    };

    function _insertPixel(url) {
        var superPixelParams = _populateSuperPixelParams(),
            img = document.createElement('img');

        img.setAttribute('alt', '');
        img.setAttribute('height', '1');
        img.setAttribute('width', '1');
        img.setAttribute('style', 'display: none;');
        img.setAttribute('src', url + superPixelParams);
        document.body.appendChild(img);

        if (log) { _log("Generated pixel url was : " + url + superPixelParams); }

        function _populateSuperPixelParams() {
            var params = '',
                v = new VisitorRepository().PopulateVisitor();

            params += "flight_pagetype=" + superPixelPageMapping.pageNameToBeSubstituted[$.inArray(repo.pagename, superPixelPageMapping.pageNameOriginal)];

            if (repo.hasOwnProperty("SP")) {
                var adtCount = ((repo.SP.paxADTCount == "ZERO") ? "0" : repo.SP.paxADTCount).toString(),
                    chdCount = ((repo.SP.paxCHDCount == "ZERO") ? "0" : repo.SP.paxCHDCount).toString();

                params += ";flight_originid=" + repo.SP.origin;
                params += ";flight_destid=" + repo.SP.destination;
                params += ";flight_startdate=" + _getFormattedData(repo.SP.outDate);
                if (repo.SP.inDate != '' && repo.SP.journeyType != "OW")
                    params += ";flight_enddate=" + _getFormattedData(repo.SP.inDate);
                params += ";flight_faretype=ADT-" + adtCount + ",CHD-" + chdCount;
                params += ";flight_itinerarytype=" + repo.SP.journeyType;
            }
            if (repo.hasOwnProperty("BP")) {
                params += ";flight_originid=" + repo.BP.origin;
                params += ";flight_destid=" + repo.BP.destination;
                params += ";flight_startdate=" + _getFormattedData(repo.BP.outDate);
                if (repo.BP.inDate != '' && repo.BP.journeyType != "OW")
                    params += ";flight_enddate=" + _getFormattedData(repo.BP.inDate);
                params += ";flight_itinerarytype=" + repo.BP.journeyType;
            }
            if (repo.hasOwnProperty("CP") && repo.CP.inCart != '')
                params += ";cart_contents=" + repo.CP.inCart.split('').join(',');

            if (v.hasOwnProperty("tier") && v.hasOwnProperty("hasInsiderSubscription")) {
                params += ";tier=" + v.tier;
                params += ";subscription=" + v.hasInsiderSubscription;
            }
            if (repo.hasOwnProperty("PP") && repo.PP.fop != '') {
                params += ";fop=" + repo.PP.fop;
            }

            function _getFormattedData(mdyDateString) {
                var result = '', mdy = mdyDateString.split('/');
                if (mdy.length == 3) {
                    var year = mdy[2], month = (mdy[0] > 9) ? mdy[0] : "0" + mdy[0].toString(), day = (mdy[1] > 9) ? mdy[1] : "0" + mdy[1].toString(),
                result = year + "-" + month + "-" + day;
                }
                return result;
            }

            return escape(params);
        }

        function _log(message) {
            var replaceSemiColon = new RegExp(";", "gi"),
                replaceEqualTo = new RegExp("=", "gi"),
                params = unescape(superPixelParams),
                paramStr = (params != "") ? '{\"' + params.replace(replaceSemiColon, '\",\"').replace(replaceEqualTo, '\":\"') + '\"}' : '',
                paramJSON = (paramStr != "") ? JSON.parse(paramStr) : {};

            console.log(message);
            if ($.browser.msie) {
                console.log(JSON.stringify(paramJSON));
            }
            else {
                console.dir(paramJSON);
            }
        }
    }
}

if (typeof (as) != "undefined" && as.IsSuperPixelDown == false) {
    as.superPixel = {};
    as.superPixel.ctrl = new SuperPixelController();
}
//ends: superPixel.js
//starts: wdcw.js
// this is for WDCW Campaign, starting Apr, 2013
function Wdcw() {
    var _me = this;
        
    // included in the following pages: visit home page, then book a flight
    //https://www.alaskaair.com/booking/payment
    //https://www.alaskaair.com/booking/reservation/saved
    this.insertPurchaseConfirmationPixels = function () {
        _insertPixel("http://bs.serving-sys.com/BurstingPipe/ActivityServer.bs?cn=as&ActivityID=232070&ns=1");
    }

    // included page: the landing / confirmation page after mileage plan account Sign Up
    this.insertMileagePlanConversionPixels = function () {
        _insertPixel("http://bs.serving-sys.com/BurstingPipe/ActivityServer.bs?cn=as&ActivityID=232071&ns=1");
    }

    // included page: the landing / confirmation page after News Letter Subscription Sign Up
    this.insertEmailSignupConversionPixels = function () {
        _insertPixel("http://bs.serving-sys.com/BurstingPipe/ActivityServer.bs?cn=as&ActivityID=234857&ns=1");
    }

    //<script type='text/javascript'>function callDeferredWdcwPixels() { as.wdcw.insertWdcwEmslPixels();}</script>
    // pixels for: http://www.alaskaair.com/content/deals/special-offers/explore-more-spend-less-ca.aspx
    this.insertWdcwEmslPixels = function () {
        _insertPixel("http://bs.serving-sys.com/BurstingPipe/ActivityServer.bs?cn=as&ActivityID=288135&ns=1");
    }

    //<script type='text/javascript'>function callDeferredWdcwPixels() { as.wdcw.insertWdcwEmslSandiegoPixels();}</script>
    // pixels for: http://www.alaskaair.com/content/flights-from-san-diego?q=SAN&o=SAN
    this.insertWdcwEmslSandiegoPixels = function () {
        _insertPixel("http://bs.serving-sys.com/BurstingPipe/ActivityServer.bs?cn=as&ActivityID=301872&ns=1");
    }

    function _insertPixel(url) {
        var img = document.createElement('img');
        img.setAttribute('alt', '');
        img.setAttribute('height', '1');
        img.setAttribute('width', '1');
        img.setAttribute('style', 'display: none;');
        img.setAttribute('src', url);
        document.body.appendChild(img);
    }
}

if (typeof (as) != "undefined" && as.IsWdcwDown == false) {
    as.wdcw = new Wdcw();
}
//ends: wdcw.js
//starts: spanish.js
// Floodlight pixels for Spanish Campaign, starting Apr, 2013
function Spanish() {
    var _me = this;
    var _random = (Math.random() + "") * 10000000000000;

    this.insertPixelsByCurrentUrl = function () {
        // Home Page Traffic
        if (_isPath("//alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/")) {
            _insertPixel("//ad.doubleclick.net/activity;src=3777717;type=traff923;cat=total247;ord=" + _random + "?");
        }
        if (_isPath("//www.alaskaair.com/")) {
            _insertPixel("//ad.doubleclick.net/activity;src=3777717;type=traff923;cat=Traff0;ord=" + _random + "?");
        }

        // Mileage Plan Traffic
        if (_isPath("http://alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/content/mileage-plan.aspx")) {
            _insertPixel("//ad.doubleclick.net/activity;src=3777717;type=traff923;cat=mptra697;ord=" + _random + "?");
        }
        if (_isPath("http://www.alaskaair.com/content/mileage-plan.aspx")) {
            _insertPixel("//ad.doubleclick.net/activity;src=3777717;type=mptra046;cat=MPTra0;ord=" + _random + "?");
        }

        // Mileage Plan Enrollment Form
        if (_pathStartsWith("http://alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/www2/ssl/myalaskaair/MyAlaskaAir.aspx?CurrentForm=UCMyAccountCreate")) {
            _insertPixel("//ad.doubleclick.net/activity;src=3777717;type=traff923;cat=mptra697;ord=" + _random + "?");
        }
        if (_pathStartsWith("http://www.alaskaair.com/www2/ssl/myalaskaair/MyAlaskaAir.aspx?CurrentForm=UCMyAccountCreate")) {
            _insertPixel("//ad.doubleclick.net/activity;src=3777717;type=traff923;cat=mptra697;ord=" + _random + "?");
        }

        // Mileage Plan Confirmation
        if (_pathStartsWith("http://alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/www2/ssl/myalaskaair/MyAlaskaAir.aspx?isNewMember=true")) {
            _insertPixel("//ad.doubleclick.net/activity;src=3777717;type=leads853;cat=mplea477;ord=" + _random + "?");
        }
        if (_pathStartsWith("http://www.alaskaair.com/www2/ssl/myalaskaair/MyAlaskaAir.aspx?isNewMember=true")) {
            _insertPixel("//ad.doubleclick.net/activity;src=3777717;type=leads853;cat=MPLea0;ord=" + _random + "?");
        }

        // Booking Traffic
        if (_isPath("//alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/planbook")) {
            _insertPixel("//ad.doubleclick.net/activity;src=3777717;type=traff923;cat=booki301;ord=" + _random + "?");
        }
        if (_isPath("//www.alaskaair.com/planbook")) {
            _insertPixel("//ad.doubleclick.net/activity;src=3777717;type=traff923;cat=Booki0;ord=" + _random + "?");
        }

        // Booking Confirmation
        if (_isPath("//alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/booking/payment")) {
            _insertPixel("//ad.doubleclick.net/activity;src=3777717;type=leads853;cat=booki057;ord=" + _random + "?");
        }
        if (_isPath("//www.alaskaair.com/booking/payment")) {
            _insertPixel("//ad.doubleclick.net/activity;src=3777717;type=leads853;cat=Booki0;ord=" + _random + "?");
        }

        // Flight Deals Traffic
        if (_isPath("http://alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/content/deals/flights.aspx")) {
            _insertPixel("//ad.doubleclick.net/activity;src=3777717;type=traff923;cat=deals381;ord=" + _random + "?");
        }
        if (_isPath("http://www.alaskaair.com/content/deals/flights.aspx")) {
            _insertPixel("//ad.doubleclick.net/activity;src=3777717;type=traff923;cat=Deals0;ord=" + _random + "?");
        }

        // LA Offers Traffic
        if (_isPath("http://alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/content/cities/flights-from/los-angeles.aspx")) {
            _insertPixel("//ad.doubleclick.net/activity;src=3777717;type=traff923;cat=laori482;ord=" + _random + "?");
        }
        if (_isPath("http://www.alaskaair.com/content/cities/flights-from/los-angeles.aspx")) {
            _insertPixel("//ad.doubleclick.net/activity;src=3777717;type=traff923;cat=LAOff0;ord=" + _random + "?");
        }

        // CA Offers Traffic
        if (_isPath("http://alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/content/deals/special-offers/explore-more-spend-less-ca.aspx")) {
            _insertPixel("//ad.doubleclick.net/activity;src=3777717;type=traff923;cat=MPAcq0;ord=" + _random + "?");
        }
        if (_isPath("http://www.alaskaair.com/content/deals/special-offers/explore-more-spend-less-ca.aspx")) {
            _insertPixel("//ad.doubleclick.net/activity;src=3777717;type=traff923;cat=MPAcq00;ord=" + _random + "?");
        }

        // Mexico Traffic 
        if (_isPath("//alaskaair.convertlanguage.com/alaskaair/enes/24/_www_alaskaair_com/planbook/vacations/mazatlan-mexico")) {
            _insertPixel("//ad.doubleclick.net/activity;src=3777717;type=traff923;cat=mazat155;ord=" + _random + "?");
        }
    }

    // url -> with querystring
    function _pathStartsWith(url) {
        return _me.isTesting || _startsWith(location.href.replace(location.protocol, "").toLowerCase(), url.toLowerCase());
    }

    // url -> no querystring
    function _isPath(url) {
        return _me.isTesting || (("//" + location.hostname + location.pathname).toLowerCase() == url.toLowerCase());
    }

    function _insertPixel(url) {
        var img = document.createElement('img');
        img.setAttribute('alt', '');
        img.setAttribute('height', '1');
        img.setAttribute('width', '1');
        img.setAttribute('style', 'display: none;');
        img.setAttribute('src', url);
        document.body.appendChild(img);
    }

    function _startsWith(thisStr, str) {
        return str.length > 0 && thisStr.substring(0, str.length) === str;
    }
}

if (typeof (as) != "undefined" && as.IsSpanishTagDown == false) {
    as.spanish = new Spanish();
}
//ends: spanish.js
//starts: adready.js
function Adready() {
    var _me = this;

    this.includeGlobalPixels = true;

    //included in every page, including homepage: http://www.alaskaair.com/
    //pixels copied from: http://www.adreadytractions.com/rt/63?p=801&async=true
    this.insertGlobalPixels = function () {
        if (_me.includeGlobalPixels == false) return;

        _insertPixel("//googleads.g.doubleclick.net/pagead/viewthroughconversion/1054000976/?value=0&label=Pf7ICLiz5wMQ0I7L9gM&guid=ON&script=0");
        _insertPixel("http://secure.quantserve.com/pixel/p-7dWYtK34XFp1Y.gif?labels=_fp.event.Universal");
        _insertPixel("http://a.adready.com/beacon.php?r=801");
        _insertPixelBasedOnHttps("//secure.adnxs.com/seg?add=99497", "//ib.adnxs.com/seg?add=99497");
        _insertPixelBasedOnHttps("//secure.media6degrees.com/orbserv/hbpix?pixId=13449&pcv=46", "//action.media6degrees.com/orbserv/hbpix?pixId=13449&pcv=46");
        _insertPixel("//cc.chango.com/c/o?pid=1824");
        _insertPixel("//r.turn.com/r/beacon?b2=yq-46M_0x8psFpA_3OVQ78_k6PkIAJIBhDqE1sEj09kUPVcur1rQmjaDUf_cXKaXzxiO_kaf-b2Y6lz4ctvkOA&cid=");
    }

    // included in the following pages: visit home page, then search a flight
    //https://www.alaskaair.com/shopping/Flights/Shop
    //https://www.alaskaair.com/shopping/Flights/Schedule
    //https://www.alaskaair.com/shopping/Flights/Calendar
    //https://www.alaskaair.com/shopping/Flights/Price
    //pixels copied from: https://www.adreadytractions.com/rt/63?p=25441&async=true
    this.insertShoppingPixels = function () {
        _insertPixel("//www.googleadservices.com/pagead/conversion/1054000976/?label=sZ8PCNi5nwIQ0I7L9gM&guid=ON&script=0");
        _insertPixel("http://secure.quantserve.com/pixel/p-7dWYtK34XFp1Y.gif?labels=_fp.event.Flight+Shopping+Page");
        //_insertPixel("//tag.yieldoptimizer.com/ps/ps?t=i&p=1194&rtg=0&pg=flight");
        _insertPixel("http://a.adready.com/beacon.php?r=25441");
        _insertPixelBasedOnHttps("//secure.adnxs.com/seg?add=161788", "//ib.adnxs.com/seg?add=161788");
    }

    // included in the following pages: visit home page, then book a flight
    //https://www.alaskaair.com/booking/payment
    //https://www.alaskaair.com/booking/reservation/saved
    this.insertConfirmationPixels = function () {
        _insertConfirmationPixels();
    }

    // included in the following pages: visit home page, then book a flight
    //https://www.alaskaair.com/booking/payment
    //https://www.alaskaair.com/booking/reservation/saved
    this.insertPurchaseConfirmationPixels = function () {
        _insertConvertionPixels();
        _insertConfirmationPixels();
    }

    // included in the following pages: visit home page, then sign in
    //https://www.alaskaair.com/www2/ssl/myalaskaair/myalaskaair.aspx
    //pixels copied from: https://www.adreadytractions.com/rt/63?p=16121&async=true
    this.insertMyAccountPixels = function () {
        _insertPixel("//www.googleadservices.com/pagead/conversion/1054000976/?label=2zloCPDghwIQ0I7L9gM&guid=ON&script=0");
        _insertPixel("http://secure.quantserve.com/pixel/p-7dWYtK34XFp1Y.gif?labels=_fp.event.Mileage+Plan+Login");
        //_insertPixel("//tag.yieldoptimizer.com/ps/ps?t=i&p=1194&rtg=0&pg=mpl"); //<-- Obsolete, replaced by the new Adara super pixel pls see SMMP#53903 and AdaraPixel.js
        _insertPixel("http://a.adready.com/beacon.php?r=16121");
        _insertPixelBasedOnHttps("//secure.adnxs.com/seg?add=99527", "//ib.adnxs.com/seg?add=99527");
    }

    // included in the following pages:
    //https://www.alaskaair.com/shopping/hotel
    //pixels copied from: https://www.adreadytractions.com/rt/63?p=19561&async=true
    this.insertHotelPixels = function () {
        _insertPixel("//www.googleadservices.com/pagead/conversion/1054000976/?label=EBFPCMiOkAIQ0I7L9gM&guid=ON&script=0");
        _insertPixel("http://secure.quantserve.com/pixel/p-7dWYtK34XFp1Y.gif?labels=_fp.event.Hotel+Page");
        _insertPixel("//tag.yieldoptimizer.com/ps/ps?t=i&p=1194&rtg=0&pg=hotel");
        _insertPixel("http://a.adready.com/beacon.php?r=19561");
        _insertPixelBasedOnHttps("//secure.adnxs.com/seg?add=124691", "//ib.adnxs.com/seg?add=124691");
    }

    // included in the following pages:
    //https://www.alaskaair.com/shopping/car
    //pixels copied from: https://www.adreadytractions.com/rt/63?p=19571&async=true
    this.insertCarPixels = function () {
        _insertPixel("//www.googleadservices.com/pagead/conversion/1054000976/?label=smO3CMCPkAIQ0I7L9gM&guid=ON&script=0");
        _insertPixel("http://secure.quantserve.com/pixel/p-7dWYtK34XFp1Y.gif?labels=_fp.event.Car+Page");
        _insertPixel("//tag.yieldoptimizer.com/ps/ps?t=i&p=1194&rtg=0&pg=car");
        _insertPixel("http://a.adready.com/beacon.php?r=19571");
        _insertPixelBasedOnHttps("//secure.adnxs.com/seg?add=124692", "//ib.adnxs.com/seg?add=124692");
    }

    // included in the following pages:
    //http://www.alaskaair.com/content/mileage-plan/benefits/about-mileage-plan.aspx?lid=nav:mileagePlan-benefits
    //pixels copied from: https://www.adreadytractions.com/rt/63?p=16131&async=true
    this.insertMileagePlanPixels = function () {
        _insertPixel("http://secure.quantserve.com/pixel/p-7dWYtK34XFp1Y.gif?labels=_fp.event.Mileage+Plan+Awards");
        _insertPixel("//tag.yieldoptimizer.com/ps/ps?t=i&p=1194&rtg=0&pg=mpa");
        _insertPixel("http://a.adready.com/beacon.php?r=16131");
        _insertPixelBasedOnHttps("//secure.adnxs.com/seg?add=99529", "//ib.adnxs.com/seg?add=99529");
    }

    // included in the following pages:
    //http://www.alaskaair.com/shopping/vacations?lid=nav:planbook-vacations
    //http://www.alaskaair.com/content/deals/vacations.aspx
    //pixels copied from: http://www.adreadytractions.com/rt/245541?p=25461&async=true
    this.insertVacationPixels = function () {
        _insertPixel("//www.googleadservices.com/pagead/conversion/1016634413/?label=K_HBCJufiQMQrbji5AM&guid=ON&script=0");
        _insertPixel("http://secure.quantserve.com/pixel/p-7dWYtK34XFp1Y.gif?labels=_fp.event.Vacation+Shopping+Page");
        _insertPixel("//tag.yieldoptimizer.com/ps/ps?t=i&p=1194&rtg=0&pg=vacation");
        _insertPixel("http://a.adready.com/beacon.php?r=25461");
        _insertPixelBasedOnHttps("//secure.adnxs.com/seg?add=161792", "//ib.adnxs.com/seg?add=161792");
    }

    //pixels copied from: http://www.adreadytractions.com/pt/398121/?h=4a05ac3aa30b6abbde2b&async=true   
    // included page: the landing / confirmation page after mileage plan account Sign Up
    this.insertMileagePlanConversionPixels = function () {
        _insertPixel("//a.adready.com/ce/60232/901651/?h=4a05ac3aa30b6abbde2b");
        _insertPixel("//fls.doubleclick.net/activityi;src=3770605;type=ar-ac016;cat=ar-ac042;ord=1?");
        _insertPixel("//www.googleadservices.com/pagead/conversion/996519509/?value=0&label=28iPCPOrrAMQ1dyW2wM&guid=ON&script=0");
        _insertPixel("http://secure.quantserve.com/pixel/p-7dWYtK34XFp1Y.gif?labels=_fp.event.Mileage+Rewards+Signup+Confirmation+Page");
        _insertPixelBasedOnHttps("//secure.adnxs.com/px?id=43855&t=2", "//ib.adnxs.com/px?id=43855&t=2");
        _insertPixel("//cs.yieldoptimizer.com/cs/c?a=1269&cpid=410&");
    }

    //pixels copied from: http://www.adreadytractions.com/pt/398131/?h=9c830cc3d57068e9bed2&async=true
    // included page: the landing / confirmation page after News Letter Subscription Sign Up
    this.insertEmailSignupConversionPixels = function () {
        _insertPixel("//a.adready.com/ce/60233/901661/?h=9c830cc3d57068e9bed2");
        _insertPixel("//fls.doubleclick.net/activityi;src=3770603;type=ar-ac347;cat=ar-ac618;ord=1?");
        _insertPixel("//www.googleadservices.com/pagead/conversion/999986501/?value=0&label=JHCFCMP8_AIQxarq3AM&guid=ON&script=0");
        _insertPixelBasedOnHttps("//secure.adnxs.com/px?id=43854&t=2", "//ib.adnxs.com/px?id=43854&t=2");
    }

    // pixels copied from: http://www.adreadytractions.com/pt/410611/?h=9e04b5282d41e0bf2072&async=true
    this.insertEasyBizConfirmationPixels = function () {
        _insertPixel("//3966881.fls.doubleclick.net/activityi;src=3966881;type=ar-ac819;cat=ar-ac273;ord=1?");
        _insertPixel("//www.googleadservices.com/pagead/conversion/1000862384/?value=0&label=ik8zCNCd4wQQsOWf3QM&guid=ON&script=0");
        _insertPixel("//a.adready.com/ce/61482/933671/?h=9e04b5282d41e0bf2072");
        _insertPixelBasedOnHttps("//secure.adnxs.com/px?id=58581&t=2", "//ib.adnxs.com/px?id=58581&t=2");
    }

    //pixels copied from: https://adreadytractions.com/pt/63/?h=fb206b070c03eba62c02    
    // included page: the landing / confirmation page successfully booked a flight ticket
    function _insertConvertionPixels() {
        _insertPixel("//a.adready.com/ce/80/71/?h=fb206b070c03eba62c02");
        _insertPixel("//ad.doubleclick.net/activity;src=2772323;type=ar-ac939;cat=ar-ac508;ord=1?");
        _insertPixel("http://secure.quantserve.com/pixel/p-7dWYtK34XFp1Y.gif?labels=_fp.event.Flight+Booking+Conversion");
        _insertPixel("//www.googleadservices.com/pagead/conversion/1054000976/?value=0.0&label=cMkhCPLKdxDQjsv2Aw&script=0");
        _insertPixel("//www.googleadservices.com/pagead/conversion/1023643442/?label=XB2pCMDVsAEQsp6O6AM&guid=ON&script=0");
        _insertPixelBasedOnHttps("//secure.adnxs.com/px?id=9478&t=2", "//ib.adnxs.com/px?id=9478&t=2");
        _insertPixel("//cs.yieldoptimizer.com/cs/c?a=1269&cpid=243&");
        _insertPixel("//as.chango.com/conv/i;%25n?conversion_id=10837");
        _insertPixelBasedOnHttps("//secure.media6degrees.com/orbserv/hbpix?pixId=13452&pcv=41", "//secure.media6degrees.com/orbserv/hbpix?pixId=13452&pcv=41");
        _insertPixel("//r.turn.com/r/beacon?b2=NXIUbeTpxngEpE-HFOhD52d0XXLMFUh5WvY0YL6awhsUPVcur1rQmjaDUf_cXKaXsbyBGpXhGatMgqAzqZFByQ&cid=");
        _insertPixelBasedOnHttps("//secure.adnxs.com/px?id=473273&t=2", "//id.travelspike.com/px?id=473273&t=2");
    }

    //pixels copied from: https://www.adreadytractions.com/rt/63?p=7091&async=true
    // included page: the landing / confirmation page successfully booked a flight ticket
    function _insertConfirmationPixels() {
        _insertPixel("//www.googleadservices.com/pagead/conversion/1054000976/?label=en4LCOjShwIQ0I7L9gM&guid=ON&script=0");
        _insertPixel("http://a.adready.com/beacon.php?r=7091");
        _insertPixelBasedOnHttps("//secure.adnxs.com/seg?add=99516", "//ib.adnxs.com/seg?add=99516");
    }

    function _insertPixel(url) {
        var img = document.createElement('img');
        img.setAttribute('alt', '');
        img.setAttribute('height', '1');
        img.setAttribute('width', '1');
        img.setAttribute('style', 'display: none;');
        img.setAttribute('src', url);
        document.body.appendChild(img);
    }

    function _insertPixelBasedOnHttps(sslUrl, url) {
        if (window.location.protocol == "https:")
            _insertPixel(sslUrl);
        else
            _insertPixel(url);
    }
}

if (typeof (as) != "undefined" && as.IsAdreadyDown == false) {
    as.adready = new Adready();
}

//ends: adready.js
//starts: intentMediaPixel.js
// after 2/25/2014, pixels can be verified here:  http: //www.alaskaair.com/tests/intentmedia.htm?debug=1
function IntentMediaPixel() {
    //<a id="intentMediaUrl" href="http://a.intentmedia.net/adServer/advertiser_conversions?entity_id=66539&site_type=ALASKA_AIRLINES&product_category=FLIGHTS&travelers=1&conversion_currency=USD&conversion_value=300.00&order_id=ABCDEF&cache_buster=123456789">
    this.insertIntentMediaConversionPixel = function () {
        if ($('#intentMediaUrl').length) {
            $('body').append($("<img width='1' height='1' border='0' alt='intent media url'></img>").attr({ src: $('#intentMediaUrl').attr("href") }));
        }
    }

    //https://gist.github.com/IMAdsTeam/1c2bdff0a7aaca5fdf84
    // all pixels are deferred loaded.  This method will be called after web page finished loading: as.intentMediaPixel.insertIntentMediaGlobalPixel()
    this.insertIntentMediaGlobalPixel = function () {
        window.IntentMediaProperties = {
            page_id: "UNKNOWN",  // possible values - Home:Home; PlanBook_Flights:Home; FlightDeals:deals-flight; 104:travel-info; 105:gifts-products; 106:mileage-plan; ...
            product_category: 'FLIGHTS',  // value hard coded
            page_view_type: 'UNKNOWN', // value hard coded
            user_member_id: '',  // possible values - Y; N
            entity_id: '66539'  // value hard coded
        };

        if ('undefined' !== typeof s && 'undefined' !== typeof s.pageName) // omniture value
            window.IntentMediaProperties.page_id = s.pageName;

        if ('undefined' !== typeof VisitorRepository) {
            var _v = new VisitorRepository().PopulateVisitor();
            window.IntentMediaProperties.user_member_id = (_v.isMileagePlanMember) ? 'Y' : 'N';
        }

        var script = document.createElement("script");
        var prefix = document.location.protocol === 'https:' ? 'https://a' : 'http://a.cdn/';
        script.src = prefix + '.intentmedia.net/javascripts/intent_media_data.js';
        document.getElementsByTagName("head")[0].appendChild(script);
    }
}

if (typeof (as) != "undefined" && as.IsIntentMediaDown == false) {
    as.intentMediaPixel = new IntentMediaPixel();
}


//ends: intentMediaPixel.js
//starts: bing.js
function Bing(tagUtil) {
    var _me = this;
    var _tagUtil = tagUtil;

    this.insertPixelsByCurrentUrl = function () {
        // booking confirmation
        if (_tagUtil.isPage_BookingConfirm()) {
            _insertPixelsForBooking();
        }
    }

    function _insertPixelsForBooking() {
        if (window.as && window.as.Page && window.as.Page.Cart && window.as.Page.Cart.Itinerary) {
            var valParam = "&revenue=" + window.as.Page.Cart.Itinerary.Revenue.toFixed(2);
            var url = "//209254.r.msn.com/?dedup=1&domainId=209254&type=1&actionid=241388" + valParam;
            _tagUtil.insertImg(url);
        }
    }
}

if (typeof (as) != "undefined" && !as.IsBingDown) {
    as.bing = new Bing(as.tagUtil);
}
//ends: bing.js
//starts: kenshoo.js
// mantis 55038 - added initial basic tracking pixel, 8/6/2014
// mantis 55256 - added reveune and type parameters, 8/20/2014
// mantis 55219 - added tracking pixel to 4 more pages 9/3/2014
function Kenshoo(tagUtil) {
    var _me = this;
    var _tagUtil = tagUtil;

    this.insertPixelsByCurrentUrl = function () {
        if (_tagUtil.isPage_BookingConfirm()) {
            _insertPixelsForBooking();
        }

        if (_tagUtil.isPage_EasybizSignupConfirm()) {
            _tagUtil.insertImg(_toUrl("&type=EasyBiz_Acct_KS"));
        }

        // mileage plan registration
        if (_tagUtil.isPage_MileagePlanSignupConfirm()) {
            _tagUtil.insertImg(_toUrl("&type=Mileage_Program_KS"));
        }
    }

    // this is made public method because it is called from a sitecore page through onclick event
    // http://www.alaskaair.com/content/credit-card/visa-signature.aspx?
    this.insertPixelsForBankcard = function () {
        _tagUtil.insertImg(_toUrl("&type=BankCard_Referrals_KS"));
    }

    // this is made public method because the confirm page is not unique - shared by other My Account pages
    // https://www.alaskaair.com/www2/ssl/myalaskaair/myalaskaair.aspx
    this.insertEmailSignupConversionPixels = function () {
        _tagUtil.insertImg(_toUrl("&type=Email_Sign_Ups_KS"));
    }

    // this is made public method because submit page is not unique - shared by both success or failure result
    // https://www.alaskaair.com/RegistrationPromo/Club49Registration/submit
    this.insertClub49ConfirmPixels = function () {
        _tagUtil.insertImg(_toUrl("&type=Club_49_Reg_KS"));
    }

    function _insertPixelsForBooking() {
        if (window.as && window.as.Page && window.as.Page.Cart && window.as.Page.Cart.Itinerary)
        {
            var valParam = "&valueCurrency=USD&val=" + window.as.Page.Cart.Itinerary.Revenue.toFixed(2) + "&orderId=" + window.as.Page.Cart.Itinerary.Recloc;
            _tagUtil.insertImg(_toUrl("&type=Bookings_KS", valParam));
        }
    }

    function _toUrl(typeParam, valParam) {
        //other params: &orderId=&promoCode=&GCID=&kw=&product=&type=&val=
        var url = "http://143.xg4ken.com/media/redir.php?track=1&token=bde70147-ad76-4556-964a-62e9a3363458";
        if (typeParam)
            url = url + typeParam;
        if (valParam)
            url = url + valParam;
        return url;
    }
}

if (typeof (as) != "undefined" && !as.IsKenshooDown) {
    as.kenshoo = new Kenshoo(as.tagUtil);
}
//ends: kenshoo.js
//starts: sojernPixel.js
function SojernPixel() {
    this.insertSojernPixel = function () {
        if ($('#sojernPixelUrl').length) {
            $('body').append($("<img width='1' height='1' border='0' alt='sojern pixel url'></img>").attr({ src: $('#sojernPixelUrl').attr("href") }));
        }
    }
}

as.sojernPixel = new SojernPixel();
//ends: sojernPixel.js
//starts: foresee.js
function Foresee() {
    this.insertForeseeScript = function () {
        if ($('#foreseeUrl').length) {
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = $('#foreseeUrl').attr('href');
            try
            {
                document.body.appendChild(script);         
            }
            catch(err){}
        }
    }
}

as.foresee = new Foresee();
//ends: foresee.js
//starts: facebooklike.js
function Facebook() {
    this.insertFacebook = function () {
        if ($('#fblikebutton').length) {
            $('#fblikebutton').append('<a title="Follow us on Facebook" id="footerFacebook" href="http://www.facebook.com/alaskaairlines"><span class="hidden">Follow us on Facebook</span></a>');
        }
    }
}

function FacebookLike() {
    this.insertFacebook = function () {
        if ($('#fblikebutton').length) {
            $('#fblikebutton').append('<span class="hidden">Follow us on Facebook</span><iframe src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2Falaskaairlines&send=false&layout=button_count&width=450&show_faces=false&action=like&colorscheme=light&font&height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:87px; height:21px;" allowTransparency="true"></iframe>');
        }
    }
}

if (typeof (as) != "undefined") {
    if (as.IsFacebookLikeDown == true) {
        as.facebook = new Facebook();        
    }
    else {
        as.facebook = new FacebookLike();
    }
}
//ends: facebooklike.js
//starts: deferredLoader.js
// usage: defined a function downloadDeferredContent() in your page, it will be called during onload event
function DeferredLoader() {
    var _contentDownloaded = false;

    this.init = function() {
        _registerToDownloadDeferredContentOnLoad();
    };

    function _registerToDownloadDeferredContentOnLoad() {
        // Check for browser support of event handling capability
        /*if (typeof $ != "undefined" && $) {
            $(window).load(_downloadDeferredContentOnLoad);            
        }
        else */
        if (window.addEventListener) {
            window.addEventListener("load", as.deferredLoader.startDownload, false);
        } else if (window.attachEvent) {
            window.attachEvent("onload", as.deferredLoader.startDownload);
        } else window.onload = as.deferredLoader.startDownload;
    }

    this.startDownload = function () {
        if (_contentDownloaded) { return; }

        _contentDownloaded = true;

        // defer omniture calls: s.t() and s2.t()
        if (typeof (callDeferredOmniture) == "function") {
            callDeferredOmniture();
        }

        //defer facebook like button (an iframe)
        if (as.facebook) {
            as.facebook.insertFacebook();
        }

        // defer responsys TWRS async script tag
        if (as.rTWRS) {
            var re = new RegExp("IMAGEToken" + "=[^;]+", "i");
            if (!document.cookie.match(re)) {
                as.rTWRS.insertAsyncScript();
            }
        }

        // defer google analytics async script tag
        if (as.ga) {
            as.ga.insertAsyncScript();
            as.ga.insertECommerceConversionPixel();
        }

        // defer adReady global pixels
        if (as.adready) {
            // defer adReady specific pixels
            if (typeof (callDeferredAdreadyPixels) == "function") {
                callDeferredAdreadyPixels();
            }

            if (as.adready.includeGlobalPixels == true) {
                as.adready.insertGlobalPixels();
            }
        }

        //  defer Intent Media Pixel
        if (as.intentMediaPixel) {
            as.intentMediaPixel.insertIntentMediaGlobalPixel();
            as.intentMediaPixel.insertIntentMediaConversionPixel();
        }

        //  defer Sojern Pixel
        if (as.sojernPixel) {
            as.sojernPixel.insertSojernPixel();
        }

        //  defer ForeSee <script> tag
        if (as.foresee) {
            as.foresee.insertForeseeScript();
        }

        //  defer AdChoices <script> tag
        if (as.adChoices) {
            as.adChoices.insertAdChoicesScript();
        }

        // defer interstitial images
        if (as.interstitial) {
            as.interstitial.insertInterstitialImages();
        }

        // defer wdcw pixels
        if (as.wdcw) {
            if (typeof (callDeferredWdcwPixels) == "function") {
                callDeferredWdcwPixels();
            }
        }

        // defer spanish pixels
        if (as.spanish) {
            as.spanish.insertPixelsByCurrentUrl();
        }

        // defer cake conversion pixels
        if (as.cake && as.CakeTag && as.CakeTag.SourceCookie &&
        (location.hostname.toString() == 'http://www.alaskaair.com/files/alaskaair.convertlanguage.com' || location.hostname.toString() == 'http://www.alaskaair.com/files/www.alaskaair.com') &&
        (location.pathname == "/alaskaair/enes/24/_www_alaskaair_com/booking/payment" || location.pathname == "/booking/payment")) {
            as.cake.insertCakeConversionPixels();
        }

        if (as.bing) {
            as.bing.insertPixelsByCurrentUrl();
        }

        if (as.kenshoo) {
            as.kenshoo.insertPixelsByCurrentUrl();
            if (typeof (callDeferredKenshooPixels) == "function") {
                callDeferredKenshooPixels();
            }
        }

        // defer adready super pixel create call
        if (typeof (as.superPixel) != "undefined") {
            as.superPixel.ctrl.createSuperPixel();
        }

        require(['common/pixelCaller'], function (pixelCaller) { pixelCaller(); });

        // create the following function for deferring execution in your specific page
        if (typeof (downloadDeferredContent) == "function") {
            downloadDeferredContent();
        }
    }

}

as.deferredLoader = new DeferredLoader();

$(document).ready(function() {
    as.deferredLoader.init();
});

//ends: deferredLoader.js
//starts: responsysTWRS.js
if (typeof (as) != "undefined" && as.IsResponsysTWRSDown == false && as.ResponsysTWRSAccount != "") {
    as.rTWRS = {
        trackingPageMapping: {
            shoppingPageIds: ["MatrixAvailability",
                                "CalendarAvailability",
                                "BundledAvailability"],
            cartPageIds: ["cart"],
            confirmPageIds: ["reservation"],
            isInShoppingPage: function (key) {
                return ($.inArray(key, as.rTWRS.trackingPageMapping.shoppingPageIds) != -1);
            },
            isInCartPage: function (key) {
                return ($.inArray(key, as.rTWRS.trackingPageMapping.cartPageIds) != -1);
            },
            isInConfirmPage: function (key) {
                var result = false;
                if ($.inArray(key, as.rTWRS.trackingPageMapping.confirmPageIds) != -1) {
                    result = true;
                    if (key == "booking:reservation") {
                        result = (repo.hasOwnProperty("formstate") && repo.formstate == "reservation^NewPurchase");
                    }
                }
                return result;
            }
        },
        insertAsyncScript: function (repo) {
            var repo = repo || window.as.Page,
                hasASPageRepo = (typeof (repo) != "undefined" && repo.hasOwnProperty("pageid"));

            if (hasASPageRepo && (as.rTWRS.trackingPageMapping.isInShoppingPage(repo.pageid) || as.rTWRS.trackingPageMapping.isInCartPage(repo.pageid) || as.rTWRS.trackingPageMapping.isInConfirmPage(repo.pageid))) {
                (function (i, s, o, g, r, a, m) {
                    i['ResponsysTWRSObject'] = r;
                    i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                    a = s.createElement(o), m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', '../../wrs.adrsp.net/ts-twrs/js/twrs.min.js'/*tpa=http://wrs.adrsp.net/ts-twrs/js/twrs.min.js*/, '_riTrack');
            }
        },
        track: function (repo) {
            var repo = repo || window.as.Page,
                hasASPageRepo = (typeof (repo) != "undefined" && repo.hasOwnProperty("pageid"));

            // For now we are only tracking standard shopping, not re-issues
            if (hasASPageRepo && as.rTWRS.trackingPageMapping.isInShoppingPage(repo.pageid) && (repo.hasOwnProperty("ShoppingSearch") || repo.hasOwnProperty("LowFareItinerary"))) {
                var ss = repo.ShoppingSearch,
                    srchEvent = _riTrack.createSearch(undefined, _customerId, ss.ItineraryType);

                for (var i = 0; i < ss.CityPairSlices.length; i++) {
                    var cps = ss.CityPairSlices[i];
                    if (!cps.IsInbound) {
                        srchEvent.addSlice(cps.DepartureShort, cps.ArrivalShort, cps.Date, null, ss.TravelersCount, ss.AdultCount, ss.ChildrenCount, (ss.IsRevenue) ? "Revenue" : "Award", 0, 0, ss.CabinType);
                    }
                    else {
                        srchEvent.addSlice(cps.DepartureShort, cps.ArrivalShort, cps.Date, null, ss.TravelersCount, ss.AdultCount, ss.ChildrenCount, (ss.IsRevenue) ? "Revenue" : "Award", 0, 0, ss.CabinType);
                    }
                }
                srchEvent.addOptionalData("DiscountType", ss.DiscountType);
                srchEvent.addOptionalData("Fare", ss.FareType);
                if (!ss.IsRevenue) {
                    srchEvent.addOptionalData("AwardOption", ss.AwardOption);
                    srchEvent.addOptionalData("ShopAwardCalendar", ss.ShopAwardCalendar);
                }
                else {
                    srchEvent.addOptionalData("ShopLowFareCalendar", ss.ShopLowFareCalendar);
                }
                srchEvent.addOptionalData("IncludeNearbyArrivalAirports", ss.IncludeNearbyArrivalAirports);
                srchEvent.addOptionalData("IncludeNearbyDepartureAirports", ss.IncludeNearbyDepartureAirports);
                srchEvent.addOptionalData("UMNRAnswer", ss.UMNRAnswer);

                srchEvent.trackSearchEvent();
                as.rTWRS.log('Responsys TWRS search event data : ', srchEvent.getJSON()[0].search);

                if (repo.hasOwnProperty("LowFareItinerary")) {
                    var itin = repo.LowFareItinerary,
                        itinEvent;

                    itinEvent = _riTrack.createSearchResults(_customerId, undefined)
                    itinEvent.addItinerary(as.rTWRS.getItinerary(itin));
                    itinEvent.addOptionalData("SeatsRemaining", itin.SeatsRemaining);
                    itinEvent.trackSearchResults();
                    as.rTWRS.log('Responsys TWRS lowfare itinerary search results found');
                    as.rTWRS.log('Responsys TWRS lowfare itinerary search results event data : ', itinEvent.getJSON());
                }
            }
            if (hasASPageRepo
                && (as.rTWRS.trackingPageMapping.isInCartPage(repo.pageid) || as.rTWRS.trackingPageMapping.isInConfirmPage(repo.pageid))
                && repo.hasOwnProperty("Cart")
                && repo.Cart.hasOwnProperty("Itinerary")
                && repo.Cart.Itinerary.HasFlight) {
                var itin = repo.Cart.Itinerary,
                    isCartPage = (as.rTWRS.trackingPageMapping.isInCartPage(repo.pageid)),
                    itinEvent = (isCartPage) ? _riTrack.createItinerarySelect(_customerId, undefined) : _riTrack.createItineraryPurchase(_customerId, undefined);

                itinEvent.setItinerary(as.rTWRS.getItinerary(itin));
                itinEvent.addOptionalData("SeatsRemaining", itin.SeatsRemaining);
                if (isCartPage) {
                    itinEvent.trackItinerarySelect();
                }
                else {
                    itinEvent.trackItineraryPurchase();
                }
                as.rTWRS.log('Responsys TWRS itinerary ' + ((isCartPage) ? 'select' : 'purchase') + ' event data : ', itinEvent.getJSON());
            }
        },
        log: function (message, jObj) {
            if (typeof (window.as) != "undefined" && window.as.hasOwnProperty("Environment") && window.as.Environment != "prod") {
                var console = window.console || { log: function () { }, dir: function () { } };

                if (message != 'undefined' || message != '')
                    console.log(message);

                if ($.browser.msie && jObj)
                    console.log(JSON.stringify(jObj));
                else if (jObj)
                    console.dir(jObj);
            }
        },
        getItinerary: function (itinModel) {
            var itinerary = _riTrack.createItinerary(itinModel.Type, itinModel.Recloc, itinModel.FareType, itinModel.TotalFare, itinModel.Miles, itinModel.Distance, itinModel.Duration);
            for (var i = 0; i < itinModel.ItinerarySlices.length; i++) {
                var tSlice = _riTrack.createSlice(),
                        segments = itinModel.ItinerarySlices[i].SliceSegments;

                for (var j = 0; j < segments.length; j++) {
                    var tLeg = _riTrack.createLeg(),
                            seg = segments[j];

                    tLeg.setSegmentNumber(seg.SegmentNumber);
                    tLeg.setSegmentTimeLength(seg.Duration);
                    tLeg.setOriginAirport(seg.DepartureStationCode);
                    tLeg.setOriginAirportName(seg.DepartureStationName);
                    tLeg.setDestAirport(seg.ArrivalStationCode);
                    tLeg.setDestAirportName(seg.ArrivalStationName);
                    tLeg.setMktgCarrier(seg.MarketingCarrierCode);
                    tLeg.setIATAAirlineDesig(null);
                    tLeg.setMktgCarrierNumber(seg.FlightNumber);
                    tLeg.setOptCarrier(seg.OperatingCarrierCode);
                    tLeg.setIATAOptDesig(null);

                    tLeg.setOrigLocalDate(seg.DepartureStationDate);
                    tLeg.setOriginLocalTime(seg.DepartureStationTime);
                    tLeg.setOriginTimezone(seg.DepartureStationUTCTimeOffset);
                    
                    tLeg.setDestLocalDate(seg.ArrivalStationDate);
                    tLeg.setDestLocalTime(seg.ArrivalStationTime);
                    tLeg.setDestTimezone(seg.ArrivalStationUTCTimeOffset);

                    tLeg.setOrigDate(seg.DepartureStationLocalDate);
                    tLeg.setOrigTime(seg.DepartureStationLocalTime);

                    tLeg.setDestDate(seg.ArrivalStationLocalDate);
                    tLeg.setDestTime(seg.ArrivalStationLocalTime);

                    tLeg.setEquipment(seg.Equipment);

                    tSlice.addLeg(tLeg);
                }
                itinerary.addSlice(tSlice);
            }
            return itinerary;
        }
    };
}

var _custIDCookieName = "riTrack_CustomerID", 
    _riAccountCode = as.ResponsysTWRSAccount;

function _riInit() {
    _riTrack = riTrack.init(_riAccountCode);
    _riTrack.setCustIDCookieName(_custIDCookieName);

    if (as.ResponsysCustContactId !== "") {
        _riTrack.setCustomer(as.ResponsysCustContactId);
    }

    as.rTWRS.log('Responsys TWRS code initialized for account id : ' + as.ResponsysTWRSAccount);
    as.rTWRS.log('Responsys TWRS - Customer ContactId  : ' + _customerId);

    as.rTWRS.track();
}
//ends: responsysTWRS.js
//starts: googleanalytics.js
//http://friendlybit.com/js/lazy-loading-asyncronous-javascript/
//http://support.google.com/googleanalytics/bin/answer.py?hl=en&answer=174090
if (typeof (as) != "undefined" && as.IsGoogleAnalyticsDown == false) {
    as.ga = {
        insertAsyncScript: function () {
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o), m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '../../www.google-analytics.com/analytics.js'/*tpa=http://www.google-analytics.com/analytics.js*/, 'ga');

            ga('create', as.GoogleAnalyticsAccount, 'http://www.alaskaair.com/files/alaskaair.com');
            ga('require', 'displayfeatures');
            ga('send', 'pageview');
            as.ga.log('GA code initialized for account id : ' + as.GoogleAnalyticsAccount);
        },
        insertECommerceConversionPixel: function () { return; },
        log: function (message, jObj) {
            if (typeof (window.as) != "undefined" && window.as.hasOwnProperty("Environment") && window.as.Environment != "prod") {
                var console = window.console || { log: function () { }, dir: function () { } };

                if (message != 'undefined' || message != '')
                    console.log(message);

                if ($.browser.msie && jObj)
                    console.log(JSON.stringify(jObj));
                else if (jObj)
                    console.dir(jObj);
            }
        }
    };
}
//ends: googleanalytics.js
//starts: omnitureUtils.js
var omniUtils = {
    console: window.console || { log: function () { return; }, dir: function () { return; } },
    debug: ((typeof (window.as) != "undefined" && as.hasOwnProperty("Environment")) ? as.Environment : '') != 'prod'
};

$(document).ready(function () {
    if (typeof (jQuery) != "undefined") {
        $("a[data-track-link], input[data-track-link]").each(function () {
            var $self = $(this),
                linkData = ($self.attr("data-track-link") != "") ? unescape($self.attr("data-track-link")) : "";

            if (linkData != "") {
                $self.click(function () {
                    var $self = $(this),
                        linkData = (window.JSON) ? JSON.parse(unescape($self.attr("data-track-link"))) : "",
                        pageId = (typeof (window.as) != "undefined" && as.hasOwnProperty("Page")) ? as.Page.pageid : '',
                        s = window.s,
                        href = $self.attr("href"),
                        isUnobtrusiveSameDomain = (linkData.hasOwnProperty("isUnobtrusiveSameDomain") && linkData.isUnobtrusiveSameDomain != "") ? linkData.isUnobtrusiveSameDomain : "";

                    if (linkData != "" && s != "undefined" && (pageId == "reservation" || pageId == "viewpnr")) {
                        var data = [];

                        data.push(pageId);

                        if (linkData.hasOwnProperty("section"))
                            data.push(linkData.section);

                        if (linkData.hasOwnProperty("linkName"))
                            data.push(linkData.linkName);

                        // currently unobtrusive-ness is identified by the dev by looking at the domain name
                        if (href != "" && isUnobtrusiveSameDomain == "true") {
                            var verifyDomain = document.createElement("a");
                            verifyDomain.href = $self.attr("href");

                            var isASDomain = (verifyDomain.hostname.indexOf('http://www.alaskaair.com/files/alaskaair.com') > 1) ? true : false;

                            if (isASDomain && href.indexOf('?') > -1) {
                                $self.attr("href", href + "&lid=" + escape(data.join(':')));
                                return true;
                            }
                            else if (isASDomain && href.indexOf('?') < 0) {
                                $self.attr("href", href + "?lid=" + escape(data.join(':')));
                                return true;
                            }
                        }
                        // just trigger tracking image code for js handled and external anchor links (if not tracked, then set target="_blank" and make it to open in new tab)
                        s.linkTrackVars = 'prop16'; s.linkTrackEvents = 'None'; s.prop16 = data.join(':'); s.tl(this, 'o', data.join(':')); s.prop16 = '';
                    }
                });
            }

            if (omniUtils.debug && window.JSON && !$.browser.msie) {
                omniUtils.console.dir(JSON.parse(linkData));
            }
            else if (omniUtils.debug) {
                omniUtils.console.log(linkData);
            }
        });
    }
});

function trackeVar59(referrerLink) {
    if (s_gi) { var s = s_gi('alaskacom'); s.linkTrackVars = 'eVar59'; s.linkTrackEvents = 'None'; s.eVar59 = referrerLink; s.tl(this, 'o', referrerLink); s.eVar59 = ''; }
    if (omniUtils.debug) { omniUtils.console.log("eVar59 tracked for : " + referrerLink); }
}

function trackeVar60(referrerLink) {
    if (s_gi) { var s = s_gi('alaskacom'); s.linkTrackVars = 'eVar60'; s.linkTrackEvents = 'None'; s.eVar60 = referrerLink; s.tl(this, 'o', referrerLink); s.eVar60 = ''; }
    if (omniUtils.debug) { omniUtils.console.log("eVar60 tracked for : " + referrerLink); }
}
//ends: omnitureUtils.js
//starts: PreventDoubleClicks.js
function do_nothing() { return false; } // prevent a second click for 10 seconds.
$(document).delegate('.nodblclick', 'click', function (e) {
    $(e.target).click(do_nothing);
    setTimeout(function () {
        $(e.target).unbind('click', do_nothing);
    }, 10000);
}); 

//ends: PreventDoubleClicks.js
