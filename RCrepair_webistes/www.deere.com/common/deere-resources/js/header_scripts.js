jQuery(document).ready(function() {
	jQuery('#search').mouseenter(function() {
		jQuery(this).find('.btn').attr('src','Unknown_83_filename'/*tpa=http://www.deere.com/common/deere-resources/js/img/btn_search_over.jpg*/);
	});

	jQuery('#search').mouseleave(function() {
		jQuery(this).find('.btn').attr('src','Unknown_83_filename'/*tpa=http://www.deere.com/common/deere-resources/js/img/btn_search.jpg*/);
	});
	
	/* 
	*  Global Header | Accessible Horizontal Drop Down Menu  
	*/
	accessibleMenuHorizontal("#mainNav");
	
});

/* 
* Global Accessible Horizontal Drop Down Menu 
*/
function accessibleMenuHorizontal(mainNav) {
	var _this = jQuery(mainNav);
	/*
	*  Enable drop down menu on hover
	*/	
	jQuery('li', _this).hover(function() {
		$('.leftColumn ul.MOD_NO_10a li .flyout_dropdown').removeClass('flyout-fixed');
		$('.leftColumn ul.MOD_NO_10a li').removeClass('flyout_hover');
		deereGlobal.advanceFlyout.hideAll();
		jQuery(this).addClass('enable_drop_down');
		jQuery(this).find('a:first').addClass('hover');
	}, function() {
		jQuery(this).removeClass('enable_drop_down');
		jQuery(this).find('a:first').removeClass('hover');
	});
	
	/*
	*  Make dropdown keyboard accessible
	*/	
	jQuery('a', _this).focus(function() {
		jQuery(this).parents('li').addClass('enable_drop_down');
		jQuery(this).parents('li').find('a:first').addClass('hover');
	}).blur(function() {
		jQuery(this).parents('li').removeClass('enable_drop_down');
		jQuery(this).parents('li').find('a:first').removeClass('hover');
	});
	
};