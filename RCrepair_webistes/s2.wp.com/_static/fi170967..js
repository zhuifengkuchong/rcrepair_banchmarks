/* Detect-zoom
 * -----------
 * Cross Browser Zoom and Pixel Ratio Detector
 * Version 1.0.4 | Apr 1 2013
 * dual-licensed under the WTFPL and MIT license
 * Maintained by https://github/tombigel
 * Original developer https://github.com/yonran
 */

//AMD and CommonJS initialization copied from https://github.com/zohararad/audio5js
(function (root, ns, factory) {
    "use strict";

    if (typeof (module) !== 'undefined' && module.exports) { // CommonJS
        module.exports = factory(ns, root);
    } else if (typeof (define) === 'function' && define.amd) { // AMD
        define("factory", function () {
            return factory(ns, root);
        });
    } else {
        root[ns] = factory(ns, root);
    }

}(window, 'detectZoom', function () {

    /**
     * Use devicePixelRatio if supported by the browser
     * @return {Number}
     * @private
     */
    var devicePixelRatio = function () {
        return window.devicePixelRatio || 1;
    };

    /**
     * Fallback function to set default values
     * @return {Object}
     * @private
     */
    var fallback = function () {
        return {
            zoom: 1,
            devicePxPerCssPx: 1
        };
    };
    /**
     * IE 8 and 9: no trick needed!
     * TODO: Test on IE10 and Windows 8 RT
     * @return {Object}
     * @private
     **/
    var ie8 = function () {
        var zoom = Math.round((screen.deviceXDPI / screen.logicalXDPI) * 100) / 100;
        return {
            zoom: zoom,
            devicePxPerCssPx: zoom * devicePixelRatio()
        };
    };

    /**
     * For IE10 we need to change our technique again...
     * thanks https://github.com/stefanvanburen
     * @return {Object}
     * @private
     */
    var ie10 = function () {
        var zoom = Math.round((document.documentElement.offsetHeight / window.innerHeight) * 100) / 100;
        return {
            zoom: zoom,
            devicePxPerCssPx: zoom * devicePixelRatio()
        };
    };

    /**
     * Mobile WebKit
     * the trick: window.innerWIdth is in CSS pixels, while
     * screen.width and screen.height are in system pixels.
     * And there are no scrollbars to mess up the measurement.
     * @return {Object}
     * @private
     */
    var webkitMobile = function () {
        var deviceWidth = (Math.abs(window.orientation) == 90) ? screen.height : screen.width;
        var zoom = deviceWidth / window.innerWidth;
        return {
            zoom: zoom,
            devicePxPerCssPx: zoom * devicePixelRatio()
        };
    };

    /**
     * Desktop Webkit
     * the trick: an element's clientHeight is in CSS pixels, while you can
     * set its line-height in system pixels using font-size and
     * -webkit-text-size-adjust:none.
     * device-pixel-ratio: http://www.webkit.org/blog/55/high-dpi-web-sites/
     *
     * Previous trick (used before http://trac.webkit.org/changeset/100847):
     * documentElement.scrollWidth is in CSS pixels, while
     * document.width was in system pixels. Note that this is the
     * layout width of the document, which is slightly different from viewport
     * because document width does not include scrollbars and might be wider
     * due to big elements.
     * @return {Object}
     * @private
     */
    var webkit = function () {
        var important = function (str) {
            return str.replace(/;/g, " !important;");
        };

        var div = document.createElement('div');
        div.innerHTML = "1<br>2<br>3<br>4<br>5<br>6<br>7<br>8<br>9<br>0";
        div.setAttribute('style', important('font: 100px/1em sans-serif; -webkit-text-size-adjust: none; text-size-adjust: none; height: auto; width: 1em; padding: 0; overflow: visible;'));

        // The container exists so that the div will be laid out in its own flow
        // while not impacting the layout, viewport size, or display of the
        // webpage as a whole.
        // Add !important and relevant CSS rule resets
        // so that other rules cannot affect the results.
        var container = document.createElement('div');
        container.setAttribute('style', important('width:0; height:0; overflow:hidden; visibility:hidden; position: absolute;'));
        container.appendChild(div);

        document.body.appendChild(container);
        var zoom = 1000 / div.clientHeight;
        zoom = Math.round(zoom * 100) / 100;
        document.body.removeChild(container);

        return{
            zoom: zoom,
            devicePxPerCssPx: zoom * devicePixelRatio()
        };
    };

    /**
     * no real trick; device-pixel-ratio is the ratio of device dpi / css dpi.
     * (Note that this is a different interpretation than Webkit's device
     * pixel ratio, which is the ratio device dpi / system dpi).
     *
     * Also, for Mozilla, there is no difference between the zoom factor and the device ratio.
     *
     * @return {Object}
     * @private
     */
    var firefox4 = function () {
        var zoom = mediaQueryBinarySearch('min--moz-device-pixel-ratio', '', 0, 10, 20, 0.0001);
        zoom = Math.round(zoom * 100) / 100;
        return {
            zoom: zoom,
            devicePxPerCssPx: zoom
        };
    };

    /**
     * Firefox 18.x
     * Mozilla added support for devicePixelRatio to Firefox 18,
     * but it is affected by the zoom level, so, like in older
     * Firefox we can't tell if we are in zoom mode or in a device
     * with a different pixel ratio
     * @return {Object}
     * @private
     */
    var firefox18 = function () {
        return {
            zoom: firefox4().zoom,
            devicePxPerCssPx: devicePixelRatio()
        };
    };

    /**
     * works starting Opera 11.11
     * the trick: outerWidth is the viewport width including scrollbars in
     * system px, while innerWidth is the viewport width including scrollbars
     * in CSS px
     * @return {Object}
     * @private
     */
    var opera11 = function () {
        var zoom = window.top.outerWidth / window.top.innerWidth;
        zoom = Math.round(zoom * 100) / 100;
        return {
            zoom: zoom,
            devicePxPerCssPx: zoom * devicePixelRatio()
        };
    };

    /**
     * Use a binary search through media queries to find zoom level in Firefox
     * @param property
     * @param unit
     * @param a
     * @param b
     * @param maxIter
     * @param epsilon
     * @return {Number}
     */
    var mediaQueryBinarySearch = function (property, unit, a, b, maxIter, epsilon) {
        var matchMedia;
        var head, style, div;
        if (window.matchMedia) {
            matchMedia = window.matchMedia;
        } else {
            head = document.getElementsByTagName('head')[0];
            style = document.createElement('style');
            head.appendChild(style);

            div = document.createElement('div');
            div.className = 'mediaQueryBinarySearch';
            div.style.display = 'none';
            document.body.appendChild(div);

            matchMedia = function (query) {
                style.sheet.insertRule('@media ' + query + '{.mediaQueryBinarySearch ' + '{text-decoration: underline} }', 0);
                var matched = getComputedStyle(div, null).textDecoration == 'underline';
                style.sheet.deleteRule(0);
                return {matches: matched};
            };
        }
        var ratio = binarySearch(a, b, maxIter);
        if (div) {
            head.removeChild(style);
            document.body.removeChild(div);
        }
        return ratio;

        function binarySearch(a, b, maxIter) {
            var mid = (a + b) / 2;
            if (maxIter <= 0 || b - a < epsilon) {
                return mid;
            }
            var query = "(" + property + ":" + mid + unit + ")";
            if (matchMedia(query).matches) {
                return binarySearch(mid, b, maxIter - 1);
            } else {
                return binarySearch(a, mid, maxIter - 1);
            }
        }
    };

    /**
     * Generate detection function
     * @private
     */
    var detectFunction = (function () {
        var func = fallback;
        //IE8+
        if (!isNaN(screen.logicalXDPI) && !isNaN(screen.systemXDPI)) {
            func = ie8;
        }
        // IE10+ / Touch
        else if (window.navigator.msMaxTouchPoints) {
            func = ie10;
        }
        //Mobile Webkit
        else if ('orientation' in window && typeof document.body.style.webkitMarquee === 'string') {
            func = webkitMobile;
        }
        //WebKit
        else if (typeof document.body.style.webkitMarquee === 'string') {
            func = webkit;
        }
        //Opera
        else if (navigator.userAgent.indexOf('Opera') >= 0) {
            func = opera11;
        }
        //Last one is Firefox
        //FF 18.x
        else if (window.devicePixelRatio) {
            func = firefox18;
        }
        //FF 4.0 - 17.x
        else if (firefox4().zoom > 0.001) {
            func = firefox4;
        }

        return func;
    }());


    return ({

        /**
         * Ratios.zoom shorthand
         * @return {Number} Zoom level
         */
        zoom: function () {
            return detectFunction().zoom;
        },

        /**
         * Ratios.devicePxPerCssPx shorthand
         * @return {Number} devicePxPerCssPx level
         */
        device: function () {
            return detectFunction().devicePxPerCssPx;
        }
    });
}));

var wpcom_img_zoomer = {
	zoomed: false,
	timer: null,
	interval: 1000, // zoom polling interval in millisecond

	// Should we apply width/height attributes to control the image size?
	imgNeedsSizeAtts: function( img ) {
		// Do not overwrite existing width/height attributes.
		if ( img.getAttribute('width') !== null || img.getAttribute('height') !== null )
			return false;
		// Do not apply the attributes if the image is already constrained by a parent element.
		if ( img.width < img.naturalWidth || img.height < img.naturalHeight )
			return false;
		return true;
	},

	init: function() {
		var t = this;
		try{
			t.zoomImages();
			t.timer = setInterval( function() { t.zoomImages(); }, t.interval );
		}
		catch(e){
		}
	},

	stop: function() {
		if ( this.timer )
			clearInterval( this.timer );
	},

	getScale: function() {
		var scale = detectZoom.device();
		// Round up to 1.5 or the next integer below the cap.
		if      ( scale <= 1.0 ) scale = 1.0;
		else if ( scale <= 1.5 ) scale = 1.5;
		else if ( scale <= 2.0 ) scale = 2.0;
		else if ( scale <= 3.0 ) scale = 3.0;
		else if ( scale <= 4.0 ) scale = 4.0;
		else                     scale = 5.0;
		return scale;
	},

	shouldZoom: function( scale ) {
		var t = this;
		// Do not operate on hidden frames.
		if ( "innerWidth" in window && !window.innerWidth )
			return false;
		// Don't do anything until scale > 1
		if ( scale == 1.0 && t.zoomed == false )
			return false;
		return true;
	},

	zoomImages: function() {
		var t = this;
		var scale = t.getScale();
		if ( ! t.shouldZoom( scale ) ){
			return;
		}
		t.zoomed = true;
		// Loop through all the <img> elements on the page.
		var imgs = document.getElementsByTagName("img");

		for ( var i = 0; i < imgs.length; i++ ) {
			// Wait for original images to load
			if ( "complete" in imgs[i] && ! imgs[i].complete )
				continue;

			// Skip images that don't need processing.
			var imgScale = imgs[i].getAttribute("scale");
			if ( imgScale == scale || imgScale == "0" )
				continue;

			// Skip images that have already failed at this scale
			var scaleFail = imgs[i].getAttribute("scale-fail");
			if ( scaleFail && scaleFail <= scale )
				continue;

			// Skip images that have no dimensions yet.
			if ( ! ( imgs[i].width && imgs[i].height ) )
				continue;

			// Skip images from Lazy Load plugins
			if ( ! imgScale && imgs[i].getAttribute("data-lazy-src") && (imgs[i].getAttribute("data-lazy-src") !== imgs[i].getAttribute("src")))
				continue;

			if ( t.scaleImage( imgs[i], scale ) ) {
				// Mark the img as having been processed at this scale.
				imgs[i].setAttribute("scale", scale);
			}
			else {
				// Set the flag to skip this image.
				imgs[i].setAttribute("scale", "0");
			}
		}
	},

	scaleImage: function( img, scale ) {
		var t = this;
		var newSrc = img.src;

		// Skip slideshow images
		if ( img.parentNode.className.match(/slideshow-slide/) )
			return false;

		// Scale gravatars that have ?s= or ?size=
		if ( img.src.match( /^https?:\/\/([^\/]*\.)?gravatar\.com\/.+[?&](s|size)=/ ) ) {
			newSrc = img.src.replace( /([?&](s|size)=)(\d+)/, function( $0, $1, $2, $3 ) {
				// Stash the original size
				var originalAtt = "originals",
				originalSize = img.getAttribute(originalAtt);
				if ( originalSize === null ) {
					originalSize = $3;
					img.setAttribute(originalAtt, originalSize);
					if ( t.imgNeedsSizeAtts( img ) ) {
						// Fix width and height attributes to rendered dimensions.
						img.width = img.width;
						img.height = img.height;
					}
				}
				// Get the width/height of the image in CSS pixels
				var size = img.clientWidth;
				// Convert CSS pixels to device pixels
				var targetSize = Math.ceil(img.clientWidth * scale);
				// Don't go smaller than the original size
				targetSize = Math.max( targetSize, originalSize );
				// Don't go larger than the service supports
				targetSize = Math.min( targetSize, 512 );
				return $1 + targetSize;
			});
		}

		// Scale resize queries (*.files.wordpress.com) that have ?w= or ?h=
		else if ( img.src.match( /^https?:\/\/([^\/]+)\.files\.wordpress\.com\/.+[?&][wh]=/ ) ) {
			if ( img.src.match( /[?&]crop/ ) )
				return false;
			var changedAttrs = {};
			var matches = img.src.match( /([?&]([wh])=)(\d+)/g );
			for ( var i = 0; i < matches.length; i++ ) {
				var lr = matches[i].split( '=' );
				var thisAttr = lr[0].replace(/[?&]/g, '' );
				var thisVal = lr[1];

				// Stash the original size
				var originalAtt = 'original' + thisAttr, originalSize = img.getAttribute( originalAtt );
				if ( originalSize === null ) {
					originalSize = thisVal;
					img.setAttribute(originalAtt, originalSize);
					if ( t.imgNeedsSizeAtts( img ) ) {
						// Fix width and height attributes to rendered dimensions.
						img.width = img.width;
						img.height = img.height;
					}
				}
				// Get the width/height of the image in CSS pixels
				var size = thisAttr == 'w' ? img.clientWidth : img.clientHeight;
				var naturalSize = ( thisAttr == 'w' ? img.naturalWidth : img.naturalHeight );
				// Convert CSS pixels to device pixels
				var targetSize = Math.ceil(size * scale);
				// Don't go smaller than the original size
				targetSize = Math.max( targetSize, originalSize );
				// Don't go bigger unless the current one is actually lacking
				if ( scale > img.getAttribute("scale") && targetSize <= naturalSize )
					targetSize = thisVal;
				// Don't try to go bigger if the image is already smaller than was requested
				if ( naturalSize < thisVal )
					targetSize = thisVal;
				if ( targetSize != thisVal )
					changedAttrs[ thisAttr ] = targetSize;
			}
			var w = changedAttrs.w || false;
			var h = changedAttrs.h || false;

			if ( w ) {
				newSrc = img.src.replace(/([?&])w=\d+/g, function( $0, $1 ) {
					return $1 + 'w=' + w;
				});
			}
			if ( h ) {
				newSrc = newSrc.replace(/([?&])h=\d+/g, function( $0, $1 ) {
					return $1 + 'h=' + h;
				});
			}
		}

		// Scale mshots that have width
		else if ( img.src.match(/^https?:\/\/([^\/]+\.)*(wordpress|wp)\.com\/mshots\/.+[?&]w=\d+/) ) {
			newSrc = img.src.replace( /([?&]w=)(\d+)/, function($0, $1, $2) {
				// Stash the original size
				var originalAtt = 'originalw', originalSize = img.getAttribute(originalAtt);
				if ( originalSize === null ) {
					originalSize = $2;
					img.setAttribute(originalAtt, originalSize);
					if ( t.imgNeedsSizeAtts( img ) ) {
						// Fix width and height attributes to rendered dimensions.
						img.width = img.width;
						img.height = img.height;
					}
				}
				// Get the width of the image in CSS pixels
				var size = img.clientWidth;
				// Convert CSS pixels to device pixels
				var targetSize = Math.ceil(size * scale);
				// Don't go smaller than the original size
				targetSize = Math.max( targetSize, originalSize );
				// Don't go bigger unless the current one is actually lacking
				if ( scale > img.getAttribute("scale") && targetSize <= img.naturalWidth )
					targetSize = $2;
				if ( $2 != targetSize )
					return $1 + targetSize;
				return $0;
			});
		}

		// Scale simple imgpress queries (s0.wp.com) that only specify w/h/fit
		else if ( img.src.match(/^https?:\/\/([^\/.]+\.)*(wp|wordpress)\.com\/imgpress\?(.+)/) ) {
			var imgpressSafeFunctions = ["zoom", "url", "h", "w", "fit", "filter", "brightness", "contrast", "colorize", "smooth", "unsharpmask"];
			// Search the query string for unsupported functions.
			var qs = RegExp.$3.split('&');
			for ( var q in qs ) {
				q = qs[q].split('=')[0];
				if ( imgpressSafeFunctions.indexOf(q) == -1 ) {
					return false;
				}
			}
			// Fix width and height attributes to rendered dimensions.
			img.width = img.width;
			img.height = img.height;
			// Compute new src
			if ( scale == 1 )
				newSrc = img.src.replace(/\?(zoom=[^&]+&)?/, '?');
			else
				newSrc = img.src.replace(/\?(zoom=[^&]+&)?/, '?zoom=' + scale + '&');
		}

		// Scale LaTeX images or Photon queries (i#.wp.com)
		else if (
			img.src.match(/^https?:\/\/([^\/.]+\.)*(wp|wordpress)\.com\/latex\.php\?(latex|zoom)=(.+)/) ||
			img.src.match(/^https?:\/\/i[\d]{1}\.wp\.com\/(.+)/)
		) {
			// Fix width and height attributes to rendered dimensions.
			img.width = img.width;
			img.height = img.height;
			// Compute new src
			if ( scale == 1 ) {
				newSrc = img.src.replace(/\?(zoom=[^&]+&)?/, '?');
			} else {
				newSrc = img.src;

				var url_var = newSrc.match( /\?w=(\d+)/ );
				if ( url_var !== null && url_var[1] ) {
					newSrc = newSrc.replace( new RegExp( 'w=' + url_var[1] ), 'w=' + img.width );
				}

				url_var = newSrc.match( /\?h=(\d+)/ );
				if ( url_var !== null && url_var[1] ) {
					newSrc = newSrc.replace( new RegExp( 'h=' + url_var[1] ), 'h=' + img.height );
				}

				var zoom_arg = '&zoom=2';
				if ( !newSrc.match( /\?/ ) ) {
					zoom_arg = '?zoom=2';
				}
				img.setAttribute( 'srcset', newSrc + zoom_arg + ' ' + scale + 'x' );
			}
		}

		// Scale static assets that have a name matching *-1x.png or *@1x.png
		else if ( img.src.match(/^https?:\/\/[^\/]+\/.*[-@]([12])x\.(gif|jpeg|jpg|png)(\?|$)/) ) {
			// Fix width and height attributes to rendered dimensions.
			img.width = img.width;
			img.height = img.height;
			var currentSize = RegExp.$1, newSize = currentSize;
			if ( scale <= 1 )
				newSize = 1;
			else
				newSize = 2;
			if ( currentSize != newSize )
				newSrc = img.src.replace(/([-@])[12]x\.(gif|jpeg|jpg|png)(\?|$)/, '$1'+newSize+'x.$2$3');
		}

		else {
			return false;
		}

		// Don't set img.src unless it has changed. This avoids unnecessary reloads.
		if ( newSrc != img.src ) {
			// Store the original img.src
			var prevSrc, origSrc = img.getAttribute("src-orig");
			if ( !origSrc ) {
				origSrc = img.src;
				img.setAttribute("src-orig", origSrc);
			}
			// In case of error, revert img.src
			prevSrc = img.src;
			img.onerror = function(){
				img.src = prevSrc;
				if ( img.getAttribute("scale-fail") < scale )
					img.setAttribute("scale-fail", scale);
				img.onerror = null;
			};
			// Finally load the new image
			img.src = newSrc;
		}

		return true;
	}
};

wpcom_img_zoomer.init();
;
/**
 * Handles toggling the main navigation menu for small screens.
 */
jQuery( document ).ready( function( $ ) {
	var $masthead = $( '#header' ),
	    timeout = false,
		columns_reversed = false;

	$.fn.smallMenu = function() {
		$masthead.find( '.site-navigation' ).removeClass( 'main-navigation' ).addClass( 'main-small-navigation' );
		$masthead.find( '.site-navigation h1' ).removeClass( 'assistive-text' ).addClass( 'menu-toggle' );

		$( '.menu-toggle' ).unbind( 'click' ).click( function() {
			$masthead.find( '.menu' ).toggle();
			$( this ).toggleClass( 'toggled-on' );
		} );
		
		switch_column_positions();
		
	};

	// Check viewport width on first load.
	if ( $( window ).width() < 768 )
		$.fn.smallMenu();

	// Check viewport width when user resizes the browser window.
	$( window ).resize( function() {
		var browserWidth = $( window ).width();

		if ( false !== timeout )
			clearTimeout( timeout );

		timeout = setTimeout( function() {
			if ( browserWidth < 768 ) {
				$.fn.smallMenu();
			} else {
				$masthead.find( '.site-navigation' ).removeClass( 'main-small-navigation' ).addClass( 'main-navigation' );
				$masthead.find( '.site-navigation h1' ).removeClass( 'menu-toggle' ).addClass( 'assistive-text' );
				$masthead.find( '.menu' ).removeAttr( 'style' );
				reverse_column_switch();
			}
		}, 200 );
	} );
	
	function switch_column_positions(){
		
		if( false === columns_reversed ){
			
			var left_col = $('.left-sidebar-widget');
			var right_col = $('.right-sidebar-widget');
			var content = $('#content');
			
			columns_reversed = true;
			
			if($('body').hasClass('page-template-page-sidebar-content-sidebar-php')){
			
				$(right_col).insertAfter( content );
				$(left_col).insertBefore(content);
			}
			else if($('body').hasClass('page-template-page-sidebar-content-php')){
			
				$(left_col).insertBefore(content);
			}
			else if($('body').hasClass('page-template-page-content-sidebar-php')){
			
				$(right_col).insertAfter(content);
			}
		}
		
	}
	
	function reverse_column_switch(){
		
		if( true === columns_reversed ){
			
			var left_col = $('.left-sidebar-widget');
			var right_col = $('.right-sidebar-widget');
			var content = $('#content');
			
			columns_reversed = false;
			
			if( $('body').hasClass('page-template-page-sidebar-content-sidebar-php') ){
			
				$(left_col).insertAfter( content );
				$(right_col).insertBefore(content);
				
			}
			else if( $('body').hasClass('page-template-page-sidebar-content-php') ){
			
				$(left_col).insertAfter(content);
				
			}
			else if( $('body').hasClass('page-template-page-content-sidebar-php') ){
			
				$(right_col).insertBefore(content);
				
			}
			
		}
		
	}
	
} );

/*
 * if ( 768 > window_width ) {
			$( 'nav h1' ).removeClass( 'assistive-text' );
			$( 'nav h1' ).addClass( 'menu-toggle' );
		}
		else{
			$( 'nav h1' ).removeClass( 'menu-toggle' );
			$( 'nav h1' ).addClass( 'assistive-text' );
		}
 * 
 * 
 */;
( function( $ ) {

	/****************************
	 *        #$Variables       *
	 ****************************/
	var window_width = $( window ).width(),
		in_small_menu_mode = false,
		title_visible = true,
		resizing = false,
		news_feed = null,
		banner_text = null;






	/****************************
	 *         #$Events         *
	 ****************************/

	//document ready event
	$( document ).ready( function() {

		window_width = $( window ).width();
		set_up_pages();
		set_up_menus();
	} );

	//on window scroll event
	$( window ).scroll( function( e ) {

		hide_title_on_scroll();
		top_fade();

	} );

	//window resize event
	$( window ).resize( function() {
		resizing = true;
		window_width = $( window ).width();
		$( window ).trigger( 'custom-resize' );
		resizing = false;
	} );






	/****************************
	 *        #$Methods         *
	 ****************************/

	function set_up_pages() {

		var wrapper = $( '#wrapper' );
		banner_text = $( '.bannerText' );
		news_feed = $( '.news-feed' );

		if ( $( 'body' ).hasClass( 'home' ) ) {
			set_up_homepage();
		}
		else if ( $( 'body' ).hasClass( 'page-wholesale-contacts' ) ) {
			set_up_wholesale_contacts();
		}
		else if ( $( 'body' ).hasClass( 'page-commercial' ) ) {
			set_up_commercial_contacts();
		}
		else if ( $( 'body' ).hasClass( 'search-results' ) ) {
			set_up_search_results();
		}

		if ( 0 < $( news_feed ).length ) {
			get_news_feed();
		}

		function set_up_homepage() {

			var slider_caption = $( '.slider-caption' ),
				slider_caption_wrapper = $( '.slider-caption-wrapper' );

			$( wrapper ).wrap( '<div class="home-wrapper"></div>' );
			jcarousel_init();
			make_jcarousel_draggable();
			adjust_home_titlebox();

			$( window ).on( 'custom-resize', function() {
				adjust_home_titlebox();
			} );


			function jcarousel_init() {

				$( '.jcarousel' ).jcarousel( {
					wrap: 'circular'
				} )
					.jcarouselAutoscroll( {
						target: '+=1',
						interval: 9000
					} );

				$( '.jcarousel-control-prev' )
					.on( 'jcarouselcontrol:active', function() {
						$( this ).removeClass( 'inactive' );
					} )
					.on( 'jcarouselcontrol:inactive', function() {
						$( this ).addClass( 'inactive' );
					} )
					.jcarouselControl( {
						target: '-=1'
					} );

				$( '.jcarousel-control-next' )
					.on( 'jcarouselcontrol:active', function() {
						$( this ).removeClass( 'inactive' );
					} )
					.on( 'jcarouselcontrol:inactive', function() {
						$( this ).addClass( 'inactive' );
					} )
					.jcarouselControl( {
						target: '+=1'
					} );

				$( '.jcarousel-pagination' )
					.on( 'jcarouselpagination:active', 'a', function() {
						$( this ).addClass( 'active' );
					} )
					.on( 'jcarouselpagination:inactive', 'a', function() {
						$( this ).removeClass( 'active' );
					} )
					.on( 'click', function( e ) {
						e.preventDefault();
					} )
					.jcarouselPagination( {
						perPage: 1,
						item: function( page ) {
							return '<a href="#' + page + '">' + page + '</a>';
						}
					} );


				$( '.jcarousel-wrapper' ).hover(
					function() {
						$( '.rsArrowIcn' ).show();
					},
					function() {
						$( '.rsArrowIcn' ).hide();
					}
				);

				if ( typeof $.fn.touchwipe === 'function' ) {

					$( '.jcarousel ul' ).touchwipe( {
						wipeLeft: function() {
							$( '.jcarousel-control-next' ).jcarouselControl( {
								target: '+=1'
							} );
						},
						wipeRight: function() {
							$( '.jcarousel-control-prev' ).jcarouselControl( {
								target: '-=1'
							} );
						}
					} );
				}

				$( '.jcarousel' ).on( 'jcarousel:scroll', function( event, carousel, target, animate ) {
					adjust_home_titlebox();
				} );

			}

			function make_jcarousel_draggable() {

				var mousedown = false,
					pos_current_x = 0,
					pos_current_y = 0,
					start_x = parseInt( $( '.jcarousel ul' ).css( 'left' ) ),
					start_y = parseInt( $( '.jcarousel ul' ).css( 'top' ) ),
					end_x,
					end_y,
					diff_x,
					diff_y,
					calc_x,
					calc_y,
					move_x,
					move_y,
					carousel_current_x,
					carousel_current_y;

				//get mouse location
				function mouse_moving( event ) {

					pos_current_x = event.pageX;
					pos_current_y = event.pageY;

					if ( true === mousedown ) {

						end_x = pos_current_x;
						end_y = pos_current_y;
						diff_x = end_x - move_x;
						diff_y = end_y - move_y;
						carousel_current_x = parseInt( $( '.jcarousel ul' ).css( 'left' ) );
						carousel_current_y = parseInt( $( '.jcarousel ul' ).css( 'top' ) );
						calc_x = carousel_current_x + diff_x;
						calc_y = carousel_current_y + diff_y;

						$( '.jcarousel ul' ).css( {
							'left': calc_x
						} );

						move_x = end_x;
						move_y = end_y;
					}
				}

				$( document ).bind( 'mousemove', function( event ) {
					mouse_moving( event );
				} );

				$( '.jcarousel ul' ).on( 'mousedown', function( event ) {

					mousedown = true;
					start_x = move_x = pos_current_x;
					start_y = move_y = pos_current_y;

					event.preventDefault();
				} );

				$( document ).on( 'mouseup', function() {

					if ( true === mousedown ) {

						mousedown = false;
						end_x = pos_current_x;
						end_y = pos_current_y;

						if ( end_x > start_x ) {

							$( '.jcarousel' ).jcarousel( 'scroll', '-=1' );

						}
						else if ( end_x < start_x ) {

							$( '.jcarousel' ).jcarousel( 'scroll', '+=1' );
						}

					}

				} );

			}

			function adjust_home_titlebox() {

				var slide_container_width = $( slider_caption_wrapper ).width();
				var move_amnt = false === resizing ? 30 : 0;
				var opacity_amnt = false === resizing ? 0 : 1;

				if ( 959 < window_width ) {
					var offset = slide_container_width - window_width + 155 + move_amnt;
					$( slider_caption ).css( { 'top': 85, 'right': offset, 'left': 'auto', 'opacity': opacity_amnt } );

					if ( false === resizing ) {

						$( slider_caption ).animate( {
							opacity: 1,
							right: "-=" + move_amnt
						}, 1000, function() {
							// Animation complete.
						} );
					}
				}
				else {
					var new_width = window_width * 0.6;
					var offset = ( ( window_width - new_width ) / 2 ) - move_amnt;
					$( slider_caption ).css( { 'top': 10, 'max-widht': 480, 'width': new_width, 'left': offset, 'right': 'auto', 'opacity': opacity_amnt } );

					if ( false === resizing ) {
						$( slider_caption ).animate( {
							opacity: 1,
							left: "+=" + move_amnt
						}, 1000, function() {
							// Animation complete.
						} );
					}
				}
			}

		}

		function set_up_wholesale_contacts() {

			var form_html = $( '<form name="myform"></form>' );
			var select_dd = $( '<select name="Contacts" onchange="disp_divb()"><option selected="selected" value="Headquarters">Headquarters</option><option value="California">California</option><option value="Southern California">Southern California</option><option value="Northern California">Northern California</option><option value="Pacific Northwest and Alaska Region">Pacific Northwest and Alaska Region</option><option value="Oregon">Oregon</option><option value="Washington">Washington</option><option value="Northern Great Plains, Mountain Region">Northern Great Plains, Mountain Region</option><option value="Utah, Idaho">Utah, Idaho</option><option value="North Dakota, South Dakota, Minnesota">North Dakota, South Dakota, Minnesota</option><option value="Alaska">Alaska</option></select>' );
			$( '#select-contacts-div div' ).css( 'display', 'none' );
			$( '#select-contacts-div #a' ).css( 'display', 'block' );
			$( '#select-contacts-div' ).prepend( select_dd );
			$( '#select-contacts-div' ).wrap( form_html );
		}

		function set_up_commercial_contacts() {

			var form_html = $( '<form name="myform"></form>' );
			var select_dd = $( '<select name="Contacts" onchange="disp_div()"><option value="Choose one...">Choose one...</option><option value="Commercial">Commercial</option><option value="Crude Oil Trading and Strategy">Crude Oil Trading and Strategy</option><option value="Products Trading and Sales">Products Trading and Sales</option><option value="Gas, Power and Specialty Products">Gas, Power and Specialty Products</option></select>' );
			$( '#select-contacts-div div' ).css( 'display', 'none' );
			$( '#select-contacts-div' ).prepend( select_dd );
			$( '#select-contacts-div' ).wrap( form_html );
		}

		function set_up_search_results() {

			$( wrapper ).wrap( '<div class="search-wrapper"></div>' );

		}

	}

	function set_up_menus() {

		set_up_responsive_menu();
		create_primary_sub_menus();
		set_up_accordion_menus();
		$( window ).on( 'custom-resize', function() {
			toggle_responisive_menu();
		} );

		function set_up_responsive_menu() {

			if ( 768 > window_width ) {
				in_small_menu_mode = true;
				$( '#mini-logo' ).appendTo( '#logo-nav-container' );
				$( '#top-widget .widget_search' ).appendTo( '#logo-nav-container' );
			}

		}

		function create_primary_sub_menus() {

			var sub_menus_wrapper = $( '<div id="sub-primary-menu-wrapper"></div>' );
			var sub_menus_arr = $( '.sub-primary-menu' );
			var menu_li_arr = $( '#menu-primary-menu > li' );

			$( '#sub-primary-menu-wrapper' ).css( { 'height': '0px', 'overflow': 'hidden' } );

			//move submenus into place
			$( sub_menus_wrapper ).append( sub_menus_arr );
			$( '.menu-primary-menu-container' ).after( sub_menus_wrapper );

			//about
			set_up_hover( $( '.aboutmenu' ), $( menu_li_arr[0] ) );

			//refining
			set_up_hover( $( '.refiningmenu' ), $( menu_li_arr[1] ) );

			//customers
			set_up_hover( $( '.customermenu' ), $( menu_li_arr[2] ) );

			//responsibility
			set_up_hover( $( '.responsemenu' ), $( menu_li_arr[3] ) );

			//careers
			set_up_hover( $( '.careermenu' ), $( menu_li_arr[4] ) );

			//investors
			set_up_hover( $( '.investorsmenu' ), $( menu_li_arr[5] ) );


			function set_up_hover( sub_menu_item, menu_li ) {

				var sub_height = sub_menu_item.height();
				sub_menu_item.css( 'height', '0px' );

				menu_li.hover(
					function() {
						sub_menu_item.show();
						menu_li.css( { 'background-color': '#13367f', 'border': '1px solid #13367f' } );
						menu_li.find( 'a' ).css( 'color', '#fff' );
						sub_menu_item.animate( { height: sub_height }, 600 );
					},
					function() {
						sub_menu_item.hide();
						menu_li.css( { 'background-color': '', 'border': '' } );
						menu_li.find( 'a' ).css( 'color', '' );
						sub_menu_item.hide();
					}
				);

				sub_menu_item.hover(
					function() {
						menu_li.css( 'background-color', '#13367f' );
						menu_li.find( 'a' ).css( 'color', '#fff' );
						menu_li.css( { 'background-color': '#13367f', 'border': '1px solid #13367f' } );
						sub_menu_item.show();
					},
					function() {
						menu_li.css( 'background-color', '' );
						menu_li.find( 'a' ).css( 'color', '' );
						menu_li.css( { 'background-color': '', 'border': '' } );
						sub_menu_item.css( 'display', 'none' );
					}

				);

			}
		}

		function set_up_accordion_menus() {

			$( '.left-sidebar-widget ul li' ).not( '.left-sidebar-widget ul ul li' ).mouseenter( function() {
				$( this ).find( 'ul' ).slideDown();
			} );

			$( '.left-sidebar-widget ul li' ).not( '.left-sidebar-widget ul ul li' ).mouseleave( function() {
				$( this ).find( 'ul' ).slideUp();
			} );

			$( '.left-sidebar-widget ul li' ).not( '.left-sidebar-widget ul ul li' ).each( function() {
				if ( 0 < $( this ).find( 'ul' ).length ) {
					$( this ).css( 'list-style-image', "url('../wp-content/themes/vip/tesoro/images/tsocorp/global/pixel-arrow.png'/*tpa=https://s2.wp.com/wp-content/themes/vip/tesoro/images/tsocorp/global/pixel-arrow.png*/)" );
				}
			} );

		}

		function toggle_responisive_menu() {

			if ( ( 768 > window_width ) && ( false === in_small_menu_mode ) ) {
				in_small_menu_mode = true;
				$( '#mini-logo' ).appendTo( '#logo-nav-container' );
				$( '#top-widget .widget_search' ).appendTo( '#logo-nav-container' );
			}
			else if ( ( 767 < window_width ) && ( true === in_small_menu_mode ) ) {
				in_small_menu_mode = false;
				$( '#mini-logo' ).appendTo( '#top-widget' );
				$( '#logo-nav-container .widget_search' ).appendTo( '#top-widget' );
			}
		}

	}

	function hide_title_on_scroll() {

		var scroll_val = $( window ).scrollTop();
		var opacity_changer = ( scroll_val / 300 ) * 2.6;

		if ( parseInt( screen.width ) > 960 )
		{
			$( banner_text ).css( 'opacity', 1 - opacity_changer );
		}
		else
		{
			$( banner_text ).css( { 'position': 'absolute', 'top': '80px' } );
		}
	}

	function top_fade() {

		var scr_val = $( window ).scrollTop();

		// for Computers
		if ( parseInt( screen.width ) > 960 ) {

			if ( scr_val > 100 ) {

				//fadeIn effect
				$( '#top-hidden' ).fadeIn( 400, function callback() {
					//logoslide down
					$( '.logoSub' ).clearQueue().stop().animate( { 'top': 12, opacity: 1 }, 400 );
				} );

			}
			else if ( scr_val < 480 ) {

				$( '#top-hidden' ).fadeOut( 400, function callback() {

					$( '.logoSub' ).clearQueue().stop().animate( { 'top': -20, opacity: 0 }, 200 );

				} );
			}

		}

		if ( 150 < scr_val && true === title_visible ) {
			title_visible = false;
			$( banner_text ).hide();
		}
		else if ( 151 > scr_val && false === title_visible ) {
			title_visible = true;
			$( banner_text ).show();
		}

	}

	function get_news_feed() {

		$( news_feed ).rssfeed( 'http://phx.corporate-ir.net/corporate.rss?c=79122&Rule=Cat=news~subcat=ALL' );

	}

} )( jQuery );

function disp_div() {
	var word = document.myform.Contacts.selectedIndex;
	var selected_text = document.myform.Contacts.options[word].text;
	var a = document.getElementById( "a" );
	var b = document.getElementById( "b" );
	var c = document.getElementById( "c" );
	var d = document.getElementById( "d" );
	var e = document.getElementById( "e" );
	var f = document.getElementById( "f" );
	var g = document.getElementById( "g" );
	if ( selected_text == 'Commercial' ) {
		a.style.display = 'block';
		b.style.display = 'none';
		c.style.display = 'none';
		d.style.display = 'none';
		e.style.display = 'none';
		f.style.display = 'none';
		g.style.display = 'none';
	} else if ( selected_text == 'Crude Oil Trading and Strategy' ) {
		a.style.display = 'none';
		b.style.display = 'block';
		c.style.display = 'none';
		d.style.display = 'none';
		e.style.display = 'none';
		f.style.display = 'none';
		g.style.display = 'none';
	} else if ( selected_text == "Products Trading and Sales" ) {
		a.style.display = 'none';
		b.style.display = 'none';
		c.style.display = 'block';
		d.style.display = 'none';
		e.style.display = 'none';
		f.style.display = 'none';
		g.style.display = 'none';
	} else if ( selected_text == 'Gas, Power and Specialty Products' ) {
		a.style.display = 'none';
		b.style.display = 'none';
		c.style.display = 'none';
		d.style.display = 'block';
		e.style.display = 'none';
		f.style.display = 'none';
		g.style.display = 'none';
	} else if ( selected_text == "Light Products Trading" ) {
		a.style.display = 'none';
		b.style.display = 'none';
		c.style.display = 'none';
		d.style.display = 'none';
		e.style.display = 'block';
		f.style.display = 'none';
		g.style.display = 'none';
	} else if ( selected_text == "Heavy Oils and Specialty Products Trading" ) {
		a.style.display = 'none';
		b.style.display = 'none';
		c.style.display = 'none';
		d.style.display = 'none';
		e.style.display = 'none';
		f.style.display = 'block';
		g.style.display = 'none';
	} else if ( selected_text == "Commodity Derivatives" ) {
		a.style.display = 'none';
		b.style.display = 'none';
		c.style.display = 'none';
		d.style.display = 'none';
		e.style.display = 'none';
		f.style.display = 'none';
		g.style.display = 'block';
	} else if ( selected_text == "Choose one..." ) {
		a.style.display = 'none';
		b.style.display = 'none';
		c.style.display = 'none';
		d.style.display = 'none';
		e.style.display = 'none';
		f.style.display = 'none';
		g.style.display = 'none';
	}
}

function disp_divb() {
	var word = document.myform.Contacts.selectedIndex;
	var selected_text = document.myform.Contacts.options[word].text;
	var a = document.getElementById( "a" );
	var b = document.getElementById( "b" );
	var c = document.getElementById( "c" );
	var d = document.getElementById( "d" );
	var e = document.getElementById( "e" );
	var f = document.getElementById( "f" );
	var g = document.getElementById( "g" );
	var h = document.getElementById( "h" );
	var i = document.getElementById( "i" );
	var j = document.getElementById( "j" );
	var k = document.getElementById( "k" );
	var l = document.getElementById( "l" );

	if ( selected_text == 'Headquarters' ) {
		a.style.display = 'block';
		b.style.display = 'none';
		c.style.display = 'none';
		d.style.display = 'none';
		e.style.display = 'none';
		f.style.display = 'none';
		g.style.display = 'none';
		h.style.display = 'none';
		i.style.display = 'none';
		j.style.display = 'none';
		k.style.display = 'none';
		l.style.display = 'none';
	} else if ( selected_text == 'California' ) {
		a.style.display = 'none';
		b.style.display = 'block';
		c.style.display = 'none';
		d.style.display = 'none';
		e.style.display = 'none';
		f.style.display = 'none';
		g.style.display = 'none';
		h.style.display = 'none';
		i.style.display = 'none';
		j.style.display = 'none';
		k.style.display = 'none';
		l.style.display = 'none';
	} else if ( selected_text == 'Southern California' ) {
		a.style.display = 'none';
		b.style.display = 'none';
		c.style.display = 'block';
		d.style.display = 'none';
		e.style.display = 'none';
		f.style.display = 'none';
		g.style.display = 'none';
		h.style.display = 'none';
		i.style.display = 'none';
		j.style.display = 'none';
		k.style.display = 'none';
		l.style.display = 'none';
	} else if ( selected_text == 'Northern California' ) {
		a.style.display = 'none';
		b.style.display = 'none';
		c.style.display = 'none';
		d.style.display = 'block';
		e.style.display = 'none';
		f.style.display = 'none';
		g.style.display = 'none';
		h.style.display = 'none';
		i.style.display = 'none';
		j.style.display = 'none';
		k.style.display = 'none';
		l.style.display = 'none';
	} else if ( selected_text == 'Pacific Northwest and Alaska Region' ) {
		a.style.display = 'none';
		b.style.display = 'none';
		c.style.display = 'none';
		d.style.display = 'none';
		e.style.display = 'block';
		f.style.display = 'none';
		g.style.display = 'none';
		h.style.display = 'none';
		i.style.display = 'none';
		j.style.display = 'none';
		k.style.display = 'none';
		l.style.display = 'none';
	} else if ( selected_text == 'Oregon' ) {
		a.style.display = 'none';
		b.style.display = 'none';
		c.style.display = 'none';
		d.style.display = 'none';
		e.style.display = 'none';
		f.style.display = 'block';
		g.style.display = 'none';
		h.style.display = 'none';
		i.style.display = 'none';
		j.style.display = 'none';
		k.style.display = 'none';
		l.style.display = 'none';
	} else if ( selected_text == 'Washington' ) {
		a.style.display = 'none';
		b.style.display = 'none';
		c.style.display = 'none';
		d.style.display = 'none';
		e.style.display = 'none';
		f.style.display = 'none';
		g.style.display = 'block';
		h.style.display = 'none';
		i.style.display = 'none';
		j.style.display = 'none';
		k.style.display = 'none';
		l.style.display = 'none';
	} else if ( selected_text == 'Northern Great Plains, Mountain Region' ) {
		a.style.display = 'none';
		b.style.display = 'none';
		c.style.display = 'none';
		d.style.display = 'none';
		e.style.display = 'none';
		f.style.display = 'none';
		g.style.display = 'none';
		h.style.display = 'block';
		i.style.display = 'none';
		j.style.display = 'none';
		k.style.display = 'none';
		l.style.display = 'none';
	} else if ( selected_text == 'Utah, Idaho' ) {
		a.style.display = 'none';
		b.style.display = 'none';
		c.style.display = 'none';
		d.style.display = 'none';
		e.style.display = 'none';
		f.style.display = 'none';
		g.style.display = 'none';
		h.style.display = 'none';
		i.style.display = 'block';
		j.style.display = 'none';
		k.style.display = 'none';
		l.style.display = 'none';
	} else if ( selected_text == 'North Dakota, South Dakota, Minnesota' ) {
		a.style.display = 'none';
		b.style.display = 'none';
		c.style.display = 'none';
		d.style.display = 'none';
		e.style.display = 'none';
		f.style.display = 'none';
		g.style.display = 'none';
		h.style.display = 'none';
		i.style.display = 'none';
		j.style.display = 'block';
		k.style.display = 'none';
		l.style.display = 'none';
	} else if ( selected_text == 'Alaska' ) {
		a.style.display = 'none';
		b.style.display = 'none';
		c.style.display = 'none';
		d.style.display = 'none';
		e.style.display = 'none';
		f.style.display = 'none';
		g.style.display = 'none';
		h.style.display = 'none';
		i.style.display = 'none';
		j.style.display = 'none';
		k.style.display = 'block';
		l.style.display = 'none';
	} else if ( selected_text == 'Hawaii' ) {
		a.style.display = 'none';
		b.style.display = 'none';
		c.style.display = 'none';
		d.style.display = 'none';
		e.style.display = 'none';
		f.style.display = 'none';
		g.style.display = 'none';
		h.style.display = 'none';
		i.style.display = 'none';
		j.style.display = 'none';
		k.style.display = 'none';
		l.style.display = 'block';
	}

};
(function($){/*


 Copyright 2012 Google Inc.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
'use strict';var e=Math,f="getLocation",h="trigger",k="bindTo",l="removeListener",n="geometry",p="attr",q="getBounds",r="find",s="addListener",t="maps",u="getMap",v="contains",w="push",x="addClass",y="getCenter",A="click",B="distanceTo",C="highlight",E="length",F="prototype",G="getId",H="getMarker",I="setMap",J="append",K="join",L="event";function M(a){return function(){return this[a]}}var N;function O(){}window.storeLocator=O;function P(a){return a*e.PI/180};function Q(a,b){this.b=a;this.a=b}O.Feature=Q;Q[F].getId=M("b");Q[F].getDisplayName=M("a");Q[F].toString=function(){return this.getDisplayName()};function R(a){this.a=[];this.b={};for(var b=0,c;c=arguments[b];b++)this.add(c)}O.FeatureSet=R;N=R[F];N.toggle=function(a){this[v](a)?this.remove(a):this.add(a)};N.contains=function(a){return a[G]()in this.b};N.getById=function(a){return a in this.b?this.a[this.b[a]]:null};N.add=function(a){a&&(this.a[w](a),this.b[a[G]()]=this.a[E]-1)};N.remove=function(a){this[v](a)&&(this.a[this.b[a[G]()]]=null,delete this.b[a[G]()])};
N.asList=function(){for(var a=[],b=0,c=this.a[E];b<c;b++){var d=this.a[b];null!==d&&a[w](d)}return a};var aa=new R;function S(a){this.c=a.tableId;this.b=a.apiKey;a.propertiesModifier&&(this.a=a.propertiesModifier)}O.GMEDataFeed=S;
S[F].getStores=function(a,b,c){var d=this,g=a[y]();a="(ST_INTERSECTS(geometry, "+ca(a)+") OR ST_DISTANCE(geometry, "+da(g)+") < 20000)";$.getJSON("https://www.googleapis.com/mapsengine/v1/tables/"+this.c+"/features?callback=?",{key:this.b,where:a,version:"published",maxResults:300},function(a){if(a.error)window.alert(a.error.message),a=[];else if(a=a.features){for(var b=[],ba=0,D;D=a[ba];ba++){var T=D[n].coordinates,T=new google[t].LatLng(T[1],T[0]);D=d.a(D.properties);b[w](new U(D.id,T,null,D))}a=
b}else a=[];ea(g,a);c(a)})};function da(a){return"ST_POINT("+a.lng()+", "+a.lat()+")"}function ca(a){var b=a.getNorthEast();a=a.getSouthWest();return["ST_GEOMFROMTEXT('POLYGON ((",a.lng()," ",a.lat(),", ",b.lng()," ",a.lat(),", ",b.lng()," ",b.lat(),", ",a.lng()," ",b.lat(),", ",a.lng()," ",a.lat(),"))')"][K]("")}S[F].a=function(a){return a};function ea(a,b){b.sort(function(b,d){return b[B](a)-d[B](a)})};function V(a,b){this.g=$(a);this.g[x]("storelocator-panel");this.c=$.extend({locationSearch:!0,locationSearchLabel:"Enter a location & select a brand",featureFilter:!0,directions:!0,view:null},b);this.l=new google[t].DirectionsRenderer({draggable:!0});this.s=new google[t].DirectionsService;fa(this)}O.Panel=V;V.prototype=new google[t].MVCObject;
function fa(a){a.c.view&&a.set("view",a.c.view);a.e=$('<form class="storelocator-filter"/>');a.g[J](a.e);a.c.locationSearch&&(a.i=$('<div class="location-search"><h4>'+a.c.locationSearchLabel+"</h4><input></div>"),a.e[J](a.i),"undefined"!=typeof google[t].places?ga(a):a.e.submit(function(){var b=$("input",a.i).val();a.searchPosition(b)}),a.e.submit(function(){return!1}),google[t][L][s](a,"geocode",function(b){if(b[n]){this.k=b[n].location;a.h&&W(a);var c=a.get("view");c[C](null);var d=c[u]();b[n].viewport?
d.fitBounds(b[n].viewport):(d.setCenter(b[n].location),d.setZoom(13));c.refreshView();X(a)}else a.searchPosition(b.name)}));if(a.c.featureFilter){a.d=$('<div class="feature-filter"/>');for(var b=a.get("view").getFeatures().asList(),c=0,d=b[E];c<d;c++){var g=b[c],m=$('<input type="checkbox"/>');m.data("feature",g);$("<label/>")[J](m)[J](g.getDisplayName()).appendTo(a.d)}a.e[J](a.d);a.d[r]("input").change(function(){var b=$(this).data("feature"),c=a.get("featureFilter");c.toggle(b);a.set("featureFilter",
c);a.get("view").refreshView()})}a.b=$('<ul class="store-list"/>');a.g[J](a.b);a.c.directions&&(a.a=$('<div class="directions-panel"><form><input class="directions-to"/><input type="submit" value="Find directions"/><a href="#" class="close-directions">Close</a></form><div class="rendered-directions"></div></div>'),a.a[r](".directions-to")[p]("readonly","readonly"),a.a.hide(),a.h=!1,a.a[r]("form").submit(function(){W(a);return!1}),a.a[r](".close-directions")[A](function(){a.hideDirections()}),a.g[J](a.a))}
var ha=new google[t].Geocoder;function X(a){var b=a.get("view");a.r&&google[t][L][l](a.r);a.r=google[t][L].addListenerOnce(b,"stores_changed",function(){a.set("stores",b.get("stores"))})}N=V[F];N.searchPosition=function(a){var b=this;a={address:a,bounds:this.get("view")[u]()[q]()};ha.geocode(a,function(a,d){d==google[t].GeocoderStatus.OK&&google[t][L][h](b,"geocode",a[0])})};N.setView=function(a){this.set("view",a)};
N.view_changed=function(){function a(){b.clearMarkers();X(c)}var b=this.get("view");this[k]("selectedStore",b);var c=this;this.o&&google[t][L][l](this.o);this.q&&google[t][L][l](this.q);this.p&&google[t][L][l](this.p);b[u]();this.o=google[t][L][s](b,"load",a);this.q=google[t][L][s](b[u](),"zoom_changed",a);this.p=google[t][L][s](b[u](),"idle",function(){var a=b[u]();c.j?a[q]()[v](c.j)||(c.j=a[y](),X(c)):c.j=a[y]()});a();this[k]("featureFilter",b);this.f&&this.f[k]("bounds",b[u]())};
function ga(a){var b=$("input",a.i)[0];a.f=new google[t].places.Autocomplete(b);a.get("view")&&a.f[k]("bounds",a.get("view")[u]());google[t][L][s](a.f,"place_changed",function(){google[t][L][h](a,"geocode",this.getPlace())})}
N.stores_changed=function(){if(this.get("stores")){var a=this.get("view"),b=a&&a[u]()[q](),c=this.get("stores"),d=this.get("selectedStore");this.b.empty();c[E]?b&&!b[v](c[0][f]())&&this.b[J]('<li class="no-stores">There are no stores in this area. However, stores closest to you are listed below.</li>'):this.b[J]('<li class="no-stores">There are no stores in this area.</li>');for(var b=function(){a[C](this.store,!0)},g=0,m=e.min(10,c[E]);g<m;g++){var z=c[g].getInfoPanelItem();z.store=c[g];d&&c[g][G]()==
d[G]()&&$(z)[x]("highlighted");z.t||(z.t=google[t][L].addDomListener(z,"click",b));this.b[J](z)}}};
N.selectedStore_changed=function(){$(".highlighted",this.b).removeClass("highlighted");var a=this,b=this.get("selectedStore");if(b){this.m=b;this.b[r]("#store-"+b[G]())[x]("highlighted");this.c.directions&&this.a[r](".directions-to").val(b.getDetails().title);var c=a.get("view").getInfoWindow().getContent(),d=$("<a/>").text("Directions")[p]("href","#")[x]("action")[x]("directions"),g=$("<a/>").text("Zoom here")[p]("href","#")[x]("action")[x]("zoomhere"),m=$("<a/>").text("Street view")[p]("href","#")[x]("action")[x]("streetview");
d[A](function(){a.showDirections();return!1});g[A](function(){a.get("view")[u]().setOptions({center:b[f](),zoom:16})});m[A](function(){var c=a.get("view")[u]().getStreetView();c.setPosition(b[f]());c.setVisible(!0)});$(c)[J](d)[J](g)[J](m)}};N.hideDirections=function(){this.h=!1;this.a.fadeOut();this.d.fadeIn();this.b.fadeIn();this.l[I](null)};
N.showDirections=function(){var a=this.get("selectedStore");this.d.fadeOut();this.b.fadeOut();this.a[r](".directions-to").val(a.getDetails().title);this.a.fadeIn();W(this);this.h=!0};function W(a){if(a.k&&a.m){var b=a.a[r](".rendered-directions").empty();a.s.route({origin:a.k,destination:a.m[f](),travelMode:google[t].DirectionsTravelMode.DRIVING},function(c,d){if(d==google[t].DirectionsStatus.OK){var g=a.l;g.setPanel(b[0]);g[I](a.get("view")[u]());g.setDirections(c)}})}}N.featureFilter_changed=function(){X(this)};function Y(){this.b=[]}O.StaticDataFeed=Y;Y[F].setStores=function(a){this.b=a;this.a?this.a():delete this.a};Y[F].getStores=function(a,b,c){if(this.b[E]){for(var d=[],g=0,m;m=this.b[g];g++)m.hasAllFeatures(b)&&d[w](m);ia(a[y](),d);c(d)}else{var z=this;this.a=function(){z.getStores(a,b,c)}}};function ia(a,b){b.sort(function(b,d){return b[B](a)-d[B](a)})};/*

  Latitude/longitude spherical geodesy formulae & scripts
  (c) Chris Veness 2002-2010
  www.movable-type.co.uk/scripts/latlong.html
*/
function U(a,b,c,d){this.f=a;this.d=b;this.a=c||aa;this.b=d||{}}O.Store=U;N=U[F];N.setMarker=function(a){this.e=a;google[t][L][h](this,"marker_changed",a)};N.getMarker=M("e");N.getId=M("f");N.getLocation=M("d");N.getFeatures=M("a");N.hasFeature=function(a){return this.a[v](a)};N.hasAllFeatures=function(a){if(!a)return!0;a=a.asList();for(var b=0,c=a[E];b<c;b++)if(this.hasFeature(a[b]))return!0;return!1};N.getDetails=M("b");
function ja(a){for(var b=["title","address","phone","misc","web"],c=[],d=0,g=b[E];d<g;d++){var m=b[d];a.b[m]&&(c[w]('<div class="'),c[w](m),c[w]('">'),c[w](a.b[m]),c[w]("</div>"))}return c[K]("")}function ka(a){var b=[];b[w]('<ul class="features">');a=a.a.asList();for(var c=0,d;d=a[c];c++)b[w]("<li>"),b[w](d.getDisplayName()),b[w]("</li>");b[w]("</ul>");return b[K]("")}N.getInfoWindowContent=function(){if(!this.c){var a=['<div class="store">'];a[w](ja(this));a[w](ka(this));a[w]("</div>");this.c=a[K]("")}return this.c};
N.getInfoPanelContent=function(){return this.getInfoWindowContent()};var Z={};U[F].getInfoPanelItem=function(){var a=this[G]();if(!Z[a]){var b=this.getInfoPanelContent();Z[a]=$('<li class="store" id="store-'+this[G]()+'">'+b+"</li>")[0]}return Z[a]};U[F].distanceTo=function(a){var b=this[f](),c=P(b.lat()),d=P(b.lng()),b=P(a.lat()),g=P(a.lng());a=b-c;d=g-d;c=e.sin(a/2)*e.sin(a/2)+e.cos(c)*e.cos(b)*e.sin(d/2)*e.sin(d/2);return 12742*e.atan2(e.sqrt(c),e.sqrt(1-c))};function la(){}O.DataFeed=la;la[F].getStores=function(){};function ma(a,b,c){this.g=a;this.f=b;this.b=$.extend({updateOnPan:!0,geolocation:!0,features:new R},c);na(this);google[t][L][h](this,"load");this.set("featureFilter",new R)}O.View=ma;ma.prototype=new google[t].MVCObject;
function oa(a){window.navigator&&navigator.geolocation&&navigator.geolocation.getCurrentPosition(function(b){b=new google[t].LatLng(b.coords.latitude,b.coords.longitude);a[u]().setCenter(b);a[u]().setZoom(11);google[t][L][h](a,"load")},void 0,{maximumAge:6E4,timeout:1E4})}
function na(a){a.b.geolocation&&oa(a);a.c={};a.a=new google[t].InfoWindow;var b=a[u]();a.set("updateOnPan",a.b.updateOnPan);google[t][L][s](a.a,"closeclick",function(){a[C](null)});google[t][L][s](b,"click",function(){a[C](null);a.a.close()})}N=ma[F];N.updateOnPan_changed=function(){this.e&&google[t][L][l](this.e);if(this.get("updateOnPan")&&this[u]()){var a=this,b=this[u]();this.e=google[t][L][s](b,"idle",function(){a.refreshView()})}};
N.addStoreToMap=function(a){var b=this[H](a);a.setMarker(b);var c=this;b.n=google[t][L][s](b,"click",function(){c[C](a,!1)});b[u]()!=this[u]()&&b[I](this[u]())};N.createMarker=function(a){a={position:a[f]()};var b=this.b.markerIcon;b&&(a.icon=b);return new google[t].Marker(a)};N.getMarker=function(a){var b=this.c,c=a[G]();b[c]||(b[c]=this.createMarker(a));return b[c]};N.getInfoWindow=function(a){if(!a)return this.a;a=$(a.getInfoWindowContent());this.a.setContent(a[0]);return this.a};
N.getFeatures=function(){return this.b.features};N.getFeatureById=function(a){if(!this.d){this.d={};for(var b=0,c;c=this.b.features[b];b++)this.d[c[G]()]=c}return this.d[a]};N.featureFilter_changed=function(){google[t][L][h](this,"featureFilter_changed",this.get("featureFilter"));this.get("stores")&&this.clearMarkers()};N.clearMarkers=function(){for(var a in this.c){this.c[a][I](null);var b=this.c[a].n;b&&google[t][L][l](b)}};
N.refreshView=function(){var a=this;this.f.getStores(this[u]()[q](),this.get("featureFilter"),function(b){var c=a.get("stores");if(c)for(var d=0,g=c[E];d<g;d++)google[t][L][l](c[d][H]().n);a.set("stores",b)})};N.stores_changed=function(){for(var a=this.get("stores"),b=0,c;c=a[b];b++)this.addStoreToMap(c)};N.getMap=M("g");
N.highlight=function(a,b){var c=this.getInfoWindow(a);a?(c=this.getInfoWindow(a),a[H]()?c.open(this[u](),a[H]()):(c.setPosition(a[f]()),c.open(this[u]())),b&&this[u]().panTo(a[f]()),this[u]().getStreetView().getVisible()&&this[u]().getStreetView().setPosition(a[f]())):c.close();this.set("selectedStore",a)};N.selectedStore_changed=function(){google[t][L][h](this,"selectedStore_changed",this.get("selectedStore"))};})(jQuery);
/**
 * @extends storeLocator.StaticDataFeed
 * @constructor
 */
function DataSource() {
	jQuery.extend( this, new storeLocator.StaticDataFeed );

	var that = this;
	var csv_url = 'http://' + location.host + '/wp-content/themes/vip/tesoro/other_assets/LocatorStoresList.csv';
	jQuery.get( csv_url, function( data ) {
		that.setStores( that.parse_( data ) );
	} );
}

/**
 * @const
 * @type {!storeLocator.FeatureSet}
 * @private
 */
DataSource.prototype.FEATURES_ = new storeLocator.FeatureSet(
	new storeLocator.Feature( 'Brand_Tesoro-YES', 'Tesoro' ),
	new storeLocator.Feature( 'Brand_Shell-YES', 'Shell' ),
	new storeLocator.Feature( 'Brand_Arco-YES', 'ARCO' ),
	new storeLocator.Feature( 'Brand_USA-YES', 'USA Gasoline' ),
	new storeLocator.Feature( 'Brand_Exxon-YES', 'Exxon' ),
	new storeLocator.Feature( 'Brand_Mobil-YES', 'Mobil' )

	);

/**
 * @return {!storeLocator.FeatureSet}
 */
DataSource.prototype.getFeatures = function() {
	return this.FEATURES_;
};

DataSource.prototype.getStores = function( bounds, features, callback ) {
	callback( this.stores );
};

/**
 * @private
 * @param {string} csv
 * @return {!Array.<!storeLocator.Store>}
 */
DataSource.prototype.parse_ = function( csv ) {
	var stores = [ ];
	var rows = csv.split( '\n' );
	var headings = this.parseRow_( rows[0] );

	for ( var i = 1, row; row = rows[i]; i++ ) {
		row = this.toObject_( headings, this.parseRow_( row ) );
		var features = new storeLocator.FeatureSet;
		features.add( this.FEATURES_.getById( 'Brand_Tesoro-' + row.Brand_Tesoro ) );
		features.add( this.FEATURES_.getById( 'Brand_Shell-' + row.Brand_Shell ) );
		features.add( this.FEATURES_.getById( 'Brand_Arco-' + row.Brand_Arco ) );
		features.add( this.FEATURES_.getById( 'Brand_USA-' + row.Brand_USA ) );
		features.add( this.FEATURES_.getById( 'Brand_Exxon-' + row.Brand_Exxon ) );
		features.add( this.FEATURES_.getById( 'Brand_Mobil-' + row.Brand_Mobil ) );

		var storeNum = this.join_( [ row.ListNumber, row.StoreNumber ], '' );
		var stateZip = this.join_( [ row.State, row.Zip ], ' ' );
		var cityStateZip = this.join_( [ row.City, stateZip ], ', ' );
		var position = new google.maps.LatLng( row.Lat, row.Lng );
		var icon = 'Unknown_83_filename'/*tpa=http://arco.com/wp-content/uploads/2013/10/fillingstation.png*/;

		var store = new storeLocator.Store( row.ListNumber, position, features, {
			title: this.join_( [ row.StoreName, row.StoreNumber ], '# ' ),
			address: this.join_( [ row.Address, cityStateZip ], '<br>' ),
			phone: row.Phone,
			icon: icon,
		} );
		stores.push( store );
	}
	return stores;
};

/**
 * Joins elements of an array that are non-empty and non-null.
 * @private
 * @param {!Array} arr array of elements to join.
 * @param {string} sep the separator.
 * @return {string}
 */
DataSource.prototype.join_ = function( arr, sep ) {
	var parts = [ ];
	for ( var i = 0, ii = arr.length; i < ii; i++ ) {
		arr[i] && parts.push( arr[i] );
	}
	return parts.join( sep );
};

/**
 * Very rudimentary CSV parsing - we know how this particular CSV is formatted.
 * IMPORTANT: Don't use this for general CSV parsing!
 * @private
 * @param {string} row
 * @return {Array.<string>}
 */
DataSource.prototype.parseRow_ = function( row ) {
	// Strip leading quote.
	if ( row.charAt( 0 ) == '"' ) {
		row = row.substring( 1 );
	}
	// Strip trailing quote. There seems to be a character between the last quote
	// and the line ending, hence 2 instead of 1.
	if ( row.charAt( row.length - 2 ) == '"' ) {
		row = row.substring( 0, row.length - 2 );
	}

	row = row.split( ',' );

	return row;
};

/**
 * Creates an object mapping headings to row elements.
 * @private
 * @param {Array.<string>} headings
 * @param {Array.<string>} row
 * @return {Object}
 */
DataSource.prototype.toObject_ = function( headings, row ) {
	var result = { };
	for ( var i = 0, ii = row.length; i < ii; i++ ) {
		result[headings[i]] = row[i];
	}
	return result;
};;
function initialize() {
	var map = new google.maps.Map( document.getElementById( 'map-canvas' ), {
		center: new google.maps.LatLng( 40, -100 ),
		zoom: 4,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	} );

	var panelDiv = document.getElementById( 'panel' );

	var data = new DataSource;

	var view = new storeLocator.View( map, data, {
		geolocation: false,
		features: data.getFeatures(),
		updateOnPan: false,
	} );

	view.createMarker = function( store ) {
		return new google.maps.Marker( {
			position: store.getLocation(),
			icon: store.getDetails().icon,
			title: store.getDetails().title,
			visible: true,
		} );
	};

	new storeLocator.Panel( panelDiv, {
		view: view
	} );

	view.clearMarkers();

	google.maps.event.addListenerOnce( map, 'center_changed', function() {
		view.refreshView();
		view.createMarker = function( store ) {
			return new google.maps.Marker( {
				position: store.getLocation(),
				icon: store.getDetails().icon,
				title: store.getDetails().title,
				visible: true,
			} );
		};
		google.maps.event.addListener( map, 'idle', function() {
			view.refreshView();
		} );
	} );
}

google.maps.event.addDomListener( window, 'load', initialize );;
/*! jCarousel - v0.3.1 - 2014-04-26
* http://sorgalla.com/jcarousel
* Copyright (c) 2014 Jan Sorgalla; Licensed MIT */
(function(t){"use strict";var i=t.jCarousel={};i.version="0.3.1";var s=/^([+\-]=)?(.+)$/;i.parseTarget=function(t){var i=!1,e="object"!=typeof t?s.exec(t):null;return e?(t=parseInt(e[2],10)||0,e[1]&&(i=!0,"-="===e[1]&&(t*=-1))):"object"!=typeof t&&(t=parseInt(t,10)||0),{target:t,relative:i}},i.detectCarousel=function(t){for(var i;t.length>0;){if(i=t.filter("[data-jcarousel]"),i.length>0)return i;if(i=t.find("[data-jcarousel]"),i.length>0)return i;t=t.parent()}return null},i.base=function(s){return{version:i.version,_options:{},_element:null,_carousel:null,_init:t.noop,_create:t.noop,_destroy:t.noop,_reload:t.noop,create:function(){return this._element.attr("data-"+s.toLowerCase(),!0).data(s,this),!1===this._trigger("create")?this:(this._create(),this._trigger("createend"),this)},destroy:function(){return!1===this._trigger("destroy")?this:(this._destroy(),this._trigger("destroyend"),this._element.removeData(s).removeAttr("data-"+s.toLowerCase()),this)},reload:function(t){return!1===this._trigger("reload")?this:(t&&this.options(t),this._reload(),this._trigger("reloadend"),this)},element:function(){return this._element},options:function(i,s){if(0===arguments.length)return t.extend({},this._options);if("string"==typeof i){if(s===void 0)return this._options[i]===void 0?null:this._options[i];this._options[i]=s}else this._options=t.extend({},this._options,i);return this},carousel:function(){return this._carousel||(this._carousel=i.detectCarousel(this.options("carousel")||this._element),this._carousel||t.error('Could not detect carousel for plugin "'+s+'"')),this._carousel},_trigger:function(i,e,r){var n,o=!1;return r=[this].concat(r||[]),(e||this._element).each(function(){n=t.Event((s+":"+i).toLowerCase()),t(this).trigger(n,r),n.isDefaultPrevented()&&(o=!0)}),!o}}},i.plugin=function(s,e){var r=t[s]=function(i,s){this._element=t(i),this.options(s),this._init(),this.create()};return r.fn=r.prototype=t.extend({},i.base(s),e),t.fn[s]=function(i){var e=Array.prototype.slice.call(arguments,1),n=this;return"string"==typeof i?this.each(function(){var r=t(this).data(s);if(!r)return t.error("Cannot call methods on "+s+" prior to initialization; "+'attempted to call method "'+i+'"');if(!t.isFunction(r[i])||"_"===i.charAt(0))return t.error('No such method "'+i+'" for '+s+" instance");var o=r[i].apply(r,e);return o!==r&&o!==void 0?(n=o,!1):void 0}):this.each(function(){var e=t(this).data(s);e instanceof r?e.reload(i):new r(this,i)}),n},r}})(jQuery),function(t,i){"use strict";var s=function(t){return parseFloat(t)||0};t.jCarousel.plugin("jcarousel",{animating:!1,tail:0,inTail:!1,resizeTimer:null,lt:null,vertical:!1,rtl:!1,circular:!1,underflow:!1,relative:!1,_options:{list:function(){return this.element().children().eq(0)},items:function(){return this.list().children()},animation:400,transitions:!1,wrap:null,vertical:null,rtl:null,center:!1},_list:null,_items:null,_target:null,_first:null,_last:null,_visible:null,_fullyvisible:null,_init:function(){var t=this;return this.onWindowResize=function(){t.resizeTimer&&clearTimeout(t.resizeTimer),t.resizeTimer=setTimeout(function(){t.reload()},100)},this},_create:function(){this._reload(),t(i).on("resize.jcarousel",this.onWindowResize)},_destroy:function(){t(i).off("resize.jcarousel",this.onWindowResize)},_reload:function(){this.vertical=this.options("vertical"),null==this.vertical&&(this.vertical=this.list().height()>this.list().width()),this.rtl=this.options("rtl"),null==this.rtl&&(this.rtl=function(i){if("rtl"===(""+i.attr("dir")).toLowerCase())return!0;var s=!1;return i.parents("[dir]").each(function(){return/rtl/i.test(t(this).attr("dir"))?(s=!0,!1):void 0}),s}(this._element)),this.lt=this.vertical?"top":"left",this.relative="relative"===this.list().css("position"),this._list=null,this._items=null;var i=this._target&&this.index(this._target)>=0?this._target:this.closest();this.circular="circular"===this.options("wrap"),this.underflow=!1;var s={left:0,top:0};return i.length>0&&(this._prepare(i),this.list().find("[data-jcarousel-clone]").remove(),this._items=null,this.underflow=this._fullyvisible.length>=this.items().length,this.circular=this.circular&&!this.underflow,s[this.lt]=this._position(i)+"px"),this.move(s),this},list:function(){if(null===this._list){var i=this.options("list");this._list=t.isFunction(i)?i.call(this):this._element.find(i)}return this._list},items:function(){if(null===this._items){var i=this.options("items");this._items=(t.isFunction(i)?i.call(this):this.list().find(i)).not("[data-jcarousel-clone]")}return this._items},index:function(t){return this.items().index(t)},closest:function(){var i,e=this,r=this.list().position()[this.lt],n=t(),o=!1,l=this.vertical?"bottom":this.rtl&&!this.relative?"left":"right";return this.rtl&&this.relative&&!this.vertical&&(r+=this.list().width()-this.clipping()),this.items().each(function(){if(n=t(this),o)return!1;var a=e.dimension(n);if(r+=a,r>=0){if(i=a-s(n.css("margin-"+l)),!(0>=Math.abs(r)-a+i/2))return!1;o=!0}}),n},target:function(){return this._target},first:function(){return this._first},last:function(){return this._last},visible:function(){return this._visible},fullyvisible:function(){return this._fullyvisible},hasNext:function(){if(!1===this._trigger("hasnext"))return!0;var t=this.options("wrap"),i=this.items().length-1;return i>=0&&!this.underflow&&(t&&"first"!==t||i>this.index(this._last)||this.tail&&!this.inTail)?!0:!1},hasPrev:function(){if(!1===this._trigger("hasprev"))return!0;var t=this.options("wrap");return this.items().length>0&&!this.underflow&&(t&&"last"!==t||this.index(this._first)>0||this.tail&&this.inTail)?!0:!1},clipping:function(){return this._element["inner"+(this.vertical?"Height":"Width")]()},dimension:function(t){return t["outer"+(this.vertical?"Height":"Width")](!0)},scroll:function(i,s,e){if(this.animating)return this;if(!1===this._trigger("scroll",null,[i,s]))return this;t.isFunction(s)&&(e=s,s=!0);var r=t.jCarousel.parseTarget(i);if(r.relative){var n,o,l,a,h,u,c,f,d=this.items().length-1,_=Math.abs(r.target),p=this.options("wrap");if(r.target>0){var g=this.index(this._last);if(g>=d&&this.tail)this.inTail?"both"===p||"last"===p?this._scroll(0,s,e):t.isFunction(e)&&e.call(this,!1):this._scrollTail(s,e);else if(n=this.index(this._target),this.underflow&&n===d&&("circular"===p||"both"===p||"last"===p)||!this.underflow&&g===d&&("both"===p||"last"===p))this._scroll(0,s,e);else if(l=n+_,this.circular&&l>d){for(f=d,h=this.items().get(-1);l>f++;)h=this.items().eq(0),u=this._visible.index(h)>=0,u&&h.after(h.clone(!0).attr("data-jcarousel-clone",!0)),this.list().append(h),u||(c={},c[this.lt]=this.dimension(h),this.moveBy(c)),this._items=null;this._scroll(h,s,e)}else this._scroll(Math.min(l,d),s,e)}else if(this.inTail)this._scroll(Math.max(this.index(this._first)-_+1,0),s,e);else if(o=this.index(this._first),n=this.index(this._target),a=this.underflow?n:o,l=a-_,0>=a&&(this.underflow&&"circular"===p||"both"===p||"first"===p))this._scroll(d,s,e);else if(this.circular&&0>l){for(f=l,h=this.items().get(0);0>f++;){h=this.items().eq(-1),u=this._visible.index(h)>=0,u&&h.after(h.clone(!0).attr("data-jcarousel-clone",!0)),this.list().prepend(h),this._items=null;var v=this.dimension(h);c={},c[this.lt]=-v,this.moveBy(c)}this._scroll(h,s,e)}else this._scroll(Math.max(l,0),s,e)}else this._scroll(r.target,s,e);return this._trigger("scrollend"),this},moveBy:function(t,i){var e=this.list().position(),r=1,n=0;return this.rtl&&!this.vertical&&(r=-1,this.relative&&(n=this.list().width()-this.clipping())),t.left&&(t.left=e.left+n+s(t.left)*r+"px"),t.top&&(t.top=e.top+n+s(t.top)*r+"px"),this.move(t,i)},move:function(i,s){s=s||{};var e=this.options("transitions"),r=!!e,n=!!e.transforms,o=!!e.transforms3d,l=s.duration||0,a=this.list();if(!r&&l>0)return a.animate(i,s),void 0;var h=s.complete||t.noop,u={};if(r){var c=a.css(["transitionDuration","transitionTimingFunction","transitionProperty"]),f=h;h=function(){t(this).css(c),f.call(this)},u={transitionDuration:(l>0?l/1e3:0)+"s",transitionTimingFunction:e.easing||s.easing,transitionProperty:l>0?function(){return n||o?"all":i.left?"left":"top"}():"none",transform:"none"}}o?u.transform="translate3d("+(i.left||0)+","+(i.top||0)+",0)":n?u.transform="translate("+(i.left||0)+","+(i.top||0)+")":t.extend(u,i),r&&l>0&&a.one("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd",h),a.css(u),0>=l&&a.each(function(){h.call(this)})},_scroll:function(i,s,e){if(this.animating)return t.isFunction(e)&&e.call(this,!1),this;if("object"!=typeof i?i=this.items().eq(i):i.jquery===void 0&&(i=t(i)),0===i.length)return t.isFunction(e)&&e.call(this,!1),this;this.inTail=!1,this._prepare(i);var r=this._position(i),n=this.list().position()[this.lt];if(r===n)return t.isFunction(e)&&e.call(this,!1),this;var o={};return o[this.lt]=r+"px",this._animate(o,s,e),this},_scrollTail:function(i,s){if(this.animating||!this.tail)return t.isFunction(s)&&s.call(this,!1),this;var e=this.list().position()[this.lt];this.rtl&&this.relative&&!this.vertical&&(e+=this.list().width()-this.clipping()),this.rtl&&!this.vertical?e+=this.tail:e-=this.tail,this.inTail=!0;var r={};return r[this.lt]=e+"px",this._update({target:this._target.next(),fullyvisible:this._fullyvisible.slice(1).add(this._visible.last())}),this._animate(r,i,s),this},_animate:function(i,s,e){if(e=e||t.noop,!1===this._trigger("animate"))return e.call(this,!1),this;this.animating=!0;var r=this.options("animation"),n=t.proxy(function(){this.animating=!1;var t=this.list().find("[data-jcarousel-clone]");t.length>0&&(t.remove(),this._reload()),this._trigger("animateend"),e.call(this,!0)},this),o="object"==typeof r?t.extend({},r):{duration:r},l=o.complete||t.noop;return s===!1?o.duration=0:t.fx.speeds[o.duration]!==void 0&&(o.duration=t.fx.speeds[o.duration]),o.complete=function(){n(),l.call(this)},this.move(i,o),this},_prepare:function(i){var e,r,n,o,l=this.index(i),a=l,h=this.dimension(i),u=this.clipping(),c=this.vertical?"bottom":this.rtl?"left":"right",f=this.options("center"),d={target:i,first:i,last:i,visible:i,fullyvisible:u>=h?i:t()};if(f&&(h/=2,u/=2),u>h)for(;;){if(e=this.items().eq(++a),0===e.length){if(!this.circular)break;if(e=this.items().eq(0),i.get(0)===e.get(0))break;if(r=this._visible.index(e)>=0,r&&e.after(e.clone(!0).attr("data-jcarousel-clone",!0)),this.list().append(e),!r){var _={};_[this.lt]=this.dimension(e),this.moveBy(_)}this._items=null}if(o=this.dimension(e),0===o)break;if(h+=o,d.last=e,d.visible=d.visible.add(e),n=s(e.css("margin-"+c)),u>=h-n&&(d.fullyvisible=d.fullyvisible.add(e)),h>=u)break}if(!this.circular&&!f&&u>h)for(a=l;;){if(0>--a)break;if(e=this.items().eq(a),0===e.length)break;if(o=this.dimension(e),0===o)break;if(h+=o,d.first=e,d.visible=d.visible.add(e),n=s(e.css("margin-"+c)),u>=h-n&&(d.fullyvisible=d.fullyvisible.add(e)),h>=u)break}return this._update(d),this.tail=0,f||"circular"===this.options("wrap")||"custom"===this.options("wrap")||this.index(d.last)!==this.items().length-1||(h-=s(d.last.css("margin-"+c)),h>u&&(this.tail=h-u)),this},_position:function(t){var i=this._first,s=i.position()[this.lt],e=this.options("center"),r=e?this.clipping()/2-this.dimension(i)/2:0;return this.rtl&&!this.vertical?(s-=this.relative?this.list().width()-this.dimension(i):this.clipping()-this.dimension(i),s+=r):s-=r,!e&&(this.index(t)>this.index(i)||this.inTail)&&this.tail?(s=this.rtl&&!this.vertical?s-this.tail:s+this.tail,this.inTail=!0):this.inTail=!1,-s},_update:function(i){var s,e=this,r={target:this._target||t(),first:this._first||t(),last:this._last||t(),visible:this._visible||t(),fullyvisible:this._fullyvisible||t()},n=this.index(i.first||r.first)<this.index(r.first),o=function(s){var o=[],l=[];i[s].each(function(){0>r[s].index(this)&&o.push(this)}),r[s].each(function(){0>i[s].index(this)&&l.push(this)}),n?o=o.reverse():l=l.reverse(),e._trigger(s+"in",t(o)),e._trigger(s+"out",t(l)),e["_"+s]=i[s]};for(s in i)o(s);return this}})}(jQuery,window),function(t){"use strict";t.jcarousel.fn.scrollIntoView=function(i,s,e){var r,n=t.jCarousel.parseTarget(i),o=this.index(this._fullyvisible.first()),l=this.index(this._fullyvisible.last());if(r=n.relative?0>n.target?Math.max(0,o+n.target):l+n.target:"object"!=typeof n.target?n.target:this.index(n.target),o>r)return this.scroll(r,s,e);if(r>=o&&l>=r)return t.isFunction(e)&&e.call(this,!1),this;for(var a,h=this.items(),u=this.clipping(),c=this.vertical?"bottom":this.rtl?"left":"right",f=0;;){if(a=h.eq(r),0===a.length)break;if(f+=this.dimension(a),f>=u){var d=parseFloat(a.css("margin-"+c))||0;f-d!==u&&r++;break}if(0>=r)break;r--}return this.scroll(r,s,e)}}(jQuery),function(t){"use strict";t.jCarousel.plugin("jcarouselControl",{_options:{target:"+=1",event:"click",method:"scroll"},_active:null,_init:function(){this.onDestroy=t.proxy(function(){this._destroy(),this.carousel().one("jcarousel:createend",t.proxy(this._create,this))},this),this.onReload=t.proxy(this._reload,this),this.onEvent=t.proxy(function(i){i.preventDefault();var s=this.options("method");t.isFunction(s)?s.call(this):this.carousel().jcarousel(this.options("method"),this.options("target"))},this)},_create:function(){this.carousel().one("jcarousel:destroy",this.onDestroy).on("jcarousel:reloadend jcarousel:scrollend",this.onReload),this._element.on(this.options("event")+".jcarouselcontrol",this.onEvent),this._reload()},_destroy:function(){this._element.off(".jcarouselcontrol",this.onEvent),this.carousel().off("jcarousel:destroy",this.onDestroy).off("jcarousel:reloadend jcarousel:scrollend",this.onReload)},_reload:function(){var i,s=t.jCarousel.parseTarget(this.options("target")),e=this.carousel();if(s.relative)i=e.jcarousel(s.target>0?"hasNext":"hasPrev");else{var r="object"!=typeof s.target?e.jcarousel("items").eq(s.target):s.target;i=e.jcarousel("target").index(r)>=0}return this._active!==i&&(this._trigger(i?"active":"inactive"),this._active=i),this}})}(jQuery),function(t){"use strict";t.jCarousel.plugin("jcarouselPagination",{_options:{perPage:null,item:function(t){return'<a href="#'+t+'">'+t+"</a>"},event:"click",method:"scroll"},_carouselItems:null,_pages:{},_items:{},_currentPage:null,_init:function(){this.onDestroy=t.proxy(function(){this._destroy(),this.carousel().one("jcarousel:createend",t.proxy(this._create,this))},this),this.onReload=t.proxy(this._reload,this),this.onScroll=t.proxy(this._update,this)},_create:function(){this.carousel().one("jcarousel:destroy",this.onDestroy).on("jcarousel:reloadend",this.onReload).on("jcarousel:scrollend",this.onScroll),this._reload()},_destroy:function(){this._clear(),this.carousel().off("jcarousel:destroy",this.onDestroy).off("jcarousel:reloadend",this.onReload).off("jcarousel:scrollend",this.onScroll),this._carouselItems=null},_reload:function(){var i=this.options("perPage");if(this._pages={},this._items={},t.isFunction(i)&&(i=i.call(this)),null==i)this._pages=this._calculatePages();else for(var s,e=parseInt(i,10)||0,r=this._getCarouselItems(),n=1,o=0;;){if(s=r.eq(o++),0===s.length)break;this._pages[n]=this._pages[n]?this._pages[n].add(s):s,0===o%e&&n++}this._clear();var l=this,a=this.carousel().data("jcarousel"),h=this._element,u=this.options("item"),c=this._getCarouselItems().length;t.each(this._pages,function(i,s){var e=l._items[i]=t(u.call(l,i,s));e.on(l.options("event")+".jcarouselpagination",t.proxy(function(){var t=s.eq(0);if(a.circular){var e=a.index(a.target()),r=a.index(t);parseFloat(i)>parseFloat(l._currentPage)?e>r&&(t="+="+(c-e+r)):r>e&&(t="-="+(e+(c-r)))}a[this.options("method")](t)},l)),h.append(e)}),this._update()},_update:function(){var i,s=this.carousel().jcarousel("target");t.each(this._pages,function(t,e){return e.each(function(){return s.is(this)?(i=t,!1):void 0}),i?!1:void 0}),this._currentPage!==i&&(this._trigger("inactive",this._items[this._currentPage]),this._trigger("active",this._items[i])),this._currentPage=i},items:function(){return this._items},reloadCarouselItems:function(){return this._carouselItems=null,this},_clear:function(){this._element.empty(),this._currentPage=null},_calculatePages:function(){for(var t,i=this.carousel().data("jcarousel"),s=this._getCarouselItems(),e=i.clipping(),r=0,n=0,o=1,l={};;){if(t=s.eq(n++),0===t.length)break;l[o]=l[o]?l[o].add(t):t,r+=i.dimension(t),r>=e&&(o++,r=0)}return l},_getCarouselItems:function(){return this._carouselItems||(this._carouselItems=this.carousel().jcarousel("items")),this._carouselItems}})}(jQuery),function(t){"use strict";t.jCarousel.plugin("jcarouselAutoscroll",{_options:{target:"+=1",interval:3e3,autostart:!0},_timer:null,_init:function(){this.onDestroy=t.proxy(function(){this._destroy(),this.carousel().one("jcarousel:createend",t.proxy(this._create,this))},this),this.onAnimateEnd=t.proxy(this.start,this)},_create:function(){this.carousel().one("jcarousel:destroy",this.onDestroy),this.options("autostart")&&this.start()},_destroy:function(){this.stop(),this.carousel().off("jcarousel:destroy",this.onDestroy)},start:function(){return this.stop(),this.carousel().one("jcarousel:animateend",this.onAnimateEnd),this._timer=setTimeout(t.proxy(function(){this.carousel().jcarousel("scroll",this.options("target"))},this),this.options("interval")),this},stop:function(){return this._timer&&(this._timer=clearTimeout(this._timer)),this.carousel().off("jcarousel:animateend",this.onAnimateEnd),this}})}(jQuery);;
(function(l){l.fn.rssfeed=function(b,h,w){h=l.extend({limit:10,offset:1,header:!0,titletag:"h4",date:!0,dateformat:"datetime",content:!0,snippet:!0,media:!0,showerror:!0,errormsg:"",key:null,ssl:!1,linktarget:"_self",linkredirect:"",linkcontent:!1,sort:"",sortasc:!0,historical:!1},h);return this.each(function(z,q){var u=l(q),f="";h.ssl&&(f="s");u.hasClass("rssFeed")||u.addClass("rssFeed");if(null==b)return!1;0<h.offset&&(h.offset-=1);h.limit+=h.offset;f="http"+f+"://ajax.googleapis.com/ajax/services/feed/load?v=1.0&callback=?&q="+
encodeURIComponent(b);f+="&num="+h.limit;h.historical&&(f+="&scoring=h");null!=h.key&&(f+="&key="+h.key);l.getJSON(f+"&output=json_xml",function(b){if(200==b.responseStatus){var f=b.responseData,e=h;if(b=f.feed){var j=[],d=0,m="",v="odd";if(e.media){var n=f.xmlString;"Microsoft Internet Explorer"==navigator.appName?(d=new ActiveXObject("Microsoft.XMLDOM"),d.async="false",d.loadXML(n)):d=(new DOMParser).parseFromString(n,"text/xml");n=d.getElementsByTagName("item")}e.header&&(m+='<div class="rssHeader"><a href="'+
b.link+'" title="'+b.description+'">'+b.title+"</a></div>");m+='<div class="rssBody"><ul>';for(f=e.offset;f<b.entries.length;f++){d=f-e.offset;j[d]=[];var g=b.entries[f],a,c="",k=g.link;switch(e.sort){case "title":c=g.title;break;case "date":c=g.publishedDate}j[d].sort=c;if(g.publishedDate)switch(c=new Date(g.publishedDate),a=c.toLocaleDateString()+" "+c.toLocaleTimeString(),e.dateformat){case "datetime":break;case "date":a=c.toLocaleDateString();break;case "time":a=c.toLocaleTimeString();break;case "timeline":a=
new Date(c);a=Math.round(((new Date).getTime()-a.getTime())/1E3);60>a?a="< 1 min":(3600>a?(a=Math.round(a/60)-1,c="min"):86400>a?(a=Math.round(a/3600)-1,c="hour"):604800>a?(a=Math.round(a/86400)-1,c="day"):(a=Math.round(a/604800)-1,c="week"),1<a&&(c+="s"),a=a+" "+c);break;default:a=c,c=new Date(a),a=e.dateformat,a=a.replace("dd",p(c.getDate())),a=a.replace("MMMM",x(c.getMonth())),a=a.replace("MM",p(c.getMonth()+1)),a=a.replace("yyyy",c.getFullYear()),a=a.replace("hh",p(c.getHours())),a=a.replace("mm",
p(c.getMinutes())),a=a.replace("ss",p(c.getSeconds()))}e.linkredirect&&(k=encodeURIComponent(k));j[d].html="<"+e.titletag+'><a href="'+e.linkredirect+k+'" title="View this feed at '+b.title+'">'+g.title+"</a></"+e.titletag+">";e.date&&a&&(j[d].html+="<div>"+a+"</div>");e.content&&(g=e.snippet&&""!=g.contentSnippet?g.contentSnippet:g.content,e.linkcontent&&(g='<a href="'+e.linkredirect+k+'" title="View this feed at '+b.title+'">'+g+"</a>"),j[d].html+="<p>"+g+"</p>");if(e.media&&0<n.length&&(k=n[f].getElementsByTagName("enclosure"),
0<k.length)){j[d].html+='<div class="rssMedia"><div>Media files</div><ul>';for(g=0;g<k.length;g++){var r=k[g].getAttribute("url"),s=k[g].getAttribute("type"),t=k[g].getAttribute("length"),c=j[d],y=j[d].html,r='<li><a href="'+r+'" title="Download this media">'+r.split("/").pop()+"</a> ("+s+", ",s=Math.floor(Math.log(t)/Math.log(1024)),t=(t/Math.pow(1024,Math.floor(s))).toFixed(2)+" "+"bytes kb MB GB TB PB".split(" ")[s];c.html=y+(r+t+")</li>")}j[d].html+="</ul></div>"}}e.sort&&j.sort(function(a,c){if(e.sortasc)var b=
a.sort,d=c.sort;else b=c.sort,d=a.sort;if("date"==e.sort)return new Date(b)-new Date(d);b=b.toLowerCase();d=d.toLowerCase();return b<d?-1:b>d?1:0});l.each(j,function(a){m+='<li class="rssRow '+v+'">'+j[a].html+"</li>";v="odd"==v?"even":"odd"});m+="</ul></div>";l(q).html(m);l("a",q).attr("target",e.linktarget)}l.isFunction(w)&&w.call(this,u)}else h.showerror&&(d=""!=h.errormsg?h.errormsg:b.responseDetails),l(q).html('<div class="rssError"><p>'+d+"</p></div>")})})};var p=function(b){b+="";2>b.length&&
(b="0"+b);return b},x=function(b){return"Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" ")[b]}})(jQuery);;
var addComment={moveForm:function(a,b,c,d){var e,f=this,g=f.I(a),h=f.I(c),i=f.I("cancel-comment-reply-link"),j=f.I("comment_parent"),k=f.I("comment_post_ID");if(g&&h&&i&&j){f.respondId=c,d=d||!1,f.I("wp-temp-form-div")||(e=document.createElement("div"),e.id="wp-temp-form-div",e.style.display="none",h.parentNode.insertBefore(e,h)),g.parentNode.insertBefore(h,g.nextSibling),k&&d&&(k.value=d),j.value=b,i.style.display="",i.onclick=function(){var a=addComment,b=a.I("wp-temp-form-div"),c=a.I(a.respondId);if(b&&c)return a.I("comment_parent").value="0",b.parentNode.insertBefore(c,b),b.parentNode.removeChild(b),this.style.display="none",this.onclick=null,!1};try{f.I("comment").focus()}catch(l){}return!1}},I:function(a){return document.getElementById(a)}};;
/*! Responsive JS Library v1.2.2 */

/*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas. Dual MIT/BSD license */
/*! NOTE: If you're already including a window.matchMedia polyfill via Modernizr or otherwise, you don't need this part */
window.matchMedia = window.matchMedia || (function(doc, undefined){

  var bool,
      docElem  = doc.documentElement,
      refNode  = docElem.firstElementChild || docElem.firstChild,
      // fakeBody required for <FF4 when executed in <head>
      fakeBody = doc.createElement('body'),
      div      = doc.createElement('div');

  div.id = 'mq-test-1';
  div.style.cssText = "position:absolute;top:-100em";
  fakeBody.style.background = "none";
  fakeBody.appendChild(div);

  return function(q){

    div.innerHTML = '&shy;<style media="'+q+'"> #mq-test-1 { width: 42px; }</style>';

    docElem.insertBefore(fakeBody, refNode);
    bool = div.offsetWidth == 42;
    docElem.removeChild(fakeBody);

    return { matches: bool, media: q };
  };

})(document);

/*! Respond.js v1.1.0: min/max-width media query polyfill. (c) Scott Jehl. MIT/GPLv2 Lic. j.mp/respondjs  */
(function( win ){
	//exposed namespace
	win.respond		= {};

	//define update even in native-mq-supporting browsers, to avoid errors
	respond.update	= function(){};

	//expose media query support flag for external use
	respond.mediaQueriesSupported	= win.matchMedia && win.matchMedia( "only all" ).matches;

	//if media queries are supported, exit here
	if( respond.mediaQueriesSupported ){ return; }

	//define vars
	var doc 			= win.document,
		docElem 		= doc.documentElement,
		mediastyles		= [],
		rules			= [],
		appendedEls 	= [],
		parsedSheets 	= {},
		resizeThrottle	= 30,
		head 			= doc.getElementsByTagName( "head" )[0] || docElem,
		base			= doc.getElementsByTagName( "base" )[0],
		links			= head.getElementsByTagName( "link" ),
		requestQueue	= [],

		//loop stylesheets, send text content to translate
		ripCSS			= function(){
			var sheets 	= links,
				sl 		= sheets.length,
				i		= 0,
				//vars for loop:
				sheet, href, media, isCSS;

			for( ; i < sl; i++ ){
				sheet	= sheets[ i ],
				href	= sheet.href,
				media	= sheet.media,
				isCSS	= sheet.rel && sheet.rel.toLowerCase() === "stylesheet";

				//only links plz and prevent re-parsing
				if( !!href && isCSS && !parsedSheets[ href ] ){
					// selectivizr exposes css through the rawCssText expando
					if (sheet.styleSheet && sheet.styleSheet.rawCssText) {
						translate( sheet.styleSheet.rawCssText, href, media );
						parsedSheets[ href ] = true;
					} else {
						if( (!/^([a-zA-Z:]*\/\/)/.test( href ) && !base)
							|| href.replace( RegExp.$1, "" ).split( "/" )[0] === win.location.host ){
							requestQueue.push( {
								href: href,
								media: media
							} );
						}
					}
				}
			}
			makeRequests();
		},

		//recurse through request queue, get css text
		makeRequests	= function(){
			if( requestQueue.length ){
				var thisRequest = requestQueue.shift();

				ajax( thisRequest.href, function( styles ){
					translate( styles, thisRequest.href, thisRequest.media );
					parsedSheets[ thisRequest.href ] = true;
					makeRequests();
				} );
			}
		},

		//find media blocks in css text, convert to style blocks
		translate			= function( styles, href, media ){
			var qs			= styles.match(  /@media[^\{]+\{([^\{\}]*\{[^\}\{]*\})+/gi ),
				ql			= qs && qs.length || 0,
				//try to get CSS path
				href		= href.substring( 0, href.lastIndexOf( "/" )),
				repUrls		= function( css ){
					return css.replace( /(url\()['"]?([^\/\)'"][^:\)'"]+)['"]?(\))/g, "$1" + href + "$2$3" );
				},
				useMedia	= !ql && media,
				//vars used in loop
				i			= 0,
				j, fullq, thisq, eachq, eql;

			//if path exists, tack on trailing slash
			if( href.length ){ href += "/"; }

			//if no internal queries exist, but media attr does, use that
			//note: this currently lacks support for situations where a media attr is specified on a link AND
				//its associated stylesheet has internal CSS media queries.
				//In those cases, the media attribute will currently be ignored.
			if( useMedia ){
				ql = 1;
			}


			for( ; i < ql; i++ ){
				j	= 0;

				//media attr
				if( useMedia ){
					fullq = media;
					rules.push( repUrls( styles ) );
				}
				//parse for styles
				else{
					fullq	= qs[ i ].match( /@media *([^\{]+)\{([\S\s]+?)$/ ) && RegExp.$1;
					rules.push( RegExp.$2 && repUrls( RegExp.$2 ) );
				}

				eachq	= fullq.split( "," );
				eql		= eachq.length;

				for( ; j < eql; j++ ){
					thisq	= eachq[ j ];
					mediastyles.push( {
						media	: thisq.split( "(" )[ 0 ].match( /(only\s+)?([a-zA-Z]+)\s?/ ) && RegExp.$2 || "all",
						rules	: rules.length - 1,
						hasquery: thisq.indexOf("(") > -1,
						minw	: thisq.match( /\(min\-width:[\s]*([\s]*[0-9\.]+)(px|em)[\s]*\)/ ) && parseFloat( RegExp.$1 ) + ( RegExp.$2 || "" ),
						maxw	: thisq.match( /\(max\-width:[\s]*([\s]*[0-9\.]+)(px|em)[\s]*\)/ ) && parseFloat( RegExp.$1 ) + ( RegExp.$2 || "" )
					} );
				}
			}

			applyMedia();
		},

		lastCall,

		resizeDefer,

		// returns the value of 1em in pixels
		getEmValue		= function() {
			var ret,
				div = doc.createElement('div'),
				body = doc.body,
				fakeUsed = false;

			div.style.cssText = "position:absolute;font-size:1em;width:1em";

			if( !body ){
				body = fakeUsed = doc.createElement( "body" );
				body.style.background = "none";
			}

			body.appendChild( div );

			docElem.insertBefore( body, docElem.firstChild );

			ret = div.offsetWidth;

			if( fakeUsed ){
				docElem.removeChild( body );
			}
			else {
				body.removeChild( div );
			}

			//also update eminpx before returning
			ret = eminpx = parseFloat(ret);

			return ret;
		},

		//cached container for 1em value, populated the first time it's needed
		eminpx,

		//enable/disable styles
		applyMedia			= function( fromResize ){
			var name		= "clientWidth",
				docElemProp	= docElem[ name ],
				currWidth 	= doc.compatMode === "CSS1Compat" && docElemProp || doc.body[ name ] || docElemProp,
				styleBlocks	= {},
				lastLink	= links[ links.length-1 ],
				now 		= (new Date()).getTime();

			//throttle resize calls
			if( fromResize && lastCall && now - lastCall < resizeThrottle ){
				clearTimeout( resizeDefer );
				resizeDefer = setTimeout( applyMedia, resizeThrottle );
				return;
			}
			else {
				lastCall	= now;
			}

			for( var i in mediastyles ){
				var thisstyle = mediastyles[ i ],
					min = thisstyle.minw,
					max = thisstyle.maxw,
					minnull = min === null,
					maxnull = max === null,
					em = "em";

				if( !!min ){
					min = parseFloat( min ) * ( min.indexOf( em ) > -1 ? ( eminpx || getEmValue() ) : 1 );
				}
				if( !!max ){
					max = parseFloat( max ) * ( max.indexOf( em ) > -1 ? ( eminpx || getEmValue() ) : 1 );
				}

				// if there's no media query at all (the () part), or min or max is not null, and if either is present, they're true
				if( !thisstyle.hasquery || ( !minnull || !maxnull ) && ( minnull || currWidth >= min ) && ( maxnull || currWidth <= max ) ){
						if( !styleBlocks[ thisstyle.media ] ){
							styleBlocks[ thisstyle.media ] = [];
						}
						styleBlocks[ thisstyle.media ].push( rules[ thisstyle.rules ] );
				}
			}

			//remove any existing respond style element(s)
			for( var i in appendedEls ){
				if( appendedEls[ i ] && appendedEls[ i ].parentNode === head ){
					head.removeChild( appendedEls[ i ] );
				}
			}

			//inject active styles, grouped by media type
			for( var i in styleBlocks ){
				var ss		= doc.createElement( "style" ),
					css		= styleBlocks[ i ].join( "\n" );

				ss.type = "text/css";
				ss.media	= i;

				//originally, ss was appended to a documentFragment and sheets were appended in bulk.
				//this caused crashes in IE in a number of circumstances, such as when the HTML element had a bg image set, so appending beforehand seems best. Thanks to @dvelyk for the initial research on this one!
				head.insertBefore( ss, lastLink.nextSibling );

				if ( ss.styleSheet ){
		        	ss.styleSheet.cssText = css;
		        }
		        else {
					ss.appendChild( doc.createTextNode( css ) );
		        }

				//push to appendedEls to track for later removal
				appendedEls.push( ss );
			}
		},
		//tweaked Ajax functions from Quirksmode
		ajax = function( url, callback ) {
			var req = xmlHttp();
			if (!req){
				return;
			}
			req.open( "GET", url, true );
			req.onreadystatechange = function () {
				if ( req.readyState != 4 || req.status != 200 && req.status != 304 ){
					return;
				}
				callback( req.responseText );
			}
			if ( req.readyState == 4 ){
				return;
			}
			req.send( null );
		},
		//define ajax obj
		xmlHttp = (function() {
			var xmlhttpmethod = false;
			try {
				xmlhttpmethod = new XMLHttpRequest();
			}
			catch( e ){
				xmlhttpmethod = new ActiveXObject( "Microsoft.XMLHTTP" );
			}
			return function(){
				return xmlhttpmethod;
			};
		})();

	//translate CSS
	ripCSS();

	//expose update for re-running respond later on
	respond.update = ripCSS;

	//adjust on resize
	function callMedia(){
		applyMedia( true );
	}
	if( win.addEventListener ){
		win.addEventListener( "resize", callMedia, false );
	}
	else if( win.attachEvent ){
		win.attachEvent( "onresize", callMedia );
	}
})(this);

/**
 * jQuery Friendly IE6 Upgrade Notice Plugin 1.0.0
 *
 * http://code.google.com/p/friendly-ie6-upgrade-notice/
 *
 * Copyright (c) 2013 Emil Uzelac - ThemeID
 *
 * http://www.gnu.org/licenses/gpl.html
 */
if (jQuery.browser.msie && jQuery.browser.version <= 6)
    jQuery('<div class="msie-box">' + '<a href="http://browsehappy.com/" title="Click here to update" target="_blank">  Your browser is no longer supported. Click here to update...</a> </div>').appendTo('#container');

/**
 * jQuery Scroll Top Plugin 1.0.0
 */
jQuery(document).ready(function ($) {
    $('a[href=#scroll-top]').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 'slow');
        return false;
    });
});;
(function() {
	var ajaxurl = window.ajaxurl || 'https://s2.wp.com/wp-admin/admin-ajax.php',
		data = window.wpcomVipAnalytics,
		dataQs, percent;

	if ( typeof XMLHttpRequest === 'undefined' ) {
		return;
	}

	if ( ! data ) {
		return;
	}

	percent = ~~data.percentToTrack;
	if ( percent && percent < 100 && ( ~~( ( Math.random() * 100 ) + 1 ) > percent ) ) {
		return;
	}

	dataQs = 'action=wpcom_vip_analytics';

	for ( var key in data ) {
		if ( key === 'percentToTrack' ) {
			continue;
		}
		if ( data.hasOwnProperty( key ) ) {
			dataQs += '&' +
				encodeURIComponent( key ).replace(/%20/g, '+' ) + '=' +
				encodeURIComponent( data[key] ).replace(/%20/g, '+' );
		}
	}

	function sendInfo() {
		var xhr = new XMLHttpRequest();
		xhr.open( 'POST', ajaxurl, true );
		xhr.setRequestHeader( 'Content-type', 'application/x-www-form-urlencoded' );
		xhr.send( dataQs );
	}

	// Delay for some time after the document is ready to ping
	function docReady() {
		setTimeout( function() {
			sendInfo();
		}, 1500 );
	}

	if ( document.readyState === 'complete' ) {
		docReady.apply();
	}
	else if ( document.addEventListener ) {
		document.addEventListener( 'DOMContentLoaded', docReady, false );
	}
	else if ( document.attachEvent ) {
		document.attachEvent( 'onreadystatechange', docReady );
	}
})();
;
/*jslint indent: 2, browser: true, bitwise: true, plusplus: true */

/*! Copyright Twitter Inc. and other contributors. Licensed under MIT *//*
https://github.com/twitter/twemoji/blob/gh-pages/LICENSE
*/

// WARNING:   this file is generated automatically via
//            `node twemoji-generator.js`
//            please update its `createTwemoji` function
//            at the bottom of the same file instead.
var twemoji = (function ( ) {
  'use strict';

  /*jshint maxparams:4 */

  var
    // the exported module object
    twemoji = {


    /////////////////////////
    //      properties     //
    /////////////////////////

      // default assets url, by default will be Twitter Inc. CDN
      base: (location.protocol === 'https:' ? 'https:' : 'http:') +
            '//twemoji.maxcdn.com/',

      // default assets file extensions, by default '.png'
      ext: '.svg',

      // default assets/folder size, by default "36x36"
      // available via Twitter CDN: 16, 36, 72
      size: 'svg',

      // basic utilities / helpers to convert code points
      // to JavaScript surrogates and vice versa
      convert: {

        /**
         * Given an HEX codepoint, returns UTF16 surrogate pairs.
         *
         * @param   string  generic codepoint, i.e. '1F4A9'
         * @return  string  codepoint transformed into utf16 surrogates pair,
         *          i.e. \uD83D\uDCA9
         *
         * @example
         *  twemoji.convert.fromCodePoint('1f1e8');
         *  // "\ud83c\udde8"
         *
         *  '1f1e8-1f1f3'.split('-').map(twemoji.convert.fromCodePoint).join('')
         *  // "\ud83c\udde8\ud83c\uddf3"
         */
        fromCodePoint: fromCodePoint,

        /**
         * Given UTF16 surrogate pairs, returns the equivalent HEX codepoint.
         *
         * @param   string  generic utf16 surrogates pair, i.e. \uD83D\uDCA9
         * @param   string  optional separator for double code points, default='-'
         * @return  string  utf16 transformed into codepoint, i.e. '1F4A9'
         *
         * @example
         *  twemoji.convert.toCodePoint('\ud83c\udde8\ud83c\uddf3');
         *  // "1f1e8-1f1f3"
         *
         *  twemoji.convert.toCodePoint('\ud83c\udde8\ud83c\uddf3', '~');
         *  // "1f1e8~1f1f3"
         */
        toCodePoint: toCodePoint
      },


    /////////////////////////
    //       methods       //
    /////////////////////////

      /**
       * User first: used to remove missing images
       * preserving the original text intent when
       * a fallback for network problems is desired.
       * Automatically added to Image nodes via DOM
       * It could be recycled for string operations via:
       *  $('img.emoji').on('error', twemoji.onerror)
       */
      onerror: function onerror() {
        if (this.parentNode) {
          this.parentNode.replaceChild(createText(this.alt), this);
        }
      },

      /**
       * Main method/logic to generate either <img> tags or HTMLImage nodes.
       *  "emojify" a generic text or DOM Element.
       *
       * @overloads
       *
       * String replacement for `innerHTML` or server side operations
       *  twemoji.parse(string);
       *  twemoji.parse(string, Function);
       *  twemoji.parse(string, Object);
       *
       * HTMLElement tree parsing for safer operations over existing DOM
       *  twemoji.parse(HTMLElement);
       *  twemoji.parse(HTMLElement, Function);
       *  twemoji.parse(HTMLElement, Object);
       *
       * @param   string|HTMLElement  the source to parse and enrich with emoji.
       *
       *          string              replace emoji matches with <img> tags.
       *                              Mainly used to inject emoji via `innerHTML`
       *                              It does **not** parse the string or validate it,
       *                              it simply replaces found emoji with a tag.
       *                              NOTE: be sure this won't affect security.
       *
       *          HTMLElement         walk through the DOM tree and find emoji
       *                              that are inside **text node only** (nodeType === 3)
       *                              Mainly used to put emoji in already generated DOM
       *                              without compromising surrounding nodes and
       *                              **avoiding** the usage of `innerHTML`.
       *                              NOTE: Using DOM elements instead of strings should
       *                              improve security without compromising too much
       *                              performance compared with a less safe `innerHTML`.
       *
       * @param   Function|Object  [optional]
       *                              either the callback that will be invoked or an object
       *                              with all properties to use per each found emoji.
       *
       *          Function            if specified, this will be invoked per each emoji
       *                              that has been found through the RegExp except
       *                              those follwed by the invariant \uFE0E ("as text").
       *                              Once invoked, parameters will be:
       *
       *                                codePoint:string  the lower case HEX code point
       *                                                  i.e. "1f4a9"
       *
       *                                options:Object    all info for this parsing operation
       *
       *                                variant:char      the optional \uFE0F ("as image")
       *                                                  variant, in case this info
       *                                                  is anyhow meaningful.
       *                                                  By default this is ignored.
       *
       *                              If such callback will return a falsy value instead
       *                              of a valid `src` to use for the image, nothing will
       *                              actually change for that specific emoji.
       *
       *
       *          Object              if specified, an object containing the following properties
       *
       *            callback   Function  the callback to invoke per each found emoji.
       *            base       string    the base url, by default twemoji.base
       *            ext        string    the image extension, by default twemoji.ext
       *            size       string    the assets size, by default twemoji.size
       *
       * @example
       *
       *  twemoji.parse("I \u2764\uFE0F emoji!");
       *  // I <img class="emoji" draggable="false" alt="❤️" src="Unknown_83_filename"/*tpa=https://s2.wp.com/assets/2764.gif*/> emoji!
       *
       *
       *  twemoji.parse("I \u2764\uFE0F emoji!", function(icon, options, variant) {
       *    return '/assets/' + icon + '.gif';
       *  });
       *  // I <img class="emoji" draggable="false" alt="❤️" src="Unknown_83_filename"/*tpa=https://s2.wp.com/assets/2764.gif*/> emoji!
       *
       *
       * twemoji.parse("I \u2764\uFE0F emoji!", {
       *   size: 72,
       *   callback: function(icon, options, variant) {
       *     return '/assets/' + options.size + '/' + icon + options.ext;
       *   }
       * });
       *  // I <img class="emoji" draggable="false" alt="❤️" src="Unknown_83_filename"/*tpa=https://s2.wp.com/assets/72x72/2764.png*/> emoji!
       *
       */
      parse: parse,

      /**
       * Given a string, invokes the callback argument
       *  per each emoji found in such string.
       * This is the most raw version used by
       *  the .parse(string) method itself.
       *
       * @param   string    generic string to parse
       * @param   Function  a generic callback that will be
       *                    invoked to replace the content.
       *                    This calback wil receive standard
       *                    String.prototype.replace(str, callback)
       *                    arguments such:
       *  callback(
       *    match,  // the emoji match
       *    icon,   // the emoji text (same as text)
       *    variant // either '\uFE0E' or '\uFE0F', if present
       *  );
       *
       *                    and others commonly received via replace.
       *
       *  NOTE: When the variant \uFE0E is found, remember this is an explicit intent
       *  from the user: the emoji should **not** be replaced with an image.
       *  In \uFE0F case one, it's the opposite, it should be graphic.
       *  This utility convetion is that only \uFE0E are not translated into images.
       */
      replace: replace,

      /**
       * Simplify string tests against emoji.
       *
       * @param   string  some text that might contain emoji
       * @return  boolean true if any emoji was found, false otherwise.
       *
       * @example
       *
       *  if (twemoji.test(someContent)) {
       *    console.log("emoji All The Things!");
       *  }
       */
      test: test
    },

    // RegExp based on emoji's official Unicode standards
    // http://www.unicode.org/Public/UNIDATA/EmojiSources.txt
    re = /((?:\ud83c\udde8\ud83c\uddf3|\ud83c\uddfa\ud83c\uddf8|\ud83c\uddf7\ud83c\uddfa|\ud83c\uddf0\ud83c\uddf7|\ud83c\uddef\ud83c\uddf5|\ud83c\uddee\ud83c\uddf9|\ud83c\uddec\ud83c\udde7|\ud83c\uddeb\ud83c\uddf7|\ud83c\uddea\ud83c\uddf8|\ud83c\udde9\ud83c\uddea|\u0039\u20e3|\u0038\u20e3|\u0037\u20e3|\u0036\u20e3|\u0035\u20e3|\u0034\u20e3|\u0033\u20e3|\u0032\u20e3|\u0031\u20e3|\u0030\u20e3|\u0023\u20e3|\ud83d\udeb3|\ud83d\udeb1|\ud83d\udeb0|\ud83d\udeaf|\ud83d\udeae|\ud83d\udea6|\ud83d\udea3|\ud83d\udea1|\ud83d\udea0|\ud83d\ude9f|\ud83d\ude9e|\ud83d\ude9d|\ud83d\ude9c|\ud83d\ude9b|\ud83d\ude98|\ud83d\ude96|\ud83d\ude94|\ud83d\ude90|\ud83d\ude8e|\ud83d\ude8d|\ud83d\ude8b|\ud83d\ude8a|\ud83d\ude88|\ud83d\ude86|\ud83d\ude82|\ud83d\ude81|\ud83d\ude36|\ud83d\ude34|\ud83d\ude2f|\ud83d\ude2e|\ud83d\ude2c|\ud83d\ude27|\ud83d\ude26|\ud83d\ude1f|\ud83d\ude1b|\ud83d\ude19|\ud83d\ude17|\ud83d\ude15|\ud83d\ude11|\ud83d\ude10|\ud83d\ude0e|\ud83d\ude08|\ud83d\ude07|\ud83d\ude00|\ud83d\udd67|\ud83d\udd66|\ud83d\udd65|\ud83d\udd64|\ud83d\udd63|\ud83d\udd62|\ud83d\udd61|\ud83d\udd60|\ud83d\udd5f|\ud83d\udd5e|\ud83d\udd5d|\ud83d\udd5c|\ud83d\udd2d|\ud83d\udd2c|\ud83d\udd15|\ud83d\udd09|\ud83d\udd08|\ud83d\udd07|\ud83d\udd06|\ud83d\udd05|\ud83d\udd04|\ud83d\udd02|\ud83d\udd01|\ud83d\udd00|\ud83d\udcf5|\ud83d\udcef|\ud83d\udced|\ud83d\udcec|\ud83d\udcb7|\ud83d\udcb6|\ud83d\udcad|\ud83d\udc6d|\ud83d\udc6c|\ud83d\udc65|\ud83d\udc2a|\ud83d\udc16|\ud83d\udc15|\ud83d\udc13|\ud83d\udc10|\ud83d\udc0f|\ud83d\udc0b|\ud83d\udc0a|\ud83d\udc09|\ud83d\udc08|\ud83d\udc07|\ud83d\udc06|\ud83d\udc05|\ud83d\udc04|\ud83d\udc03|\ud83d\udc02|\ud83d\udc01|\ud83d\udc00|\ud83c\udfe4|\ud83c\udfc9|\ud83c\udfc7|\ud83c\udf7c|\ud83c\udf50|\ud83c\udf4b|\ud83c\udf33|\ud83c\udf32|\ud83c\udf1e|\ud83c\udf1d|\ud83c\udf1c|\ud83c\udf1a|\ud83c\udf18|\ud83c\udccf|\ud83c\udd70|\ud83c\udd71|\ud83c\udd7e|\ud83c\udd8e|\ud83c\udd91|\ud83c\udd92|\ud83c\udd93|\ud83c\udd94|\ud83c\udd95|\ud83c\udd96|\ud83c\udd97|\ud83c\udd98|\ud83c\udd99|\ud83c\udd9a|\ud83d\ude0d|\ud83d\udec5|\ud83d\udec4|\ud83d\udec3|\ud83d\udec2|\ud83d\udec1|\ud83d\udebf|\ud83d\udeb8|\ud83d\udeb7|\ud83d\udeb5|\ud83c\ude01|\ud83c\ude02|\ud83c\ude32|\ud83c\ude33|\ud83c\ude34|\ud83c\ude35|\ud83c\ude36|\ud83c\ude37|\ud83c\ude38|\ud83c\ude39|\ud83c\ude3a|\ud83c\ude50|\ud83c\ude51|\ud83c\udf00|\ud83c\udf01|\ud83c\udf02|\ud83c\udf03|\ud83c\udf04|\ud83c\udf05|\ud83c\udf06|\ud83c\udf07|\ud83c\udf08|\ud83c\udf09|\ud83c\udf0a|\ud83c\udf0b|\ud83c\udf0c|\ud83c\udf0f|\ud83c\udf11|\ud83c\udf13|\ud83c\udf14|\ud83c\udf15|\ud83c\udf19|\ud83c\udf1b|\ud83c\udf1f|\ud83c\udf20|\ud83c\udf30|\ud83c\udf31|\ud83c\udf34|\ud83c\udf35|\ud83c\udf37|\ud83c\udf38|\ud83c\udf39|\ud83c\udf3a|\ud83c\udf3b|\ud83c\udf3c|\ud83c\udf3d|\ud83c\udf3e|\ud83c\udf3f|\ud83c\udf40|\ud83c\udf41|\ud83c\udf42|\ud83c\udf43|\ud83c\udf44|\ud83c\udf45|\ud83c\udf46|\ud83c\udf47|\ud83c\udf48|\ud83c\udf49|\ud83c\udf4a|\ud83c\udf4c|\ud83c\udf4d|\ud83c\udf4e|\ud83c\udf4f|\ud83c\udf51|\ud83c\udf52|\ud83c\udf53|\ud83c\udf54|\ud83c\udf55|\ud83c\udf56|\ud83c\udf57|\ud83c\udf58|\ud83c\udf59|\ud83c\udf5a|\ud83c\udf5b|\ud83c\udf5c|\ud83c\udf5d|\ud83c\udf5e|\ud83c\udf5f|\ud83c\udf60|\ud83c\udf61|\ud83c\udf62|\ud83c\udf63|\ud83c\udf64|\ud83c\udf65|\ud83c\udf66|\ud83c\udf67|\ud83c\udf68|\ud83c\udf69|\ud83c\udf6a|\ud83c\udf6b|\ud83c\udf6c|\ud83c\udf6d|\ud83c\udf6e|\ud83c\udf6f|\ud83c\udf70|\ud83c\udf71|\ud83c\udf72|\ud83c\udf73|\ud83c\udf74|\ud83c\udf75|\ud83c\udf76|\ud83c\udf77|\ud83c\udf78|\ud83c\udf79|\ud83c\udf7a|\ud83c\udf7b|\ud83c\udf80|\ud83c\udf81|\ud83c\udf82|\ud83c\udf83|\ud83c\udf84|\ud83c\udf85|\ud83c\udf86|\ud83c\udf87|\ud83c\udf88|\ud83c\udf89|\ud83c\udf8a|\ud83c\udf8b|\ud83c\udf8c|\ud83c\udf8d|\ud83c\udf8e|\ud83c\udf8f|\ud83c\udf90|\ud83c\udf91|\ud83c\udf92|\ud83c\udf93|\ud83c\udfa0|\ud83c\udfa1|\ud83c\udfa2|\ud83c\udfa3|\ud83c\udfa4|\ud83c\udfa5|\ud83c\udfa6|\ud83c\udfa7|\ud83c\udfa8|\ud83c\udfa9|\ud83c\udfaa|\ud83c\udfab|\ud83c\udfac|\ud83c\udfad|\ud83c\udfae|\ud83c\udfaf|\ud83c\udfb0|\ud83c\udfb1|\ud83c\udfb2|\ud83c\udfb3|\ud83c\udfb4|\ud83c\udfb5|\ud83c\udfb6|\ud83c\udfb7|\ud83c\udfb8|\ud83c\udfb9|\ud83c\udfba|\ud83c\udfbb|\ud83c\udfbc|\ud83c\udfbd|\ud83c\udfbe|\ud83c\udfbf|\ud83c\udfc0|\ud83c\udfc1|\ud83c\udfc2|\ud83c\udfc3|\ud83c\udfc4|\ud83c\udfc6|\ud83c\udfc8|\ud83c\udfca|\ud83c\udfe0|\ud83c\udfe1|\ud83c\udfe2|\ud83c\udfe3|\ud83c\udfe5|\ud83c\udfe6|\ud83c\udfe7|\ud83c\udfe8|\ud83c\udfe9|\ud83c\udfea|\ud83c\udfeb|\ud83c\udfec|\ud83c\udfed|\ud83c\udfee|\ud83c\udfef|\ud83c\udff0|\ud83d\udc0c|\ud83d\udc0d|\ud83d\udc0e|\ud83d\udc11|\ud83d\udc12|\ud83d\udc14|\ud83d\udc17|\ud83d\udc18|\ud83d\udc19|\ud83d\udc1a|\ud83d\udc1b|\ud83d\udc1c|\ud83d\udc1d|\ud83d\udc1e|\ud83d\udc1f|\ud83d\udc20|\ud83d\udc21|\ud83d\udc22|\ud83d\udc23|\ud83d\udc24|\ud83d\udc25|\ud83d\udc26|\ud83d\udc27|\ud83d\udc28|\ud83d\udc29|\ud83d\udc2b|\ud83d\udc2c|\ud83d\udc2d|\ud83d\udc2e|\ud83d\udc2f|\ud83d\udc30|\ud83d\udc31|\ud83d\udc32|\ud83d\udc33|\ud83d\udc34|\ud83d\udc35|\ud83d\udc36|\ud83d\udc37|\ud83d\udc38|\ud83d\udc39|\ud83d\udc3a|\ud83d\udc3b|\ud83d\udc3c|\ud83d\udc3d|\ud83d\udc3e|\ud83d\udc40|\ud83d\udc42|\ud83d\udc43|\ud83d\udc44|\ud83d\udc45|\ud83d\udc46|\ud83d\udc47|\ud83d\udc48|\ud83d\udc49|\ud83d\udc4a|\ud83d\udc4b|\ud83d\udc4c|\ud83d\udc4d|\ud83d\udc4e|\ud83d\udc4f|\ud83d\udc50|\ud83d\udc51|\ud83d\udc52|\ud83d\udc53|\ud83d\udc54|\ud83d\udc55|\ud83d\udc56|\ud83d\udc57|\ud83d\udc58|\ud83d\udc59|\ud83d\udc5a|\ud83d\udc5b|\ud83d\udc5c|\ud83d\udc5d|\ud83d\udc5e|\ud83d\udc5f|\ud83d\udc60|\ud83d\udc61|\ud83d\udc62|\ud83d\udc63|\ud83d\udc64|\ud83d\udc66|\ud83d\udc67|\ud83d\udc68|\ud83d\udc69|\ud83d\udc6a|\ud83d\udc6b|\ud83d\udc6e|\ud83d\udc6f|\ud83d\udc70|\ud83d\udc71|\ud83d\udc72|\ud83d\udc73|\ud83d\udc74|\ud83d\udc75|\ud83d\udc76|\ud83d\udc77|\ud83d\udc78|\ud83d\udc79|\ud83d\udc7a|\ud83d\udc7b|\ud83d\udc7c|\ud83d\udc7d|\ud83d\udc7e|\ud83d\udc7f|\ud83d\udc80|\ud83d\udc81|\ud83d\udc82|\ud83d\udc83|\ud83d\udc84|\ud83d\udc85|\ud83d\udc86|\ud83d\udc87|\ud83d\udc88|\ud83d\udc89|\ud83d\udc8a|\ud83d\udc8b|\ud83d\udc8c|\ud83d\udc8d|\ud83d\udc8e|\ud83d\udc8f|\ud83d\udc90|\ud83d\udc91|\ud83d\udc92|\ud83d\udc93|\ud83d\udc94|\ud83d\udc95|\ud83d\udc96|\ud83d\udc97|\ud83d\udc98|\ud83d\udc99|\ud83d\udc9a|\ud83d\udc9b|\ud83d\udc9c|\ud83d\udc9d|\ud83d\udc9e|\ud83d\udc9f|\ud83d\udca0|\ud83d\udca1|\ud83d\udca2|\ud83d\udca3|\ud83d\udca4|\ud83d\udca5|\ud83d\udca6|\ud83d\udca7|\ud83d\udca8|\ud83d\udca9|\ud83d\udcaa|\ud83d\udcab|\ud83d\udcac|\ud83d\udcae|\ud83d\udcaf|\ud83d\udcb0|\ud83d\udcb1|\ud83d\udcb2|\ud83d\udcb3|\ud83d\udcb4|\ud83d\udcb5|\ud83d\udcb8|\ud83d\udcb9|\ud83d\udcba|\ud83d\udcbb|\ud83d\udcbc|\ud83d\udcbd|\ud83d\udcbe|\ud83d\udcbf|\ud83d\udcc0|\ud83d\udcc1|\ud83d\udcc2|\ud83d\udcc3|\ud83d\udcc4|\ud83d\udcc5|\ud83d\udcc6|\ud83d\udcc7|\ud83d\udcc8|\ud83d\udcc9|\ud83d\udcca|\ud83d\udccb|\ud83d\udccc|\ud83d\udccd|\ud83d\udcce|\ud83d\udccf|\ud83d\udcd0|\ud83d\udcd1|\ud83d\udcd2|\ud83d\udcd3|\ud83d\udcd4|\ud83d\udcd5|\ud83d\udcd6|\ud83d\udcd7|\ud83d\udcd8|\ud83d\udcd9|\ud83d\udcda|\ud83d\udcdb|\ud83d\udcdc|\ud83d\udcdd|\ud83d\udcde|\ud83d\udcdf|\ud83d\udce0|\ud83d\udce1|\ud83d\udce2|\ud83d\udce3|\ud83d\udce4|\ud83d\udce5|\ud83d\udce6|\ud83d\udce7|\ud83d\udce8|\ud83d\udce9|\ud83d\udcea|\ud83d\udceb|\ud83d\udcee|\ud83d\udcf0|\ud83d\udcf1|\ud83d\udcf2|\ud83d\udcf3|\ud83d\udcf4|\ud83d\udcf6|\ud83d\udcf7|\ud83d\udcf9|\ud83d\udcfa|\ud83d\udcfb|\ud83d\udcfc|\ud83d\udd03|\ud83d\udd0a|\ud83d\udd0b|\ud83d\udd0c|\ud83d\udd0d|\ud83d\udd0e|\ud83d\udd0f|\ud83d\udd10|\ud83d\udd11|\ud83d\udd12|\ud83d\udd13|\ud83d\udd14|\ud83d\udd16|\ud83d\udd17|\ud83d\udd18|\ud83d\udd19|\ud83d\udd1a|\ud83d\udd1b|\ud83d\udd1c|\ud83d\udd1d|\ud83d\udd1e|\ud83d\udd1f|\ud83d\udd20|\ud83d\udd21|\ud83d\udd22|\ud83d\udd23|\ud83d\udd24|\ud83d\udd25|\ud83d\udd26|\ud83d\udd27|\ud83d\udd28|\ud83d\udd29|\ud83d\udd2a|\ud83d\udd2b|\ud83d\udd2e|\ud83d\udd2f|\ud83d\udd30|\ud83d\udd31|\ud83d\udd32|\ud83d\udd33|\ud83d\udd34|\ud83d\udd35|\ud83d\udd36|\ud83d\udd37|\ud83d\udd38|\ud83d\udd39|\ud83d\udd3a|\ud83d\udd3b|\ud83d\udd3c|\ud83d\udd3d|\ud83d\udd50|\ud83d\udd51|\ud83d\udd52|\ud83d\udd53|\ud83d\udd54|\ud83d\udd55|\ud83d\udd56|\ud83d\udd57|\ud83d\udd58|\ud83d\udd59|\ud83d\udd5a|\ud83d\udd5b|\ud83d\uddfb|\ud83d\uddfc|\ud83d\uddfd|\ud83d\uddfe|\ud83d\uddff|\ud83d\ude01|\ud83d\ude02|\ud83d\ude03|\ud83d\ude04|\ud83d\ude05|\ud83d\ude06|\ud83d\ude09|\ud83d\ude0a|\ud83d\ude0b|\ud83d\ude0c|\ud83d\udeb4|\ud83d\ude0f|\ud83d\ude12|\ud83d\ude13|\ud83d\ude14|\ud83d\ude16|\ud83d\ude18|\ud83d\ude1a|\ud83d\ude1c|\ud83d\ude1d|\ud83d\ude1e|\ud83d\ude20|\ud83d\ude21|\ud83d\ude22|\ud83d\ude23|\ud83d\ude24|\ud83d\ude25|\ud83d\ude28|\ud83d\ude29|\ud83d\ude2a|\ud83d\ude2b|\ud83d\ude2d|\ud83d\ude30|\ud83d\ude31|\ud83d\ude32|\ud83d\ude33|\ud83d\ude35|\ud83d\ude37|\ud83d\ude38|\ud83d\ude39|\ud83d\ude3a|\ud83d\ude3b|\ud83d\ude3c|\ud83d\ude3d|\ud83d\ude3e|\ud83d\ude3f|\ud83d\ude40|\ud83d\ude45|\ud83d\ude46|\ud83d\ude47|\ud83d\ude48|\ud83d\ude49|\ud83d\ude4a|\ud83d\ude4b|\ud83d\ude4c|\ud83d\ude4d|\ud83d\ude4e|\ud83d\ude4f|\ud83d\ude80|\ud83d\ude83|\ud83d\ude84|\ud83d\ude85|\ud83d\ude87|\ud83d\ude89|\ud83d\ude8c|\ud83d\ude8f|\ud83d\ude91|\ud83d\ude92|\ud83d\ude93|\ud83d\ude95|\ud83d\ude97|\ud83d\ude99|\ud83d\ude9a|\ud83d\udea2|\ud83d\udea4|\ud83d\udea5|\ud83d\udea7|\ud83d\udea8|\ud83d\udea9|\ud83d\udeaa|\ud83d\udeab|\ud83d\udeac|\ud83d\udead|\ud83d\udeb2|\ud83d\udeb6|\ud83d\udeb9|\ud83d\udeba|\ud83d\udebb|\ud83d\udebc|\ud83d\udebd|\ud83d\udebe|\ud83d\udec0|\ud83c\udde6|\ud83c\udde7|\ud83c\udde8|\ud83c\udde9|\ud83c\uddea|\ud83c\uddeb|\ud83c\uddec|\ud83c\udded|\ud83c\uddee|\ud83c\uddef|\ud83c\uddf0|\ud83c\uddf1|\ud83c\uddf2|\ud83c\uddf3|\ud83c\uddf4|\ud83c\uddf5|\ud83c\uddf6|\ud83c\uddf7|\ud83c\uddf8|\ud83c\uddf9|\ud83c\uddfa|\ud83c\uddfb|\ud83c\uddfc|\ud83c\uddfd|\ud83c\uddfe|\ud83c\uddff|\ud83c\udf0d|\ud83c\udf0e|\ud83c\udf10|\ud83c\udf12|\ud83c\udf16|\ud83c\udf17|\ud83c\udf18|\ud83c\udf1a|\ud83c\udf1c|\ud83c\udf1d|\ud83c\udf1e|\ud83c\udf32|\ud83c\udf33|\ud83c\udf4b|\ud83c\udf50|\ud83c\udf7c|\ud83c\udfc7|\ud83c\udfc9|\ud83c\udfe4|\ud83d\udc00|\ud83d\udc01|\ud83d\udc02|\ud83d\udc03|\ud83d\udc04|\ud83d\udc05|\ud83d\udc06|\ud83d\udc07|\ud83d\udc08|\ud83d\udc09|\ud83d\udc0a|\ud83d\udc0b|\ud83d\udc0f|\ud83d\udc10|\ud83d\udc13|\ud83d\udc15|\ud83d\udc16|\ud83d\udc2a|\ud83d\udc65|\ud83d\udc6c|\ud83d\udc6d|\ud83d\udcad|\ud83d\udcb6|\ud83d\udcb7|\ud83d\udcec|\ud83d\udced|\ud83d\udcef|\ud83d\udcf5|\ud83d\udd00|\ud83d\udd01|\ud83d\udd02|\ud83d\udd04|\ud83d\udd05|\ud83d\udd06|\ud83d\udd07|\ud83d\udd08|\ud83d\udd09|\ud83d\udd15|\ud83d\udd2c|\ud83d\udd2d|\ud83d\udd5c|\ud83d\udd5d|\ud83d\udd5e|\ud83d\udd5f|\ud83d\udd60|\ud83d\udd61|\ud83d\udd62|\ud83d\udd63|\ud83d\udd64|\ud83d\udd65|\ud83d\udd66|\ud83d\udd67|\ud83d\ude00|\ud83d\ude07|\ud83d\ude08|\ud83d\ude0e|\ud83d\ude10|\ud83d\ude11|\ud83d\ude15|\ud83d\ude17|\ud83d\ude19|\ud83d\ude1b|\ud83d\ude1f|\ud83d\ude26|\ud83d\ude27|\ud83d\ude2c|\ud83d\ude2e|\ud83d\ude2f|\ud83d\ude34|\ud83d\ude36|\ud83d\ude81|\ud83d\ude82|\ud83d\ude86|\ud83d\ude88|\ud83d\ude8a|\ud83d\ude8b|\ud83d\ude8d|\ud83d\ude8e|\ud83d\ude90|\ud83d\ude94|\ud83d\ude96|\ud83d\ude98|\ud83d\ude9b|\ud83d\ude9c|\ud83d\ude9d|\ud83d\ude9e|\ud83d\ude9f|\ud83d\udea0|\ud83d\udea1|\ud83d\udea3|\ud83d\udea6|\ud83d\udeae|\ud83d\udeaf|\ud83d\udeb0|\ud83d\udeb1|\ud83d\udeb3|\ud83d\udeb4|\ud83d\udeb5|\ud83d\udeb7|\ud83d\udeb8|\ud83d\udebf|\ud83d\udec1|\ud83d\udec2|\ud83d\udec3|\ud83d\udec4|\ud83d\udec5|\ud83c\udf17|\ud83c\udf16|\ud83c\udde6|\ud83c\udde7|\ud83c\udde8|\ud83c\udde9|\ud83c\uddea|\ud83c\uddeb|\ud83c\uddec|\ud83c\udded|\ud83c\uddee|\ud83c\uddef|\ud83c\uddf0|\ud83c\uddf1|\ud83c\uddf2|\ud83c\uddf3|\ud83c\uddf4|\ud83c\uddf5|\ud83c\uddf6|\ud83c\uddf7|\ud83c\uddf8|\ud83c\uddf9|\ud83c\uddfa|\ud83c\uddfb|\ud83c\uddfc|\ud83c\uddfd|\ud83c\uddfe|\ud83c\uddff|\ud83c\udf0d|\ud83c\udf0e|\ud83c\udf10|\ud83c\udf12|\ud83c\udf16|\ud83c\udf17|\ud83c\udf18|\ud83c\udf1a|\ud83c\udf1c|\ud83c\udf1d|\ud83c\udf1e|\ud83c\udf32|\ud83c\udf33|\ud83c\udf4b|\ud83c\udf50|\ud83c\udf7c|\ud83c\udfc7|\ud83c\udfc9|\ud83c\udfe4|\ud83d\udc00|\ud83d\udc01|\ud83d\udc02|\ud83d\udc03|\ud83d\udc04|\ud83d\udc05|\ud83d\udc06|\ud83d\udc07|\ud83d\udc08|\ud83d\udc09|\ud83d\udc0a|\ud83d\udc0b|\ud83d\udc0f|\ud83d\udc10|\ud83d\udc13|\ud83d\udc15|\ud83d\udc16|\ud83d\udc2a|\ud83d\udc65|\ud83d\udc6c|\ud83d\udc6d|\ud83d\udcad|\ud83d\udcb6|\ud83d\udcb7|\ud83d\udcec|\ud83d\udced|\ud83d\udcef|\ud83d\udcf5|\ud83d\udd00|\ud83d\udd01|\ud83d\udd02|\ud83d\udd04|\ud83d\udd05|\ud83d\udd06|\ud83d\udd07|\ud83d\udd08|\ud83d\udd09|\ud83d\udd15|\ud83d\udd2c|\ud83d\udd2d|\ud83d\udd5c|\ud83d\udd5d|\ud83d\udd5e|\ud83d\udd5f|\ud83d\udd60|\ud83d\udd61|\ud83d\udd62|\ud83d\udd63|\ud83d\udd64|\ud83d\udd65|\ud83d\udd66|\ud83d\udd67|\ud83d\ude00|\ud83d\ude07|\ud83d\ude08|\ud83d\ude0e|\ud83d\ude10|\ud83d\ude11|\ud83d\ude15|\ud83d\ude17|\ud83d\ude19|\ud83d\ude1b|\ud83d\ude1f|\ud83d\ude26|\ud83d\ude27|\ud83d\ude2c|\ud83d\ude2e|\ud83d\ude2f|\ud83d\ude34|\ud83d\ude36|\ud83d\ude81|\ud83d\ude82|\ud83d\ude86|\ud83d\ude88|\ud83d\ude8a|\ud83d\ude8b|\ud83d\ude8d|\ud83d\ude8e|\ud83d\ude90|\ud83d\ude94|\ud83d\ude96|\ud83d\ude98|\ud83d\ude9b|\ud83d\ude9c|\ud83d\ude9d|\ud83d\ude9e|\ud83d\ude9f|\ud83d\udea0|\ud83d\udea1|\ud83d\udea3|\ud83d\udea6|\ud83d\udeae|\ud83d\udeaf|\ud83d\udeb0|\ud83d\udeb1|\ud83d\udeb3|\ud83d\udeb4|\ud83d\udeb5|\ud83d\udeb7|\ud83d\udeb8|\ud83d\udebf|\ud83d\udec1|\ud83d\udec2|\ud83d\udec3|\ud83d\udec4|\ud83d\udec5|\ud83c\udf12|\ud83c\udf10|\ud83c\udde6|\ud83c\udde7|\ud83c\udde8|\ud83c\udde9|\ud83c\uddea|\ud83c\uddeb|\ud83c\uddec|\ud83c\udded|\ud83c\uddee|\ud83c\uddef|\ud83c\uddf0|\ud83c\uddf1|\ud83c\uddf2|\ud83c\uddf3|\ud83c\uddf4|\ud83c\uddf5|\ud83c\uddf6|\ud83c\uddf7|\ud83c\uddf8|\ud83c\uddf9|\ud83c\uddfa|\ud83c\uddfb|\ud83c\uddfc|\ud83c\uddfd|\ud83c\uddfe|\ud83c\uddff|\ud83c\udf0d|\ud83c\udf0e|\ue50a|\ue50a|\ue50a|\u27bf|\u3030|\u27b0|\u2797|\u2796|\u2795|\u2755|\u2754|\u2753|\u274e|\u274c|\u2728|\u270b|\u270a|\u2705|\u26ce|\u27bf|\u23f3|\u23f0|\u23ec|\u23eb|\u23ea|\u23e9|\u2122|\u27bf|\u00a9|\u00ae)|(?:(?:\ud83c\udc04|\ud83c\udd7f|\ud83c\ude1a|\ud83c\ude2f|\u3299|\u3297|\u303d|\u2b55|\u2b50|\u2b1c|\u2b1b|\u2b07|\u2b06|\u2b05|\u2935|\u2934|\u27a1|\u2764|\u2757|\u2747|\u2744|\u2734|\u2733|\u2716|\u2714|\u2712|\u270f|\u270c|\u2709|\u2708|\u2702|\u26fd|\u26fa|\u26f5|\u26f3|\u26f2|\u26ea|\u26d4|\u26c5|\u26c4|\u26be|\u26bd|\u26ab|\u26aa|\u26a1|\u26a0|\u2693|\u267b|\u2668|\u2666|\u2665|\u2663|\u2660|\u2653|\u2652|\u2651|\u2650|\u264f|\u264e|\u264d|\u264c|\u264b|\u264a|\u2649|\u2648|\u263a|\u261d|\u2615|\u2614|\u2611|\u260e|\u2601|\u2600|\u25fe|\u25fd|\u25fc|\u25fb|\u25c0|\u25b6|\u25ab|\u25aa|\u24c2|\u231b|\u231a|\u21aa|\u21a9|\u2199|\u2198|\u2197|\u2196|\u2195|\u2194|\u2139|\u2049|\u203c|\u267f)([\uFE0E\uFE0F]?)))/g,

    // nodes with type 1 which should **not** be parsed
    shouldntBeParsed = /IFRAME|NOFRAMES|NOSCRIPT|SCRIPT|STYLE/,

    // just a private shortcut
    fromCharCode = String.fromCharCode;

  return twemoji;


  /////////////////////////
  //  private functions  //
  //     declaration     //
  /////////////////////////

  /**
   * Shortcut to create text nodes
   * @param   string  text used to create DOM text node
   * @return  Node  a DOM node with that text
   */
  function createText(text) {
    return document.createTextNode(text);
  }

  /**
   * Default callback used to generate emoji src
   *  based on Twitter CDN
   * @param   string    the emoji codepoint string
   * @param   string    the default size to use, i.e. "36x36"
   * @param   string    optional "\uFE0F" variant char, ignored by default
   * @return  string    the image source to use
   */
  function defaultImageSrcGenerator(icon, options) {
    return ''.concat(options.base, options.size, '/', icon, options.ext);
  }

  /**
   * Given a generic DOM nodeType 1, walk through all children
   * and store every nodeType 3 (#text) found in the tree.
   * @param   Element a DOM Element with probably some text in it
   * @param   Array the list of previously discovered text nodes
   * @return  Array same list with new discovered nodes, if any
   */
  function grabAllTextNodes(node, allText) {
    var
      childNodes = node.childNodes,
      length = childNodes.length,
      subnode,
      nodeType;
    while (length--) {
      subnode = childNodes[length];
      nodeType = subnode.nodeType;
      // parse emoji only in text nodes
      if (nodeType === 3) {
        // collect them to process emoji later
        allText.push(subnode);
      }
      // ignore all nodes that are not type 1 or that
      // should not be parsed as script, style, and others
      else if (nodeType === 1 && !shouldntBeParsed.test(subnode.nodeName)) {
        grabAllTextNodes(subnode, allText);
      }
    }
    return allText;
  }

  /**
   * Used to both remove the possible variant
   *  and to convert utf16 into code points
   * @param   string    the emoji surrogate pair
   * @param   string    the optional variant char, if any
   */
  function grabTheRightIcon(icon, variant) {
    // if variant is present as \uFE0F
    return toCodePoint(
      variant === '\uFE0F' ?
        // the icon should not contain it
        icon.slice(0, -1) :
        icon
    );
  }

  /**
   * DOM version of the same logic / parser:
   *  emojify all found sub-text nodes placing images node instead.
   * @param   Element   generic DOM node with some text in some child node
   * @param   Object    options  containing info about how to parse
    *
    *            .callback   Function  the callback to invoke per each found emoji.
    *            .base       string    the base url, by default twemoji.base
    *            .ext        string    the image extension, by default twemoji.ext
    *            .size       string    the assets size, by default twemoji.size
    *
   * @return  Element same generic node with emoji in place, if any.
   */
  function parseNode(node, options) {
    var
      allText = grabAllTextNodes(node, []),
      length = allText.length,
      fragment,
      subnode,
      text,
      match,
      i,
      index,
      img,
      alt,
      icon,
      variant,
      src;
    while (length--) {
      fragment = document.createDocumentFragment();
      subnode = allText[length];
      text = subnode.nodeValue;
      i = 0;
      while ((match = re.exec(text))) {
        index = match.index;
        if (index !== i) {
          fragment.appendChild(
            createText(text.slice(i, index))
          );
        }
        alt = match[0];
        icon = match[1];
        variant = match[2];
        i = index + alt.length;
        if (variant !== '\uFE0E') {
          src = options.callback(
            grabTheRightIcon(icon, variant),
            options,
            variant
          );
          if (src) {
            img = new Image();
            img.onerror = twemoji.onerror;
            img.className = 'emoji';
            img.setAttribute('draggable', 'false');
            img.alt = alt;
            img.src = src;
          }
        }
        fragment.appendChild(img || createText(alt));
        img = null;
      }
      // is there actually anything to replace in here ?
      if (0 < i) {
        // any text left to be added ?
        if (i < text.length) {
          fragment.appendChild(
            createText(text.slice(i))
          );
        }
        // replace the text node only, leave intact
        // anything else surrounding such text
        subnode.parentNode.replaceChild(fragment, subnode);
      }
    }
    return node;
  }

  /**
   * String/HTML version of the same logic / parser:
   *  emojify a generic text placing images tags instead of surrogates pair.
   * @param   string    generic string with possibly some emoji in it
   * @param   Object    options  containing info about how to parse
   *
   *            .callback   Function  the callback to invoke per each found emoji.
   *            .base       string    the base url, by default twemoji.base
   *            .ext        string    the image extension, by default twemoji.ext
   *            .size       string    the assets size, by default twemoji.size
   *
   * @return  the string with <img tags> replacing all found and parsed emoji
   */
  function parseString(str, options) {
    return replace(str, function (match, icon, variant) {
      var src;
      // verify the variant is not the FE0E one
      // this variant means "emoji as text" and should not
      // require any action/replacement
      // http://unicode.org/Public/UNIDATA/StandardizedVariants.html
      if (variant !== '\uFE0E') {
        src = options.callback(
          grabTheRightIcon(icon, variant),
          options,
          variant
        );
        if (src) {
          // recycle the match string replacing the emoji
          // with its image counter part
          match = '<img '.concat(
            'class="emoji" ',
            'draggable="false" ',
            // needs to preserve user original intent
            // when variants should be copied and pasted too
            'alt="',
            match,
            '" ',
            'src="',
            src,
            '"',
            '>'
          );
        }
      }
      return match;
    });
  }

  /**
   * Given a generic value, creates its squared counterpart if it's a number.
   *  As example, number 36 will return '36x36'.
   * @param   any     a generic value.
   * @return  any     a string representing asset size, i.e. "36x36"
   *                  only in case the value was a number.
   *                  Returns initial value otherwise.
   */
  function toSizeSquaredAsset(value) {
    return typeof value === 'number' ?
      value + 'x' + value :
      value;
  }


  /////////////////////////
  //  exported functions //
  //     declaration     //
  /////////////////////////

  function fromCodePoint(codepoint) {
    var code = typeof codepoint === 'string' ?
          parseInt(codepoint, 16) : codepoint;
    if (code < 0x10000) {
      return fromCharCode(code);
    }
    code -= 0x10000;
    return fromCharCode(
      0xD800 + (code >> 10),
      0xDC00 + (code & 0x3FF)
    );
  }

  function parse(what, how) {
    if (!how || typeof how === 'function') {
      how = {callback: how};
    }
    // if first argument is string, inject html <img> tags
    // otherwise use the DOM tree and parse text nodes only
    return (typeof what === 'string' ? parseString : parseNode)(what, {
      callback: how.callback || defaultImageSrcGenerator,
      base:     typeof how.base === 'string' ? how.base : twemoji.base,
      ext:      how.ext || twemoji.ext,
      size:     "svg" // toSizeSquaredAsset(how.size || twemoji.size)
    });
  }

  function replace(text, callback) {
    return String(text).replace(re, callback);
  }

  function test(text) {
    // IE6 needs a reset before too
    re.lastIndex = 0;
    var result = re.test(text);
    re.lastIndex = 0;
    return result;
  }

  function toCodePoint(unicodeSurrogates, sep) {
    var
      r = [],
      c = 0,
      p = 0,
      i = 0;
    while (i < unicodeSurrogates.length) {
      c = unicodeSurrogates.charCodeAt(i++);
      if (p) {
        r.push((0x10000 + ((p - 0xD800) << 10) + (c - 0xDC00)).toString(16));
        p = 0;
      } else if (0xD800 <= c && c <= 0xDBFF) {
        p = c;
      } else {
        r.push(c.toString(16));
      }
    }
    return r.join(sep || '-');
  }

}());

;
var JetpackEmoji = {
		EMOJI_SIZE: 72,
		BASE_URL: '//s0.wp.com/wp-content/mu-plugins/emoji/twemoji/',

        parse: function( element, size, base_url ) {
            twemoji.parse( element, {
                size: size || this.EMOJI_SIZE,
				base: base_url || this.BASE_URL,
				callback: function(icon, options, variant) {
					// Ignore some standard characters that TinyMCE recommends in its character map.
					// https://teamtinker.wordpress.com/2014/10/29/emoji-launch-week/#comment-2103
					switch ( icon ) {
						case 'a9':
						case 'ae':
						case '2122':
						case '2194':
						case '2660':
						case '2663':
						case '2665':
						case '2666':
							return false;
					}
				
					// directly from twemoji
					return ''.concat(options.base, options.size, '/', icon, options.ext);
				}
            } );
        }
}
;
(function() {
    var emoji = {	
        init: function() {
			if ( typeof JetpackEmoji === 'undefined' ) {
				console.error( 'JetpackEmoji not found.' );
				return;
			}

			var size, base_url;
			if ( typeof JetpackEmojiSettings !== 'undefined' ) {
				size = JetpackEmojiSettings.size || null;
				base_url = JetpackEmojiSettings.base_url || null;
			}	

            JetpackEmoji.parse( document.body, size, base_url );

            if ( typeof infiniteScroll !== 'undefined' ) {
                jQuery( document.body ).on( 'post-load', function( response ) {
                    // TODO: ideally, we should only target the newly added elements
                    JetpackEmoji.parse( document.body, size, base_url );
                } );
            }
        }
    };

    if ( window.addEventListener ) {
        window.addEventListener( 'load', emoji.init, false );
    } else if ( window.attachEvent ) {
        window.attachEvent( 'onload', emoji.init );
    }
})();
;
