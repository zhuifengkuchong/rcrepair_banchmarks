var sliderObject = { 
	slides : {
		mainHome : [
		{"imageSrc":"../images/lets-connect-584x225.jpg"/*tpa=http://media.optimumonline.com/images/lets-connect-584x225.jpg*/,"link":"http://www.optimum.com/order/triple_play.jsp","id":"triple_play","alt":"The Optimum Triple Play","title":"The Optimum Triple Play","pos":"h1","name":"Home: The Optimum Triple Play"},
		{"imageSrc":"../images/CS-921_DPB_584x225.jpg"/*tpa=http://media.optimumonline.com/images/CS-921_DPB_584x225.jpg*/,"link":"http://www.optimum.com/order/tv_internet.jsp","id":"tvandinternet","alt":"Optimum TV & Internet","title":"Optimum TV & Internet","pos":"h2","name":"Home: Optimum TV & Internet"},
		{"imageSrc":"../images/CS-807_Biz-Bundle_584x225.jpg"/*tpa=http://media.optimumonline.com/images/CS-807_Biz-Bundle_584x225.jpg*/,"link":"http://www.optimumbusiness.com/","id":"business","alt":"Optimum Business","title":"Phone and Internet for Business","pos":"h3","name":"Home: Optimum Business"},
		{"imageSrc":"../images/CS-874_OOL_39_95_584x225.jpg"/*tpa=http://media.optimumonline.com/images/CS-874_OOL_39_95_584x225.jpg*/,"link":"http://www.optimum.com/home-internet-service/?s_cid=ool_3995_banner_home","id":"ool","alt":"Optimum Online","title":"Optimum Online 39.95/mo for a year","pos":"h4","name":"Home: Optimum Online"},
		{"imageSrc":"../images/CS-1171_Piggy-WiFi_584x225.jpg"/*tpa=http://media.optimumonline.com/images/CS-1171_Piggy-WiFi_584x225.jpg*/,"link":"/home-internet-service/wifi-service.jsp/?s_cid=one_million_hotspots_optcom","id":"onemilhs","alt":"Optimum WiFi","title":"Optimum WiFi","pos":"h5","name":"Home: Optimum WiFi"}
		],
		cvcHome : [
		{"imageSrc":"../images/lets-connect-584x225.jpg"/*tpa=http://media.optimumonline.com/images/lets-connect-584x225.jpg*/,"link":"http://www.optimum.com/order/triple_play.jsp?s_cid=cvccom_tp","id":"triple_play","alt":"The Optimum Triple Play","title":"The Optimum Triple Play","pos":"h1","name":"Home: The Optimum Triple Play"},
		{"imageSrc":"../images/CS-921_DPB_584x225.jpg"/*tpa=http://media.optimumonline.com/images/CS-921_DPB_584x225.jpg*/,"link":"http://www.optimum.com/order/tv_internet.jsp?s_cid=cvccom_tvInternet_7995","id":"tvandinternet","alt":"Optimum TV & Internet","title":"Optimum TV & Internet","pos":"h2","name":"Home: Optimum TV & Internet"},
		{"imageSrc":"../images/CS-807_Biz-Bundle_584x225.jpg"/*tpa=http://media.optimumonline.com/images/CS-807_Biz-Bundle_584x225.jpg*/,"link":"http://www.optimumbusiness.com/","id":"business","alt":"Optimum Business","title":"Phone and Internet for Business","pos":"h3","name":"Home: Optimum Business"},
		{"imageSrc":"../images/CS-874_OOL_39_95_584x225.jpg"/*tpa=http://media.optimumonline.com/images/CS-874_OOL_39_95_584x225.jpg*/,"link":"http://www.optimum.com/home-internet-service/?s_cid=ool_3995_banner_home","id":"ool","alt":"Optimum Online","title":"Optimum Online 39.95/mo for a year","pos":"h4","name":"Home: Optimum Online"},
		{"imageSrc":"../images/CS-1171_Piggy-WiFi_584x225.jpg"/*tpa=http://media.optimumonline.com/images/CS-1171_Piggy-WiFi_584x225.jpg*/,"link":"http://www.optimum.com/home-internet-service/wifi-service.jsp/?s_cid=one_million_hotspots_cvccom","id":"onemilhs","alt":"Optimum WiFi","title":"Optimum WiFi","pos":"h5","name":"Home: Optimum WiFi"}
		],
		ioHome : [
		{"imageSrc":"../images/io/triple_play.jpg"/*tpa=http://media.optimumonline.com/images/io/triple_play.jpg*/,"link":"http://media.optimumonline.com/order/triple_play.jsp","id":"triple_play","alt":"The Triple Play - iO TV + Optimum Online + Optimum Voice","title":"","pos":"io1","name":"iO: Triple Play"},
		{"imageSrc":"../images/io/tag_games.jpg"/*tpa=http://media.optimumonline.com/images/io/tag_games.jpg*/,"link":"/digital-cable-tv/games","id":"tag_games","alt":"Introducing TAG Games - Free for iO TV Customers","title":"","pos":"io2","name":"iO: TAG Games"},
		/*{"imageSrc":"../images/io/fortypercent.jpg"/*tpa=http://media.optimumonline.com/images/io/fortypercent.jpg*/,"link":"http://media.optimumonline.com/optimum-vs-fios.jsp","id":"fortypercent","alt":"See why over 40% of Optimum customers who tried FiOS have switched back!","title":"","pos":"io3","name":"iO: 40% Customer Return"},*/
		{"imageSrc":"../images/io/io_res_home_silver.jpg"/*tpa=http://media.optimumonline.com/images/io/io_res_home_silver.jpg*/,"link":"http://media.optimumonline.com/digital-cable-tv/silver.jsp","id":"silver","alt":"iO Silver for $9.95 more a month for a year","title":"","pos":"io4","name":"iO: iO Silver"}
		],
		oolHome : [
		{"imageSrc":"../images/ool/lou.jpg"/*tpa=http://media.optimumonline.com/images/ool/lou.jpg*/,"link":"/home-internet-service/broadband","id":"lou","alt":"Up to 5x faster than phone company high-speed Internet.","title":"","pos":"ool1","name":"OOL: 5x Faster"},
		{"imageSrc":"../images/ool/fom_wifi_060111.jpg"/*tpa=http://media.optimumonline.com/images/ool/fom_wifi_060111.jpg*/,"link":"/home-internet-service/features","id":"fom_wifi","alt":"Featured of the Month: Optimum WiFi","title":"","pos":"ool2","name":"OOL: WiFi"},
		{"imageSrc":"../images/ool/ipod_200.jpg"/*tpa=http://media.optimumonline.com/images/ool/ipod_200.jpg*/,"link":"http://media.optimumonline.com/order/ipod_200.jsp","id":"ipod_200","alt":"Switch to Optimum Online and Optimum Voice and get an iPod Touch or $200 back.","title":"","pos":"ool3","name":"OOL: iPod/200"},
		{"imageSrc":"../images/ool/triple_play.jpg"/*tpa=http://media.optimumonline.com/images/ool/triple_play.jpg*/,"link":"http://media.optimumonline.com/order/triple_play.jsp","id":"triple_play","alt":"The Triple Play - iO TV + Optimum Online + Optimum Voice","title":"","pos":"ool4","name":"OOL: Triple Play"}
		],
		ovHome : [
		{"imageSrc":"../images/ov/ov_res_lou_137.jpg"/*tpa=http://media.optimumonline.com/images/ov/ov_res_lou_137.jpg*/,"link":"http://media.optimumonline.com/home-phone-service/best-phone.jsp","id":"ov_res_lou_137","alt":"Unlimited calling to the U.S., Canada and Puerto Rico for as little as $19.95/month.","title":"","pos":"ov1","name":"OV: Unlimited Calling"},
		{"imageSrc":"../images/ov/fom_homepage.jpg"/*tpa=http://media.optimumonline.com/images/ov/fom_homepage.jpg*/,"link":"http://media.optimumonline.com/home-phone-service/manage-voicemail.jsp","id":"fom_homepage","alt":"Optimum Voice Homepage - Manage all of your calling features and more online!","title":"","pos":"ov2","name":"OV: OV Homepage"},
		{"imageSrc":"../images/ov/ipod_200.jpg"/*tpa=http://media.optimumonline.com/images/ov/ipod_200.jpg*/,"link":"http://media.optimumonline.com/order/ipod_200.jsp","id":"ipod_200","alt":"Switch to Optimum Online and Optimum Voice and get an iPod Touch or $200 back.","title":"","pos":"ov3","name":"OV: iPod/200"},
		{"imageSrc":"../images/ov/triple_play.jpg"/*tpa=http://media.optimumonline.com/images/ov/triple_play.jpg*/,"link":"http://media.optimumonline.com/order/triple_play.jsp","id":"triple_play","alt":"The Triple Play - iO TV + Optimum Online + Optimum Voice","title":"","pos":"ov4","name":"OV: Triple Play"}
		],
		obbLoggedOut : [
		{"imageSrc":"../images/business/benefits/logged_out/usbt.jpg"/*tpa=http://media.optimumonline.com/images/business/benefits/logged_out/usbt.jpg*/,"link":"http://media.optimumonline.com/obb/login.jsp","id":"usbt_out","alt":"USBT - Office equipment sales & leasing","title":"","pos":"ov1","name":"OBB Logged Out: USBT"},
		{"imageSrc":"../images/business/benefits/logged_out/fedex.jpg"/*tpa=http://media.optimumonline.com/images/business/benefits/logged_out/fedex.jpg*/,"link":"http://media.optimumonline.com/obb/login.jsp","id":"fedex_out","alt":"FedEx - Ship, print and save today!","title":"","pos":"ov2","name":"OBB Logged Out: FedEx"},
		{"imageSrc":"../images/business/benefits/logged_out/gotopc.jpg"/*tpa=http://media.optimumonline.com/images/business/benefits/logged_out/gotopc.jpg*/,"link":"http://media.optimumonline.com/obb/login.jsp","id":"gotopc_out","alt":"GoToMyPC - Access Your Mac or PC from Anywhere","title":"","pos":"ov3","name":"OBB Logged Out: GoToMyPC"},
		{"imageSrc":"../images/business/benefits/logged_out/cardworks.jpg"/*tpa=http://media.optimumonline.com/images/business/benefits/logged_out/cardworks.jpg*/,"link":"http://media.optimumonline.com/obb/login.jsp","id":"cardworks_out","alt":"CardWorks - Credit card processing","title":"","pos":"ov4","name":"OBB Logged Out: CardWorks"}
		],
		obbLoggedIn : [
		{"imageSrc":"../images/business/benefits/logged_in/fedex.jpg"/*tpa=http://media.optimumonline.com/images/business/benefits/logged_in/fedex.jpg*/,"link":"https://www.optimumbusiness.com/obb/fedex.jsp?","id":"fedex_in","alt":"Special member discounts - up to 20% off select FedEx services","title":"","pos":"ov1","name":"OBB Logged In: FedEx"},
		{"imageSrc":"../images/business/benefits/logged_in/usbt.jpg"/*tpa=http://media.optimumonline.com/images/business/benefits/logged_in/usbt.jpg*/,"link":"http://optimum.usbtonline.com/?","id":"usbt_in","alt":"USBT - Office equipment sales & leasing","title":"","pos":"ov2","name":"OBB Logged In: USBT"},
		{"imageSrc":"../images/business/benefits/logged_in/cardworks.jpg"/*tpa=http://media.optimumonline.com/images/business/benefits/logged_in/cardworks.jpg*/,"link":"http://www.cardworksoffers.com/?","id":"cardworks_in","alt":"CardWorks - Credit card processing","title":"","pos":"ov3","name":"OBB Logged In: CardWorks"},
		{"imageSrc":"../images/business/benefits/logged_in/gotopc.jpg"/*tpa=http://media.optimumonline.com/images/business/benefits/logged_in/gotopc.jpg*/,"link":"https://www.gotomypc.com/tr/obb/2011_Q1/launchtxt_021411/g25bd_obblp?Target=mm/g25bd_obblp.tmpl&obb=true","id":"gotopc_in","alt":"GoToMyPC - Try it free for 60 days","title":"","pos":"ov4","name":"OBB Logged In: GoToMyPC"}
		],
		rewardsHome : [
		{"imageSrc":"../images/rewards/espy.jpg"/*tpa=http://media.optimumonline.com/images/rewards/espy.jpg*/,"link":"http://www.optimumrewards.com/listUniqueDetail.do?uniqueId=141","id":"espy","alt":"The ESPYS","title":"","pos":"rh1","name":"Home: The ESPYS"},
		{"imageSrc":"../images/rewards/bet_960x300.jpg"/*tpa=http://media.optimumonline.com/images/rewards/bet_960x300.jpg*/,"link":"http://www.optimumrewards.com/listUniqueDetail.do?uniqueId=140","id":"bet","alt":"BET Awards 2011","title":"","pos":"rh2","name":"Home: BET Awards 2011"},
		{"imageSrc":"../images/rewards/movies/rio.jpg"/*tpa=http://media.optimumonline.com/images/rewards/movies/rio.jpg*/,"link":"http://www.optimumrewards.com/cinemaRewards.do","id":"rio","alt":"Cinema Rewards - Rio","title":"","pos":"rh2","name":"Home: Cinema Rewards - Rio"},
		{"imageSrc":"../images/rewards/movies/sucker_punch.jpg"/*tpa=http://media.optimumonline.com/images/rewards/movies/sucker_punch.jpg*/,"link":"http://www.optimumrewards.com/cinemaRewards.do","id":"sucker_punch","alt":"Cinema Rewards - Sucker Punch","title":"","pos":"rh3","name":"Home: Cinema Rewards - Sucker Punch"}
		],
		rewardsUE : [
		{"imageSrc":"../images/rewards/ue/sny_051112.jpg"/*tpa=http://media.optimumonline.com/images/rewards/ue/sny_051112.jpg*/,"link":"http://www.optimumrewards.com/listUniqueDetail.do?uniqueId=190","id":"sny","alt":"sny","title":"","pos":"ue1","name":"UE: SNY"}
		],
		rewardsNew : [
		{"imageSrc":"../images/rewards/optimum_rewards_discounts_home_banner.jpg"/*tpa=http://media.optimumonline.com/images/rewards/optimum_rewards_discounts_home_banner.jpg*/,"link":"http://www.optimumrewards.com/listRewards.do?categoryId=86&catName=Live%20Events","id":"optimumRewardsDiscounts","alt":"Optimum Rewards Live Events","title":"","pos":"rh1","name":"Optimum Rewards Live Events"}
		]
	}
};

function doSlider(sliderPage,site,w,h) {
	imageWidth = w.slice(0,-2);
	imageHeight = h.slice(0,-2);
	switch(sliderPage) {
		case "mainHome" : sObj = sliderObject.slides.mainHome;break;
		case "cvcHome" : sObj = sliderObject.slides.cvcHome;break;
		case "ioHome" : sObj = sliderObject.slides.ioHome;break;
		case "oolHome" : sObj = sliderObject.slides.oolHome;break;
		case "ovHome" : sObj = sliderObject.slides.ovHome;break;
		case "bizioHome" : sObj = sliderObject.slides.bizioHome;break;
		case "bizoolHome" : sObj = sliderObject.slides.bizoolHome;break;
		case "bizovHome" : sObj = sliderObject.slides.bizovHome;break;
		case "obbLoggedOut" : sObj = sliderObject.slides.obbLoggedOut;break;
		case "obbLoggedIn" : sObj = sliderObject.slides.obbLoggedIn;break;
		case "rewardsHome" : sObj = sliderObject.slides.rewardsHome;break;
		case "rewardsUE" : sObj = sliderObject.slides.rewardsUE;break;
		case "rewardsNew" : sObj = sliderObject.slides.rewardsNew;break;
		case "storeHome" : sObj = sliderObject.slides.storeHome;break;
	}

	if (sliderPage === "rewardsHome" || sliderPage === "mainHome" || sliderPage === "rewardsUE" || sliderPage === "cvcHome" || sliderPage === "bizHome") {
		$("#slider").css({"height":h,"width":w});
	} else if (sliderPage === "rewardsNew") {
		$("#slider").css({"height":"240px","width":"720px"});
	} else if (sliderPage === "storeHome") {
		$("#slider").css({"height":"350px","width":"900px"});
	} else {
		$("#slider").css({"height":"137px","width":"584px"});
	}
	for (i=0;i<sObj.length;i++) {
		var x = sObj[i];
		if (site === "cablevision") {
			(function() { 
				var ind = i;
				var y = sObj[ind];
				$("<a/>", {
					"id":y.id,
					"href":y.link
				}).click(function() {
						s.linkTrackVars='eVar18,eVar19';
						linkCode(this,y.pos,'','','19');
						linkCode(this,y.name,'','','18');
				}).appendTo("#slider")})();				
		}
		else if (site === "rewards") {
			(function() { 
				var ind = i;
				var y = sObj[ind];
				$("<a/>", {
					"id":y.id,
					"href":y.link
				}).click(function() {
						s.linkTrackVars='eVar18,eVar19';
						linkCode(this,y.pos,'','','19');
						linkCode(this,y.name,'','','18');
				}).appendTo("#slider")})();				
		} else if (site === "optimum") {
			(function() { 
				var ind = i;
				var y = sObj[ind];
				$("<a/>", {
					"id":y.id,
					"href":y.link
				}).click(function() {
						s.linkTrackVars='eVar4,eVar11,prop28,prop29';
						s.eVar4 = y.pos;
						s.eVar11 = y.name;
						s.prop28 = "D=v4";
						s.prop29 = "D=v11";
						s.tl();
				}).appendTo("#slider")})();				
		}  else {
			(function() { 
				var ind = i;
				var y = sObj[ind];
				$("<a/>", {
					"id":y.id,
					"href":y.link
				}).click(function() {
						s.linkTrackVars='eVar4,eVar11';
						linkCode(this,y.pos,'','','4');
						linkCode(this,y.name,'','','11');
				}).appendTo("#slider")})();
		}


			$("<img/>", {
				"id": x.id + "Image",
				"src": x.imageSrc,
				"width": imageWidth,
				"height": imageHeight,
				"alt": x.alt,
				"title": x.title,
				"border": "1"
			}).appendTo("#" + x.id);	

	}
	
	$("<img/>", {
		"src": "Unknown_83_filename"/*tpa=http://media.optimumonline.com/images/slider/gray_bottom.gif*/,
		"width": "584",
		"height": "23",
		"alt": "",
		"border": "0"
	}).appendTo("#sliderBottom");
		
}