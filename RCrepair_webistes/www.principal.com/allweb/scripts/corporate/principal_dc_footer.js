<!--

function dcsMultiTrack(){
  if (arguments.length%2==0){
    for (var i=0;i<arguments.length;i+=2){
      if (arguments[i].indexOf('WT.')==0){
      WT[arguments[i].substring(3)]=
      arguments[i+1];
      }
      else if (arguments[i].indexOf('DCS.')==0){
      DCS[arguments[i].substring(4)]=
      arguments[i+1];
      }
      else if (arguments[i].indexOf('DCSext.')==0){
      DCSext[arguments[i].substring(7)]=
      arguments[i+1];
      }
    }
    var dCurrent=new Date();
    DCS.dcsdat=dCurrent.getTime();
    dcsTag();
  }
}
// End dcsMultiTrack Function




function A(N,V){
	return "&"+N+"="+dcsEscape(V);
}

function dcsEscape(S){
	if (typeof(RE)!="undefined"){
		var retStr = new String(S);
		for (R in RE){
			retStr = retStr.replace(RE[R],R);
		}
		return retStr;
	}
	else{
		return escape(S);
	}
}

function dcsCreateImage(dcsSrc){
	if (document.images){
		gImages[gIndex]=new Image;
		gImages[gIndex].src=dcsSrc;
		gIndex++;
	}
}

function dcsMeta(){
	var myDocumentElements;
	if (document.all){
		myDocumentElements=document.all.tags("meta");
	}
	else if (document.documentElement){
		myDocumentElements=document.getElementsByTagName("meta");
	}
	if (typeof(myDocumentElements)!="undefined"){
		for (var i=1;i<=myDocumentElements.length;i++){
			myMeta=myDocumentElements.item(i-1);
			if (myMeta.name){
				if (myMeta.name.indexOf('WT.')==0){
					WT[myMeta.name.substring(3)]=myMeta.content;
				}
				else if (myMeta.name.indexOf('DCSext.')==0){
					DCSext[myMeta.name.substring(7)]=myMeta.content;
				}
				else if (myMeta.name.indexOf('DCS.')==0){
					DCS[myMeta.name.substring(4)]=myMeta.content;
				}
			}
		}
	}
}

function dcsTag(){
	var P="http"+(window.location.protocol.indexOf('https:')==0?'s':'')+"://"+gDomain+(gDcsId==""?'':'/'+gDcsId)+"/dcs.gif?";
	for (N in DCS){
		if (DCS[N]) {
			P+=A(N,DCS[N]);
		}
	}
	for (N in WT){
		if (WT[N]) {
			P+=A("WT."+N,WT[N]);
		}
	}
	for (N in DCSext){
		if (DCSext[N]) {
			P+=A(N,DCSext[N]);
		}
	}
	if (P.length>2048&&navigator.userAgent.indexOf('MSIE')>=0){
		P=P.substring(0,2040)+"&WT.tu=1";
	}
	dcsCreateImage(P);
}

(function(window) {
	if (window.performance && window.performance.timing) {
		var timing = window.performance.timing,
			perf = {},
			valid = true,
			time,
			loop = setInterval(function() {
				if ( timing.loadEventEnd ) {
					clearInterval(loop);
					
					perf.perfNegotiation = timing.requestStart - timing.navigationStart;
					perf.perfServerProcessing = timing.responseStart - timing.requestStart;
					perf.perfDownload = timing.responseEnd - timing.responseStart;
					perf.perfDocumentReady = timing.domComplete - timing.navigationStart;
					perf.perfProcessingTime = timing.loadEventStart - timing.domLoading;
					perf.perfOnLoadProcessing = timing.loadEventEnd - timing.loadEventStart;
					perf.perfTotal = timing.loadEventEnd - timing.navigationStart;
					
					for ( time in perf ) {
						if ( perf[time] < 0 || perf[time] > 999999 ) {
							valid = false;
							break;
						}
					}
					
					if ( valid ) {
						for ( time in perf ) {
							DCSext[time] = perf[time];
						}
					}

					dcsMeta();
					dcsTag();
				}
			}, 100);
	} else {
		dcsMeta();
		dcsTag();
	}
})(window);

//-->