$( document ).ready( function() {
	// OLB logon form client-side validation:
	$( '#loginFormOLB' ).submit( function() {
		var t = $( '#usernamefield' ).val();
		//console.log( 'usernamefield: "' + t + '"' );
		
		if ( t.length > 0 ) {
			this.submit();
		}
		else {
			alert( 'A valid User ID is needed to process your logon request.' );
		}
		
		return false;
	} );
	
	// Additional tweak for Wealth page logon:
	$( 'button.onlineBankingLogon' ).click( function() {
		$( '#loginFormOLB' ).submit();
	} );
	
	// Default focus to username field of OLB logon form:
	$( '#usernamefield' ).focus();
} );
