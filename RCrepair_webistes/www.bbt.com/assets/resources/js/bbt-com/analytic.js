if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
        "use strict";
        if (this == null) {
            throw new TypeError();
        }
        var t = Object(this);
        var len = t.length >>> 0;
        if (len === 0) {
            return -1;
        }
        var n = 0;
        if (arguments.length > 1) {
            n = Number(arguments[1]);
            if (n != n) { // shortcut for verifying if it's NaN
                n = 0;
            } else if (n != 0 && n != Infinity && n != -Infinity) {
                n = (n > 0 || -1) * Math.floor(Math.abs(n));
            }
        }
        if (n >= len) {
            return -1;
        }
        var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
        for (; k < len; k++) {
            if (k in t && t[k] === searchElement) {
                return k;
            }
        }
        return -1;
    }
}


var attrName = '', strUser, dropdownValue;

$('.buttonRightRail')
		.on(
				'click',
				function() {
					attrName = $(this).prev('select').attr('name');

					 if(typeof attrName != 'undefined')
					 {
					
					strUser = $('select[name=' + attrName + '] option:selected')
							.text();
							
					   if(typeof strUser != 'undefined')
					     {
					dropdownValue = $(
							'select[name=' + attrName + '] option:selected')
							.val();
					var linkType = "";
					var linkName = "";

					if (dropdownValue.indexOf("http") == 0
							|| dropdownValue.indexOf("https") == 0) {
						linkType = 'e';
					} else {
						linkType = 'o';
					}

					s.linkTrackVars = 'prop2,prop38,eVar18';
					s.linkTrackEvents = 'None';
					s.prop2 = s.pageName;
					s.prop38 = strUser;
					s.eVar18 = strUser;
					s.tl(this, linkType, $(this).text());
					}
					}

				})

function setvar(url, pagename, label, check) {

	var linkType = "";
	var linkName = "";
	var socialLinkName = "";
	if (label.indexOf(":") > -1) {
		var split = label.split(':');
		var linkName = split[0];
		var socialLinkName = split[1];
	}

	if(label.indexOf("<!-- mp_trans") > -1)
	{
	label =  label.substring(0,label.indexOf("<"));
	}
	
		if (url.indexOf("http") == 0
				|| url.indexOf("https") == 0) {
			linkType = 'e';
		} else {
			linkType = 'o';
		}
	 

	var json = [ {
		"htmlid" : "calltoaction",
		"s.linkTrackVars" : "prop2,eVar19,prop24",
		"s.linkTrackEvents" : "None",
		"s.prop2" : pagename,
		"s.eVar19" : pagename + "|" + label,
		"linktype" : linkType
	}, {
		"htmlid" : "socialengagement",
		"s.linkTrackVars" : "prop43,prop2",
		"s.linkTrackEvents" : "None",
		"s.prop43" : socialLinkName,
		"s.prop2" : pagename,
		"link" : linkType
	} ];

	var flag;
	var index = 0;
	var array = new Array();
	var arrayKey = new Array();
	var arrayValue = new Array();
	var j = 0;
	var count = 0;
	var length = json.length;
	for ( var i = 0; i < length; i++) {
		var obj = json[i]

		for ( var key in obj) {
			index++;

			if (index == 1) {
				if (obj[key] == check) {
					flag = 1;
				}
			}
			if (flag == 1) {
				var key = key;
				var value = obj[key];
				//count = Object.keys(obj).length;

				array[j] = value;

				j++;

			}
		}

		index = 0;

	}

	if (check == "calltoaction") {

		s.linkTrackVars = array[1];
		s.linkTrackEvents = array[2];
		s.prop2 = array[3];
		s.prop24 = array[4];
		s.eVar19 = array[4];
		s.tl(this, linkType, label);

	} else if (check == "socialengagement") {
		s.linkTrackVars = array[1];
		s.linkTrackEvents = array[2];
		s.prop43 = array[3];
		s.prop2 = array[4];
		s.tl(this, linkType, 'BBT:Social:' + linkName);
	}
}
$(function() {
	$('.tabs-list li').on("click", function(event){
			var name = s.pageName;
			var tabname = new Array();
			tabname =name.split(':');
			var temp='';
			 for(var count = 0;count < tabname.length -1;count++)
			 {
			 if(tabname[count] != '')
			 {
			 temp = temp+":"+tabname[count];
			 }
			 
			 }
			temp = temp.substring(1,temp.length);
        
			
				var tabs;
				tabs = $(this).text();
				tabs = tabs.split(':')[1];
				
				
				s.pageName=temp+":"+tabs;
				
if (typeof s != 'undefined'){
	var s_code=s.t();if(s_code)document.write(s_code);
}

				});
});