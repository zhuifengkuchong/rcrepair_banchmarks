d=document;f=d.documentElement;g=d.forms;j=d.all;k=d.URL;m=d.location;o=d.images;p=d.anchors;t=d.links;u=d.embeds;v=false;ba=window;bb=true;bc=parseInt;bd=location;be=bd.pathname;bj=Array;bk=Object;bl=eval;bm=navigator;bn=bm.plugins;bo=bm.userAgent;bp=undefined;bq=unescape;br=parseFloat;PINT_Global=new bk();PINT_Global.HTML=new bk();PINT_Global.HTML.anchors=d.getElementsByTagName('a');PINT_Global.HTML.label=d.getElementsByTagName('label');PINT_Global.Status=new bk();PINT_Global.Status.windowStatus="";PINT_Global.Browser=new Browser();function PINT_GetEventSource(e){if(e&&e.target){var event=e&&e.target;while(event&&event.nodeType==3)event=event.parentNode;return(event);}if(ba&&ba.event&&ba.event.srcElement)return(ba&&ba.event&&ba.event.srcElement);return v}function PINT_GetElementById(idname){var handle;if(d.getElementById){handle=d.getElementById(idname);if(handle)return handle}if(d.getElementByName){handle=d.getElementByName(idname)[0];if(handle)return handle}handle=document[idname];if(handle)return handle;if(d.all){handle=d.all[idname];if(handle)return handle}if(d.anchors){handle=d.anchors[idname];if(handle)return handle}if(d.links){handle=d.links[idname];if(handle)return handle}if(d.images){handle=d.images[idname];if(handle)return handle}if(d.embeds){handle=d.embeds[idname];if(handle)return handle}return handle}function PINT_GetIdByElement(element){if(!(element))return bp;if(element.id)return element.id;if(element.name)return element.name;return bp}function PINT_ChangePageTitle(pageTitle){if(d.title.readOnly==bb)d.title=pageTitle}function PINT_GetCurrentFileName(){var URL=bq(bd.pathname);var start=URL.lastIndexOf("/")+1;var end=(URL.indexOf("?")>0)?URL.indexOf("?"):URL.length;return(URL.substring(start,end));}function PINT_GetCurrentFilePath(){var URL=bq(bd.pathname);var start=URL.lastIndexOf("/");return(URL.substring(0,start));}function PINT_GetCurrentDirectory(){var filePath=PINT_GetCurrentFilePath();var directories=filePath.split("/");return directories.length&&directories[directories.length-1]!=""?directories[directories.length-1]:""}function PINT_IsRootDirectory(directory){return directory.toLowerCase()==PINT_GetRootDirectory().toLowerCase()?bb:v}function PINT_IsDefaultFile(){var fileName=typeof(PINT_IsDefaultFile.arguments[0])!='undefined'?PINT_IsDefaultFile.arguments[0]:PINT_GetCurrentFileName();if(fileName=="")return bb;var fileNameList=PINT_GetDefaultFile();if(bl('typeof(fileNameList)')=='object'){for(var fileNameListIndex=0;fileNameListIndex<fileNameList.length;fileNameListIndex++)if((fileName==fileNameList[fileNameListIndex]))return bb}return v}function PINT_GetDefaultFile(){if(typeof(defaultFileList)=='undefined')return"";else return defaultFileList.split(",");}function PINT_FirstFocus(){var elementref;var i=0;if(!(elementref=PINT_FirstFocus.arguments[0])){if(!(d.forms[0]))return v;while((elementref=d.forms[0].elements[i++])&&((elementref.type=='hidden')||(elementref.type=='radio')||(elementref.disabled)||(elementref.type=='submit')||(elementref.type=='submit')||(elementref.type=='checkbox'))){}}else{var formIndex;var formElementIndex;var formElementName=PINT_FirstFocus.arguments[0];elementref=null;for(formIndex=0;formIndex<d.forms.length;formIndex++){for(formElementIndex=0;formElementIndex<d.forms[formIndex].elements.length;formElementIndex++){if(d.forms[formIndex].elements[formElementIndex].name==formElementName){elementref=d.forms[formIndex].elements[formElementIndex];break}}if(elementref)break}}if(!(elementref))return v;elementref.focus();return bb}function PINT_OnMouseOverHandler(e){e=(e)?e:((ba.event)?ba.event:"");if(e){var eventsource=PINT_GetEventSource(e);if(bl('typeof(PINT_MenuTriggers)')!='undefined'){typeOfEventSource=typeof(PINT_MenuTriggers[eventsource.id]);if(typeOfEventSource!='undefined')PINT_MenuPopUp(e);}else if(bl('typeof(PINT_Global.Rollover.triggers)')!='undefined'){typeOfEventSource=typeof(PINT_Global.Rollover.triggers[eventsource.id]);if(typeOfEventSource!='undefined')PINT_RORollover(e);}PINT_SetWindowStatus();}return bb}function PINT_OnMouseOutHandler(e){e=(e)?e:((ba.event)?ba.event:"");if(e){var eventsource=PINT_GetEventSource(e);if(bl('typeof(PINT_MenuTriggers)')!='undefined'){typeOfEventSource=typeof(PINT_MenuTriggers[eventsource.id]);if(typeOfEventSource!='undefined')PINT_MenuPopDown(e);}else if(bl('typeof(PINT_Global.Rollover.triggers)')){typeOfEventSource=typeof(PINT_Global.Rollover.triggers[eventsource.id]);if(typeOfEventSource!='undefined')PINT_RORollout(e);}}return bb}function PINT_SetWindowStatus(){if(PINT_SetWindowStatus.arguments.length==0){if(typeof(PINT_Global.Status.windowStatus)!='undefined'&&PINT_Global.Status.windowStatus!=""){ba.status=PINT_Global.Status.windowStatus;PINT_Global.Status.windowStatus=""}}else ba.status=PINT_SetWindowStatus.arguments[0];return bb}function PINT_GetRootDirectory(){if(typeof(rootDirectory)=='undefined')return"";else return rootDirectory}function PINT_getElementsByClass(name){var all=d.all?d.all:d.getElementsByTagName('*');var elements=new bj();for(var e=0;e<all.length;e++){if((name!='')&&(all[e].className.indexOf(name)>=0))elements[elements.length]=all[e]}return elements}function PINT_getURLParam(name,defaultVal){var paramVal=defaultVal;var regex=new RegExp("\&"+name+"\=([^$\&]+)","i");if(d.URL.indexOf('?')!=-1){var qString='&'+d.URL.substring((d.URL.indexOf('?')+1),d.URL.length);var urlMatches=qString.match(regex);if((urlMatches!=null)&&(urlMatches.length==2))paramVal=urlMatches[1]}return paramVal}PINT_Global.OnChange=new bk;PINT_Global.OnChange.linkType=new bj();function PINT_OnChangeHandler(e){var formElement;e=(e)?e:((ba.event)?ba.event:"");if(e){var eventsource=PINT_GetEventSource(e);for(formIndex=0;formIndex<d.forms.length;formIndex++){formElement=d.forms[formIndex];for(elementIndex=0;elementIndex<formElement.elements.length;elementIndex++){if(eventsource.name==formElement.elements[elementIndex].name){if(PINT_Global.OnChange.linkType[eventsource.id]=="anchor"&&formElement.elements[elementIndex].value!="")bd=formElement.action+"#"+formElement.elements[elementIndex].value;else if(PINT_Global.OnChange.linkType[eventsource.id]=="page"&&formElement.elements[elementIndex].value!="")bd=formElement.elements[elementIndex].value}}}}return bb}function PINT_OnChangeInit(){if(PINT_OnChangeInit.arguments.length!=2)return v;if(d.getElementById){var trigger=d.getElementById(PINT_OnChangeInit.arguments[0]);if(trigger){PINT_Global.OnChange.linkType[trigger.id]=PINT_OnChangeInit.arguments[1];trigger.onchange=PINT_OnChangeHandler}}return bb}var rot13map;function rot13init(){var map=new bj();var s="abcdefghijklmnopqrstuvwxyz";for(i=0;i<s.length;i++)map[s.charAt(i)]=s.charAt((i+13)%26);for(i=0;i<s.length;i++)map[s.charAt(i).toUpperCase()]=s.charAt((i+13)%26).toUpperCase();return map}function rot13(a){if(!rot13map)rot13map=rot13init();var s="";for(i=0;i<a.length;i++){var b=a.charAt(i);s+=(b>='A'&&b<='Z'||b>='a'&&b<='z'?rot13map[b]:b);}return s}function print_e(user,domain){var e=rot13(user)+"@"+rot13(domain);var out='<a href="mailto:'+e+'">';out+=e;out+='</a>';d.write(out);}PINT_FlashObject=function(swf,id,w,h,defaultImage,ver,imageMap,c){this.swf=swf;this.id=id;this.width=w;this.height=h;this.imageMap=imageMap;this.version=ver||6;this.align="middle";this.codebase=this.version+",0,0,0";this.redirect="";this.sq=m.search.split("?")[1]||"";this.defaultImage=defaultImage;this.altTxt="Please <a href='http://www.macromedia.com/go/getflashplayer'>upgrade your Flash Player</a>.";this.bypassTxt="";this.params=new bk();this.variables=new bk();if(c)this.color=this.addParam('bgcolor',c);this.addParam('quality','high');this.doDetect=getQueryParamValue('detectflash');};PINT_FlashObject.prototype.addParam=function(name,value){this.params[name]=value};PINT_FlashObject.prototype.getParams=function(){return this.params};PINT_FlashObject.prototype.getParam=function(name){return this.params[name]};PINT_FlashObject.prototype.addVariable=function(name,value){this.variables[name]=value};PINT_FlashObject.prototype.getVariable=function(name){return this.variables[name]};PINT_FlashObject.prototype.getVariables=function(){return this.variables};PINT_FlashObject.prototype.getParamTags=function(){var paramTags="";for(var param in this.getParams()){paramTags+='<param name="'+param+'" value="'+this.getParam(param)+'" />'}if(paramTags==""){paramTags=null}return paramTags};PINT_FlashObject.prototype.getHTML=function(){var flashHTML="";if(ba.ActiveXObject&&bm.userAgent.indexOf('Mac')==-1){flashHTML+='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version='+this.codebase+'" width="'+this.width+'" height="'+this.height+'" id="'+this.id+'" align="'+this.align+'">';flashHTML+='<param name="movie" value="'+this.swf+'" />';if(this.getParamTags()!=null){flashHTML+=this.getParamTags();}if(this.getVariablePairs()!=null){flashHTML+='<param name="flashVars" value="'+this.getVariablePairs()+'" />'}flashHTML+='</object>'}else{flashHTML+='<embed type="application/x-shockwave-flash" src="'+this.swf+'" width="'+this.width+'" height="'+this.height+'" id="'+this.id+'" align="'+this.align+'"';for(var param in this.getParams()){flashHTML+=' '+param+'="'+this.getParam(param)+'"'}if(this.getVariablePairs()!=null){flashHTML+=' flashVars="'+this.getVariablePairs()+'"'}flashHTML+='></embed>'}return flashHTML};PINT_FlashObject.prototype.getVariablePairs=function(){var variablePairs=new bj();for(var name in this.getVariables()){variablePairs.push(name+"="+escape(this.getVariable(name)));}if(variablePairs.length>0){return variablePairs.join("&");}else{return null}};PINT_FlashObject.prototype.write=function(elementId){if(detectFlash(this.version)||this.doDetect=='false'){if(elementId){d.getElementById(elementId).innerHTML=this.getHTML();}else{d.write(this.getHTML());}}else{if(this.redirect!=""){m.replace(this.redirect);}else if(this.defaultImage!=""){imageString="<img src=\""+this.defaultImage+"\" width=\""+this.width+"\" height=\""+this.height+"\" border=\"0\" alt=\"\"";if(bl('typeof(this.imageMap)')!="undefined"&&this.imageMap!="")imageString+=" usemap=\"#"+this.imageMap+"\" ";imageString+=" class=\"inlineimage\" />";d.write(imageString);}else d.write(this.altTxt+""+this.bypassTxt);}};function getFlashVersion(){var flashversion=0;if(bm.plugins&&bm.plugins.length){var x=bm.plugins["Shockwave Flash"];if(x){if(x.description){var y=x.description;flashversion=y.charAt(y.indexOf('.')-1);}}}else{result=v;for(var i=15;i>=3&&result!=bb;i--){execScript('on error resume next: result = IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.'+i+'"))','VBScript');flashversion=i}}return flashversion}function detectFlash(ver){if(getFlashVersion()>=ver){return bb}else{return v}}function getQueryParamValue(param){var q=m.search;var detectIndex=q.indexOf(param);if(q.length>1&&detectIndex!=-1){return q.substring(q.indexOf("=",detectIndex)+1,q.indexOf("&",detectIndex));}else{return bb}}function addEvent(obj,evType,fn){if(obj.addEventListener){obj.addEventListener(evType,fn,bb);return bb}else if(obj.attachEvent){var r=obj.attachEvent("on"+evType,fn);return r}else{return v}}function Browser(){var ua,s,i;this.isIE=v;this.isNS=v;this.version=null;ua=bm.userAgent;s="MSIE";if((i=ua.indexOf(s))>=0){this.isIE=bb;this.version=br(ua.substr(i+s.length));return}s="Netscape6/";if((i=ua.indexOf(s))>=0){this.isNS=bb;this.version=br(ua.substr(i+s.length));return}s="Gecko";if((i=ua.indexOf(s))>=0){this.isNS=bb;this.version=6.1;return}}function PINT_BrowserDetection(){if(PINT_BrowserDetection.arguments.length!=1)return v;var browserUpgradeFile=PINT_BrowserDetection.arguments[0];var currentFileName=PINT_GetCurrentFileName();if(!d.getElementById&&(browserUpgradeFile.indexOf(currentFileName)=="-1"||currentFileName==""))bd.replace(PINT_GetRootDirectory()+'/'+browserUpgradeFile);}function popupWindow(){if(popupWindow.arguments.length<1)return v;var popupWin=null;popupWin=ba.open(popupWindow.arguments[0],popupWindow.arguments[1],popupWindow.arguments[2]);}function PINT_GetWindowSize(style){var size=400;if(style=="width"){if(typeof(ba.innerWidth)=='number')size=ba.innerWidth;else if(d.documentElement&&(d.documentElement.clientWidth||d.documentElement.clientHeight))size=d.documentElement.clientWidth;else if(d.body&&d.body.clientWidth)size=d.body.clientWidth}else if(style=="height"){if(typeof(ba.innerWidth)=='number')size=ba.innerHeight;else if(d.documentElement&&(d.documentElement.clientWidth||d.documentElement.clientHeight))size=d.documentElement.clientHeight;else if(d.body&&d.body.clientHeight)size=d.body.clientHeight}return size}function PINT_AnchorPopupWindows(){if(!d.getElementsByTagName)return;var anchors=PINT_Global.HTML.anchors;var currentAnchor;for(var anchorIndex=0;anchorIndex<anchors.length;anchorIndex++){var targetLink,relArray,relInformation,windowAttributes,javascriptTargetLink,windowName;var bd,menubar,resizable,scrollbars,status,toolbar;var width,height,windowType,windowName;currentAnchor=anchors[anchorIndex];targetLink=currentAnchor.getAttribute("href");relInformation=currentAnchor.getAttribute("rel");if(relInformation&&targetLink){relArray=relInformation.split("|");if(relArray[0]=="popup"&&relArray.length>=4){if(relArray[1]!="null")width=bc(relArray[1])?bc(relArray[1]):400;else width=PINT_GetWindowSize("width");if(relArray[2]!="null")height=bc(relArray[2])?bc(relArray[2]):400;else height=PINT_GetWindowSize("height");windowType=relArray[3];windowAttributes="width="+width+",height="+height;if(windowType=="custom"){if(relArray.length<10)return v;bd=bc(relArray[4])?bc(relArray[4]):0;menubar=bc(relArray[5])?bc(relArray[5]):0;resizable=bc(relArray[6])?bc(relArray[6]):0;scrollbars=bc(relArray[7])?bc(relArray[7]):0;status=bc(relArray[8])?bc(relArray[8]):0;toolbar=bc(relArray[9])?bc(relArray[9]):0;if(relArray.length==11)windowName=relArray[10];else windowName="popupWindow";windowAttributes+=",location="+bd+",menubar="+menubar+",resizable="+resizable+",scrollbars="+scrollbars+",status="+status+",toolbar="+toolbar}else{if(relArray.length==5)windowName=relArray[4];else windowName="popupWindow";if(windowType=="standard")windowAttributes+=",location=0,menubar=0,resizable=0,scrollbars=0,status=0,toolbar=0";else if(windowType=="resize")windowAttributes+=",location=0,menubar=0,resizable=1,scrollbars=0,status=0,toolbar=0";else if(windowType=="scrollbar")windowAttributes+=",location=0,menubar=0,resizable=0,scrollbars=1,status=0,toolbar=0";else if(windowType=="blank")windowAttributes="";else return v}javascriptTargetLink="javascript:popupWindow('"+targetLink+"','"+windowName+"','"+windowAttributes+"');";currentAnchor.setAttribute("href",javascriptTargetLink);}}}}

function PINT_global_locations () {

	if (document.getElementById('global_locations') && document.getElementById('global_locations_form') ) {

		var form = document.getElementById('global_locations_form');
		var box = document.getElementById('global_locations');

		form.onsubmit = function () {

			var destination = box.options[box.selectedIndex].value;
			var newDestination = destination.toLowerCase();
			if (destination) location.href = newDestination + '.aspx';
			return false;

		}
	}

	if (document.getElementById('global_locations_home') && document.getElementById('global_locations_form') ) {

		var form = document.getElementById('global_locations_form');
		var box = document.getElementById('global_locations_home');

		form.onsubmit = function () {

			var destination = box.options[box.selectedIndex].value;
			var newDestination = destination.toLowerCase();
			if (destination) location.href = 'http://www.allergan.com/assets/scripts/global/about/global/' + newDestination + '.aspx';
			return false;

		}
	}

}

function PINT_site_search() {
    if (document.forms[0] && document.getElementById('btn_search') && document.getElementById('query') ) {
        var button = document.getElementById('btn_search');
        var queryField = document.getElementById('query');
        button.onclick = function(){
            location.href = 'http://www.allergan.com/search/search_results.htm?query=' + escape(queryField.value);
            //document.forms[0].submit();
            return false;
        }
        
        queryField.onkeydown = function(event){
        var keyCode = event ? event.which : window.event.keyCode;
            if (keyCode==13)
            {
                location.href = 'http://www.allergan.com/search/search_results.htm?query=' + escape(queryField.value);
                //document.forms[0].submit();
                return false;
            } 
            else return true;
        }
        
    }
}

function PINT_product_search()
{ 
    if(!(document.getElementById('product_search_areas') && document.getElementById('product_search_products')
        && document.getElementById('product_search_form')))
        return;
    var areaNode = document.getElementById('product_search_areas');
    var productsNode = document.getElementById('product_search_products');
    
    var populateProducts = function()
    {
        var area = areaNode.options[areaNode.selectedIndex].text;
        if(areaNode.options[areaNode.selectedIndex].value=="")
        {
            populateAllProducts();
            return;
        }       
        productsNode.options.length = 0;
        var defaultOption = document.createElement('option');
        defaultOption.text = searchMap.ProductsTitle;
        defaultOption.value = "";
        addOption(productsNode, defaultOption);
        for(index in searchMap.AreaProductsMap)
        {
            if(searchMap.AreaProductsMap[index]['Area']!=area)
                continue;
            var option = document.createElement('option');
            option.text =  searchMap.AreaProductsMap[index]['Product'];
            option.value =  searchMap.Products[option.text];
            addOption(productsNode, option);
        }
    };
    var populateAllProducts = function()
    {
        productsNode.options.length = 0;
        var defaultOption = document.createElement('option');
        defaultOption.text = searchMap.ProductsTitle;
        defaultOption.value = "";
        addOption(productsNode, defaultOption);
        for(index in searchMap.Products)
        {
            var option = document.createElement('option');
            option.text =  index;
            option.value = searchMap.Products[index];
            addOption(productsNode, option);
        }
    };
    var populateAllAreas = function()
    {    
        areaNode.options.length = 0;
        var defaultOption = document.createElement('option');
        defaultOption.text = searchMap.AreaTitle;
        defaultOption.value = "";
        addOption(areaNode, defaultOption);
        for(index in searchMap.Areas)
        {
            var option = document.createElement('option');
            option.text =  index;
            option.value =  searchMap.Areas[index];
            addOption(areaNode, option);
        }
    };
    
    var redirectToPage = function()
    {
        var productURL = productsNode.options[productsNode.selectedIndex].value;
        if(productURL!=null && productURL!="")
        {
            location.href = productURL;
        }
        else
        {
            var areaURL = areaNode.options[areaNode.selectedIndex].value;
            if(areaURL!=null && areaURL!="")
            {
                 location.href = areaURL;
            }
        }
        return false;
    }
    var callback = 
    {
        success: function(response)
        {
            searchMap= eval('('+ response.responseText +')');
            areaNode.blur();
            populateAllProducts();
            populateAllAreas();
            areaNode.onchange = populateProducts;
            document.getElementById('product_search_form').onsubmit = redirectToPage;
        },
        timeout:5000
    };
    YAHOO.util.Connect.asyncRequest('GET', 'http://www.allergan.com/product_list.aspx', callback, null);
}

function addOption(selectElement, option)
{
    try
    {
        selectElement.add(option,null);
    }catch(ex)
    {
        selectElement.add(option);
    }
}


function addPipelineHandlers(op_container) {
    if( YAHOO.util.Dom.getElementsByClassName('op_link', 'a', op_container )[0] && YAHOO.util.Dom.getElementsByClassName('op_text', 'p', op_container )[0] ) {
	    var op_link			= YAHOO.util.Dom.getElementsByClassName('op_link', 'a', op_container )[0]; 
	    var op_text			= YAHOO.util.Dom.getElementsByClassName('op_text', 'p', op_container )[0]; 
		
	    YAHOO.util.Dom.setStyle(op_text, 'display', 'block');
	    var region = YAHOO.util.Dom.getRegion(op_link);
	    var minHeight = (region.bottom - region.top) + 5;
	    //Set the height of the container so that only op_link is visible
	    YAHOO.util.Dom.setStyle(op_container, 'height', minHeight + 'px');
	    
	    region = YAHOO.util.Dom.getRegion(op_text);
	    var maxHeight = minHeight + (region.bottom - region.top);
	    
	    var op_open			= new YAHOO.util.Anim(op_container, { height: { to: maxHeight } }, .4, YAHOO.util.Easing.easeBoth);
	    var op_close		= new YAHOO.util.Anim(op_container, { height: { to: minHeight } }, .4, YAHOO.util.Easing.easeBoth);


	    function reveal (e) {
		    // Corrective for IE event object
		    var event = e || window.event;
		    var target = event.target || event.srcElement;
		    if ( target.id=='op_link') {
			    op_open.animate();
			    op_link.className = 'down';

		    }
	    }

	    function hide (e) {

		    // Correctives for IE objects
		    var event = e || window.event;
		    var relatedNode = event.relatedTarget || event.toElement;

		    if (relatedNode.id !== 'op_link' && 
			    relatedNode.id !== 'op_text' && 
			    relatedNode.id !== 'op_container'){

			    op_close.animate();
			    op_link.className = '';

		    }
	    }

	    op_link.onmouseover = hide;
	    op_container.onmouseout = hide;
    }
} 
function PINT_pipeline () {

	var infoLinks = document.getElementsByTagName('a');

	for (var i=0; i<infoLinks.length; i++) {
	
		if (infoLinks[i].className != 'informative') continue;

		infoLinks[i].onmouseover = function () {

			//YAHOO.util.Dom.setStyle(this.parentNode.childNodes[3],'display','block');
			YAHOO.util.Dom.setStyle(this.parentNode.getElementsByTagName('div')[0],'display','block');

		}

		infoLinks[i].onmouseout = function () {

			//YAHOO.util.Dom.setStyle(this.parentNode.childNodes[3],'display','none');
			YAHOO.util.Dom.setStyle(this.parentNode.getElementsByTagName('div')[0],'display','none');

		}

	}

   
    var containers = YAHOO.util.Dom.getElementsByClassName('bg_btm');
    for(i=0; i<containers.length; i++)
    {
	    addPipelineHandlers(containers[i]);
	}

}

function PINT_overlay () {

	overlayState = false;
	linkup();
	backdrop();

	function linkup () {

		var overlinks = document.getElementsByTagName('a');

		for (var i=0; i<overlinks.length; i++){

			if ( overlinks[i].target === 'overlay' ) {

				if ( overlinks[i].hash && overlinks[i].hash !== '#' ) {

					 overlinks[i].onclick = internal;

				}

				else {

					 overlinks[i].onclick = external;

				}

			} else if ( overlinks[i].className === 'close' ) {

				 overlinks[i].onclick = restore;

			}

		}

	}

	function backdrop () {

		if (!document.getElementById('backdrop')) {

			var backdrop = document.createElement('div');
				backdrop.id = 'backdrop';
				backdrop.style.background = '#000';
				backdrop.style.height = documentHeight() + 'px';
				backdrop.style.width = documentWidth() + 'px';
				backdrop.style.position = 'absolute';
				backdrop.style.top = '0px';
				backdrop.style.left = '0px';
				backdrop.style.zIndex = '98';
				hide ( backdrop );

			document.body.appendChild ( backdrop );

		}

	}

	function overlay ( element ) {

		var pocket = document.getElementById ('pocket');

		var frame = document.createElement ('div');
			frame.id = 'frame';
			frame.style.width = documentWidth() + 'px';
			frame.style.position = 'absolute';
			frame.style.top = yScroll() + 'px';
			frame.style.left = '0px';
			frame.style.textAlign = 'center';
			frame.style.zIndex = '99';
			hide ( frame );

		document.body.appendChild ( frame );

		var frame = document.getElementById ('frame');
			appendNode ( element, frame );
			element.style.display = 'block';
			removeNode ( pocket );

		var appear = new YAHOO.util.Anim (frame, { opacity: { to: 1 } }, .75, YAHOO.util.Easing.easeOut);
		
			appear.onStart.subscribe( function() { frame.style.display = 'block'; frame.style.opacity=100;});
			
			
			appear.onComplete.subscribe ( function() {
				overlayState = true;
			});
		var darken = new YAHOO.util.Anim ('backdrop', { opacity: { to: .8 } }, .75, YAHOO.util.Easing.easeOut);
			darken.onStart.subscribe ( function() {
				document.getElementById('backdrop').style.display = 'block';
			});
			darken.onComplete.subscribe ( function() { 
				appear.animate ();
			});

		darken.animate ();

	}

	function swap (element) {

		var pocket = document.getElementById ('pocket');
		var frame = document.getElementById ('frame');
		var onStage = frame.firstChild;

		var strike = new YAHOO.util.Anim (onStage, { opacity: { to: 0 } }, .33, YAHOO.util.Easing.easeOut);
			strike.onStart.subscribe ( function() {
				hide ( element );
				appendNode ( element, frame );
				removeNode ( pocket );
			});
			strike.onComplete.subscribe ( function() {
				onStage.style.display = 'none';
				appear.animate();
			});

		var appear = new YAHOO.util.Anim (element, { opacity: { to: 1 } }, .33, YAHOO.util.Easing.easeOut);
			appear.onStart.subscribe ( function() {
				element.style.display = 'block';
				element.style.opacity = 100;
				removeNode ( onStage );
			});

		strike.animate ();

	}

	function restore () {

		var frame = document.getElementById ('frame');
		var onStage = frame.firstChild;

		var disappear = new YAHOO.util.Anim ( frame, { opacity: { to: 0 } }, .5, YAHOO.util.Easing.easeOut );
			disappear.onComplete.subscribe ( function() { 
				hide ( onStage );
				removeNode ( frame );
				lighten.animate ();
			});

		var lighten = new YAHOO.util.Anim ('backdrop', { opacity: { to: 0 } }, .5, YAHOO.util.Easing.easeOut );
			lighten.onComplete.subscribe ( function() {
				hide( document.getElementById ('backdrop') );
				overlayState = false;
			});

		disappear.animate ();

		return false;

	}

	window.onresize = function () {

		YAHOO.util.Dom.setStyle ( 'backdrop', 'width', documentWidth() + 'px' );
		YAHOO.util.Dom.setStyle ( 'backdrop', 'height', documentHeight() + 'px' );

	}

	function inline_image () {

//		alert('image'); 

		return false;

	}

	function internal () {

//		alert('internal'); 

//		var element = document.getElementById(this.hash.substring(1));

//		overlay( element );

		return false;	

	}

	function external () {

		link = this.href;

		var xhr = createXHR ();

		if ( xhr ) {

			xhr.open ( "GET", this.href, true );

			xhr.onreadystatechange = function () { handleResponse ( xhr ); };

			xhr.send ( null );

		}

		else return true;	// if no XHR Object, follow link

		return false;

	}

	function createXHR () {

		try { return new XMLHttpRequest (); } catch ( e ) {}
		try { return new ActiveXObject("Msxml2.XMLHTTP.6.0");	} catch ( e ) {}
		try { return new ActiveXObject("Msxml2.XMLHTTP.3.0");	} catch ( e ) {}
		try { return new ActiveXObject("Msxml2.XMLHTTP");		} catch ( e ) {}
		try { return new ActiveXObject("Microsoft.XMLHTTP");	} catch ( e ) {}

		return null;

	}

	function handleResponse ( xhr ) {

		if ( xhr.readyState == 4 && xhr.status == 200 ) {

			var response = xhr.responseText;

			var pocket = document.createElement('div');
				pocket.id = 'pocket';
				pocket.style.display = 'none';

			pocket.innerHTML = response;
			document.body.appendChild ( pocket );

			linkup ();

			//var page = document.getElementById ( link.substring( link.lastIndexOf('/')+1, link.lastIndexOf('.') ) );
			
			var page = document.getElementById('overlay_contents');
            page.id = 'overlay_contents_onstage';
			if ( !overlayState ) {

				overlay ( page ); 

			} else {

				swap ( page ); 

			}

		}

	}

	function appendNode ( element, surrogate ) {
		if ( surrogate ) {
			surrogate.appendChild( element );
		} else {
			document.body.appendChild( element );
		}
	}

	function removeNode ( element ) {
		element.parentNode.removeChild( element );	
	}

	function hide ( element ) {
		element.style.display = 'none';
		element.style.filter = 'alpha(opacity=0)';
		element.style.opacity = '0';	
	}

	function documentWidth () {
		if ( document.documentElement && document.documentElement.scrollWidth ) {
			return document.documentElement.scrollWidth;
		} else if ( document.body.scrollWidth ) {
			return document.body.scrollWidth;
		} else {
			throw new Error('cannot get document width');
		}
	}

	function documentHeight () {
		if ( document.documentElement && document.documentElement.scrollHeight ) {
			return document.documentElement.scrollHeight;
		} else if ( document.body.scrollHeight ) {
			return document.body.scrollHeight;
		} else {
			throw new Error('cannot get document height');
		}
	}

	function xScroll () {
		var xScroll = window.scrollX || document.documentElement.scrollLeft;
		return xScroll;
	}	

	function yScroll () {
		var yScroll = window.scrollY || document.documentElement.scrollTop;
		return yScroll;
	}
}

function ShowShim(menu)
{  
   if (navigator.appName != "Microsoft Internet Explorer" || navigator.appVersion >= 7)
   {
      return;	
   }
   
   if(menu == null)
   {
       return;	 
   }
      
   var left = 0;   
   var obj = menu;
   while(obj.offsetParent)
   {
      left += obj.offsetLeft;   
      obj = obj.offsetParent;
   }

  if (menu.className == "nav_7_on") {
      var height = 344;
  }
  
  else if (menu.className == "nav_8") {
//       left -= 117;
       var height = 175;
   }

   else if (menu.className == "nav_6") {
       var height = 257;
   }

   else if (menu.className == "nav_5") {
       var height = 100;
   }

   else if (menu.className == "nav_4") {
       var height = 112;
   }

   else if (menu.className == "nav_3") {
       var height = 199;
   }

   else if (menu.className == "nav_2") {
       var height = 432;
   }

   else if (menu.className == "nav_1") {
       var height = 75;
   }
            
   var shim = document.getElementById('shim');      
   var X = YAHOO.util.Dom.getX(menu);
   var Y = YAHOO.util.Dom.getY(menu);

//   var height = 350;
 
   if(shim == null)
   {
     return;	   	
   }   

   shim.style.zindex=100;   
   shim.style.display = 'block';
   shim.style.position = 'absolute';
   
   // shim.style.top=menu.currentStyle['top'];
   // shim.style.left=left;
   shim.style.width =230;
   shim.style.height = height;
   shim.setAttribute("src","");
   shim.setAttribute("frameBorder","0");
   shim.setAttribute("scrolling","no");    
   
   YAHOO.util.Dom.setX(shim,X);
   YAHOO.util.Dom.setY(shim,Y);       
   
   // console.log("DivName: " + divname + " Y: " + Y + " Y2: " + Y2 + " Height: " + height);
}

function HideShim()
{
  var shim = document.getElementById('shim');
  
  if(shim == null)
  {
     return;
  }
  
  shim.style.display = 'none';   
}	

function PromptAlert(link)
{	
	return true;
}

function HideAlert(link)
{		
	
	var div = document.getElementById('alertDiv');	
	var body = document.getElementsByTagName('body')[0];	
	body.removeChild(div);
	
	return true;
}

function ShowAlert(link)
{
	var div = document.createElement('div');
	
   var left = 0;   
   var top = 0;
   var obj = link;
   while(obj.offsetParent)
   {
      left += obj.offsetLeft;   
      top += obj.offsetTop;
      obj = obj.offsetParent;
   }	
	
	
	div.id = 'alertDiv';
	div.style.left = left + link.offsetWidth + "px";
	div.style.top = top + "px";
	div.style.display='block';	
	div.style.position='absolute';
	div.innerHTML = "This link is for reference only. Allergan is not responsible for the content on third party Web sites and does not ensure the validity of the information within the Web sites. Allergan's Privacy Statement and Terms of Use do not apply to such third party websites.";	
	div.className = 'alertClass';
	
	var body = document.getElementsByTagName('body')[0];
	
	body.appendChild(div);

	
	return true;
}

function ContactBack()
{
	history.go(-1);
	return false;	
}