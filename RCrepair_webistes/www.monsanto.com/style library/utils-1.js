
/* DETECT OLD BROWSERS (UDA defined)
--------------------------------------------------------------------*/
function oldBrowser() {
	var oldIE = $.browser.msie && parseInt($.browser.version) <= 6, // IE6 and older
		oldNN = navigator.userAgent.toLowerCase().indexOf("netscape") != -1, // all netscapes 
		oldFF = $.browser.mozilla && $.browser.version.substr(0,3) <= 1.7, // FF1 and older
		oldOP = $.browser.opera && parseInt($.browser.version) <= 7, // Opera 7 and older
		oldSF = $.browser.safari && parseInt($.browser.version) <= 418; // Safari 2 and older (418.8 = webkit version for 2)
	if (oldIE || oldNN || oldFF || oldOP || oldSF) return true;
	return false;
}


/* LAUNCH A COLORBOX FROM GSHOWPRO FLASH (only flash uses this function)
-------------------------------------------------------------------------*/
function launchColorBoxFromFlash(theURL, theWidth, theHeight) {
	$.fn.colorbox({href:theURL, innerWidth:theWidth, innerHeight:theHeight, iframe:true});
}


/* GET A QUERYSTRING VALUE
-------------------------------------------------------------------------*/
function GetQueryStringParam(sParam) {  
	var sPageURL = window.location.search.substring(1);  
	var sURLVariables = sPageURL.split('&');  
	for (var i = 0; i < sURLVariables.length; i++) {  
		var sParameterName = sURLVariables[i].split('=');  
		if (sParameterName[0] == sParam) {  
			return sParameterName[1];  
		}  
	}  
} 


$(document).ready(function() { // When DOM is loaded run these
	
	/* MODAL POP-UP: BROWSER UPGRADE NOTICE
	--------------------------------------------------------------------*/
	if (oldBrowser() && $.cookie("browserNotice") != "displayed") {
		$.fn.colorbox({
			href:"http://www.monsanto.com/popups/Pages/browser-upgrade.aspx", innerWidth:620, innerHeight:465, iframe:true,
			onOpen:function(){ 
				// create session cookie
				$.cookie("browserNotice", "displayed", {path:"/"});
			}
		});
	}

	
	/* NOMOBILE: Adds a cookie to the page if the user clicked the "Full Site" link on the mobile site
	--------------------------------------------------------------------*/
	if (GetQueryStringParam("p") == "t" && $.cookie("nomobile") != "true") {
		$.cookie("nomobile", "true", {path:"/"});
	}
	
	
	/* MODAL POP-UP: SELECT A COUNTRY (content href defined in html)
	--------------------------------------------------------------------*/
	$("#top-nav-mn li.select-country-mn a").colorbox({innerWidth:820, innerHeight:520, iframe:true});
	

	/* MODAL POP-UPS: FARM MOM (content href defined in html)
	--------------------------------------------------------------------*/
	$("a.farmmom-nom-popup").colorbox({innerWidth:534, innerHeight:520, iframe:true});	
	$("a.farmmom-vote-popup").colorbox({innerWidth:718, innerHeight:520, iframe:true});	
	
	
	/* MODAL POP-UP: YOUTUBE VIDEOS (content href defined in html)
	--------------------------------------------------------------------*/
	$(".youtube-popup").colorbox({iframe:true, innerWidth:640, innerHeight:390, overlayClose:false});

	
	/* MODAL POP-UP: BRIGHTCOVE VIDEOS (content href defined in html)
	--------------------------------------------------------------------*/
	// Standard Agreed Video Size - 650 x 360 (extra 5px added to innerHeight for colorbox reasons)
	$(".brightcove-popup").colorbox({iframe:true, innerWidth:650, innerHeight:365, overlayClose:false});
	
	// Smaller Video Size - 480 x 270 (extra 5px added to innerHeight for colorbox reasons)
	$(".brightcove-popup-small").colorbox({iframe:true, innerWidth:480, innerHeight:275, overlayClose:false});
	
	
	/* MODAL POP-UP: MONSANTO HISTORY FLASH (swf href defined in html)
	--------------------------------------------------------------------*/
	$("a.modal-monhistory").colorbox({inline:true, href:"#flashcontent",
		onLoad:function(){ 
			$("#flashcontent").show();
			// Executing this here so every time the colorbox loads, the swf is re-initialized. Not necessary for Safari/FireFox, but it is for IE7/IE8. 
			var so = new SWFObject(this.href, "Player", "900", "555", "8", "#FFFFFF");
			so.addVariable("theme_str", this.href.split("=")[1]);
			so.addParam("wmode", "transparent");
			so.write("flashcontent");
		},
		onCleanup:function(){ $("#flashcontent").hide(); }
	});
	
	
	/* TABBED CONTENT BOXES
	--------------------------------------------------------------------*/
	if ($("div.tab-content-mn").length) { // only execute below code on pages that have tabbed content
		// cache objects
		var $tab_content_mn = $("div.tab-content-mn"),
		    $box_tabs_mn_li = $("ul.box-tabs-mn li");
		
		// hide all content, activate first tab
		$tab_content_mn.hide();
		$box_tabs_mn_li.eq(0).addClass("active");
		$tab_content_mn.eq(0).show();
		
		// onclick event: reset tabs/hide all content, fade in currently selected tab
		$box_tabs_mn_li.click(function() {
			$box_tabs_mn_li.removeClass("active");
			$tab_content_mn.hide();
			$(this).addClass("active");
			var activeTab = $(this).find("a").attr("href");
			$(activeTab).show();
			return false;
		});
	}

	
	/* TABLE SORT
	--------------------------------------------------------------------*/
	if ($("table.tablesort").length) $("table.tablesort").tablesorter( {sortList: [[0,0], [0,0]] });
	
	
	/* REPLACE DEFAULT WEB PART ERROR MESSAGE (K.Sauer requested)
	--------------------------------------------------------------------*/
	$("div.ms-vb:contains('Unable to display this Web Part.')").text("Feed unavailable at this time. Please check back soon.");	
	
}); // End DOM ready check