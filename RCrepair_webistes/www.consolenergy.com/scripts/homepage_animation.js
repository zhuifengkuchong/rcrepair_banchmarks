// the HTML element to write the canvas into
var canvasId = "homepageAnimation";
var paper = "";
var images = "";
var text = "";

var isLoaded = false;

// the dimensions of the canvas
//var canvasDimensions = new Rectangle(0, 0, 960, 409);

/* Images/Text variables */
var text01 = "";
var text02 = "";
var text03 = "";

var image01 = "";
var image02 = "";
var image03 = "";

var imageOut01 = "";
var imageOut02 = "";
var imageOut03 = "";
var imageOut04 = "";
var imageOut05 = "";

var coast01 = "";
var coast02 = "";
var coast03 = "";

var logo = "";

/* Animation Scheme Variables */
var text01_ani = "";
var text02_ani = "";
var text03_ani = "";

var image01_ani = "";
var image02_ani = "";
var image03_ani = "";

var imageOut01_ani = "";
var imageOut02_ani = "";
var imageOut03_ani = "";
var imageOut04_ani = "";
var imageOut05_ani = "";

var coast01_ani = "";
var coast02_ani = "";
var coast03_ani = "";

var logo_ani = "";

// Images sizes/coordinates

/* Run after Page finishes loading */
$(window).bind("load", function () {
    //paper = Raphael(canvasId, 960, 409);

    $("#" + canvasId).append('<div id="homeText" style="position: absolute; left: 0; top: 0; width: 100%; z-index:2;"></div>');
    $("#" + canvasId).append('<div id="homeImages" style="position: absolute; left: 0; top: 0; width: 100%; z-index:1;"></div>');

    images = Raphael("homeImages", 960, 409);
    text = Raphael("homeText", 960, 409);

    createText();
    createImages();

    if (isLoaded) {
        loaded();
    }
});

function createText() {
    text01 = text.image("../images/home-page/animation/txt01_withoutUs.png"/*tpa=http://www.consolenergy.com/images/home-page/animation/txt01_withoutUs.png*/, 66, 57, 163, 27).attr({
        opacity: 0.0
    });
    text02 = text.image("../images/home-page/animation/txt02_withCoal.png"/*tpa=http://www.consolenergy.com/images/home-page/animation/txt02_withCoal.png*/, 66, 57, 142, 27).attr({
        opacity: 0.0
    });
    text03 = text.image("../images/home-page/animation/txt03_withCoalAndGas.png"/*tpa=http://www.consolenergy.com/images/home-page/animation/txt03_withCoalAndGas.png*/, 66, 57, 379, 27).attr({
        opacity: 0.0
    });
}

function createImages() {
    image01 = images.image("../images/home-page/animation/image01.jpg"/*tpa=http://www.consolenergy.com/images/home-page/animation/image01.jpg*/, 0, 0, 960, 409).attr({
        opacity: 0.0
    });
    image02 = images.image("../images/home-page/animation/image02.jpg"/*tpa=http://www.consolenergy.com/images/home-page/animation/image02.jpg*/, 0, 0, 960, 409).attr({
        opacity: 0.0
    });
    image03 = images.image("../images/home-page/animation/image03.jpg"/*tpa=http://www.consolenergy.com/images/home-page/animation/image03.jpg*/, 0, 0, 960, 409).attr({
        opacity: 0.0
    });

    imageOut01 = images.image("../images/home-page/animation/imageOut01.jpg"/*tpa=http://www.consolenergy.com/images/home-page/animation/imageOut01.jpg*/, 0, 0, 960, 409).attr({
        opacity: 0.0
    });
    imageOut02 = images.image("../images/home-page/animation/imageOut02.jpg"/*tpa=http://www.consolenergy.com/images/home-page/animation/imageOut02.jpg*/, 0, 0, 960, 409).attr({
        opacity: 0.0
    });
    imageOut03 = images.image("../images/home-page/animation/imageOut03.jpg"/*tpa=http://www.consolenergy.com/images/home-page/animation/imageOut03.jpg*/, 0, 0, 960, 409).attr({
        opacity: 0.0
    });
    imageOut04 = images.image("../images/home-page/animation/imageOut04.jpg"/*tpa=http://www.consolenergy.com/images/home-page/animation/imageOut04.jpg*/, 0, 0, 960, 409).attr({
        opacity: 0.0
    });
    imageOut05 = images.image("../images/home-page/animation/imageOut05.jpg"/*tpa=http://www.consolenergy.com/images/home-page/animation/imageOut05.jpg*/, 0, 0, 960, 409).attr({
        opacity: 0.0
    });

    coast01 = images.image("../images/home-page/animation/coast01.jpg"/*tpa=http://www.consolenergy.com/images/home-page/animation/coast01.jpg*/, 0, 0, 960, 409).attr({
        opacity: 0.0
    });
    coast02 = images.image("../images/home-page/animation/coast02.jpg"/*tpa=http://www.consolenergy.com/images/home-page/animation/coast02.jpg*/, 0, 0, 960, 409).attr({
        opacity: 0.0
    });
    coast03 = images.image("../images/home-page/animation/coast03.jpg"/*tpa=http://www.consolenergy.com/images/home-page/animation/coast03.jpg*/, 0, 0, 960, 409).attr({
        opacity: 0.0
    });

    logo = images.image("../images/home-page/animation/logo.jpg"/*tpa=http://www.consolenergy.com/images/home-page/animation/logo.jpg*/, 0, 0, 960, 409).attr({
        opacity: 0.0
    });

    isLoaded = true;
}

function loaded() {
    $("#fauxLoader").fadeOut(500, function () {
        $("#fauxLoader").css("top", -409 + "px");
        startAnimation();
        //transitionOut();
    });
}

function startAnimation() {
    image01_ani = Raphael.animation({
        opacity:1
    }, 250);

    text01_ani = Raphael.animation({
        opacity: 1
    }, 250, sceneTwo);

    image01.animate(image01_ani);
    text01.animate(text01_ani);
}

function sceneTwo() {
    image02_ani = Raphael.animation({
        opacity:1
    }, 10);

    text01_ani = Raphael.animation({
        opacity:0
    }, 10);

    image01_ani = Raphael.animation({
        opacity: 0
    }, 10);

    text02_ani = Raphael.animation({
        opacity: 1
    }, 10, sceneThree);

    image01.animate(image01_ani.delay(1100));
    text01.animate(text01_ani.delay(1000));
    image02.animate(image02_ani.delay(1000));
    text02.animate(text02_ani.delay(1000));
}

function sceneThree() {
    image03_ani = Raphael.animation({
        opacity: 1
    }, 10);

    text02_ani = Raphael.animation({
        opacity: 0
    }, 10);

    image02_ani = Raphael.animation({
        opacity: 0
    }, 10);

    text03_ani = Raphael.animation({
        opacity: 1
    }, 10, transitionOut);

    image02.animate(image02_ani.delay(1350));
    text02.animate(text02_ani.delay(1250));
    image03.animate(image03_ani.delay(1250));
    text03.animate(text03_ani.delay(1250));
}

function transitionOut() {
    text03_ani = Raphael.animation({
        opacity: 0
    }, 10);

    imageOut01_ani = Raphael.animation({
        opacity: 1
    }, 100);

    imageOut02_ani = Raphael.animation({
        opacity: 1
    }, 100);

    imageOut03_ani = Raphael.animation({
        opacity: 1
    }, 100);

    imageOut04_ani = Raphael.animation({
        opacity: 1
    }, 100);

    imageOut05_ani = Raphael.animation({
        opacity: 1
    }, 100, coastAnimation);

    //logo_ani = Raphael.animation({
    //    opacity:1
    //}, 75, fadeOutLogo);

    //text03.animate(text03_ani.delay(1500));

    text03.animate(text03_ani.delay(1500));
    imageOut01.animate(imageOut01_ani.delay(1600));
    imageOut02.animate(imageOut02_ani.delay(1675));
    imageOut03.animate(imageOut03_ani.delay(1750));
    imageOut04.animate(imageOut04_ani.delay(1825));
    imageOut05.animate(imageOut05_ani.delay(1900));
    //logo.animate(logo_ani.delay(1950));
}

function coastAnimation() {
    image03_ani = Raphael.animation({
        opacity: 0
    }, 10);
    
    imageOut01_ani = Raphael.animation({
        opacity: 0
    }, 10);

    imageOut02_ani = Raphael.animation({
        opacity: 0
    }, 10);

    imageOut03_ani = Raphael.animation({
        opacity: 0
    }, 10);

    imageOut04_ani = Raphael.animation({
        opacity: 0
    }, 10);

    imageOut05_ani = Raphael.animation({
        opacity: 0
    }, 10);
    
    coast01_ani = Raphael.animation({
        opacity: 1
    }, 100);

    coast02_ani = Raphael.animation({
        opacity: 1
    }, 100);

    coast03_ani = Raphael.animation({
        opacity: 1
    }, 100, homepageOut);

    coast01.animate(coast01_ani);
    image03.animate(image03_ani.delay(110));
    imageOut01.animate(imageOut01_ani.delay(110));
    imageOut02.animate(imageOut02_ani.delay(110));
    imageOut03.animate(imageOut03_ani.delay(110));
    imageOut04.animate(imageOut04_ani.delay(110));
    imageOut05.animate(imageOut05_ani.delay(110));
    coast02.animate(coast02_ani.delay(100));
    coast03.animate(coast03_ani.delay(200));
}

function homepageOut() {
    coast01_ani = Raphael.animation({
        opacity: 0
    }, 10);

    coast02_ani = Raphael.animation({
        opacity: 0
    }, 10);

    coast03_ani = Raphael.animation({
        opacity: 0
    }, 500, bringInFeature);

    coast01.animate(coast01_ani);
    coast02.animate(coast02_ani);
    coast03.animate(coast03_ani.delay(2500));
}

function fadeOutLogo() {
    image03_ani = Raphael.animation({
        opacity: 0
    }, 10);

    imageOut01_ani = Raphael.animation({
        opacity: 0
    }, 10);

    imageOut02_ani = Raphael.animation({
        opacity: 0
    }, 10);

    imageOut03_ani = Raphael.animation({
        opacity: 0
    }, 10);

    imageOut04_ani = Raphael.animation({
        opacity: 0
    }, 10);

    imageOut05_ani = Raphael.animation({
        opacity: 0
    }, 10);
    
    logo_ani = Raphael.animation({
        opacity: 0
    }, 250, bringInFeature);

    image03.animate(image03_ani.delay(10));
    imageOut01.animate(imageOut01_ani.delay(10));
    imageOut02.animate(imageOut02_ani.delay(10));
    imageOut03.animate(imageOut03_ani.delay(10));
    imageOut04.animate(imageOut04_ani.delay(10));
    imageOut05.animate(imageOut05_ani.delay(10));

    logo.animate(logo_ani.delay(1500));
}

function bringInFeature() {
    $("#fauxLoader").css("top", 0 + "px");
    $("#fauxLoader").fadeIn(500, function () {
        featuredStory();
    });
}

function featuredStory() {
    var featureImgWidth = $(".homeFeature .left img").width();
    var featureTxtWidth = $(".homeFeature").outerWidth() - featureImgWidth;
    var learnMoreHeight = $(".homeFeature .right .learn-more").find("a").outerHeight();

    //$(".homeFeature .left").css("width", featureImgWidth + "px");
    //$(".homeFeature .right").css("width", featureTxtWidth + "px");
    //$(".homeFeature .right .learn-more").css("width", featureTxtWidth + "px").css("height", learnMoreHeight + "px");
    
    $("#homepageAnimation").css("display", "none");
    $("#homepageAnimation").css("top", -409 + "px");
    $(".homeFeature").fadeIn(500);

    if (jQuery.browser.msie) {
        $("#fauxLoader").fadeOut(100);
        $(".homeFeature, .homeFeature .homeFeatureImageWrap").css("visibility", "visible");
    } else {
        $("#fauxLoader").fadeOut(500);
        $(".homeFeature .homeFeatureImageWrap").css("visibility", "visible");
        $(".homeFeature").css("opacity", 0).css("visibility", "visible").animate({
            opacity: 1
        }, 500);
    }
}