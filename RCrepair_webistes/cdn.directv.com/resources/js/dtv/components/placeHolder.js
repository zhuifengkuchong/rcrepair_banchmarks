/**
* placeHolder.js holds javascript for place holders for UI elements.
* The flows that are using this component currently are as follows:-
* - Registration.
* - Reset Password.
* - Retrieve Email.
**/

!function ($) {
	/**
	* @class Wrapper for all placeholders used.
	* @name DtvApp.Controls.dtvPlaceHolder
	* @extends DtvApp.Modules.BaseModule
	*/
    DtvApp.Modules.BaseModule('DtvApp.Controls.dtvPlaceHolder', {
	},
	{
		/* To avoid initializing multiple times */
    	init: function (){
    		DtvApp.Controls.dtvPlaceHolder = this;
    	},
		/* Initializes placeholder in different
		*  form elements using form id.
		*/
		setPlaceHolder: function(formId){
			$(formId).find('input[type=text]').each(function () {
				/* Place holder logic for input type text elements*/
				var placeholder = $(this).parent().find("span.placeholder").html();
                $(this).attr("placeholder", placeholder);
				
				/* Place holder logic for password and confirm password fields*/
				if($(this).hasClass("dup-password")){
					$(this).focus(function(){
						$(this).hide();
						$(this).siblings('.password').show().focus();
					});
				}
            });
            $(":input[placeholder]").placeholder();
			
			/* Place holder logic for input type tel elements*/
			$(formId).find('input[type=tel]').each(function () {
				/* Place holder logic for input type tel elements*/
				var placeholder = $(this).parent().find("span.placeholder").html();
                $(this).attr("placeholder", placeholder);
				
            });
            $(":input[placeholder]").placeholder();

            /* Place holder logic for input type email elements*/
			$(formId).find('input[type=email]').each(function () {
				/* Place holder logic for input type email elements*/
				var placeholder = $(this).parent().find("span.placeholder").html();
                $(this).attr("placeholder", placeholder);
				
            });
            $(":input[placeholder]").placeholder();

			/* Place holder logic for password and confirm password fields*/
			$(formId).find('input[type=password].password').each(function () {
				$(this).blur(function(){
					if($(this).val() == ""){
						$(this).hide();
						$(this).siblings('.dup-password').show();
					}
				});
            });
		
			/* To avoid copy paste in confirm password field */
			$('#confirmPassword').bind("cut copy paste",function(e) {
				e.preventDefault();
			});
		}
	});
   DtvApp.createControl(document.body, 'dtvPlaceHolder');
}(jQuery);