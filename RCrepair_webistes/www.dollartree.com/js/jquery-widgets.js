// Plugin for slider control (widgetSlider)
(function() {
	jQuery.widgetSlider = function(settings) {
		
		// define defaults and override with options, if available
        // by extending the default settings, we don't modify the argument
        settings = jQuery.extend({
         viewport: "#glo-ucart-slider-viewport",
         content: "#glo-ucart-slider-content",
         next: "#widget-slider-next",
         prev: "#widget-slider-prev",
         item: "div",
         direction: "vertical",
         showAmount: 3
        }, settings);
        
        //if slider exists exist
        if ( jQuery(settings.viewport).size() > 0 )
        {
        	//setup the buttons.
        	jQuery(settings.next).hide().attr("href","javascript:void(0)");
        	jQuery(settings.prev).hide().attr("href","javascript:void(0)");
        	if( jQuery(settings.item).size() > settings.showAmount )
        	{ jQuery(settings.next).show(); }
        
        	if( settings.direction == "vertical" )
        	{
				  //setup the vars.
				  sliderInfo = new Object();
				  sliderInfo.start = 0;
				  sliderInfo.end = (jQuery(settings.item).size() - settings.showAmount) * jQuery(settings.item).eq(0).height() * -1;
				  sliderInfo.itemSize = jQuery(settings.item).eq(0).height();
				  sliderInfo.top = 0;

				  //setup the events for navigation
				  prevButton(settings.prev,settings);
				  nextButton(settings.next,settings);
			  }
        	else
        	{ 
        		//setup the vars.
        		sliderInfo = new Object();
        		sliderInfo.start = 0;
        		sliderInfo.end = (jQuery(settings.item).size() - settings.showAmount) * jQuery(settings.item).eq(0).width() * -1;
        		sliderInfo.itemSize = jQuery(settings.item).eq(0).width();
        		sliderInfo.left = 0;
        	
        		//setup the events for navigation
        		prevButton(settings.prev,settings);
        		nextButton(settings.next,settings);
         	}
        }
    };
    
    prevButton = function(prevObj,settings) {
    	$(prevObj).unbind("click");
    	setTimeout(function() { 
    		$(prevObj).click(function() {
    			sliderInfo.top = parseInt(jQuery(settings.content).css("top").split("px")[0]) + sliderInfo.itemSize;
         		jQuery(settings.content).animate( { top:sliderInfo.top+"px"}, 200 );
         	
         		if( sliderInfo.top != sliderInfo.end )
         		{ jQuery(settings.next).show(); }
         		if( sliderInfo.top == 0 )
         		{ jQuery(settings.prev).hide();  }
         		
         		prevButton(prevObj,settings);
         	});
        }, 351);
    };
    
    nextButton = function(nextObj,settings) {
    	$(nextObj).unbind("click");
    	setTimeout(function() { 
    		$(nextObj).click(function() {
    			sliderInfo.top = parseInt(jQuery(settings.content).css("top").split("px")[0]) - sliderInfo.itemSize;
         		jQuery(settings.content).animate( { top:sliderInfo.top+"px"}, 200 );
         	
         		if( sliderInfo.top == sliderInfo.end )
         		{ jQuery(settings.next).hide(); }
         		if( sliderInfo.top != 0 )
         		{ jQuery(settings.prev).show();  }
         		
         		nextButton(nextObj,settings);
         	});
        }, 351);
    };
    
})(jQuery);
/* ----------------------------------------- */