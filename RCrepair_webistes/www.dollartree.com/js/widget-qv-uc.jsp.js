

/*Pre-load the loading image for both uc and qv.*/
preload_QV_UC_loading = new Image();
preload_QV_UC_loading.src = "../images/uc_qv/ima-glo-loading.gif"/*tpa=http://www.dollartree.com/images/uc_qv/ima-glo-loading.gif*/;

/*
    Persistent Cart (universal cart) Javascript Commands

    persistantCartCommands[0] = "showProduct" action
    persistantCartCommands[1] = "addProduct" action
    persistantCartCommands[2] = "addEnsemble" action
    persistantCartCommands[3] = "remove" action
    persistantCartCommands[4] = "addCatalogItems" action
    persistantCartCommands[5] = "addProductWishlist" action
    persistantCartCommands[6] = "addAllProductsWishlist" action
*/

var persistantCartCommands = new Array(9);
persistantCartCommands[0] = "http://www.dollartree.com/checkout/universal_cart.jsp";
persistantCartCommands[1] = "http://www.dollartree.com/checkout/add_item_pc.cmd";
persistantCartCommands[2] = "http://www.dollartree.com/checkout/add_items_pc.cmd";
persistantCartCommands[3] = "http://www.dollartree.com/checkout/delete_item_in_cart.cmd";
persistantCartCommands[4] = "http://www.dollartree.com/checkout/add_catalog_order_item_pc.cmd";
persistantCartCommands[5] = "http://www.dollartree.com/user/add_wishlist_item_to_basket_pc.cmd";
persistantCartCommands[6] = "http://www.dollartree.com/user/add_all_wishlist_items_to_basket_pc.cmd";
persistantCartCommands[7] = "http://www.dollartree.com/user/instore_pickup_zip_json_pc.jsp";
persistantCartCommands[8] = "http://www.dollartree.com/checkout/add_gift_certificate_pc.cmd";

var requestURL = "";
var persistantCartContainerId = "#widget-ucart";
var persistantCartCloseButClass = ".widget-ucart-close-but";
var hideTimeOuts= new Array();
var ucHideTimeout = null;
var isQuickView = false;
var ucartLoadingHTML = 	'<div id="widget-ucart">' +
				 		'  <div id="glo-ucart-top" class="widget-ie6png"><!--  --></div>' +
				  		'  <div id="glo-ucart-body" class="widget-ie6png">' +
				  		'    <div id="glo-ucart-content">'+
				  		'	   <div class="widget-ima-loader"><img src="../assets/images/common/uc_qv/ima-glo-loading.gif"/*tpa=http://www.dollartree.com/assets/images/common/uc_qv/ima-glo-loading.gif*/ alt="Loading..."/></div>' +
				  		'    </div>' +
				  		'  </div>' +
				  		'  <div id="glo-ucart-bottom" class="widget-ie6png"><!--  --></div>' +
  				  		'</div>';

/* Function(s) to Show the Basket Layer */
function showBasket(action, params , refreshPage, refreshDelayTime)
{ 
    showloading();

	if(action == "show" || action == "showFromQuickview") requestURL = persistantCartCommands[0];
	else if(action == "addProduct") requestURL = persistantCartCommands[1];
	else if(action == "addEnsemble") requestURL = persistantCartCommands[2];
	else if(action == "remove") requestURL = persistantCartCommands[3];
	else if(action == "addCatalogItems") requestURL = persistantCartCommands[4];
	else if(action == "addProductWishlist") requestURL = persistantCartCommands[5];
	else if(action == "addAllProductsWishlist") requestURL = persistantCartCommands[6];
	else if(action == "addGiftCertificate") requestURL = persistantCartCommands[8];
	else { alert("missing action"); }

	requestURL = requestURL;
	params = "ts=" + timestamp() + "&action=" + action + "&" + params;

	$.ajax({
		type: "POST",
		url: requestURL,
		data: params,
		dataType: "html",
        timeout: 1500000,
		success: function(data)
        {
			hideloading();
			$(persistantCartContainerId).append(data);
			$(persistantCartContainerId).show();
			var val = Number($("#grandTotalUpdater").val().substring(1));
			if(val > 0) $("#widget-but-ucartcount").html("(" + $("#grandTotalUpdater").val() + ")");
			else $("#widget-but-ucartcount").html("");
			var newTotal = $("#glo-ucart-content .greenHead .totalPrice").text();

			if(newTotal != "http://www.dollartree.com/js/$0.00") {
				$(".total-text a").html(newTotal);
                //console.log('==> set SLI Amount cookie from qv-uc:' + newTotal);
                setSLICookieAmount(newTotal);
			}  else {
				$(".total-text a").html("")
			}

			if (refreshPage != undefined && refreshPage) {
            	setTimeout( function() { location.reload(true); }, refreshDelayTime != undefined? refreshDelayTime : 0);
            }

			return true;
		},
		error: function()
        {
			hideloading();
			return false;
		}
	});
};

function showloading()
{
    $(persistantCartContainerId).remove();

	/*load, position, show new cart*/
	$("#outer_wrap").append(ucartLoadingHTML);
	positionPersistantCart();
	$(persistantCartContainerId).show();
};

function hideloading() {
	$(persistantCartContainerId + " *").remove();
	$(persistantCartContainerId).html("");
};

function isZip(s)
{

     /*Check for correct zip code*/
     reZip = new RegExp(/(^\d{5}$)|(^\d{5}-\d{4}$)/);

     if (!reZip.test(s)) {
          return false;
     }
    return true;
}

function isNumber(s)
{

     /*Check for correct zip code*/
     reNumber = new RegExp(/(^\d{1}$)|(^\d{2})/);

     if (!reNumber.test(s)) {
          return false;
     }

    return true;
}

function addToCart() {
    var caseQuantity = ($("input[name=enterCases]").val() > 0) ? $("input[name=enterCases]").val() : $("select[name=numOfCases]").val();
    var unitQuantity = ($("input[name=numOfUnits]").val() > 0) ? $("input[name=numOfUnits]").val() : 0;
    var casePackSize = parseInt($("input[name=casePack]").val()) || 0;
    var totalQuantity = (caseQuantity * casePackSize) + (parseInt(unitQuantity) || 0);

	var params = "productId=" + $("input[name=productId]").val()
	            + "&categoryId=" + $("input[name=categoryId]").val()
	            + "&parentCategoryId=" + $("input[name=parentCategoryId]").val()
	            + "&subCategoryId=" + $("input[name=subCategoryId]").val()
	            + "&quantity=" + caseQuantity
	            + "&productVariantId=" + $("input[name=productVariantId]").val()
	            + "&inStorePickupZipCode=" + $("input[name=inStorePickupZipCode]").val()
                + "&inStorePickupState=" + $("input[name=inStorePickupState]").val()
				+ "&hasWarnings="  + $("input[name=hasWarnings]").val()
				+ "&availableQuantity=" + $("input[name=availableQuantity]").val()
				+ "&casePack=" + $("input[name=casePack]").val()
				+ "&inStorePreferredStore=" + $("input[name=inStorePreferredStore]").val()
				+ "&numOfUnits=" + $("select[name=numOfUnits]").val() 
				+ "&upgradeToCase=" + $("input[name=upgradeToCase]").val()
				+ "&sourceCode=" + $("input[name=sourceCode]").val()
				+ "&siteId=" + $("input[name=siteId]").val();

	/*see if this is an update.*/
	if($("input[name=itemGUID]").val().length > 0) params = params + "&itemGUID=" + $("input[name=itemGUID]").val() + "&isUpdate=1";

	//if there are warnings, show the overlay
    var isWarning = $('input[name="sfAknowledgment"]:not(:checked)').length;
    if (isWarning > 0) {
        $('#deliveryOptionsWrap').css('display', 'none');
        $("div.warnWrap ul").html("");
        $('input[name="sfAknowledgment"]:not(:checked)').each(function () {
            $("div.warnWrap ul").append("<li><div class='chkBox'><input type='checkbox' style='float:left;' onClick='btnEnableDisable();'>"
                    + $(this).attr("data-description")
                    + "</div><div class='clr'></div></li>");
        });
        $.colorbox({
            inline: true,
            href: function () {
                return "#productWarningsPopup";
            },
            width: 500,
            overlayClose: true
        });
        return false;
    } else {
        //ENH00952: TEALIUM for Coremetrics (cart_add for pdp)
        var x = {
            event_type: "cart_add",
            product_quantity: ["" + totalQuantity],
            product_id: [$("input[name=productId]").val()],
            product_name: [$("input[name=productName]").val()],
            product_category: [$("input[name=subCategoryId]").val()],
            product_unit_price: [$("input[name=itemPrice]").val()]
        };
        utag.link(x);
        showBasket('addProduct', params);
    }
};

function acknowledgeAddToCart() {
    $('input[name="sfAknowledgment"]:not(:checked)').each(function() {
        $(this).prop('checked', true);
    });
    $.colorbox.close();
    addToCart();
}

/* Used from the PDP overlay after ALL acknowledgements are checked */
function justAddToCart() {
    var caseQuantity = ($("select[name=numOfCases]").val() > 0)?$("select[name=numOfCases]").val():$("input[name=enterCases]").val();

	var params = "productId=" + $("input[name=productId]").val()
	            + "&categoryId=" + $("input[name=categoryId]").val()
	            + "&parentCategoryId=" + $("input[name=parentCategoryId]").val()
	            + "&subCategoryId=" + $("input[name=subCategoryId]").val()
	            + "&quantity=" + caseQuantity
	            + "&productVariantId=" + $("input[name=productVariantId]").val()
	            + "&inStorePickupZipCode=" + $("input[name=inStorePickupZipCode]").val()
                + "&inStorePickupState=" + $("input[name=inStorePickupState]").val()
				+ "&hasWarnings="  + $(quickViewContainerId + " input[name=hasWarnings]").val()
				+ "&availableQuantity=" + $("input[name=availableQuantity]").val()
				+ "&casePack=" + $("input[name=casePack]").val()
				+ "&inStorePreferredStore=" + $("input[name=inStorePreferredStore]").val()
				+ "&numOfUnits=" + $("select[name=numOfUnits]").val()
				+ "&upgradeToCase=" + $("input[name=upgradeToCase]").val()
				+ "&sourceCode=" + $("input[name=sourceCode]").val()
				+ "&siteId=" + $("input[name=siteId]").val();

	/*see if this is an update.*/
	if($("input[name=itemGUID]").val().length > 0) params = params + "&itemGUID=" + $("input[name=itemGUID]").val() + "&isUpdate=1";
    /* add to basket and show the minicart */
    showBasket('addProduct', params);
};

function addGCToCart() {
	var gcAmount = $("input[name=amount]").val();
	if (gcAmount.indexOf(".") == -1)
		gcAmount += ".00";
	var params = "productId=" + $("input[name=productId]").val() +
				"&productVariantId=" + $("input[name=productVariantId]").val() +
				"&gcStyle=" + $("#gcStyle").val() +
				"&amount=" + gcAmount +
				"&quantity=" + $("input[name=quantity]").val() +
				"&inStorePickupZipCode=" + $("input[name=inStorePickupZipCode]").val();

	/*see if this is an update.*/
	if( $("input[name=itemGUID]").val().length > 0 ) {
		params = params + "&itemGUID=" + $("input[name=itemGUID]").val() + "&isUpdate=1";
	}
	setUserZipCodePCAndForward('addGiftCertificate',params);
};

function wishListAddToCart(params) {
	showBasket('addProductWishlist',params);
};

function wishListAddAllToCart(params) {
	showBasket('addAllProductsWishlist',params);
};

function addCatalogOrderItemsToCart() {

	var params = "productId=" + $("input[name=productId]").val() +
			 "&itemNumber=" + $("input[name=itemNumber]").val() +
			 "&productVariantId=" + $("input[name=productVariantId]").val() +
			 "&quantity=" + $("input[name=quantity]").val() +
			 "&inStorePickupZipCode=" + $("input[name=inStorePickupZipCode]").val() +
			 "&inStorePreferredStore=" + $("input[name=inStorePreferredStore]").val() +
			 "&numOfUnits=" + $("select[name=numOfUnits]").val();
	setUserZipCodePCAndForward('addCatalogItems',params);

};

function addEnsembleToCart(type)
{
	var params =  "productId=" + $("input[name=productId]").val() +
			  "&ensembleId=" + $("input[name=ensembleId]").val() +
			  "&categoryId=" + $("input[name=categoryId]").val() +
			  "&parentCategoryId=" + $("input[name=parentCategoryId]").val() +
			  "&inStorePickupZipCode=" + $("input[name=inStorePickupZipCode]").val() +
			  "&inStorePreferredStore=" + $("input[name=inStorePreferredStore]").val();

	/*iterate through products in the ensemble for variant id*/
	$(".the-variant-ids").each(function() {
		params = params + "&" + $(this).attr("name") + "=" + $(this).val();
	});

	/*iterate through the products for the casepack*/
	$(".the-variant-casePack").each(function() {
		params = params + "&" + $(this).attr("name") + "=" + $(this).val();
	});

	/*iterate through the products for available qty*/
	$(".the-variant-availQty").each(function() {
		params = params + "&" + $(this).attr("name") + "=" + $(this).val();
	});

	/*iterate through the products for determining if the user chose to upgrade to a case*/
	$(".the-variant-upgrade").each(function() {
		params = params + "&" + $(this).attr("name") + "=" + $(this).val();
	});

	/* iterate through the products for num of units*/
	$(".the-variant-numOfUnits").each(function() {
		params = params + "&" + $(this).attr("name") + "=" + $(this).val();
	});

	/*iterate through products for qty*/
	$(".the-variant-qtys").each(function() {
		if(type == 'all')
		{
		  $(this).val("1");
		  params = params + "&" + $(this).attr("name") + "=1";
		}
		else params = params + "&" + $(this).attr("name") + "=" + $(this).val();
	});

	params = params + "&productCount=" + $(".the-variant-ids").length;
	setUserZipCodePCAndForward('addEnsemble', params);
};

function setUserZipCodePC() {
    var zipCode = $("input[name=usrZip]").val();
    if(!zipCode.match(/^\d+$/)) {
        alert('Please enter a numeric zip code.');
        return false;
    }
    lat = 0;
    lng = 0;
    var params = "ts=" + timestamp() + "&action=updateUserZipCode" + "&storesListZipCode=" +  zipCode +
            "&storesListLatitude=" +  lat + "&storesListLongitude=" +  lng;
    var requestURL = persistantCartCommands[7];

    $.ajax({
        type: "GET",
        url: requestURL,
        data: params,
        dataType: "json",
        timeout: 1500000,
        success: function(data)
        {
            dealWithModal();
            return true;
        },
        error: function()
        {
            alert('An error occurred while trying to save your zip code. Please try again later.');
            return false;
        }
    });
}

function dealWithModal() {
    //close modal
    $('#deliveryOptionsWrap').slideUp('slow');
    // stop modal from displaying again
    if (readCookie('page_count') == null ? false : true) {
        eraseCookie('page_count');
        createCookie('page_count', 4, '.dollartree.com');
    } else {
        createCookie('page_count', 4, '.dollartree.com');
    }
    //reload parent so it refreshes zip
    window.location.reload();
};

/*  This function is designed to be called from product, ensemble, and quickview only! On those pages the zip code is
    stored in a field named inStorePickupZipCode. */
function setUserZipCodePCAndForward(next, paramsToPass)
{
    var zipCode = $("input[name=inStorePickupZipCode]").val();
    var cases = $("input[name=quantity]").val();
    var numOfUnits = $("select[name=numOfUnits]").val();
    var lat = "";
    var lng = "";

    var isStateRestricted =$("input[name=isStateRestricted]").val();
    if(isStateRestricted == "true") alert("Item not available in state due to state restrictions.");
    else  {
        if(zipCode=='')  {
            errorAppend("#error-zip","Please enter your ZIP code to ensure that this item is available in your area.");
            if(cases == '' && numOfUnits <= 0) errorAppend("#error-quantity", "Please enter a quantity between 1 and 999.");
            return false;
        }  else  {
            /*Call the set zip code command.*/
            lat = 0;
            lng = 0;
            var params = "ts=" + timestamp() + "&action=updateUserZipCode" + "&storesListZipCode=" +  zipCode +
                            "&storesListLatitude=" +  lat + "&storesListLongitude=" +  lng;
            var requestURL = persistantCartCommands[7];

            $.ajax({
                type: "GET",
                url: requestURL,
                data: params,
                dataType: "json",
                timeout: 150000,
                success: function(data)
                {
                    showBasket(next, paramsToPass);
                    return true;
                },
                error: function()
                {
                    alert('There was an error trying to save your zip code.');
                    return false;
                }
            });
        }
    }
}

function updateHeader(amt) {
	if(amt == 1)
	{ $("#widget-ucart-item-count").text(amt + " item(s)"); }
	else
	{ $("#widget-ucart-item-count").text(amt + " item(s)");}
};

function errorAppend(area,msg) {
	$(area).html(msg.replace(/&amp;/g, "&").replace(/&lt;/g,
        "<").replace(/&gt;/g, ">").replace(/&#39;/g, "'"));
	$(area).show();
};

function resetErrorFields() {
	$(".glo-tex-error").hide();
	$(".glo-tex-error").html("");
};

function messageAppend(area,msg) {
	$(area).html(msg);
	$(area).show();
};

function resetMessageFields() {
	$(".glo-tex-info").hide();
};

function clearAllTimeouts() {
	if (ucHideTimeout != null)
		clearTimeout(ucHideTimeout);
	ucHideTimeout = setTimeout( hideBasket, 5000);
};

function timestamp() { return new Date().getTime(); }

/*Edit this function if need to do something special on basket close. */
function hideBasket() {
	
	$(persistantCartContainerId).hide();
	$(persistantCartContainerId).remove();
	shoppingBagBut = $("#widget-header-active-link").eq(0);
	$(shoppingBagBut).attr("id","");
	$(shoppingBagBut).mouseout();
};

/*edit this function to update the setup*/
function setupPersistantCartButtons() {
	/*draw focus near this*/
	window.location = "#";

	$(persistantCartCloseButClass).unbind("click").click(function() {
		hideBasket();
	});
	$(persistantCartCloseButClass).attr("href","javascript:void(0)");
	clearAllTimeouts();
};

var ucartItemHeights = new Array();
var ucartContentHeight = 0;
function setupSlider() {
	/*size the slider*/
	var h = 0;
	$(".glo-ucart-slider-item").each(function() {
		h = $(this).height();
		ucartItemHeights.push(h);
		ucartContentHeight = ucartContentHeight + h;
	});

	$.widgetSlider({
		viewport: "#glo-ucart-slider-viewport",
		content: "#glo-ucart-slider-content",
		next: "#widget-slider-next",
		prev: "#widget-slider-prev",
		item: ".glo-ucart-slider-item",
		direction: "vertical",
		showAmount: 3
	});
};

/*edit this function to position cart.*/
function positionPersistantCart() {
	return; /*dynamic positioning not needed*/
	newLeft = 10 + ($("body").width() / 2) + ( $("#mainDiv").width() / 2 ) - $(persistantCartContainerId).width();

	$(persistantCartContainerId).css("left", newLeft+"px");
	$(persistantCartContainerId).css("top", "30px");
};
/* ---------------------------------------- */
/* QuickView Javascript */
/* ---------------------------------------- */

var quickViewCommands = new Array(2);
quickViewCommands[0] = "http://www.dollartree.com/catalog/quickview.jsp";
quickViewCommands[1] = "http://www.dollartree.com/catalog/qv_add_item_pc.cmd";
quickViewCommands[2] = "http://www.dollartree.com/checkout/add_item.cmd";

var quickViewContainerId = "#widget-quickview";
var quickViewCloseButtonsClass = ".widget-quickview-but-close";
var quickViewCloseButtonsAdd = ".widget-quickview-but-add";

var quickviewContainerHTML = '<div id="widget-quickview" class="prod-ens"></div>';
var quickviewLoadingHTML = 	'  <div id="cat-quickview-top" class="widget-ie6png"><!--  --></div>' +
				  			'  <div id="cat-quickview-body" class="widget-ie6png">' +
				  			'	 <div id="cat-quickview-content">'+
				  			'	   <div class="widget-ima-loader"><img src="../assets/images/common/uc_qv/ima-glo-loading.gif"/*tpa=http://www.dollartree.com/assets/images/common/uc_qv/ima-glo-loading.gif*/ alt="Loading..."/></div>' +
				  			'    </div>' +
				  			'  </div>' +
				  			'  <div id="cat-quickview-bottom" class="widget-ie6png"><!--  --></div>';


/* Function(s) for QuickView */
function loadQuickView(params,selector) {
    isQuickView = true;
	closeQuickView();
	addQuickView(selector);
	ajaxQuickView(quickViewCommands[0],params);

	/* if this is an edit, want to have this appear above the cart. */
	if ( params.indexOf("itemGUID") != -1 ) {
		$(quickViewContainerId).css("z-index", "701"); 
	}
    loadQuickView
};

function addQuickView(selector) {
	$(selector).append(quickviewContainerHTML);
	$(quickViewContainerId).append(quickviewLoadingHTML);
	adjustQuickviewLocation();
	$(quickViewContainerId).show();
};

function closeQuickView() {
	$(quickViewContainerId).remove();
};

function ajaxQuickView(page,params) {
	params = "rId=" + new Date().getTime() + "&" + params;
	$.ajax({
   		type: "POST",
   		url: page,
   		data: params,
   		dataType: "html",
   		success: function(msg) {
   			$(quickViewContainerId).html(msg);
			adjustQuickviewLocation();
   		}
   	});
};

function nonAjaxAddItem(page,params) {
	window.location = page + "?" + params;
};

function addProductFromQuickView(params) {
	var bypassUC = false;
/*	productName was phased out because of PCI scan fails, but quick view functionality relies on it being set here
	to check if something was added to the cart (quickview.jsp - ln. 59); */
    var caseQuantity = ($(quickViewContainerId + " select[name=numOfCases]").val() > 0)?$(quickViewContainerId + " select[name=numOfCases]").val():$(quickViewContainerId + " input[name=enterCases]").val();
    var unitQuantity = ($(quickViewContainerId + " select[name=numOfUnits]").val() > 0) ? $(quickViewContainerId + " select[name=numOfUnits]").val() : 0;
    var casePackSize = parseInt($(quickViewContainerId + " input[name=casePack]").val()) || 0;
    var totalQuantity = (caseQuantity * casePackSize) + (parseInt(unitQuantity) || 0);
	params = params + "&productName=addedProductFromQuickView"
	    + "&productVariantId=" + $(quickViewContainerId + " input[name=productVariantId2]").val()
	    + "&quantity=" + caseQuantity
        + "&inStorePickupZipCode=" + $(quickViewContainerId + " input[name=inStorePickupZipCode]").val()
        + "&inStorePickupState=" + $("input[name=inStorePickupState]").val()
        + "&hasWarnings=" + $(quickViewContainerId + " input[name=hasWarnings]").val()
        + "&availableQuantity=" + $(quickViewContainerId + " input[name=availableQuantity]").val()
        + "&casePack=" + $(quickViewContainerId + " input[name=casePack]").val()
        + "&inStorePreferredStore=" + $(quickViewContainerId + " input[name=inStorePreferredStore]").val()
        + "&numOfUnits=" + $(quickViewContainerId + " select[name=numOfUnits]").val()
        + "&sourceCode=" + $("input[name=sourceCode]").val() + "&siteId=" + $("input[name=siteId]").val();

        var isStateRestricted =$(quickViewContainerId + " input[name=isStateRestricted]").val();
        if( isStateRestricted == "true") {
            alert("Item not available in state due to state restrictions.");
        }
        else {
            if ( $(quickViewContainerId + " input[name=itemGUID]").size() > 0 ) {
                params = params + "&itemGUID=" + $(quickViewContainerId + " input[name=itemGUID]").val();
            }
            if ( $(quickViewContainerId + " input[name=onBasketPage]").size() > 0 ) {
                params = params + "&onBasketPage=" + $(quickViewContainerId + " input[name=onBasketPage]").val();
            }
            $(quickViewContainerId + " select").each(function() {
                params = params + "&" + $(this).attr("name") + "=" + $(this).val();
            });

            /*Coremetrics Shop 5 tag from Quick view*/
            //psCreateShopAction5Tag($(quickViewContainerId + " input[name=productVariantId2]").val(), $(quickViewContainerId + " input[name=productName]").val(),  $(quickViewContainerId + " input[name=quantity]").val(), $(quickViewContainerId + " input[name=unitPrice]").val(), $(quickViewContainerId + " input[name=categoryId]").val());
            //psDisplayShop5s();
            //ENH00952: TEALIUM for Coremetrics (cart_add for quickview)
            var x = {
                event_type : "cart_add",
                product_quantity: ["" + totalQuantity],
                product_id : [$(quickViewContainerId + " input[name=productVariantId2]").val()],
                product_name : [$(quickViewContainerId + " input[name=productName]").val()],
                product_category : [$(quickViewContainerId + " input[name=categoryId]").val()],
                product_unit_price : [$(quickViewContainerId + " input[name=unitPrice]").val()]
            };
            utag.link(x);
            $(quickViewContainerId + " *").remove();
            $(quickViewContainerId).append(quickviewLoadingHTML);
            if (bypassUC) {
                nonAjaxAddItem(quickViewCommands[2],params);
            } else {
                ajaxQuickView(quickViewCommands[1],params);
            }
        }
};

var qvInterval;
function setupAddQuickViewButton() {
	$(quickViewCloseButtonsAdd).unbind("click").click(function() {
		params = "productId=" + $(this).attr("id").split("-")[1];
		addProductFromQuickView(params);
	});
};

function setupCloseQuickViewButton() {
    $(quickViewCloseButtonsClass).unbind("click").click(function() { closeQuickView(); });
	$(quickViewCloseButtonsClass).add( $(quickViewCloseButtonsAdd) ).attr("href","javascript:void(0)");
};

/*Edit this per site to adjust location*/
function adjustQuickviewLocation() {
	var bWindowOffsets = getScrollXY();
	var bWindowViewport = getViewportSize();
	var qvTop = ((bWindowViewport[1] / 2) - ($(quickViewContainerId).height() / 2)) + bWindowOffsets[1];
	qvTop = (qvTop < 0) ? 100 : qvTop;
	$(quickViewContainerId).css("top",qvTop+"px");
};

/* Helper Function(s) */
function getScrollXY() {
  var scrOfX = 0, scrOfY = 0;
  if( typeof( window.pageYOffset ) == 'number' ) {
    scrOfY = window.pageYOffset;
    scrOfX = window.pageXOffset;
  } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
    scrOfY = document.body.scrollTop;
    scrOfX = document.body.scrollLeft;
  } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
    scrOfY = document.documentElement.scrollTop;
    scrOfX = document.documentElement.scrollLeft;
  }
  return [ scrOfX, scrOfY ];
}

function getViewportSize() {
  var vpW = 0, vpH = 0;
  if (typeof window.innerWidth != 'undefined')
  {
    vpW = window.innerWidth;
    vpH = window.innerHeight;
  }
  else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0)
  {
    vpW = document.documentElement.clientWidth;
    vpH = document.documentElement.clientHeight;
  }
  else
  {
    vpW = document.getElementsByTagName('body')[0].clientWidth;
    vpH = document.getElementsByTagName('body')[0].clientHeight;
  }
  return [  vpW, vpH ];
};

var iframeFix = function($) {
	return {
		add : function(id ,selector, parentId) {
			if (parentId != "body")
				parentId = "#" + parentId;
			iframeFix.remove(id);
			iframeHTML = '<div id="' + id + '"><iframe width="100%" height="100%" src="" style="filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0);" frameborder="0"></iframe></div>';
			$(parentId).append(iframeHTML);
			$("#" + id).css("position", "absolute");
			$("#" + id).css("top", $(selector).css("top")); 
			$("#" + id).css("left", $(selector).css("left"));
			$("#" + id).css("right", $(selector).css("right"));
			$("#" + id).css("margin-top", $(selector).css("margin-top"));
			$("#" + id).css("margin-right", $(selector).css("margin-right"));
			$("#" + id).css("margin-bottom", $(selector).css("margin-bottom"));
			$("#" + id).css("margin-left", $(selector).css("margin-left"));
			$("#" + id).css("height", $(selector).height() + "px" );
			$("#" + id).css("width", $(selector).width() + "px" );
			$("#" + id).css("z-index", $(selector).css("z-index") - 1);
		},
		remove : function(id) {
			$("#" + id).remove(); 
		}
	};
}($);

/* Quickview setup */
function setup_quickview() {
	var qTimer;
	var imgHTML = '<a href="#"><button type="button" id="widget-quickview-but" name="QUICK VIEW" class="button small grey">QUICK VIEW</button></a>';
	$(".widget-app-quickview").each(function() {
		$(this).parent().parent().css("position","relative");
		$(this).mouseover(function() {
			if( typeof qTimer != undefined )
			{
				clearTimeout(qTimer); 
				$("#widget-quickview-but").parent().remove();
			}
			$(this).parent().parent().append(imgHTML);
			$("#widget-quickview-but").css("position","absolute");
			$("#widget-quickview-but").css("left", "50px");
			qTop = $(".widget-app-quickview", $("#widget-quickview-but").parent().parent()).height() - $("#widget-quickview-but").height() - 2 - 10;
			$("#widget-quickview-but").css("top", qTop+"px");
			$("#widget-quickview-but").mouseover(function() {
				if(typeof qTimer != undefined)
				{ clearTimeout(qTimer); }
			});
			$("#widget-quickview-but").parent().click(function(ev) {
				type = $(this).parent().attr("id").split("-")[0];
                id = $(this).parent().attr("id").split("-")[1];
				if( type == "p" )
				{ params = "productId="+id;}
				else
				{ params = "ensembleId="+id; }
				loadQuickView(params, $("body"));
			});
			$("#widget-quickview-but").parent().attr("href","javascript:void(0);");
		});
		$(this).mouseout(function() {
			qTimer = setTimeout(function() { $("#widget-quickview-but").parent().remove(); }, 100);
		});
	});		
};

function setup_quickview_basket() {
	var qTimer;
	var imgHTML = '<a href="#"><img id="widget-quickview-but" class="widget-ie6png" src="../assets/images/uc_qv/but-cat-quickview.png"/*tpa=http://www.dollartree.com/assets/images/uc_qv/but-cat-quickview.png*/ alt="QUICK VIEW" /></a>';
	$(".widget-app-quickview-basket").each(function() {
		$(this).parent().parent().css("position","relative");
		$(this).mouseover(function() {
			if( typeof qTimer != undefined )
			{
				clearTimeout(qTimer); 
				$("#widget-quickview-but").parent().remove();
			}
			$(this).parent().parent().append(imgHTML);
			$("#widget-quickview-but").css("position","absolute");
			$("#widget-quickview-but").css("left", "55px");
			qTop = $(".widget-app-quickview-basket", $("#widget-quickview-but").parent().parent()).height() - $("#widget-quickview-but").height() - 2 - 10;
			$("#widget-quickview-but").css("top", qTop+"px");
			$("#widget-quickview-but").mouseover(function() {
				if(typeof qTimer != undefined)
				{ clearTimeout(qTimer); }
			});
			$("#widget-quickview-but").parent().click(function(ev) {
				params = $("span", $(this).parent()).text();
				loadQuickView(params, $("body"));
			});
			$("#widget-quickview-but").parent().attr("href","javascript:void(0);");
		});
		$(this).mouseout(function() {
			qTimer = setTimeout(function() { $("#widget-quickview-but").parent().remove(); }, 100);
		});
	});		
};


$(function() {
	setup_quickview();
	setup_quickview_basket();
});