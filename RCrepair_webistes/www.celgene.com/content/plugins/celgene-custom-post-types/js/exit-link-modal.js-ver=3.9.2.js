//	CELGENE EXIT LINK MODAL SCRIPT

_exit_link_modal = {

	PLUGIN_PATH: _celgene_exit_link_modal_plugin_path.path,
	WP_THEME_ROOT: _celgene_exit_link_modal_wp_theme_root.root,

	document_init: function($) {
		_exit_link_modal.generate_markup($);
		_exit_link_modal.bind_window_resize($);
		_exit_link_modal.bind_overlay_close($);
        this.window_init($);
	},

	window_init: function($) {
		_exit_link_modal.attach_modal_event($);

		_exit_link_modal.bind_manual_triggers($);
	},

	generate_markup: function($) {
/*		var $LOAD = $('<div class="exit-link-modal-content-block" id="exit-link-modal-content-block"></div>');
		$LOAD.load(_exit_link_modal.WP_THEME_ROOT + "/inc/exit-link-modals/default.php", function() {
			var DEFAULT = $LOAD.html();
			$LOAD.load(_exit_link_modal.WP_THEME_ROOT + "/inc/exit-link-modals/product-sites.php", function() {
				var PRODUCT_SITES = $LOAD.html();
				var HTML = '';
				HTML += '<div class="exit-link-modal-background" id="exit-link-modal-background"></div>';
				HTML += '<div class="exit-link-modal-content" id="exit-link-modal-content">';
					HTML += '<a href="#" class="exit-link-modal-close modal-close" id="exit-link-modal-close">X</a>';
					HTML += DEFAULT;
					HTML += PRODUCT_SITES;
				HTML += '</div>';
				$("body").append(HTML);
			});
		}); */
	},

	attach_modal_event: function($) {
		$(document).on("click", "a", function(event) {
			if( !$(this).hasClass("exit-link-modal-continue") && !$(this).hasClass("exit-link-modal-label-continue") ) {
				var href = "";
				if( typeof $(this).attr("href") !== "undefined" ) {
					href = $(this).attr("href");
				}
				var rel = $(this).attr("rel");
				if( rel == "celgene-owned" || $(this).hasClass("celgene-owned")) {
					return;
				}
				var is_external = (rel == 'celgene-owned-modal' || $(this).hasClass("celgene-owned-modal") || $(this).hasClass("trigger_interstitial") || _exit_link_modal.is_external(href));
				if( !is_external ) {
					return;
				} else {
					event.preventDefault();
					_exit_link_modal.set_overlay(href, rel);
				}
			}
		});
	},

	set_overlay: function(href, which) {
		$(".exit-link-modal-content-content").hide();
		$(".exit-link-modal-continue").attr("href", href);
		$(".exit-link-link-label a").attr("href", href);
		var short_href = href;
		if( href.length > 50 ) {
			short_href = href.substr(0, 50) + "...";
		}
		$(".exit-link-link-label a").html(short_href);
		switch(which) {
			case "product-sites":
				$("#product-sites-exit-link-modal").show();
				break;
			default:
				$("#default-exit-link-modal").show();
				break;
		}
		_exit_link_modal.open_overlay($);
	},

	bind_manual_triggers: function($) {
		$(".menu-countries-container, .menu-therapies-container").on('click', 'ul li', function() {
			var $this= $(this);
			if( $this.attr("data-option-value") != "") {
				var href = $this.attr("data-option-value");
				var is_external = _exit_link_modal.is_external(href);
				var rel = $this.attr("data-option-rel");

				if ($.isFunction($this.send_ga)) $this.send_ga();

				if( is_external ) {
					_exit_link_modal.set_overlay(href, rel);
					/**
                     * Lloyd McGarrigal
                     * 10/29/2014
                     * Adding analytics events for the therapies dropdown
                     *
                     * Oleg
                     * 12/17/14
                     * moved out to celgene-ga plugin
                     * /
                    var hrefToParams = {
                        'http://www.abraxane.com/' : {
                            'category' : 'Find a Therapy',
                            'action' : 'Click',
                            'label' : 'Abraxane'
                        },'http://www.istodax.com/' : {
                            'category' : 'Find a Therapy',
                            'action' : 'Click',
                            'label' : 'Istodax'
                        },
                        'http://www.otezla.com/' : {
                            'category' : 'Find a Therapy',
                            'action' : 'Click',
                            'label' : 'Otezla'
                        },
                        'http://www.pomalyst.com/' : {
                            'category' : 'Find a Therapy',
                            'action' : 'Click',
                            'label' : 'Pomalyst'
                        },
                        'http://www.revlimid.com/' : {
                            'category' : 'Find a Therapy',
                            'action' : 'Click',
                            'label' : 'Revlimid'
                        },
                        'http://www.thalomid.com/' : {
                            'category' : 'Find a Therapy',
                            'action' : 'Click',
                            'label' : 'Thalomid'
                        },
                        'http://www.vidaza.com/' : {
                            'category' : 'Find a Therapy',
                            'action' : 'Click',
                            'label' : 'Vidaza'
                        },
                        'https://www.lifebankusa.com' : {
                            'category' : 'Find a Therapy',
                            'action' : 'Click',
                            'label' : 'Life Bank USA'
                        }
                    }
                    if( hrefToParams.hasOwnProperty(href) ) {
                        ga('send', 'event', hrefToParams[href]['category'], hrefToParams[href]['action'],hrefToParams[href]['label']);
                    }*/
				} else {
					window.location.href = href;
				}
			}
		});
	},

	bind_window_resize: function($) {
		_exit_link_modal.size_modals($);
		$(window).resize(function() {
			_exit_link_modal.size_modals($);
		});
	},

	size_modals: function($) {
        $modal_content = $("#exit-link-modal-content");
		$("#exit-link-modal-background").css({
			width: $(window).width() + "px",
			height: $(document).height() + "px"
		});
		$modal_content.css({
			top: ( $(window).height() / 2 ) - ( $("#exit-link-modal-content").outerHeight() / 2 ) + "px",
			left: ( $(window).width() / 2 ) - ( $("#exit-link-modal-content").outerWidth() / 2 ) + "px"
		});
        // // modal is too tall on mobile
        // // Lloyd McGarrigal
        // // 12/11/2014
        // if( document.documentElement.clientHeight <= $modal_content.height() ) {
        //     $modal_content.height( document.documentElement.clientHeight * 0.8 );
        //     $modal_content.css('overflow-y', 'scroll');
        // }
        // if( parseInt($modal_content.css('top')) < 0  ) {
        //     $modal_content.css('top', '5px');
        // }
	},

	bind_overlay_close: function($) {
		$(document).on("click", ".exit-link-modal-close", function(event) {
			event.preventDefault();
			_exit_link_modal.close_overlay($);
		});
		$(document).on("click", "#exit-link-modal-background", function(event) {
			_exit_link_modal.close_overlay($);
		});
		$(document).on("click", ".exit-link-modal-continue", function(event) {
			_exit_link_modal.close_overlay($);
		});
		$(document).on("click", ".exit-link-link-label a", function(event) {
			_exit_link_modal.close_overlay($);
		});
	},

	close_overlay: function($) {
		$("#exit-link-modal-background").fadeOut();
		$("#exit-link-modal-content").fadeOut();
	},

	open_overlay: function($) {
		$("#exit-link-modal-background").fadeIn();
		$("#exit-link-modal-content").fadeIn();
		$(window).trigger("resize");
	},

	get_domain: function(url) {
		return url.replace("https://www.", "").replace("http://www.", "").replace("https://", "").replace("http://", "").split("/")[0];
	},

	is_external: function(url) {
		url = url.toLowerCase();
		var celgene = _exit_link_modal.get_domain(document.location.href.toLowerCase());
		var domain = _exit_link_modal.get_domain(url);
		var celgene_parts = celgene.split(".");
		if( celgene_parts.length === 3 ) {
			celgene = celgene_parts[1] + "." + celgene_parts[2];
		}
		var domain_parts = domain.split(".");
		if( domain_parts.length === 3 ) {
			domain = domain_parts[1] + "." + domain_parts[2];
		}
		if( domain == celgene || "#" === domain || domain.indexOf("javascript:") >= 0 || domain.indexOf("mailto:") >= 0 || domain.indexOf("tel:") >= 0 || domain == "" ) {
			return false;
		}
		return true;
	}

}
//waitForJquery(function() {
    jQuery(document).ready(function($) {
        _exit_link_modal.document_init($);

        /**
         * Lloyd McGarrigal
         * Wed Feb 25 16:34:50 2015
         *
         * window_init called at end of document_init function
         * see above
         */

        // App.EventEmitter.on('window:load', function() {
        //      _exit_link_modal.window_init($);
        // });
        // $(window).load(function() {

        // });
    });
//});

