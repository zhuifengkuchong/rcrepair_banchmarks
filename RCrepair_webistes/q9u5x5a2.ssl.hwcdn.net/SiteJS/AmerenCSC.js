// Allow domain to be set for eCUStomer windows management - don't allow if in edit mode to
// avoid interference with Telerik control ... IS_EDIT_MODE js variable set in ameren.master
if (!window.IS_EDIT_MODE && new String(window.location).indexOf("_layouts") == -1)
    document.domain = 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ameren.com';

/***********************************************************************************************
/* Custom jQuery filter
/***********************************************************************************************/
jQuery.expr[':'].regex = function (elem, index, match) {
    var matchParams = match[3].split(','),
        validLabels = /^(data|css):/,
        attr =
        {
            method: matchParams[0].match(validLabels) ?
                matchParams[0].split(':')[0] : 'attr',

            property: matchParams.shift().replace(validLabels, '')
        },
        regexFlags = 'ig',
        regex = new RegExp("^" + matchParams.join('').replace(/^\s+|\s+$/g, ''), regexFlags);


    return regex.test(jQuery(elem)[attr.method](attr.property));
}


/***********************************************************************************************
/*
/* interceptUrls is an array of objects which store information about the urls that need to
/* be intercepted.
/*
/* The object's properties are as follows:
/*
/* lookupUrl (required) -- The url to be intercepted.
/*
/* windowName (required when popWindow = true) -- The name of the window. This keeps the links
/*                                                opening in the same window.
/*
/* width (required when popWindow = true) -- The width of the window when it opens
/*
/* height (required when popWindow = true) -- The height of the window when it opens
/*
/* popWindow (required) -- A boolean flag that states whether the link should open in a new window
/*
/* hostnameMapping (required) -- Acts as a hash to determine which environment to open the app in.
/*                               The 'exception' hash is used when the url that the page is being
/*                               viewed on does not match any of the other hash urls.
/*
/**********************************************************************************************/
var interceptUrls =
[
    { // agent home

        lookupUrl: "https://q9u5x5a2.ssl.hwcdn.net/SiteJS/www2.ameren.com/prot/agent/AgentHome.aspx",

        windowName: "AmereneCustomerAgentHomeWindow",

        width: 800,

        height: 600,

        popWindow: true,

        hostnameMapping:
        {
            'exception': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/www2.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.amerensc.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.authoring.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.authoring.amerensc.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringdev.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa.Ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa.Ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat2.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat3.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat4.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa4.Ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoring.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa.Ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/staging.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa.Ameren.com'
        }
    },
    { //eCustomer Fiserv Connect

        lookupUrl: "www2.ameren.com/prot/fiservconnect",

        windowName: "AmereneCustomerFiservWindow",

        width: 800,

        height: 600,

        popWindow: true,

        hostnameMapping:
        {
            'exception': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/www2.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.amerensc.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.authoring.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.authoring.amerensc.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringdev.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa.Ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa.Ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat2.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat3.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat4.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa4.Ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoring.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa.Ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/staging.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa.Ameren.com'
        }
    },
    {   // eCustomer Log out link

        lookupUrl: "https://q9u5x5a2.ssl.hwcdn.net/SiteJS/www2.ameren.com/logout.aspx",

        popWindow: false,

        hostnameMapping:
        {
            'exception': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/www2.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.amerensc.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.authoring.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.authoring.amerensc.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringdev.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa.Ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa.Ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat2.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat3.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat4.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa4.Ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoring.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa.Ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/staging.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa.Ameren.com'
        }
    },
    { //eCustomer Residential TurnOn

        lookupUrl: "www2.ameren.com/prot/Common/TurnOn/Index",

        width: 800,

        height: 600,

        popWindow: false,

        hostnameMapping:
        {
            'exception': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/www2.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust2.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.amerensc.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust2.qa.ameren.com'
        }
    },

        { //eCustomer TurnOnCommercial

            lookupUrl: "https://q9u5x5a2.ssl.hwcdn.net/SiteJS/www2.ameren.com/business/adTurnOnSvcBus.aspx",

            width: 800,

            height: 600,

            popWindow: false,

            hostnameMapping:
            {
                'exception': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/www2.ameren.com',

                'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust2.qa.ameren.com',
                'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.amerensc.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust2.qa.ameren.com'
            }
    },

        { //eCustomer Residential TurnOff

            lookupUrl: "www2.ameren.com/prot/residential/TurnOnOff/toodefault.aspx\\?ServiceType=TurnOff",

            width: 800,

            height: 600,

            popWindow: false,

            hostnameMapping:
            {
                'exception': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/www2.ameren.com',

                'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust2.qa.ameren.com',
                'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.amerensc.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust2.qa.ameren.com'
            }
    },

        { //eCustomer Residential PendingOrders

            lookupUrl: "www2.ameren.com/prot/residential/TurnOnOff/toodefault.aspx\\?ServiceType=Maintain",

            width: 800,

            height: 600,

            popWindow: false,

            hostnameMapping:
            {
                'exception': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/www2.ameren.com',

                'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust2.qa.ameren.com',
                'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.amerensc.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust2.qa.ameren.com'
            }
    },

        { //eCustomer PropertyManagement ManageProperties

            lookupUrl: "www2.ameren.com/prot/propertymanagement/manageproperties",

            width: 800,

            height: 600,

            popWindow: false,

            hostnameMapping:
            {
                'exception': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/www2.ameren.com',

                'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust2.qa.ameren.com',
                'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.amerensc.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust2.qa.ameren.com'
            }
    },
        { //eCustomer PropertyManagement ManageAgreements

            lookupUrl: "www2.ameren.com/prot/propertymanagement/manageagreements",

            width: 800,

            height: 600,

            popWindow: false,

            hostnameMapping:
            {
                'exception': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/www2.ameren.com',

                'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust2.qa.ameren.com',
                'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.amerensc.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust2.qa.ameren.com'
            }
    },
    {   // forms.ameren.com

        lookupUrl: "https://q9u5x5a2.ssl.hwcdn.net/SiteJS/forms.ameren.com",

        windowName: "AmereneFormsWindow",

        width: 800,

        height: 99999,

        popWindow: true,

        hostnameMapping:
        {
            'exception': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/forms.Ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringdev.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/formsdev.Ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/formsuat.Ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat2.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/formsuat.Ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat3.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/formsuat.Ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat4.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/formsuat.Ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoring.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/formsauthoring.Ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/staging.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/formsuat.Ameren.com'
        }
    },

    {   // www2.ameren.com

        lookupUrl: "https://q9u5x5a2.ssl.hwcdn.net/SiteJS/www2.ameren.com",

        windowName: "AmereneCustomerWindow",

        width: 800,

        height: 600,

        popWindow: false,

        hostnameMapping:
        {
            'exception': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/www2.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.uat.amerensc.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.authoring.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sc.authoring.amerensc.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringdev.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa.Ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa.Ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat2.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat3.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecust.qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat4.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa4.Ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoring.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa.Ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/uat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustuat.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/staging.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ecustqa.Ameren.com'
        }
    },

    {   // /apps

        lookupUrl: "/apps",

        windowName: "AmerenNoneCustomerWindow",

        width: 800,

        height: 600,

        popWindow: true,

        hostnameMapping:
        {
            'exception': '/apps',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringdev.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/adcstaging.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/adcstaging.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat2.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/adcstaging.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat3.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/adcstaging.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat4.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/adcstaging.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoring.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/adcstaging.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/staging.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/adcstaging.ameren.com'
        }
    },

    {   // www.ameren.com/apps

        lookupUrl: "www.ameren.com/apps",

        windowName: "AmerenNoneCustomerWindow",

        width: 800,

        height: 600,

        popWindow: true,

        hostnameMapping:
        {
            'exception': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/www.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringdev.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/adcstaging.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/adcstaging.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat2.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/adcstaging.Ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat3.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/adcstaging.Ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat4.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/adcstaging.Ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoring.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/adcstaging.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/staging.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/adcstaging.ameren.com'
        }
    },
    {   // apps.ameren.com/Outage

        lookupUrl: "apps.ameren.com/outage",

        windowName: "AmerenNoneCustomerWindow",

        width: 800,

        height: 600,

        popWindow: false,

        hostnameMapping:
        {
            'exception': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/apps.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringdev.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/apps.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/apps.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat2.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/apps.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat3.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/apps.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat4.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/apps.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoring.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/apps.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/staging.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/apps.ameren.com'
        }
    },
    {   // apps.ameren.com

        lookupUrl: "https://q9u5x5a2.ssl.hwcdn.net/SiteJS/apps.ameren.com",

        windowName: "AmerenNoneCustomerWindow",

        width: 800,

        height: 600,

        popWindow: true,

        hostnameMapping:
        {
            'exception': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/apps.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringdev.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/adcstaging.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/apps.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat2.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/apps.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat3.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/apps.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat4.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/apps.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoring.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/adcstaging.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/staging.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/adcstaging.ameren.com'
        }
    },

    {   // ameren.mediaroom.com

        lookupUrl: "https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ameren.mediaroom.com",

        windowName: "AmerenPRNewsWindow",

        width: 800,

        height: 600,

        popWindow: true,

        hostnameMapping:
        {
            'exception': "https://q9u5x5a2.ssl.hwcdn.net/SiteJS/ameren.mediaroom.com",

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringdev.ameren.com': 'stage2.mediaroom.com/ameren',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat.ameren.com': 'stage2.mediaroom.com/ameren',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoring.ameren.com': 'stage2.mediaroom.com/ameren',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/staging.ameren.com': 'stage2.mediaroom.com/ameren'
        }
    },

    {   // phx.corporate-ir.net

        lookupUrl: "https://q9u5x5a2.ssl.hwcdn.net/SiteJS/phx.corporate-ir.net",

        width: 800,

        height: 600,

        popWindow: false,

        hostnameMapping:
        {
            'exception': "https://q9u5x5a2.ssl.hwcdn.net/SiteJS/phx.corporate-ir.net",

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringdev.ameren.com': 'phx.corporate-ir.net/preview',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat.ameren.com': 'phx.corporate-ir.net/preview',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoring.ameren.com': 'phx.corporate-ir.net/preview',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/staging.ameren.com': 'phx.corporate-ir.net/preview'
        }
    },

    {   // sign-in-secure3.ameren.com

        lookupUrl: "https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sign-in-secure3.ameren.com",

        windowName: "AmereneCustomerWindow",

        width: 800,

        height: 600,

        popWindow: true,

        hostnameMapping:
        {
            'exception': "https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sign-in-secure3.ameren.com",

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringdev.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sis3qa.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sis3qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat2.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sis3qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat3.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sis3qa.ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat4.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sis3qa.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoring.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sis3qa.ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/staging.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/sis3qa.ameren.com'
        }
    },
    {   // gp.ameren.com - Government Portal

        lookupUrl: "https://q9u5x5a2.ssl.hwcdn.net/SiteJS/gp.ameren.com",

        windowName: "AmerenGovernmentPortalWindow",

        width: 1024,

        height: 768,

        popWindow: true,

        hostnameMapping:
        {
            'exception': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/gp.Ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringdev.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/gpdev.Ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/gpqa.Ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat2.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/gpqa.Ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat3.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/gpqa.Ameren.com',
            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoringuat4.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/gpqa.Ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/authoring.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/gpqa.Ameren.com',

            'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/staging.ameren.com': 'https://q9u5x5a2.ssl.hwcdn.net/SiteJS/gpqa.Ameren.com'
        }
    }

];

$(document).ready(function () {

    var clickCount = 0;		//Kludge alert: jquery 1.7.2 fires two click events for every link we're handling, we will process only the even numbered ones

    for (var i = 0; i < interceptUrls.length; i++) {
        (function () {
            var interceptUrl = interceptUrls[i],

                lookup = interceptUrl.lookupUrl;

            if (interceptUrl.lookupUrl && interceptUrl.lookupUrl.length) {

                if (lookup.substr(0, 1) != "/") {
                    lookup = "http(s)?:\/\/" + lookup;
                }

                $('a:regex(href,' + lookup + ')').on('click', function (e) {
                    e.preventDefault();

                    //handle only even numbered click events
                    if (clickCount % 2 == 0) {
                        if (interceptUrl.popWindow) {
                            OpenDialog($(this).attr('href'), interceptUrl);
                        }
                        else {
                            var url = GetUrl($(this).attr('href'), interceptUrl.hostnameMapping);

                            window.location = url;
                        }
                    }
                    clickCount++;
                    return false; // return false to prevent any other handlers from executing
                });

                $('area:regex(href,' + lookup + ')').on('click', function (e) {
                    e.preventDefault();

                    //handle only even numbered click events
                    if (clickCount % 2 == 0) {
                        if (interceptUrl.popWindow) {
                            OpenDialog($(this).attr('href'), interceptUrl);
                        }
                        else {
                            var url = GetUrl($(this).attr('href'), interceptUrl.hostnameMapping);

                            window.location = url;
                        }
                    }
                    clickCount++;
                    return false; // return false to prevent any other handlers from executing
                });

            }

        })();
    }
});


/***********************************************************************************************
/*
/* OpenDialog(string url, object interceptUrl)
/*
/* url: The url of the page which the window will display.
/*
/* interceptUrl: An object like the one described in the interceptUrls array above.
/*
/**********************************************************************************************/
function OpenDialog(url, interceptUrl) {
    var pos = GetCenterPos(interceptUrl.width, interceptUrl.height);

    var features =
	{
	    left: pos.left,

	    top: pos.top,

	    width: interceptUrl.width,

	    height: interceptUrl.height
	};

    if (interceptUrl.additionalFeatures && interceptUrl.additionalFeatures instanceof Object) {
        features = $.extend({}, features, interceptUrl.additionalFeatures);
    }

    features = GetFeaturesString(features);

    url = GetUrl(url, interceptUrl.hostnameMapping);

    var win = window.open(url, interceptUrl.windowName, features);

    win.focus();
}

function GetFeaturesString(options) {
    var defaults =
    {
        width: '800',
        height: '600',
        location: 'no',
        menubar: 'no',
        status: 'yes',
        toolbar: 'no',
        scrollbars: 'yes',
        resizable: 'yes',
        left: 0,
        top: 0
    };

    var settings = $.extend({}, defaults, options),

			        features = [];

    for (var setting in settings) {
        features.push(setting + "=" + settings[setting]);
    }

    return features.join(", ");
}

function GetCenterPos(w, h) {
    var pos = {};

    if (h > screen.height) h = screen.height;

    pos.left = Math.round((screen.width / 2) - (w / 2));

    pos.top = Math.round((screen.height / 2) - (h / 2));

    return pos;
}

function GetUrl(url, hostnameMapping) {
    var hostname = window.location.hostname; // www.example.com

    // If not on www.ameren.com
    if (hostname.substr(0, 3) != "www") {
        var urlParts = url.split('/');

        var curHostName = "";

        if (urlParts[0].indexOf("http") == -1)
            curHostName = urlParts[0]; // Example: www2.ameren.com/path/to/app.aspx
        else
            curHostName = urlParts[2]; // Example: http://www2.ameren.com/path/to/app.aspx

        var newHostName = hostnameMapping[hostname];

        if (!newHostName)
            newHostName = hostnameMapping["exception"];

        if (url.substr(0, 1) == "/") {
            url = "http://" + newHostName + url;
        }
        else {
            url = url.replace(curHostName, newHostName);
        }
    }
    else if (url.substr(0, 5) == "/apps") {
        url = "http://apps.ameren.com" + url;
    }

    return url;
}