var _amClone = null;

(function ($) {
    $(document).ready(function () {
        if (!window.IS_EDIT_MODE) {
            $('.collapseImage').live('collapseImageLoaded', function () {
                collapseImage($(this));
            });

            $('.collapseText').live('collapseTextLoaded', function () {
                collapseText($(this));
            });

            $('.collapseImage').trigger('collapseImageLoaded');

            $('.collapseText').trigger('collapseTextLoaded');
        }

        $('.chicletImage').css('position', 'static').parent().css('position', 'static');
    });


    $(window).bind('load', function () {
        CalculateLeftNavHeight();
    });

    function collapseImage(elem) {
        if (!elem.find('img').length) {
            elem.remove();
        }
    }

    function collapseText(elem) {
        if (!/[a-zA-Z]/.test(elem.text())) {
            elem.remove();
        }
    }

    $(document).ready(function () {
        // If header does not have a link, make it gray, bolder, and add padding to the top
        $('a.ameren-navheader').each(function () {
            var link = $(this);

            if (!link.attr('href') || link.attr('href') == '#') {
                link.parent().css('padding-top', '8px');

                link.addClass('header-no-link');
            }
        });

        //This Function adds selected CSS style to proper Left Nav Link (for Sitecore site)
        $('.ameren-navheader').each(function () {
            var windowLink = parseURL(String(window.location));

            if ($(this).attr('href') == windowLink.relative) {
                $(this).addClass('ameren-selectednavheader');
                // For ie
                $(this).css('color', 'rgb(244, 238, 54)');
            }
        });
        $('.ameren-navitem').each(function () {
            var windowLink = parseURL(String(window.location));

            if ($(this).attr('href') == windowLink.relative) {
                $(this).addClass('ameren-selectednav');
                // For ie
                $(this).css('color', 'rgb(244, 238, 54)');
            }
        });

        // For sub pages of sub sites, add a dash to the text, padding to the left, and hide the container.
        $('a.ameren-navitem').each(function () {
            $(this).parents('table[class^=ameren-navSubMenu]')
                   .parents('tr')
                   .hide();

            $(this).text("- " + ($(this).text()));
        });

        // If Subsite is the selected item, show it's sub pages
        var selectedNavHeader = $('a[class~="ameren-selectednavheader"]');

        if (selectedNavHeader.length) {
            selectedNavHeader.parents('table.ameren-navheader')
                             .parents('tr')
                             .next()
                             .show();
        }

        // If a sub page of a subsite is selected, highlight the subsite (breadcrumb)
        var navItem = $('.ameren-selectednav');

        if (navItem.length) {
            //$('a.ameren-navitem').show();

            navItem.parents('table[class^=ameren-navSubMenu]')
                   .parents('tr')
                   .show()
                   //.prev()
                   //.find('a')
                   //.addClass('ameren-selectednavheader');
        }

        $('#leftNavWrapper').show();
    });

    $(document).ready(function () {
        $('.jsHide').hide();

        //This function controls the main menu submenu's (Hide / Show / Proper Positioning)
        $('li.ameren-topNav').each(function () {
            var MainDropMenuID = $(this).attr('id').replace("MainMenuLink_", "MainDropMenu_");

            //Set the "Selected" Top Menu item.
            if (isCurrentPage($(this).children('a').attr('href'))) {
                $(this).addClass('ameren-topNavSelected');
                // For ie
                $(this).children('a').css('color', 'rgb(36, 147, 60)');
            }

            $(this).hover(
                function () {
                    //The menu's that open at the end have to be offset properly.
                    if ($('#' + MainDropMenuID).hasClass('ameren-topNavFlyOutsLeft')) {
                        var left;
                        left = (-217 + $(this).width()) + 'px';

                        $('#' + MainDropMenuID).css("left", left);
                    }

                    ShowDiv(MainDropMenuID);
                }, function () {
                    HideDiv(MainDropMenuID);
                });
        });
    });

    function ShowDiv(divID) {
        $("#" + divID).show();
    }

    function HideDiv(divID) {
        $("#" + divID).hide();
    }

    function buildDropDown(item, flyoutRight, depth) {
        depth = depth || 0;

        var itemsDiv = $('#' + item.attr('id') + 'Items'),

            paddingLeft = (depth * 4),

            dropDown = null;

        if (!itemsDiv.length)
            return [];

        dropDown = $("<ul></ul>").attr('id', itemsDiv.attr('id'));

        $('tr[id^=zz1_]', itemsDiv).each(function (i) {
            var link = $(this).find('a'),

                dropDownItem = dropDown.append('<li></li>').children('li:eq(' + i + ')'),

                subDropDown = buildDropDown($(this), flyoutRight, depth + 1);

            if (depth == 0) {
                if (flyoutRight)
                    dropDownItem.addClass('ameren-topNavFlyOutRightItem');
                else
                    dropDownItem.addClass('ameren-topNavFlyOutLeftItem');
            }

            if (depth > 1) {
                dropDownItem.css('padding-left', paddingLeft).html('&nbsp;-&nbsp;');
            }

            dropDownItem.append(link.clone());

            if (!link.attr('href') || link.attr('href') == "#") {
                dropDownItem.css('padding-top', '5px')
                            .children('a')
                            .css({ fontWeight: 'bold', color: 'rgb(80, 80, 80)', cursor: 'default' });
            }

            if (subDropDown.length) {
                dropDownItem.append(subDropDown)
            }
        });

        if (depth == 0) {
            if (flyoutRight) {
                dropDown.prepend('<li><img src="Unknown_83_filename"/*tpa=https://q9u5x5a2.ssl.hwcdn.net/Style Library/AmerenBranding/Images/topNavFlyoutRightTop.png*/ /></li>');

                dropDown.append('<li style="position: relative; top: -3px">' +
                                    '<img src="Unknown_83_filename"/*tpa=https://q9u5x5a2.ssl.hwcdn.net/Style Library/AmerenBranding/Images/topNavFlyoutRightBottom.png*/ />' +
                                '</li>');
            }
            else {
                dropDown.prepend('<li><img src="Unknown_83_filename"/*tpa=https://q9u5x5a2.ssl.hwcdn.net/Style Library/AmerenBranding/Images/topNavFlyoutLeftTop.png*/ /></li>');

                dropDown.append('<li style="position: relative; top: -3px">' +
                                    '<img src="Unknown_83_filename"/*tpa=https://q9u5x5a2.ssl.hwcdn.net/Style Library/AmerenBranding/Images/topNavFlyoutLeftBottom.png*/ />' +
                                '</li>');
            }
        }

        return dropDown;
    }

    function parseURL(url) {

        var a = document.createElement('a');

        a.href = url;

        return {

            source: url,

            protocol: a.protocol.replace(':', ''),

            host: a.hostname,

            port: a.port,

            query: a.search,

            params: (function () {
                var ret = {},
                    seg = a.search.replace(/^\?/, '').split('&'),
                    len = seg.length, i = 0, s;
                for (; i < len; i++) {
                    if (!seg[i]) { continue; }
                    s = seg[i].split('=');
                    ret[s[0]] = s[1];
                }
                return ret;
            })(),

            file: (a.pathname.match(/\/([^\/?#]+)$/i) || [, ''])[1],
            hash: a.hash.replace('#', ''),
            path: a.pathname.replace(/^([^\/])/, '/$1'),
            relative: (a.href.match(/tps?:\/\/[^\/]+(.+)/) || [, ''])[1],
            segments: a.pathname.replace(/^\//, '').split('/')
        };
    }

    function isCurrentPage(link) {
        var windowLink = parseURL(String(window.location));
        var aLink = parseURL(link);

        if (window.location.pathname.toLowerCase() == link.toLowerCase()) {
            return true;
        }
        else {
            if (windowLink.segments[0].toLowerCase() == "missouri" || windowLink.segments[0].toLowerCase() == "illinois") {
                if (aLink.segments.length > 1 && windowLink.segments.length > 1) {
                    return aLink.segments[1].toLowerCase() == windowLink.segments[1].toLowerCase();
                }
            }
            else {
                return aLink.segments[0].toLowerCase() == windowLink.segments[0].toLowerCase();
            }
        }

        return false;
    }

})(jQuery);

function PromiseLogoError(obj) {
    obj.style.display = "none";
}

//Not Necessary for Sitecore Implementation.
//$(document).ready(function () {
//    //promiseLogo link on /sites/* goes to application root site
//    var promiseHref = 'https://q9u5x5a2.ssl.hwcdn.net/' + $('#promiseWrapper a').attr('href');
//    if (promiseHref.indexOf('sites') != 0) {
//        $('#promiseWrapper a').attr('href', promiseHref.substring(promiseHref.indexOf('/pages')));
//    }
//});

$(document).ready(function () {
    //invoke browser bookmark functionality
    $(".pageBookmark").jBrowserBookmark();
});

$(document).ready(function () {
    //only show Alert if it has message text
    var alertTextString = $('.alertText').text();
    if (alertTextString.length > 38) {
        $('.alertContent').toggle();
    }
});


//QUICKLINKS JS for "NEW" QuickLinks implemented with Sitecore Re-Platform
$(document).ready(function () {
    $(".quickLinks").each(function () {
        var origHeight = 0;
        var expandedHeight = 0;
        var quickLinksCount = $(this).find("li").length;

        var ArrayItemHeights = new Array(quickLinksCount);

        var i = 0;

        $(this).find("li").each(function () {
            ArrayItemHeights[i] = $(this).height() + 20; //+ 20 is for padding
            expandedHeight += ArrayItemHeights[i];
            i++;
        });

        if (quickLinksCount < 5) {
            for (i = 0; i < quickLinksCount; i++) {
                origHeight += ArrayItemHeights[i];
            }
        }
        else {
            for (i = 0; i < 5; i++) {
                origHeight += ArrayItemHeights[i];
            }
        }
        $(this).height(origHeight);

        if (quickLinksCount <= 5) {
            $(this).parent().find(".quickLinksFooter").hide();
        }

        //QuickLinks expand/collapse arrow
        $(this).parent().find(".qlButton").click(function () {
            //The user collapsing the QuickLinks
            if ($.trim($(this).attr('src')) == "../-/media/Common-Assets/Images/Button-Images/QuickLinks_ButtonUp.gif"/*tpa=https://q9u5x5a2.ssl.hwcdn.net/-/media/Common-Assets/Images/Button-Images/QuickLinks_ButtonUp.gif*/) {
                $(this).parent().siblings(".quickLinks").animate({ height: origHeight }, 500);
                $(this).attr('src', '../-/media/Common-Assets/Images/Button-Images/QuickLinks_ButtonDown.gif'/*tpa=https://q9u5x5a2.ssl.hwcdn.net/-/media/Common-Assets/Images/Button-Images/QuickLinks_ButtonDown.gif*/);
                $(this).attr('alt', 'More');
            }
            else {//The user is expanding the QuickLinks
                var newHeight = expandedHeight;
                $(this).parent().siblings(".quickLinks").animate({ height: newHeight }, 500);
                $(this).attr('src', '../-/media/Common-Assets/Images/Button-Images/QuickLinks_ButtonUp.gif'/*tpa=https://q9u5x5a2.ssl.hwcdn.net/-/media/Common-Assets/Images/Button-Images/QuickLinks_ButtonUp.gif*/);
                $(this).attr('alt', 'Less');
            }
            return true;
        });
    });
});
//END QUICKLINKS JS for "NEW" QuickLinks implemented with Sitecore Re-Platform

//FAQS JS for "NEW" FAQs implemented with Sitecore Re-Platform
$(document).ready(function () {
    //User click to expand a specific FAQ item
    $('li[id^="faq_Title_"]').click(function () {
        var sId = this.id;
        var sItemID = sId.split('_')[2];
        var sNum = sId.split('_')[3];

        //The user opening the FAQ
        if ($("#faq_View_" + sItemID + '_' + sNum).html() == 'View') {
            $(this).addClass("faqselected");
            $("#faq_View_" + sItemID + '_' + sNum).addClass("faqclose");
            $("#faq_View_" + sItemID + '_' + sNum).html('Close');
            $("#faq_Answer_" + sItemID + '_' + sNum).removeClass('faqhidden');
            $("#faq_Answer_" + sItemID + '_' + sNum).addClass('faqdisplay');
        }
        else { //The user is closing the FAQ
            $(this).removeClass("faqselected");
            $("#faq_View_" + sItemID + '_' + sNum).removeClass("faqclose");
            $("#faq_View_" + sItemID + '_' + sNum).html('View');
            $("#faq_Answer_" + sItemID + '_' + sNum).removeClass('faqdisplay');
            $("#faq_Answer_" + sItemID + '_' + sNum).addClass('faqhidden');
        }

        return true;
    });

});
//END FAQS JS for "NEW" FAQs implemented with Sitecore Re-Platform

//CALENDAR JS for "NEW" Calendar Page implemented with Sitecore Re-Platform
function SwitchCalendarMonth(link) {
    var values = $("#" + link.id).attr("rel").split("_*_");
    var URL = values[0];
    var NewMonth = values[1];
    var NewYear = values[2];
    var CurrentView = $('#CurrentCalendarView').val();

    $.get(URL + "?Month=" + NewMonth + "&Year=" + NewYear, function (data) {
        var calendarcontainer = $('.calendarcontainer', $(data)); //Parse returned HTML to get to just the data for the calendar.
        $(".calendarcontainer").html(calendarcontainer.html()); //Replace the current calendar data with the "new" calendar data.
        $('#CurrentCalendarView').val(CurrentView);
        SetUpCalendarJS(); //This is important to re-initialize the JS on the cnew alendar data.
    });
}

function SwitchCalendarView(NewView) {
    if (NewView == "Calendar") {
        //Remove the selected class and hide the list
        $("#calListButton").removeClass('selected');
        $("#listView").hide();
        //Add the selected class and show the calendar
        $("#calViewButton").addClass('selected');
        $("#calendarView").show();
        $('#CurrentCalendarView').val("Calendar");
    }
    else {
        //Remove the selected class and hide the calendar
        $("#calViewButton").removeClass('selected');
        $("#calendarView").hide();
        //Add the selected class and show the list
        $("#calListButton").addClass('selected');
        $("#listView").show();
        $('#CurrentCalendarView').val("List");
    }
}

function SetUpCalendarJS() {
    var currentLink = '';
    var CurrentView = $('#CurrentCalendarView').val();

    //Calender Previous Month button click
    $("#calPrevButton").click(function () {
        SwitchCalendarMonth(this);

        return false;
    });

    //Calender Next Month button click
    $("#calNextButton").click(function () {
        SwitchCalendarMonth(this);

        return false;
    });

    //Calender View button click
    $("#calViewButton").click(function () {
        SwitchCalendarView("Calendar");

        return false;
    });

    //Calender List button click
    $("#calListButton").click(function () {
        SwitchCalendarView("List");

        return false;
    });

    $(".infoModalClose").click(function () {
        $(".infoModal").attr('style', 'display:none;');
        return false;
    });

    $(".dayevents a").click(function () {
        var sId = this.id;
        var iRow = parseInt(sId.split('_')[0]);
        var iCol = parseInt(sId.split('_')[1]);
        var iLink = parseInt(sId.split('_')[2]);
        var xOffset;
        var yOffset;
        var linkPosition = $("#" + sId).position();

        //Is the same link being clicked, if so then hide the modal
        if (currentLink == sId) {
            $(".infoModal").attr('style', 'display:none;');
            currentLink = "";
        }
        else {
            $("#infoModalArrow").attr('class', '');

            //Depending on the column add the correct arrow and class to position the arrow
            if (iCol >= 4) {
                $("#infoModalArrow").addClass('infomodalarrowright');
                $("#infoModalArrow").attr('src', '/-/media/Common-Assets/Images/Component-Structural-Images/InfoModalArrowRight');
            }
            else {
                $("#infoModalArrow").addClass('infomodalarrowleft');
                $("#infoModalArrow").attr('src', '/-/media/Common-Assets/Images/Component-Structural-Images/InfoModalArrowLeft');
            }

            //Populate values for
            $("#infoModal #EventNameHeader").html($("#EventName_" + sId).val());
            $("#infoModal #EventNameTitle").html($("#EventName_" + sId).val());
            $("#infoModal #EventDescription").html($("#EventDescription_" + sId).val());
            $("#infoModal #EventLocation").html($("#EventLocation_" + sId).val());

            //Correctly locate the modal depending on the row and column
            switch (iRow) {
                case 0:
                    yOffset = -8 + linkPosition.top + 'px;';
                    break;
                case 1:
                    yOffset = 170 + linkPosition.top + 'px;';
                    break;
                case 2:
                    yOffset = 345 + linkPosition.top + 'px;';
                    break;
                case 3:
                    yOffset = 525 + linkPosition.top + 'px;';
                    break;
                case 4:
                    yOffset = 695 + linkPosition.top + 'px;';
                    break;
                case 5:
                    yOffset = 870 + linkPosition.top + 'px;';
                    break;
                default:
                    yOffset = 10 + linkPosition.top + 'px;';
            }
            switch (iCol) {
                case 0:
                    xOffset = '140px;';
                    break;
                case 1:
                    xOffset = '240px;';
                    break;
                case 2:
                    xOffset = '340px;';
                    break;
                case 3:
                    xOffset = '440px;';
                    break;
                case 4:
                    xOffset = '100px;';
                    break;
                case 5:
                    xOffset = '200px;';
                    break;
                case 6:
                    xOffset = '300px;';
                    break;
                default:
                    xOffset = '140px;';
            }

            $("#infoModal").attr('style', 'display:block;left:' + xOffset + ';top:' + yOffset + ';');
            currentLink = sId;
        }

        return false;
    });

    $(".dayevents a").click(function () {

        return false;
    });

    $(function () {
        $('.scroll-pane').jScrollPane({
            verticalDragMinHeight: 16,
            verticalDragMaxHeight: 16
        });
    });

    //Set current Calendar View on Page Load
    SwitchCalendarView(CurrentView);
}

$(document).ready(function () {
    SetUpCalendarJS(); //Initialize Calendar JS on initial Page Load
});
//END CALENDAR JS for "NEW" Calendar Page implemented with Sitecore Re-Platform

function alertBoxClose() {
    $('.alertContent').remove();
}

function resizeImg(dimensions, imgSelector) {

    if (jQuery(imgSelector).length > 0) {

        var img = jQuery(imgSelector).find('img');

        img.each(function () {

            var curImg = jQuery(this);

            var aImgWidth = curImg.width();
            var aImgHeight = curImg.height();

            if (dimensions.width && dimensions.height) {


                if (aImgHeight > dimensions.height) {

                    curImg.css('height', dimensions.height + 'px');
                }

                if (aImgWidth > dimensions.width) {

                    curImg.css('width', dimensions.width + 'px');
                }
            }
            else if (dimensions.width) {

                if (aImgWidth > dimensions.width) {

                    var newHeight = (dimensions.width / aImgWidth) * aImgHeight;

                    curImg.css('width', dimensions.width + 'px');

                    curImg.css('height', newHeight + 'px');
                }
            }
            else if (dimensions.height) {

                if (aImgHeight > dimensions.height) {

                    var newWidth = (dimensions.height / aImgHeight) * aImgWidth;

                    curImg.css('height', dimensions.height + 'px');

                    curImg.css('width', newWidth + 'px');
                }
            }
        });
    }
}

//Dynamically Calculates height of Ameren.com Left Nav based on Page Content length.
function CalculateLeftNavHeight() {
    var defaultRightHeight = 540,

        defaultLeftNavHeight = 100,

        curRightHeight = $('#contentBodyRight').height(),

        curLeftHeight = $('#contentBodyLeft').height(),

        difference = curRightHeight - curLeftHeight,

        curNavHeight = $('#leftNavigation').height();

    if (difference > 0) {
        //var height = curNavHeight + (difference - (curNavHeight - defaultLeftNavHeight));
        var height = curNavHeight + difference;

        if (height != curNavHeight) // The nav needs to be adjusted
        {
            $('#leftNavigation').css('height', height + 'px');
        }
    }
}


//Controls Info Link component on Component Page templates.
$(document).ready(function () {
    $(".infoLinkButton").fancybox({
        'width': 780,
        'height': '75%',
        'autoScale': false,
        'transitionIn': 'none',
        'transitionOut': 'none',
        'type': 'iframe'
    });

    $('*[name="modalIframe"]').fancybox({
        'width': '50%',
        'height': '50%',
        'autoScale': true,
        'transitionIn': 'none',
        'transitionOut': 'none',
        'type': 'iframe'
    });

    $('*[class="modalIframe"]').fancybox({
        'width': '50%',
        'height': '50%',
        'autoScale': true,
        'transitionIn': 'none',
        'transitionOut': 'none',
        'type': 'iframe'
    });
});

//Controls to keep Image Captions no wider than their respective image.
$(window).load(function () {
    $(".imageCaption").each(function (index) {
        var MediaURL = $(this).attr('rel');

        //First try to get image width by finding img tag in source
        var imgWidth = $("img[src='" + MediaURL + "']").width();

        if (imgWidth == undefined || imgWidth == 0) {
            //Couldn't find Image tag in source, let's try to dynamically create a JS object to get width.
            var img = $("<img id='dynamic' src='" + MediaURL + "' />"); //Equivalent: $(document.createElement('img'))

            var pic_real_width;
            $("<img/>") // Make in memory copy of image to avoid css issues
                .attr("src", MediaURL)
                .load(function () {
                    pic_real_width = this.width;   // Note: $(this).width() will not work for in memory images.
                });

            imgWidth = pic_real_width;
        }

        if (imgWidth == undefined || imgWidth == 0) {
            //Still don't have a proper width :(
            imgWidth = 200; //At this point, just default to 200px;
        }

        //Set Caption to same width as image (or default 200px)
        $(this).attr("style", "max-width:" + imgWidth + "px;");
    });
});

function SC_RunSearchQuery(scope) {
    var newHost = location.host;

    //determine if one of the other sites by state inclusion in URL
    if (location.href.indexOf(location.host + '/illinois') > -1) {
        newHost = [newHost, '/illinois'].join('');
    }
    if (location.href.indexOf(location.host + '/missouri') > -1) {
        newHost = [newHost, '/missouri'].join('');
    }
    var url = [location.protocol, '//', newHost].join('');

    //extract(location);
    this.document.location.href = url + '/search-results?q=' + document.getElementById("ctl00_PlaceHolderSearchArea_ctl01_S3031AEBB_InputKeywords").value + '&s=' + scope;
}


$(document).ready(function () {
    //If mobile device add 'Mobile Site' nav footer link
    if ($.browser.mobile || $.browser.tablet) {
        $('.footerLink.firstFooterLink').removeClass('firstFooterLink');
        $('#footerLinksList').prepend('<li class="footerLink firstFooterLink"><a href="javascript:RedirectToMobile()" target="">Mobile Site</a></li>');
    }
});
