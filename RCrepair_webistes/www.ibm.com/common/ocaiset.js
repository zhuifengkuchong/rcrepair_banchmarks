// $Id: ocaiset.js,v 1.2 2007/09/12 22:50:27 krusch Exp $

if (typeof ibmCommonCookie != 'undefined' && !!ibmCommonCookie && typeof RegExp != 'undefined') {
	(function () {
		var c = new ibmCommonCookie;
		var prefs = c.getCookie("lenovoPrefs");

		var re = /cc=(..);lc=(..)/;
		var mymatch = re.exec(prefs);

		if (!!mymatch) {
			var cc = String(mymatch[1]).toLowerCase();
			var lc = String(mymatch[2]).toLowerCase();
			var cclc = cc + '-' + lc;

			var cclcre = /ae-en|ag-en|ai-en|an-en|ar-es|at-de|au-en|aw-en|bb-en|bd-en|be-en|be-fr|be-nl|bg-bg|bh-en|bm-en|bo-es|br-pt|bs-en|ca-en|ca-fr|ch-de|ch-fr|cl-es|cn-zh|co-es|cs-sr|cy-en|cz-cs|de-de|dk-da|dm-en|ec-es|ee-et|eg-en|es-es|fi-fi|fr-fr|gd-en|gr-el|gy-en|hk-en|hr-hr|hu-hu|id-en|ie-en|il-he|in-en|it-it|jm-en|jo-en|jp-ja|kn-en|kr-ko|kw-en|ky-en|lb-en|lc-en|lk-en|lt-lt|lv-lv|ms-en|mx-es|my-en|nl-nl|no-no|nz-en|om-en|pe-es|ph-en|pk-en|pl-pl|pt-pt|py-es|qa-en|ro-ro|ru-ru|sa-en|se-sv|sg-en|si-sl|sk-sk|sr-en|tc-en|th-en|tr-tr|tt-en|tw-zh|ua-uk|uk-en|us-en|uy-es|vc-en|ve-es|vg-en|vn-en|za-en/;
			if (cclcre.exec(cclc)) {
				c.setCookie("ipcInfo", prefs, 86400 * 365 * 10, "/", ".ibm.com");
				c.setCookie("icn", "cc=" + cc + ";lc=" + lc + ";url=http://www.ibm.com/" + cc + "/" + lc + "/;status=0", 0, "/", ".ibm.com");
			}
		}
	}) ();
}
