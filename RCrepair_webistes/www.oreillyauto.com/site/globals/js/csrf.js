function wrapAjaxForCsrf(){$(document).ajaxSend(function(b,c,a){if(a.type!=="GET"&&a.type!=="HEAD"&&a.type!=="TRACE"&&a.type!=="OPTIONS"){c.setRequestHeader("X-CSRF-Token",csrfToken)
}}).ajaxComplete(function(b,c,a){setCsrfTokenByResponse(c)
})
}function tryWrapAjaxForCsrf(){if("$" in window){wrapAjaxForCsrf()
}else{setTimeout(tryWrapAjaxForCsrf,200)
}}tryWrapAjaxForCsrf();
function setCsrfTokenByResponse(b){var a=b.getResponseHeader("X-CSRF-Token");
if(a!==null&&a!==""){setCsrfToken(a)
}}function setCsrfToken(a){csrfToken=a;
$("input[type='hidden'].csrfTokenHiddenElement").val(a)
};