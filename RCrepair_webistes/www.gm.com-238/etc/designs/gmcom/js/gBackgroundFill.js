var entrada=true;
function backgroundChange(){
	
	if($("#background").find('img').length){
		var imagen;
		
		if ($("#background").find('img').length == 2) {
			imagen = $("#background").find('img').eq(1).attr('src');
		}else {
			imagen = $("#background").find('img').attr('src');
		}
		
		$("body").ezBgResize({
			img:imagen
		});
		
		if(entrada){
			$("#background").hide();
			entrada=false;
		}
		else
			$("#background").show();
	}
}

$( document ).ready( function(){	
	$("#saturn_vehicles,#hummer_vehicles").find("div#background").hide();
	if($('.subsection_image_link_container').length){		
		$('.subsection_image_link_container').find('li').eq(0).css('marginLeft','0px');
	}
});

$(window).load(function() {
	backgroundChange();
	
	$("#bottomTabContainer").find('.tabs').find("li").live('click',function(e){
		backgroundChange();
	});
	$("#bottomTabContainer").find('.bottomTabContainerNext').live('click',function(e){
		backgroundChange();
	});	
	$("#bottomTabContainer").find('.bottomTabContainerPrev').live('click',function(e){
		backgroundChange();
	});
});