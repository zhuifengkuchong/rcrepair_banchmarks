// JAVASCRIPT for MULTI FRAME SLIDER

// FUNCTION: CHANGE CIRCLE BUTTONS
function change_buttons(page){
	$('#z_nav img').attr('src', '../css/images/circle.png'/*tpa=http://www.staples.com/sbd/cre/_lib/css/images/circle.png*/);
	$('#z_nav img').eq(page).attr('src', '../css/images/circle_on.png'/*tpa=http://www.staples.com/sbd/cre/_lib/css/images/circle_on.png*/);
}

// FUNCTION: SLIDE ON TIMEOUT					
function slide(){
			
	// clear timeout
	clearTimeout($('#z_slider').data('opts').timeout);	
	
	// set new timeout
	$('#z_slider').data('opts').timeout = setTimeout(function() {			
		
		$('#z_slider').animate({
			left: '-=' + $('#z_slider').data('opts').frameWidth
			}, $('#z_slider').data('opts').speed, 'swing', function(){						
				
				$('#z_slider :first').clone().appendTo($('#z_slider'));
				$('#z_slider :first').remove();
				$('#z_slider').css('left', 0);		
						
			});	
			
			// Increment thepage to keep track of what div we are on
			thepage++;
			
			// Check what div we are on, if we are on last page, reset thepage counter to 0
			if(thepage == $('#z_slider').children('div').size()){
				thepage = 0;
			}
			
			// change circles to show active
			change_buttons(thepage);
						
			slide();			
		}, $('#z_slider').data('opts').interval);
		
}

// FUNCTION: SLIDE BACK
function slide_back(){
	
	// clear timeout
	clearTimeout($('#z_slider').data('opts').timeout);	
	
	// finish animation if already going
	$('#z_slider').stop(false, true);
	
	// clone last child and move it to the beginning
	$('#z_slider div:last').clone().insertBefore($('#z_slider :first'));
	$('#z_slider div:last').remove();
	$('#z_slider').css('left', '-940px');
	
	// animate back
	$('#z_slider').animate({
		left: '+=' + $('#z_slider').data('opts').frameWidth
		}, $('#z_slider').data('opts').speed, 'swing', function(){						
			$('#z_slider').css('left', 0);		
		});	
	
	if (thepage == 0){ thepage = $('#z_slider').children('div').size() - 1; }
	
	else{ thepage--; }
				
	// change circles to show active
	change_buttons(thepage);							
}


// FUNCTION: SLIDE FORWARD
function slide_forward(){

	// clear timeout
	clearTimeout($('#z_slider').data('opts').timeout);	
	
	// finish animation if already going
	$('#z_slider').stop(false, true);
	
	// animate forward
	$('#z_slider').animate({
			left: '-=' + $('#z_slider').data('opts').frameWidth
			}, $('#z_slider').data('opts').speed, 'swing', function(){						
				
				// clone first child and move it to the end
				$('#z_slider :first').clone().appendTo($('#z_slider'));
				$('#z_slider :first').remove();
				$('#z_slider').css('left', 0);				
			});	
	
	thepage++;
	
	if (thepage == $('#z_slider').children('div').size()){
		thepage = 0;
	}
				
	// change circles to show active
	change_buttons(thepage);							
}




// DOCUMENT READY
function z_init_multi(){
	
	// add data to slider object
	 $('#z_slider').data('opts', {
		timeout: null,
		interval: 5000,
		speed: 400,
		frame: 1,
		frameWidth: $('#z_slider div:first').width()	
	});
	
	// number of slider children 
	var numkids = $('#z_slider').children().size();
	
	// calculate and set width of z_slider (number of children x frame width)
	$('#z_slider').css('width', numkids * $('#z_slider').data('opts').frameWidth);
					
	// create circle images
	for (var x = 0; x < numkids; x++){
		var newImg = document.createElement('img');
		$(newImg).attr('height', '10');
		$(newImg).attr('width', '10');
		$(newImg).attr('src', '../css/images/circle.png'/*tpa=http://www.staples.com/sbd/cre/_lib/css/images/circle.png*/);
		$('#z_nav').append(newImg);
	}
		
	$('#z_prev').click(function(){ 
		slide_back();
	});
		
	$('#z_next').click(function(){
		slide_forward();
	});
	
	// set THEPAGE, keeps track of which frame we are on
	thepage = 0;
	
	// change circles to show active
	change_buttons(thepage);
	
	// start sliding
	slide();
}