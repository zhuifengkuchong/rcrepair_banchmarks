/*
 * homepage javascript utilities
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
  function CreateCategoryCheckboxes() {
    $(".tile-checkbox-items").empty();
    $.each(tileCategories, function(key, item) {
      $(".tile-checkbox-items").append("<li><input class='tile-category-checkbox' type='checkbox' value='" + item.tid + "' /><label></label><span>" + item.name + "</span></li>");
    });
  }

  function CreateDivisionCheckboxes() {
    $(".tile-checkbox-items").empty();
    $.each(tileDivisions, function(key, item) {
      $(".tile-checkbox-items").append("<li><input class='tile-division-checkbox' data-color='" + item.color +  "' type='checkbox' value='" + item.tid + "' /><label></label><span>" + item.name + "</span></li>");
    });
  }

  // when page finished loading
  $(document).ready(function() {

    // handle filter change
    $(".tile-select-options").change(function() {
      if ($(this).val().toLowerCase() == "categories") {
        CreateCategoryCheckboxes();
      }
      else if ($(this).val().toLowerCase() == "brands") {
        CreateDivisionCheckboxes();
      }

      tileCount = 0;
      tilePage = 0;
      tileCategoryIDs = '';
      tileDivisionIDs = '';
      SetupFirstPageTiles();

      // for mobile safari
      if ($(window).width() <= 667) {
        $("html, body").animate({ scrollTop: $("#tile-filters-form").offset().top - $("#site-header").height() });
      }
    });

    $(document).on("click touchstart", "input.tile-category-checkbox", function() {
      var newIDs = '';
      $("input.tile-category-checkbox").each(function() {
        if ($(this).is(":checked")) {
          if (newIDs.length == 0) {
            newIDs = $(this).val();
          }
          else {
            newIDs += "/" + $(this).val();
          }
        }
      });

      tileCount = 0;
      tilePage = 0;
      tileCategoryIDs = newIDs;
      tileDivisionIDs = "";
      GetTilesByAjax(1);
    });

    $(document).on("click touchstart", "input.tile-division-checkbox", function() {
      var newIDs = '';
      $("input.tile-division-checkbox").each(function() {
        if ($(this).is(":checked")) {
          if (newIDs.length == 0) {
            newIDs = $(this).val();
          }
          else {
            newIDs += "/" + $(this).val();
          }
        }
      });

      var colorCode = $(this).attr("data-color");
      if ($(this).is(":checked")) {
        $(this).parent().children("label").css("background-color", "#" + colorCode);
      }
      else {
        $(this).parent().children("label").css("background-color", "#C2C2C2");
      }

      tileCount = 0;
      tilePage = 0;
      tileCategoryIDs = "";
      tileDivisionIDs = newIDs;
      GetTilesByAjax(1);
    });

    // populate default checkboxes
    CreateDivisionCheckboxes();

    // load more
    $("a.load-more").click(function() {
      tilePage++;
      $(tileLoadMoreButton + " a").addClass("loading");
      GetTilesByAjax(1);
    });

    // populate default tiles
    SetupFirstPageTiles();

    // toggle select drop-down color on hover
    $(".tile-select-text").val($(".tile-select-options").val());
    $(".tile-select-options").change(function() {
      $(".tile-select-text").val($(this).val());
    });
    $(".tile-select-options").hover(function() {
      $(".tile-select-text").css('color','#605E5E');
    }, function() {
      $(".tile-select-text").css('color','#c2c2c2');
    });
  });
})(jQuery, Drupal, this, this.document);
