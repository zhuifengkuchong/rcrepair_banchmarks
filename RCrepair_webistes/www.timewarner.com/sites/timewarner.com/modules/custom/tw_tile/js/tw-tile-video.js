/*
 * homepage javascript utilities
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

  // parameters
  var heroVideoOn      = false;
  var heroVideoPlayer  = ".hero-video-player";
  var heroVideoWrapper = ".hero-video-player-wrapper";
  var siteHeader       = "#site-header";
  var tileList         = "#tile-list";
  var tileVideoOn      = false;
  var tileVideoPlayer  = ".tile-video-player";
  var playerID         = "jw-player";
  var animationSpeed   = 800;

  function RemoveHeroVideoPlayer() {
    //if (heroVideoOn) {
      heroVideoOn = false;
      jwplayer(playerID).remove();
      $(heroVideoWrapper + " " + heroVideoPlayer).remove();
      $(heroVideoWrapper).hide(animationSpeed);
    //}
  }

  // when page finished loading
  $(document).ready(function() {

    // launch tile video
    $(document).on("click touchstart", ".tile-play-video, .tile-play-icon", function() {
      var headerHeight = $(siteHeader).height();
      var listItemHeight = $(window).height() - headerHeight;
      var tileVideoMarkup = tileTemplates["tileVideoTemplate"];

      var myTop = 0;
      var tileNumber = 0;
      var numberOfTiles = 0;
      var listItem = "";
      var createNewPlayer = true;
      var differenceThreshold = 5; // account for minor height difference between tiles with slides and tiles without slides

      var bgColor   = $(this).attr("data-color");
      var imagePath = $(this).attr("data-image");
      var logo      = $(this).attr("data-logo");
      var skinPath  = $(this).attr("data-skin");
      var title     = $(this).attr("data-title");
      var videoPath = $(this).attr("data-video");

      var bgColorMarkup = "";
      var videoLogo     = "";
      var videoTitle    = "";

      var playerOptions = {
        autostart : true,
        file      : videoPath,
        height    : "100%",
        image     : imagePath,
        skin      : skinPath,
        width     : "100%"
      };

      if (bgColor.length > 0) {
        bgColorMarkup = "background-color:#" + bgColor + ";";
      }
      if (logo.length > 0) {
        videoLogo = "<img src='" + logo + "' />";
      }
      if (title.length > 0) {
        videoTitle = title;
      }

      tileVideoMarkup = tileVideoMarkup.replace("#BACKGROUND-COLOR#", bgColorMarkup);
      tileVideoMarkup = tileVideoMarkup.replace("#VIDEO-LOGO#", videoLogo);
      tileVideoMarkup = tileVideoMarkup.replace("#VIDEO-TITLE#", videoTitle);

      if ($(this).hasClass("tile-play-video")) {
        myTop = Math.round($(this).parent().parent().parent().parent().position().top);
      }
      else if ($(this).hasClass("tile-play-icon")) {
        myTop = Math.round($(this).parent().parent().position().top);
      }

      // figure out where to insert video player
      $(tileList + " li").each(function (index) {
        if (((Math.round($(this).position().top)) > (myTop + differenceThreshold)) && (tileNumber == 0)) {
          tileNumber = index;
        }
        numberOfTiles++;
      });

      // last tile
      if (tileNumber == 0) {
        tileNumber = numberOfTiles;
      }

      // find if a tile video player is already open
      if (tileVideoOn) {
        $(tileList + " li").each(function (index) {
          listItem = $(this).html();
          if (listItem.search(tileVideoPlayer) != -1) {
            if (index < tileNumber) {
              tileVideoOn = false;
              jwplayer(playerID).remove();
              $(this).remove();
              tileNumber--;
            }
            else if (index == tileNumber) {
              createNewPlayer = false;
            }
            else if (index > tileNumber) {
              tileVideoOn = false;
              jwplayer(playerID).remove();
              $(this).remove();
            }
          }
        });
      }

      if (heroVideoOn) {
        RemoveHeroVideoPlayer();
      }

      if (createNewPlayer) {
        $(tileList + " li:nth-child(" + tileNumber + ")").after("<li class='tile-one-per-row' style='height:" + listItemHeight + "px;'>" + tileVideoMarkup + "</li>");
        $(tileList + " " + tileVideoPlayer).slideDown(animationSpeed);
      }

      tileNumber++;

      if (!createNewPlayer) {
        $(tileList + " li:nth-child(" + tileNumber + ")").html(tileVideoMarkup);
      }

      $("html, body").animate({
        scrollTop: $(tileList + " li:nth-child(" + tileNumber + ")").offset().top - headerHeight
      }, animationSpeed);

      tileVideoOn = true;
      jwplayer(playerID).setup(playerOptions);
    });

    // stop tile video
    $(document).on("click touchstart", ".tile-video-player .video-close-icon", function() {
      var listItemTop = $(this).parent().parent().parent().offset().top;
      var halfWindowHeight = $(window).height() / 2;
      if (listItemTop > halfWindowHeight) {
        listItemTop -= halfWindowHeight;
      }

      tileVideoOn = false;
      jwplayer(playerID).remove();
      $(this).parent().parent().slideUp(animationSpeed, function() {
        $(this).parent().remove();
      });

      $("html, body").animate({ scrollTop: listItemTop }, animationSpeed);
    });

    // launch hero video
    $(document).on("click touchstart", ".hero-play-video, .hero-play-icon", function() {
      var heroVideoMarkup = tileTemplates["heroVideoTemplate"];
      var heroWrapperHeight = $(".hero-wrapper").height();
      var maxVideoHeight = $(window).height() - $(siteHeader).height();

      var bgColor   = $(this).attr("data-color");
      var imagePath = $(this).attr("data-image");
      var logo      = $(this).attr("data-logo");
      var skinPath  = $(this).attr("data-skin");
      var title     = $(this).attr("data-title");
      var videoPath = $(this).attr("data-video");

      var bgColorMarkup = "";
      var videoLogo     = "";
      var videoTitle    = "";

      var playerOptions = {
        autostart : true,
        file      : videoPath,
        height    : "100%",
        image     : imagePath,
        skin      : skinPath,
        width     : "100%"
      };

      if (bgColor.length > 0) {
        bgColorMarkup = "background-color:#" + bgColor + ";";
      }
      if (logo.length > 0) {
        videoLogo = "<img src='" + logo + "' />";
      }
      if (title.length > 0) {
        videoTitle = title;
      }

      heroVideoMarkup = heroVideoMarkup.replace("#BACKGROUND-COLOR#", bgColorMarkup);
      heroVideoMarkup = heroVideoMarkup.replace("#VIDEO-LOGO#", videoLogo);
      heroVideoMarkup = heroVideoMarkup.replace("#VIDEO-TITLE#", videoTitle);

      // find if a tile video player is already open
      $(tileList + " li").each(function (index) {
        listItem = $(this).html();
        if (listItem.search(tileVideoPlayer) != -1) {
          jwplayer(playerID).remove();
          $(this).remove();
        }
      });

      if (heroVideoOn) {
        RemoveHeroVideoPlayer();
      }

      if (maxVideoHeight < heroWrapperHeight) {
        $(heroVideoWrapper).height(maxVideoHeight);
      }

      $("html, body").animate({ scrollTop: 0 }, animationSpeed);

      heroVideoOn = true;
      $(heroVideoWrapper).html(heroVideoMarkup);
      $(heroVideoWrapper).show(animationSpeed);
      jwplayer(playerID).setup(playerOptions);
    });

    // stop hero video player
		$(document).on('touchstart', '.hero-video-player .video-close-icon', function() {
      RemoveHeroVideoPlayer();
    });
    $(document).on("click", ".hero-video-player .video-close-icon", function() {
      RemoveHeroVideoPlayer();
    });
  });

})(jQuery, Drupal, this, this.document);
