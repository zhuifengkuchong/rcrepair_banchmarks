$(function () {
    $("#navContainer a[href^=#]").addClass("nonLink").click(function (e) {
        e.preventDefault()
    }); 
	$("a[data-code]").click(function (e) {
		var str = $(this).attr("data-code");
        var str2 = $(this).attr("data-name");
		var splitCode = str.split("-",2);
		var s=s_gi(s_account); 
		s.tl($(this),'o', str2 + ' | ' + splitCode[0] + ' | ' + splitCode[1]);
	});	
	$("form#URLLocation").submit(function (e) {
		e.preventDefault();
		window.location = $('#CocLocal').val();
		
	});	
	
    $("#navContainer #navListPanel > li").each(function () {
        var $navThis = $(this);
        var $childSize = $(".navBoxList ul", $navThis).size();
        if ($childSize <= 3) {
            $(".navBox", $navThis).parent().addClass("childItems" + $childSize + "")
        }
        if ($childSize >= 5) {
            $(".navBox", $navThis).parent().addClass("largeChildItems")
        }
    });
    $("#textResizeTop a, #textResizeBtm a").each(function (e) {
        $(this).click(function (z) {
            z.preventDefault();
            $("body").attr("class", $(this).attr("class"));
            $("#textResizeTop a, #textResizeBtm a").removeClass("active");
            $(this).addClass("active")
        })
    });
    if ($(".openTT").length) {
        $("#topMenu").css("margin-bottom", "23px")
    }
    $(".footerLink li").eq(0).attr("class", "first");
    slideIn($(".contactList>a"), "fast");
    $("a.offerLink").click(function (e) {
        $(this).parent().toggleClass("activated");
        $('html, body').animate({
            scrollTop: $("#footerSecContainer").offset().top
        }, 200);
        return false
    });
    if ($("ul#navListPanel").length) {
        $("ul#navListPanel").find("li:has(div)").addClass("arrow");
        $("ul#navListPanel li ul>li").hover(function () {
            $($(this).children("#navBox3"),this).show();
            if ($(this).children().length > 1) {
                $(this).addClass("selected")
            }
        }, function () {
            $("div:eq(0)", this).hide();
            if ($(this).children().length > 1) {
                $(this).removeClass("selected")
            }
        })
    }
    initAccordMenu("#gridCenter .catAccordion:eq(0)", 0);
    initAccordMenu("#gridBottom .catAccordion:eq(0)", 0);
    initAccordMenu("#gridCenter .catAccordion:eq(1)", 0);
    initAccordMenu("#gridBottom .catAccordion:eq(1)", 0);
    initAccordMenu("#gridCenter .catAccordion:eq(2)", 0);
    initAccordMenu("#gridBottom .catAccordion:eq(2)", 0);
    initAccordMenu(".accordionModule:eq(0)", 0);
    initAccordMenu(".accordionModule:eq(1)", 0);
    initAccordMenu(".accordionModule:eq(2)", 0);
    initAccordMenu(".accCenterModule", 0);
    $(".hpsAcc").each(function () {
        var $parThis = $(this);
        $(".accExpand", $parThis).click(function (e) {
            var $childThis = $(this);
            $(".catAccordion ul", $parThis).slideDown("slow", function () {
                equalHeight($("#gridRight, #gridCenter, #gridLeft"))
            }).parent().addClass("expanded");
            $childThis.hide();
            $(".accDefault", $parThis).show();
            e.preventDefault()
        });
        $(".accDefault", $parThis).click(function (e) {
            var $childThis1 = $(this);
            $(".catAccordion ul", $parThis).slideUp("slow", function () {
                equalHeight($("#gridRight, #gridCenter, #gridLeft"))
            }).parent().removeClass("expanded");
            $childThis1.hide();
            $(".accExpand", $parThis).show();
            e.preventDefault()
        })
    });
    $("#gridRight .rightModule ul.bulleted").parent().addClass("rightBulleted");
    $("#gridRight .rightModule ul.bigBulleted").parent().addClass("rightBigBulleted");
    $("#gridRight .rightModule ul.nonBulleted").parent().addClass("rightNonBulleted");
    $("#gridRight .rightModule ul.imageBulleted").parent().addClass("rightImageBulleted");
    $("#gridRight .rightModule.gradientBg").removeClass("rightBigBulleted");
    $("#gridRight .rightModule.gradientBg ul.imageBulleted").parent().removeClass("gradientBg").removeClass("rightImageBulleted").addClass("rightGradImageBulleted");
    $("#gridRight .rightModule.rightBigBulleted.rightNonBulleted").removeClass("rightBigBulleted").removeClass("rightNonBulleted").addClass("rightBigNonBulleted");
    $("#gridLeft .leftModule ul.bigBulleted").parent().addClass("leftBigBulleted");
    $("#gridLeft .leftModule ul.nonBulleted").parent().addClass("leftNonBulleted");
    $("#gridLeft .leftModule ul.imageBulleted").parent().addClass("leftImageBulleted");
    $(".tabModule").each(function () {
        var $thisPar = $(this);
        $(".tabContainer .tabModContent", $thisPar).each(function (e) {
            var $tChild = $(this);
            if ($("ul", $tChild).length < 1) {
                var z = $(".tabContainer .tabModContent", $thisPar).index($tChild);
                $(".tabList li", $thisPar).eq(z).remove();
                $(this).remove()
            }
        });
        $(".tabModContent", $thisPar).hide();
        $("ul.tabList li:first", $thisPar).addClass("active").show();
        $(".tabModContent:first", $thisPar).show();
        $("ul.tabList li", $thisPar).click(function () {
            var $thisChild = $(this);
            var findIndex = $("ul.tabList li", $thisPar).index(this);
            $("ul.tabList li", $thisPar).removeClass("active");
            $thisChild.addClass("active");
            $(".tabModContent", $thisPar).hide();
            $(".tabModContent", $thisPar).eq(findIndex).fadeIn();
            equalHeight($("#gridRight, #gridCenter, #gridLeft"));
            return false
        })
    });
    if ($(".indusProdContent").length) {
        $("#prodFilterSelect option").eq(0).attr("selected", true);
        $(".indusProdContent").hide();
        if ($(".indusProdOverview").length === 0) {
            $("#prodFilterSelect option").eq(1).attr("selected", true);
            $(".indusProdContent").show()
        }
        $("#prodFilterSelect").change(function () {
            var idx = document.getElementById("prodFilterSelect").selectedIndex;
            if ($(".indusProdOverview").is(":visible") && idx != 0) {
                $(".indusProdOverview").slideUp("slow", function () {
                    indusProducts()
                })
            } else {
                indusProducts()
            }
        });
        if ($(".prodOverviewLink").length) {
            $(".prodOverviewLink a").click(function (e) {
                e.preventDefault();
                $(".indusProdOverview").slideToggle("slow");
                $(".prodOverviewLink a").toggleClass("inActive");
                var $tClass = $(this).attr("class");
                if ($("#prodFilterSelect").length) {
                    var idx = document.getElementById("prodFilterSelect").selectedIndex;
                    if (idx == 0) {
                        $("#prodFilterSelect option:eq(1)").attr("selected", true)
                    } else {
                        if ($tClass == "" && idx != 0) {
                            $("#prodFilterSelect option:eq(0)").attr("selected", true)
                        }
                    }
                }
                indusProducts()
            })
        } else {
            $("#prodFilterSelect option").eq(1).attr("selected", true);
            $(".indusProdContent").show()
        }
        var selSize = $("#prodFilterSelect option").size();
        if (selSize == 2) {
            $("#prodFilterSelect").remove()
        }
    }
    if ($(".moduleOverviewLink").length) {
        $(".categoryWrap ul").slideUp(100);
        $(".moduleOverviewLink a").click(function (e) {
            e.preventDefault();
            $(".moduleOverviewLink a").toggleClass("inActive");
            var divPos = $(".categoryWrap .moduleOverview").css("position");
            var divHeight = $(".indusProdContainer").css("height");
            if (divPos == "relative") {
                $(".categoryWrap .moduleOverview").slideToggle("slow", function () {
                    $(".categoryWrap .moduleOverview").css("position", "absolute")
                });
                $(".categoryWrap ul").slideToggle("slow", function () {
                    equalHeight($("#gridRight, #gridCenter, #gridLeft"))
                })
            }
            if (divPos == "absolute") {
                $(".categoryWrap ul").slideToggle("slow");
                $(".categoryWrap .moduleOverview").slideToggle("slow");
                $(".categoryWrap .moduleOverview").css("position", "relative");
                equalHeight($("#gridRight, #gridCenter, #gridLeft"))
            }
        })
    }
  if ($(".detailAccModule").length) {
		var lnkClick = false;
		$('.detModDesc a').click(function(){
			lnkClick = true;
		});	
        $(".detailAccModule").each(function (x) {
            var $tParent = $(this);
            $(">ul", $tParent).each(function (y) {
                var $tChild = $(this);
                $($tChild).addClass("deActive");
                $(">ul", $tParent).addClass("deActive");
                $(">ul", $tParent).eq(0).removeClass("deActive").addClass("active");
                $(">ul.deActive .detModImage, >ul.deActive .detModDesc", $tParent).hide();
                var thisInActHeight = $($tChild).outerHeight();
                var thisActHeight = $($tChild).outerHeight();
                $tChild.click(function (z) {
                    if ($(this).attr("class") == "detModule deActive" && (lnkClick == false) ) {
                        $(">ul", $tParent).removeClass("active").removeClass("deActive");
                        $(">ul .detModImage, >ul .detModDesc", $tParent).slideUp(function () {
                            equalHeight($("#gridRight, #gridCenter, #gridLeft"))
                        });
                        $(">ul", $tParent).addClass("deActive");
                        $tChild.removeClass("deActive").addClass("active");
                        $(".detModImage, .detModDesc", $tChild).slideDown(function () {
                            equalHeight($("#gridRight, #gridCenter, #gridLeft"))
                        })
                    } else {
						if(lnkClick == false)
						{
                        $(">ul", $tParent).removeClass("active").addClass("deActive");
						
                        $(">ul .detModImage, >ul .detModDesc", $tParent).slideUp(function () {
                            equalHeight($("#gridRight, #gridCenter, #gridLeft"))
                        })
						}
                    }
					lnkClick = false;
                })
            })
        })
    }
    if ($("#srchPgForm").length) {
        var allWords = "",
            exactWords = "",
            moreWords = "";
        $("#wrdsField").keyup(function () {
            allWords = trimThis($(this).val());
            outPut()
        });
        $("#exactField").keyup(function () {
            exactWords = trimThis($(this).val());
            if (exactWords != "") {
                exactWords = '"' + exactWords + '"'
            }
            outPut()
        });
        $("#moreField").keyup(function () {
            moreWords = trimThis($(this).val());
            moreWords = moreWords.replace(/\s/g, " OR ");
            outPut()
        });

        function outPut() {
            strV = allWords + " " + exactWords + " " + moreWords;
            strV = strV.replace(/^\s\s*/, "").replace(/\s\s*$/, "");
            $("#searchTerm").val(strV)
        }
        $("#moreSearch").hide();
        $("#srchNeedMore").click(function () {
            $("#moreSearch").toggle("slow");
            $(this).toggleClass("active");
            return false
        });
        $("#searchLink, #srchPageBtn").click(function () {
            if ($("#srchPageLink").val() == $("#srchPageLinkTemp").val()) {
                $("#srchPageLink").val("");
                return true
            }
            return true
        })
    }
    $(".tipBtn1,.tipBtn2,.tipBtn3").click(function () {
        $(".tips1,.tips2,.tips3").hide();
        var btnClass = $(this).attr("class").replace("tipBtn", "");
        $(".tips" + btnClass).show()
    });
    $(".tips1 .closeTips, .tips2 .closeTips,.tips3 .closeTips").click(function () {
        var btnClass = $(this).parent().attr("class").replace("tips", "");
        $(".tips" + btnClass).hide()
    });
    if ($("#gridLeft #navlist").length) {
        $("body").addClass("withLeftNav")
    }
    if ($(".relatedDownload").length) {
        $(".relatedDownload a").each(function (e) {
            var fileType = $(this).attr("href").substr(($(this).attr("href").lastIndexOf(".") + 1));
            $(this).prev().addClass(fileType);
			if($(this).attr('title').search('New!') != -1){
				$(this).addClass('hasNewDocs').html('<em>' + $(this).text().replace(' New!','')+'</em>').append('<small>New!</small>');
			}
        })
    }
    if ($(".insightSection").length) {
        $(".insightSection").each(function () {
            var $mThis = $(this);
            var $docType = $("h3 a", $mThis).attr("href");
            var $ext = $docType.substr($docType.lastIndexOf(".") + 1);
            $(".icons", $mThis).addClass($ext)
        })
    }
    $("#searchBanner").insertAfter("#topMenu");
    if (($(".prodCol1").length == 1) && ($(".prodCol2").length == 0)) {
        $(".prodCol1").css("width", "100%");
        $(".prodCol1").css("border-left", "0");
        $(".prodCol1").css("padding-left", "0")
    }
    if (($(".prodCol1").length == 0) && ($(".prodCol2").length == 1)) {
        $(".prodCol2").css("width", "100%");
        $(".prodCol2").css("border-left", "0");
        $(".prodCol2").css("padding-left", "0")
    };
    equalHeight($("#gridRight, #gridCenter, #gridLeft"));
    $('a').each(function () {
        var point = $(this).attr('href').lastIndexOf('.');
        var fileType = $(this).attr('href').substr(point);
        if (fileType == '.doc' || fileType == '.pdf' || fileType == '.xls' || fileType == '.ppt' || fileType == '.ppt' || fileType == '.swf' || fileType == '.vid' || fileType == '.calc') {
            $(this).attr('target', '_blank')
        }
    })
});
var oldBodyOnLoad = window.onload;
window.onload = function () { };

function indusProducts() {
    if ($("#prodFilterSelect").length) {
        var idx = document.getElementById("prodFilterSelect").selectedIndex;
        if (idx == 1) {
            $(".indusProdContent").slideDown("slow", function () {
                equalHeight($("#gridRight, #gridCenter, #gridLeft"))
            });
            $(".prodOverviewLink a").addClass("inActive")
        } else {
            if (idx == 0) {
                $(".indusProdContent").slideUp();
                $(".indusProdOverview").slideDown("slow", function () {
                    equalHeight($("#gridRight, #gridCenter, #gridLeft"))
                });
                $(".prodOverviewLink a").attr("class", "")
            } else {
                if (idx > 1) {
                    $(".indusProdContent").slideUp().eq(idx - 2).slideDown("slow", function () {
                        equalHeight($("#gridRight, #gridCenter, #gridLeft"))
                    });
                    $(".prodOverviewLink a").addClass("inActive")
                } else {
                    $(".indusProdContent").slideUp().eq(idx).slideDown("slow", function () {
                        equalHeight($("#gridRight, #gridCenter, #gridLeft"))
                    })
                }
            }
        }
    } else {
        $(".indusProdContent").slideDown(function () {
            equalHeight($("#gridRight, #gridCenter, #gridLeft"))
        })
    }
}
function equalHeight(group) {
	return true;
    // var obj = $.browser.msie;
    // var objv = $.browser;
    // if (obj && objv.version < 7) {
       // group.each(function () {
         //   $(this).height("0px")
      //  });
//        var tallest = 0;
 //       group.each(function () {
//            var thisHeight = $(this).height();
//            if (thisHeight > tallest) {
//                tallest = thisHeight
//            }
//        });
//        group.each(function () {
//            $(this).height(tallest)
//        })
//    } else {
//        group.each(function () {
//           $(this).height("auto")
//        });
//        var tallest = 0;
//        group.each(function () {
//            var thisHeight = $(this).height();
//            if (thisHeight > tallest) {
//               tallest = thisHeight
//            }
//        });
//        group.each(function () {
//            $(this).height(tallest - 30)
//        })
//    }
}
function slideIn(element, speed) {
    $(element).next().hide();
    $(element).click(function (e) {
        e.preventDefault();
        $(this).parent().toggleClass("activated");
        $(this).next().slideToggle("speed", function () {
            equalHeight($("#gridRight, #gridCenter, #gridLeft"))
        })
    })
}
function addClass() {
    $("body").addClass("pagePreLoad")
}
function initAccordMenu(accorElemt, sel) {
    $("" + accorElemt + ">li").each(function () {
        $(this).hover(function () {
            $(this).addClass("hOver")
        }, function () {
            $(this).removeClass("hOver")
        })
    });
    $("" + accorElemt + " ul").hide().eq(sel).show().parent().addClass("expanded");
    $("" + accorElemt + " li div").click(function () {
        var checkElement = $(this).next();
        if ((checkElement.is("ul")) && (!checkElement.is(":visible"))) {
            $("" + accorElemt + " ul:visible").slideUp("slow", function () {
                equalHeight($("#gridRight, #gridCenter, #gridLeft"))
            }).parent().removeClass("expanded");
            checkElement.slideDown("slow", function () {
                equalHeight($("#gridRight, #gridCenter, #gridLeft"))
            }).parent().addClass("expanded")
        }
    })
}
function initSimpleAccordMenu(accorElemt, sel) {
    equalHeight($("" + accorElemt + " > li > div"));
    $("" + accorElemt + " ul").hide();
    $("" + accorElemt + " li div").click(function () {
        $(this).next().slideToggle("normal")
    })
}
function trimThis(str) {
    return str = str.replace(/^\s\s*/, "").replace(/\s\s*$/, "").replace(/\s{2,}/g, " ")
}
try {
    document.execCommand("BackgroundImageCache", false, true)
} catch (e) { }
function showSection(x) {
    var bcID = "section" + x;
    document.getElementById(bcID).style.display = "block";
    if (document.getElementById("DivShim")) {
        DivSetVisible(true, document.getElementById(bcID))
    }
}
function hideSection(x) {
    var bcID = "section" + x;
    document.getElementById(bcID).style.display = "none";
    if (document.getElementById("DivShim")) {
        document.getElementById("DivShim").style.display = "none"
    }
}
$.getScript = function (url, callback, cache) {
    $.ajax({
        type: "GET",
        url: url,
        success: callback,
        dataType: "script",
        cache: true
    })
};
$(function () {
    if ($('#gridRight a[href*=".swf"], #gridRight a[href*="youtube.com/embed/"], #gridTop a[href*="youtube.com/embed/"], #gridTop a[href*=".swf"], #gridCenter a[href*="youtube.com/embed/"], #gridCenter a[href*=".swf"], #gridCenter a[href*="/embed"], #gridBottom a[href*=".swf"], #gridLeft a[href*=".swf"], #gridBottom a[href*="youtube.com/embed/"], #gridLeft a[href*="youtube.com/embed/"]').length) {
        $.getScript("../../../../../www-111.chartisinsurance.com/js/showFlash.js"/*tpa=http://www-111.chartisinsurance.com/js/showFlash.js*/);
        $('#gridRight a[href*=".swf"], #gridTop a[href*=".swf"], a[href*="youtube.com/embed/"], #gridCenter a[href*=".swf"], #gridBottom a[href*=".swf"], #gridLeft a[href*=".swf"]').click(function (e) {
            e.preventDefault();
            var $src = $(this).attr("href"),
                vidWidth = gup("width", $src),
                vidHeight = gup("height", $src),
                $contentBlocks = '<div id="overLayBg"></div><div id="overLayBlock"><div id="overLayContent"><a class="closeLayer" href="#Close"></a></div></div>';
            $("body").prepend($contentBlocks);
            $("#overLayBg").css("display", "block");
            $("#overLayBlock").css("display", "block");
            if (vidHeight == "") { vidHeight = "360" };
            if (vidWidth == "") { vidWidth = "640" };
            $("#overLayContent").css("height", "" + vidHeight + "px").css("width", "" + vidWidth + "px").center();
            var $strVar = '<div id="flashBanner1" style="width:' + vidWidth + "px;height:" + vidHeight + 'px"><strong>You need to upgrade your Flash Player.</strong></br>Please download the latest version of flash player from <a href="http://www.adobe.com/go/getflash/">here</a>.</div>';

            if ($('a[href*="youtube.com/embed/"]').length) {
                $src = $src + "?autoplay=1&origin=" + location.hostname;

                var $strVar = '<div id="flashBanner1" style="width:' + vidWidth + "px;height:" + vidHeight + 'px"><iframe width="' + vidWidth + '" height="' + vidHeight + '" type="text/html" src="' + $src + '" frameborder="0" allowfullscreen></iframe></div>';
                $("#overLayContent").append($strVar);
            } else {
                $("#overLayContent").append($strVar);
                var so = new showFlash($src, "flashBanners", vidWidth, vidHeight, "9", "");
                so.addParam("allowScriptAccess", "always");
                so.addParam("wmode", "opaque");
                so.addParam("swliveconnect", "true");
                so.write("flashBanner1");
            }
            fullHeightOn();
            $("#overLayContent a").click(function (e1) {
                e1.preventDefault();
                
                $("#overLayBlock, #overLayBg").hide().remove();
				$("#flashBanner1").html("");
            });
            if ($("#overLayBg").is(":visible")) {
                $(window).bind("resize", function () {
                    fullHeightOn();
                    $("#overLayContent").center()
                })
            } else {
                $(window).unbind("resize")
            }
        })
    }
});
jQuery.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
    this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
    return this
};

function fullHeightOn() {
    $("#overLayBg").height("" + getDocHeight() + "px").width("" + getDocWidth() + "px")
}
function getDocHeight() {
    return Math.max($(document).height(), $(window).height(), $("#body_wrapper").outerHeight(true), document.documentElement.clientHeight)
}
function getDocWidth() {
    return Math.max($("body").width(), $(window).width(), $("#body_wrapper").outerWidth(true), document.documentElement.clientWidth)
}
function gup(name, src) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)",
        regex = new RegExp(regexS),
        results = regex.exec(src);
    if (results == null) {
        return ""
    } else {
        return results[1]
    }
}
if ($.browser.msie) {
    e = "header,menu,nav,footer,abbr,article,aside,audio,canvas,datalist,details,eventsource,figure,hgroup,mark,meter,output,progress,section,time,video";
    x = e;
    e = e.split(",");
    i = e.length;
    while (i--) {
        document.createElement(e[i])
    }
};

<!-- Twitter function-->
/* var tweetUsers = ['AIG_LatestNews'];
var buildString = "";

$(document).ready(function () {

    $('#twitter-ticker').slideDown('slow');
    for (var i = 0; i < tweetUsers.length; i++) {
        if (i != 0) buildString += '+OR+';
        buildString += 'from:' + tweetUsers[i];
    }

    var fileref = document.createElement('script');

    fileref.setAttribute("type", "text/javascript");
    fileref.setAttribute("src", "http://search.twitter.com/search.json?q=" + buildString + "&callback=TweetTick&rpp=10");

    document.getElementsByTagName("head")[0].appendChild(fileref);

});

function TweetTick(ob) {
    var container = $('#tweet-container');
    container.html('');

    $(ob.results).each(function (el) {

        var str = '	<div class="tweet">\
					<div class="avatar"><a href="http://twitter.com/' + this.from_user + '" target="_blank"><img src="' + this.profile_image_url + '" alt="' + this.from_user + '" /></a></div>\
					<div class="user"><a href="http://twitter.com/' + this.from_user + '" target="_blank">' + this.from_user + '</a></div>\
					<div class="time">' + relativeTime(this.created_at) + '</div>\
					<div class="txt">' + formatTwitString(this.text) + '</div>\
					</div>';

        container.append(str);

    });

    //container.jScrollPane();
}

function formatTwitString(str) {
    str = ' ' + str;
    str = str.replace(/((ftp|https?):\/\/([-\w\.]+)+(:\d+)?(\/([\w/_\.]*(\?\S+)?)?)?)/gm, '<a href="$1" target="_blank">$1</a>');
    str = str.replace(/([^\w])\@([\w\-]+)/gm, '$1@<a href="http://twitter.com/$2" target="_blank">$2</a>');
    str = str.replace(/([^\w])\#([\w\-]+)/gm, '$1<a href="http://twitter.com/search?q=%23$2" target="_blank">#$2</a>');
    return str;
}

function relativeTime(pastTime) {
    var origStamp = Date.parse(pastTime);
    var curDate = new Date();
    var currentStamp = curDate.getTime();

    var difference = parseInt((currentStamp - origStamp) / 1000);

    if (difference < 0) return false;

    if (difference <= 5) return "Just now";
    if (difference <= 20) return "Seconds ago";
    if (difference <= 60) return "A minute ago";
    if (difference < 3600) return parseInt(difference / 60) + " minutes ago";
    if (difference <= 1.5 * 3600) return "One hour ago";
    if (difference < 23.5 * 3600) return Math.round(difference / 3600) + " hours ago";
    if (difference < 1.5 * 24 * 3600) return "One day ago";

    var dateArr = pastTime.split(' ');
    return dateArr[4].replace(/\:\d+$/, '') + ' ' + dateArr[2] + ' ' + dateArr[1] + (dateArr[3] != curDate.getFullYear() ? ' ' + dateArr[3] : '');
}*/
<!--end of twitter function-->

// DivOverLay Function for leadership 
function OverLayDivPopup(eventObject) {
	
	jQuery.fn.center = function () {
    this.css("position", "absolute");
    this.css("top", ($(window).height() - this.height()) / 4 + $(window).scrollTop() + "px");
    this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
    return this
};

    var $src = $(eventObject).attr("href"),
        vidWidth = 500,
        vidHeight = '100%',
        $contentBlocks = '<div id="overLayBg"></div><div id="overLayBlock"><div id="overLayContent"><a class="closeLayer1" href="#Close"></a></div></div>';
    $("body").prepend($contentBlocks);
    $("#overLayBg").css("display", "block").css("z-index", 1000);
    $("#overLayBlock").css("display", "block").css("z-index", 1000);
    $("#overLayContent").css('height', 'auto').css("width", "" + vidWidth + "px").center().css("padding-left", 20 + "px").css("padding-right", 20 + "px").css("padding-top", 20 + "px").css("padding-bottom", 10 + "px");
    var divname = eventObject.id + 'div';
    var content = document.getElementById(divname).innerHTML;
    var $strVar = '<div id="flashBanner1" style="width:' + vidWidth + "px;height:" + vidHeight + 'px">' + content + '</div>';
    $("#overLayContent").append($strVar);
    fullHeightOn();
    $("#overLayContent a").click(function (e1) {
        e1.preventDefault();
       
        $("#overLayBlock,#overLayBg").hide().remove();
    $("#flashBanner1").html("");
   });
    if ($("#overLayBg").is(":visible")) {
        $(window).bind("resize", function () {
            fullHeightOn();
            $("#overLayContent").center()
        })
    } else {
        $(window).unbind("resize")
    }
}; 