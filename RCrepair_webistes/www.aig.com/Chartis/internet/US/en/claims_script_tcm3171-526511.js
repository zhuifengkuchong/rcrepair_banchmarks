var func = [];
var j = 0;
var finalArray = [];
$(function(){

	//Start=> Social media icons script
		
	$('<div id="socialIcons" />').prependTo('#footerSection');
	$('#logobanner2').appendTo('#socialIcons').html("Connect with AIG:");
	$('#logobanner7').appendTo('#socialIcons');
	$('#logobanner6').appendTo('#socialIcons');
	$('#logobanner5').appendTo('#socialIcons');
	$('#logobanner4').appendTo('#socialIcons');
	$('#logobanner3').appendTo('#socialIcons');

	//End=> Social media icons script
	
    $('.caForm input:radio').click(function(){
        $(this).parents('.caForm').find('select').attr('selectedIndex',0);
    });
	$('th.cTitle').attr('width','330');
		$('.caResults th').each(function(e){
		$(this).addClass('sortArr' + e + '');
	});
	$('#caLineBus, #caIndus, #caBusinessIndustry').attr('checked', false);
	$("#logobanner2 a").prepend('<span class="socialTitle">'+$("#logobanner2 a img").attr("title")+'</span>');
// Detail page
	$('#printTop').insertAfter('#internalDetail h2');
	var backLink = $('.backResults').eq(1).parent('span');
	$('#printBot').insertBefore(backLink);
	$('#printTop a:eq(0),#printBot a:eq(0)').addClass("before");
//Search Tips
	$('.caSearchTips').click(function(){
		$('#helpLayer').show();
		$('#caClose').show();
	});
	$('#caClose').click(function(){
		$(this).hide();
		$('#helpLayer').hide();
	});
	
//Initial setting of hidden field for disabled value
	$('.caForm input[name=qp_EmdClaimsCauseOfLossOrLiability]').val('ALL');
	
//To pass disabled values
	$('.caForm').submit(function(){
		if($('.disabledVal')){
			$('.disabledVal').remove();
		}
		$('.caForm select').each(function(){
			if($(this).attr('disabled')){
				$(this).parent().append("<input class='disabledVal' name='"+$(this).attr('name')+"' type='hidden' value='"+$(this).val()+"'/>");
			}
		});
	});
	
//Results page script starts
if($('#claimsPResults').val() == ''){
$('#helpLayer').css('left','200px');

//Sorting
	$('.caResults tr').each(function(){
		var arrayBox = [];
		$(this).find('td').each(function(){
			if($(this).find('a').text() !=''){
				var aLink = $(this).find('a').text();
				var xVal = $(this).html();
				arrayBox.push(aLink+''+xVal);	
			}
			else{
				var xVal = $(this).html();
				arrayBox.push(xVal);	
			}
		});
		if(arrayBox.length){
			finalArray.push(arrayBox);
		}
	});
	if(finalArray.length > 0){
		var len = finalArray.length;
		var innerLen = finalArray[0].length;
		$('.caResults th').each(function(e){
		var stringVal;
			$(this).addClass('sortClick');
			var cTable = $(this).closest('table').find('tr');
			$(this).find('a').toggle(function(){
			var newTr = '';
				$('.caResults th').addClass('sortClick').removeClass('dscSortClick').removeClass('ascSortClick');
				$(this).parent('th').removeClass('sortClick').removeClass('dscSortClick').addClass('ascSortClick');
				finalArray.sort(function(a,b){
					if (a[e] == b[e])
						return 0;
					if (a[e] > b[e])
						return 1;
					else
						return -1; 
				});
				for(var i=0; i<len; i++){
						stringVal = finalArray[i][j].toString();
						if(stringVal.indexOf('<') > 0){
							newTr += '<tr><td class="cTitle">'+finalArray[i][0].substr(finalArray[i][0].indexOf('<'))+'</td><td>'+finalArray[i][1]+'</td><td>'+finalArray[i][2]+'</td><td>'+finalArray[i][3]+'</td><td>'+finalArray[i][4]+'</td></tr>';
						}
						else{
							newTr += '<tr><td class="cTitle">'+finalArray[i][0]+'</td><td>'+finalArray[i][1]+'</td><td>'+finalArray[i][2]+'</td><td>'+finalArray[i][3]+'</td><td>'+finalArray[i][4]+'</td></tr>';
						}
				}
			$('.caResults th').parent().addClass('tHead');
			$('.caResults tr:not(.tHead)').remove();
			$(newTr).insertAfter('.caResults tr.tHead');
			},function(){
			var newTr = '';
				$('.caResults th').addClass('sortClick').removeClass('dscSortClick').removeClass('ascSortClick');
				$(this).parent('th').removeClass('sortClick').removeClass('ascSortClick').addClass('dscSortClick');
				finalArray.sort(function(a,b){
					if (a[e] == b[e])
						return 0;
					if (a[e] < b[e])
						return 1;
					else
						return -1; 
				});
				for(var i=0; i<len; i++){
						stringVal = finalArray[i][j].toString();
						if(stringVal.indexOf('<') > 0){
							newTr += '<tr><td class="cTitle">'+finalArray[i][0].substr(finalArray[i][0].indexOf('<'))+'</td><td>'+finalArray[i][1]+'</td><td>'+finalArray[i][2]+'</td><td>'+finalArray[i][3]+'</td><td>'+finalArray[i][4]+'</td></tr>';
						}
						else{
							newTr += '<tr><td class="cTitle">'+finalArray[i][0]+'</td><td>'+finalArray[i][1]+'</td><td>'+finalArray[i][2]+'</td><td>'+finalArray[i][3]+'</td><td>'+finalArray[i][4]+'</td></tr>';
						}
				}
			$('.caResults th').parent().addClass('tHead');
			$('.caResults tr:not(.tHead)').remove();
			$(newTr).insertAfter('.caResults tr.tHead');
			});
		});
	}
	$('#qp_EmdClaimsLOB').bind('change',function(){
	//DropdownFIX for IE
		if (isIEandLessThan9())
			$('.causeSelected select.EmdCauseOfLossOrLiability')._ie_select(j);
		$('.EmdCauseOfLossOrLiability')[0].selectedIndex = 0;
	});
	$('.caForm input[name=qp_EmdClaimsCauseOfLossOrLiability]').val('ALL');
	$('#caLineofBusiness, #caIndustry').show();
	$('#caLineofBusiness ul li').hide();
	$('#caSubmit,#caProductLine, #ALL').show();
	var selectedVal = $.trim($('#qp_EmdClaimsLOB').val().replace(/[^a-zA-Z0-9]/g, '').replace(/\s+/g, ''));
	if(selectedVal == 'ALL'){
		$('#caLineofBusiness ul li').hide();
		$('#ALL, #caProductLine, #caSubmit').show();
	}
	else{
		$('#caLineofBusiness ul li').hide();
		$('#caProductLine, #caSubmit').show();
		$('#'+selectedVal+'').show();
		$('#caLineofBusiness ul li').removeClass('causeSelected');
		$('#'+selectedVal+'').addClass('causeSelected');
		$('.causeSelected select').bind('change',function(){
			$('.caForm input[name=qp_EmdClaimsCauseOfLossOrLiability]').val($(this).val());
		});
	}

	// To remove duplicate values
	var seen = {};
	var opLen;
	$('.caForm select').each(function(){
		opLen = $(this).find('option').length;
		if(opLen > 1){
			$(this).children().each(function() {
				if($(this).val() != 'ALL'){
					var txt = $(this).clone().wrap('<select>').parent().html();
					if (seen[txt]) {
						$(this).remove();
					} else {
						seen[txt] = true;
					}
				}
			});
		}
	});
	// To sort dropdowns
	$('select').each(function(){
		$(this).html($(this).find('option').sort(function(x, y) {
			return $(x).text() < $(y).text() ? -1 : 1;
		}))
		$(this).get(0).selectedIndex = 0;
	});
	$('.caForm input[name=qp_EmdClaimsCauseOfLossOrLiability]').val($('.causeSelected select').val());
	if (isIEandLessThan9()){
		$('#qp_EmdClaimsLOB')._ie_select(j);
		$('#qp_EmdClaimsIndustry')._ie_select(j);
		//$('#qp_EmdState')._ie_select(j);
		$('.causeSelected select.EmdCauseOfLossOrLiability')._ie_select(j);
		}
	}
//Results page script ends
	$('#caLineBus').click(function(){
		if($(this).is(':checked')){
			$('#caSearchBtn').show();
			setZero();
			$('#caLineofBusiness ul li, #caIndustry').hide();
			$('#caLineofBusiness, #caSubmit,#ALL, #caProductLine').show();
			//DropdownFIX for IE
			if (isIEandLessThan9())
				$('#qp_EmdClaimsLOB')._ie_select(j);
		}
	});
	$('#caIndus').click(function(){
		if($(this).is(':checked')){
			$('#caSearchBtn').show();
			setZero();
			$('#caIndustry,#caSubmit').show();
			$('#caLineofBusiness').hide();
			//DropdownFIX for IE
			if (isIEandLessThan9())
				$('#qp_EmdClaimsIndustry')._ie_select(j);
		}
	});
	$('#caBusinessIndustry').click(function(){
		if($(this).is(':checked')){
			$('#caSearchBtn').show();
			setZero();
			$('#caLineofBusiness ul li').hide();
			$('#caIndustry, #caLineofBusiness, #caSubmit, #caProductLine, #ALL').show();
			//DropdownFIX for IE
			if (isIEandLessThan9())
				$('#qp_EmdClaimsLOB, #qp_EmdClaimsIndustry')._ie_select(j);
		}
	}); 

$('#qp_EmdClaimsLOB').bind('change',function(){
	$('.caForm input[name=qp_EmdClaimsCauseOfLossOrLiability]').val('ALL');
	var selectedVal = $.trim($(this).val().replace(/[^a-zA-Z0-9]/g, '').replace(/\s+/g, ''));
	if(selectedVal == 'ALL'){
		$('#caLineofBusiness ul li').hide();
		$('#ALL, #caProductLine, #caSubmit').show();
	}
	else{
		$('#caLineofBusiness ul li').hide();
		$('#caProductLine, #caSubmit').show();
		$('#'+selectedVal+'').show();
		$('.causeSelected select').val('ALL');
		$('#caLineofBusiness ul li').removeClass('causeSelected');
		$('#'+selectedVal+'').addClass('causeSelected');
		//DropdownFIX for IE
		if (isIEandLessThan9())
			$('.causeSelected select.EmdClaimsCauseOfLossOrLiability')._ie_select(j);
		$('.causeSelected select').bind('change',function(){
		$('.caForm input[name=qp_EmdClaimsCauseOfLossOrLiability]').val($(this).val());
		});
	}
}); 
});
function setZero(){
	$('#qp_EmdClaimsIndustry').val('ALL');
	$('#qp_EmdClaimsLOB').val('ALL');
}
// START Code for Dropdown field IE FIX - UI 
function isIEandLessThan9(){
	return $.browser.msie && $.browser.version < 9;
}   
(function ($) {
    $.fn._ie_select = function (j) {
        return $(this).each(function () {
            j++;
            if(j == 1){
            var a = $(this), wrDiv = '<span/>', p = a.parent(), o = a.position(), h = a.outerHeight(), l = o.left, t = o.top, w = $(this).width();
            a.wrap(wrDiv); var c = a.clone(true);
            if(!c.hasClass('modified'))
            {
            p.css('position', 'relative');
            $.data(c, 'element', a);
            c.addClass('modified');
            c.css({
                zIndex: 100,
                height: h,
                top: 'auto',
                left: 'auto',
                position: 'absolute',
                width: 'auto',
                minWidth: w + 'px',
                opacity: 0
            /*}).attr({
                id: this.id + '-clone',
                name: this.name + '-clone'*/
            }).change(function () {
                $.data(c, 'element')
                    .val($(this).val())
                    .trigger('change')
            });
            }
            a.before(c).click(function () {
                c.trigger('click');
            });         
 			if(c.width()<200)
			c.css('width','200px');		 	
            }
        });
    };
})(jQuery); // END SAFETY
