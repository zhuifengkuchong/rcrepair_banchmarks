/*! Avery Dennison FED Javascript
	Primary FED: erik.johnsen@acquitygroup.com
*/
 
/*Start Modal code*/
var documentHeight;
var windowWidth;
var windowHeight;
var modalAnimation = 250; //.25 seconds in and out
var currentModalID = "";
var auto;



//Primary modal display and switching function. Will work modal to modal. example onclick="javascript:showModal('#actualModalID');"
function showModal(id, id2){
	if($(id).length){

		currentModalID = id;
		documentHeight = $(document).height();
		windowWidth = $(window).width();
		windowHeight = $(window).height();
		$(".modalContainer").fadeOut(modalAnimation);
		$('#modalBg').css({'width':windowWidth,'height':documentHeight,'opacity':0.7});
		$(id).css('top', (windowHeight/2)-($(id).height()/2)+$(window).scrollTop());
		$(id).css('left', (windowWidth/2)-($(id).width()/2));
		$(id).stop(true,true).fadeIn(modalAnimation);
		$("#modalBg").fadeIn(modalAnimation);
		
		if(id.indexOf("modalNewsletter1")!=-1){
			captchaRefresh();
			
		}
		if(id.indexOf("video")!=-1){
			//toggleVideo(id2);
			var t=setTimeout("jwplayer('"+id2+"').play();",500)
		}

	}	
	else{
		hideModal(); //if input id is non-existant, hide any open modals
	}
}

// This function is triggered from /libs/collab/commons/components/comments/clientlibs.js and passed the unique ID of this particular reply form
function getFormID(theID){
	//console.log("CQ JS SENT:"+theID);
	var tempID = "#"+theID;
	
	// Create a starting point for this particular form and work with it's children
	var formID = tempID.substr(0, tempID.length-5)

	var replyName = formID+'-userIdentifier';
	var replyEmail = formID+'-email';
	var replyComment = formID+'-text';
	var submitID = formID+'-submit';
	
	// Adding in the container for the required asterisk
	//$(replyName).parent().children('.labelWrap').children('label:first').after('<div class="form_leftcolmark"> *</div>');
	//$(replyEmail).parent().children('.labelWrap').children('label:first').after('<div class="form_leftcolmark"> *</div>');
	//$(replyComment).parent().children('.labelWrap').children('label:first').after('<div class="form_leftcolmark"> *</div>');

	// Default the error messages to not show when CQ outputs this form
	$(replyName).parent().children('span:first').addClass('hide');
	$(replyEmail).parent().children('span:first').addClass('hide');
	$(replyComment).parent().children('div:first').addClass('hide');
	
	// Valiudation
	$(submitID).click(function(e) {
			//console.log("SUBMIT BUTTON ID:"+$(this).attr('id'));	
			//console.log("REPLY FORM CLICK");
			
			// Default the error messages to not show when CQ outputs this form
			$(replyName).parent().children('span:first').addClass('hide');
			$(replyEmail).parent().children('span:first').addClass('hide');
			$(replyComment).parent().children('div:first').addClass('hide');

			// Check for Name value
			if($(replyName).val().length<=0){
				$(replyName).parent().children('span:first').removeClass('hide');
				e.preventDefault();
			}
			// Check for Email value
			else if($(replyEmail).val().length<=0){
				$(replyEmail).parent().children('span:first').removeClass('hide');
				e.preventDefault();
			}
			// Check for Comment value
			else if($(replyComment).val().length<=0){
				$(replyComment).parent().children('div:first').removeClass('hide');
				e.preventDefault();
			}
			else{
				//console.log("NO ERRORS :)  Allowing submit to fire....");
				
				// REMOVE THIS WHEN DOING A REAL SUBMIT
				//e.preventDefault();
			}

			//$(this).parents('form').submit();
		});
}
	
// Force delay of communicating with video player until modal has opened
function timedMsg(theVideo){

	if(jwplayer(theVideo).getState()=="PLAYING"){
		var t=setTimeout("jwplayer('"+theVideo+"').pause();",0)
	}
	else if(jwplayer(theVideo).getState()=="PAUSED"){
		var t=setTimeout("jwplayer('"+theVideo+"').play();",1000)
	}
	else if(jwplayer(theVideo).getState()=="IDLE"){
		//alert("Idle");
		var t=setTimeout("jwplayer('"+theVideo+"').play();",500)
	}
	
}

function toggleVideo(video){
	timedMsg(video);
}

function pauseVideo(videoToPause){

	//if(jwplayer(videoToPause).getState()=="PLAYING"){
		
		var t=setTimeout("jwplayer('"+videoToPause+"').pause('true');",0)
	//}
}
//Universal hide all modals. example onclick="javascript:hideModal();"
function hideModal(){
 	$('#modalBg').fadeOut();
	$('.modalContainer').fadeOut();
	$('#modalBg').off('click',function(e){hideModal();});
	
	$(".modalContent.newsLetter").show();
	$(".modalContent.newsLetter.thankyou").hide();
}

function submitSearch(formID){
	$(formID).submit();
}

function submitCQform(theForm, theURL){
	
	/*
    $.ajax({
           type: "POST",
           url: theURL,
           data: $(theForm).serialize(), // serializes the form's elements.
           success: function(data)
           {
			   $('html').html(data);
           }
         });
		return false;
		*/
}

function handleDeeplink(){

	// DEEPLINKING + ANCHOR FOR PRODUCT FINDER
	if (location.hash.length){
		var deeplink  = location.hash;
		var foundItem = $("#productFinder").find(deeplink);
		var myDeeplink = deeplink.substring(1);
		var groupFlag = $(foundItem).attr('ref');
		//console.log(myDeeplink);
		var allItems = $('.panes').children('#productList').children('li');
		
		//var family;
		$(allItems).each(function(index) {
		
			if($(this).children('.subContent').attr('ref')==groupFlag){
				$(this).addClass('bgWhite');
				//family = $(this);
			}
		});
		
		//$(foundItem).addClass("DEEPLINK");
		
		//Do work on Parents of Deeplink item to expand down to selected item
		$(foundItem).parents('li').addClass('bgWhite');
		$(foundItem).parents('li').children('.productItem').children('.productImage').children('a').toggleClass("selected");
		$(foundItem).parents('li').children('.productItem').children('.shadow50').removeClass("hide");
		
		
		//Do work on Children of Deeplink item
		$(foundItem).children().children().children('.productItem').children('.productImage').children('a').toggleClass("selected");
		$(foundItem).children().children().children('.productItem').children('.shadow50').removeClass("hide");
		$(foundItem).parent().children('.subContent:first').children().children('li:first').css({borderTop: 'none'});
		$(foundItem).children().children().children('.subContent').children('#productList').children('li:first').css({borderTop: 'none'});
		
		//Animate Parents
		$(foundItem).parents('li').children('.subContent').slideToggle('fast', function() {});
		
		//Animate Children
		$(foundItem).children().children().children('.subContent').slideToggle('fast', function() {	
			// SCROLL THE PAGE TO THE SELECTED ID
			$('html,body').animate({scrollTop: $("#"+myDeeplink).offset().top},'slow');
		});
		
	}

}// END DEEPLINK AND ANCHOR	



		
		
function checkOfficesDropdown(){

	var outboundString;
	var countryString = $('#country').attr('value');
	var buString = $('#bu').attr('value');
	var stateString = $('#state').attr('value');
	
	//if state is all, submit request with country and BU
	if(buString!="false" && stateString!="false"){
		buString = buString+'.';
	}
	else if(buString!="false" && stateString=="false"){
		buString = buString;
	}
	else{
		buString = '';
	}
	if(stateString=='false' || stateString=='all'){
		stateString = '';
	}
	else{
		stateString = stateString;
	}
	if(countryString!="false"  && countryString!=""){
		countryString = countryString + '.';
		outboundString = countryString.concat(buString, stateString);
		getDropdownContent(outboundString);
	}
}
function getDropdownContent(selectedValue) {
	
		var pagePrefix = $('#pagePrefix').attr('value');
		var jsonPrefix = $('#jsonPrefix').attr('value');
		var pageSuffix = $('#pageSuffix').attr('value');
		var jsonSuffix = $('#jsonSuffix').attr('value');
		
		var dropDownValue = selectedValue;
		var dropDownStorage = dropDownValue;
		//dropDownValue ='';

		var itemsArray = [];
		
		// Remove all the items in the dropdowns before repopulating
		$('#state_list').children('option').each(function(index) {
			$(this).remove();
		});
		$('#bu_list').children('option').each(function(index) {
			$(this).remove();
		});
		
		//dropDownValue='';
		//if(dropDownStorage!='' && dropDownStorage!=null){
			$.get(jsonPrefix+dropDownStorage+jsonSuffix, function(data, textStatus) {
			var items = JSON.parse(data);
	
			for(var p=0; p < items.dropdownmenu.length; p++) {
				itemsArray.push(items.dropdownmenu[p]);
			}
			
			// iterate over json and assign to vars for BUs
			if(itemsArray[0].bu.length>=1){
				
				$('.subItem').addClass('show');
				
				
				for(var x=0; x < itemsArray[0].bu.length; x++) {
					var buLabel = itemsArray[0].bu[x].label;
					var buValue = itemsArray[0].bu[x].value;
					
					// Write out new divs under each dropdown
					if(itemsArray[0].bu[x].selected=='true'){
						
						if(x==0){
							var bu = '<option class="false" value="'+buValue+'" selected="selected">'+buLabel+'</option>';
						}
						else{
							var bu = '<option value="'+buValue+'" selected="selected">'+buLabel+'</option>';
						}
						
						$('#bu_list').append(bu);
					}
					else{
						var bu = '<option value="'+buValue+'">'+buLabel+'</option>';
						$('#bu_list').append(bu);
				
					}
				}
				
				if($('#bu_list').children("option:first-child").hasClass('false')){
					
					$('.subSearch').removeClass('show');
				}
				else{
					$('.subSearch').addClass('show');	
				}

			}
			else{
				//alert("No BUs");
			}
			
			if(itemsArray[1].state.length>=1){
				
				$('.subOr').addClass('show');
				$('.subState').addClass('show');
				$('.subSearch').addClass('show');
				
				// iterate over json and assign to vars for States
				for(var t=0; t < itemsArray[1].state.length; t++) {
					var buLabel = itemsArray[1].state[t].label;
					var buValue = itemsArray[1].state[t].value;
					
					var stateLabel = itemsArray[1].state[t].label;
					var stateValue = itemsArray[1].state[t].value;
					
					// Write out new divs under each dropdown
					if(itemsArray[1].state[t].selected=='true'){
						var state = '<option value="'+stateValue+'" selected="selected">'+stateLabel+'</option>';
						$('#state_list').append(state);
					}
					else{
						var state = '<option value="'+stateValue+'">'+stateLabel+'</option>';
						$('#state_list').append(state);
					}
				}
			}
			else{
			//$('#state_list').parent().removeClass('show');
			$('.subState').removeClass('show');
			$('.subOr').removeClass('show');
			//$('.subSearch').removeClass('show');
			}
			
			//populate the value of the searhc button
			var target = pagePrefix+dropDownStorage+pageSuffix;
			var buString = $('#bu').attr('value');
			var searchButton = $('#searchBUS');
			//$('#searchBUS').attr("href", pagePrefix+dropDownStorage+pageSuffix);
			//$('#searchBUS').attr("onClick","javascript:_gaq.push(['_trackEvent','BU Finder','search','" + pagePrefix+dropDownStorage+pageSuffix + " ']);")
			searchButton.attr("href",target);
			searchButton.attr("onClick","javascript:_gaq.push(['_trackEvent','Office Finder','search','" + buString + " ']);");
			
		}, 'text');	
} // END dropdown content


//Extra Modal code, reposition on window resize, etc	
$(document).ready(function(){
	
	/*
	window.fbAsyncInit = function() {
		
		FB.Event.subscribe('edge.create', function(response) {
			alert('You liked the URL: ' + response);
		});
	};
	*/
	
	//reposition on window resize
	$(window).resize(function () {
		windowWidth = $(window).width();
		windowHeight = $(window).height();
		$(currentModalID).css('top', (windowHeight/2) - ($(currentModalID).height()/2)+$(window).scrollTop());
		$(currentModalID).css('left', (windowWidth/2) - ($(currentModalID).width()/2));
 		$('#modalBg').css({'width':windowWidth});
	});
	//When an A tag with rel=modal is clicked, make the background on click close window
	$('a[rel=modal]').on('click',function(e){
		e.preventDefault();
		//$('#modalBg').on('click',function(e){hideModal();}); Removed to force the user to use the close button and not the background of the modal
	});
	//when an element with .close class inside of modal content or a class with .hideModal is clicked close all modals
	
	$('.modalContent .close, .hideModal').on('click',function(e){
		e.preventDefault();
		hideModal();
		
	});
	//Move all modalBgs (should only be one) and prepend IE7 modal fix to top of document, for better cross browser results
	if($("#modalBg").length){
		$("#modalBg").each(function(){
			$('html > body:first').prepend('<div id="modalIE7Fix">&nbsp;</div>');
			$('html > body:first').prepend(this);
		});
	}
	//Move all modal content to top for better cross browser results
	if($("body .modalContainer").length){
		$("body .modalContainer").each(function(){
			$('html > body:first').prepend(this);
		//$('html > body:first').prepend("<a class='close'>Close</a>");
		//$('.close').fadeOut();	
		});
		
		/*
		top: 250px;
		right: 182px;
		*/
	}
	/*END onReady Modal*/
	
	/* Check for WW office dropdown default show and population*/
	if ($(".selectCountry ").length){
		//alert("Dropdowns found");
		checkOfficesDropdown();
	}
	else{
		//alert("No Dropdowns");
	}
});
	
/*Start General On Ready*/	
$(document).ready(function(){
	
	/*preloader*/
	function preload(arrayOfImages) {
			$(arrayOfImages).each(function(){
					(new Image()).src = this;
			});
	}
	//alert(location.hash);
	//Paths may need to be updated on going live
	preload([
			'../img/ui/bg/mainDropBg.png'/*tpa=http://www.averydennison.com/includes/img/ui/bg/mainDropBg.png*/ //cq 
			/*'Unknown_83_filename'/*tpa=http://www.averydennison.com/includes/js/includes/img/ui/bg/mainDropBg.png*/*/	// Rockflicker
			
			
			/*'Unknown_83_filename'/*tpa=http://www.averydennison.com/includes/js/img/ui/nav/dropNav/megaBottom.png*/*/
	]);
	
	
	/*
	START SEARCH EXPANDER
	*/
	//hack for overflowed main nav over search area, may be removed in future with design change
	
		if(parseInt($(".topNav .mainNav ul.navLinks").css("width"))>720){
		$(".topNav .collapsedSearch").animate({top: 10},500);
		}
		
	var expandRegion;
	var searchWidth;
		$('.topNav .expandSearch').hover(function(){
			expandRegion = $(this).parent().children('.expandThis');
			searchWidth = parseInt(expandRegion.css('width'));
			if (searchWidth < 26){//would ideally be "<=183",hacked for IE7
				expandRegion.animate({width: 183},250).focus();
			}
		});
	/*
	END SEARCH EXPANDER
	*/
	$('body').attr("class","js");
	
	handleDeeplink();
	/*
	START NAV
	*/
	if ($(".mainNav").length){
		var divToDrop;
		var divLinkActive;
		var objectOffset;
		var timeoutDuration = 200;
		var slideDownDuration = 200;
		var fadeOutDuration = 198; // must be less than timeoutDuration
		function putDropOnStartTimer(startLink, state){
			if (($(startLink).hasClass("noDrop")) ){
				return true;
			}
			divToDrop = startLink.next('.mainNavDropContainer');
			var timeout = $(this).data("timeout");
			if(timeout) clearTimeout(timeout);
			if (state == "start"){
				$(this).data("timeout", setTimeout($.proxy(function() {
					startDropDown(startLink, divToDrop);
				}, this), timeoutDuration));
			}
			else {
				clearTimeout(timeout);
			}
		}
		function startDropDown(startLink, divToDrop){
			$(".mainNavDropContainer").fadeOut(fadeOutDuration);
			$(".mainNav ul li a.navLink").off('mouseenter mouseleave');
			objectOffset = $(startLink).parent().closest("li").position();
			
			$(divToDrop).css('left',-objectOffset.left).slideDown(slideDownDuration);
			if (!($(startLink).hasClass("noDrop")) ){
				$(startLink).addClass("active");
			}
			
			setTimeout(function(){
				$(startLink).parent().hover(
					function() {},
					function(){
						$(divToDrop).fadeOut(fadeOutDuration);
						setTimeout(function() { $(".mainNav ul li a").removeClass("active"); }, fadeOutDuration-75);
						bindUm();
				});
			}, slideDownDuration);
			
			//alert(objectOffset.left);
		}

		
		//Setup Nav triggers and placement
		function bindUm(){
			$(".mainNav ul li a.navLink").each(function(){
				$(this).hover(function() {putDropOnStartTimer($(this), "start")}, function() {putDropOnStartTimer($(this), "stop")});
			});
		}
		
		bindUm();

	// END NAV


	
	/*
	START HOMEPAGE FEATURE
	*/
	/* Original Avery Colors*/
	bgcRedBright={dark:"#CC0000",light:"#FF0014"};
	bgcYellowOrange={dark:"#FF9900",light:"#FAB023"};
	bgcOrange={dark:"#FF3300",light:"#FF6600"};
	bgcToupe={dark:"#a99d81",light:"#c4b796"};
	bgcPurpleDark={dark:"#330066",light:"#580e9b"};
	bgcPurpleLight={dark:"#993399",light:"#9f60b5"};
	bgcGreenLime={dark:"#009900",light:"#33CC00"};	
	bgcGreenMedium={dark:"#004300",light:"#006600"};			
	bgcBlueMuted={dark:"#2b4872",light:"#3c639c"};
	bgcAqua={dark:"#3399FF",light:"#00CCFF"};
	
	// Shared colors
	bgcGreyDark={dark:"#333333",light:"#4d4f53"};
	bgcGreyLight={dark:"#666666",light:"#999999"};
	
	
	
	// MEDICAL COLORS
	bgcOrangeDark={dark:"#Cf673B",light:"#FF6633"};
	bgcGreenDark={dark:"#6D9830",light:"#8DC63F"};
	bgcBlueDark={dark:"#007BAE",light:"#0096D6"};
	bgcBlueLightDark={dark:"#4A9BAB",light:"#60C9DD"};
	bgcOrangeLightDark={dark:"#D9982D",light:"#FBB034"};
	bgcYellowDark={dark:"#B3B446",light:"#E3E65B"};
	bgcFusiaDark={dark:"#7B1264",light:"#A21984"};
	bgcPurpleDarkMedical={dark:"#9C6A97",light:"#BF83B9"};
	
	
	
	// LANDING FEATURE
	if ($(".landingFeatureA").length){
		
		var featureColor;
		//= hpfColor;
		$(".landingFeatureA").each(function(){

			// Get the current color of something
			if($(this).children(".featureColor").val()){
				featureColor = $(this).children(".featureColor").val();
				featureColor = "bgc"+featureColor;
				// Target the background
				$(this).css("background-color", window[featureColor].light);
				// Target the square Icon
				$(this).children(".featureLeft").find(".actionArrow").css("background-color", window[featureColor].dark);
			}
		});
	}
	
	if ($(".landingFeatureB").length){
		
		var featureColor;
		//= hpfColor;
		$(".landingFeatureB").each(function(){

			// Get the current color of something
			if($(this).children(".featureColor").val()){
				featureColor = $(this).children(".featureColor").val();
				featureColor = "bgc"+featureColor;
				// Target the background
				$(this).css("background-color", window[featureColor].light);
				// Target the square Icon
				$(this).children(".featureLeft").find(".actionArrow").css("background-color", window[featureColor].dark);
			}
		});
	}
	
	if ($(".searchPromoContainer").length){
		
		var promoColor;
		//= hpfColor;
		$(".searchPromoContainer").each(function(){

			// Get the current color of something
			if($(this).children(".promoColor").val()){
				promoColor = $(this).children(".promoColor").val();
				promoColor = "bgc"+promoColor;
				// Target the background
				$(this).css("background-color", window[promoColor].light);
			}
		});
	}
	

	/*Start Image Galleries*/
	if ($(".imageGallery").length){
		$(".imageGallery").each(function(){
			var imgIndex;
			var imageLink;
			var imgOffset;
			var animationLength = 280;
			var targetSlide = $(this).children(".display");
			var targetInfo = $(this).children(".info");
			var targetSlideText;
			var galleryh6="";
			var galleryh2="";
			var galleryb4="";
			
			$(this).children(".icons").children("img").click(function(){
				imgIndex = $(this).index();
				imgOffset = $(this).position();
				
				
				$(this).parent().children(".dottedOver").css("left",imgOffset.left).css("top",imgOffset.top);
				$(this).parent(".icons").children("img").stop(true,true).animate({
					opacity: .3
				}, animationLength );
				$(this).stop(true,true).css("opacity",1);
				targetSlide.children("div").fadeOut(animationLength);
				targetSlide.children("div").eq(imgIndex).stop(true,true).fadeIn(animationLength);
				
				targetSlideText = $(this).parents(".imageGallery").children(".display").children("div").eq(imgIndex);
				if(targetSlideText.children(".infoh6").length){galleryh6 = targetSlideText.children(".infoh6").html();}
				if(targetSlideText.children(".infoh2").length){galleryh2 = targetSlideText.children(".infoh2").html();}
				if(targetSlideText.children(".infob5").length){galleryb4 = targetSlideText.children(".infob5").html();}
				targetInfo.stop(true,true).animate({
					opacity: 0
				}, animationLength/2 );
				setTimeout(function(){
					targetInfo.children("h6").html(galleryh6);
					targetInfo.children("h2").html(galleryh2);
					if(targetInfo.children(".bodyTextBold").length){
						targetInfo.children(".bodyTextBold").html(galleryb4);
					}
					targetInfo.stop(true,true).animate({
						opacity: 1
					}, animationLength/2 );
				}, animationLength/2);
				
			});
		
		});
	}
	
	if ($("#worldwideSites_mainContainer").length){
		
		var zIndexNumber = 1000;
		$('#worldwideSites_sectionLinks li').each(function() {
			$(this).css('zIndex', zIndexNumber);
			zIndexNumber -= 10;
		});

	}
	
	// HOMEPAGE FEATURE
	if ($(".homepageFeature").length){
		
		$(".homepageFeature").each(function(){
			
			//Duplicate center h3
			targetDupeH3 = $(this).children(".options").children("h3").eq(1);
			targetDupeH3.clone(true).insertAfter(targetDupeH3);
				var animationLength = 750;
				var positionToMove = 0;
				var h3index;
				var hpfColor;
				var hpfLink;
				
				var hpfTitle;
				var hpfText;
				var hpfActions;
				var imgSwap;
				var targetDetailWindow = $(this).children(".detailWindow");
				var targetMainImageLink = $(this).children(".imgSwap").children(".mainImageLink");
				var targetMainImagePlayLink = $(this).children(".playOverlay").children(".mainImageLink");
				var targetActionBar = targetDetailWindow.children(".actionBar");
				var targetDetailsText = targetDetailWindow.children(".detailsText");
				var targetDetailsTextGroup = targetDetailsText.children(".group");
				var targetActionContainer = targetActionBar.children(".actionContainer");
				var targetActionBarContent = targetActionBar.children(".actionContainer").children(".actionBarContent");
				
				var targetDetailWindowAuto = $(this).children(".detailWindow");
				var targetMainImageLinkAuto = $(this).children(".imgSwap").children(".mainImageLink");
				var targetActionBarAuto = targetDetailWindowAuto.children(".actionBar");
				var targetDetailsTextAuto = targetDetailWindowAuto.children(".detailsText");
				var targetDetailsTextGroupAuto = targetDetailsTextAuto.children(".group");
				var targetActionContainerAuto = targetActionBarAuto.children(".actionContainer");
				var targetActionBarContentAuto = targetActionBarAuto.children(".actionContainer").children(".actionBarContent");
				
				/* Original Avery Colors*/
				bgcRedBright={dark:"#CC0000",light:"#FF0014"};
				bgcYellowOrange={dark:"#FF9900",light:"#FAB023"};
				bgcOrange={dark:"#FF3300",light:"#FF6600"};
				bgcToupe={dark:"#a99d81",light:"#c4b796"};
				bgcPurpleDark={dark:"#330066",light:"#580e9b"};
				bgcPurpleLight={dark:"#993399",light:"#9f60b5"};
				bgcGreenLime={dark:"#009900",light:"#33CC00"};	
				bgcGreenMedium={dark:"#004300",light:"#006600"};			
				bgcBlueMuted={dark:"#2b4872",light:"#3c639c"};
				bgcAqua={dark:"#3399FF",light:"#00CCFF"};
				
				// Shared colors
				bgcGreyDark={dark:"#333333",light:"#4d4f53"};
				bgcGreyLight={dark:"#666666",light:"#999999"};
				
				
				
				// MEDICAL COLORS
				bgcOrangeDark={dark:"#Cf673B",light:"#FF6633"};
				bgcGreenDark={dark:"#6D9830",light:"#8DC63F"};
				bgcBlueDark={dark:"#007BAE",light:"#0096D6"};
				bgcBlueLightDark={dark:"#4A9BAB",light:"#60C9DD"};
				bgcOrangeLightDark={dark:"#D9982D",light:"#FBB034"};
				bgcYellowDark={dark:"#B3B446",light:"#E3E65B"};
				bgcFusiaDark={dark:"#7B1264",light:"#A21984"};
				bgcPurpleDarkMedical={dark:"#9C6A97",light:"#BF83B9"};
				
				var currentIndex = 0;
				
				var scrollArray = [];
				
				var timer = $.timer(function() {
					//alert(scrollArray.length);
					if(currentIndex==1 && scrollArray.length<4){
						currentIndex=0;
					}
					else if(currentIndex==1 && scrollArray.length>3){
						currentIndex=3;
					}
					else if(currentIndex==3){
						currentIndex=0;
					}
					else if(currentIndex==0){
						currentIndex=1;
					}
					else{
						alert("No condition");
					}


					if(targetMainImageLink.children("img").length>1){
						
						autoRotate(scrollArray[currentIndex]);
					}
				});
				
				$(this).children(".options").children("h3").each(function(index) {
					//alert("Pushing");
					scrollArray.push($(this));
					
				});

				// place if here to not start timer

				timer.set({ time : 5000, autostart : true });
				
				$(this).children(".options").children("h3").click(function(){
					
					timer.stop();
										
					//Gathering Data
					h3index = $(this).index();
					
					if (h3index == 0){positionToMove=0;imgSwap=".hpcMainImg1";}
					if ((h3index == 1)||(h3index == 2)){positionToMove=240;imgSwap=".hpcMainImg2";}
					if (h3index >= 3){positionToMove=480;imgSwap=".hpcMainImg3";}
					var targetImage = $(this).parents(".homepageFeature").children(".imgSwap").children(imgSwap);
					
					// Check if hidden param of video is true to show the overlay of the play button image
					if($(this).children(".video").val()){$(this).parent().parent().children(".playOverlay").removeClass('hide');}
					else{$(this).parent().parent().children(".playOverlay").addClass('hide');}
					
					if($(this).children(".hpfColor").val()){hpfColor = $(this).children(".hpfColor").val();}
					else{hpfColor="Aqua";}
					hpfColor = "bgc"+hpfColor;
					if($(this).children(".hpfLink").val()){hpfLink = $(this).children(".hpfLink").val();}
					else{hpfLink="/";}
					if($(this).children(".windowh3").length){hpfTitle = $(this).children(".windowh3").html();}
					
					if($(this).children(".windowh4").length){hpfText = $(this).children(".windowh4").html();}
					if($(this).children(".windowA").length){hpfActions = $(this).children(".windowA").html();}
					if (hpfActions == ""){hpfActions="<span class='actionArrow'>&ndash;&gt;</span><span class='linkDescription'>Read More</span>";}
					
					//Primary Animation
					targetMainImageLink.children("img").stop(true,true).fadeOut(animationLength);
					targetMainImageLink.children(imgSwap).stop(true,true).fadeIn(animationLength);

					targetDetailWindow.animate({
						left: positionToMove
					}, animationLength );
					//window.dave === window['dave']
					//alert(hpfColor);
					targetActionContainer.children(".actionArrowBg").stop(true,true).animate({
						backgroundColor: window[hpfColor].dark
					}, animationLength );
					targetActionBar.children(".actionBarBg").stop(true,true).animate({
						backgroundColor: window[hpfColor].light
					}, animationLength );
					targetDetailsText.stop(true,true).animate({
						backgroundColor: window[hpfColor].light
					}, animationLength );
					targetDetailsTextGroup.stop(true,true).animate({
						opacity: 0
					}, animationLength/2 );
					targetActionBarContent.stop(true,true).animate({
						opacity: 0
					}, animationLength/2 );
					
					//Secondary Animation + New data population
					
					setTimeout(function() {
						
						targetDetailsTextGroup.children("h3").html(hpfTitle);
						targetDetailsTextGroup.children("p").html(hpfText);
						targetActionBarContent.html(hpfActions);
						targetDetailWindow.attr("href", hpfLink);
						targetMainImageLink.attr("href", hpfLink);
						targetMainImagePlayLink.attr("href", hpfLink);
						
						targetDetailsTextGroup.stop(true,true).animate({
							opacity: 1
						}, animationLength/2 );
						targetActionBarContent.stop(true,true).animate({
							opacity: 1
						}, animationLength/2 );
					}, animationLength/2); 
					//end timeout
				});
				//end function
				
				// sterling
				function autoRotate(theIndex){

					//Gathering Data
					h3index = theIndex.index();
					
					var objReference = theIndex;
					
					if (h3index == 0){positionToMove=0;imgSwap=".hpcMainImg1";}
					if (h3index == 1){positionToMove=240;imgSwap=".hpcMainImg2";}
					if (h3index == 2){positionToMove=240;imgSwap=".hpcMainImg2";}
					if (h3index >= 3){positionToMove=480;imgSwap=".hpcMainImg3";}
					var targetImageAuto = objReference.parent(".homepageFeature").children(".imgSwap").children(imgSwap);
					//alert(targetImageAuto.val());
					//alert("Target image auto:"+targetImageAuto.length);
					//alert("IMG Length:"+$(this).parents(".homepageFeature").children(".imgSwap").children(imgSwap).length);
					
					// Check if hidden param of video is true to show the overlay of the play button image
					if(objReference.children(".video").val()){objReference.parent().parent().children(".playOverlay").removeClass('hide');} 
					else{objReference.parent().parent().children(".playOverlay").addClass('hide');}
					
					if(objReference.children(".hpfColor").val()){hpfColor = objReference.children(".hpfColor").val();
						//alert("color:"+hpfColor);
					}
					else{hpfColor="Aqua";}
					hpfColor = "bgc"+hpfColor;
					if(objReference.children(".hpfLink").val()){hpfLink = objReference.children(".hpfLink").val();}
					else{hpfLink="/";}
					if(objReference.children(".windowh3").length){hpfTitle = objReference.children(".windowh3").html();}
					
					if(objReference.children(".windowh4").length){hpfText = objReference.children(".windowh4").html();}
					if(objReference.children(".windowA").length){hpfActions = objReference.children(".windowA").html();}
					if (hpfActions == ""){hpfActions="<span class='actionArrow'>&ndash;&gt;</span><span class='linkDescription'>Read More</span>";}
					
					//alert(hpfLink);
					
					//Primary Animation
					
					targetMainImageLink.children("img").stop(true,true).fadeOut(animationLength);
					targetMainImageLink.children(imgSwap).stop(true,true).fadeIn(animationLength);
					targetDetailWindow.animate({
						left: positionToMove
					}, animationLength );
					
					targetActionContainerAuto.children(".actionArrowBg").stop(true,true).animate({
						backgroundColor: window[hpfColor].dark
					}, animationLength );
					
					targetActionBar.children(".actionBarBg").stop(true,true).animate({
						backgroundColor: window[hpfColor].light
					}, animationLength );
					targetDetailsText.stop(true,true).animate({
						backgroundColor: window[hpfColor].light
					}, animationLength );
					targetDetailsTextGroup.stop(true,true).animate({
						opacity: 0
					}, animationLength/2 );
					targetActionBarContent.stop(true,true).animate({
						opacity: 0
					}, animationLength/2 );
					
					//Secondary Animation + New data population
					
					//setTimeout(function() {
						//alert(hpfTitle);
						targetDetailsTextGroup.children("h3").html(hpfTitle);
						targetDetailsTextGroup.children("p").html(hpfText);
						targetActionBarContent.html(hpfActions);
						targetDetailWindow.attr("href", hpfLink);
						targetMainImageLink.attr("href", hpfLink);
						targetMainImagePlayLink.attr("href", hpfLink);
						targetDetailsTextGroup.stop(true,true).animate({
							opacity: 1
						}, animationLength/2 );
						targetActionBarContent.stop(true,true).animate({
							opacity: 1
						}, animationLength/2 ); 
					//}, animationLength/2);
				}
				//end sterling
		});
}
		/* END HOMEPAGE FEATURE */
	/*END GALLERIES*/
	
	
	
	
	/*Start Left Nav*/
	var leftNavAnimation = 500;
	if($(".leftNavContainer").length){
	
		$(".controls a").on("click", function(ep){
			
			if($(this).hasClass("showOn")){
				$(this).removeClass("showOn").addClass("hideOn");
				$(this).parent().next("dl.leftNav").children("dd").stop(true,true).slideDown(leftNavAnimation);
			}
			else if($(this).hasClass("hideOn")){
				$(this).removeClass("hideOn").addClass("showOn");
				$(this).parent().next("dl.leftNav").children("dd").stop(true,true).slideUp(leftNavAnimation);
			}

			if($(this).hasClass("actionAll")){
				
				if($(this).children("span:first-child").hasClass("show")){
					$(this).children("span:first-child").removeClass("show").addClass("hide");
					$(this).children("span:last-child").removeClass("hide").addClass("show");
					
					$(this).parent().next("dl.leftNav").children("dt").stop(true,true).slideDown(leftNavAnimation);
				}
				else{
					$(this).children("span:first-child").removeClass("hide").addClass("show");
					$(this).children("span:last-child").removeClass("show").addClass("hide");
					
					$(this).parent().next("dl.leftNav").children("dt.closed").stop(true,true).slideUp(leftNavAnimation);
				}
				
				return false;
			}
			else{
				return true;
			}

			
		});
		
	}
	
	// Check for CQ comments component.  Add marlup for submit styling
	if($(".commentForm").length){
		//$(".submit-block").append('<span class="cap"></span>');	
		
		console.log("TOP FORM IN PLAY");
		
		// OUTPUT ID OF FORM SUBMIT BUTTON FOUND ON PAGE
		//console.log($('.commentForm').children().children('form').find('input:submit').attr('id'));

		var formID = "#"+$('.commentForm').children().children('form').find('input:submit').attr('id');
		var formAnchor = "#"+$('.commentForm').children('div:first').attr('id');
		
		//User Inputs
		var uName = formAnchor+'-userIdentifier';
		var uEmail = formAnchor+'-email';
		var uComment = formAnchor+'-text';
		
		// Error display containers.  Hide them on load
		
		$(uName).parent().children('span:first').addClass('hide');
		$(uEmail).parent().children('span:first').addClass('hide');
		$(uComment).parent().children('div:first').addClass('hide');
		
		// TEMP INJECTION OF NEW DIV
		//$(uName).parent().children('label:first').before('<div class="form_leftcollabel">');
		//$(uName).parent().children('.form_leftcolmark').before('</div>');
		
		//Add the Asterisk to required fields
		//$(uName).parent().children('.labelWrap').children('label:first').after('<div class="form_leftcolmark"> *</div>');
		//$(uEmail).parent().children('.labelWrap').children('label:first').after('<div class="form_leftcolmark"> *</div>');
		//$(uComment).parent().children('.labelWrap').children('label:first').after('<div class="form_leftcolmark"> *</div>');

		//Add event handler for this particular comment form's submit
		$(formID).click(function(e) {
			
			//console.log("SUBMIT BUTTON ID:"+$(this).attr('id'));	
			
			$(uName).parent().children('span:first').addClass('hide');
			$(uEmail).parent().children('span:first').addClass('hide');
			$(uComment).parent().children('span:first').addClass('hide');
		
			// Check for Name value
			if($(uName).val().length<=0){
				$(uName).parent().children('span:first').removeClass('hide');
				e.preventDefault();
			}
			// Check for Email value
			else if($(uEmail).val().length<=0){
				$(uEmail).parent().children('span:first').removeClass('hide');
				e.preventDefault();
			}
			//Check Email for @ and .
			else if($(uEmail).val().indexOf('@')==-1 || $(uEmail).val().indexOf('.')==-1){
				$(uEmail).parent().children('span:first').removeClass('hide');
				e.preventDefault();
			}
			// Check for Comment value
			else if($(uComment).val().length<=0){
				$(uComment).parent().children('span:first').removeClass('hide');
				e.preventDefault();
			}
			else{
				//console.log("NO ERRORS :)  Allowing submit to fire....");
				
				// REMOVE THIS WHEN DOING A REAL SUBMIT
				//e.preventDefault();
			}
			
			//$(this).parents('form').submit();
		});
		
	}
	// End Comments component
		
	}
	
	/* Comments submit*/
	
	
	
	
	/*End Left Nav*/

	/*Start Input highlights*/
	$(".form table input").focus(function(){
		$(this).parent().children("label").css("color", "#000");
		$(this).css("border-color","#000");
	});
	$(".form table input").blur(function(){
		$(this).parent().children("label").css("color", "");
		$(this).css("border-color","");
	});

	/*Start WORLDWIDE SITES TOGGLE*/
	$('#toggleMenu, #toggleMenu2').click(function() {
		$('#worldwideSites_mainContainer').slideToggle('slow', function() { // Animation complete.
		});
	});
	
	$('#resultsList li a').click(function() {
		$(this).parent().children('.businessAddress').addClass('bgWhite');
  		
		if( $(this).parent().children('.shadow10').hasClass('hide')){
		 	$(this).parent().children('.shadow10').removeClass('hide');
		}
		else{
			$(this).parent().children('.shadow10').addClass('hide');
		}
		$(this).parent().find('.businessAddress').slideToggle('slow', function() {  // Animation complete.
		});
	});
	
	$('#resultsList li a').click(function() {
  		$(this).toggleClass("selected");
	});
	/*
	$("#cqSubmit").click(function() {

		//var formID = $(this).closest('form').attr('id');
		//var formURL = $(this).closest('form').attr('action');
		//alert(formID);
		//alert(formURL);
		
		 $.ajax({
           type: "POST",
           url: formURL,
           data: $("#"+formID).serialize(), // serializes the form's elements.
           success: function(data)
           {
			   $('html').html(data);
           }
         });
		 
		return false;
		
		//$("#"+formID).addClass("TEST");
		
		//document.forms['testme'].submit();
		//$("#_content_des_global_en_home_contactus_jcr_content_parsys_start").submit();
		//e.preventDefault();
	});
	*/
	// Product finder 
	$('.productItem a').click(function() {
  		
		$(this).toggleClass("selected");
		
		if( $(this).parent().parent().parent().hasClass('bgWhite')){
		 	$(this).parent().parent().parent().removeClass('bgWhite');
			$(this).parent().parent().children('.shadow50').addClass('hide');
		}
		else{
			$(this).parent().parent().children('.shadow50').removeClass('hide');
			$(this).parent().parent().parent().addClass('bgWhite');

			//Force removal of barder on first SubItem in active list
			$(this).parent().parent().parent().children('.subContent').children('#productList').children('li:first').css({borderTop: 'none'});
			
		}
		
		$(this).parent().parent().parent().children().next('.subContent').slideToggle('slow', function() { // Animation complete.
			});
	});
	
	
	
	$('#toggleClose').click(function() {
		$('#worldwideSites_mainContainer').slideUp('slow', function() { // Animation complete.
		});
	});


	$("#toggleMenu, #toggleMenu2").click(function () {
		$(this).toggleClass("selected");
	});
		
	$("#toggleClose").click(function () {
		$('#toggleMenu, #toggleMenu2').toggleClass("selected");
	});
	
	// setup ul.tabs to work as tabs for each div directly under div.panes
	$(function() {
		$("http://www.averydennison.com/includes/js/ul.tabs").tabs("div.panes > div");
		$("ul.tabs li").click(function () {
			$("ul.tabs li").removeClass("current");
			$(this).addClass("current");
		});
		
		$(".showMap").click(function () {
			$('ul.selectCountry').removeClass('hide');		
		});
		
		$(".hideMap").click(function () {
			$('ul.selectCountry').addClass('hide');		
		});
		
	});
	
	/* CheckBoxes and Radios */
	/*
      When the toggle switch is clicked, check off / de-select the associated checkbox
     */
    $('.toggle').click(function(e) {
       var checkboxID = $(this).attr("ref");
       var checkbox = $('#'+checkboxID);

       if (checkbox.is(":checked")) {
         checkbox.removeAttr("checked");

       }else{
         checkbox.attr("checked","true");
       }
       $(this).toggleClass("checked");

       e.preventDefault();

    });

	// radio
	$('.section.radio .radio').click(function(e) {
       
	   var radioID = $(this).attr("ref");
       var radio = $('#'+radioID);
	   var radioName = radio.attr('name');
		
		// OnClick, uncheck all in this radio group 
	   $('input:radio[name='+radioName+']').attr('checked',false);
	   
	   // OnClick, uncheck all the <a> tags in this radio group
	   $('input:radio[name='+radioName+']').next('a').removeClass('checked');
	   
	   // Toggle the this particular <a> tag as checked or not
	   $(this).toggleClass("checked");
	   
	   //Check this input only
	   $(radio).attr("checked", "true");

       e.preventDefault();

    });
	
	if($(".filterContainer").length){
		
		$('.filterHide').addClass('hide');
		
		$('.filterDisplay').click(function(e) {
		   
		   //Scope for this particular group
		   var startList = $(this).parent().parent();
		   
		   if($(startList).children().children('.filterDisplay').children('.filterShow').hasClass('hide')){
			  
			 // Hide only those that do not have 'default' on them. 
			  $(startList).children().children('.filterItem').each(function(index){
				  if(!$(this).hasClass('default')){
					$(this).addClass('hide');
				  }
			  });
		   }
		   else{
			   // Show all in this set
			   $(startList).children().children('.filterItem').each(function(index){
					$(this).removeClass('hide');
			   });
		   }
		   $(startList).children().children('.filterDisplay').children('.filterShow').toggleClass('hide');
		   $(startList).children().children('.filterDisplay').children('.filterHide').toggleClass('hide');

		   e.preventDefault();
	
		});
	}
	/*
	$('.filterCheckBox .navToggleRadio').click(function(e) {
       var radioID = $(this).attr("ref");
       var radio = $('#'+radioID);
			
       if (radio.is(":checked")) {
		   alert("checked");
         radio.removeAttr("checked");

       }else{
		    alert("NOT checked");
         radio.attr("checked","true");
       }
      $(this).toggleClass("checked");

       e.preventDefault();

    });
	*/
	/* End Check boxes and radios */
	
	
	
	
	
	
	$('#searchPromoDropdown').change(function(e) {

		var dropDownValue = $(this).val();
	    $(this).parent().parent().find('#buSearch').attr("href", dropDownValue);

       e.preventDefault();
    });
	
	
	/*
	$('.lockTwitter').click(function() {
      
       $(this).addClass("HI");
		console.log("HIT");
		//alert("HIT");
       e.preventDefault();

    });
	*/

    /*$('span.languages a').click(function(){
        var spans = $(this).closest('.sectionLanguage').parent().find('span');
        if (spans){
            $.cookie($(spans[0]).html(), $(this).html(), { expires: 1, path: '/', domain: '.averydennison.com'});
        }
    });*/

    // Set current Language
    var currentLanguage = $('#currentLanguage').val();
    var currentSite = $('#currentSite').val();
    //var selectedLanguage = $.cookie(currentSite.replace('&','&amp;'));

    if (currentLanguage){
		$('#toggleMenu2').hide();
		$('.languageSelected').show();
		$('#toggleMenu').show();
        $('#toggleMenu').next().addClass('leftBorder');
    }
    else{
		$('#toggleMenu').hide();
		$('#toggleMenu2').show();
        $('.languageSelected').hide();
        $('#toggleMenu').next().removeClass('leftBorder');
    }

});


     
     
