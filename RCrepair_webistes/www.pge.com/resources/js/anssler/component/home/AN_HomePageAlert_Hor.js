jQuery.namespace('http://www.pge.com/resources/js/anssler/component/home/anssler.component.home');

anssler.component.home.AN_HomePageAlert_Hor = anssler.component.Component.extend( {
	data : {
		numAlerts:0,
		maxHeight:200,
		expanded:false,
		curPos:1,
		maxPos:1,
		reverse: false,
		mLeft:0,
		fos:20
	},
	ready : function() {
	  var cs=this;
	  cs.showAllAlerts();
	},
	showAllAlerts : function() {
		var cs=this;
		cs.data.maxHeight = 60+57*(cs.data.numAlerts-1);	 
		  $('.an_c56_homep_alert_rest').each(function(index) {
			//the first one is 60 px followed 
			//$(this).css('top',60 + 57*(index));
			cs.data.maxPos = cs.data.maxPos+1;
		 });
		//set the width of the entire list to be whole width
		$('.an_c56_homep_alert_list').css('width',cs.data.maxPos*800+cs.data.fos);
		//set the max width to the min-width of the div so that the alert doesn't overlay on the count
		$('.an_c56_homep_alert_desc').css('max-width',$('.an_c56_homep_alert_desc').css('min-width'));
		  $("#an_c56_homep_control").click( function() {
			if(cs.data.expanded==false){ 
				cs.data.expanded=true;
				cs.expandAlerts();
			} else{
				cs.data.expanded=false;
				cs.collapseAlerts();
			}
		  });
		//set the time for the alerts
		setTimeout(function(){
			cs.animateAlerts()
		  	},12000);
	},
	/**
 	 *  The animation will make the list go up
	 **/
        animateAlerts : function() {
		var cs=this;
	//Do the animation only if it is not expanded
		if(cs.data.expanded==false) {
		   if(cs.data.reverse==false){
			cs.scrollAlertList(-1);	
			cs.data.curPos=cs.data.curPos+1;
		   } else {
			cs.scrollAlertList(1);	
			cs.data.curPos=cs.data.curPos-1;
		   }
		   if(cs.data.curPos==cs.data.maxPos){ 
			cs.data.reverse=true;
		    }
		   else if(cs.data.curPos==1){ 
			cs.data.reverse=false;
		   }
		}
		setTimeout(function(){
			cs.animateAlerts()
		  	},10000);
 
	},
	scrollAlertList: function(dir) {
		var cs=this;
		var left=800*(dir);
		cs.data.mLeft = cs.data.mLeft+left;	
		cs.setAlertTop();
	},
	setAlertTop : function() {
		var cs=this;
		$('.an_c56_homep_alert_list').animate({
				"margin-left": cs.data.mLeft
			}, 500, function() {
				// Animation complete.
			});
	},
	expandAlerts : function() {
		var cs=this;
		cs.data.mLeft=0;
		cs.data.curPos=1;
		cs.data.reverse=false;
		cs.setAlertTop();
		$('.an_c56_homep_alert_desc').css('overflow','visible');
		$('.an_c56_homep_alert_container').css('background-size',cs.datamaxHeight);
		$('.an_c56_homep_alert_container').css('height',cs.data.maxHeight);
		//$('.an_pge_home-alert').css('height',cs.data.maxHeight);
		//$('.an_pge_home-alert').css('background-size',cs.data.maxHeight);
		//$('.an_pge_home-alert').css('height',cs.data.maxHeight);
		//cs.expandAlert();
		$('.an_c56_homep_alert_count').css('height',cs.data.maxHeight);
		$('.an_c56_homep_alert_count').css('top',cs.data.maxHeight/2-3);
		$('#an_c56_homep_control').removeClass('an_c56_homep_alert_down');
		$('#an_c56_homep_control').addClass('an_c56_homep_alert_up');
		//remove the float left for it to display down
		$('.an_c56_homep_alert_rest').css('float','');
		$('.an_c56_homep_alert_first').css('float','');
	},
	collapseAlerts : function() {
		var cs=this;
		$('.an_c56_homep_alert_desc').css('overflow','hidden');
	//	$('.an_pge_home-alert').css('background-size','60px');
		$('.an_c56_homep_alert_container').css('background-size','60px');
        //		cs.collapseAlert();
		$('.an_c56_homep_alert_container').css('height','60px');
		//$('.an_pge_home-alert').css('height','60px');
		$('.an_c56_homep_alert_count').css('height','60px');
		$('.an_c56_homep_alert_count').css('top','20px');
		$('#an_c56_homep_control').removeClass('an_c56_homep_alert_up');
		$('#an_c56_homep_control').addClass('an_c56_homep_alert_down');
		//This is needed to make the list horizontal again
		$('.an_c56_homep_alert_rest').css('float','left');
		$('.an_c56_homep_alert_first').css('float','left');
	},
	expandAlert : function() {
	 var cs=this;
	$('.an_pge_home-alert').animate({
			"height": cs.data.maxHeight
		}, 500, function() {
			// Animation complete.
		   $('.an_c56_homep_alert_count').css('height',cs.data.maxHeight);
		   $('.an_c56_homep_alert_count').css('top',cs.data.maxHeight/2-3);
		});
 
	},
	collapseAlert : function() {
	$('.an_pge_home-alert').animate({
			"height": '60px'
		}, 500, function() {
			// Animation complete.
			$('.an_c56_homep_alert_count').css('top','20px');
		});

	}
});
