
var ValeroCST = {
	myInterval: [],
	incrementer: []
};

	$(function() {
		
		
		var requestUri = _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/GetByTitle('HomePageImages')/items?$orderby=Ordinal&$top=5";		
		//trasmit GET request to SP
		jqhxr = $.ajax({
        url: requestUri,
        type: 'GET',
        dataType: 'json',
        success: onSuccess,
        error: onFail,
        beforeSend: setHeader
    });
		
	});
	
	function onFail(errorObject, errorMessage) {
   		alert("Error: " + errorMessage + " - Status: " + errorObject.status.toString() + ", " + errorObject.statusText);
	}
	function onSuccess(data){
		
		$.each(data.d.results, function() {

				var li = $("<li class='dfwp-item' ><div class='item'><a class='item-link' href='#'><div><div class='image-area-left'>"+									"<img class='image' width='55' /></div><div class='right-col'><div class='item-title'></div><div class='description'></div></div></div></a></div>");
				li.find('.image').attr('src', this.ThumbnailImage.Url);
				li.find('.description').text(this.ThumbnailDescription);
				li.find('.item-title').text(this.ThumbnailTitle);		
				li.appendTo('#pagination');
		

		var slide = $("<div class='cst_slide' id='slide'><div class='slide-container'><div class='slide-headline' ></div><div class='slide-title'></div>"+
					"<div class='slide-contentbody'>MIKE TEST</div></div><div id='Units' style='display:none'></div></div>");	
		
		if (this.ShowCounterOverlay == true) {
				slide.find('#Units').text(this.IncrementPerSecond);
				slide.find('.slide-headline').text(Globalize.format( this.StartValue, "n0" ));
				slide.find('.slide-title').text(this.MainSlideTitle);
				slide.find('.slide-contentbody').html(this.MainSlideDescription).text();
			}
		else {
	
				slide.find('.slide-container').empty();		
					
		}	
		
		if(this.CSTVideoLink && this.CSTVideoLinkImage)
		{
			var a = '<div class="cst_videoLink"><a href="'+ this.CSTVideoLink.Url + '"><img src="'+ this.CSTVideoLinkImage.Url+ '" alt=""/></a></<div>';
			slide.append(a);
		}
		var bkg = "url('" + this.MainSlideImage.Url +"')";
		slide.css('background-image', bkg);
		
		slide.appendTo('.cst_stage');
		
			
			

		});
		
		var count = $('#pagination li').length;
		var height;
		switch(count)
		{
			case 3:
  			height = '140px';
  			break;
			case 4:
  			height = '100px';
  			break;
			case 5:
  			height = '80px';
  			break;
			default:
			height = '';
			break;
		}
		
			$('#pagination .item').css('height', height);
		
		RunSlides();
		StartSlideTicker(1)
	}
 

	function setHeader(xhr) {
   		 xhr.setRequestHeader("Accept", "application/json; odata=verbose");
	}
	
		

		
		function RunSlides(){
			$('#products').slides({
				preload: true,
				preloadImage: 'Unknown_83_filename'/*tpa=http://www.cstbrands.com/en-us/SiteAssets/loading.gif*/,
				effect: 'slide, fade',
				crossfade: true,
				slideSpeed: 350,
				fadeSpeed: 500,
				generateNextPrev: false,
				generatePagination: false,
				animationComplete: function(current){StartSlideTicker(current)}
             		});
		}
	
		
		
		function StartSlideTicker(current)
		{
			var startslide = 1;
		  	$(".cst_slide").each(function(){
			  	if(startslide == current)
			  	{
			  		window.clearInterval(ValeroCST.myInterval);
					ValeroCST.incrementer = $(this).find('.slide-headline');
					var units = $(this).find('#Units').text();
					var increment =  parseFloat(1);
					var seconds = 1000;
					seconds = seconds/units;	 
	 				ValeroCST.myInterval = setInterval(function(){
	 				StartTicker(increment)}, seconds);	
			  	}
			  	startslide++
		  	});

		}
	
		function Go(currentSlide)
		{
			window.clearInterval(ValeroCST.myInterval);
			ValeroCST.incrementer = $(currentSlide).find('.slide-headline');
			var units = $(currentSlide).find('#Units').text();
			var increment =  parseFloat(1);
			var seconds = 1000;
			seconds = seconds/units;	 
	 		ValeroCST.myInterval = setInterval(function(){
	 			StartTicker(increment)}, seconds);
			
		}
		
		function StartTicker(incrementPerSec)
		{
			var oldVal = $(ValeroCST.incrementer).text().replace(/,/g, '');
			var newVal = parseFloat(oldVal) + parseFloat(incrementPerSec);
			newVal = Globalize.format( newVal, "n0" );
				$(ValeroCST.incrementer).text(newVal);
			
		}