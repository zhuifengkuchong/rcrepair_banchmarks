
var MENU_POS11653558_GEN_PORT_TOP_NAV_MENU_919918912 = [
{
    // item sizes
    'height': 16,
    'width': 120,
    // menu block offset from the origin:
    //    for root level origin is upper left corner of the page
    //    for other levels origin is upper left corner of parent item
    'block_top': 0,
    'block_left': 0,
    // offsets between items of the same level
    'top': 0,
    'left': 0,
    // time in milliseconds before menu is hidden after cursor has gone out
    // of any items
    'hide_delay': 400,
    'expd_delay': 200,
    'css' : {
        'outer': ['m0l0oout', 'm0l0oover'],
        'inner': ['m0l0iout', 'm0l0iover']
    }
},
{
    'block_top': 16,
    'block_left': 0,
    'top': 16,
    'left': 0,
    'css': {
        'outer' : ['m0l1oout', 'm0l1oover'],
        'inner' : ['m0l1iout', 'm0l1iover']
    }
},
{
    'block_top': 0,
    'block_left': 120,
    'top': 16,
    'left': 0,
    'css': {
        'outer' : ['m0l2oout', 'm0l2oover'],
        'inner' : ['m0l2iout', 'm0l2iover']
    }
}
]
      
