// OPTIMOST PAGE & TRIAL CODE V2.7 - Copyright 2002-2011 Optimost LLC
var optimost={A:{},C:{},D:document,L:document.location,M:[],Q:{},T:new Date(),U:'',V:'2.7',Enabled:true,ST:"script",SA:
{"type":"text/javascript"},I:function(){var s=this.L.search;var c=this.D.cookie;if(s.length>3){for(var a=s.substring(1)
.split("&"),i=0,l=a.length;i<l;i++){var p=a[i].indexOf("=");if(p>0)this.Q[a[i].substring(0,p)]=unescape(a[i].substring(
p+1));}}if(c.length>3){for(var a=c.split(";"),i=0,b=a.length;i<b;i++){var v=a[i].split("=");while(v[0].substring(0,
1)==" ")v[0]=v[0].substring(1,v[0].length);if(v.length==2)this.C[v[0]]=unescape(v[1]);}}},B:function(){var n;this.A={
};var _o=this;this.A.D_ts=Math.round(_o.T.getTime()/1000);this.A.D_tzo=_o.T.getTimezoneOffset();this.A.D_loc=_o.L.protocol+
"//"+_o.L.hostname+_o.L.pathname;this.A.D_ckl=_o.D.cookie.length;this.A.D_ref=_o.D.referrer;if(typeof optrial=="object")
for(n in optrial)this.A[n]=optrial[n];for(n in this.Q)this.A[n]=this.Q[n];for(n in this.C)if(n.substring(0,2)=="op")this.A[n]=
this.C[n];},S:function(){var q='';for(var n in this.A)if(this.A[n]!=null && this.A[n]!="")q+=(q.length>0?"&":(this.U.indexOf(
"?")>0?"&":"?"))+n+"="+escape(this.A[n]);return this.U+q;},SC:function(n,v,e,d){var de=new Date();de.setTime(
de.getTime()+e * 1000);this.D.cookie=n+"="+escape(v)+((e==null)?"":("; expires="+de.toGMTString()))+"; path=/"+((d==
null)?"":(";domain="+d));},SLD:function(){var sld=this.D.domain;var dp=sld.split(".");var l=dp.length;if(l<2)sld=null;
else if(!isNaN(dp[l-1])&&!isNaN(dp[l-2]))sld=null;else sld="."+dp[l-2]+"."+dp[l-1];return sld;},R:function(r,c,d,
e){if(this.Enabled){var b=true;if(r<1000){b=(Math.floor(Math.random()*1000)<r);if(c!=null){if(this.C[c]!=null)b=(this.C[c]!=
"mvt-no");else this.SC(c,b?"mvt-yes":"mvt-no",e,d);}}if(b){var t='<'+this.ST+' src="'+this.S()+'"';for(n in this.SA)
t+=(" "+n+'="'+this.SA[n]+'"');t+='><\/'+this.ST+'>';this.D.write(t);}}},addModule:function(s,f){this.M[s]=f;
},displayModule:function(s){if(typeof this.M[s]=="function")this.M[s]();},hasModules:function(){return count(this.M)>0;
}};optimost.I();



// START AUTHOR TOOL CODE

// if this is a good browser, then let the good browser do the work.
if (document.querySelector)
{
    optimost.selectNode = function (selector)
    {
        return (selector) ? document.querySelector(selector) : null;
    };
}
// if this is not a good browser, we will do the work, but not nearly as well as the good browsers
// do it, but still better than the not-so-good browsers, since they aren't doing it at all.  also
// not as well certain frameworks which we are inexplicably not allowed to use, in case anyone was
// wondering why we don't just let a framework do the work.
else
{
    optimost.selectNode = (function ()
    {
        /**
        * Parses the given CSS selector into an array of individual selectors.
        *
        * @param selector the selector to parse
        *
        * @return an array of individual selectors
        **/
        function parse(selector)
        {
            var selectors = [];

            var inAttr = false;
            var inString = false;
            var escaped = false;
            var addChild = false;

            var start = -1;
            var end = -1;

            start = 0;
            for (var i = 0, s = selector.length; i < s; ++i)
            {
                var c = selector.charAt(i);
                // an attribute selector extends from a '[' to a ']'.
                if (inAttr)
                {
                    if (c === ']')
                    {
                        end = i;
                        inAttr = false;
                        // an attribute selector can't contain a closing ']' as far as I can tell,
                        // and if there is an unclosed string, it will be a syntax error later but
                        // we don't care right now.
                        inString = false;
                        escaped = false;
                    }
                }
                // a string extends from a "'" or '"' to the next unescaped occurence of the same.
                else if (inString)
                {
                    if (c === inString)
                    {
                        // the charater was escaped.
                        if (escaped)
                        {
                            escaped = false;
                        }
                        // the character was unescaped, end the string.
                        else
                        {
                            end = i;
                            inString = false;
                        }
                    }
                }
                // assume the escape character only applies to the current character.
                else if (escaped)
                {
                    escaped = false;
                }
                else
                {
                    switch (c)
                    {
                        // start an attribute selector.
                        case '[':
                            inAttr = true;
                        break;
                        // start a string.
                        case '\'':
                        case '"':
                            inString = c;
                        break;
                        // escape the next character.
                        case '\\':
                            escaped = true;
                        break;
                        // child selector.  everything before this should be added as a selector,
                        // but then the child selector should also be added.
                        case '>':
                            end = i - 1;
                            addChild = true;
                        break;
                    }
                }

                if (end !== -1)
                {
                    var next = selector.substring(start, end + 1).replace(/^\s+|\s+$/g, '');
                    if (next !== '')
                    {
                        selectors.push(next);
                    }

                    start = end + 1;
                    end = -1;

                    if (addChild)
                    {
                        selectors.push('>');
                        ++start;
                        addChild = false;
                    }
                }
            }

            if (start < selector.length)
            {
                selectors.push(selector.substring(start));
            }

            return selectors;
        }

        /** 
        * Returns true if the given element matches the specified nodeName, id, classes, and
        * attributes; false otherwise.
        *
        * @param element the element to test
        * @param nodeName the nodeName to match
        * @param id the id the element to match
        * @param classes the classes to match
        * @param attributes the attributes to match
        *
        * @return true if the given element matches the specified nodeName, id, classes, and
        * attributes; false otherwise
        **/ 
        function match(element, nodeName, id, classes, attributes)
        {
            if (nodeName)
            {
                // the node name must match exactly.  as an internal function, we know nodeName has
                // already been uppercased by something higher on the call stack.  and the element's
                // nodeName property is always returned in uppercase.
                if (element.nodeName !== nodeName)
                {
                    return false;
                }
            }
            if (id)
            {
                // the id must match exactly.
                if (element.id !== id)
                {
                    return false;
                }
            }
            if (classes)
            {
                // the className must contain each of the specified classes.
                var className = ' ' + element.className + ' ';
                for (var i = 0, s = classes.length; i < s; ++i)
                {
                    if (className.indexOf(' ' + classes[i] + ' ') === -1)
                    {
                        return false;
                    }
                }
            }
            if (attributes)
            {
                // the element must match each of the specified attributes.
                for (var i = 0, s = attributes.length; i < s; ++i)
                {
                    var attribute = attributes[i];
                    // if there is a test, use it to test the attribute value.
                    if (attribute.test)
                    {
                        var attributeValue = null;
                        switch (attribute.name)
                        {
                            case 'class':
                                attributeValue = element.className;
                            break;
                            case 'for':
                                attributeValue = element.htmlFor;
                            break;
                            default:
                                attributeValue = element.getAttribute(attribute.name);
                        }
                        switch (attribute.test)
                        {
                            // the attribute value must exactly match the specified value.
                            case '=':
                                if (attributeValue !== attribute.value)
                                {
                                    return false;
                                }
                            break;
                            // the attribute value must be a whitespace-separatd list of words, one
                            // of which must exactly match the specified value.  if the specified
                            // value is empty or contains whitespace, it will never match.
                            case '~=':
                                if ((attribute.value === '') || /\s/.test(attribute.value))
                                {
                                    return false;
                                }
                                attributeValue = ' ' + attributeValue + ' ';
                                if (attributeValue.indexOf(' ' + attribute.value + ' ') === -1)
                                {
                                    return false;
                                }
                            break;
                            // the attribute value must either exactly match the specified value, or
                            // must start with the specified value followed by '-'.
                            case '|=':
                                if ((attributeValue !== attribute.value) &&
                                    (attributeValue.substring(0, attribute.value.length + 1) !== (attribute.value + '-')))
                                {
                                    return false;
                                }
                            break;
                            // an unknown operator, assume it doesn't match.
                            default:
                                return false;
                        }
                    }
                    // if there isn't a test, test for the existence of the attribute, regardless of
                    // its value.
                    else
                    {
                        var hasAttribute = false;
                        if (element.hasAttribute)
                        {
                            hasAttribute = element.hasAttribute(attribute.name);
                        }
                        else if (element.getAttributeNode)
                        {
                            hasAttribute = element.getAttributeNode(attribute.name).specified;
                        }
                        if (!hasAttribute)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        /** 
        * Returns an array of the elements contained with the given element that match the specified
        * nodeName, id, classes, and attributes.
        *
        * @param element the element to filter
        * @param onlyChildren true to only consider direct children of the given element, false to
        * consider descendant elements at any depth
        * @param nodeName the nodeName to match
        * @param id the id to match
        * @param classes the classes to match
        * @param attributes the attributes to match
        *
        * @return an array of the elements contained with the given element that match the specified
        * nodeName, id, classes, and attributes
        **/ 
        function filter(element, onlyChildren, nodeName, id, classes, attributes)
        {
            var elements = null;
            if (onlyChildren)
            {
                elements = element.childNodes;
            }
            else
            {
                elements = element.getElementsByTagName(nodeName || '*');
                // if nodeName is not specified, getElementsByTagName('*') returns a collection of
                // all elements contained in the given element.  we don't care about the nodeName of
                // these elements, because nodeName wasn't specified.
                // if nodeName is specified, getElementsByTagName(nodeName) returns a collection of
                // elements that have that nodeName contained in the given element.  we don't care
                // about the nodeName of these elements, because we know each of these elements has
                // the specified nodeName, because that is how getElementsByTagName works.
                // either way, we don't need to manually filter by nodeName below, so null out the
                // reference make sure we don't check it.
                nodeName = null;
            }

            var matches = [];
            for (var i = 0, s = elements.length; i < s; ++i)
            {
                var subelement = elements[i];

                // only include the subelement if its nodeName, id, classes, and attributes match
                // the specified values.
                // the nodeType test is only useful when onlyChildren is true, because in that case
                // we are working with the childNodes collection.  when onlyChildren is false, we
                // are working with getElementsByTagName, which only contains elements, which are by
                // definition nodes whose nodeType is 1.
                if ((subelement.nodeType === 1) && match(subelement, nodeName, id, classes, attributes))
                {
                    matches.push(subelement);
                }
            }
            return matches;
        }

        var nodeNameSelectorRe = /^(\*|\w+)/;
        var idSelectorRe = /^#([\w-]+)/;
        var classSelectorRe = /^\.([\w-]+)/;
        var attributeSelectorRe = /^\[([\w-]+)\s*(([~|]?=)\s*['"]?(.*?)['"]?)?\]/;

        return function (selector)
        {
            if (!selector)
            {
                return null;
            }

            selector = parse(selector);

            var matches = [document];
            while (selector.length > 0)
            {
                var onlyChildren = false;
                var nodeName = null;
                var id = null;
                var classes = null;
                var attributes = null;

                var next = selector.shift();
                // the next selector applies only to children of the matched nodes.
                if (next === '>')
                {
                    next = selector.shift();
                    onlyChildren = true;
                }

                var match = null;

                // test for a particular node name.
                if (match = next.match(nodeNameSelectorRe))
                {
                    nodeName = match[1].toUpperCase();
                    next = next.substring(match[0].length);
                    // universal selector matches everything, so don't bother with it.
                    if (nodeName === '*')
                    {
                        nodeName = null;
                    }
                }

                // test for any number of id, class, and attribute selectors.
                while (true)
                {
                    var selectorKnown = false;

                    // test for an id selector.
                    while (match = next.match(idSelectorRe))
                    {
                        // if we already have an id, there must be at least two id selectors.  they
                        // can't both match unless they use the same id, because an element can have
                        // have one id.  so if they don't use the same id, we can abort immediately.
                        if (id && (id !== match[1]))
                        {
                            return null;
                        }
                        // otherwise, we set the id.  if we already have an id, it must be the same,
                        // so setting it again won't change anything.  and if we don't already have
                        // an id, we do now.
                        else
                        {
                            id = match[1];
                            next = next.substring(match[0].length);
                            selectorKnown = true;
                        }
                    }

                    // test for a class selector.
                    while (match = next.match(classSelectorRe))
                    {
                        if (!classes)
                        {
                            classes = [];
                        }
                        classes.push(match[1]);
                        next = next.substring(match[0].length);
                        selectorKnown = true;
                    }

                    // test for an attribute selector.
                    while (match = next.match(attributeSelectorRe))
                    {
                        if (!attributes)
                        {
                            attributes = [];
                        }
                        attributes.push({
                            name: match[1],
                            test: match[3],
                            value: match[4]
                        });
                        next = next.substring(match[0].length);
                        selectorKnown = true;
                    }

                    // we didn't know the selector.
                    if (!selectorKnown)
                    {
                        // if there were no selectors, we can safely continue processing.
                        if (next === '')
                        {
                            break;
                        }
                        // otherwise, it is guaranteed we won't match the selector, because we don't
                        // know what to do with it.
                        else
                        {
                            return null;
                        }
                    }
                }

                var submatches = [];
                for (var i = 0, s = matches.length; i < s; ++i)
                {
                    var element = matches[i];
                    submatches.push.apply(submatches, filter(element, onlyChildren, nodeName, id, classes, attributes));
                }
                matches = submatches;

                if (matches.length === 0)
                {
                    return null;
                }
            }

            return matches[0];
        };
    })();
}

// END AUTHOR TOOL CODE

$s={};
$s.path=(window.location&&window.location.protocol&&window.location.protocol.toLowerCase().indexOf('https')>-1)?'https://by.essl.optimost.com/':'http://es.optimost.com/';
$s.url='Unknown_83_filename'/*tpa=http://es.optimost.com/es/466/c/13/u/es/466/c/13/u/live.js*/;
$s.url_qa='Unknown_83_filename'/*tpa=http://es.optimost.com/es/466/c/13/u/es/466/c/13/u/staging.js*/;
if(typeof optimost=='object'){
      $s.qc=optimost.Q['opselect']||optimost.C['opselect']||'none';
      if($s.qc.toLowerCase()=='qa'){
            $s.url=$s.url_qa;
      }
      if($s.qc.toLowerCase()!='off'){
            document.write('<'+'script type="text/javascript" src="'+$s.path+$s.url+'"><\/'+'script>');
      }
}