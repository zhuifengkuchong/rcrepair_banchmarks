/**
 * @file
 * Brand camp likes javascript handlers
 */

(function ($) {
  /**
   * Loads dynamic links per user settings.
   */
  Drupal.behaviors.brand_camp_likes = {
    attach: function(context, settings) {
      $(window).load(function(){

        var isTouchDevice = 'ontouchstart' in document.documentElement;

        if (isTouchDevice) {
          //code for touch devices - tablets etc.
          $('.views-row').live("click", function(e) {

            if ($(this).find('article.node-bc-ugc-image').length == 0) {
              //this is not UGC tile. Process click right away
              console.log('redirect'); // don't remove please it makes iOS to really process click
              return true;
            }

            //In case it's a UGC tile
            if ($(this).hasClass('hover')) {
              // hover already visible. Process click
              return true;
            } else {
              // show hover. Do not process click
              $(this).addClass('hover');
              e.stopPropagation();
              return false;
            }
          });
        } else {
          // code for desktop
          $('.views-row')
            .live("mouseenter", function() { $(this).addClass('hover'); })
            .live("mouseleave", function() { $(this).removeClass('hover'); });
        }

        // Init spinner for loading.
        var spinner = new Spinner({color: '#fff', top: '42%'}).spin();

        Drupal.behaviors.brand_camp_likes.setLikesTiles();

        var tilesList = $("div.view-brand-camp-list div.view-content .node-bc-ugc-image");
        var likesList = tilesList.find('.node-content .field-name-field-bc-src-likes-count');
        $('body').delegate('.field-name-field-bc-src-likes-count', 'click', function clickLikeCallback( e ){
          e.stopPropagation();
          var _thisLike = $(this);
          var _thisTile = _thisLike.parent().parent();
          var _thisLikeField = _thisLike.find(".field-item");

          // Prevent clicking if exist loader.
          if (_thisLike.find('.spinner').length) {
            return;
          }
          _thisLike.append(spinner.el);

          var nodeId = _thisTile.attr('id').substr(5);
          var ajaxUrl = settings.basePath + 'brand-camp/chute-like/' + nodeId;

          if (!_thisTile.hasClass('chute-liked')) {
            $.ajax({
              url: ajaxUrl + '/add',
              dataType: 'json',
              error: function() {
                _thisLike.find('.spinner').remove();
              },
              success: function (data) {
                if (data) {
                  _thisTile.addClass('chute-liked');
                  _thisLikeField.html(data.count);
                  _thisLike.find('.spinner').remove();
                }
              }
            });
          } else {
            $.ajax({
              url: ajaxUrl + '/remove',
              dataType: 'json',
              error: function() {
                _thisLike.find('.spinner').remove();
              },
              success: function (data) {
                if (data) {
                  _thisTile.removeClass('chute-liked');
                  _thisLikeField.html(data.count);
                  _thisLike.find('.spinner').remove();
                }
              }
            });
          }
        });

        // Add behaviour on click 'load more'.
        $('#isopager').click(function(e) {
          Drupal.behaviors.brand_camp_likes.setLikesTiles();
        });
      });
    },
    setLikesTiles: function () {
      var chute_likes = jQuery.cookie('chute_likes');
      var chute_likes_json = {};
      if (chute_likes && chute_likes.length > 0) {
        try {
          chute_likes_json = JSON.parse(chute_likes);
        }
        catch(e){
          return;
        }

        for (var nid in chute_likes_json) {
          $("#node-" + nid).addClass('chute-liked');
        }
      }
    }
  };
})(jQuery);
;/**/
//fgnass.github.com/spin.js#v2.0.1
!function(a,b){"object"==typeof exports?module.exports=b():"function"==typeof define&&define.amd?define(b):a.Spinner=b()}(this,function(){"use strict";function a(a,b){var c,d=document.createElement(a||"div");for(c in b)d[c]=b[c];return d}function b(a){for(var b=1,c=arguments.length;c>b;b++)a.appendChild(arguments[b]);return a}function c(a,b,c,d){var e=["opacity",b,~~(100*a),c,d].join("-"),f=.01+c/d*100,g=Math.max(1-(1-a)/b*(100-f),a),h=j.substring(0,j.indexOf("Animation")).toLowerCase(),i=h&&"-"+h+"-"||"";return l[e]||(m.insertRule("@"+i+"keyframes "+e+"{0%{opacity:"+g+"}"+f+"%{opacity:"+a+"}"+(f+.01)+"%{opacity:1}"+(f+b)%100+"%{opacity:"+a+"}100%{opacity:"+g+"}}",m.cssRules.length),l[e]=1),e}function d(a,b){var c,d,e=a.style;for(b=b.charAt(0).toUpperCase()+b.slice(1),d=0;d<k.length;d++)if(c=k[d]+b,void 0!==e[c])return c;return void 0!==e[b]?b:void 0}function e(a,b){for(var c in b)a.style[d(a,c)||c]=b[c];return a}function f(a){for(var b=1;b<arguments.length;b++){var c=arguments[b];for(var d in c)void 0===a[d]&&(a[d]=c[d])}return a}function g(a,b){return"string"==typeof a?a:a[b%a.length]}function h(a){this.opts=f(a||{},h.defaults,n)}function i(){function c(b,c){return a("<"+b+' xmlns="urn:schemas-microsoft.com:vml" class="spin-vml">',c)}m.addRule(".spin-vml","behavior:url(#default#VML)"),h.prototype.lines=function(a,d){function f(){return e(c("group",{coordsize:k+" "+k,coordorigin:-j+" "+-j}),{width:k,height:k})}function h(a,h,i){b(m,b(e(f(),{rotation:360/d.lines*a+"deg",left:~~h}),b(e(c("roundrect",{arcsize:d.corners}),{width:j,height:d.width,left:d.radius,top:-d.width>>1,filter:i}),c("fill",{color:g(d.color,a),opacity:d.opacity}),c("stroke",{opacity:0}))))}var i,j=d.length+d.width,k=2*j,l=2*-(d.width+d.length)+"px",m=e(f(),{position:"absolute",top:l,left:l});if(d.shadow)for(i=1;i<=d.lines;i++)h(i,-2,"progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)");for(i=1;i<=d.lines;i++)h(i);return b(a,m)},h.prototype.opacity=function(a,b,c,d){var e=a.firstChild;d=d.shadow&&d.lines||0,e&&b+d<e.childNodes.length&&(e=e.childNodes[b+d],e=e&&e.firstChild,e=e&&e.firstChild,e&&(e.opacity=c))}}var j,k=["webkit","Moz","ms","O"],l={},m=function(){var c=a("style",{type:"text/css"});return b(document.getElementsByTagName("head")[0],c),c.sheet||c.styleSheet}(),n={lines:12,length:7,width:5,radius:10,rotate:0,corners:1,color:"#000",direction:1,speed:1,trail:100,opacity:.25,fps:20,zIndex:2e9,className:"spinner",top:"50%",left:"50%",position:"absolute"};h.defaults={},f(h.prototype,{spin:function(b){this.stop();{var c=this,d=c.opts,f=c.el=e(a(0,{className:d.className}),{position:d.position,width:0,zIndex:d.zIndex});d.radius+d.length+d.width}if(e(f,{left:d.left,top:d.top}),b&&b.insertBefore(f,b.firstChild||null),f.setAttribute("role","progressbar"),c.lines(f,c.opts),!j){var g,h=0,i=(d.lines-1)*(1-d.direction)/2,k=d.fps,l=k/d.speed,m=(1-d.opacity)/(l*d.trail/100),n=l/d.lines;!function o(){h++;for(var a=0;a<d.lines;a++)g=Math.max(1-(h+(d.lines-a)*n)%l*m,d.opacity),c.opacity(f,a*d.direction+i,g,d);c.timeout=c.el&&setTimeout(o,~~(1e3/k))}()}return c},stop:function(){var a=this.el;return a&&(clearTimeout(this.timeout),a.parentNode&&a.parentNode.removeChild(a),this.el=void 0),this},lines:function(d,f){function h(b,c){return e(a(),{position:"absolute",width:f.length+f.width+"px",height:f.width+"px",background:b,boxShadow:c,transformOrigin:"left",transform:"rotate("+~~(360/f.lines*k+f.rotate)+"deg) translate("+f.radius+"px,0)",borderRadius:(f.corners*f.width>>1)+"px"})}for(var i,k=0,l=(f.lines-1)*(1-f.direction)/2;k<f.lines;k++)i=e(a(),{position:"absolute",top:1+~(f.width/2)+"px",transform:f.hwaccel?"translate3d(0,0,0)":"",opacity:f.opacity,animation:j&&c(f.opacity,f.trail,l+k*f.direction,f.lines)+" "+1/f.speed+"s linear infinite"}),f.shadow&&b(i,e(h("#000","0 0 4px #000"),{top:"2px"})),b(d,b(i,h(g(f.color,k),"0 0 1px rgba(0,0,0,.1)")));return d},opacity:function(a,b,c){b<a.childNodes.length&&(a.childNodes[b].style.opacity=c)}});var o=e(a("group"),{behavior:"url(#default#VML)"});return!d(o,"transform")&&o.adj?i():j=d(o,"animation"),h});;/**/
/**
 * @file
 * Integration file for fancyBox module.
 */

(function($) {
  Drupal.behaviors.fancyBox = {
    attach: function(context, settings) {
      var selectors = ['.fancybox'];

      if (typeof settings.fancybox === 'undefined') {
        settings.fancybox = {};
      }

      if (typeof settings.fancybox.options === 'undefined') {
        settings.fancybox.options = {};
      }

      if (typeof settings.fancybox.callbacks !== 'undefined') {
        $.each(settings.fancybox.callbacks, function(i, cal) {
          settings.fancybox.options[i] = window[cal];
        });
      }

      if (typeof settings.fancybox.helpers !== 'undefined') {
        settings.fancybox.options.helpers = settings.fancybox.helpers;
        delete settings.fancybox.helpers;
      }

      if (typeof settings.fancybox.selectors !== 'undefined') {
        selectors = selectors.concat(settings.fancybox.selectors);
      }

      // Not needed because behaviors are reattached.
      settings.fancybox.options.live = false;

      $(selectors.join(',')).fancybox(settings.fancybox.options);
    }
  };
})(jQuery);
;/**/
/*! fancyBox v2.1.5 fancyapps.com | fancyapps.com/fancybox/#license */
(function(s,H,f,w){var K=f("html"),q=f(s),p=f(H),b=f.fancybox=function(){b.open.apply(this,arguments)},J=navigator.userAgent.match(/msie/i),C=null,t=H.createTouch!==w,u=function(a){return a&&a.hasOwnProperty&&a instanceof f},r=function(a){return a&&"string"===f.type(a)},F=function(a){return r(a)&&0<a.indexOf("%")},m=function(a,d){var e=parseInt(a,10)||0;d&&F(a)&&(e*=b.getViewport()[d]/100);return Math.ceil(e)},x=function(a,b){return m(a,b)+"px"};f.extend(b,{version:"2.1.5",defaults:{padding:15,margin:20,
width:800,height:600,minWidth:100,minHeight:100,maxWidth:9999,maxHeight:9999,pixelRatio:1,autoSize:!0,autoHeight:!1,autoWidth:!1,autoResize:!0,autoCenter:!t,fitToView:!0,aspectRatio:!1,topRatio:0.5,leftRatio:0.5,scrolling:"auto",wrapCSS:"",arrows:!0,closeBtn:!0,closeClick:!1,nextClick:!1,mouseWheel:!0,autoPlay:!1,playSpeed:3E3,preload:3,modal:!1,loop:!0,ajax:{dataType:"html",headers:{"X-fancyBox":!0}},iframe:{scrolling:"auto",preload:!0},swf:{wmode:"transparent",allowfullscreen:"true",allowscriptaccess:"always"},
keys:{next:{13:"left",34:"up",39:"left",40:"up"},prev:{8:"right",33:"down",37:"right",38:"down"},close:[27],play:[32],toggle:[70]},direction:{next:"left",prev:"right"},scrollOutside:!0,index:0,type:null,href:null,content:null,title:null,tpl:{wrap:'<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',image:'<img class="fancybox-image" src="{href}" alt="" />',iframe:'<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen'+
(J?' allowtransparency="true"':"")+"></iframe>",error:'<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',closeBtn:'<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"></a>',next:'<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',prev:'<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'},openEffect:"fade",openSpeed:250,openEasing:"swing",openOpacity:!0,
openMethod:"zoomIn",closeEffect:"fade",closeSpeed:250,closeEasing:"swing",closeOpacity:!0,closeMethod:"zoomOut",nextEffect:"elastic",nextSpeed:250,nextEasing:"swing",nextMethod:"changeIn",prevEffect:"elastic",prevSpeed:250,prevEasing:"swing",prevMethod:"changeOut",helpers:{overlay:!0,title:!0},onCancel:f.noop,beforeLoad:f.noop,afterLoad:f.noop,beforeShow:f.noop,afterShow:f.noop,beforeChange:f.noop,beforeClose:f.noop,afterClose:f.noop},group:{},opts:{},previous:null,coming:null,current:null,isActive:!1,
isOpen:!1,isOpened:!1,wrap:null,skin:null,outer:null,inner:null,player:{timer:null,isActive:!1},ajaxLoad:null,imgPreload:null,transitions:{},helpers:{},open:function(a,d){if(a&&(f.isPlainObject(d)||(d={}),!1!==b.close(!0)))return f.isArray(a)||(a=u(a)?f(a).get():[a]),f.each(a,function(e,c){var l={},g,h,k,n,m;"object"===f.type(c)&&(c.nodeType&&(c=f(c)),u(c)?(l={href:c.data("fancybox-href")||c.attr("href"),title:f("<div/>").text(c.data("fancybox-title")||c.attr("title")).html(),isDom:!0,element:c},
f.metadata&&f.extend(!0,l,c.metadata())):l=c);g=d.href||l.href||(r(c)?c:null);h=d.title!==w?d.title:l.title||"";n=(k=d.content||l.content)?"html":d.type||l.type;!n&&l.isDom&&(n=c.data("fancybox-type"),n||(n=(n=c.prop("class").match(/fancybox\.(\w+)/))?n[1]:null));r(g)&&(n||(b.isImage(g)?n="image":b.isSWF(g)?n="swf":"#"===g.charAt(0)?n="inline":r(c)&&(n="html",k=c)),"ajax"===n&&(m=g.split(/\s+/,2),g=m.shift(),m=m.shift()));k||("inline"===n?g?k=f(r(g)?g.replace(/.*(?=#[^\s]+$)/,""):g):l.isDom&&(k=c):
"html"===n?k=g:n||g||!l.isDom||(n="inline",k=c));f.extend(l,{href:g,type:n,content:k,title:h,selector:m});a[e]=l}),b.opts=f.extend(!0,{},b.defaults,d),d.keys!==w&&(b.opts.keys=d.keys?f.extend({},b.defaults.keys,d.keys):!1),b.group=a,b._start(b.opts.index)},cancel:function(){var a=b.coming;a&&!1===b.trigger("onCancel")||(b.hideLoading(),a&&(b.ajaxLoad&&b.ajaxLoad.abort(),b.ajaxLoad=null,b.imgPreload&&(b.imgPreload.onload=b.imgPreload.onerror=null),a.wrap&&a.wrap.stop(!0,!0).trigger("onReset").remove(),
b.coming=null,b.current||b._afterZoomOut(a)))},close:function(a){b.cancel();!1!==b.trigger("beforeClose")&&(b.unbindEvents(),b.isActive&&(b.isOpen&&!0!==a?(b.isOpen=b.isOpened=!1,b.isClosing=!0,f(".fancybox-item, .fancybox-nav").remove(),b.wrap.stop(!0,!0).removeClass("fancybox-opened"),b.transitions[b.current.closeMethod]()):(f(".fancybox-wrap").stop(!0).trigger("onReset").remove(),b._afterZoomOut())))},play:function(a){var d=function(){clearTimeout(b.player.timer)},e=function(){d();b.current&&b.player.isActive&&
(b.player.timer=setTimeout(b.next,b.current.playSpeed))},c=function(){d();p.unbind(".player");b.player.isActive=!1;b.trigger("onPlayEnd")};!0===a||!b.player.isActive&&!1!==a?b.current&&(b.current.loop||b.current.index<b.group.length-1)&&(b.player.isActive=!0,p.bind({"onCancel.player beforeClose.player":c,"onUpdate.player":e,"beforeLoad.player":d}),e(),b.trigger("onPlayStart")):c()},next:function(a){var d=b.current;d&&(r(a)||(a=d.direction.next),b.jumpto(d.index+1,a,"next"))},prev:function(a){var d=
b.current;d&&(r(a)||(a=d.direction.prev),b.jumpto(d.index-1,a,"prev"))},jumpto:function(a,d,e){var c=b.current;c&&(a=m(a),b.direction=d||c.direction[a>=c.index?"next":"prev"],b.router=e||"jumpto",c.loop&&(0>a&&(a=c.group.length+a%c.group.length),a%=c.group.length),c.group[a]!==w&&(b.cancel(),b._start(a)))},reposition:function(a,d){var e=b.current,c=e?e.wrap:null,l;c&&(l=b._getPosition(d),a&&"scroll"===a.type?(delete l.position,c.stop(!0,!0).animate(l,200)):(c.css(l),e.pos=f.extend({},e.dim,l)))},
update:function(a){var d=a&&a.originalEvent&&a.originalEvent.type,e=!d||"orientationchange"===d;e&&(clearTimeout(C),C=null);b.isOpen&&!C&&(C=setTimeout(function(){var c=b.current;c&&!b.isClosing&&(b.wrap.removeClass("fancybox-tmp"),(e||"load"===d||"resize"===d&&c.autoResize)&&b._setDimension(),"scroll"===d&&c.canShrink||b.reposition(a),b.trigger("onUpdate"),C=null)},e&&!t?0:300))},toggle:function(a){b.isOpen&&(b.current.fitToView="boolean"===f.type(a)?a:!b.current.fitToView,t&&(b.wrap.removeAttr("style").addClass("fancybox-tmp"),
b.trigger("onUpdate")),b.update())},hideLoading:function(){p.unbind(".loading");f("#fancybox-loading").remove()},showLoading:function(){var a,d;b.hideLoading();a=f('<div id="fancybox-loading"><div></div></div>').click(b.cancel).appendTo("body");p.bind("keydown.loading",function(a){27===(a.which||a.keyCode)&&(a.preventDefault(),b.cancel())});b.defaults.fixed||(d=b.getViewport(),a.css({position:"absolute",top:0.5*d.h+d.y,left:0.5*d.w+d.x}));b.trigger("onLoading")},getViewport:function(){var a=b.current&&
b.current.locked||!1,d={x:q.scrollLeft(),y:q.scrollTop()};a&&a.length?(d.w=a[0].clientWidth,d.h=a[0].clientHeight):(d.w=t&&s.innerWidth?s.innerWidth:q.width(),d.h=t&&s.innerHeight?s.innerHeight:q.height());return d},unbindEvents:function(){b.wrap&&u(b.wrap)&&b.wrap.unbind(".fb");p.unbind(".fb");q.unbind(".fb")},bindEvents:function(){var a=b.current,d;a&&(q.bind("orientationchange.fb"+(t?"":" http://www.wholefoodsmarket.com/sites/default/files/advagg_js/resize.fb")+(a.autoCenter&&!a.locked?" http://www.wholefoodsmarket.com/sites/default/files/advagg_js/scroll.fb":""),b.update),(d=a.keys)&&p.bind("http://www.wholefoodsmarket.com/sites/default/files/advagg_js/keydown.fb",function(e){var c=
e.which||e.keyCode,l=e.target||e.srcElement;if(27===c&&b.coming)return!1;e.ctrlKey||e.altKey||e.shiftKey||e.metaKey||l&&(l.type||f(l).is("[contenteditable]"))||f.each(d,function(d,l){if(1<a.group.length&&l[c]!==w)return b[d](l[c]),e.preventDefault(),!1;if(-1<f.inArray(c,l))return b[d](),e.preventDefault(),!1})}),f.fn.mousewheel&&a.mouseWheel&&b.wrap.bind("http://www.wholefoodsmarket.com/sites/default/files/advagg_js/mousewheel.fb",function(d,c,l,g){for(var h=f(d.target||null),k=!1;h.length&&!(k||h.is(".fancybox-skin")||h.is(".fancybox-wrap"));)k=h[0]&&!(h[0].style.overflow&&
"hidden"===h[0].style.overflow)&&(h[0].clientWidth&&h[0].scrollWidth>h[0].clientWidth||h[0].clientHeight&&h[0].scrollHeight>h[0].clientHeight),h=f(h).parent();0!==c&&!k&&1<b.group.length&&!a.canShrink&&(0<g||0<l?b.prev(0<g?"down":"left"):(0>g||0>l)&&b.next(0>g?"up":"right"),d.preventDefault())}))},trigger:function(a,d){var e,c=d||b.coming||b.current;if(c){f.isFunction(c[a])&&(e=c[a].apply(c,Array.prototype.slice.call(arguments,1)));if(!1===e)return!1;c.helpers&&f.each(c.helpers,function(d,e){if(e&&
b.helpers[d]&&f.isFunction(b.helpers[d][a]))b.helpers[d][a](f.extend(!0,{},b.helpers[d].defaults,e),c)})}p.trigger(a)},isImage:function(a){return r(a)&&a.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg)((\?|#).*)?$)/i)},isSWF:function(a){return r(a)&&a.match(/\.(swf)((\?|#).*)?$/i)},_start:function(a){var d={},e,c;a=m(a);e=b.group[a]||null;if(!e)return!1;d=f.extend(!0,{},b.opts,e);e=d.margin;c=d.padding;"number"===f.type(e)&&(d.margin=[e,e,e,e]);"number"===f.type(c)&&(d.padding=[c,c,
c,c]);d.modal&&f.extend(!0,d,{closeBtn:!1,closeClick:!1,nextClick:!1,arrows:!1,mouseWheel:!1,keys:null,helpers:{overlay:{closeClick:!1}}});d.autoSize&&(d.autoWidth=d.autoHeight=!0);"auto"===d.width&&(d.autoWidth=!0);"auto"===d.height&&(d.autoHeight=!0);d.group=b.group;d.index=a;b.coming=d;if(!1===b.trigger("beforeLoad"))b.coming=null;else{c=d.type;e=d.href;if(!c)return b.coming=null,b.current&&b.router&&"jumpto"!==b.router?(b.current.index=a,b[b.router](b.direction)):!1;b.isActive=!0;if("image"===
c||"swf"===c)d.autoHeight=d.autoWidth=!1,d.scrolling="visible";"image"===c&&(d.aspectRatio=!0);"iframe"===c&&t&&(d.scrolling="scroll");d.wrap=f(d.tpl.wrap).addClass("fancybox-"+(t?"mobile":"desktop")+" fancybox-type-"+c+" fancybox-tmp "+d.wrapCSS).appendTo(d.parent||"body");f.extend(d,{skin:f(".fancybox-skin",d.wrap),outer:f(".fancybox-outer",d.wrap),inner:f(".fancybox-inner",d.wrap)});f.each(["Top","Right","Bottom","Left"],function(a,b){d.skin.css("padding"+b,x(d.padding[a]))});b.trigger("onReady");
if("inline"===c||"html"===c){if(!d.content||!d.content.length)return b._error("content")}else if(!e)return b._error("href");"image"===c?b._loadImage():"ajax"===c?b._loadAjax():"iframe"===c?b._loadIframe():b._afterLoad()}},_error:function(a){f.extend(b.coming,{type:"html",autoWidth:!0,autoHeight:!0,minWidth:0,minHeight:0,scrolling:"no",hasError:a,content:b.coming.tpl.error});b._afterLoad()},_loadImage:function(){var a=b.imgPreload=new Image;a.onload=function(){this.onload=this.onerror=null;b.coming.width=
this.width/b.opts.pixelRatio;b.coming.height=this.height/b.opts.pixelRatio;b._afterLoad()};a.onerror=function(){this.onload=this.onerror=null;b._error("image")};a.src=b.coming.href;!0!==a.complete&&b.showLoading()},_loadAjax:function(){var a=b.coming;b.showLoading();b.ajaxLoad=f.ajax(f.extend({},a.ajax,{url:a.href,error:function(a,e){b.coming&&"abort"!==e?b._error("ajax",a):b.hideLoading()},success:function(d,e){"success"===e&&(a.content=d,b._afterLoad())}}))},_loadIframe:function(){var a=b.coming,
d=f(a.tpl.iframe.replace(/\{rnd\}/g,(new Date).getTime())).attr("scrolling",t?"auto":a.iframe.scrolling).attr("src",a.href);f(a.wrap).bind("onReset",function(){try{f(this).find("iframe").hide().attr("src","//about:blank").end().empty()}catch(a){}});a.iframe.preload&&(b.showLoading(),d.one("load",function(){f(this).data("ready",1);t||f(this).bind("http://www.wholefoodsmarket.com/sites/default/files/advagg_js/load.fb",b.update);f(this).parents(".fancybox-wrap").width("100%").removeClass("fancybox-tmp").show();b._afterLoad()}));a.content=d.appendTo(a.inner);a.iframe.preload||
b._afterLoad()},_preloadImages:function(){var a=b.group,d=b.current,e=a.length,c=d.preload?Math.min(d.preload,e-1):0,f,g;for(g=1;g<=c;g+=1)f=a[(d.index+g)%e],"image"===f.type&&f.href&&((new Image).src=f.href)},_afterLoad:function(){var a=b.coming,d=b.current,e,c,l,g,h;b.hideLoading();if(a&&!1!==b.isActive)if(!1===b.trigger("afterLoad",a,d))a.wrap.stop(!0).trigger("onReset").remove(),b.coming=null;else{d&&(b.trigger("beforeChange",d),d.wrap.stop(!0).removeClass("fancybox-opened").find(".fancybox-item, .fancybox-nav").remove());
b.unbindEvents();e=a.content;c=a.type;l=a.scrolling;f.extend(b,{wrap:a.wrap,skin:a.skin,outer:a.outer,inner:a.inner,current:a,previous:d});g=a.href;switch(c){case "inline":case "ajax":case "html":a.selector?e=f("<div>").html(e).find(a.selector):u(e)&&(e.data("fancybox-placeholder")||e.data("fancybox-placeholder",f('<div class="fancybox-placeholder"></div>').insertAfter(e).hide()),e=e.show().detach(),a.wrap.bind("onReset",function(){f(this).find(e).length&&e.hide().replaceAll(e.data("fancybox-placeholder")).data("fancybox-placeholder",
!1)}));break;case "image":e=a.tpl.image.replace(/\{href\}/g,g);break;case "swf":e='<object id="fancybox-swf" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%"><param name="movie" value="'+g+'"></param>',h="",f.each(a.swf,function(a,b){e+='<param name="'+a+'" value="'+b+'"></param>';h+=" "+a+'="'+b+'"'}),e+='<embed src="'+g+'" type="application/x-shockwave-flash" width="100%" height="100%"'+h+"></embed></object>"}u(e)&&e.parent().is(a.inner)||a.inner.append(e);b.trigger("beforeShow");
a.inner.css("overflow","yes"===l?"scroll":"no"===l?"hidden":l);b._setDimension();b.reposition();b.isOpen=!1;b.coming=null;b.bindEvents();if(!b.isOpened)f(".fancybox-wrap").not(a.wrap).stop(!0).trigger("onReset").remove();else if(d.prevMethod)b.transitions[d.prevMethod]();b.transitions[b.isOpened?a.nextMethod:a.openMethod]();b._preloadImages()}},_setDimension:function(){var a=b.getViewport(),d=0,e=!1,c=!1,e=b.wrap,l=b.skin,g=b.inner,h=b.current,c=h.width,k=h.height,n=h.minWidth,v=h.minHeight,p=h.maxWidth,
q=h.maxHeight,t=h.scrolling,r=h.scrollOutside?h.scrollbarWidth:0,y=h.margin,z=m(y[1]+y[3]),s=m(y[0]+y[2]),w,A,u,D,B,G,C,E,I;e.add(l).add(g).width("auto").height("auto").removeClass("fancybox-tmp");y=m(l.outerWidth(!0)-l.width());w=m(l.outerHeight(!0)-l.height());A=z+y;u=s+w;D=F(c)?(a.w-A)*m(c)/100:c;B=F(k)?(a.h-u)*m(k)/100:k;if("iframe"===h.type){if(I=h.content,h.autoHeight&&1===I.data("ready"))try{I[0].contentWindow.document.location&&(g.width(D).height(9999),G=I.contents().find("body"),r&&G.css("overflow-x",
"hidden"),B=G.outerHeight(!0))}catch(H){}}else if(h.autoWidth||h.autoHeight)g.addClass("fancybox-tmp"),h.autoWidth||g.width(D),h.autoHeight||g.height(B),h.autoWidth&&(D=g.width()),h.autoHeight&&(B=g.height()),g.removeClass("fancybox-tmp");c=m(D);k=m(B);E=D/B;n=m(F(n)?m(n,"w")-A:n);p=m(F(p)?m(p,"w")-A:p);v=m(F(v)?m(v,"h")-u:v);q=m(F(q)?m(q,"h")-u:q);G=p;C=q;h.fitToView&&(p=Math.min(a.w-A,p),q=Math.min(a.h-u,q));A=a.w-z;s=a.h-s;h.aspectRatio?(c>p&&(c=p,k=m(c/E)),k>q&&(k=q,c=m(k*E)),c<n&&(c=n,k=m(c/
E)),k<v&&(k=v,c=m(k*E))):(c=Math.max(n,Math.min(c,p)),h.autoHeight&&"iframe"!==h.type&&(g.width(c),k=g.height()),k=Math.max(v,Math.min(k,q)));if(h.fitToView)if(g.width(c).height(k),e.width(c+y),a=e.width(),z=e.height(),h.aspectRatio)for(;(a>A||z>s)&&c>n&&k>v&&!(19<d++);)k=Math.max(v,Math.min(q,k-10)),c=m(k*E),c<n&&(c=n,k=m(c/E)),c>p&&(c=p,k=m(c/E)),g.width(c).height(k),e.width(c+y),a=e.width(),z=e.height();else c=Math.max(n,Math.min(c,c-(a-A))),k=Math.max(v,Math.min(k,k-(z-s)));r&&"auto"===t&&k<B&&
c+y+r<A&&(c+=r);g.width(c).height(k);e.width(c+y);a=e.width();z=e.height();e=(a>A||z>s)&&c>n&&k>v;c=h.aspectRatio?c<G&&k<C&&c<D&&k<B:(c<G||k<C)&&(c<D||k<B);f.extend(h,{dim:{width:x(a),height:x(z)},origWidth:D,origHeight:B,canShrink:e,canExpand:c,wPadding:y,hPadding:w,wrapSpace:z-l.outerHeight(!0),skinSpace:l.height()-k});!I&&h.autoHeight&&k>v&&k<q&&!c&&g.height("auto")},_getPosition:function(a){var d=b.current,e=b.getViewport(),c=d.margin,f=b.wrap.width()+c[1]+c[3],g=b.wrap.height()+c[0]+c[2],c={position:"absolute",
top:c[0],left:c[3]};d.autoCenter&&d.fixed&&!a&&g<=e.h&&f<=e.w?c.position="fixed":d.locked||(c.top+=e.y,c.left+=e.x);c.top=x(Math.max(c.top,c.top+(e.h-g)*d.topRatio));c.left=x(Math.max(c.left,c.left+(e.w-f)*d.leftRatio));return c},_afterZoomIn:function(){var a=b.current;a&&((b.isOpen=b.isOpened=!0,b.wrap.css("overflow","visible").addClass("fancybox-opened"),b.update(),(a.closeClick||a.nextClick&&1<b.group.length)&&b.inner.css("cursor","pointer").bind("http://www.wholefoodsmarket.com/sites/default/files/advagg_js/click.fb",function(d){f(d.target).is("a")||f(d.target).parent().is("a")||
(d.preventDefault(),b[a.closeClick?"close":"next"]())}),a.closeBtn&&f(a.tpl.closeBtn).appendTo(b.skin).bind("http://www.wholefoodsmarket.com/sites/default/files/advagg_js/click.fb",function(a){a.preventDefault();b.close()}),a.arrows&&1<b.group.length&&((a.loop||0<a.index)&&f(a.tpl.prev).appendTo(b.outer).bind("http://www.wholefoodsmarket.com/sites/default/files/advagg_js/click.fb",b.prev),(a.loop||a.index<b.group.length-1)&&f(a.tpl.next).appendTo(b.outer).bind("http://www.wholefoodsmarket.com/sites/default/files/advagg_js/click.fb",b.next)),b.trigger("afterShow"),a.loop||a.index!==a.group.length-1)?b.opts.autoPlay&&!b.player.isActive&&(b.opts.autoPlay=!1,b.play(!0)):b.play(!1))},
_afterZoomOut:function(a){a=a||b.current;f(".fancybox-wrap").trigger("onReset").remove();f.extend(b,{group:{},opts:{},router:!1,current:null,isActive:!1,isOpened:!1,isOpen:!1,isClosing:!1,wrap:null,skin:null,outer:null,inner:null});b.trigger("afterClose",a)}});b.transitions={getOrigPosition:function(){var a=b.current,d=a.element,e=a.orig,c={},f=50,g=50,h=a.hPadding,k=a.wPadding,n=b.getViewport();!e&&a.isDom&&d.is(":visible")&&(e=d.find("img:first"),e.length||(e=d));u(e)?(c=e.offset(),e.is("img")&&
(f=e.outerWidth(),g=e.outerHeight())):(c.top=n.y+(n.h-g)*a.topRatio,c.left=n.x+(n.w-f)*a.leftRatio);if("fixed"===b.wrap.css("position")||a.locked)c.top-=n.y,c.left-=n.x;return c={top:x(c.top-h*a.topRatio),left:x(c.left-k*a.leftRatio),width:x(f+k),height:x(g+h)}},step:function(a,d){var e,c,f=d.prop;c=b.current;var g=c.wrapSpace,h=c.skinSpace;if("width"===f||"height"===f)e=d.end===d.start?1:(a-d.start)/(d.end-d.start),b.isClosing&&(e=1-e),c="width"===f?c.wPadding:c.hPadding,c=a-c,b.skin[f](m("width"===
f?c:c-g*e)),b.inner[f](m("width"===f?c:c-g*e-h*e))},zoomIn:function(){var a=b.current,d=a.pos,e=a.openEffect,c="elastic"===e,l=f.extend({opacity:1},d);delete l.position;c?(d=this.getOrigPosition(),a.openOpacity&&(d.opacity=0.1)):"fade"===e&&(d.opacity=0.1);b.wrap.css(d).animate(l,{duration:"none"===e?0:a.openSpeed,easing:a.openEasing,step:c?this.step:null,complete:b._afterZoomIn})},zoomOut:function(){var a=b.current,d=a.closeEffect,e="elastic"===d,c={opacity:0.1};e&&(c=this.getOrigPosition(),a.closeOpacity&&
(c.opacity=0.1));b.wrap.animate(c,{duration:"none"===d?0:a.closeSpeed,easing:a.closeEasing,step:e?this.step:null,complete:b._afterZoomOut})},changeIn:function(){var a=b.current,d=a.nextEffect,e=a.pos,c={opacity:1},f=b.direction,g;e.opacity=0.1;"elastic"===d&&(g="down"===f||"up"===f?"top":"left","down"===f||"right"===f?(e[g]=x(m(e[g])-200),c[g]="+=200px"):(e[g]=x(m(e[g])+200),c[g]="-=200px"));"none"===d?b._afterZoomIn():b.wrap.css(e).animate(c,{duration:a.nextSpeed,easing:a.nextEasing,complete:b._afterZoomIn})},
changeOut:function(){var a=b.previous,d=a.prevEffect,e={opacity:0.1},c=b.direction;"elastic"===d&&(e["down"===c||"up"===c?"top":"left"]=("up"===c||"left"===c?"-":"+")+"=200px");a.wrap.animate(e,{duration:"none"===d?0:a.prevSpeed,easing:a.prevEasing,complete:function(){f(this).trigger("onReset").remove()}})}};b.helpers.overlay={defaults:{closeClick:!0,speedOut:200,showEarly:!0,css:{},locked:!t,fixed:!0},overlay:null,fixed:!1,el:f("html"),create:function(a){var d;a=f.extend({},this.defaults,a);this.overlay&&
this.close();d=b.coming?b.coming.parent:a.parent;this.overlay=f('<div class="fancybox-overlay"></div>').appendTo(d&&d.lenth?d:"body");this.fixed=!1;a.fixed&&b.defaults.fixed&&(this.overlay.addClass("fancybox-overlay-fixed"),this.fixed=!0)},open:function(a){var d=this;a=f.extend({},this.defaults,a);this.overlay?this.overlay.unbind(".overlay").width("auto").height("auto"):this.create(a);this.fixed||(q.bind("resize.overlay",f.proxy(this.update,this)),this.update());a.closeClick&&this.overlay.bind("click.overlay",
function(a){if(f(a.target).hasClass("fancybox-overlay"))return b.isActive?b.close():d.close(),!1});this.overlay.css(a.css).show()},close:function(){q.unbind("resize.overlay");this.el.hasClass("fancybox-lock")&&(f(".fancybox-margin").removeClass("fancybox-margin"),this.el.removeClass("fancybox-lock"),q.scrollTop(this.scrollV).scrollLeft(this.scrollH));f(".fancybox-overlay").remove().hide();f.extend(this,{overlay:null,fixed:!1})},update:function(){var a="100%",b;this.overlay.width(a).height("100%");
J?(b=Math.max(H.documentElement.offsetWidth,H.body.offsetWidth),p.width()>b&&(a=p.width())):p.width()>q.width()&&(a=p.width());this.overlay.width(a).height(p.height())},onReady:function(a,b){var e=this.overlay;f(".fancybox-overlay").stop(!0,!0);e||this.create(a);a.locked&&this.fixed&&b.fixed&&(b.locked=this.overlay.append(b.wrap),b.fixed=!1);!0===a.showEarly&&this.beforeShow.apply(this,arguments)},beforeShow:function(a,b){b.locked&&!this.el.hasClass("fancybox-lock")&&(!1!==this.fixPosition&&f("*").filter(function(){return"fixed"===
f(this).css("position")&&!f(this).hasClass("fancybox-overlay")&&!f(this).hasClass("fancybox-wrap")}).addClass("fancybox-margin"),this.el.addClass("fancybox-margin"),this.scrollV=q.scrollTop(),this.scrollH=q.scrollLeft(),this.el.addClass("fancybox-lock"),q.scrollTop(this.scrollV).scrollLeft(this.scrollH));this.open(a)},onUpdate:function(){this.fixed||this.update()},afterClose:function(a){this.overlay&&!b.coming&&this.overlay.fadeOut(a.speedOut,f.proxy(this.close,this))}};b.helpers.title={defaults:{type:"float",
position:"bottom"},beforeShow:function(a){var d=b.current,e=d.title,c=a.type;f.isFunction(e)&&(e=e.call(d.element,d));if(r(e)&&""!==f.trim(e)){d=f('<div class="fancybox-title fancybox-title-'+c+'-wrap">'+e+"</div>");switch(c){case "inside":c=b.skin;break;case "outside":c=b.wrap;break;case "over":c=b.inner;break;default:c=b.skin,d.appendTo("body"),J&&d.width(d.width()),d.wrapInner('<span class="child"></span>'),b.current.margin[2]+=Math.abs(m(d.css("margin-bottom")))}d["top"===a.position?"prependTo":
"appendTo"](c)}}};f.fn.fancybox=function(a){var d,e=f(this),c=this.selector||"",l=function(g){var h=f(this).blur(),k=d,l,m;g.ctrlKey||g.altKey||g.shiftKey||g.metaKey||h.is(".fancybox-wrap")||(l=a.groupAttr||"data-fancybox-group",m=h.attr(l),m||(l="rel",m=h.get(0)[l]),m&&""!==m&&"nofollow"!==m&&(h=c.length?f(c):e,h=h.filter("["+l+'="'+m+'"]'),k=h.index(this)),a.index=k,!1!==b.open(h,a)&&g.preventDefault())};a=a||{};d=a.index||0;c&&!1!==a.live?p.undelegate(c,"click.fb-start").delegate(c+":not('.fancybox-item, .fancybox-nav')",
"click.fb-start",l):e.unbind("click.fb-start").bind("click.fb-start",l);this.filter("[data-fancybox-start=1]").trigger("click");return this};p.ready(function(){var a,d;f.scrollbarWidth===w&&(f.scrollbarWidth=function(){var a=f('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo("body"),b=a.children(),b=b.innerWidth()-b.height(99).innerWidth();a.remove();return b});f.support.fixedPosition===w&&(f.support.fixedPosition=function(){var a=f('<div style="position:fixed;top:20px;"></div>').appendTo("body"),
b=20===a[0].offsetTop||15===a[0].offsetTop;a.remove();return b}());f.extend(b.defaults,{scrollbarWidth:f.scrollbarWidth(),fixed:f.support.fixedPosition,parent:f("body")});a=f(s).width();K.addClass("fancybox-lock-test");d=f(s).width();K.removeClass("fancybox-lock-test");f("<style type='text/css'>.fancybox-margin{margin-right:"+(d-a)+"px;}</style>").appendTo("head")})})(window,document,jQuery);;/**/
/*! Copyright (c) 2013 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.1.3
 *
 * Requires: 1.2.2+
 */
;(function(c){"function"===typeof define&&define.amd?define(["jquery"],c):"object"===typeof exports?module.exports=c:c(jQuery)})(function(c){function l(b){var a=b||window.event,h=[].slice.call(arguments,1),d=0,e=0,f=0,g=0,g=0;b=c.event.fix(a);b.type="mousewheel";a.wheelDelta&&(d=a.wheelDelta);a.detail&&(d=-1*a.detail);a.deltaY&&(d=f=-1*a.deltaY);a.deltaX&&(e=a.deltaX,d=-1*e);void 0!==a.wheelDeltaY&&(f=a.wheelDeltaY);void 0!==a.wheelDeltaX&&(e=-1*a.wheelDeltaX);g=Math.abs(d);if(!m||g<m)m=g;g=Math.max(Math.abs(f),
Math.abs(e));if(!k||g<k)k=g;a=0<d?"floor":"ceil";d=Math[a](d/m);e=Math[a](e/k);f=Math[a](f/k);try{b.originalEvent.hasOwnProperty("wheelDelta")}catch(l){f=d}h.unshift(b,d,e,f);return(c.event.dispatch||c.event.handle).apply(this,h)}var n=["wheel","mousewheel","DOMMouseScroll","MozMousePixelScroll"],h="onwheel"in document||9<=document.documentMode?["wheel"]:["mousewheel","DomMouseScroll","MozMousePixelScroll"],m,k;if(c.event.fixHooks)for(var p=n.length;p;)c.event.fixHooks[n[--p]]=c.event.mouseHooks;
c.event.special.mousewheel={setup:function(){if(this.addEventListener)for(var b=h.length;b;)this.addEventListener(h[--b],l,!1);else this.onmousewheel=l},teardown:function(){if(this.removeEventListener)for(var b=h.length;b;)this.removeEventListener(h[--b],l,!1);else this.onmousewheel=null}};c.fn.extend({mousewheel:function(b){return b?this.bind("mousewheel",b):this.trigger("mousewheel")},unmousewheel:function(b){return this.unbind("mousewheel",b)}})});;/**/
var Lightvideo;(function(){Lightvideo={startVideo:function(e){Lightvideo.checkKnownVideos(e)||(e.match(/\.mov$/i)?Lightbox.modalHTML=navigator.plugins&&navigator.plugins.length?'<object id="qtboxMovie" type="video/quicktime" codebase="http://www.apple.com/qtactivex/qtplugin.cab" data="'+e+'" width="'+Lightbox.modalWidth+'" height="'+Lightbox.modalHeight+'"><param name="allowFullScreen" value="true"></param><param name="src" value="'+e+'" /><param name="scale" value="aspect" /><param name="controller" value="true" /><param name="autoplay" value="true" /><param name="bgcolor" value="#000000" /><param name="enablejavascript" value="true" /></object>':'<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab" width="'+Lightbox.modalWidth+'" height="'+Lightbox.modalHeight+'" id="qtboxMovie"><param name="allowFullScreen" value="true"></param><param name="src" value="'+e+'" /><param name="scale" value="aspect" /><param name="controller" value="true" /><param name="autoplay" value="true" /><param name="bgcolor" value="#000000" /><param name="enablejavascript" value="true" /></object>':e.match(/\.wmv$/i)||e.match(/\.asx$/i)?Lightbox.modalHTML='<object NAME="Player" WIDTH="'+Lightbox.modalWidth+'" HEIGHT="'+Lightbox.modalHeight+'" align="left" hspace="0" type="application/x-oleobject" CLASSID="CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6"><param name="allowFullScreen" value="true"></param><param NAME="URL" VALUE="'+e+'"></param><param NAME="AUTOSTART" VALUE="true"></param><param name="showControls" value="true"></param><embed WIDTH="'+Lightbox.modalWidth+'" HEIGHT="'+Lightbox.modalHeight+'" align="left" hspace="0" SRC="'+e+'" TYPE="application/x-oleobject" AUTOSTART="false"></embed></object>':(Lightbox.videoId=e,variables="",e.match(/\.swf$/i)||(e=Lightbox.flvPlayer+"?file="+e,Lightbox.flvFlashvars.length&&(variables=Lightbox.flvFlashvars)),Lightvideo.createEmbed(e,"flvplayer","#ffffff",variables)))},createEmbed:function(e,t,i,n){var s='bgcolor="'+i+'"',o="";n&&(o='flashvars="'+n+'"'),Lightbox.modalHTML='<embed type="application/x-shockwave-flash" src="'+e+'" '+'id="'+t+'" name="'+t+'" '+s+" "+'quality="high" wmode="transparent" '+o+" "+'height="'+Lightbox.modalHeight+'" '+'width="'+Lightbox.modalWidth+'" '+'allowfullscreen="true" '+"></embed>"},checkKnownVideos:function(e){return Lightvideo.checkYouTubeVideo(e)||Lightvideo.checkGoogleVideo(e)||Lightvideo.checkMySpaceVideo(e)||Lightvideo.checkLiveVideo(e)||Lightvideo.checkMetacafeVideo(e)||Lightvideo.checkIFilmSpikeVideo(e)?!0:!1},checkYouTubeVideo:function(e){for(var t=['youtube.com/v/([^"&]+)','youtube.com/watch\\?v=([^"&]+)','youtube.com/\\?v=([^"&]+)'],i=0;t.length>i;i++){var n=RegExp(t[i],"i"),s=n.exec(e);if(null!==s){Lightbox.videoId=s[1];var e="http://www.youtube.com/v/"+Lightbox.videoId,o="fs=1";return Lightbox.flvFlashvars.length&&(o=o+"&"+Lightbox.flvFlashvars,e=e+"&"+o),Lightvideo.createEmbed(e,"flvvideo","#ffffff",o),!0}}return!1},checkGoogleVideo:function(e){for(var t=["http://video.google.[a-z]{2,4}/googleplayer.swf//?docId=(-?//d*)","http://video.google.[a-z]{2,4}/videoplay\\?docid=([^&]*)&","http://video.google.[a-z]{2,4}/videoplay//?docid=(.*)"],i=0;t.length>i;i++){var n=RegExp(t[i],"i"),s=n.exec(e);if(null!==s){Lightbox.videoId=s[1];var e="http://video.google.com/googleplayer.swf?docId="+Lightbox.videoId+"&hl=en",o="fs=true";return Lightbox.flvFlashvars.length&&(o=o+"&"+Lightbox.flvFlashvars,e=e+"&"+o),Lightvideo.createEmbed(e,"flvvideo","#ffffff",o),!0}}return!1},checkMetacafeVideo:function(e){for(var t=["metacafe.com/watch/(.[^/]*)/(.[^/]*)/","metacafe.com/watch/(.[^/]*)/(.*)","metacafe.com/fplayer/(.[^/]*)/(.[^.]*)."],i=0;t.length>i;i++){var n=RegExp(t[i],"i"),s=n.exec(e);if(null!==s)return Lightbox.videoId=s[1],Lightvideo.createEmbed("http://www.metacafe.com/fplayer/"+Lightbox.videoId+"/.swf","flvvideo","#ffffff"),!0}return!1},checkIFilmSpikeVideo:function(e){for(var t=['spike.com/video/[^/&"]*?/(\\d+)','ifilm.com/video/[^/&"]*?/(\\d+)','spike.com/video/([^/&"]*)','ifilm.com/video/([^/&"]*)'],i=0;t.length>i;i++){var n=RegExp(t[i],"i"),s=n.exec(e);if(null!==s)return Lightbox.videoId=s[1],Lightvideo.createEmbed("http://www.spike.com/efp","flvvideo","#000","flvbaseclip="+Lightbox.videoId+"&amp;"),!0}return!1},checkMySpaceVideo:function(e){for(var t=['src="myspace.com/index.cfm\\?fuseaction=vids.individual&videoid=([^&"]+)','myspace.com/index.cfm\\?fuseaction=vids.individual&videoid=([^&"]+)','src="myspacetv.com/index.cfm\\?fuseaction=vids.individual&videoid=([^&"]+)"','myspacetv.com/index.cfm\\?fuseaction=vids.individual&videoid=([^&"]+)'],i=0;t.length>i;i++){var n=RegExp(t[i],"i"),s=n.exec(e);if(null!==s)return Lightbox.videoId=s[1],Lightvideo.createEmbed("../../../../../lads.myspace.com/videos/vplayer.swf"/*tpa=http://lads.myspace.com/videos/vplayer.swf*/,"flvvideo","#ffffff","m="+Lightbox.videoId),!0}return!1},checkLiveVideo:function(e){for(var t=['livevideo.com/flvplayer/embed/([^"]*)"',"livevideo.com/video/[^/]*?/([^/]*)/","livevideo.com/video/([^/]*)/"],i=0;t.length>i;i++){var n=RegExp(t[i],"i"),s=n.exec(e);if(null!==s)return Lightbox.videoId=s[1],Lightvideo.createEmbed("http://www.livevideo.com/flvplayer/embed/"+Lightbox.videoId,"flvvideo","#ffffff"),!0}return!1}}})(jQuery);;/**/
/**
 * jQuery Lightbox
 * @author
 *   Stella Power, <http://drupal.org/user/66894>
 *
 * Based on Lightbox v2.03.3 by Lokesh Dhakar
 * <http://www.huddletogether.com/projects/lightbox2/>
 * Also partially based on the jQuery Lightbox by Warren Krewenki
 *   <http://warren.mesozen.com>
 *
 * Permission has been granted to Mark Ashmead & other Drupal Lightbox2 module
 * maintainers to distribute this file via Drupal.org
 * Under GPL license.
 *
 * Slideshow, iframe and video functionality added by Stella Power.
 */
var Lightbox;!function(e){Lightbox={auto_modal:!1,overlayOpacity:.8,overlayColor:"000",disableCloseClick:!0,resizeSequence:0,resizeSpeed:"normal",fadeInSpeed:"normal",slideDownSpeed:"slow",minWidth:240,borderSize:10,boxColor:"fff",fontColor:"000",topPosition:"",infoHeight:20,alternative_layout:!1,imageArray:[],imageNum:null,total:0,activeImage:null,inprogress:!1,disableResize:!1,disableZoom:!1,isZoomedIn:!1,rtl:!1,loopItems:!1,keysClose:["c","x",27],keysPrevious:["p",37],keysNext:["n",39],keysZoom:["z"],keysPlayPause:[32],slideInterval:5e3,showPlayPause:!0,autoStart:!0,autoExit:!0,pauseOnNextClick:!1,pauseOnPrevClick:!0,slideIdArray:[],slideIdCount:0,isSlideshow:!1,isPaused:!1,loopSlides:!1,isLightframe:!1,iframe_width:600,iframe_height:400,iframe_border:1,enableVideo:!1,flvPlayer:"Unknown_83_filename"/*tpa=http://www.wholefoodsmarket.com/flvplayer.swf*/,flvFlashvars:"",isModal:!1,isVideo:!1,videoId:!1,modalWidth:400,modalHeight:400,modalHTML:null,initialize:function(){var i=Drupal.settings.lightbox2;Lightbox.overlayOpacity=i.overlay_opacity,Lightbox.overlayColor=i.overlay_color,Lightbox.disableCloseClick=i.disable_close_click,Lightbox.resizeSequence=i.resize_sequence,Lightbox.resizeSpeed=i.resize_speed,Lightbox.fadeInSpeed=i.fade_in_speed,Lightbox.slideDownSpeed=i.slide_down_speed,Lightbox.borderSize=i.border_size,Lightbox.boxColor=i.box_color,Lightbox.fontColor=i.font_color,Lightbox.topPosition=i.top_position,Lightbox.rtl=i.rtl,Lightbox.loopItems=i.loop_items,Lightbox.keysClose=i.keys_close.split(" "),Lightbox.keysPrevious=i.keys_previous.split(" "),Lightbox.keysNext=i.keys_next.split(" "),Lightbox.keysZoom=i.keys_zoom.split(" "),Lightbox.keysPlayPause=i.keys_play_pause.split(" "),Lightbox.disableResize=i.disable_resize,Lightbox.disableZoom=i.disable_zoom,Lightbox.slideInterval=i.slideshow_interval,Lightbox.showPlayPause=i.show_play_pause,Lightbox.showCaption=i.show_caption,Lightbox.autoStart=i.slideshow_automatic_start,Lightbox.autoExit=i.slideshow_automatic_exit,Lightbox.pauseOnNextClick=i.pause_on_next_click,Lightbox.pauseOnPrevClick=i.pause_on_previous_click,Lightbox.loopSlides=i.loop_slides,Lightbox.alternative_layout=i.use_alt_layout,Lightbox.iframe_width=i.iframe_width,Lightbox.iframe_height=i.iframe_height,Lightbox.iframe_border=i.iframe_border,Lightbox.enableVideo=i.enable_video,i.enable_video&&(Lightbox.flvPlayer=i.flvPlayer,Lightbox.flvFlashvars=i.flvFlashvars);var t=i.use_alt_layout?"lightbox2-alt-layout":"lightbox2-orig-layout",o='<div id="lightbox2-overlay" style="display: none;"></div>      <div id="lightbox" style="display: none;" class="'+t+'">        <div id="outerImageContainer"></div>        <div id="imageDataContainer" class="clearfix">          <div id="imageData"></div>        </div>      </div>',a='<div id="loading"><a href="#" id="loadingLink"></a></div>',h='<div id="modalContainer" style="display: none;"></div>',l='<div id="frameContainer" style="display: none;"></div>',g='<div id="imageContainer" style="display: none;"></div>',r='<div id="imageDetails"></div>',s='<div id="bottomNav"></div>',n='<img id="lightboxImage" alt="" />',d='<div id="hoverNav"><a id="prevLink" href="#"></a><a id="nextLink" href="#"></a></div>',b='<div id="frameHoverNav"><a id="framePrevLink" href="#"></a><a id="frameNextLink" href="#"></a></div>',d='<div id="hoverNav"><a id="prevLink" title="'+Drupal.t("Previous")+'" href="#"></a><a id="nextLink" title="'+Drupal.t("Next")+'" href="#"></a></div>',b='<div id="frameHoverNav"><a id="framePrevLink" title="'+Drupal.t("Previous")+'" href="#"></a><a id="frameNextLink" title="'+Drupal.t("Next")+'" href="#"></a></div>',x='<span id="caption"></span>',c='<span id="numberDisplay"></span>',m='<a id="bottomNavClose" title="'+Drupal.t("Close")+'" href="#"></a>',L='<a id="bottomNavZoom" href="#"></a>',u='<a id="bottomNavZoomOut" href="#"></a>',f='<a id="lightshowPause" title="'+Drupal.t("Pause Slideshow")+'" href="#" style="display: none;"></a>',v='<a id="lightshowPlay" title="'+Drupal.t("Play Slideshow")+'" href="#" style="display: none;"></a>';e("body").append(o),e("#outerImageContainer").append(h+l+g+a),i.use_alt_layout?(e("#outerImageContainer").append(s),e("#imageContainer").append(n),e("#bottomNav").append(m+L+u),e("#imageData").append(d+r),e("#imageDetails").append(x+c+f+v)):(e("#imageContainer").append(n+d),e("#imageData").append(r+s),e("#imageDetails").append(x+c),e("#bottomNav").append(b+m+L+u+f+v)),Lightbox.disableCloseClick&&e("#lightbox2-overlay").click(function(){return Lightbox.end(),!1}).hide(),e("#loadingLink, #bottomNavClose").click(function(){return Lightbox.end("forceClose"),!1}),e("#prevLink, #framePrevLink").click(function(){return Lightbox.changeData(Lightbox.activeImage-1),!1}),e("#nextLink, #frameNextLink").click(function(){return Lightbox.changeData(Lightbox.activeImage+1),!1}),e("#bottomNavZoom").click(function(){return Lightbox.changeData(Lightbox.activeImage,!0),!1}),e("#bottomNavZoomOut").click(function(){return Lightbox.changeData(Lightbox.activeImage,!1),!1}),e("#lightshowPause").click(function(){return Lightbox.togglePlayPause("lightshowPause","lightshowPlay"),!1}),e("#lightshowPlay").click(function(){return Lightbox.togglePlayPause("lightshowPlay","lightshowPause"),!1}),e("#prevLink, #nextLink, #framePrevLink, #frameNextLink").css({paddingTop:Lightbox.borderSize+"px"}),e("#imageContainer, #frameContainer, #modalContainer").css({padding:Lightbox.borderSize+"px"}),e("#outerImageContainer, #imageDataContainer, #bottomNavClose").css({backgroundColor:"#"+Lightbox.boxColor,color:"#"+Lightbox.fontColor}),Lightbox.alternative_layout?e("#bottomNavZoom, #bottomNavZoomOut").css({bottom:Lightbox.borderSize+"px",right:Lightbox.borderSize+"px"}):1==Lightbox.rtl&&e.browser.msie&&e("#bottomNavZoom, #bottomNavZoomOut").css({left:"0px"}),i.force_show_nav&&e("#prevLink, #nextLink").addClass("force_show_nav")},initList:function(i){(void 0==i||null==i)&&(i=document),e("a[rel^='lightbox']:not(.lightbox-processed), area[rel^='lightbox']:not(.lightbox-processed)",i).addClass("lightbox-processed").click(function(i){return Lightbox.disableCloseClick&&(e("#lightbox").unbind("click"),e("#lightbox").click(function(){Lightbox.end("forceClose")})),Lightbox.start(this,!1,!1,!1,!1),i.preventDefault&&i.preventDefault(),!1}),e("a[rel^='lightshow']:not(.lightbox-processed), area[rel^='lightshow']:not(.lightbox-processed)",i).addClass("lightbox-processed").click(function(i){return Lightbox.disableCloseClick&&(e("#lightbox").unbind("click"),e("#lightbox").click(function(){Lightbox.end("forceClose")})),Lightbox.start(this,!0,!1,!1,!1),i.preventDefault&&i.preventDefault(),!1}),e("a[rel^='lightframe']:not(.lightbox-processed), area[rel^='lightframe']:not(.lightbox-processed)",i).addClass("lightbox-processed").click(function(i){return Lightbox.disableCloseClick&&(e("#lightbox").unbind("click"),e("#lightbox").click(function(){Lightbox.end("forceClose")})),Lightbox.start(this,!1,!0,!1,!1),i.preventDefault&&i.preventDefault(),!1}),Lightbox.enableVideo&&e("a[rel^='lightvideo']:not(.lightbox-processed), area[rel^='lightvideo']:not(.lightbox-processed)",i).addClass("lightbox-processed").click(function(i){return Lightbox.disableCloseClick&&(e("#lightbox").unbind("click"),e("#lightbox").click(function(){Lightbox.end("forceClose")})),Lightbox.start(this,!1,!1,!0,!1),i.preventDefault&&i.preventDefault(),!1}),e("a[rel^='lightmodal']:not(.lightbox-processed), area[rel^='lightmodal']:not(.lightbox-processed)",i).addClass("lightbox-processed").click(function(i){return e("#lightbox").unbind("click"),e("#lightbox").addClass(e(this).attr("class")),e("#lightbox").removeClass("lightbox-processed"),Lightbox.start(this,!1,!1,!1,!0),i.preventDefault&&i.preventDefault(),!1}),e("#lightboxAutoModal:not(.lightbox-processed)",i).addClass("lightbox-processed").click(function(i){return Lightbox.auto_modal=!0,e("#lightbox").unbind("click"),Lightbox.start(this,!1,!1,!1,!0),i.preventDefault&&i.preventDefault(),!1})},start:function(i,t,o,a,h){Lightbox.isPaused=!Lightbox.autoStart,Lightbox.toggleSelectsFlash("hide");var l=Lightbox.getPageSize();e("#lightbox2-overlay").hide().css({width:"100%",zIndex:"10090",height:l[1]+"px",backgroundColor:"#"+Lightbox.overlayColor}),a&&this.detectMacFF2()?(e("#lightbox2-overlay").removeClass("overlay_default"),e("#lightbox2-overlay").addClass("overlay_macff2"),e("#lightbox2-overlay").css({opacity:null})):(e("#lightbox2-overlay").removeClass("overlay_macff2"),e("#lightbox2-overlay").addClass("overlay_default"),e("#lightbox2-overlay").css({opacity:Lightbox.overlayOpacity})),e("#lightbox2-overlay").fadeIn(Lightbox.fadeInSpeed),Lightbox.isSlideshow=t,Lightbox.isLightframe=o,Lightbox.isVideo=a,Lightbox.isModal=h,Lightbox.imageArray=[],Lightbox.imageNum=0;var g=e(i.tagName),r=null,s=Lightbox.parseRel(i),n=s.rel,d=s.group,b=s.title?s.title:i.title,x=null,c=0;s.flashvars&&(Lightbox.flvFlashvars=Lightbox.flvFlashvars+"&"+s.flashvars);var m=i.title;if(!m){var L=e(i).find("img");m=L&&e(L).attr("alt")?e(L).attr("alt"):b}if("lightboxAutoModal"==e(i).attr("id"))x=s.style,Lightbox.imageArray.push(["#lightboxAutoModal > *",b,m,x,1]);else if("lightbox"!=n&&"lightshow"!=n||d)if(d){for(c=0;c<g.length;c++)if(r=g[c],r.href&&"string"==typeof r.href&&e(r).attr("rel")){var u=Lightbox.parseRel(r),f=u.title?u.title:r.title;if(img_alt=r.title,!img_alt){var v=e(r).find("img");img_alt=v&&e(v).attr("alt")?e(v).attr("alt"):b}u.rel==n&&u.group==d&&((Lightbox.isLightframe||Lightbox.isModal||Lightbox.isVideo)&&(x=u.style),Lightbox.imageArray.push([r.href,f,img_alt,x]))}for(c=0;c<Lightbox.imageArray.length;c++)for(j=Lightbox.imageArray.length-1;j>c;j--)Lightbox.imageArray[c][0]==Lightbox.imageArray[j][0]&&Lightbox.imageArray.splice(j,1);for(;Lightbox.imageArray[Lightbox.imageNum][0]!=i.href;)Lightbox.imageNum++}else x=s.style,Lightbox.imageArray.push([i.href,b,m,x]);else Lightbox.imageArray.push([i.href,b,m]);Lightbox.isSlideshow&&Lightbox.showPlayPause&&Lightbox.isPaused&&(e("#lightshowPlay").show(),e("#lightshowPause").hide());var p=Lightbox.getPageScroll(),y=p[1]+1*(""==Lightbox.topPosition?l[3]/10:Lightbox.topPosition),w=p[0];e("#frameContainer, #modalContainer, #lightboxImage").hide(),e("#hoverNav, #prevLink, #nextLink, #frameHoverNav, #framePrevLink, #frameNextLink").hide(),e("#imageDataContainer, #numberDisplay, #bottomNavZoom, #bottomNavZoomOut").hide(),e("#outerImageContainer").css({width:"250px",height:"250px"}),e("#lightbox").css({zIndex:"10500",top:y+"px",left:w+"px"}).show(),Lightbox.total=Lightbox.imageArray.length,Lightbox.changeData(Lightbox.imageNum)},changeData:function(i,t){if(Lightbox.inprogress===!1){if(Lightbox.total>1&&(Lightbox.isSlideshow&&Lightbox.loopSlides||!Lightbox.isSlideshow&&Lightbox.loopItems)&&(i>=Lightbox.total&&(i=0),0>i&&(i=Lightbox.total-1)),Lightbox.isSlideshow)for(var o=0;o<Lightbox.slideIdCount;o++)window.clearTimeout(Lightbox.slideIdArray[o]);if(Lightbox.inprogress=!0,Lightbox.activeImage=i,Lightbox.disableResize&&!Lightbox.isSlideshow&&(t=!0),Lightbox.isZoomedIn=t,e("#loading").css({zIndex:"10500"}).show(),Lightbox.alternative_layout||e("#imageContainer").hide(),e("#frameContainer, #modalContainer, #lightboxImage").hide(),e("#hoverNav, #prevLink, #nextLink, #frameHoverNav, #framePrevLink, #frameNextLink").hide(),e("#imageDataContainer, #numberDisplay, #bottomNavZoom, #bottomNavZoomOut").hide(),Lightbox.isLightframe||Lightbox.isVideo||Lightbox.isModal){if(Lightbox.isLightframe){e("#lightbox #imageDataContainer").addClass("lightbox2-alt-layout-data");var a=Lightbox.imageArray[Lightbox.activeImage][0];e("#frameContainer").html('<iframe id="lightboxFrame" style="display: none;" src="'+a+'"></iframe>'),e.browser.mozilla&&-1!=a.indexOf(".swf")&&setTimeout(function(){document.getElementById("lightboxFrame").src=Lightbox.imageArray[Lightbox.activeImage][0]},1e3),Lightbox.iframe_border||(e("#lightboxFrame").css({border:"none"}),e("#lightboxFrame").attr("frameborder","0"));var h=document.getElementById("lightboxFrame"),l=Lightbox.imageArray[Lightbox.activeImage][3];h=Lightbox.setStyles(h,l),Lightbox.resizeContainer(parseInt(h.width,10),parseInt(h.height,10))}else if(Lightbox.isVideo||Lightbox.isModal){e("#lightbox #imageDataContainer").addClass("lightbox2-alt-layout-data");var g=document.getElementById("modalContainer"),r=Lightbox.imageArray[Lightbox.activeImage][3];g=Lightbox.setStyles(g,r),Lightbox.isVideo&&(Lightbox.modalHeight=parseInt(g.height,10)-10,Lightbox.modalWidth=parseInt(g.width,10)-10,Lightvideo.startVideo(Lightbox.imageArray[Lightbox.activeImage][0])),Lightbox.resizeContainer(parseInt(g.width,10),parseInt(g.height,10))}}else e("#lightbox #imageDataContainer").removeClass("lightbox2-alt-layout-data"),imgPreloader=new Image,imgPreloader.onerror=function(){Lightbox.imgNodeLoadingError(this)},imgPreloader.onload=function(){var i=document.getElementById("lightboxImage");i.src=Lightbox.imageArray[Lightbox.activeImage][0],i.alt=Lightbox.imageArray[Lightbox.activeImage][2];var o=imgPreloader.width,a=imgPreloader.height,h=Lightbox.getPageSize(),l={w:h[2]-2*Lightbox.borderSize,h:h[3]-6*Lightbox.borderSize-4*Lightbox.infoHeight-h[3]/10},g={w:imgPreloader.width,h:imgPreloader.height};if(t!==!0){var r=1;e("#bottomNavZoomOut, #bottomNavZoom").hide(),(g.w>=l.w||g.h>=l.h)&&g.h&&g.w&&(r=l.w/g.w<l.h/g.h?l.w/g.w:l.h/g.h,Lightbox.disableZoom||Lightbox.isSlideshow||e("#bottomNavZoom").css({zIndex:"10500"}).show()),o=Math.floor(g.w*r),a=Math.floor(g.h*r)}else e("#bottomNavZoom").hide(),(g.w>=l.w||g.h>=l.h)&&g.h&&g.w&&(Lightbox.disableResize||Lightbox.isSlideshow!==!1||Lightbox.disableZoom||e("#bottomNavZoomOut").css({zIndex:"10500"}).show());i.style.width=o+"px",i.style.height=a+"px",Lightbox.resizeContainer(o,a),imgPreloader.onload=function(){}},imgPreloader.src=Lightbox.imageArray[Lightbox.activeImage][0],imgPreloader.alt=Lightbox.imageArray[Lightbox.activeImage][2]}},imgNodeLoadingError:function(e){var i=Drupal.settings.lightbox2,t=Lightbox.imageArray[Lightbox.activeImage][0];""!==i.display_image_size&&(t=t.replace(new RegExp("."+i.display_image_size),"")),Lightbox.imageArray[Lightbox.activeImage][0]=t,e.onerror=function(){Lightbox.imgLoadingError(e)},e.src=t},imgLoadingError:function(e){var i=Drupal.settings.lightbox2;Lightbox.imageArray[Lightbox.activeImage][0]=i.default_image,e.src=i.default_image},resizeContainer:function(i,t){i=i<Lightbox.minWidth?Lightbox.minWidth:i,this.widthCurrent=e("#outerImageContainer").width(),this.heightCurrent=e("#outerImageContainer").height();var o=i+2*Lightbox.borderSize,a=t+2*Lightbox.borderSize;if(this.xScale=o/this.widthCurrent*100,this.yScale=a/this.heightCurrent*100,wDiff=this.widthCurrent-o,hDiff=this.heightCurrent-a,e("#modalContainer").css({width:i,height:t}),Lightbox.resizeSequence){var h={width:o},l={height:a};2==Lightbox.resizeSequence&&(h={height:a},l={width:o}),e("#outerImageContainer").animate(h,Lightbox.resizeSpeed).animate(l,Lightbox.resizeSpeed,"linear",function(){Lightbox.showData()})}else e("#outerImageContainer").animate({width:o,height:a},Lightbox.resizeSpeed,"linear",function(){Lightbox.showData()});0===hDiff&&0===wDiff&&Lightbox.pause(e.browser.msie?250:100);var g=Drupal.settings.lightbox2;g.use_alt_layout||e("#prevLink, #nextLink").css({height:t+"px"}),e("#imageDataContainer").css({width:o+"px"})},showData:function(){if(e("#loading").hide(),Lightbox.isLightframe||Lightbox.isVideo||Lightbox.isModal)if(Lightbox.updateDetails(),Lightbox.isLightframe)e("#frameContainer").show(),e.browser.safari||0===Lightbox.fadeInSpeed?e("#lightboxFrame").css({zIndex:"10500"}).show():e("#lightboxFrame").css({zIndex:"10500"}).fadeIn(Lightbox.fadeInSpeed);else if(Lightbox.isVideo)e("#modalContainer").html(Lightbox.modalHTML).click(function(){return!1}).css("zIndex","10500").show();else{var i=unescape(Lightbox.imageArray[Lightbox.activeImage][0]);Lightbox.imageArray[Lightbox.activeImage][4]?(e(i).appendTo("#modalContainer"),e("#modalContainer").css({zIndex:"10500"}).show()):e("#modalContainer").hide().load(i,function(){e("#modalContainer").css({zIndex:"10500"}).show()}),e("#modalContainer").unbind("click")}else e("#imageContainer").show(),e.browser.safari||0===Lightbox.fadeInSpeed?e("#lightboxImage").css({zIndex:"10500"}).show():e("#lightboxImage").css({zIndex:"10500"}).fadeIn(Lightbox.fadeInSpeed),Lightbox.updateDetails(),this.preloadNeighborImages();Lightbox.inprogress=!1,Lightbox.isSlideshow&&(Lightbox.loopSlides||Lightbox.activeImage!=Lightbox.total-1?!Lightbox.isPaused&&Lightbox.total>1&&(Lightbox.slideIdArray[Lightbox.slideIdCount++]=setTimeout(function(){Lightbox.changeData(Lightbox.activeImage+1)},Lightbox.slideInterval)):Lightbox.autoExit&&(Lightbox.slideIdArray[Lightbox.slideIdCount++]=setTimeout(function(){Lightbox.end("slideshow")},Lightbox.slideInterval)),Lightbox.showPlayPause&&Lightbox.total>1&&!Lightbox.isPaused?(e("#lightshowPause").show(),e("#lightshowPlay").hide()):Lightbox.showPlayPause&&Lightbox.total>1&&(e("#lightshowPause").hide(),e("#lightshowPlay").show()));var t=Lightbox.getPageSize(),o=Lightbox.getPageScroll(),a=t[1];if(Lightbox.isZoomedIn&&t[1]>t[3]){var h=1*(""==Lightbox.topPosition?t[3]/10:Lightbox.topPosition);a=a+o[1]+h}e("#lightbox2-overlay").css({height:a+"px",width:t[0]+"px"}),e.browser.mozilla&&-1!=Lightbox.imageArray[Lightbox.activeImage][0].indexOf(".pdf")&&setTimeout(function(){document.getElementById("lightboxFrame").src=Lightbox.imageArray[Lightbox.activeImage][0]},1e3)},updateDetails:function(){e("#imageDataContainer").hide();var i=Drupal.settings.lightbox2;if(i.show_caption){var t=Lightbox.filterXSS(Lightbox.imageArray[Lightbox.activeImage][1]);t||(t=""),e("#caption").html(t).css({zIndex:"10500"}).show()}var o=null;if(i.image_count&&Lightbox.total>1){var a=Lightbox.activeImage+1;o=Lightbox.isLightframe||Lightbox.isModal||Lightbox.isVideo?Lightbox.isVideo?i.video_count.replace(/\!current/,a).replace(/\!total/,Lightbox.total):i.page_count.replace(/\!current/,a).replace(/\!total/,Lightbox.total):i.image_count.replace(/\!current/,a).replace(/\!total/,Lightbox.total),e("#numberDisplay").html(o).css({zIndex:"10500"}).show()}else e("#numberDisplay").hide();e("#imageDataContainer").hide().slideDown(Lightbox.slideDownSpeed,function(){e("#bottomNav").show()}),1==Lightbox.rtl&&e("#bottomNav").css({"float":"left"}),Lightbox.updateNav()},updateNav:function(){e("#hoverNav").css({zIndex:"10500"}).show();var i="#prevLink",t="#nextLink";Lightbox.isSlideshow?(Lightbox.total>1&&Lightbox.loopSlides||0!==Lightbox.activeImage?e(i).css({zIndex:"10500"}).show().click(function(){return Lightbox.pauseOnPrevClick&&Lightbox.togglePlayPause("lightshowPause","lightshowPlay"),Lightbox.changeData(Lightbox.activeImage-1),!1}):e(i).hide(),Lightbox.total>1&&Lightbox.loopSlides||Lightbox.activeImage!=Lightbox.total-1?e(t).css({zIndex:"10500"}).show().click(function(){return Lightbox.pauseOnNextClick&&Lightbox.togglePlayPause("lightshowPause","lightshowPlay"),Lightbox.changeData(Lightbox.activeImage+1),!1}):e(t).hide()):((Lightbox.isLightframe||Lightbox.isModal||Lightbox.isVideo)&&!Lightbox.alternative_layout&&(e("#frameHoverNav").css({zIndex:"10500"}).show(),e("#hoverNav").css({zIndex:"10500"}).hide(),i="#framePrevLink",t="#frameNextLink"),Lightbox.total>1&&Lightbox.loopItems||0!==Lightbox.activeImage?e(i).css({zIndex:"10500"}).show().unbind().click(function(){return Lightbox.changeData(Lightbox.activeImage-1),!1}):e(i).hide(),Lightbox.total>1&&Lightbox.loopItems||Lightbox.activeImage!=Lightbox.total-1?e(t).css({zIndex:"10500"}).show().unbind().click(function(){return Lightbox.changeData(Lightbox.activeImage+1),!1}):e(t).hide()),Lightbox.isModal||this.enableKeyboardNav()},enableKeyboardNav:function(){e(document).bind("keydown",this.keyboardAction)},disableKeyboardNav:function(){e(document).unbind("keydown",this.keyboardAction)},keyboardAction:function(e){if(null===e?(keycode=event.keyCode,escapeKey=27):(keycode=e.keyCode,escapeKey=e.DOM_VK_ESCAPE),key=String.fromCharCode(keycode).toLowerCase(),Lightbox.checkKey(Lightbox.keysClose,key,keycode))Lightbox.end("forceClose");else if(Lightbox.checkKey(Lightbox.keysPrevious,key,keycode))(Lightbox.total>1&&(Lightbox.isSlideshow&&Lightbox.loopSlides||!Lightbox.isSlideshow&&Lightbox.loopItems)||0!==Lightbox.activeImage)&&Lightbox.changeData(Lightbox.activeImage-1);else if(Lightbox.checkKey(Lightbox.keysNext,key,keycode))(Lightbox.total>1&&(Lightbox.isSlideshow&&Lightbox.loopSlides||!Lightbox.isSlideshow&&Lightbox.loopItems)||Lightbox.activeImage!=Lightbox.total-1)&&Lightbox.changeData(Lightbox.activeImage+1);else{if(!(!Lightbox.checkKey(Lightbox.keysZoom,key,keycode)||Lightbox.disableResize||Lightbox.disableZoom||Lightbox.isSlideshow||Lightbox.isLightframe))return Lightbox.isZoomedIn?Lightbox.changeData(Lightbox.activeImage,!1):Lightbox.isZoomedIn||Lightbox.changeData(Lightbox.activeImage,!0),!1;if(Lightbox.checkKey(Lightbox.keysPlayPause,key,keycode)&&Lightbox.isSlideshow)return Lightbox.isPaused?Lightbox.togglePlayPause("lightshowPlay","lightshowPause"):Lightbox.togglePlayPause("lightshowPause","lightshowPlay"),!1}},preloadNeighborImages:function(){Lightbox.total-1>Lightbox.activeImage&&(preloadNextImage=new Image,preloadNextImage.src=Lightbox.imageArray[Lightbox.activeImage+1][0]),Lightbox.activeImage>0&&(preloadPrevImage=new Image,preloadPrevImage.src=Lightbox.imageArray[Lightbox.activeImage-1][0])},end:function(i){var t="slideshow"==i?!1:!0;if(!(Lightbox.isSlideshow&&Lightbox.isPaused&&!t||Lightbox.inprogress===!0&&"forceClose"!=i))if(Lightbox.disableKeyboardNav(),e("#lightbox").hide(),e("#lightbox2-overlay").fadeOut(),Lightbox.isPaused=!0,Lightbox.inprogress=!1,Lightbox.imageArray=[],Lightbox.imageNum=0,Lightbox.toggleSelectsFlash("visible"),Lightbox.isSlideshow){for(var o=0;o<Lightbox.slideIdCount;o++)window.clearTimeout(Lightbox.slideIdArray[o]);e("#lightshowPause, #lightshowPlay").hide()}else Lightbox.isLightframe?e("#frameContainer").empty().hide():(Lightbox.isVideo||Lightbox.isModal)&&(Lightbox.auto_modal||e("#modalContainer").hide().html(""),Lightbox.auto_modal=!1)},getPageScroll:function(){var e,i;return self.pageYOffset||self.pageXOffset?(i=self.pageYOffset,e=self.pageXOffset):document.documentElement&&(document.documentElement.scrollTop||document.documentElement.scrollLeft)?(i=document.documentElement.scrollTop,e=document.documentElement.scrollLeft):document.body&&(i=document.body.scrollTop,e=document.body.scrollLeft),arrayPageScroll=[e,i]},getPageSize:function(){var e,i;window.innerHeight&&window.scrollMaxY?(e=window.innerWidth+window.scrollMaxX,i=window.innerHeight+window.scrollMaxY):document.body.scrollHeight>document.body.offsetHeight?(e=document.body.scrollWidth,i=document.body.scrollHeight):(e=document.body.offsetWidth,i=document.body.offsetHeight);var t,o;return self.innerHeight?(t=document.documentElement.clientWidth?document.documentElement.clientWidth:self.innerWidth,o=self.innerHeight):document.documentElement&&document.documentElement.clientHeight?(t=document.documentElement.clientWidth,o=document.documentElement.clientHeight):document.body&&(t=document.body.clientWidth,o=document.body.clientHeight),pageHeight=o>i?o:i,pageWidth=t>e?e:t,arrayPageSize=new Array(pageWidth,pageHeight,t,o)},pause:function(e){var i=new Date,t=null;do t=new Date;while(e>t-i)},toggleSelectsFlash:function(i){"visible"==i?e("select.lightbox_hidden, embed.lightbox_hidden, object.lightbox_hidden").show():"hide"==i&&(e("select:visible, embed:visible, object:visible").not("#lightboxAutoModal select, #lightboxAutoModal embed, #lightboxAutoModal object").addClass("lightbox_hidden"),e("select.lightbox_hidden, embed.lightbox_hidden, object.lightbox_hidden").hide())},parseRel:function(i){var t=[];if(t.rel=t.title=t.group=t.style=t.flashvars=null,!e(i).attr("rel"))return t;if(t.rel=e(i).attr("rel").match(/\w+/)[0],e(i).attr("rel").match(/\[(.*)\]/)){var o=e(i).attr("rel").match(/\[(.*?)\]/)[1].split("|");t.group=o[0],t.style=o[1],void 0!=t.style&&t.style.match(/flashvars:\s?(.*?);/)&&(t.flashvars=t.style.match(/flashvars:\s?(.*?);/)[1])}return e(i).attr("rel").match(/\[.*\]\[(.*)\]/)&&(t.title=e(i).attr("rel").match(/\[.*\]\[(.*)\]/)[1]),t},setStyles:function(e,i){if(e.width=Lightbox.iframe_width,e.height=Lightbox.iframe_height,e.scrolling="auto",!i)return e;for(var t=i.split(";"),o=0;o<t.length;o++)if(t[o].indexOf("width:")>=0){var a=t[o].replace("width:","");e.width=jQuery.trim(a)}else if(t[o].indexOf("height:")>=0){var h=t[o].replace("height:","");e.height=jQuery.trim(h)}else if(t[o].indexOf("scrolling:")>=0){var l=t[o].replace("scrolling:","");e.scrolling=jQuery.trim(l)}else if(t[o].indexOf("overflow:")>=0){var g=t[o].replace("overflow:","");e.overflow=jQuery.trim(g)}return e},togglePlayPause:function(i,t){if(Lightbox.isSlideshow&&"lightshowPause"==i)for(var o=0;o<Lightbox.slideIdCount;o++)window.clearTimeout(Lightbox.slideIdArray[o]);e("#"+i).hide(),e("#"+t).show(),"lightshowPlay"==i?(Lightbox.isPaused=!1,Lightbox.loopSlides||Lightbox.activeImage!=Lightbox.total-1?Lightbox.total>1&&Lightbox.changeData(Lightbox.activeImage+1):Lightbox.end()):Lightbox.isPaused=!0},triggerLightbox:function(i,t){i.length&&(t&&t.length?e("a[rel^='"+i+"["+t+"]'], area[rel^='"+i+"["+t+"]']").eq(0).trigger("click"):e("a[rel^='"+i+"'], area[rel^='"+i+"']").eq(0).trigger("click"))},detectMacFF2:function(){var e=navigator.userAgent.toLowerCase();if(/firefox[\/\s](\d+\.\d+)/.test(e)){var i=new Number(RegExp.$1);if(3>i&&-1!=e.indexOf("mac"))return!0}return!1},checkKey:function(e,i,t){return-1!=jQuery.inArray(i,e)||-1!=jQuery.inArray(String(t),e)},filterXSS:function(i,t){var o="";return e.ajax({url:Drupal.settings.basePath+"?q=system/lightbox2/filter-xss",data:{string:i,allowed_tags:t},type:"POST",async:!1,dataType:"json",success:function(e){o=e}}),o}},Drupal.behaviors.initLightbox={attach:function(i){e("body:not(.lightbox-processed)",i).addClass("lightbox-processed").each(function(){return Lightbox.initialize(),!1}),Lightbox.initList(i),e("#lightboxAutoModal",i).triggerHandler("click")}}}(jQuery);;/**/

(function ($) {
  Drupal.Panels = Drupal.Panels || {};

  Drupal.Panels.autoAttach = function() {
    if ($.browser.msie) {
      // If IE, attach a hover event so we can see our admin links.
      $("div.panel-pane").hover(
        function() {
          $('div.panel-hide', this).addClass("panel-hide-hover"); return true;
        },
        function() {
          $('div.panel-hide', this).removeClass("panel-hide-hover"); return true;
        }
      );
      $("div.admin-links").hover(
        function() {
          $(this).addClass("admin-links-hover"); return true;
        },
        function(){
          $(this).removeClass("admin-links-hover"); return true;
        }
      );
    }
  };

  $(Drupal.Panels.autoAttach);
})(jQuery);
;/**/
(function(e){Drupal.viewsSlideshow=Drupal.viewsSlideshow||{},Drupal.viewsSlideshowControls=Drupal.viewsSlideshowControls||{},Drupal.viewsSlideshowControls.play=function(e){try{Drupal.settings.viewsSlideshowControls[e.slideshowID].top.type!==void 0&&"function"==typeof Drupal[Drupal.settings.viewsSlideshowControls[e.slideshowID].top.type].play&&Drupal[Drupal.settings.viewsSlideshowControls[e.slideshowID].top.type].play(e)}catch(t){}try{Drupal.settings.viewsSlideshowControls[e.slideshowID].bottom.type!==void 0&&"function"==typeof Drupal[Drupal.settings.viewsSlideshowControls[e.slideshowID].bottom.type].play&&Drupal[Drupal.settings.viewsSlideshowControls[e.slideshowID].bottom.type].play(e)}catch(t){}},Drupal.viewsSlideshowControls.pause=function(e){try{Drupal.settings.viewsSlideshowControls[e.slideshowID].top.type!==void 0&&"function"==typeof Drupal[Drupal.settings.viewsSlideshowControls[e.slideshowID].top.type].pause&&Drupal[Drupal.settings.viewsSlideshowControls[e.slideshowID].top.type].pause(e)}catch(t){}try{Drupal.settings.viewsSlideshowControls[e.slideshowID].bottom.type!==void 0&&"function"==typeof Drupal[Drupal.settings.viewsSlideshowControls[e.slideshowID].bottom.type].pause&&Drupal[Drupal.settings.viewsSlideshowControls[e.slideshowID].bottom.type].pause(e)}catch(t){}},Drupal.behaviors.viewsSlideshowControlsText={attach:function(t){e(".views_slideshow_controls_text_previous:not(.views-slideshow-controls-text-previous-processed)",t).addClass("views-slideshow-controls-text-previous-processed").each(function(){var t=e(this).attr("id").replace("views_slideshow_controls_text_previous_","");e(this).click(function(){return Drupal.viewsSlideshow.action({action:"previousSlide",slideshowID:t}),!1})}),e(".views_slideshow_controls_text_next:not(.views-slideshow-controls-text-next-processed)",t).addClass("views-slideshow-controls-text-next-processed").each(function(){var t=e(this).attr("id").replace("views_slideshow_controls_text_next_","");e(this).click(function(){return Drupal.viewsSlideshow.action({action:"nextSlide",slideshowID:t}),!1})}),e(".views_slideshow_controls_text_pause:not(.views-slideshow-controls-text-pause-processed)",t).addClass("views-slideshow-controls-text-pause-processed").each(function(){var t=e(this).attr("id").replace("views_slideshow_controls_text_pause_","");e(this).click(function(){return Drupal.settings.viewsSlideshow[t].paused?Drupal.viewsSlideshow.action({action:"play",slideshowID:t,force:!0}):Drupal.viewsSlideshow.action({action:"pause",slideshowID:t,force:!0}),!1})})}},Drupal.viewsSlideshowControlsText=Drupal.viewsSlideshowControlsText||{},Drupal.viewsSlideshowControlsText.pause=function(t){var i=Drupal.theme.prototype.viewsSlideshowControlsPause?Drupal.theme("viewsSlideshowControlsPause"):"";e("#views_slideshow_controls_text_pause_"+t.slideshowID+" a").text(i)},Drupal.viewsSlideshowControlsText.play=function(t){var i=Drupal.theme.prototype.viewsSlideshowControlsPlay?Drupal.theme("viewsSlideshowControlsPlay"):"";e("#views_slideshow_controls_text_pause_"+t.slideshowID+" a").text(i)},Drupal.theme.prototype.viewsSlideshowControlsPause=function(){return Drupal.t("Resume")},Drupal.theme.prototype.viewsSlideshowControlsPlay=function(){return Drupal.t("Pause")},Drupal.viewsSlideshowPager=Drupal.viewsSlideshowPager||{},Drupal.viewsSlideshowPager.transitionBegin=function(e){try{Drupal.settings.viewsSlideshowPager[e.slideshowID].top.type!==void 0&&"function"==typeof Drupal[Drupal.settings.viewsSlideshowPager[e.slideshowID].top.type].transitionBegin&&Drupal[Drupal.settings.viewsSlideshowPager[e.slideshowID].top.type].transitionBegin(e)}catch(t){}try{Drupal.settings.viewsSlideshowPager[e.slideshowID].bottom.type!==void 0&&"function"==typeof Drupal[Drupal.settings.viewsSlideshowPager[e.slideshowID].bottom.type].transitionBegin&&Drupal[Drupal.settings.viewsSlideshowPager[e.slideshowID].bottom.type].transitionBegin(e)}catch(t){}},Drupal.viewsSlideshowPager.goToSlide=function(e){try{Drupal.settings.viewsSlideshowPager[e.slideshowID].top.type!==void 0&&"function"==typeof Drupal[Drupal.settings.viewsSlideshowPager[e.slideshowID].top.type].goToSlide&&Drupal[Drupal.settings.viewsSlideshowPager[e.slideshowID].top.type].goToSlide(e)}catch(t){}try{Drupal.settings.viewsSlideshowPager[e.slideshowID].bottom.type!==void 0&&"function"==typeof Drupal[Drupal.settings.viewsSlideshowPager[e.slideshowID].bottom.type].goToSlide&&Drupal[Drupal.settings.viewsSlideshowPager[e.slideshowID].bottom.type].goToSlide(e)}catch(t){}},Drupal.viewsSlideshowPager.previousSlide=function(e){try{Drupal.settings.viewsSlideshowPager[e.slideshowID].top.type!==void 0&&"function"==typeof Drupal[Drupal.settings.viewsSlideshowPager[e.slideshowID].top.type].previousSlide&&Drupal[Drupal.settings.viewsSlideshowPager[e.slideshowID].top.type].previousSlide(e)}catch(t){}try{Drupal.settings.viewsSlideshowPager[e.slideshowID].bottom.type!==void 0&&"function"==typeof Drupal[Drupal.settings.viewsSlideshowPager[e.slideshowID].bottom.type].previousSlide&&Drupal[Drupal.settings.viewsSlideshowPager[e.slideshowID].bottom.type].previousSlide(e)}catch(t){}},Drupal.viewsSlideshowPager.nextSlide=function(e){try{Drupal.settings.viewsSlideshowPager[e.slideshowID].top.type!==void 0&&"function"==typeof Drupal[Drupal.settings.viewsSlideshowPager[e.slideshowID].top.type].nextSlide&&Drupal[Drupal.settings.viewsSlideshowPager[e.slideshowID].top.type].nextSlide(e)}catch(t){}try{Drupal.settings.viewsSlideshowPager[e.slideshowID].bottom.type!==void 0&&"function"==typeof Drupal[Drupal.settings.viewsSlideshowPager[e.slideshowID].bottom.type].nextSlide&&Drupal[Drupal.settings.viewsSlideshowPager[e.slideshowID].bottom.type].nextSlide(e)}catch(t){}},Drupal.behaviors.viewsSlideshowPagerFields={attach:function(t){e(".views_slideshow_pager_field:not(.views-slideshow-pager-field-processed)",t).addClass("views-slideshow-pager-field-processed").each(function(){var t=e(this).attr("id").split("_"),i=t[2];t.splice(0,3);var n=t.join("_");Drupal.settings.viewsSlideshowPagerFields[n][i].activatePauseOnHover?e(this).children().each(function(t,i){var s=function(){Drupal.viewsSlideshow.action({action:"goToSlide",slideshowID:n,slideNum:t}),Drupal.viewsSlideshow.action({action:"pause",slideshowID:n})},o=function(){Drupal.viewsSlideshow.action({action:"play",slideshowID:n})};jQuery.fn.hoverIntent?e(i).hoverIntent(s,o):e(i).hover(s,o)}):e(this).children().each(function(t,i){e(i).click(function(){Drupal.viewsSlideshow.action({action:"goToSlide",slideshowID:n,slideNum:t})})})})}},Drupal.viewsSlideshowPagerFields=Drupal.viewsSlideshowPagerFields||{},Drupal.viewsSlideshowPagerFields.transitionBegin=function(t){for(pagerLocation in Drupal.settings.viewsSlideshowPager[t.slideshowID])e('[id^="views_slideshow_pager_field_item_'+pagerLocation+"_"+t.slideshowID+'"]').removeClass("active"),e("#views_slideshow_pager_field_item_"+pagerLocation+"_"+t.slideshowID+"_"+t.slideNum).addClass("active")},Drupal.viewsSlideshowPagerFields.goToSlide=function(t){for(pagerLocation in Drupal.settings.viewsSlideshowPager[t.slideshowID])e('[id^="views_slideshow_pager_field_item_'+pagerLocation+"_"+t.slideshowID+'"]').removeClass("active"),e("#views_slideshow_pager_field_item_"+pagerLocation+"_"+t.slideshowID+"_"+t.slideNum).addClass("active")},Drupal.viewsSlideshowPagerFields.previousSlide=function(t){for(pagerLocation in Drupal.settings.viewsSlideshowPager[t.slideshowID]){var i=e('[id^="views_slideshow_pager_field_item_'+pagerLocation+"_"+t.slideshowID+'"].active').attr("id").replace("views_slideshow_pager_field_item_"+pagerLocation+"_"+t.slideshowID+"_","");0==i?i=e('[id^="views_slideshow_pager_field_item_'+pagerLocation+"_"+t.slideshowID+'"]').length()-1:i--,e('[id^="views_slideshow_pager_field_item_'+pagerLocation+"_"+t.slideshowID+'"]').removeClass("active"),e("#views_slideshow_pager_field_item_"+pagerLocation+"_"+t.slideshowID+"_"+i).addClass("active")}},Drupal.viewsSlideshowPagerFields.nextSlide=function(t){for(pagerLocation in Drupal.settings.viewsSlideshowPager[t.slideshowID]){var i=e('[id^="views_slideshow_pager_field_item_'+pagerLocation+"_"+t.slideshowID+'"].active').attr("id").replace("views_slideshow_pager_field_item_"+pagerLocation+"_"+t.slideshowID+"_",""),n=e('[id^="views_slideshow_pager_field_item_'+pagerLocation+"_"+t.slideshowID+'"]').length();i++,i==n&&(i=0),e('[id^="views_slideshow_pager_field_item_'+pagerLocation+"_"+t.slideshowID+'"]').removeClass("active"),e("#views_slideshow_pager_field_item_"+pagerLocation+"_"+t.slideshowID+"_"+slideNum).addClass("active")}},Drupal.viewsSlideshowSlideCounter=Drupal.viewsSlideshowSlideCounter||{},Drupal.viewsSlideshowSlideCounter.transitionBegin=function(t){e("#views_slideshow_slide_counter_"+t.slideshowID+" .num").text(t.slideNum+1)},Drupal.viewsSlideshow.action=function(e){var t={value:!0,text:""};if(e.action===void 0||""==e.action)return t.value=!1,t.text=Drupal.t("There was no action specified."),error;if("pause"==e.action)Drupal.settings.viewsSlideshow[e.slideshowID].paused=1,e.force&&(Drupal.settings.viewsSlideshow[e.slideshowID].pausedForce=1);else if("play"==e.action){if(Drupal.settings.viewsSlideshow[e.slideshowID].pausedForce&&!e.force)return t.value=!1,t.text+=" "+Drupal.t("This slideshow is forced paused."),t;Drupal.settings.viewsSlideshow[e.slideshowID].paused=0,Drupal.settings.viewsSlideshow[e.slideshowID].pausedForce=0}switch(e.action){case"goToSlide":case"transitionBegin":case"transitionEnd":(e.slideNum===void 0||"number"!=typeof e.slideNum||parseInt(e.slideNum)!=e.slideNum-0)&&(t.value=!1,t.text=Drupal.t("An invalid integer was specified for slideNum."));case"pause":case"play":case"nextSlide":case"previousSlide":var i=Drupal.settings.viewsSlideshow[e.slideshowID].methods,n={};if(e.excludeMethods!==void 0)for(var s=0;excludeMethods.length>s;s++)n[excludeMethods[s]]="";for(s=0;i[e.action].length>s;s++)void 0==Drupal[i[e.action][s]]||"function"!=typeof Drupal[i[e.action][s]][e.action]||i[e.action][s]in n||Drupal[i[e.action][s]][e.action](e);break;default:t.value=!1,t.text=Drupal.t('An invalid action "!action" was specified.',{"!action":e.action})}return t}})(jQuery);;/**/
(function(e){function t(t){var i=/^(([^\/:]+?\.)*)([^\.:]{4,})((\.[a-z]{1,4})*)(:[0-9]{1,5})?$/,n=window.location.host.replace(i,"$3$4"),s=window.location.host.replace(i,"$1");if(Drupal.settings.extlink.extSubdomains)var o="([^/]*\\.)?";else if("www."==s||""==s)var o="(www\\.)?";else var o=s.replace(".","\\.");var a=RegExp("^https?://"+o+n,"i"),r=!1;Drupal.settings.extlink.extInclude&&(r=RegExp(Drupal.settings.extlink.extInclude.replace(/\\/,"\\")));var l=!1;Drupal.settings.extlink.extExclude&&(l=RegExp(Drupal.settings.extlink.extExclude.replace(/\\/,"\\")));var c=[],u=[];e("a:not(."+Drupal.settings.extlink.extClass+", ."+Drupal.settings.extlink.mailtoClass+")",t).each(function(){try{var e=this.href.toLowerCase();0!=e.indexOf("http")||!(!e.match(a)||r&&e.match(r))||l&&e.match(l)?0==e.indexOf("mailto:")&&u.push(this):c.push(this)}catch(t){return!1}}),Drupal.settings.extlink.extClass&&(1.2>parseFloat(e().jquery)?e(c).not("[img]").addClass(Drupal.settings.extlink.extClass).each(function(){"inline"==e(this).css("display")&&e(this).after("<span class="+Drupal.settings.extlink.extClass+"></span>")}):e(c).not(e(c).find("img").parents("a")).addClass(Drupal.settings.extlink.extClass).each(function(){"inline"==e(this).css("display")&&e(this).after("<span class="+Drupal.settings.extlink.extClass+"></span>")})),Drupal.settings.extlink.mailtoClass&&(1.2>parseFloat(e().jquery)?e(u).not("[img]").addClass(Drupal.settings.extlink.mailtoClass).each(function(){"inline"==e(this).css("display")&&e(this).after("<span class="+Drupal.settings.extlink.mailtoClass+"></span>")}):e(u).not(e(u).find("img").parents("a")).addClass(Drupal.settings.extlink.mailtoClass).each(function(){"inline"==e(this).css("display")&&e(this).after("<span class="+Drupal.settings.extlink.mailtoClass+"></span>")})),Drupal.settings.extlink.extTarget&&e(c).attr("target",Drupal.settings.extlink.extTarget),Drupal.settings.extlink.extAlert&&e(c).click(function(){return confirm(Drupal.settings.extlink.extAlertText)}),(e.support&&void 0!==e.support.boxModel&&!e.support.boxModel||e.browser.msie&&7>=parseInt(e.browser.version))&&e("span.ext, span.mailto").css("display","inline-block")}Drupal.behaviors.extlink={attach:function(e){t(e)}}})(jQuery);;/**/
function extlink_extra_timer(){0!=Drupal.settings.extlink_extra.extlink_alert_timer&&null!=Drupal.settings.extlink_extra.extlink_alert_timer&&(extlink_int=setInterval(function(){var e=jQuery(".automatic-redirect-countdown"),t=e.attr("rel");if(null==t&&(t=Drupal.settings.extlink_extra.extlink_alert_timer),t>=0)e.html('<span class="extlink-timer-text">Automatically redirecting in: </span><span class="extlink-count">'+t+'</span><span class="extlink-timer-text"> seconds.</span>'),e.attr("rel",--t);else{extlink_stop_timer(),e.remove();var i=jQuery("div.extlink-extra-go-action a").attr("href");window.location=i}},1e3))}function extlink_stop_timer(){clearInterval(extlink_int)}(function(e){Drupal.behaviors.extlink_extra={attach:function(){jQuery("http://www.wholefoodsmarket.com/sites/default/files/advagg_js/a.ext").unbind("click").not(".ext-override").click(function(){if("confirm"==Drupal.settings.extlink_extra.extlink_alert_type)return confirm(Drupal.settings.extlink.extAlertText.value);var t=jQuery(this).attr("href"),i=window.location.href,n=Drupal.settings.extlink_extra.extlink_alert_url;return e.cookie("external_url",t,{path:"/"}),e.cookie("back_url",i,{path:"/"}),"colorbox"==Drupal.settings.extlink_extra.extlink_alert_type?(jQuery.colorbox({href:n+" .extlink-extra-leaving",height:"50%",width:"50%",initialWidth:"50%",initialHeight:"50%",onComplete:function(){jQuery("div.extlink-extra-back-action a").click(function(){return jQuery.colorbox.close(),!1}),extlink_extra_timer()},onClosed:extlink_stop_timer}),!1):"page"==Drupal.settings.extlink_extra.extlink_alert_type?(window.location=n,!1):void 0})}}})(jQuery);var extlink_int;;/**/
(function(e){Drupal.progressBar=function(t,i,n,s){this.id=t,this.method=n||"GET",this.updateCallback=i,this.errorCallback=s,this.element=e('<div class="progress" aria-live="polite"></div>').attr("id",t),this.element.html('<div class="bar"><div class="filled"></div></div><div class="percentage"></div><div class="message">&nbsp;</div>')},Drupal.progressBar.prototype.setProgress=function(t,i){t>=0&&100>=t&&(e("div.filled",this.element).css("width",t+"%"),e("div.percentage",this.element).html(t+"%")),e("div.message",this.element).html(i),this.updateCallback&&this.updateCallback(t,i,this)},Drupal.progressBar.prototype.startMonitoring=function(e,t){this.delay=t,this.uri=e,this.sendPing()},Drupal.progressBar.prototype.stopMonitoring=function(){clearTimeout(this.timer),this.uri=null},Drupal.progressBar.prototype.sendPing=function(){if(this.timer&&clearTimeout(this.timer),this.uri){var t=this;e.ajax({type:this.method,url:this.uri,data:"",dataType:"json",success:function(e){return 0==e.status?(t.displayError(e.data),void 0):(t.setProgress(e.percentage,e.message),t.timer=setTimeout(function(){t.sendPing()},t.delay),void 0)},error:function(e){var i=Drupal.ajaxError(e,t.uri);t.displayError(""+i)}})}},Drupal.progressBar.prototype.displayError=function(t){var i=e('<div class="messages error"></div>').html(t);e(this.element).before(i).hide(),this.errorCallback&&this.errorCallback(this)}})(jQuery);;/**/
!function(o){Drupal.CTools=Drupal.CTools||{},Drupal.CTools.Modal=Drupal.CTools.Modal||{},Drupal.CTools.Modal.show=function(t){var l={};t&&"string"==typeof t&&Drupal.settings[t]?o.extend(!0,l,Drupal.settings[t]):t&&o.extend(!0,l,t);var e={modalTheme:"CToolsModalDialog",throbberTheme:"CToolsModalThrobber",animation:"show",animationSpeed:"fast",modalSize:{type:"scale",width:.8,height:.8,addWidth:0,addHeight:0,contentRight:25,contentBottom:45},modalOptions:{opacity:.55,background:"#fff"}},a={};o.extend(!0,a,e,Drupal.settings.CToolsModal,l),Drupal.CTools.Modal.currentSettings&&Drupal.CTools.Modal.currentSettings!=a&&(Drupal.CTools.Modal.modal.remove(),Drupal.CTools.Modal.modal=null),Drupal.CTools.Modal.currentSettings=a;var n=function(t){var l=t?document:Drupal.CTools.Modal.modal;if("scale"==Drupal.CTools.Modal.currentSettings.modalSize.type)var e=o(window).width()*Drupal.CTools.Modal.currentSettings.modalSize.width,a=o(window).height()*Drupal.CTools.Modal.currentSettings.modalSize.height;else var e=Drupal.CTools.Modal.currentSettings.modalSize.width,a=Drupal.CTools.Modal.currentSettings.modalSize.height;o("div.ctools-modal-content",l).css({width:e+Drupal.CTools.Modal.currentSettings.modalSize.addWidth+"px",height:a+Drupal.CTools.Modal.currentSettings.modalSize.addHeight+"px"}),o("div.ctools-modal-content .modal-content",l).css({width:e-Drupal.CTools.Modal.currentSettings.modalSize.contentRight+"px",height:a-Drupal.CTools.Modal.currentSettings.modalSize.contentBottom+"px"})};Drupal.CTools.Modal.modal||(Drupal.CTools.Modal.modal=o(Drupal.theme(a.modalTheme)),"scale"==a.modalSize.type&&o(window).bind("resize",n)),n(),o("span.modal-title",Drupal.CTools.Modal.modal).html(Drupal.CTools.Modal.currentSettings.loadingText),Drupal.CTools.Modal.modalContent(Drupal.CTools.Modal.modal,a.modalOptions,a.animation,a.animationSpeed),o("#modalContent .modal-content").html(Drupal.theme(a.throbberTheme)),o("#modalContent .modal-content").delegate("input.form-autocomplete","keyup",function(){o("#autocomplete").css("top",o(this).position().top+o(this).outerHeight()+o(this).offsetParent().filter("#modal-content").scrollTop())})},Drupal.CTools.Modal.dismiss=function(){Drupal.CTools.Modal.modal&&Drupal.CTools.Modal.unmodalContent(Drupal.CTools.Modal.modal)},Drupal.theme.prototype.CToolsModalDialog=function(){var o="";return o+='  <div id="ctools-modal">',o+='    <div class="ctools-modal-content">',o+='      <div class="modal-header">',o+='        <a class="close" href="#">',o+=Drupal.CTools.Modal.currentSettings.closeText+Drupal.CTools.Modal.currentSettings.closeImage,o+="        </a>",o+='        <span id="modal-title" class="modal-title">&nbsp;</span>',o+="      </div>",o+='      <div id="modal-content" class="modal-content">',o+="      </div>",o+="    </div>",o+="  </div>"},Drupal.theme.prototype.CToolsModalThrobber=function(){var o="";return o+='  <div id="modal-throbber">',o+='    <div class="modal-throbber-wrapper">',o+=Drupal.CTools.Modal.currentSettings.throbber,o+="    </div>",o+="  </div>"},Drupal.CTools.Modal.getSettings=function(t){var l=o(t).attr("class").match(/ctools-modal-(\S+)/);return l?l[1]:void 0},Drupal.CTools.Modal.clickAjaxCacheLink=function(){return Drupal.CTools.Modal.show(Drupal.CTools.Modal.getSettings(this)),Drupal.CTools.AJAX.clickAJAXCacheLink.apply(this)},Drupal.CTools.Modal.clickAjaxLink=function(){return Drupal.CTools.Modal.show(Drupal.CTools.Modal.getSettings(this)),!1},Drupal.CTools.Modal.submitAjaxForm=function(){var t=o(this),l=t.attr("action");return setTimeout(function(){Drupal.CTools.AJAX.ajaxSubmit(t,l)},1),!1},Drupal.behaviors.ZZCToolsModal={attach:function(t){o("area.ctools-use-modal, a.ctools-use-modal",t).once("ctools-use-modal",function(){var t=o(this);t.click(Drupal.CTools.Modal.clickAjaxLink);var l={};t.attr("href")&&(l.url=t.attr("href"),l.event="click",l.progress={type:"throbber"});var e=t.attr("href");Drupal.ajax[e]=new Drupal.ajax(e,this,l)}),o("input.ctools-use-modal, button.ctools-use-modal",t).once("ctools-use-modal",function(){var t=o(this);t.click(Drupal.CTools.Modal.clickAjaxLink);var l=this,e={};e.url=Drupal.CTools.Modal.findURL(this),""==e.url&&(e.url=o(this).closest("form").attr("action")),e.event="click",e.setClick=!0;var a=t.attr("id");Drupal.ajax[a]=new Drupal.ajax(a,this,e),o("."+o(l).attr("id")+"-url").change(function(){Drupal.ajax[a].options.url=Drupal.CTools.Modal.findURL(l)})}),o("#modal-content form",t).once("ctools-use-modal",function(){var t=o(this),l={};l.url=t.attr("action"),l.event="submit",l.progress={type:"throbber"};var e=t.attr("id");Drupal.ajax[e]=new Drupal.ajax(e,this,l),Drupal.ajax[e].form=t,o("input[type=submit], button",this).click(function(t){return Drupal.ajax[e].element=this,this.form.clk=this,Drupal.autocompleteSubmit&&!Drupal.autocompleteSubmit()?!1:void 0==t.bubbles?(o(this.form).trigger("submit"),!1):void 0})}),o(".ctools-close-modal",t).once("ctools-close-modal").click(function(){return Drupal.CTools.Modal.dismiss(),!1})}},Drupal.CTools.Modal.modal_display=function(t,l){0==o("#modalContent").length&&Drupal.CTools.Modal.show(Drupal.CTools.Modal.getSettings(t.element)),o("#modal-title").html(l.title),o("#modal-content").html(l.output).scrollTop(0);var e=l.settings||t.settings||Drupal.settings;Drupal.attachBehaviors("#modalContent",e)},Drupal.CTools.Modal.modal_dismiss=function(){Drupal.CTools.Modal.dismiss(),o("link.ctools-temporary-css").remove()},Drupal.CTools.Modal.modal_loading=function(){Drupal.CTools.Modal.modal_display({output:Drupal.theme(Drupal.CTools.Modal.currentSettings.throbberTheme),title:Drupal.CTools.Modal.currentSettings.loadingText})},Drupal.CTools.Modal.findURL=function(t){var l="",e="."+o(t).attr("id")+"-url";return o(e).each(function(){var t=o(this);l&&t.val()&&(l+="/"),l+=t.val()}),l},Drupal.CTools.Modal.modalContent=function(t,l,e,a){function n(){o(window).unbind("resize",modalContentResize),o("body").unbind("focus",modalEventHandler),o("body").unbind("keypress",modalEventHandler),o(".close").unbind("click",modalContentClose),o("body").unbind("keypress",modalEventEscapeCloseHandler),o(document).trigger("CToolsDetachBehaviors",o("#modalContent")),"fadeIn"==e&&(e="fadeOut"),"slideDown"==e&&(e="slideUp"),"show"==e&&(e="hide"),c.hide()[e](a),o("#modalContent").remove(),o("#modalBackdrop").remove()}if(e?"fadeIn"!=e&&"slideDown"!=e&&(e="show"):e="show",a||(a="fast"),l=jQuery.extend({position:"absolute",left:"0px",margin:"0px",background:"#000",opacity:".55"},l),l.filter="alpha(opacity="+100*l.opacity+")",t.hide(),o("#modalBackdrop").length&&o("#modalBackdrop").remove(),o("#modalContent").length&&o("#modalContent").remove(),self.pageYOffset)var d=self.pageYOffset;else if(document.documentElement&&document.documentElement.scrollTop)var d=document.documentElement.scrollTop;else if(document.body)var d=document.body.scrollTop;var s=o(document).height()+50,i=o(document).width(),r=o(window).height(),u=o(window).width();r>s&&(s=r),o("body").append('<div id="modalBackdrop" style="z-index: 1000; display: none;"></div><div id="modalContent" style="z-index: 1001; position: absolute;">'+o(t).html()+"</div>"),modalEventHandler=function(t){target=null,t?target=t.target:(t=window.event,target=t.srcElement);for(var l=o(target).parents().get(),e=0;e<l.length;++e){var a=o(l[e]).css("position");if("absolute"==a||"fixed"==a)return!0}return o(target).is("#modalContent, body")||o(target).filter("*:visible").parents("#modalContent").length?!0:(o("#modalContent").focus(),void t.preventDefault())},o("body").bind("focus",modalEventHandler),o("body").bind("keypress",modalEventHandler);var c=o("#modalContent").css("top","-1000px"),m=d+r/2-c.outerHeight()/2,p=u/2-c.outerWidth()/2;o("#modalBackdrop").css(l).css("top",0).css("height",s+"px").css("width",i+"px").show(),c.css({top:m+"px",left:p+"px"}).hide()[e](a),modalContentClose=function(){return n(),!1},o(".close").bind("click",modalContentClose),modalEventEscapeCloseHandler=function(o){return 27==o.keyCode?(n(),!1):void 0},o(document).bind("keydown",modalEventEscapeCloseHandler),modalContentResize=function(){if(self.pageYOffset)var t=self.pageYOffset;else if(document.documentElement&&document.documentElement.scrollTop)var t=document.documentElement.scrollTop;else if(document.body)var t=document.body.scrollTop;var l=o(document).height(),e=o(document).width(),a=o(window).height(),n=o(window).width();a>l&&(l=a);var d=o("#modalContent"),s=t+a/2-d.outerHeight()/2,i=n/2-d.outerWidth()/2;o("#modalBackdrop").css("height",l+"px").css("width",e+"px").show(),d.css("top",s+"px").css("left",i+"px").show()},o(window).bind("resize",modalContentResize),o("#modalContent").focus()},Drupal.CTools.Modal.unmodalContent=function(t,l,e){if(l)"fadeOut"!=l&&"slideUp"!=l&&(l="show");else var l="show";if(!e)var e="fast";o(window).unbind("resize",modalContentResize),o("body").unbind("focus",modalEventHandler),o("body").unbind("keypress",modalEventHandler),o(".close").unbind("click",modalContentClose),o(document).trigger("CToolsDetachBehaviors",o("#modalContent")),t.each(function(){"fade"==l?o("#modalContent").fadeOut(e,function(){o("#modalBackdrop").fadeOut(e,function(){o(this).remove()}),o(this).remove()}):"slide"==l?o("#modalContent").slideUp(e,function(){o("#modalBackdrop").slideUp(e,function(){o(this).remove()}),o(this).remove()}):(o("#modalContent").remove(),o("#modalBackdrop").remove())})},o(function(){Drupal.ajax.prototype.commands.modal_display=Drupal.CTools.Modal.modal_display,Drupal.ajax.prototype.commands.modal_dismiss=Drupal.CTools.Modal.modal_dismiss})}(jQuery);;/**/
var $isotopeContainer;
var $isotopePager = {};
var filter;

(function($){
  // Function includes Isotope bindings to provide filtering
  // and custom code to provide paging
  $(document).ready(function isotopeBindings() {
    //easily configurable options
    var $itemSelector = '.views-row';

    var $frontPage = $('body').hasClass('page-values-matter');

    filter = {
      showme: 0,
      iwantto: 0,
      ugc: 0,
      ugcSearch: '',
      prepended: [],
      setFilterValue: function(_class, value) {
        if (_class == 'showme') {
          this.setShowme(value);
        }
        if (_class == 'iwantto') {
          this.setIwantto(value);
        }
        if (_class == 'ugc') {
          //this.setUGC(value);
          if (value == 1) {
            window.location = "http://www.wholefoodsmarket.com/values-matter/your-stories";
          } else {
            window.location = "http://www.wholefoodsmarket.com/values-matter";
          }
          return;
        }
        this.updateFilteredList();
      },
      updateFilteredList: function() {
        Drupal.settings.brandCamp.lists.mainFiltered = [];
        var list = Drupal.settings.brandCamp.lists.main;
        var filtered = Drupal.settings.brandCamp.lists.mainFiltered;
        if (Drupal.settings.brandCamp.lists.prepend) {
          for(var index = 0; index < Drupal.settings.brandCamp.lists.prepend.length; index++) {
            this.prepended.push(Drupal.settings.brandCamp.lists.prepend[index].nid);
          }
        }
        for(var index = 0; index < list.length; index++) {
          if (this.isMatch(list[index])) {
            filtered.push(index);
          }
        }
      },
      setShowme: function(value) {
        this.showme = value;
      },
      setIwantto: function(value) {
        this.iwantto = value;
      },
      setUGC: function(value) {
        this.ugc = value;
      },
      setUGCSearch: function(value) {
        value = value.trim();
        if (value.charAt(0) == '@') {
          value = value.substr(1);
        }
        if (value == this.ugcSearch) {
          return;
        }
        this.ugcSearch = value;
        if (value) {
          Drupal.settings.brandCamp.lists.ugcSearch = [];
          Drupal.settings.brandCamp.pages.ugcSearch = 1;
          $isotopePager.pagesLoaded.ugcSearch = 0;
        }
      },
      reset: function() {
        this.showme = this.iwantto = 0;
      },
      isMatch: function(element) {
        return (this.prepended.indexOf(element.nid) < 0) && (this.iwantto == 0 || element.iwantto.indexOf(this.iwantto) >= 0) && (this.showme == 0 || element.showme.indexOf(this.showme) >= 0);
      }
    };

    $isotopePager = {
      $id: 'isopager',
      currentPage: 1,
      perPage: 12,
      curItem: {ugc: 0, connect: 0, main: 0},
      pageStartIndexes: [],
      $detached: null,
      pagesLoaded: {ugc: 1, main: 1, ugcSearch: 0},
      ajaxing: false,
      ajaxFailCounter: 0,
      curtainColors: ['#99aea5', '#29634f', '#e1781d', '#d0b657', '#a49e87'],
      animating: true,
      badRows: [],
      prependedLength: 0, // 3 nodes that are prepended before main list on Node page

      next: function() {
        var lists = Drupal.settings.brandCamp.lists;
        var elems = [];
        var startIndex = Math.min(Math.max($isotopeContainer.find($itemSelector).length - this.prependedLength - this.currentPage * this.perPage, 0), 12);
        var endOfContent = false;
        var thisPager = this;

        this.showPager();
        this.pageStartIndexes[this.currentPage] = jQuery.extend({},this.curItem);

        if (this.currentPage < 0) {
          // this is node view page that has additional 3 nodes prepended before main list
          this.prependedLength = lists.prepend.length;
          startIndex = this.perPage - this.prependedLength;
        }

        for (var index = startIndex; index < this.perPage && !endOfContent; index++) {
          var weight = this.currentPage * this.perPage + index;
          var type, info, element;
          if (this.currentPage < 0) {
            // this is node view page that has additional 3 nodes prepended before main list
            type = 'article';
            info = lists.prepend[weight + this.prependedLength];
          }
          else if (lists.connect.length && (index == 2 || index == 6)) {
            // Insert special blocks on any of BC pages
            type = 'connect';
            info = lists.connect[this.curItem.connect];
            this.curItem.connect = ++this.curItem.connect % lists.connect.length;
          }
          else if (filter.ugc == 1) { // filter.ugc == 1 on "Your Stories" page
            type = 'ugc-image';
            var list;
            if (filter.ugcSearch) {
              list = lists.ugcSearch;
            }
            else {
              list = lists.ugc;
            }
            info = list[this.curItem.ugc];
            // check whether we need to preload more items if only 20 left in the array of available ones
            if (this.curItem.ugc + 20 >= list.length) {
              if (filter.ugcSearch) {
                this.loadMore('ugcSearch');
              }
              else {
                this.loadMore('ugc');
              }
            }
            // increments curItem counter and checks if that's last item among available ones
            if (++this.curItem.ugc >= list.length) {
              this.hidePager();
              this.curItem.ugc = list.length;
              endOfContent = true;
            }
          }
          else if (lists.ugc.length && (index == 1 || index == 8)) {
            type = 'ugc-image';
            info = lists.ugcShuffle[this.curItem.ugc];
            this.curItem.ugc = ++this.curItem.ugc % lists.ugc.length;
          }
          else {
            type = 'article';
            info = lists.main[lists.mainFiltered[this.curItem.main]];
            if (this.curItem.main + 20 >= lists.mainFiltered.length) {
              this.loadMore('main');
            }
            if (++this.curItem.main >= lists.mainFiltered.length) {
              this.hidePager();
              endOfContent = true;
            }
          }

          if (!info) {
            this.hidePager();
            break;
          }
          var colorInd = Math.floor((Math.random() * 5) + 1) -1;
          info.curtain_color = $isotopePager.curtainColors[colorInd];

          if (this.$detached && (element = this.$detached.filter('.views-row[nid=' + info.nid + ']').first()) && element.length) {
            $isotopeContainer.append(element.attr('weight', weight));
          }
          else {
            element = $(Drupal.theme('brandCampListRow', type, info)).attr('weight', weight);
            if (!elems.length) {
              elems = element;
            }
            else {
              elems = elems.add(element);
            }

            $isotopeContainer.append(element);

            onLoad = function() {
              $(this).parents('.views-row')
                .addClass('animate')
                .one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function() {
                  $(this).removeClass('animate').addClass('shown');
                });
            }
            element.find('img').load(onLoad).error(onLoad);
            element.find('.ugc_img')
              .load(function() {
                var w = $(this)[0].naturalWidth, h = $(this)[0].naturalHeight;
                if (w > 0 && h > 0) {
                  if (w > h) {
                    $(this).css({
                      width: 100 * w / h + '%',
                      left: -50 * (w / h - 1) + '%',
                      maxWidth: 'none'
                    });
                  }
                  else if (w < h) {
                    $(this).css('top', -50 * (h / w - 1) + '%');
                  }
                }
              })
              .error(function() {
                thisPager.badRows.push($(this).parents('.views-row'));
                $(this).remove();
                thisPager.removeBadRows();
              });
          }
        } //endfor

        this.currentPage++;
        if (this.$detached) {
          if (jQuery().isotope) {
            this.$detached.each(function () {
              if (!$(this).parent().length) {
                $isotopeContainer.isotope('remove', $(this)).isotope();
              }
            })
          }
          this.$detached = null;
        }

        if (jQuery().isotope) {
          thisPager.animating = true;
          // re-insert elements into isotope updating sort as they might be wrongly ordered after manipulations
          $isotopeContainer.isotope('updateSortData').isotope('insert', elems).isotope();
        }

        if ($frontPage && !this.ajaxing && !$isotopeContainer.find($itemSelector).length) {
          $isotopeContainer.append($('<div class="no-matches">No matches found</div>'));
        }
      },

      reset: function() {
        this.$detached = $isotopeContainer.find($itemSelector).detach();
        this.currentPage = this.curItem.ugc = this.curItem.connect = this.curItem.main = 0;
        if (Drupal.settings.brandCamp.lists.prepend) {
          // this is node view page that has additional 3 nodes prepended before main list
          this.currentPage--;
        }
        this.showPager();
        this.next();

      },

      loadMore: function(type) {
        if (this.ajaxing || this.pagesLoaded[type] >= Drupal.settings.brandCamp.pages[type]) return;
        this.ajaxing = true;
        thisPager = this;
        url = '/values-matter/ajax/' + (type == 'main' ? 'main' : 'ugc') + '/' + this.pagesLoaded[type];
        if (type == 'ugcSearch' && filter.ugcSearch) {
          url += '/' + filter.ugcSearch
        }
        $.ajax(url)
          .done(function( data ) {
            Drupal.settings.brandCamp.lists[type] = Drupal.settings.brandCamp.lists[type].concat(data);

            filter.updateFilteredList();
            thisPager.pagesLoaded[type]++;
            if (type == 'ugcSearch' && data.length == 100) {
              Drupal.settings.brandCamp.pages[type]++;
            }
            thisPager.ajaxing = false;
            thisPager.ajaxFailCounter = 0;
            thisPager.layout();
          })
          .fail(function() {
            if (thisfilter.ajaxFailCounter++ < 3) {
              setTimeout(function() {
                thisPager.ajaxing = false;
                thisPager.loadMore();
              }, 1000);
            }
          });
      },

      layout: function() {
        this.currentPage--;
        //this.showPager();
        this.next();
      },

      hidePager: function() {
        $('#' + this.$id).hide();
      },

      showPager: function() {
        $('#' + this.$id).show();
        $isotopeContainer.find('.no-matches').remove();
      },

      removeBadRows: function() {
        thisPager = $isotopePager;
        if (!thisPager.badRows.length) {
          return;
        }
        if (thisPager.animating) {
          setTimeout(thisPager.removeBadRows, 100);
          return;
        }

        var nids = {};
        for (var i = 0; i < thisPager.badRows.length; i++) {
          nids[thisPager.badRows[i].attr('nid')] = true;
        }

        for (var i = 0; i < Drupal.settings.brandCamp.lists.ugc.length; i++) {
          if (nids[Drupal.settings.brandCamp.lists.ugc[i].nid]) {
            Drupal.settings.brandCamp.lists.ugc.splice(i, 1);
            Drupal.settings.brandCamp.lists.ugcShuffle.splice(i, 1);
          }
        }

        thisPager.currentPage--;
        thisPager.$detached = $isotopeContainer.find($itemSelector).slice(thisPager.currentPage * thisPager.perPage).detach();
        thisPager.curItem = jQuery.extend({}, thisPager.pageStartIndexes[thisPager.currentPage]);
        thisPager.next();
      }
    };

    $isotopeContainer = $('.view-brand-camp-list > .view-content').html('');

    if (jQuery().isotope) {
      //create isotope context on element
      $isotopeContainer.isotope({
        itemSelector: $itemSelector,
        layoutMode: 'cellsByRow',
        cellsByRow: {
          columnWidth: 360,
          isFitWidth: true
        },
        getSortData: {
          name: 'weight',
          weight: function (element) {
            return parseInt($(element).attr('weight'));
          }
        },
        sortBy: 'weight',
        hiddenStyle: { opacity: 0 },
        visibleStyle: { opacity: 1 },
        isJQueryFiltering: false //keep this disabled for jQuery < 1.6
      });
    }

    Drupal.settings.brandCamp.lists.ugcShuffle = Drupal.settings.brandCamp.lists.ugc.slice(0);
    shuffle(Drupal.settings.brandCamp.lists.ugcShuffle);
    if ($('body').hasClass('node-type-bc-ugc-image') || window.location.pathname == '/values-matter/your-stories') {
      filter.setUGC(1);
      var name = getParameterByName('name')
      if (name) {
        name = html_sanitize(name);
        filter.setUGCSearch(name);
      }
    }
    filter.updateFilteredList();
    $isotopePager.reset();

    if (jQuery().isotope) {
      $isotopeContainer.isotope( 'on', 'layoutComplete', function() {
        $isotopePager.animating = false;
      });
    }

    //function to create filters block
    (function createFiltersBlock($) {
      var theme = Drupal.settings.WholeFoods.theme,
          filter_type = '';
      switch (theme) {
        case 'wholefoods_mobile':
          filter_type = 'mobile';
          break;
        case 'brand_camp_theme':
          filter_type = 'desktop';
          break;
        default:
          filter_type = 'desktop';
      };
      $('<div class="isoFilters ' + filter_type + '">' +
      (!filter.ugc ?
      '<div class="showme isoFilters-widget">' +
      '<div class="title">Show me</div>' +
      '<div class="filters" data-filter="showme">' +
      '<div class="filter" data-filter="1">Food</div>' +
      '<div class="filter" data-filter="2">People</div>' +
      '<div class="filter" data-filter="3">Planet</div>' +
      '<div class="filter selected" data-filter="0">All</div>' +
      '</div>' +
      '</div>' +
      '<div class="iwantto isoFilters-widget">' +
      '<div class="title">I want to</div>' +
      '<div class="filters" data-filter="iwantto">' +
      '<div class="filter" data-filter="1">Learn</div>' +
      '<div class="filter" data-filter="2">Do</div>' +
      '<div class="filter selected" data-filter="0">Both</div>' +
      '</div>' +
      '</div>' :
      '<div class="ugc-search isoFilters-widget">' +
      '<div class="title">SEARCH BY @NAME</div>' +
      '<form id="ugc-search-form"><input id="ugc-search-value" type="text" name="name" value="' + filter.ugcSearch + '"><input id="ugc-search-submit" type="submit" value="FIND IT!"></form>' +
      '</div>') +
      '<div class="ugc isoFilters-widget">' +
      '<div class="title">View</div>' +
      '<div class="filters" data-filter="ugc">' +
      '<div class="filter" data-filter="0">Our Stories</div>' +
      '<div class="filter" data-filter="1">Your Stories</div>' +
      '</div>' +
      '</div>' +
      '</div>')
        .insertBefore('#block-system-main .view-brand-camp-list .view-content');
      $('.ugc.isoFilters-widget .filter[data-filter="' + filter.ugc + '"]').addClass('selected');
      $('<div id="' + $isotopePager.$id + '" data-page="1">Load more</div>')
        .insertAfter('.view-brand-camp-list .view-content');
    })($);

    if ($frontPage) {
      $('#ugc-search-form').submit(function() {
        filter.setUGCSearch($('#ugc-search-value').val());
        $isotopePager.reset();
        return false;
      });
    }

    //assign events to filters block
    $('.isoFilters .filter').click(function() {
      if (this.hasAttribute('data-filter')) {
        filter.setFilterValue($(this).parent('.filters').attr('data-filter'), $(this).attr('data-filter'));
      }
      $(this).parent('div').find('.filter').removeClass('selected');
      $(this).addClass('selected');

      $isotopePager.reset();
    });

    //pager event
    $('#isopager').click(function() {
      $isotopePager.next();
    });

    // check URL args for direct linking to specific filter combinations
    var params, paramsRe = /field_bc_(\w+)_value=(\d+)/g;
    while ((params = paramsRe.exec(window.location.search)) != null) {
      $('.filters[data-filter="' + params[1] + '"] .filter[data-filter="' + params[2] + '"]').click();
    }

    function shuffle(array) {
      var currentIndex = array.length, temporaryValue, randomIndex ;

      // While there remain elements to shuffle...
      while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }

      return array;
    }

    function getParameterByName(name) {
      name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
      return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
  });

})(jQuery);


Drupal.theme.prototype.brandCampListRow = function(type, elem) {
  var image, 
      content='', 
      showme = '', 
      names = ['', 'Food', 'People', 'Planet'],
      dimensions = [],
      theme = Drupal.settings.WholeFoods.theme;

  dimensions.ugc = [];
  dimensions.bc = [];
  dimensions.connect = [];

  switch(theme) {
    case 'wholefoods_mobile':
      dimensions.ugc.width = 640;
      dimensions.ugc.height = 640;
      dimensions.bc.width = 640;
      dimensions.bc.height = 510;
      dimensions.connect.width = 640;
      dimensions.connect.height = 640;
      break;
    case 'brand_camp_theme':
      dimensions.ugc.width = 340;
      dimensions.ugc.height = 340;
      dimensions.bc.width = 340;
      dimensions.bc.height = 270;
      dimensions.connect.width = 340;
      dimensions.connect.height = 340;
      break;
    default:
      dimensions.ugc.width = 340;
      dimensions.ugc.height = 340;
      dimensions.bc.width = 340;
      dimensions.bc.height = 270;
      dimensions.connect.width = 340;
      dimensions.connect.height = 340;
  }

  if (type == 'article') {
    image = 'height="' + dimensions.bc.height + '" src="/sites/default/files/styles/bc_image_' + dimensions.bc.width +'x' + dimensions.bc.height + '/public'+ elem.image + '"';
    for (var index = 0; index < elem.showme.length; ++index) {
      showme += '<div class="field-item odd field-key-' + elem.showme[index] + '" title="' + names[elem.showme[index]] + '">' + names[elem.showme[index]] + '</div>';
    }
    content = '<div class="field field-name-field-bc-showme field-type-list-text field-label-hidden view-mode-teaser"><div class="field-items">' + showme + '</div></div>'
        + '<div class="field field-name-field-bc-summary field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + elem.summary + '</div></div></div>';
  }
  else if (type == 'ugc-image') {
    if (theme == 'brand_camp_theme') {
      image = 'height="' + dimensions.ugc.height + '" src="'+ elem.image + '" class="ugc_img"';
    } 
    else if (theme == 'wholefoods_mobile') {
      image = 'height="' + dimensions.ugc.height + '" src="'+ elem.image_full + '" class="ugc_img"';
    }
    else {
      image = 'height="' + dimensions.ugc.height + '" src="'+ elem.image + '" class="ugc_img"';
    }
    if (theme == 'brand_camp_theme') {
      content = '<div class="field field-name-field-bc-src-username field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even"><a href="/values-matter/your-stories?name=' + elem.user + '">@' + elem.user + '</a></div></div></div>' +
        '<div class="field field-name-field-bc-album-name field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + elem.album_name + '</div></div></div>' +
        '<div class="field field-name-field-bc-src-service field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + elem.service + '</div></div></div>' +
        '<div class="field field-name-field-bc-src-likes-count field-type-number-integer field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + elem.likes + '</div></div></div>';
    }
    else if (theme == 'wholefoods_mobile') {
      content = '<div class="field field-name-field-bc-src-username field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even"><a href="/values-matter/your-stories?name=' + elem.user + '">@' + elem.user + '</a></div></div></div>' +
        '<div class="field field-name-field-bc-album-name field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + elem.album_name + '</div></div></div>' +
        '<div class="field field-name-field-bc-src-likes-count field-type-number-integer field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + elem.likes + '</div></div></div>' +
        '<div class="field field-name-field-bc-src-service field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + Drupal.t('Via ') + elem.service + '</div></div></div>';
    }
    else {
      content = '<div class="field field-name-field-bc-src-username field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even"><a href="/values-matter/your-stories?name=' + elem.user + '">@' + elem.user + '</a></div></div></div>' +
        '<div class="field field-name-field-bc-album-name field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + elem.album_name + '</div></div></div>' +
        '<div class="field field-name-field-bc-src-likes-count field-type-number-integer field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + elem.likes + '</div></div></div>' +
        '<div class="field field-name-field-bc-src-service field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + elem.service + '</div></div></div>';
    }
  }
  else {
    image = 'height="' + dimensions.connect.height + '" src="/sites/default/files/styles/bc_image_' + dimensions.connect.width + 'x' + dimensions.connect.height + '/public'+ elem.image + '"';
  }
  return '<div class="views-row" nid="' + elem.nid + '"><article id="node-' + elem.nid + '" class="node node-bc-' + (elem.type ? elem.type : type) + ' node-teaser article clearfix">'
      + '<header class="node-header"><h1 property="dc:title" datatype="" class="node-title"><a href="' + elem.url + '" rel="bookmark" class="no-colorbox-node" data-inner-width="1060" data-inner-height="754">'
      + (type == 'article' ? elem.title + ' <span class="sub-title">' + elem.sub_title + '</span>' : 'title') + '</a></h1></header>'
      + '<div class="node-content"><div class="field field-name-field-bc-cover-image field-type-image field-label-hidden view-mode-teaser"><div class="field-items"><figure class="clearfix field-item even">'
      + '<a href="' + elem.url + '"><img width="' + dimensions.bc.width + '" '+ image +'></a></figure></div></div>'
      + content
      + '</div></article><div class="curtain" style="background-color:' + elem.curtain_color + '"></div></div>';
}
;/**/
(function(){var c=void 0,n=!0,s=null,C=!1,J=["aliceblue,antiquewhite,aqua,aquamarine,azure,beige,bisque,black,blanchedalmond,blue,blueviolet,brown,burlywood,cadetblue,chartreuse,chocolate,coral,cornflowerblue,cornsilk,crimson,cyan,darkblue,darkcyan,darkgoldenrod,darkgray,darkgreen,darkkhaki,darkmagenta,darkolivegreen,darkorange,darkorchid,darkred,darksalmon,darkseagreen,darkslateblue,darkslategray,darkturquoise,darkviolet,deeppink,deepskyblue,dimgray,dodgerblue,firebrick,floralwhite,forestgreen,fuchsia,gainsboro,ghostwhite,gold,goldenrod,gray,green,greenyellow,honeydew,hotpink,indianred,indigo,ivory,khaki,lavender,lavenderblush,lawngreen,lemonchiffon,lightblue,lightcoral,lightcyan,lightgoldenrodyellow,lightgreen,lightgrey,lightpink,lightsalmon,lightseagreen,lightskyblue,lightslategray,lightsteelblue,lightyellow,lime,limegreen,linen,magenta,maroon,mediumaquamarine,mediumblue,mediumorchid,mediumpurple,mediumseagreen,mediumslateblue,mediumspringgreen,mediumturquoise,mediumvioletred,midnightblue,mintcream,mistyrose,moccasin,navajowhite,navy,oldlace,olive,olivedrab,orange,orangered,orchid,palegoldenrod,palegreen,paleturquoise,palevioletred,papayawhip,peachpuff,peru,pink,plum,powderblue,purple,red,rosybrown,royalblue,saddlebrown,salmon,sandybrown,seagreen,seashell,sienna,silver,skyblue,slateblue,slategray,snow,springgreen,steelblue,tan,teal,thistle,tomato,transparent,turquoise,violet,wheat,white,whitesmoke,yellow,yellowgreen".split(","),
"all-scroll,col-resize,crosshair,default,e-resize,hand,help,move,n-resize,ne-resize,no-drop,not-allowed,nw-resize,pointer,progress,row-resize,s-resize,se-resize,sw-resize,text,vertical-text,w-resize,wait".split(","),"armenian,decimal,decimal-leading-zero,disc,georgian,lower-alpha,lower-greek,lower-latin,lower-roman,square,upper-alpha,upper-latin,upper-roman".split(","),"100,200,300,400,500,600,700,800,900,bold,bolder,lighter".split(","),"block-level,inline-level,table-caption,table-cell,table-column,table-column-group,table-footer-group,table-header-group,table-row,table-row-group".split(","),
"condensed,expanded,extra-condensed,extra-expanded,narrower,semi-condensed,semi-expanded,ultra-condensed,ultra-expanded,wider".split(","),"inherit,inline,inline-block,inline-box,inline-flex,inline-grid,inline-list-item,inline-stack,inline-table,run-in".split(","),"behind,center-left,center-right,far-left,far-right,left-side,leftwards,right-side,rightwards".split(","),"large,larger,small,smaller,x-large,x-small,xx-large,xx-small".split(","),"dashed,dotted,double,groove,outset,ridge,solid".split(","),
"ease,ease-in,ease-in-out,ease-out,linear,step-end,step-start".split(","),"at,closest-corner,closest-side,ellipse,farthest-corner,farthest-side".split(","),"baseline,middle,sub,super,text-bottom,text-top".split(","),"caption,icon,menu,message-box,small-caption,status-bar".split(","),"fast,faster,slow,slower,x-fast,x-slow".split(","),["above","below","higher","level","lower"],["cursive","fantasy","monospace","sans-serif","serif"],["loud","silent","soft","x-loud","x-soft"],["no-repeat","repeat-x","repeat-y",
"round","space"],["blink","line-through","overline","underline"],["block","flex","grid","table"],["high","low","x-high","x-low"],["nowrap","pre","pre-line","pre-wrap"],["absolute","relative","static"],["alternate","alternate-reverse","reverse"],["border-box","content-box","padding-box"],["capitalize","lowercase","uppercase"],["child","female","male"],["=","opacity"],["backwards","forwards"],["bidi-override","embed"],["bottom","top"],["break-all","keep-all"],["clip","ellipsis"],["contain","cover"],
["continuous","digits"],["end","start"],["flat","preserve-3d"],["hide","show"],["horizontal","vertical"],["inside","outside"],["italic","oblique"],["left","right"],["ltr","rtl"],["no-content","no-display"],["paused","running"],["suppress","unrestricted"],["thick","thin"],[","],["/"],["all"],["always"],["auto"],["avoid"],["both"],["break-word"],["center"],["circle"],["code"],["collapse"],["contents"],["fixed"],["hidden"],["infinite"],["inset"],["invert"],["justify"],["list-item"],["local"],["medium"],
["mix"],["none"],["normal"],["once"],["repeat"],["scroll"],["separate"],["small-caps"],["spell-out"],["to"],["visible"]],L={animation:{cssPropBits:517,cssLitGroup:[J[10],J[24],J[29],J[45],J[48],J[54],J[63],J[71],J[72]],cssFns:["cubic-bezier()","steps()"]},"animation-delay":{cssPropBits:5,cssLitGroup:[J[48]],cssFns:[]},"animation-direction":{cssPropBits:0,cssLitGroup:[J[24],J[48],J[72]],cssFns:[]},"animation-duration":"animation-delay","animation-fill-mode":{cssPropBits:0,cssLitGroup:[J[29],J[48],
J[54],J[71]],cssFns:[]},"animation-iteration-count":{cssPropBits:5,cssLitGroup:[J[48],J[63]],cssFns:[]},"animation-name":{cssPropBits:512,cssLitGroup:[J[48],J[71]],cssFns:[]},"animation-play-state":{cssPropBits:0,cssLitGroup:[J[45],J[48]],cssFns:[]},"animation-timing-function":{cssPropBits:0,cssLitGroup:[J[10],J[48]],cssFns:["cubic-bezier()","steps()"]},appearance:{cssPropBits:0,cssLitGroup:[J[71]],cssFns:[]},azimuth:{cssPropBits:5,cssLitGroup:[J[7],J[42],J[56]],cssFns:[]},"backface-visibility":{cssPropBits:0,
cssLitGroup:[J[59],J[62],J[80]],cssFns:[]},background:{cssPropBits:23,cssLitGroup:[J[0],J[18],J[25],J[31],J[34],J[42],J[48],J[49],J[52],J[56],J[61],J[68],J[71],J[74],J[75]],cssFns:"image(),linear-gradient(),radial-gradient(),repeating-linear-gradient(),repeating-radial-gradient(),rgb(),rgba()".split(",")},"background-attachment":{cssPropBits:0,cssLitGroup:[J[48],J[61],J[68],J[75]],cssFns:[]},"background-color":{cssPropBits:2,cssLitGroup:[J[0]],cssFns:["rgb()","rgba()"]},"background-image":{cssPropBits:16,
cssLitGroup:[J[48],J[71]],cssFns:["image()","linear-gradient()","radial-gradient()","repeating-linear-gradient()","repeating-radial-gradient()"]},"background-position":{cssPropBits:5,cssLitGroup:[J[31],J[42],J[48],J[56]],cssFns:[]},"background-repeat":{cssPropBits:0,cssLitGroup:[J[18],J[48],J[74]],cssFns:[]},"background-size":{cssPropBits:5,cssLitGroup:[J[34],J[48],J[52]],cssFns:[]},border:{cssPropBits:7,cssLitGroup:[J[0],J[9],J[47],J[62],J[64],J[69],J[71]],cssFns:["rgb()","rgba()"]},"border-bottom":"border",
"border-bottom-color":"background-color","border-bottom-left-radius":{cssPropBits:5,cssFns:[]},"border-bottom-right-radius":"border-bottom-left-radius","border-bottom-style":{cssPropBits:0,cssLitGroup:[J[9],J[62],J[64],J[71]],cssFns:[]},"border-bottom-width":{cssPropBits:5,cssLitGroup:[J[47],J[69]],cssFns:[]},"border-collapse":{cssPropBits:0,cssLitGroup:[J[59],J[76]],cssFns:[]},"border-color":"background-color","border-left":"border","border-left-color":"background-color","border-left-style":"border-bottom-style",
"border-left-width":"border-bottom-width","border-radius":{cssPropBits:5,cssLitGroup:[J[49]],cssFns:[]},"border-right":"border","border-right-color":"background-color","border-right-style":"border-bottom-style","border-right-width":"border-bottom-width","border-spacing":"border-bottom-left-radius","border-style":"border-bottom-style","border-top":"border","border-top-color":"background-color","border-top-left-radius":"border-bottom-left-radius","border-top-right-radius":"border-bottom-left-radius",
"border-top-style":"border-bottom-style","border-top-width":"border-bottom-width","border-width":"border-bottom-width",bottom:{cssPropBits:5,cssLitGroup:[J[52]],cssFns:[]},box:{cssPropBits:0,cssLitGroup:[J[60],J[71],J[72]],cssFns:[]},"box-shadow":{cssPropBits:7,cssLitGroup:[J[0],J[48],J[64],J[71]],cssFns:["rgb()","rgba()"]},"box-sizing":{cssPropBits:0,cssLitGroup:[J[25]],cssFns:[]},"caption-side":{cssPropBits:0,cssLitGroup:[J[31]],cssFns:[]},clear:{cssPropBits:0,cssLitGroup:[J[42],J[54],J[71]],cssFns:[]},
clip:{cssPropBits:0,cssLitGroup:[J[52]],cssFns:["rect()"]},color:"background-color",content:{cssPropBits:8,cssLitGroup:[J[71],J[72]],cssFns:[]},cue:{cssPropBits:16,cssLitGroup:[J[71]],cssFns:[]},"cue-after":"cue","cue-before":"cue",cursor:{cssPropBits:16,cssLitGroup:[J[1],J[48],J[52]],cssFns:[]},direction:{cssPropBits:0,cssLitGroup:[J[43]],cssFns:[]},display:{cssPropBits:0,cssLitGroup:[J[4],J[6],J[20],J[52],J[67],J[71]],cssFns:[]},"display-extras":{cssPropBits:0,cssLitGroup:[J[67],J[71]],cssFns:[]},
"display-inside":{cssPropBits:0,cssLitGroup:[J[20],J[52]],cssFns:[]},"display-outside":{cssPropBits:0,cssLitGroup:[J[4],J[71]],cssFns:[]},elevation:{cssPropBits:5,cssLitGroup:[J[15]],cssFns:[]},"empty-cells":{cssPropBits:0,cssLitGroup:[J[38]],cssFns:[]},filter:{cssPropBits:0,cssFns:["alpha()"]},"float":{cssPropBits:0,cssLitGroup:[J[42],J[71]],cssFns:[]},font:{cssPropBits:73,cssLitGroup:[J[3],J[8],J[13],J[16],J[41],J[48],J[49],J[69],J[72],J[77]],cssFns:[]},"font-family":{cssPropBits:72,cssLitGroup:[J[16],
J[48]],cssFns:[]},"font-size":{cssPropBits:1,cssLitGroup:[J[8],J[69]],cssFns:[]},"font-stretch":{cssPropBits:0,cssLitGroup:[J[5],J[72]],cssFns:[]},"font-style":{cssPropBits:0,cssLitGroup:[J[41],J[72]],cssFns:[]},"font-variant":{cssPropBits:0,cssLitGroup:[J[72],J[77]],cssFns:[]},"font-weight":{cssPropBits:0,cssLitGroup:[J[3],J[72]],cssFns:[]},height:"bottom",left:"bottom","letter-spacing":{cssPropBits:5,cssLitGroup:[J[72]],cssFns:[]},"line-height":{cssPropBits:1,cssLitGroup:[J[72]],cssFns:[]},"list-style":{cssPropBits:16,
cssLitGroup:[J[2],J[40],J[57],J[71]],cssFns:["image()","linear-gradient()","radial-gradient()","repeating-linear-gradient()","repeating-radial-gradient()"]},"list-style-image":{cssPropBits:16,cssLitGroup:[J[71]],cssFns:["image()","linear-gradient()","radial-gradient()","repeating-linear-gradient()","repeating-radial-gradient()"]},"list-style-position":{cssPropBits:0,cssLitGroup:[J[40]],cssFns:[]},"list-style-type":{cssPropBits:0,cssLitGroup:[J[2],J[57],J[71]],cssFns:[]},margin:"bottom","margin-bottom":"bottom",
"margin-left":"bottom","margin-right":"bottom","margin-top":"bottom","max-height":{cssPropBits:1,cssLitGroup:[J[52],J[71]],cssFns:[]},"max-width":"max-height","min-height":{cssPropBits:1,cssLitGroup:[J[52]],cssFns:[]},"min-width":"min-height",opacity:{cssPropBits:1,cssFns:[]},outline:{cssPropBits:7,cssLitGroup:[J[0],J[9],J[47],J[62],J[64],J[65],J[69],J[71]],cssFns:["rgb()","rgba()"]},"outline-color":{cssPropBits:2,cssLitGroup:[J[0],J[65]],cssFns:["rgb()","rgba()"]},"outline-style":"border-bottom-style",
"outline-width":"border-bottom-width",overflow:{cssPropBits:0,cssLitGroup:[J[52],J[62],J[75],J[80]],cssFns:[]},"overflow-wrap":{cssPropBits:0,cssLitGroup:[J[55],J[72]],cssFns:[]},"overflow-x":{cssPropBits:0,cssLitGroup:[J[44],J[52],J[62],J[75],J[80]],cssFns:[]},"overflow-y":"overflow-x",padding:"opacity","padding-bottom":"opacity","padding-left":"opacity","padding-right":"opacity","padding-top":"opacity","page-break-after":{cssPropBits:0,cssLitGroup:[J[42],J[51],J[52],J[53]],cssFns:[]},"page-break-before":"page-break-after",
"page-break-inside":{cssPropBits:0,cssLitGroup:[J[52],J[53]],cssFns:[]},pause:"border-bottom-left-radius","pause-after":"border-bottom-left-radius","pause-before":"border-bottom-left-radius",perspective:{cssPropBits:5,cssLitGroup:[J[71]],cssFns:[]},"perspective-origin":{cssPropBits:5,cssLitGroup:[J[31],J[42],J[56]],cssFns:[]},pitch:{cssPropBits:5,cssLitGroup:[J[21],J[69]],cssFns:[]},"pitch-range":"border-bottom-left-radius","play-during":{cssPropBits:16,cssLitGroup:[J[52],J[70],J[71],J[74]],cssFns:[]},
position:{cssPropBits:0,cssLitGroup:[J[23]],cssFns:[]},quotes:{cssPropBits:8,cssLitGroup:[J[71]],cssFns:[]},resize:{cssPropBits:0,cssLitGroup:[J[39],J[54],J[71]],cssFns:[]},richness:"border-bottom-left-radius",right:"bottom",speak:{cssPropBits:0,cssLitGroup:[J[71],J[72],J[78]],cssFns:[]},"speak-header":{cssPropBits:0,cssLitGroup:[J[51],J[73]],cssFns:[]},"speak-numeral":{cssPropBits:0,cssLitGroup:[J[35]],cssFns:[]},"speak-punctuation":{cssPropBits:0,cssLitGroup:[J[58],J[71]],cssFns:[]},"speech-rate":{cssPropBits:5,
cssLitGroup:[J[14],J[69]],cssFns:[]},stress:"border-bottom-left-radius","table-layout":{cssPropBits:0,cssLitGroup:[J[52],J[61]],cssFns:[]},"text-align":{cssPropBits:0,cssLitGroup:[J[42],J[56],J[66]],cssFns:[]},"text-decoration":{cssPropBits:0,cssLitGroup:[J[19],J[71]],cssFns:[]},"text-indent":"border-bottom-left-radius","text-overflow":{cssPropBits:8,cssLitGroup:[J[33]],cssFns:[]},"text-shadow":"box-shadow","text-transform":{cssPropBits:0,cssLitGroup:[J[26],J[71]],cssFns:[]},"text-wrap":{cssPropBits:0,
cssLitGroup:[J[46],J[71],J[72]],cssFns:[]},top:"bottom",transform:{cssPropBits:0,cssLitGroup:[J[71]],cssFns:"matrix(),perspective(),rotate(),rotate3d(),rotatex(),rotatey(),rotatez(),scale(),scale3d(),scalex(),scaley(),scalez(),skew(),skewx(),skewy(),translate(),translate3d(),translatex(),translatey(),translatez()".split(",")},"transform-origin":"perspective-origin","transform-style":{cssPropBits:0,cssLitGroup:[J[37]],cssFns:[]},transition:{cssPropBits:1029,cssLitGroup:[J[10],J[48],J[50],J[71]],cssFns:["cubic-bezier()",
"steps()"]},"transition-delay":"animation-delay","transition-duration":"animation-delay","transition-property":{cssPropBits:1024,cssLitGroup:[J[48],J[50]],cssFns:[]},"transition-timing-function":"animation-timing-function","unicode-bidi":{cssPropBits:0,cssLitGroup:[J[30],J[72]],cssFns:[]},"vertical-align":{cssPropBits:5,cssLitGroup:[J[12],J[31]],cssFns:[]},visibility:"backface-visibility","voice-family":{cssPropBits:8,cssLitGroup:[J[27],J[48]],cssFns:[]},volume:{cssPropBits:1,cssLitGroup:[J[17],J[69]],
cssFns:[]},"white-space":{cssPropBits:0,cssLitGroup:[J[22],J[72]],cssFns:[]},width:"min-height","word-break":{cssPropBits:0,cssLitGroup:[J[32],J[72]],cssFns:[]},"word-spacing":"letter-spacing","word-wrap":"overflow-wrap","z-index":"bottom",zoom:"line-height","cubic-bezier()":"animation-delay","steps()":{cssPropBits:5,cssLitGroup:[J[36],J[48]],cssFns:[]},"image()":{cssPropBits:18,cssLitGroup:[J[0],J[48]],cssFns:["rgb()","rgba()"]},"linear-gradient()":{cssPropBits:7,cssLitGroup:[J[0],J[31],J[42],J[48],
J[79]],cssFns:["rgb()","rgba()"]},"radial-gradient()":{cssPropBits:7,cssLitGroup:[J[0],J[11],J[31],J[42],J[48],J[56],J[57]],cssFns:["rgb()","rgba()"]},"repeating-linear-gradient()":"linear-gradient()","repeating-radial-gradient()":"radial-gradient()","rgb()":{cssPropBits:1,cssLitGroup:[J[48]],cssFns:[]},"rgba()":"rgb()","rect()":{cssPropBits:5,cssLitGroup:[J[48],J[52]],cssFns:[]},"alpha()":{cssPropBits:1,cssLitGroup:[J[28]],cssFns:[]},"matrix()":"animation-delay","perspective()":"border-bottom-left-radius",
"rotate()":"border-bottom-left-radius","rotate3d()":"animation-delay","rotatex()":"border-bottom-left-radius","rotatey()":"border-bottom-left-radius","rotatez()":"border-bottom-left-radius","scale()":"animation-delay","scale3d()":"animation-delay","scalex()":"border-bottom-left-radius","scaley()":"border-bottom-left-radius","scalez()":"border-bottom-left-radius","skew()":"animation-delay","skewx()":"border-bottom-left-radius","skewy()":"border-bottom-left-radius","translate()":"animation-delay","translate3d()":"animation-delay",
"translatex()":"border-bottom-left-radius","translatey()":"border-bottom-left-radius","translatez()":"border-bottom-left-radius"},O;for(O in L)"string"===typeof L[O]&&Object.hasOwnProperty.call(L,O)&&(L[O]=L[L[O]]);"undefined"!==typeof window&&(window.cssSchema=L);var U,X;
(function(){function g(a){var f=parseInt(a.substring(1),16);return 65535<f?(f-=65536,String.fromCharCode(55296+(f>>10),56320+(f&1023))):f==f?String.fromCharCode(f):" ">a[1]?"":a[1]}function w(a,f){return'"'+a.replace(/[\u0000-\u001f\\\"<>]/g,f)+'"'}function M(a){return E[a]||(E[a]="\\"+a.charCodeAt(0).toString(16)+" ")}function x(a){return e[a]||(e[a]=("\u0010">a?"%0":"%")+a.charCodeAt(0).toString(16))}var E={"\\":"\\\\"},e={"\\":"%5c"},v=RegExp("\\uFEFF|U[+][0-9A-F?]{1,6}(?:-[0-9A-F]{1,6})?|url[(][\\t\\n\\f ]*(?:\"(?:'|[^'\"\\n\\f\\\\]|\\\\[\\s\\S])*\"|'(?:\"|[^'\"\\n\\f\\\\]|\\\\[\\s\\S])*'|(?:[\\t\\x21\\x23-\\x26\\x28-\\x5b\\x5d-\\x7e]|[\\u0080-\\ud7ff\\ue000-\\ufffd]|[\\ud800-\\udbff][\\udc00-\\udfff]|\\\\(?:[0-9a-fA-F]{1,6}[\\t\\n\\f ]?|[\\u0020-\\u007e\\u0080-\\ud7ff\\ue000\\ufffd]|[\\ud800-\\udbff][\\udc00-\\udfff]))*)[\\t\\n\\f ]*[)]|(?!url[(])-?(?:[a-zA-Z_]|[\\u0080-\\ud7ff\\ue000-\\ufffd]|[\\ud800-\\udbff][\\udc00-\\udfff]|\\\\(?:[0-9a-fA-F]{1,6}[\\t\\n\\f ]?|[\\u0020-\\u007e\\u0080-\\ud7ff\\ue000\\ufffd]|[\\ud800-\\udbff][\\udc00-\\udfff]))(?:[a-zA-Z0-9_-]|[\\u0080-\\ud7ff\\ue000-\\ufffd]|[\\ud800-\\udbff][\\udc00-\\udfff]|\\\\(?:[0-9a-fA-F]{1,6}[\\t\\n\\f ]?|[\\u0020-\\u007e\\u0080-\\ud7ff\\ue000\\ufffd]|[\\ud800-\\udbff][\\udc00-\\udfff]))*[(]|(?:@?-?(?:[a-zA-Z_]|[\\u0080-\\ud7ff\\ue000-\\ufffd]|[\\ud800-\\udbff][\\udc00-\\udfff]|\\\\(?:[0-9a-fA-F]{1,6}[\\t\\n\\f ]?|[\\u0020-\\u007e\\u0080-\\ud7ff\\ue000\\ufffd]|[\\ud800-\\udbff][\\udc00-\\udfff]))|#)(?:[a-zA-Z0-9_-]|[\\u0080-\\ud7ff\\ue000-\\ufffd]|[\\ud800-\\udbff][\\udc00-\\udfff]|\\\\(?:[0-9a-fA-F]{1,6}[\\t\\n\\f ]?|[\\u0020-\\u007e\\u0080-\\ud7ff\\ue000\\ufffd]|[\\ud800-\\udbff][\\udc00-\\udfff]))*|\"(?:'|[^'\"\\n\\f\\\\]|\\\\[\\s\\S])*\"|'(?:\"|[^'\"\\n\\f\\\\]|\\\\[\\s\\S])*'|[-+]?(?:[0-9]+(?:[.][0-9]+)?|[.][0-9]+)(?:%|-?(?:[a-zA-Z_]|[\\u0080-\\ud7ff\\ue000-\\ufffd]|[\\ud800-\\udbff][\\udc00-\\udfff]|\\\\(?:[0-9a-fA-F]{1,6}[\\t\\n\\f ]?|[\\u0020-\\u007e\\u0080-\\ud7ff\\ue000\\ufffd]|[\\ud800-\\udbff][\\udc00-\\udfff]))(?:[a-zA-Z0-9_-]|[\\u0080-\\ud7ff\\ue000-\\ufffd]|[\\ud800-\\udbff][\\udc00-\\udfff]|\\\\(?:[0-9a-fA-F]{1,6}[\\t\\n\\f ]?|[\\u0020-\\u007e\\u0080-\\ud7ff\\ue000\\ufffd]|[\\ud800-\\udbff][\\udc00-\\udfff]))*)?|<\!--|--\>|[\\t\\n\\f ]+|/(?:[*][^*]*[*]+(?:[^/][^*]*[*]+)*/|/[^\\n\\f]*)|[~|^$*]=|[^\"'\\\\/]|/(?![/*])","gi"),
b=RegExp("\\\\(?:(?:[0-9a-fA-F]{1,6}[\\t\\n\\f ]?|[\\u0020-\\u007e\\u0080-\\ud7ff\\ue000\\ufffd]|[\\ud800-\\udbff][\\udc00-\\udfff])|[\\n\\f])","g"),a=RegExp("^url\\([\\t\\n\\f ]*[\"']?|[\"']?[\\t\\n\\f ]*\\)$","gi");X=function(a){return a.replace(b,g)};U=function(b){for(var b=(""+b).replace(/\r\n?/g,"\n").match(v)||[],f=0,h=" ",d=0,y=b.length;d<y;++d){var l=X(b[d]),V=l.length,g=l.charCodeAt(0),l=34==g||39==g?w(l.substring(1,V-1),M):47==g&&1<V||"\\"==l||"--\>"==l||"<\!--"==l||"\ufeff"==l||32>=g?" ":
/url\(/i.test(l)?"url("+w(l.replace(a,""),x)+")":l;if(h!=l||" "!=l)b[f++]=h=l}b.length=f;return b}})();"undefined"!==typeof window&&(window.lexCss=U,window.decodeCss=X);var Y=function(){function g(d){d=(""+d).match(k);return!d?s:new e(v(d[1]),v(d[2]),v(d[3]),v(d[4]),v(d[5]),v(d[6]),v(d[7]))}function w(d,a){return"string"==typeof d?encodeURI(d).replace(a,M):s}function M(d){d=d.charCodeAt(0);return"%"+"0123456789ABCDEF".charAt(d>>4&15)+"0123456789ABCDEF".charAt(d&15)}function x(d){if(d===s)return s;for(var d=d.replace(/(^|\/)\.(?:\/|$)/g,"$1").replace(/\/{2,}/g,"/"),a=b,h;(h=d.replace(a,"$1"))!=d;d=h);return d}function E(d,h){var b=d.T(),f=h.K();f?b.ga(h.j):f=h.X();
f?b.da(h.n):f=h.Y();f?b.ea(h.k):f=h.$();var g=h.g,k=x(g);if(f)b.ca(h.V()),k=k&&k.replace(a,"");else if(f=!!g){if(47!==k.charCodeAt(0))var k=x(b.g||"").replace(a,""),e=k.lastIndexOf("/")+1,k=x((e?k.substring(0,e):"")+x(g)).replace(a,"")}else k=k&&k.replace(a,""),k!==g&&b.G(k);f?b.G(k):f=h.aa();f?b.O(h.l):f=h.Z();f&&b.fa(h.o);return b}function e(d,a,h,f,b,g,k){this.j=d;this.n=a;this.k=h;this.h=f;this.g=b;this.l=g;this.o=k}function v(d){return"string"==typeof d&&0<d.length?d:s}var b=RegExp(/(\/|^)(?:[^./][^/]*|\.{2,}(?:[^./][^/]*)|\.{3,}[^/]*)\/\.\.(?:\/|$)/),
a=/^(?:\.\.\/)*(?:\.\.$)?/;e.prototype.toString=function(){var d=[];s!==this.j&&d.push(this.j,":");s!==this.k&&(d.push("//"),s!==this.n&&d.push(this.n,"@"),d.push(this.k),s!==this.h&&d.push(":",this.h.toString()));s!==this.g&&d.push(this.g);s!==this.l&&d.push("?",this.l);s!==this.o&&d.push("#",this.o);return d.join("")};e.prototype.T=function(){return new e(this.j,this.n,this.k,this.h,this.g,this.l,this.o)};e.prototype.W=function(){return this.j&&decodeURIComponent(this.j).toLowerCase()};e.prototype.ga=
function(d){this.j=d?d:s};e.prototype.K=function(){return s!==this.j};e.prototype.da=function(d){this.n=d?d:s};e.prototype.X=function(){return s!==this.n};e.prototype.ea=function(d){this.k=d?d:s;this.G(this.g)};e.prototype.Y=function(){return s!==this.k};e.prototype.V=function(){return this.h&&decodeURIComponent(this.h)};e.prototype.ca=function(d){if(d){d=Number(d);if(d!==(d&65535))throw Error("Bad port number "+d);this.h=""+d}else this.h=s};e.prototype.$=function(){return s!==this.h};e.prototype.U=
function(){return this.g&&decodeURIComponent(this.g)};e.prototype.G=function(d){d?(d=""+d,this.g=!this.k||/^\//.test(d)?d:"/"+d):this.g=s};e.prototype.O=function(d){this.l=d?d:s};e.prototype.aa=function(){return s!==this.l};e.prototype.ba=function(d){if("object"===typeof d&&!(d instanceof Array)&&(d instanceof Object||"[object Array]"!==Object.prototype.toString.call(d))){var a=[],h=-1,f;for(f in d){var b=d[f];"string"===typeof b&&(a[++h]=f,a[++h]=b)}d=a}for(var a=[],h="",g=0;g<d.length;)f=d[g++],
b=d[g++],a.push(h,encodeURIComponent(f.toString())),h="&",b&&a.push("=",encodeURIComponent(b.toString()));this.l=a.join("")};e.prototype.fa=function(d){this.o=d?d:s};e.prototype.Z=function(){return s!==this.o};var k=/^(?:([^:/?#]+):)?(?:\/\/(?:([^/?#]*)@)?([^/?#:@]*)(?::([0-9]+))?)?([^?#]+)?(?:\?([^#]*))?(?:#(.*))?$/,f=/[#\/\?@]/g,h=/[\#\?]/g;e.parse=g;e.create=function(d,a,b,g,k,Q,N){d=new e(w(d,f),w(a,f),"string"==typeof b?encodeURIComponent(b):s,0<g?g.toString():s,w(k,h),s,"string"==typeof N?encodeURIComponent(N):
s);Q&&("string"===typeof Q?d.O(Q.replace(/[^?&=0-9A-Za-z_\-~.%]/g,M)):d.ba(Q));return d};e.N=E;e.ma=x;e.ha={ua:function(d){return/\.html$/.test(g(d).U())?"text/html":"application/javascript"},N:function(d,a){return d?E(g(d),g(a)).toString():""+a}};return e}();"undefined"!==typeof window&&(window.URI=Y);var aa=c,ba=c,da=c,Z=c;
(function(){function g(a){return"string"===typeof a?'url("'+a.replace(e,w)+'")':'url("about:blank")'}function w(a){return v[a]}function M(a,d){return a?Y.ha.N(a,d):d}function x(h,d,f){if(!f)return s;var g=(""+h).match(b);return g&&(!g[1]||a.test(g[1]))?f(h,d):s}function E(a){return a.replace(/^-(?:apple|css|epub|khtml|moz|mso?|o|rim|wap|webkit|xv)-(?=[a-z])/,"")}var e=/[\n\f\r\"\'()*<>]/g,v={"\n":"%0a","\u000c":"%0c","\r":"%0d",'"':"%22","'":"%27","(":"%28",")":"%29","*":"%2a","<":"%3c",">":"%3e"},
b=/^(?:([^:/?# ]+):)?/,a=/^(?:https?|mailto)$/i;aa=function(){var a={};return function y(f,b,k,e,N){var f=E(f),u=L[f];if(!u||"object"!==typeof u)b.length=0;else{for(var i=u.cssPropBits,q=i&80,B=i&1536,F=NaN,r=0,o=0;r<b.length;++r){var j=b[r].toLowerCase(),I=j.charCodeAt(0),R,v,P,S,D,w;if(32===I)j="";else if(34===I)j=16===q?k?g(x(M(e,X(b[r].substring(1,j.length-1))),f,k)):"":i&8&&!(q&q-1)?j:"";else if("inherit"!==j){if(D=u.cssLitGroup){var G;if(!(G=u.cssLitMap)){G={};for(var K=D.length;0<=--K;)for(var A=
D[K],T=A.length;0<=--T;)G[A[T]]=a;G=u.cssLitMap=G}D=G}else D=a;if(!(w=D,w[E(j)]===a))if(35===I&&/^#(?:[0-9a-f]{3}){1,2}$/.test(j))j=i&2?j:"";else if(48<=I&&57>=I)j=i&1?j:"";else if(R=j.charCodeAt(1),v=j.charCodeAt(2),P=48<=R&&57>=R,S=48<=v&&57>=v,43===I&&(P||46===R&&S))j=i&1?(P?"":"0")+j.substring(1):"";else if(45===I&&(P||46===R&&S))j=i&4?(P?"-":"-0")+j.substring(1):i&1?"0":"";else if(46===I&&P)j=i&1?"0"+j:"";else if('url("'===j.substring(0,5))j=k&&i&16?g(x(M(e,b[r].substring(5,j.length-2)),f,k)):
"";else if("("===j.charAt(j.length-1))a:{D=b;G=r;j=1;K=G+1;for(I=D.length;K<I&&j;)A=D[K++],j+=")"===A?-1:/^[^"']*\($/.test(A);if(!j){j=D[G].toLowerCase();I=E(j);D=D.splice(G,K-G,"");G=u.cssFns;K=0;for(A=G.length;K<A;++K)if(G[K].substring(0,I.length)==I){D[0]=D[D.length-1]="";y(G[K],D,k,e);j=j+D.join(" ")+")";break a}}j=""}else j=B&&/^-?[a-z_][\w\-]*$/.test(j)&&!/__$/.test(j)?N&&512===B?b[r]+N:1024===B&&L[j]&&"number"===typeof L[j].oa?j:"":/^\w+$/.test(j)&&64===q&&i&8?F+1===o?(b[F]=b[F].substring(0,
b[F].length-1)+" "+j+'"',""):(F=o,'"'+j+'"'):""}j&&(b[o++]=j)}1===o&&'url("about:blank")'===b[0]&&(o=0);b.length=o}}}();var k=RegExp("^(active|after|before|blank|checked|default|disabled|drop|empty|enabled|first|first-child|first-letter|first-line|first-of-type|fullscreen|focus|hover|in-range|indeterminate|invalid|last-child|last-of-type|left|link|only-child|only-of-type|optional|out-of-range|placeholder-shown|read-only|read-write|required|right|root|scope|user-error|valid|visited)$"),f={};f[">"]=
f["+"]=f["~"]=f;ba=function(a,d,b){function g(i,r){function o(b,f,g){var y,e,i,l,o,m=n;y="";if(b<f)if(o=a[b],"*"===o)++b,y=o;else if(/^[a-zA-Z]/.test(o)&&(e=x(o.toLowerCase(),[])))"tagName"in e&&(o=e.tagName),++b,y=o;for(l=i=e="";m&&b<f;++b)if(o=a[b],"#"===o.charAt(0))/^#_|__$|[^\w#:\-]/.test(o)?m=C:e+=o+v;else if("."===o)++b<f&&/^[0-9A-Za-z:_\-]+$/.test(o=a[b])&&!/^_|__$/.test(o)?e+="."+o:m=C;else if(b+1<f&&"["===a[b]){++b;var H=a[b++].toLowerCase();o=$.m[y+"::"+H];o!==+o&&(o=$.m["*::"+H]);var W;
d.ia?(W=d.ia(y,H),"string"!==typeof W&&(m=C,W=H),m&&o!==+o&&(o=$.d.NONE)):(W=H,o!==+o&&(m=C));var p=H="",ca=C;/^[~^$*|]?=$/.test(a[b])&&(H=a[b++],p=a[b++],/^[0-9A-Za-z:_\-]+$/.test(p)?p='"'+p+'"':"]"===p&&(p='""',--b),/^"([^\"\\]|\\.)*"$/.test(p)||(m=C),(ca="i"===a[b])&&++b);"]"!==a[b]&&(++b,m=C);switch(o){case $.d.CLASSES:case $.d.LOCAL_NAME:case $.d.NONE:break;case $.d.GLOBAL_NAME:case $.d.ID:case $.d.IDREF:("="===H||"~="===H||"$="===H)&&'""'!=p&&!ca?p='"'+p.substring(1,p.length-1)+v+'"':"|="===
H||""===H||(m=C);break;case $.d.URI:case $.d.URI_FRAGMENT:""!==H&&(m=C);break;default:m=C}m&&(i+="["+W.replace(/[^\w-]/g,"\\$&")+H+p+(ca?" i]":"]"))}else if(b<f&&":"===a[b])if(o=a[++b],k.test(o))l+=":"+o;else break;else break;b!==f&&(m=C);m&&(b=(y+e).replace(/[^ .*#\w-]/g,"\\$&")+i+l+g)&&j.push(b);return m}" "===a[i]&&++i;r-1!==i&&" "===a[r]&&--r;for(var j=[],l=i,q=n,u=i;q&&u<r;++u){var B=a[u];if(f[B]===f||" "===B)o(l,u,B)?l=u+1:q=C}o(l,r,"")||(q=C);return q?(j.length&&(l=j.join(""),e!==s&&(l="."+
e+" "+l),N.push(l)),n):!b||b(a.slice(i,r))}var e=d.na,v=d.L,x=d.Aa,N=[],u=0,i,q=0,B;for(i=0;i<a.length;++i)if(B=a[i],"("==B||"["==B?(++q,n):")"==B||"]"==B?(q&&--q,n):!(" "==a[i]&&(q||f[a[i-1]]===f||f[a[i+1]]===f)))a[u++]=a[i];a.length=u;u=a.length;for(i=q=0;i<u;++i)if(","===a[i]){if(!g(q,i))return s;q=i+1}return!g(q,u)?s:N};(function(){var a=/^\w/,d=RegExp("^(?:(?:(?:(?:only|not) )?(?:all|aural|braille|embossed|handheld|print|projection|screen|speech|tty|tv)|\\( (?:(?:min-|max-)?(?:(?:device-)?(?:aspect-ratio|height|width)|color(?:-index)?|monochrome|orientation|resolution)|grid|hover|luminosity|pointer|scan|script) (?:: -?(?:[a-z]\\w+(?:-\\w+)*|\\d+(?: / \\d+|(?:\\.\\d+)?(?:p[cxt]|[cem]m|in|dpi|dppx|dpcm|%)?)) )?\\))(?: and ?\\( (?:(?:min-|max-)?(?:(?:device-)?(?:aspect-ratio|height|width)|color(?:-index)?|monochrome|orientation|resolution)|grid|hover|luminosity|pointer|scan|script) (?:: -?(?:[a-z]\\w+(?:-\\w+)*|\\d+(?: / \\d+|(?:\\.\\d+)?(?:p[cxt]|[cem]m|in|dpi|dppx|dpcm|%)?)) )?\\))*)(?: , (?:(?:(?:(?:only|not) )?(?:all|aural|braille|embossed|handheld|print|projection|screen|speech|tty|tv)|\\( (?:(?:min-|max-)?(?:(?:device-)?(?:aspect-ratio|height|width)|color(?:-index)?|monochrome|orientation|resolution)|grid|hover|luminosity|pointer|scan|script) (?:: -?(?:[a-z]\\w+(?:-\\w+)*|\\d+(?: / \\d+|(?:\\.\\d+)?(?:p[cxt]|[cem]m|in|dpi|dppx|dpcm|%)?)) )?\\))(?: and ?\\( (?:(?:min-|max-)?(?:(?:device-)?(?:aspect-ratio|height|width)|color(?:-index)?|monochrome|orientation|resolution)|grid|hover|luminosity|pointer|scan|script) (?:: -?(?:[a-z]\\w+(?:-\\w+)*|\\d+(?: / \\d+|(?:\\.\\d+)?(?:p[cxt]|[cem]m|in|dpi|dppx|dpcm|%)?)) )?\\))*))*$",
"i");Z=function(b){for(var b=b.slice(),f=b.length,g=0,k=0;k<f;++k){var e=b[k];" "!=e&&(b[g++]=e)}b.length=g;b=b.join(" ");return b=!b.length?"":!d.test(b)?"not all":a.test(b)?b:"not all , "+b}})();(function(){function a(b){var d=/^\s*[']([^']*)[']\s*$/,f=/^\s*url\s*[(]["]([^"]*)["][)]\s*$/,g=/^\s*url\s*[(][']([^']*)['][)]\s*$/,h=/^\s*url\s*[(]([^)]*)[)]\s*$/,k;return(k=/^\s*["]([^"]*)["]\s*$/.exec(b))||(k=d.exec(b))||(k=f.exec(b))||(k=g.exec(b))||(k=h.exec(b))?k[1]:s}function b(f,g,k,e,v,w,u){function i(){r=
F.length&&F[F.length-1]===s}var q=c,B=u||[0],F=[],r=C;fa(g,{startStylesheet:function(){q=[]},endStylesheet:function(){},startAtrule:function(g,j){if(r)g=s;else if("@media"===g)q.push("@media"," ",Z(j));else if("@keyframes"===g||"@-webkit-keyframes"===g){var i=j[0];1===j.length&&!/__$|[^\w\-]/.test(i)?(q.push(g," ",i+k.L),g="@keyframes"):g=s}else if("@import"===g&&0<j.length)if(g=s,"function"===typeof w){var l=Z(j.slice(1));if("not all"!==l){++B[0];var u=[];q.push(u);var E=x(M(f,a(j[0])),function(a){var f=
b(E,a.qa,k,e,v,w,B);--B[0];a=l?{toString:function(){return"@media "+l+" {"+f.result+"}"}}:f.result;u[0]=a;w(a,!!B[0])},v)}}else window.console&&window.console.log("@import "+j.join(" ")+" elided");r=!g;F.push(g)},endAtrule:function(){F.pop();r||q.push(";");i()},startBlock:function(){r||q.push("{")},endBlock:function(){r||(q.push("}"),r=n)},startRuleset:function(a){if(!r){var b=c;"@keyframes"===F[F.length-1]?(b=a.join(" ").match(/^ *(?:from|to|\d+(?:\.\d+)?%) *(?:, *(?:from|to|\d+(?:\.\d+)?%) *)*$/i),
r=!b,b&&(b=b[0].replace(/ +/g,""))):(a=ba(a,k),!a||!a.length?r=n:b=a.join(", "));r||q.push(b,"{")}F.push(s)},endRuleset:function(){F.pop();r||q.push("}");i()},declaration:function(a,b){if(!r){var d=C,g=b.length;2<=g&&"!"===b[g-2]&&"important"===b[g-1].toLowerCase()&&(d=n,b.length-=2);aa(a,b,e,f,k.L);b.length&&q.push(a,":",b.join(" "),d?" !important;":";")}}});return{result:{toString:function(){return q.join("")}},va:!!B[0]}}da=function(a,f,g,k){return b(a,f,g,k,c,c).result.toString()}})()})();
"undefined"!==typeof window&&(window.sanitizeCssProperty=aa,window.sanitizeCssSelectorList=ba,window.sanitizeStylesheet=da,window.sanitizeMediaQuery=Z);var fa,ga;
(function(){function g(b,a,g,f,h){for(var d=a++;a<g&&"{"!==b[a]&&";"!==b[a];)++a;if(a<g&&(h||";"===b[a])){var h=d+1,e=a;h<g&&" "===b[h]&&++h;e>h&&" "===b[e-1]&&--e;f.startAtrule&&f.startAtrule(b[d].toLowerCase(),b.slice(h,e));a="{"===b[a]?w(b,a,g,f):a+1;f.endAtrule&&f.endAtrule()}return a}function w(b,a,k,f){++a;for(f.startBlock&&f.startBlock();a<k;){var h=b[a].charAt(0);if("}"==h){++a;break}a=" "===h||";"===h?a+1:"@"===h?g(b,a,k,f,C):"{"===h?w(b,a,k,f):M(b,a,k,f)}f.endBlock&&f.endBlock();return a}
function M(b,a,g,f){var h=a,d=x(b,a,g,n);if(0>d)return d=~d,d===h?d+1:d;var y=b[d];if("{"!==y)return d===h?d+1:d;a=d+1;d>h&&" "===b[d-1]&&--d;for(f.startRuleset&&f.startRuleset(b.slice(h,d));a<g;){y=b[a];if("}"===y){++a;break}a=" "===y?a+1:e(b,a,g,f)}f.endRuleset&&f.endRuleset();return a}function x(b,a,g,f){for(var h,d=[],e=-1;a<g;++a)if(h=b[a].charAt(0),"["===h||"("===h)d[++e]=h;else if("]"===h&&"["===d[e]||")"===h&&"("===d[e])--e;else if("{"===h||"}"===h||";"===h||"@"===h||":"===h&&!f)break;0<=
e&&(a=~(a+1));return a}function E(b,a,g){for(;a<g&&";"!==b[a]&&"}"!==b[a];)++a;return a<g&&";"===b[a]?a+1:a}function e(b,a,g,f){var h=b[a++];if(!v.test(h))return E(b,a,g);a<g&&" "===b[a]&&++a;if(a==g||":"!==b[a])return E(b,a,g);++a;a<g&&" "===b[a]&&++a;var d=x(b,a,g,C);if(0>d)d=~d;else{for(var e=[],l=0,w=a;w<d;++w)a=b[w]," "!==a&&(e[l++]=a);if(d<g){do{a=b[d];if(";"===a||"}"===a)break;l=0}while(++d<g);";"===a&&++d}l&&f.declaration&&f.declaration(h.toLowerCase(),e)}return d}fa=function(b,a){var e=U(b);
a.startStylesheet&&a.startStylesheet();for(var f=0,h=e.length;f<h;)f=" "===e[f]?f+1:f<h?"@"===e[f].charAt(0)?g(e,f,h,a,n):M(e,f,h,a):f;a.endStylesheet&&a.endStylesheet()};var v=/^-?[a-z]/i;ga=function(b,a){for(var g=U(b),f=0,h=g.length;f<h;)f=" "!==g[f]?e(g,f,h,a):f+1}})();"undefined"!==typeof window&&(window.parseCssStylesheet=fa,window.parseCssDeclarations=ga);var $={d:{NONE:0,URI:1,URI_FRAGMENT:11,SCRIPT:2,STYLE:3,HTML:12,ID:4,IDREF:5,IDREFS:6,GLOBAL_NAME:7,LOCAL_NAME:8,CLASSES:9,FRAME_TARGET:10,MEDIA_QUERY:13}};$.atype=$.d;
$.m={"*::class":9,"*::dir":0,"*::draggable":0,"*::hidden":0,"*::id":4,"*::inert":0,"*::itemprop":0,"*::itemref":6,"*::itemscope":0,"*::lang":0,"*::onblur":2,"*::onchange":2,"*::onclick":2,"*::ondblclick":2,"*::onerror":2,"*::onfocus":2,"*::onkeydown":2,"*::onkeypress":2,"*::onkeyup":2,"*::onload":2,"*::onmousedown":2,"*::onmousemove":2,"*::onmouseout":2,"*::onmouseover":2,"*::onmouseup":2,"*::onreset":2,"*::onscroll":2,"*::onselect":2,"*::onsubmit":2,"*::ontouchcancel":2,"*::ontouchend":2,"*::ontouchenter":2,
"*::ontouchleave":2,"*::ontouchmove":2,"*::ontouchstart":2,"*::onunload":2,"*::spellcheck":0,"*::style":3,"*::tabindex":0,"*::title":0,"*::translate":0,"a::accesskey":0,"a::coords":0,"a::href":1,"a::hreflang":0,"a::name":7,"a::onblur":2,"a::onfocus":2,"a::shape":0,"a::target":10,"a::type":0,"area::accesskey":0,"area::alt":0,"area::coords":0,"area::href":1,"area::nohref":0,"area::onblur":2,"area::onfocus":2,"area::shape":0,"area::target":10,"audio::controls":0,"audio::loop":0,"audio::mediagroup":5,
"audio::muted":0,"audio::preload":0,"audio::src":1,"bdo::dir":0,"blockquote::cite":1,"br::clear":0,"button::accesskey":0,"button::disabled":0,"button::name":8,"button::onblur":2,"button::onfocus":2,"button::type":0,"button::value":0,"canvas::height":0,"canvas::width":0,"caption::align":0,"col::align":0,"col::char":0,"col::charoff":0,"col::span":0,"col::valign":0,"col::width":0,"colgroup::align":0,"colgroup::char":0,"colgroup::charoff":0,"colgroup::span":0,"colgroup::valign":0,"colgroup::width":0,
"command::checked":0,"command::command":5,"command::disabled":0,"command::icon":1,"command::label":0,"command::radiogroup":0,"command::type":0,"data::value":0,"del::cite":1,"del::datetime":0,"details::open":0,"dir::compact":0,"div::align":0,"dl::compact":0,"fieldset::disabled":0,"font::color":0,"font::face":0,"font::size":0,"form::accept":0,"form::action":1,"form::autocomplete":0,"form::enctype":0,"form::method":0,"form::name":7,"form::novalidate":0,"form::onreset":2,"form::onsubmit":2,"form::target":10,
"h1::align":0,"h2::align":0,"h3::align":0,"h4::align":0,"h5::align":0,"h6::align":0,"hr::align":0,"hr::noshade":0,"hr::size":0,"hr::width":0,"iframe::align":0,"iframe::frameborder":0,"iframe::height":0,"iframe::marginheight":0,"iframe::marginwidth":0,"iframe::width":0,"img::align":0,"img::alt":0,"img::border":0,"img::height":0,"img::hspace":0,"img::ismap":0,"img::name":7,"img::src":1,"img::usemap":11,"img::vspace":0,"img::width":0,"input::accept":0,"input::accesskey":0,"input::align":0,"input::alt":0,
"input::autocomplete":0,"input::checked":0,"input::disabled":0,"input::inputmode":0,"input::ismap":0,"input::list":5,"input::max":0,"input::maxlength":0,"input::min":0,"input::multiple":0,"input::name":8,"input::onblur":2,"input::onchange":2,"input::onfocus":2,"input::onselect":2,"input::pattern":0,"input::placeholder":0,"input::readonly":0,"input::required":0,"input::size":0,"input::src":1,"input::step":0,"input::type":0,"input::usemap":11,"input::value":0,"ins::cite":1,"ins::datetime":0,"label::accesskey":0,
"label::for":5,"label::onblur":2,"label::onfocus":2,"legend::accesskey":0,"legend::align":0,"li::type":0,"li::value":0,"map::name":7,"menu::compact":0,"menu::label":0,"menu::type":0,"meter::high":0,"meter::low":0,"meter::max":0,"meter::min":0,"meter::value":0,"ol::compact":0,"ol::reversed":0,"ol::start":0,"ol::type":0,"optgroup::disabled":0,"optgroup::label":0,"option::disabled":0,"option::label":0,"option::selected":0,"option::value":0,"output::for":6,"output::name":8,"p::align":0,"pre::width":0,
"progress::max":0,"progress::min":0,"progress::value":0,"q::cite":1,"select::autocomplete":0,"select::disabled":0,"select::multiple":0,"select::name":8,"select::onblur":2,"select::onchange":2,"select::onfocus":2,"select::required":0,"select::size":0,"source::type":0,"table::align":0,"table::bgcolor":0,"table::border":0,"table::cellpadding":0,"table::cellspacing":0,"table::frame":0,"table::rules":0,"table::summary":0,"table::width":0,"tbody::align":0,"tbody::char":0,"tbody::charoff":0,"tbody::valign":0,
"td::abbr":0,"td::align":0,"td::axis":0,"td::bgcolor":0,"td::char":0,"td::charoff":0,"td::colspan":0,"td::headers":6,"td::height":0,"td::nowrap":0,"td::rowspan":0,"td::scope":0,"td::valign":0,"td::width":0,"textarea::accesskey":0,"textarea::autocomplete":0,"textarea::cols":0,"textarea::disabled":0,"textarea::inputmode":0,"textarea::name":8,"textarea::onblur":2,"textarea::onchange":2,"textarea::onfocus":2,"textarea::onselect":2,"textarea::placeholder":0,"textarea::readonly":0,"textarea::required":0,
"textarea::rows":0,"textarea::wrap":0,"tfoot::align":0,"tfoot::char":0,"tfoot::charoff":0,"tfoot::valign":0,"th::abbr":0,"th::align":0,"th::axis":0,"th::bgcolor":0,"th::char":0,"th::charoff":0,"th::colspan":0,"th::headers":6,"th::height":0,"th::nowrap":0,"th::rowspan":0,"th::scope":0,"th::valign":0,"th::width":0,"thead::align":0,"thead::char":0,"thead::charoff":0,"thead::valign":0,"tr::align":0,"tr::bgcolor":0,"tr::char":0,"tr::charoff":0,"tr::valign":0,"track::default":0,"track::kind":0,"track::label":0,
"track::srclang":0,"ul::compact":0,"ul::type":0,"video::controls":0,"video::height":0,"video::loop":0,"video::mediagroup":5,"video::muted":0,"video::poster":1,"video::preload":0,"video::src":1,"video::width":0};$.ATTRIBS=$.m;$.c={OPTIONAL_ENDTAG:1,EMPTY:2,CDATA:4,RCDATA:8,UNSAFE:16,FOLDABLE:32,SCRIPT:64,STYLE:128,VIRTUALIZED:256};$.eflags=$.c;
$.f={a:0,abbr:0,acronym:0,address:0,applet:272,area:2,article:0,aside:0,audio:0,b:0,base:274,basefont:274,bdi:0,bdo:0,big:0,blockquote:0,body:305,br:2,button:0,canvas:0,caption:0,center:0,cite:0,code:0,col:2,colgroup:1,command:2,data:0,datalist:0,dd:1,del:0,details:0,dfn:0,dialog:272,dir:0,div:0,dl:0,dt:1,em:0,fieldset:0,figcaption:0,figure:0,font:0,footer:0,form:0,frame:274,frameset:272,h1:0,h2:0,h3:0,h4:0,h5:0,h6:0,head:305,header:0,hgroup:0,hr:2,html:305,i:0,iframe:4,img:2,input:2,ins:0,isindex:274,
kbd:0,keygen:274,label:0,legend:0,li:1,link:274,map:0,mark:0,menu:0,meta:274,meter:0,nav:0,nobr:0,noembed:276,noframes:276,noscript:276,object:272,ol:0,optgroup:0,option:1,output:0,p:1,param:274,pre:0,progress:0,q:0,s:0,samp:0,script:84,section:0,select:0,small:0,source:2,span:0,strike:0,strong:0,style:148,sub:0,summary:0,sup:0,table:0,tbody:1,td:1,textarea:8,tfoot:1,th:1,thead:1,time:0,title:280,tr:1,track:2,tt:0,u:0,ul:0,"var":0,video:0,wbr:2};$.ELEMENTS=$.f;
$.Q={a:"HTMLAnchorElement",abbr:"HTMLElement",acronym:"HTMLElement",address:"HTMLElement",applet:"HTMLAppletElement",area:"HTMLAreaElement",article:"HTMLElement",aside:"HTMLElement",audio:"HTMLAudioElement",b:"HTMLElement",base:"HTMLBaseElement",basefont:"HTMLBaseFontElement",bdi:"HTMLElement",bdo:"HTMLElement",big:"HTMLElement",blockquote:"HTMLQuoteElement",body:"HTMLBodyElement",br:"HTMLBRElement",button:"HTMLButtonElement",canvas:"HTMLCanvasElement",caption:"HTMLTableCaptionElement",center:"HTMLElement",
cite:"HTMLElement",code:"HTMLElement",col:"HTMLTableColElement",colgroup:"HTMLTableColElement",command:"HTMLCommandElement",data:"HTMLElement",datalist:"HTMLDataListElement",dd:"HTMLElement",del:"HTMLModElement",details:"HTMLDetailsElement",dfn:"HTMLElement",dialog:"HTMLDialogElement",dir:"HTMLDirectoryElement",div:"HTMLDivElement",dl:"HTMLDListElement",dt:"HTMLElement",em:"HTMLElement",fieldset:"HTMLFieldSetElement",figcaption:"HTMLElement",figure:"HTMLElement",font:"HTMLFontElement",footer:"HTMLElement",
form:"HTMLFormElement",frame:"HTMLFrameElement",frameset:"HTMLFrameSetElement",h1:"HTMLHeadingElement",h2:"HTMLHeadingElement",h3:"HTMLHeadingElement",h4:"HTMLHeadingElement",h5:"HTMLHeadingElement",h6:"HTMLHeadingElement",head:"HTMLHeadElement",header:"HTMLElement",hgroup:"HTMLElement",hr:"HTMLHRElement",html:"HTMLHtmlElement",i:"HTMLElement",iframe:"HTMLIFrameElement",img:"HTMLImageElement",input:"HTMLInputElement",ins:"HTMLModElement",isindex:"HTMLUnknownElement",kbd:"HTMLElement",keygen:"HTMLKeygenElement",
label:"HTMLLabelElement",legend:"HTMLLegendElement",li:"HTMLLIElement",link:"HTMLLinkElement",map:"HTMLMapElement",mark:"HTMLElement",menu:"HTMLMenuElement",meta:"HTMLMetaElement",meter:"HTMLMeterElement",nav:"HTMLElement",nobr:"HTMLElement",noembed:"HTMLElement",noframes:"HTMLElement",noscript:"HTMLElement",object:"HTMLObjectElement",ol:"HTMLOListElement",optgroup:"HTMLOptGroupElement",option:"HTMLOptionElement",output:"HTMLOutputElement",p:"HTMLParagraphElement",param:"HTMLParamElement",pre:"HTMLPreElement",
progress:"HTMLProgressElement",q:"HTMLQuoteElement",s:"HTMLElement",samp:"HTMLElement",script:"HTMLScriptElement",section:"HTMLElement",select:"HTMLSelectElement",small:"HTMLElement",source:"HTMLSourceElement",span:"HTMLSpanElement",strike:"HTMLElement",strong:"HTMLElement",style:"HTMLStyleElement",sub:"HTMLElement",summary:"HTMLElement",sup:"HTMLElement",table:"HTMLTableElement",tbody:"HTMLTableSectionElement",td:"HTMLTableDataCellElement",textarea:"HTMLTextAreaElement",tfoot:"HTMLTableSectionElement",
th:"HTMLTableHeaderCellElement",thead:"HTMLTableSectionElement",time:"HTMLTimeElement",title:"HTMLTitleElement",tr:"HTMLTableRowElement",track:"HTMLTrackElement",tt:"HTMLElement",u:"HTMLElement",ul:"HTMLUListElement","var":"HTMLElement",video:"HTMLVideoElement",wbr:"HTMLElement"};$.ELEMENT_DOM_INTERFACES=$.Q;$.P={NOT_LOADED:0,SAME_DOCUMENT:1,NEW_DOCUMENT:2};$.ueffects=$.P;
$.J={"a::href":2,"area::href":2,"audio::src":1,"blockquote::cite":0,"command::icon":1,"del::cite":0,"form::action":2,"img::src":1,"input::src":1,"ins::cite":0,"q::cite":0,"video::poster":1,"video::src":1};$.URIEFFECTS=$.J;$.M={UNSANDBOXED:2,SANDBOXED:1,DATA:0};$.ltypes=$.M;$.I={"a::href":2,"area::href":2,"audio::src":2,"blockquote::cite":2,"command::icon":1,"del::cite":2,"form::action":2,"img::src":1,"input::src":1,"ins::cite":2,"q::cite":2,"video::poster":1,"video::src":2};$.LOADERTYPES=$.I;
"undefined"!==typeof window&&(window.html4=$);var ha=function(g){function w(a){if(i.hasOwnProperty(a))return i[a];var b=a.match(q);return b?String.fromCharCode(parseInt(b[1],10)):(b=a.match(B))?String.fromCharCode(parseInt(b[1],16)):r&&F.test(a)?(r.innerHTML="&"+a+";",b=r.textContent,i[a]=b):"&"+a+";"}function M(a,b){return w(b)}function x(a){return a.replace(j,M)}function E(a){return(""+a).replace(R,"&amp;").replace(P,"&lt;").replace(S,"&gt;").replace(D,"&#34;")}function e(a){return a.replace(ia,"&amp;$1").replace(P,"&lt;").replace(S,"&gt;")}
function v(b){var d={z:b.z||b.cdata,A:b.A||b.comment,B:b.B||b.endDoc,t:b.t||b.endTag,e:b.e||b.pcdata,F:b.F||b.rcdata,H:b.H||b.startDoc,w:b.w||b.startTag};return function(b,g){var f;var H=/(<\/|<\!--|<[!?]|[&<>])/g;f=b+"";if(G)f=f.split(H);else{for(var e=[],h=0,j;(j=H.exec(f))!==s;)e.push(f.substring(h,j.index)),e.push(j[0]),h=j.index+j[0].length;e.push(f.substring(h));f=e}a(d,f,0,{r:C,C:C},g)}}function b(b,d,g,f,t){return function(){a(b,d,g,f,t)}}function a(a,d,p,e,t){try{a.H&&0==p&&a.H(t);for(var h,
z,j,i=d.length;p<i;){var o=d[p++],l=d[p];switch(o){case "&":I.test(l)?(a.e&&a.e("&"+l,t,A,b(a,d,p,e,t)),p++):a.e&&a.e("&amp;",t,A,b(a,d,p,e,t));break;case "</":if(h=/^([-\w:]+)[^\'\"]*/.exec(l))if(h[0].length===l.length&&">"===d[p+1])p+=2,j=h[1].toLowerCase(),a.t&&a.t(j,t,A,b(a,d,p,e,t));else{var m=d,q=p,r=a,u=t,v=A,y=e,w=f(m,q);w?(r.t&&r.t(w.name,u,v,b(r,m,q,y,u)),p=w.next):p=m.length}else a.e&&a.e("&lt;/",t,A,b(a,d,p,e,t));break;case "<":if(h=/^([-\w:]+)\s*\/?/.exec(l))if(h[0].length===l.length&&
">"===d[p+1]){p+=2;j=h[1].toLowerCase();a.w&&a.w(j,[],t,A,b(a,d,p,e,t));var B=g.f[j];B&K&&(p=k(d,{name:j,next:p,c:B},a,t,A,e))}else{var m=d,q=a,r=t,u=A,v=e,x=f(m,p);x?(q.w&&q.w(x.name,x.R,r,u,b(q,m,x.next,v,r)),p=x.c&K?k(m,x,q,r,u,v):x.next):p=m.length}else a.e&&a.e("&lt;",t,A,b(a,d,p,e,t));break;case "<\!--":if(!e.C){for(z=p+1;z<i&&!(">"===d[z]&&/--$/.test(d[z-1]));z++);if(z<i){if(a.A){var D=d.slice(p,z).join("");a.A(D.substr(0,D.length-2),t,A,b(a,d,z+1,e,t))}p=z+1}else e.C=n}e.C&&a.e&&a.e("&lt;!--",
t,A,b(a,d,p,e,t));break;case "<!":if(/^\w/.test(l)){if(!e.r){for(z=p+1;z<i&&">"!==d[z];z++);z<i?p=z+1:e.r=n}e.r&&a.e&&a.e("&lt;!",t,A,b(a,d,p,e,t))}else a.e&&a.e("&lt;!",t,A,b(a,d,p,e,t));break;case "<?":if(!e.r){for(z=p+1;z<i&&">"!==d[z];z++);z<i?p=z+1:e.r=n}e.r&&a.e&&a.e("&lt;?",t,A,b(a,d,p,e,t));break;case ">":a.e&&a.e("&gt;",t,A,b(a,d,p,e,t));break;case "":break;default:a.e&&a.e(o,t,A,b(a,d,p,e,t))}}a.B&&a.B(t)}catch(E){if(E!==A)throw E;}}function k(a,d,f,h,t,j){var z=a.length;T.hasOwnProperty(d.name)||
(T[d.name]=RegExp("^"+d.name+"(?:[\\s\\/]|$)","i"));for(var i=T[d.name],k=d.next,l=d.next+1;l<z&&!("</"===a[l-1]&&i.test(a[l]));l++);l<z&&(l-=1);z=a.slice(k,l).join("");if(d.c&g.c.CDATA)f.z&&f.z(z,h,t,b(f,a,l,j,h));else if(d.c&g.c.RCDATA)f.F&&f.F(e(z),h,t,b(f,a,l,j,h));else throw Error("bug");return l}function f(a,b){var d=/^([-\w:]+)/.exec(a[b]),f={};f.name=d[1].toLowerCase();f.c=g.f[f.name];for(var e=a[b].substr(d[0].length),h=b+1,j=a.length;h<j&&">"!==a[h];h++)e+=a[h];if(!(j<=h)){for(var l=[];""!==
e;)if(d=ja.exec(e))if(d[4]&&!d[5]||d[6]&&!d[7]){for(var d=d[4]||d[6],i=C,e=[e,a[h++]];h<j;h++){if(i){if(">"===a[h])break}else 0<=a[h].indexOf(d)&&(i=n);e.push(a[h])}if(j<=h)break;e=e.join("")}else{var i=d[1].toLowerCase(),k;if(d[2]){k=d[3];var m=k.charCodeAt(0);if(34===m||39===m)k=k.substr(1,k.length-2);k=x(k.replace(o,""))}else k="";l.push(i,k);e=e.substr(d[0].length)}else e=e.replace(/^[\s\S][^a-z\s]*/,"");f.R=l;f.next=h+1;return f}}function h(a){function b(a,d){f||d.push(a)}var d,f;return v({startDoc:function(){d=
[];f=C},startTag:function(b,e,h){if(!f&&g.f.hasOwnProperty(b)){var j=g.f[b];if(!(j&g.c.FOLDABLE)){var k=a(b,e);if(k){if("object"!==typeof k)throw Error("tagPolicy did not return object (old API?)");if("attribs"in k)e=k.attribs;else throw Error("tagPolicy gave no attribs");var i;"tagName"in k?(i=k.tagName,k=g.f[i]):(i=b,k=j);if(j&g.c.OPTIONAL_ENDTAG){var l=d[d.length-1];l&&l.D===b&&(l.v!==i||b!==i)&&h.push("</",l.v,">")}j&g.c.EMPTY||d.push({D:b,v:i});h.push("<",i);b=0;for(l=e.length;b<l;b+=2){var m=
e[b],o=e[b+1];o!==s&&o!==c&&h.push(" ",m,'="',E(o),'"')}h.push(">");j&g.c.EMPTY&&!(k&g.c.EMPTY)&&h.push("</",i,">")}else f=!(j&g.c.EMPTY)}}},endTag:function(a,b){if(f)f=C;else if(g.f.hasOwnProperty(a)){var e=g.f[a];if(!(e&(g.c.EMPTY|g.c.FOLDABLE))){if(e&g.c.OPTIONAL_ENDTAG)for(e=d.length;0<=--e;){var h=d[e].D;if(h===a)break;if(!(g.f[h]&g.c.OPTIONAL_ENDTAG))return}else for(e=d.length;0<=--e&&d[e].D!==a;);if(!(0>e)){for(h=d.length;--h>e;){var j=d[h].v;g.f[j]&g.c.OPTIONAL_ENDTAG||b.push("</",j,">")}e<
d.length&&(a=d[e].v);d.length=e;b.push("</",a,">")}}}},pcdata:b,rcdata:b,cdata:b,endDoc:function(a){for(;d.length;d.length--)a.push("</",d[d.length-1].v,">")}})}function d(a,b,d,f,e){if(!e)return s;try{var g=Y.parse(""+a);if(g&&(!g.K()||ka.test(g.W()))){var h=e(g,b,d,f);return h?h.toString():s}}catch(j){}return s}function y(a,b,d,f,e){d||a(b+" removed",{S:"removed",tagName:b});if(f!==e){var g="changed";f&&!e?g="removed":!f&&e&&(g="added");a(b+"."+d+" "+g,{S:g,tagName:b,la:d,oldValue:f,newValue:e})}}
function l(a,b,d){b=b+"::"+d;if(a.hasOwnProperty(b))return a[b];b="*::"+d;if(a.hasOwnProperty(b))return a[b]}function V(a,b,f,e,h){for(var j=0;j<b.length;j+=2){var k=b[j],i=b[j+1],m=i,o=s,q;if((q=a+"::"+k,g.m.hasOwnProperty(q))||(q="*::"+k,g.m.hasOwnProperty(q)))o=g.m[q];if(o!==s)switch(o){case g.d.NONE:break;case g.d.SCRIPT:i=s;h&&y(h,a,k,m,i);break;case g.d.STYLE:if("undefined"===typeof N){i=s;h&&y(h,a,k,m,i);break}var r=[];N(i,{declaration:function(a,b){var e=a.toLowerCase();u(e,b,f?function(a){return d(a,
g.P.ja,g.M.ka,{TYPE:"CSS",CSS_PROP:e},f)}:s);b.length&&r.push(e+": "+b.join(" "))}});i=0<r.length?r.join(" ; "):s;h&&y(h,a,k,m,i);break;case g.d.ID:case g.d.IDREF:case g.d.IDREFS:case g.d.GLOBAL_NAME:case g.d.LOCAL_NAME:case g.d.CLASSES:i=e?e(i):i;h&&y(h,a,k,m,i);break;case g.d.URI:i=d(i,l(g.J,a,k),l(g.I,a,k),{TYPE:"MARKUP",XML_ATTR:k,XML_TAG:a},f);h&&y(h,a,k,m,i);break;case g.d.URI_FRAGMENT:i&&"#"===i.charAt(0)?(i=i.substring(1),i=e?e(i):i,i!==s&&i!==c&&(i="#"+i)):i=s;h&&y(h,a,k,m,i);break;default:i=
s,h&&y(h,a,k,m,i)}else i=s,h&&y(h,a,k,m,i);b[j+1]=i}return b}function ea(a,b,d){return function(e,f){if(g.f[e]&g.c.UNSAFE)d&&y(d,e,c,c,c);else return{attribs:V(e,f,a,b,d)}}}function Q(a,b){var d=[];h(b)(a,d);return d.join("")}var N,u;"undefined"!==typeof window&&(N=window.parseCssDeclarations,u=window.sanitizeCssProperty);var i={lt:"<",LT:"<",gt:">",GT:">",amp:"&",AMP:"&",quot:'"',apos:"'",nbsp:"\u00a0"},q=/^#(\d+)$/,B=/^#x([0-9A-Fa-f]+)$/,F=/^[A-Za-z][A-za-z0-9]+$/,r="undefined"!==typeof window&&
window.document?window.document.createElement("textarea"):s,o=/\0/g,j=/&(#[0-9]+|#[xX][0-9A-Fa-f]+|\w+);/g,I=/^(#[0-9]+|#[xX][0-9A-Fa-f]+|\w+);/,R=/&/g,ia=/&([^a-z#]|#(?:[^0-9x]|x(?:[^0-9a-f]|$)|$)|$)/gi,P=/[<]/g,S=/>/g,D=/\"/g,ja=/^\s*([-.:\w]+)(?:\s*(=)\s*((")[^"]*("|$)|(')[^']*('|$)|(?=[a-z][-\w]*\s*=)|[^"'\s]*))?/i,G=3==="a,b".split(/(,)/).length,K=g.c.CDATA|g.c.RCDATA,A={},T={},ka=/^(?:https?|mailto)$/i,m={};m.pa=m.escapeAttrib=E;m.ra=m.makeHtmlSanitizer=h;m.sa=m.makeSaxParser=v;m.ta=m.makeTagPolicy=
ea;m.wa=m.normalizeRCData=e;m.xa=m.sanitize=function(a,b,d,e){return Q(a,ea(b,d,e))};m.ya=m.sanitizeAttribs=V;m.za=m.sanitizeWithPolicy=Q;m.Ba=m.unescapeEntities=x;return m}($),la=ha.sanitize;"undefined"!==typeof window&&(window.html=ha,window.html_sanitize=la);})();
;/**/
/**
 * @file views_load_more.js
 *
 * Handles the AJAX pager for the view_load_more plugin.
 */
(function ($) {

  /**
   * Provide a series of commands that the server can request the client perform.
   */
  Drupal.ajax.prototype.commands.viewsLoadMoreAppend = function (ajax, response, status) {
    // Get information from the response. If it is not there, default to
    // our presets.
    var wrapper = response.selector ? $(response.selector) : $(ajax.wrapper);
    var method = response.method || ajax.method;
    var targetList = response.targetList || '';
    var effect = ajax.getEffect(response);
    var pager_selector = response.options.pager_selector ? response.options.pager_selector : '.pager-load-more';

    // We don't know what response.data contains: it might be a string of text
    // without HTML, so don't rely on jQuery correctly iterpreting
    // $(response.data) as new HTML rather than a CSS selector. Also, if
    // response.data contains top-level text nodes, they get lost with either
    // $(response.data) or $('<div></div>').replaceWith(response.data).
    var new_content_wrapped = $('<div></div>').html(response.data);
    var new_content = new_content_wrapped.contents();

    // For legacy reasons, the effects processing code assumes that new_content
    // consists of a single top-level element. Also, it has not been
    // sufficiently tested whether attachBehaviors() can be successfully called
    // with a context object that includes top-level text nodes. However, to
    // give developers full control of the HTML appearing in the page, and to
    // enable Ajax content to be inserted in places where DIV elements are not
    // allowed (e.g., within TABLE, TR, and SPAN parents), we check if the new
    // content satisfies the requirement of a single top-level element, and
    // only use the container DIV created above when it doesn't. For more
    // information, please see http://drupal.org/node/736066.
    if (new_content.length != 1 || new_content.get(0).nodeType != 1) {
      new_content = new_content_wrapped;
    }
    // If removing content from the wrapper, detach behaviors first.
    var settings = response.settings || ajax.settings || Drupal.settings;
    Drupal.detachBehaviors(wrapper, settings);
    if ($.waypoints != undefined) {
      $.waypoints('refresh');
    }

    // Set up our default query options. This is for advance users that might
    // change there views layout classes. This allows them to write there own
    // jquery selector to replace the content with.
    // Provide sensible defaults for unordered list, ordered list and table
    // view styles.
    var content_query = targetList && !response.options.content ? '> .view-content ' + targetList : response.options.content || '> .view-content';

    // If we're using any effects. Hide the new content before adding it to the DOM.
    if (effect.showEffect != 'show') {
      new_content.find(content_query).children().hide();
    }

    // Update the pager
    // Find both for the wrapper as the newly loaded content the direct child
    // .item-list in case of nested pagers
    wrapper.find(pager_selector).replaceWith(new_content.find(pager_selector));

    // Add the new content to the page.
    wrapper.find(content_query)[method](new_content.find(content_query).children());

    // Re-class the loaded content.
    // @todo this is faulty in many ways.  first of which is that user may have configured view to not have these classes at all.
    wrapper.find(content_query).children()
      .removeClass('views-row-first views-row-last views-row-odd views-row-even')
      .filter(':first')
        .addClass('views-row-first')
        .end()
      .filter(':last')
        .addClass('views-row-last')
        .end()
      .filter(':even')
        .addClass('views-row-odd')
        .end()
      .filter(':odd')
        .addClass('views-row-even')
        .end();

    if (effect.showEffect != 'show') {
      wrapper.find(content_query).children(':not(:visible)')[effect.showEffect](effect.showSpeed);
    }

    // Additional processing over new content
    wrapper.trigger('views_load_more.new_content', new_content.clone());

    // Attach all JavaScript behaviors to the new content
    // Remove the Jquery once Class, TODO: There needs to be a better
    // way of doing this, look at .removeOnce() :-/
    var classes = wrapper.attr('class');
    var onceClass = classes.match(/jquery-once-[0-9]*-[a-z]*/);
    wrapper.removeClass(onceClass[0]);
    settings = response.settings || ajax.settings || Drupal.settings;
    Drupal.attachBehaviors(wrapper, settings);
  };

  /**
   * Attaches the AJAX behavior to Views Load More waypoint support.
   */
  Drupal.behaviors.ViewsLoadMore = {
    attach: function (context, settings) {
      var default_opts = {
          offset: '100%'
        };

      if (settings && settings.viewsLoadMore && settings.views && settings.views.ajaxViews) {
        $.each(settings.viewsLoadMore, function(i, setting) {
          var view = '.view-id-' + setting.view_name + '.view-display-id-' + setting.view_display_id + ' .pager-next a',
            opts = {};

          $.extend(opts, default_opts, settings.viewsLoadMore[i].opts);

          $(view).waypoint('destroy');
          $(view).waypoint(function(event, direction) {
            $(view).click();
          }, opts);
        });
      }
    },
    detach: function (context, settings, trigger) {
      if (settings && settings.viewsLoadMore && settings.views && settings.views.ajaxViews) {
        $.each(settings.viewsLoadMore, function(i, setting) {
          var view = '.view-id-' + setting.view_name + '.view-display-id-' + setting.view_display_id;
          if ($(context).is(view)) {
            $('.pager-next a', view).waypoint('destroy');
          }
          else {
            $(view, context).waypoint('destroy');
          }
        });
      }
    }
  };
})(jQuery);
;/**/
(function(e){Drupal.Views={},Drupal.behaviors.viewsTabs={attach:function(){e.viewsUi&&e.viewsUi.tabs&&e("#views-tabset").once("views-processed").viewsTabs({selectedClass:"active"}),e("a.views-remove-link").once("views-processed").click(function(t){var i=e(this).attr("id").replace("views-remove-link-","");e("#views-row-"+i).hide(),e("#views-removed-"+i).attr("checked",!0),t.preventDefault()}),e("a.display-remove-link").addClass("display-processed").click(function(){var t=e(this).attr("id").replace("display-remove-link-","");return e("#display-row-"+t).hide(),e("#display-removed-"+t).attr("checked",!0),!1})}},Drupal.Views.parseQueryString=function(e){var t={},i=e.indexOf("?");-1!=i&&(e=e.substring(i+1));var n=e.split("&");for(var s in n)if("string"==typeof n[s]){var o=n[s].split("=");"q"!=o[0]&&o[1]&&(t[decodeURIComponent(o[0].replace(/\+/g," "))]=decodeURIComponent(o[1].replace(/\+/g," ")))}return t},Drupal.Views.parseViewArgs=function(e,t){var i={},n=Drupal.Views.getPath(e);if(t&&n.substring(0,t.length+1)==t+"/"){var s=decodeURIComponent(n.substring(t.length+1,n.length));i.view_args=s,i.view_path=n}return i},Drupal.Views.pathPortion=function(e){var t=window.location.protocol;return e.substring(0,t.length)==t&&(e=e.substring(e.indexOf("/",t.length+2))),e},Drupal.Views.getPath=function(e){e=Drupal.Views.pathPortion(e),e=e.substring(Drupal.settings.basePath.length,e.length),"?q="==e.substring(0,3)&&(e=e.substring(3,e.length));var t=["#","?","&"];for(i in t)e.indexOf(t[i])>-1&&(e=e.substr(0,e.indexOf(t[i])));return e}})(jQuery);;/**/
!function(e){Drupal.behaviors.ViewsAjaxView={},Drupal.behaviors.ViewsAjaxView.attach=function(){Drupal.settings&&Drupal.settings.views&&Drupal.settings.views.ajaxViews&&e.each(Drupal.settings.views.ajaxViews,function(e,t){Drupal.views.instances[e]=new Drupal.views.ajaxView(t)})},Drupal.views={},Drupal.views.instances={},Drupal.views.ajaxView=function(t){var i=".view-dom-id-"+t.view_dom_id;this.$view=e(i);var s=Drupal.settings.views.ajax_path;-1!=s.constructor.toString().indexOf("Array")&&(s=s[0]);var a=window.location.search||"";if(""!==a){var a=a.slice(1).replace(/q=[^&]+&?|&?render=[^&]+/,"");""!==a&&(a=(/\?/.test(s)?"&":"?")+a)}this.element_settings={url:s+a,submit:t,setClick:!0,event:"click",selector:i,progress:{type:"throbber"}},this.settings=t,this.$exposed_form=this.$view.children(".view-filters").children("form"),this.$exposed_form.once(jQuery.proxy(this.attachExposedFormAjax,this)),this.$view.filter(jQuery.proxy(this.filterNestedViews,this)).once(jQuery.proxy(this.attachPagerAjax,this));var r=this.element_settings;r.event="RefreshView",this.refreshViewAjax=new Drupal.ajax(this.selector,this.$view,r)},Drupal.views.ajaxView.prototype.attachExposedFormAjax=function(){var t=e("input[type=submit], button[type=submit], input[type=image]",this.$exposed_form);t=t[0],this.exposedFormAjax=new Drupal.ajax(e(t).attr("id"),t,this.element_settings)},Drupal.views.ajaxView.prototype.filterNestedViews=function(){return!this.$view.parents(".view").size()},Drupal.views.ajaxView.prototype.attachPagerAjax=function(){this.$view.find("ul.pager > li > a, th.views-field a, .attachment .views-summary a").each(jQuery.proxy(this.attachPagerLinkAjax,this))},Drupal.views.ajaxView.prototype.attachPagerLinkAjax=function(t,i){var s=e(i),a={},r=s.attr("href");e.extend(a,this.settings,Drupal.Views.parseQueryString(r),Drupal.Views.parseViewArgs(r,this.settings.view_base_path)),e.extend(a,Drupal.Views.parseViewArgs(r,this.settings.view_base_path)),this.element_settings.submit=a,this.pagerAjax=new Drupal.ajax(!1,s,this.element_settings)},Drupal.ajax.prototype.commands.viewsScrollTop=function(t,i){for(var s=e(i.selector).offset(),a=i.selector;0==e(a).scrollTop()&&e(a).parent();)a=e(a).parent();s.top-10<e(a).scrollTop()&&e(a).animate({scrollTop:s.top-10},500)}}(jQuery);;/**/
/**
 * @file
 * Monster Navigation javascript handlers
 */

(function ($) {
  /**
   * Loads dynamic links per user settings.
   */
  Drupal.behaviors.wfm_monster_nav = {
    attach: function(context, settings) {
      $(window).load(function(){
        var links_container = $('div.main-links-container', context);
        //console.log('wfm_monster_nav.js - Drupal.behaviors.wfm_monster_nav: settings.wfm_monster_nav = ');
        //console.debug(settings.wfm_monster_nav);
        if (links_container.length) {
          //if (typeof settings.wfm_monster_nav == 'object') {
          if (typeof Drupal.settings.WholeFoods == 'object') {
          
            //var dynamic_links_url = settings.basePath + 'wfm-monster-nav/load-dynamic-links/' +
                //settings.wfm_monster_nav.uid + '/' + settings.wfm_monster_nav.store_nid;
          
            if (Drupal.settings.WholeFoods.user.drupalUID) {
              var userid = Drupal.settings.WholeFoods.user.drupalUID;
            } else {
              var userid = '0';
            }
            if (Drupal.settings.WholeFoods.localStore) {
            var dynamic_links_url = settings.basePath + 'wfm-monster-nav/load-dynamic-links/' + userid + '/' + 
              Drupal.settings.WholeFoods.localStore;
            } else {
              /* TODO this is default node id of US.  Needs to work internationally */
              var dynamic_links_url = settings.basePath + 'wfm-monster-nav/load-dynamic-links/' + userid + '/129556';
            }
            //console.log('wfm_monster_nav.js - Drupal.behaviors.wfm_monster_nav: dynamic_links_url = ' + dynamic_links_url);
            $.ajax({
              url: dynamic_links_url,
              dataType: 'json',
              error: function() {
             },
              success: function (data) {
                // Add loaded items
                for (var selector in data) {
                  var dynamic_link = $('div.main-links-container a.ajax-load[rel="' + selector + '"] + div.sub-menu-drop-container', context);
                  if (dynamic_link.length) {
                    if (data[selector].length) {
                      dynamic_link.html(data[selector]);
                    }
                    else {
                      // Remove links block if no items loaded
                      dynamic_link.parent('li').remove();
                    }
                  }
                }

                // Execute behaviors
                Drupal.attachBehaviors(links_container);
              }
            });
          }
        }
      });//document.ready
    }
  }
})(jQuery);
;/**/

/**
 *  @file
 *  Attach Media WYSIWYG behaviors.
 */

(function ($) {

Drupal.media = Drupal.media || {};

// Define the behavior.
if (typeof Drupal.wysiwyg != 'undefined') {
Drupal.wysiwyg.plugins.media = {

  /**
   * Initializes the tag map.
   */
  initializeTagMap: function () {
    if (typeof Drupal.settings.tagmap == 'undefined') {
      Drupal.settings.tagmap = { };
    }
  },
  /**
   * Execute the button.
   * @TODO: Debug calls from this are never called. What's its function?
   */
  invoke: function (data, settings, instanceId) {
    if (data.format == 'html') {
      Drupal.media.popups.mediaBrowser(function (mediaFiles) {
        Drupal.wysiwyg.plugins.media.mediaBrowserOnSelect(mediaFiles, instanceId);
      }, settings['global']);
    }
  },

  /**
   * Respond to the mediaBrowser's onSelect event.
   * @TODO: Debug calls from this are never called. What's its function?
   */
  mediaBrowserOnSelect: function (mediaFiles, instanceId) {
    var mediaFile = mediaFiles[0];
    var options = {};
    Drupal.media.popups.mediaStyleSelector(mediaFile, function (formattedMedia) {
      Drupal.wysiwyg.plugins.media.insertMediaFile(mediaFile, formattedMedia.type, formattedMedia.html, formattedMedia.options, Drupal.wysiwyg.instances[instanceId]);
    }, options);

    return;
  },

  insertMediaFile: function (mediaFile, viewMode, formattedMedia, options, wysiwygInstance) {
    if(!mediaFile.filemime.match(/image/)){
      instance = wysiwygInstance.field;
      var currentEditor = CKEDITOR['instances'][instance];
      var selectedObj= currentEditor.getSelection();
      var text = '';

			if (typeof selectedObj != 'undefined' && selectedObj != null) { 
        if (CKEDITOR.env.ie) {
          selectedObj.unlock(true);
          text = selectedObj.getNative().createRange().text;
        } else {
          text = selectedObj.getNative();
        }
				text = text.toString();
			}


      if(!text.length > 0){
        text = mediaFile.filename;
      }

      var url = mediaFile.url;
      var exploded = url.split('/');
      exploded.shift();
      exploded.shift();
      exploded.shift();
      var url = '/' + exploded.join('/');

      url = url.replace('http://', '');
      url = url.replace('https://', '');
      url = url.replace(document.domain, '');
      var toInsert = '<a href="' + url + '">' + text + '</a>';
    }else{
      this.initializeTagMap();
      // @TODO: the folks @ ckeditor have told us that there is no way
      // to reliably add wrapper divs via normal HTML.
      // There is some method of adding a "fake element"
      // But until then, we're just going to embed to img.
      // This is pretty hacked for now.
      //
      var imgElement = $(this.stripDivs(formattedMedia));
      this.addImageAttributes(imgElement, mediaFile.fid, viewMode, options);

      var toInsert = this.outerHTML(imgElement);
      // Create an inline tag
      var inlineTag = Drupal.wysiwyg.plugins.media.createTag(imgElement);
      // Add it to the tag map in case the user switches input formats
      Drupal.settings.tagmap[inlineTag] = toInsert;
    }
    wysiwygInstance.insert(toInsert);
  },

  /**
   * Gets the HTML content of an element
   *
   * @param jQuery element
   */
  outerHTML: function (element) {
    return $('<div>').append( element.eq(0).clone() ).html();
  },

  addImageAttributes: function (imgElement, fid, view_mode, additional) {
    //    imgElement.attr('fid', fid);
    //    imgElement.attr('view_mode', view_mode);
    // Class so we can find this image later.
    imgElement.addClass('media-image');
    this.forceAttributesIntoClass(imgElement, fid, view_mode, additional);
    if (additional) {
      for (k in additional) {
        if (additional.hasOwnProperty(k)) {
          if (k === 'attr') {
            imgElement.attr(k, additional[k]);
          }
        }
      }
    }
  },

  /**
   * Due to problems handling wrapping divs in ckeditor, this is needed.
   *
   * Going forward, if we don't care about supporting other editors
   * we can use the fakeobjects plugin to ckeditor to provide cleaner
   * transparency between what Drupal will output <div class="field..."><img></div>
   * instead of just <img>, for now though, we're going to remove all the stuff surrounding the images.
   *
   * @param String formattedMedia
   *  Element containing the image
   *
   * @return HTML of <img> tag inside formattedMedia
   */
  stripDivs: function (formattedMedia) {
    // Check to see if the image tag has divs to strip
    var stripped = null;
    if ($(formattedMedia).is('img')) {
      stripped = this.outerHTML($(formattedMedia));
    } else {
      stripped = this.outerHTML($('img', $(formattedMedia)));
    }
    // This will fail if we pass the img tag without anything wrapping it, like we do when re-enabling WYSIWYG
    return stripped;
  },

  /**
   * Attach function, called when a rich text editor loads.
   * This finds all [[tags]] and replaces them with the html
   * that needs to show in the editor.
   *
   */
  attach: function (content, settings, instanceId) {
    var matches = content.match(/\[\[.*?\]\]/g);
    this.initializeTagMap();
    var tagmap = Drupal.settings.tagmap;
    if (matches) {
      var inlineTag = "";
      for (i = 0; i < matches.length; i++) {
        inlineTag = matches[i];
        if (tagmap[inlineTag]) {
          // This probably needs some work...
          // We need to somehow get the fid propogated here.
          // We really want to
          var tagContent = tagmap[inlineTag];
          var mediaMarkup = this.stripDivs(tagContent); // THis is <div>..<img>

          var _tag = inlineTag;
          _tag = _tag.replace('[[','');
          _tag = _tag.replace(']]','');
          try {
            mediaObj = JSON.parse(_tag);
          }
          catch(err) {
            mediaObj = null;
          }
          if(mediaObj) {
            var imgElement = $(mediaMarkup);
            this.addImageAttributes(imgElement, mediaObj.fid, mediaObj.view_mode);
            var toInsert = this.outerHTML(imgElement);
            content = content.replace(inlineTag, toInsert);
          }
        }
        else {
          debug.debug("Could not find content for " + inlineTag);
        }
      }
    }
    return content;
  },

  /**
   * Detach function, called when a rich text editor detaches
   */
  detach: function (content, settings, instanceId) {
    // Replace all Media placeholder images with the appropriate inline json
    // string. Using a regular expression instead of jQuery manipulation to
    // prevent <script> tags from being displaced.
    // @see http://drupal.org/node/1280758.
    if (matches = content.match(/<img[^>]+class=([\'"])media-image[^>]*>/gi)) {
      for (var i = 0; i < matches.length; i++) {
        var imageTag = matches[i];
        var inlineTag = Drupal.wysiwyg.plugins.media.createTag($(imageTag));
        Drupal.settings.tagmap[inlineTag] = imageTag;
        content = content.replace(imageTag, inlineTag);
      }
    }
    return content;
  },

  /**
   * @param jQuery imgNode
   *  Image node to create tag from
   */
  createTag: function (imgNode) {
    // Currently this is the <img> itself
    // Collect all attribs to be stashed into tagContent
    var mediaAttributes = {};
    var imgElement = imgNode[0];
    var sorter = [];

    // @todo: this does not work in IE, width and height are always 0.
    for (i=0; i< imgElement.attributes.length; i++) {
      var attr = imgElement.attributes[i];
      if (attr.specified == true) {
        if (attr.name !== 'class') {
          sorter.push(attr);
        }
        else {
          // Exctract expando properties from the class field.
          var attributes = this.getAttributesFromClass(attr.value);
          for (var name in attributes) {
            if (attributes.hasOwnProperty(name)) {
              var value = attributes[name];
              if (value.type && value.type === 'attr') {
                sorter.push(value);
              }
            }
          }
        }
      }
    }

    sorter.sort(this.sortAttributes);

    for (var prop in sorter) {
      mediaAttributes[sorter[prop].name] = sorter[prop].value;
    }

    // The following 5 ifs are dedicated to IE7
    // If the style is null, it is because IE7 can't read values from itself
    if (jQuery.browser.msie && jQuery.browser.version == '7.0') {
      if (mediaAttributes.style === "null") {
        var imgHeight = imgNode.css('height');
        var imgWidth = imgNode.css('width');
        mediaAttributes.style = {
          height: imgHeight,
          width: imgWidth
        }
        if (!mediaAttributes['width']) {
          mediaAttributes['width'] = imgWidth;
        }
        if (!mediaAttributes['height']) {
          mediaAttributes['height'] = imgHeight;
        }
      }
      // If the attribute width is zero, get the CSS width
      if (Number(mediaAttributes['width']) === 0) {
        mediaAttributes['width'] = imgNode.css('width');
      }
      // IE7 does support 'auto' as a value of the width attribute. It will not
      // display the image if this value is allowed to pass through
      if (mediaAttributes['width'] === 'auto') {
        delete mediaAttributes['width'];
      }
      // If the attribute height is zero, get the CSS height
      if (Number(mediaAttributes['height']) === 0) {
        mediaAttributes['height'] = imgNode.css('height');
      }
      // IE7 does support 'auto' as a value of the height attribute. It will not
      // display the image if this value is allowed to pass through
      if (mediaAttributes['height'] === 'auto') {
        delete mediaAttributes['height'];
      }
    }

   // Convert style-based floating of images to classes. Note we leave the
   // the attribute so that the WYSIWYG editor will remember the setting.
   // First, remove any previous left/right classes, note that class will
   // contain at least 'media-image'
   mediaAttributes['class'] = mediaAttributes['class'].replace(/\s*media-image-(left|right)\s*/g, ' ');

   // Add appropriate left/right class to the <img> tag.
   if (mediaAttributes['style']) {
     if (-1 != mediaAttributes['style'].indexOf('float: right;')) {
       mediaAttributes['class'] += ' media-image-right';
    }
    if (-1 != mediaAttributes['style'].indexOf('float: left;')) {
      mediaAttributes['class'] += ' media-image-left';
     }
   }

    // Remove elements from attribs using the blacklist
    for (var blackList in Drupal.settings.media.blacklist) {
      delete mediaAttributes[Drupal.settings.media.blacklist[blackList]];
    }
    tagContent = {
      "type": 'media',
      // @todo: This will be selected from the format form
      "view_mode": attributes['view_mode'].value,
      "fid" : attributes['fid'].value,
      "attributes": mediaAttributes
    };
    return '[[' + JSON.stringify(tagContent) + ']]';
  },

  /**
   * Forces custom attributes into the class field of the specified image.
   *
   * Due to a bug in some versions of Firefox
   * (http://forums.mozillazine.org/viewtopic.php?f=9&t=1991855), the
   * custom attributes used to share information about the image are
   * being stripped as the image markup is set into the rich text
   * editor.  Here we encode these attributes into the class field so
   * the data survives.
   *
   * @param imgElement
   *   The image
   * @fid
   *   The file id.
   * @param view_mode
   *   The view mode.
   * @param additional
   *   Additional attributes to add to the image.
   */
  forceAttributesIntoClass: function (imgElement, fid, view_mode, additional) {
    var wysiwyg = imgElement.attr('wysiwyg');
    if (wysiwyg) {
      imgElement.addClass('attr__wysiwyg__' + wysiwyg);
    }
    var format = imgElement.attr('format');
    if (format) {
      imgElement.addClass('attr__format__' + format);
    }
    var typeOf = imgElement.attr('typeof');
    if (typeOf) {
      imgElement.addClass('attr__typeof__' + typeOf);
    }
    if (fid) {
      imgElement.addClass('img__fid__' + fid);
    }
    if (view_mode) {
      imgElement.addClass('img__view_mode__' + view_mode);
    }
    if (additional) {
      for (var name in additional) {
        if (additional.hasOwnProperty(name)) {
          if (name !== 'alt') {
            imgElement.addClass('attr__' + name + '__' + additional[name]);
          }
        }
      }
    }
  },

  /**
   * Retrieves encoded attributes from the specified class string.
   *
   * @param classString
   *   A string containing the value of the class attribute.
   * @return
   *   An array containing the attribute names as keys, and an object
   *   with the name, value, and attribute type (either 'attr' or
   *   'img', depending on whether it is an image attribute or should
   *   be it the attributes section)
   */
  getAttributesFromClass: function (classString) {
    var actualClasses = [];
    var otherAttributes = [];
    var classes = classString.split(' ');
    var regexp = new RegExp('^(attr|img)__([^\S]*)__([^\S]*)$');
    for (var index = 0; index < classes.length; index++) {
      var matches = classes[index].match(regexp);
      if (matches && matches.length === 4) {
        otherAttributes[matches[2]] = {name: matches[2], value: matches[3], type: matches[1]};
      }
      else {
        actualClasses.push(classes[index]);
      }
    }
    if (actualClasses.length > 0) {
      otherAttributes['class'] = {name: 'class', value: actualClasses.join(' '), type: 'attr'};
    }
    return otherAttributes;
  },

  /*
   *
   */
  sortAttributes: function (a, b) {
    var nameA = a.name.toLowerCase();
    var nameB = b.name.toLowerCase();
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0;
  }
};
}

})(jQuery);
;/**/
/*
 *  JAIL: jQuery Asynchronous Image Loader (brand camp version)
 */
(function ( name, definition ){
  // jquery plugin pattern - AMD + CommonJS - by Addy Osmani (https://github.com/addyosmani/jquery-plugin-patterns/blob/master/amd+commonjs/pluginCore.js)
  var theModule = definition(jQuery),
      hasDefine = typeof define === 'function' && define.amd;

  if ( hasDefine ){
    // AMD module
    define( 'jail', ['jquery'], theModule );

  }  else {
    ( this.jQuery || this.$ || this )[name] = theModule;
  }
}( 'jail', function ($) {
  /*
   * Public function defining 'jail'
   *
   * @param elements : images to load
   * @param options : configurations object
   */
  $.jail = function( elements, options) {
    $('body.page-node img[data-src]:visible').each(function(){
      $img = $(this);
      $img.attr("src", $img.attr("data-src"));
      $img.removeAttr('data-src');
    });
    if (!$isotopePager.hasOwnProperty('getVisible')) {
      return; //pager haven't loaded yet
    }

    $($isotopePager.getVisible()).find('img[data-src]').each(function(){
      $page = $('#page');
      $img = $(this);
      //if (_isInTheScreen($page, $img, 0)) {
        $img.attr("src", $img.attr("data-src"));
        $img.removeAttr('data-src');
      //}
    })
  };

  /*
   * Function that returns true if the image is visible inside the "window" (or specified container element)
   *
   * @param $ct : container - jQuery obj
   * @param $img : image selected - jQuery obj
   * @param optionOffset : offset
   */
  function _isInTheScreen ( $ct, $img, optionOffset ) {
    var is_ct_window  = $ct[0] === window,
        ct_offset  = (is_ct_window ? { top:0, left:0 } : $ct.offset()),
        ct_top     = ct_offset.top + ( is_ct_window ? $ct.scrollTop() : 0),
        ct_left    = ct_offset.left + ( is_ct_window ? $ct.scrollLeft() : 0),
        ct_right   = ct_left + $ct.width(),
        ct_bottom  = ct_top + $ct.height(),
        img_offset = $img.offset(),
        img_width = $img.width(),
        img_height = $img.height();

    return (ct_top - optionOffset) <= (img_offset.top + img_height) &&
        (ct_bottom + optionOffset) >= img_offset.top &&
        (ct_left - optionOffset)<= (img_offset.left + img_width) &&
        (ct_right + optionOffset) >= img_offset.left;
  }

  // Small wrapper
  $.fn.jail = function( options ) {

    new $.jail( this, options );

    // Empty current stack
    currentStack = [];

    return this;
  };

  return $.jail;
}));;/**/
/**
 * @file
 * Theme specific JS file for brand_camp_theme
 */
(function($){
  $('.bc-menu-btn').live('click', function(e) {
    if ($('.block-wfm-monster-nav .block-content').toggleClass('displayed').hasClass('displayed')) {
      $('.block-wfm-monster-nav .block-content').animate({marginLeft: 0}, 400);
    }
    else {
      $('.block-wfm-monster-nav .block-content').animate({marginLeft: "-205px"}, 400);
    }
  });
  $('.nav-tab').live('click', function(e) {
    var $monsterNavBlockContent = $('.block-wfm-monster-nav .block-content');
    if ($monsterNavBlockContent.toggleClass('displayed').hasClass('displayed')) {
      $monsterNavBlockContent.animate({marginLeft: 0}, 400);
    }
    else {
      $monsterNavBlockContent.animate({marginLeft: "-205px"}, 400);
    }
  });
  $('.custom-load-more a').live('click', function(e) {
    $(this).hide();
    $('#block-views-brand-camp-list-block-1').show();
    $.jail();
    return false;
  });

  Drupal.behaviors.events = {
    attach: function(context, settings) {
      $(document).ajaxComplete(function(event, xhr, settings){
        //$(window).resize();
      //  $('.bc-progress').remove();
        if ($('#colorbox:visible').length) {
          var $cboxImages = $('#cboxContent img');
          if (!$cboxImages.length) {
            $.colorbox.resize();
          } else {
            var i = 0;
            $cboxImages.each(function(){
              $(this).load(function(){
                i++;
                if(i == $cboxImages.length) {
                  setTimeout($.colorbox.resize(), 100);
                }
              });
            });
          }

          $('#footer .view-display-id-wfm_footer_store_info').each(function() {
            $('#colorbox #footer-user-store-selector').replaceWith($(this).html());
          });
        }
      });
      //
      //$form.ajaxStart(function(event, xhr, settings){
      //  $(window).resize();
      //  if (!$('.bc-progress').length) {
      //    $(this).parent('.view-filters').after('<div class="bc-progress"></div>');
      //  }
      //});
    }
  };

  $(document).ready(function(){
    $('<div class="bc-menu-btn">' + Drupal.t('Menu') + '</div>').prependTo('#block-wfm-monster-nav-wfm-monster-navigation .block-inner');
    $(window).resize(function(){
      if ($('#colorbox:visible').length) {
        $.colorbox.resize({width: Math.min(1060, $(window).width())});
      }
      var $widget = $('.views-exposed-widget, .isoFilters-widget');
      var $formWrp = $('.view-filters, .isoFilters');
      if ($('#page').width() <= 1059) {
        $widget.addClass('tablet-widget');
        assignClick();
        // maybe we should replace it by media-query
        $formWrp.removeClass('desktop');
      }
      else {
        $formWrp.addClass('desktop');
        $widget.removeClass('tablet-widget').removeClass('expanded').attr('style', '');
        $widget.unbind('click');
      }
    });
    $(window).resize();
  });

  function assignClick() {
    // Removes previous event binding to prevent reactions duplicate
    $('.tablet-widget').unbind('click');
    $('.tablet-widget').click(function(e){
      var height = getLabelsHeight($(this));
      if (!$(this).hasClass('expanded')) {
        $(this).siblings().height(50).removeClass('expanded');
        $(this).height(height).addClass('expanded');
      }
      else {
        if (e.target.nodeName == 'LABEL') {
          $('#' + $(e.target).attr('for')).attr('checked', 'checked');
          $('#edit-submit-brand-camp-list').click();
          return;
        }
        $(this).height(50).removeClass('expanded');
      }
    });
  }

  function getLabelsHeight($el) {
    var $labels = $el.find('.views-widget label, .filter');
    var height = $labels.height();
    return height * ($labels.length + 1);
  }

  // animates couple of text slides
  function animateNext(outId, inId) {
    if ('flagIE' in window && flagIE == true) {
      jQuery(outId).animate({
        left: "-1000px",
      }, 1000, function(){
        jQuery(this).hide();
      });
      jQuery(inId).css('left', '1000px').show().animate({
        left: "0",
      }, 1000);

      return;
    }
    jQuery(outId).removeClass('slideInRight').addClass('slideOutLeft');
    jQuery(inId).removeClass('invisible').removeClass('slideOutLeft').addClass('slideInRight');
  }

  //animates array of text slides (id's)
  function animateSlides(slides) {
    var currentSlide = 0;
//    slides.forEach(function (slide) { jQuery(slide).removeClass('invisible') });
    setInterval(function() {
      if (currentSlide == slides.length-1) {
        animateNext(slides[currentSlide], slides[0]);
        currentSlide = 0;
      } else {
        animateNext(slides[currentSlide], slides[currentSlide+1]);
        currentSlide++;
      }
    }, 5000);
  }

  //animate header and footer text slides
  setTimeout(function() {
    animateSlides(['#header1', '#header2', '#header3', '#header4']);
  }, 1000);

  $(document).ready(function() {
    (function addScrollTopButton() {
      $('body').append(
        $('<div></div>')
          .attr({ class: 'scrollTop', id: 'scrollTop' })
          .click(function() {
            $.colorbox.close();
            window.scrollTo(0, 0);
          })
          .hide()
      );
    })();

    $(window).scroll(function() {
      if ($(this).scrollTop() > 1000 ) {
        $('#scrollTop').fadeIn(400);
      } else {
        $('#scrollTop').fadeOut(400);
      }
    });

    var _windowWidth = 0;
    $(window).resize(function() {
      if (_windowWidth != jQuery(window).width()) {
        _windowWidth = jQuery(window).width();
        jQuery('.page-values-matter .view-brand-camp-list.views-quicksand-container .view-content')
          .css({width: 'auto', height: 'auto'});
      }
    });
  });

})(jQuery);

jQuery.urlParam = function(name){
  var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
  if (results==null){
    return null;
  }
  else{
    return results[1] || 0;
  }
};/**/
/*!
 * Isotope PACKAGED v2.0.1
 * Filter & sort magical layouts
 * http://isotope.metafizzy.co
 */

/**
 * Bridget makes jQuery widgets
 * v1.0.1
 */

( function( window ) {



// -------------------------- utils -------------------------- //

  var slice = Array.prototype.slice;

  function noop() {}

// -------------------------- definition -------------------------- //

  function defineBridget( $ ) {

// bail if no jQuery
    if ( !$ ) {
      return;
    }

// -------------------------- addOptionMethod -------------------------- //

    /**
     * adds option method -> $().plugin('option', {...})
     * @param {Function} PluginClass - constructor class
     */
    function addOptionMethod( PluginClass ) {
      // don't overwrite original option method
      if ( PluginClass.prototype.option ) {
        return;
      }

      // option setter
      PluginClass.prototype.option = function( opts ) {
        // bail out if not an object
        if ( !$.isPlainObject( opts ) ){
          return;
        }
        this.options = $.extend( true, this.options, opts );
      };
    }


// -------------------------- plugin bridge -------------------------- //

// helper function for logging errors
// $.error breaks jQuery chaining
    var logError = typeof console === 'undefined' ? noop :
      function( message ) {
        console.error( message );
      };

    /**
     * jQuery plugin bridge, access methods like $elem.plugin('method')
     * @param {String} namespace - plugin name
     * @param {Function} PluginClass - constructor class
     */
    function bridge( namespace, PluginClass ) {
      // add to jQuery fn namespace
      $.fn[ namespace ] = function( options ) {
        if ( typeof options === 'string' ) {
          // call plugin method when first argument is a string
          // get arguments for method
          var args = slice.call( arguments, 1 );

          for ( var i=0, len = this.length; i < len; i++ ) {
            var elem = this[i];
            var instance = $.data( elem, namespace );
            if ( !instance ) {
              logError( "cannot call methods on " + namespace + " prior to initialization; " +
              "attempted to call '" + options + "'" );
              continue;
            }
            if ( !$.isFunction( instance[options] ) || options.charAt(0) === '_' ) {
              logError( "no such method '" + options + "' for " + namespace + " instance" );
              continue;
            }

            // trigger method with arguments
            var returnValue = instance[ options ].apply( instance, args );

            // break look and return first value if provided
            if ( returnValue !== undefined ) {
              return returnValue;
            }
          }
          // return this if no return value
          return this;
        } else {
          return this.each( function() {
            var instance = $.data( this, namespace );
            if ( instance ) {
              // apply options & init
              instance.option( options );
              instance._init();
            } else {
              // initialize new instance
              instance = new PluginClass( this, options );
              $.data( this, namespace, instance );
            }
          });
        }
      };

    }

// -------------------------- bridget -------------------------- //

    /**
     * converts a Prototypical class into a proper jQuery plugin
     *   the class must have a ._init method
     * @param {String} namespace - plugin name, used in $().pluginName
     * @param {Function} PluginClass - constructor class
     */
    $.bridget = function( namespace, PluginClass ) {
      addOptionMethod( PluginClass );
      bridge( namespace, PluginClass );
    };

    return $.bridget;

  }

// transport
  if ( typeof define === 'function' && define.amd ) {
    // AMD
    define( 'jquery-bridget/jquery.bridget',[ 'jquery' ], defineBridget );
  } else {
    // get jquery from browser global
    defineBridget( window.jQuery );
  }

})( window );

/*!
 * eventie v1.0.5
 * event binding helper
 *   eventie.bind( elem, 'click', myFn )
 *   eventie.unbind( elem, 'click', myFn )
 * MIT license
 */

/*jshint browser: true, undef: true, unused: true */
/*global define: false, module: false */

( function( window ) {



  var docElem = document.documentElement;

  var bind = function() {};

  function getIEEvent( obj ) {
    var event = window.event;
    // add event.target
    event.target = event.target || event.srcElement || obj;
    return event;
  }

  if ( docElem.addEventListener ) {
    bind = function( obj, type, fn ) {
      obj.addEventListener( type, fn, false );
    };
  } else if ( docElem.attachEvent ) {
    bind = function( obj, type, fn ) {
      obj[ type + fn ] = fn.handleEvent ?
        function() {
          var event = getIEEvent( obj );
          fn.handleEvent.call( fn, event );
        } :
        function() {
          var event = getIEEvent( obj );
          fn.call( obj, event );
        };
      obj.attachEvent( "on" + type, obj[ type + fn ] );
    };
  }

  var unbind = function() {};

  if ( docElem.removeEventListener ) {
    unbind = function( obj, type, fn ) {
      obj.removeEventListener( type, fn, false );
    };
  } else if ( docElem.detachEvent ) {
    unbind = function( obj, type, fn ) {
      obj.detachEvent( "on" + type, obj[ type + fn ] );
      try {
        delete obj[ type + fn ];
      } catch ( err ) {
        // can't delete window object properties
        obj[ type + fn ] = undefined;
      }
    };
  }

  var eventie = {
    bind: bind,
    unbind: unbind
  };

// ----- module definition ----- //

  if ( typeof define === 'function' && define.amd ) {
    // AMD
    define( 'eventie/eventie',eventie );
  } else if ( typeof exports === 'object' ) {
    // CommonJS
    module.exports = eventie;
  } else {
    // browser global
    window.eventie = eventie;
  }

})( this );

/*!
 * docReady
 * Cross browser DOMContentLoaded event emitter
 */

/*jshint browser: true, strict: true, undef: true, unused: true*/
/*global define: false */

( function( window ) {



  var document = window.document;
// collection of functions to be triggered on ready
  var queue = [];

  function docReady( fn ) {
    // throw out non-functions
    if ( typeof fn !== 'function' ) {
      return;
    }

    if ( docReady.isReady ) {
      // ready now, hit it
      fn();
    } else {
      // queue function when ready
      queue.push( fn );
    }
  }

  docReady.isReady = false;

// triggered on various doc ready events
  function init( event ) {
    // bail if IE8 document is not ready just yet
    var isIE8NotReady = event.type === 'readystatechange' && document.readyState !== 'complete';
    if ( docReady.isReady || isIE8NotReady ) {
      return;
    }
    docReady.isReady = true;

    // process queue
    for ( var i=0, len = queue.length; i < len; i++ ) {
      var fn = queue[i];
      fn();
    }
  }

  function defineDocReady( eventie ) {
    eventie.bind( document, 'DOMContentLoaded', init );
    eventie.bind( document, 'readystatechange', init );
    eventie.bind( window, 'load', init );

    return docReady;
  }

// transport
  if ( typeof define === 'function' && define.amd ) {
    // AMD
    // if RequireJS, then doc is already ready
    docReady.isReady = typeof requirejs === 'function';
    define( 'doc-ready/doc-ready',[ 'eventie/eventie' ], defineDocReady );
  } else {
    // browser global
    window.docReady = defineDocReady( window.eventie );
  }

})( this );

/*!
 * EventEmitter v4.2.7 - git.io/ee
 * Oliver Caldwell
 * MIT license
 * @preserve
 */

(function () {


  /**
   * Class for managing events.
   * Can be extended to provide event functionality in other classes.
   *
   * @class EventEmitter Manages event registering and emitting.
   */
  function EventEmitter() {}

  // Shortcuts to improve speed and size
  var proto = EventEmitter.prototype;
  var exports = this;
  var originalGlobalValue = exports.EventEmitter;

  /**
   * Finds the index of the listener for the event in it's storage array.
   *
   * @param {Function[]} listeners Array of listeners to search through.
   * @param {Function} listener Method to look for.
   * @return {Number} Index of the specified listener, -1 if not found
   * @api private
   */
  function indexOfListener(listeners, listener) {
    var i = listeners.length;
    while (i--) {
      if (listeners[i].listener === listener) {
        return i;
      }
    }

    return -1;
  }

  /**
   * Alias a method while keeping the context correct, to allow for overwriting of target method.
   *
   * @param {String} name The name of the target method.
   * @return {Function} The aliased method
   * @api private
   */
  function alias(name) {
    return function aliasClosure() {
      return this[name].apply(this, arguments);
    };
  }

  /**
   * Returns the listener array for the specified event.
   * Will initialise the event object and listener arrays if required.
   * Will return an object if you use a regex search. The object contains keys for each matched event. So /ba[rz]/ might return an object containing bar and baz. But only if you have either defined them with defineEvent or added some listeners to them.
   * Each property in the object response is an array of listener functions.
   *
   * @param {String|RegExp} evt Name of the event to return the listeners from.
   * @return {Function[]|Object} All listener functions for the event.
   */
  proto.getListeners = function getListeners(evt) {
    var events = this._getEvents();
    var response;
    var key;

    // Return a concatenated array of all matching events if
    // the selector is a regular expression.
    if (evt instanceof RegExp) {
      response = {};
      for (key in events) {
        if (events.hasOwnProperty(key) && evt.test(key)) {
          response[key] = events[key];
        }
      }
    }
    else {
      response = events[evt] || (events[evt] = []);
    }

    return response;
  };

  /**
   * Takes a list of listener objects and flattens it into a list of listener functions.
   *
   * @param {Object[]} listeners Raw listener objects.
   * @return {Function[]} Just the listener functions.
   */
  proto.flattenListeners = function flattenListeners(listeners) {
    var flatListeners = [];
    var i;

    for (i = 0; i < listeners.length; i += 1) {
      flatListeners.push(listeners[i].listener);
    }

    return flatListeners;
  };

  /**
   * Fetches the requested listeners via getListeners but will always return the results inside an object. This is mainly for internal use but others may find it useful.
   *
   * @param {String|RegExp} evt Name of the event to return the listeners from.
   * @return {Object} All listener functions for an event in an object.
   */
  proto.getListenersAsObject = function getListenersAsObject(evt) {
    var listeners = this.getListeners(evt);
    var response;

    if (listeners instanceof Array) {
      response = {};
      response[evt] = listeners;
    }

    return response || listeners;
  };

  /**
   * Adds a listener function to the specified event.
   * The listener will not be added if it is a duplicate.
   * If the listener returns true then it will be removed after it is called.
   * If you pass a regular expression as the event name then the listener will be added to all events that match it.
   *
   * @param {String|RegExp} evt Name of the event to attach the listener to.
   * @param {Function} listener Method to be called when the event is emitted. If the function returns true then it will be removed after calling.
   * @return {Object} Current instance of EventEmitter for chaining.
   */
  proto.addListener = function addListener(evt, listener) {
    var listeners = this.getListenersAsObject(evt);
    var listenerIsWrapped = typeof listener === 'object';
    var key;

    for (key in listeners) {
      if (listeners.hasOwnProperty(key) && indexOfListener(listeners[key], listener) === -1) {
        listeners[key].push(listenerIsWrapped ? listener : {
          listener: listener,
          once: false
        });
      }
    }

    return this;
  };

  /**
   * Alias of addListener
   */
  proto.on = alias('addListener');

  /**
   * Semi-alias of addListener. It will add a listener that will be
   * automatically removed after it's first execution.
   *
   * @param {String|RegExp} evt Name of the event to attach the listener to.
   * @param {Function} listener Method to be called when the event is emitted. If the function returns true then it will be removed after calling.
   * @return {Object} Current instance of EventEmitter for chaining.
   */
  proto.addOnceListener = function addOnceListener(evt, listener) {
    return this.addListener(evt, {
      listener: listener,
      once: true
    });
  };

  /**
   * Alias of addOnceListener.
   */
  proto.once = alias('addOnceListener');

  /**
   * Defines an event name. This is required if you want to use a regex to add a listener to multiple events at once. If you don't do this then how do you expect it to know what event to add to? Should it just add to every possible match for a regex? No. That is scary and bad.
   * You need to tell it what event names should be matched by a regex.
   *
   * @param {String} evt Name of the event to create.
   * @return {Object} Current instance of EventEmitter for chaining.
   */
  proto.defineEvent = function defineEvent(evt) {
    this.getListeners(evt);
    return this;
  };

  /**
   * Uses defineEvent to define multiple events.
   *
   * @param {String[]} evts An array of event names to define.
   * @return {Object} Current instance of EventEmitter for chaining.
   */
  proto.defineEvents = function defineEvents(evts) {
    for (var i = 0; i < evts.length; i += 1) {
      this.defineEvent(evts[i]);
    }
    return this;
  };

  /**
   * Removes a listener function from the specified event.
   * When passed a regular expression as the event name, it will remove the listener from all events that match it.
   *
   * @param {String|RegExp} evt Name of the event to remove the listener from.
   * @param {Function} listener Method to remove from the event.
   * @return {Object} Current instance of EventEmitter for chaining.
   */
  proto.removeListener = function removeListener(evt, listener) {
    var listeners = this.getListenersAsObject(evt);
    var index;
    var key;

    for (key in listeners) {
      if (listeners.hasOwnProperty(key)) {
        index = indexOfListener(listeners[key], listener);

        if (index !== -1) {
          listeners[key].splice(index, 1);
        }
      }
    }

    return this;
  };

  /**
   * Alias of removeListener
   */
  proto.off = alias('removeListener');

  /**
   * Adds listeners in bulk using the manipulateListeners method.
   * If you pass an object as the second argument you can add to multiple events at once. The object should contain key value pairs of events and listeners or listener arrays. You can also pass it an event name and an array of listeners to be added.
   * You can also pass it a regular expression to add the array of listeners to all events that match it.
   * Yeah, this function does quite a bit. That's probably a bad thing.
   *
   * @param {String|Object|RegExp} evt An event name if you will pass an array of listeners next. An object if you wish to add to multiple events at once.
   * @param {Function[]} [listeners] An optional array of listener functions to add.
   * @return {Object} Current instance of EventEmitter for chaining.
   */
  proto.addListeners = function addListeners(evt, listeners) {
    // Pass through to manipulateListeners
    return this.manipulateListeners(false, evt, listeners);
  };

  /**
   * Removes listeners in bulk using the manipulateListeners method.
   * If you pass an object as the second argument you can remove from multiple events at once. The object should contain key value pairs of events and listeners or listener arrays.
   * You can also pass it an event name and an array of listeners to be removed.
   * You can also pass it a regular expression to remove the listeners from all events that match it.
   *
   * @param {String|Object|RegExp} evt An event name if you will pass an array of listeners next. An object if you wish to remove from multiple events at once.
   * @param {Function[]} [listeners] An optional array of listener functions to remove.
   * @return {Object} Current instance of EventEmitter for chaining.
   */
  proto.removeListeners = function removeListeners(evt, listeners) {
    // Pass through to manipulateListeners
    return this.manipulateListeners(true, evt, listeners);
  };

  /**
   * Edits listeners in bulk. The addListeners and removeListeners methods both use this to do their job. You should really use those instead, this is a little lower level.
   * The first argument will determine if the listeners are removed (true) or added (false).
   * If you pass an object as the second argument you can add/remove from multiple events at once. The object should contain key value pairs of events and listeners or listener arrays.
   * You can also pass it an event name and an array of listeners to be added/removed.
   * You can also pass it a regular expression to manipulate the listeners of all events that match it.
   *
   * @param {Boolean} remove True if you want to remove listeners, false if you want to add.
   * @param {String|Object|RegExp} evt An event name if you will pass an array of listeners next. An object if you wish to add/remove from multiple events at once.
   * @param {Function[]} [listeners] An optional array of listener functions to add/remove.
   * @return {Object} Current instance of EventEmitter for chaining.
   */
  proto.manipulateListeners = function manipulateListeners(remove, evt, listeners) {
    var i;
    var value;
    var single = remove ? this.removeListener : this.addListener;
    var multiple = remove ? this.removeListeners : this.addListeners;

    // If evt is an object then pass each of it's properties to this method
    if (typeof evt === 'object' && !(evt instanceof RegExp)) {
      for (i in evt) {
        if (evt.hasOwnProperty(i) && (value = evt[i])) {
          // Pass the single listener straight through to the singular method
          if (typeof value === 'function') {
            single.call(this, i, value);
          }
          else {
            // Otherwise pass back to the multiple function
            multiple.call(this, i, value);
          }
        }
      }
    }
    else {
      // So evt must be a string
      // And listeners must be an array of listeners
      // Loop over it and pass each one to the multiple method
      i = listeners.length;
      while (i--) {
        single.call(this, evt, listeners[i]);
      }
    }

    return this;
  };

  /**
   * Removes all listeners from a specified event.
   * If you do not specify an event then all listeners will be removed.
   * That means every event will be emptied.
   * You can also pass a regex to remove all events that match it.
   *
   * @param {String|RegExp} [evt] Optional name of the event to remove all listeners for. Will remove from every event if not passed.
   * @return {Object} Current instance of EventEmitter for chaining.
   */
  proto.removeEvent = function removeEvent(evt) {
    var type = typeof evt;
    var events = this._getEvents();
    var key;

    // Remove different things depending on the state of evt
    if (type === 'string') {
      // Remove all listeners for the specified event
      delete events[evt];
    }
    else if (evt instanceof RegExp) {
      // Remove all events matching the regex.
      for (key in events) {
        if (events.hasOwnProperty(key) && evt.test(key)) {
          delete events[key];
        }
      }
    }
    else {
      // Remove all listeners in all events
      delete this._events;
    }

    return this;
  };

  /**
   * Alias of removeEvent.
   *
   * Added to mirror the node API.
   */
  proto.removeAllListeners = alias('removeEvent');

  /**
   * Emits an event of your choice.
   * When emitted, every listener attached to that event will be executed.
   * If you pass the optional argument array then those arguments will be passed to every listener upon execution.
   * Because it uses `apply`, your array of arguments will be passed as if you wrote them out separately.
   * So they will not arrive within the array on the other side, they will be separate.
   * You can also pass a regular expression to emit to all events that match it.
   *
   * @param {String|RegExp} evt Name of the event to emit and execute listeners for.
   * @param {Array} [args] Optional array of arguments to be passed to each listener.
   * @return {Object} Current instance of EventEmitter for chaining.
   */
  proto.emitEvent = function emitEvent(evt, args) {
    var listeners = this.getListenersAsObject(evt);
    var listener;
    var i;
    var key;
    var response;

    for (key in listeners) {
      if (listeners.hasOwnProperty(key)) {
        i = listeners[key].length;

        while (i--) {
          // If the listener returns true then it shall be removed from the event
          // The function is executed either with a basic call or an apply if there is an args array
          listener = listeners[key][i];

          if (listener.once === true) {
            this.removeListener(evt, listener.listener);
          }

          response = listener.listener.apply(this, args || []);

          if (response === this._getOnceReturnValue()) {
            this.removeListener(evt, listener.listener);
          }
        }
      }
    }

    return this;
  };

  /**
   * Alias of emitEvent
   */
  proto.trigger = alias('emitEvent');

  /**
   * Subtly different from emitEvent in that it will pass its arguments on to the listeners, as opposed to taking a single array of arguments to pass on.
   * As with emitEvent, you can pass a regex in place of the event name to emit to all events that match it.
   *
   * @param {String|RegExp} evt Name of the event to emit and execute listeners for.
   * @param {...*} Optional additional arguments to be passed to each listener.
   * @return {Object} Current instance of EventEmitter for chaining.
   */
  proto.emit = function emit(evt) {
    var args = Array.prototype.slice.call(arguments, 1);
    return this.emitEvent(evt, args);
  };

  /**
   * Sets the current value to check against when executing listeners. If a
   * listeners return value matches the one set here then it will be removed
   * after execution. This value defaults to true.
   *
   * @param {*} value The new value to check for when executing listeners.
   * @return {Object} Current instance of EventEmitter for chaining.
   */
  proto.setOnceReturnValue = function setOnceReturnValue(value) {
    this._onceReturnValue = value;
    return this;
  };

  /**
   * Fetches the current value to check against when executing listeners. If
   * the listeners return value matches this one then it should be removed
   * automatically. It will return true by default.
   *
   * @return {*|Boolean} The current value to check for or the default, true.
   * @api private
   */
  proto._getOnceReturnValue = function _getOnceReturnValue() {
    if (this.hasOwnProperty('_onceReturnValue')) {
      return this._onceReturnValue;
    }
    else {
      return true;
    }
  };

  /**
   * Fetches the events object and creates one if required.
   *
   * @return {Object} The events storage object.
   * @api private
   */
  proto._getEvents = function _getEvents() {
    return this._events || (this._events = {});
  };

  /**
   * Reverts the global {@link EventEmitter} to its previous value and returns a reference to this version.
   *
   * @return {Function} Non conflicting EventEmitter class.
   */
  EventEmitter.noConflict = function noConflict() {
    exports.EventEmitter = originalGlobalValue;
    return EventEmitter;
  };

  // Expose the class either via AMD, CommonJS or the global object
  if (typeof define === 'function' && define.amd) {
    define('eventEmitter/EventEmitter',[],function () {
      return EventEmitter;
    });
  }
  else if (typeof module === 'object' && module.exports){
    module.exports = EventEmitter;
  }
  else {
    this.EventEmitter = EventEmitter;
  }
}.call(this));

/*!
 * getStyleProperty v1.0.3
 * original by kangax
 * http://perfectionkills.com/feature-testing-css-properties/
 */

/*jshint browser: true, strict: true, undef: true */
/*global define: false, exports: false, module: false */

( function( window ) {



  var prefixes = 'Webkit Moz ms Ms O'.split(' ');
  var docElemStyle = document.documentElement.style;

  function getStyleProperty( propName ) {
    if ( !propName ) {
      return;
    }

    // test standard property first
    if ( typeof docElemStyle[ propName ] === 'string' ) {
      return propName;
    }

    // capitalize
    propName = propName.charAt(0).toUpperCase() + propName.slice(1);

    // test vendor specific properties
    var prefixed;
    for ( var i=0, len = prefixes.length; i < len; i++ ) {
      prefixed = prefixes[i] + propName;
      if ( typeof docElemStyle[ prefixed ] === 'string' ) {
        return prefixed;
      }
    }
  }

// transport
  if ( typeof define === 'function' && define.amd ) {
    // AMD
    define( 'get-style-property/get-style-property',[],function() {
      return getStyleProperty;
    });
  } else if ( typeof exports === 'object' ) {
    // CommonJS for Component
    module.exports = getStyleProperty;
  } else {
    // browser global
    window.getStyleProperty = getStyleProperty;
  }

})( window );

/**
 * getSize v1.1.7
 * measure size of elements
 */

/*jshint browser: true, strict: true, undef: true, unused: true */
/*global define: false, exports: false, require: false, module: false */

( function( window, undefined ) {



// -------------------------- helpers -------------------------- //

  var getComputedStyle = window.getComputedStyle;
  var getStyle = getComputedStyle ?
    function( elem ) {
      return getComputedStyle( elem, null );
    } :
    function( elem ) {
      return elem.currentStyle;
    };

// get a number from a string, not a percentage
  function getStyleSize( value ) {
    var num = parseFloat( value );
    // not a percent like '100%', and a number
    var isValid = value.indexOf('%') === -1 && !isNaN( num );
    return isValid && num;
  }

// -------------------------- measurements -------------------------- //

  var measurements = [
    'paddingLeft',
    'paddingRight',
    'paddingTop',
    'paddingBottom',
    'marginLeft',
    'marginRight',
    'marginTop',
    'marginBottom',
    'borderLeftWidth',
    'borderRightWidth',
    'borderTopWidth',
    'borderBottomWidth'
  ];

  function getZeroSize() {
    var size = {
      width: 0,
      height: 0,
      innerWidth: 0,
      innerHeight: 0,
      outerWidth: 0,
      outerHeight: 0
    };
    for ( var i=0, len = measurements.length; i < len; i++ ) {
      var measurement = measurements[i];
      size[ measurement ] = 0;
    }
    return size;
  }



  function defineGetSize( getStyleProperty ) {

// -------------------------- box sizing -------------------------- //

    var boxSizingProp = getStyleProperty('boxSizing');
    var isBoxSizeOuter;

    /**
     * WebKit measures the outer-width on style.width on border-box elems
     * IE & Firefox measures the inner-width
     */
    ( function() {
      if ( !boxSizingProp ) {
        return;
      }

      var div = document.createElement('div');
      div.style.width = '200px';
      div.style.padding = '1px 2px 3px 4px';
      div.style.borderStyle = 'solid';
      div.style.borderWidth = '1px 2px 3px 4px';
      div.style[ boxSizingProp ] = 'border-box';

      var body = document.body || document.documentElement;
      body.appendChild( div );
      var style = getStyle( div );

      isBoxSizeOuter = getStyleSize( style.width ) === 200;
      body.removeChild( div );
    })();


// -------------------------- getSize -------------------------- //

    function getSize( elem ) {
      // use querySeletor if elem is string
      if ( typeof elem === 'string' ) {
        elem = document.querySelector( elem );
      }

      // do not proceed on non-objects
      if ( !elem || typeof elem !== 'object' || !elem.nodeType ) {
        return;
      }

      var style = getStyle( elem );

      // if hidden, everything is 0
      if ( style.display === 'none' ) {
        return getZeroSize();
      }

      var size = {};
      size.width = elem.offsetWidth;
      size.height = elem.offsetHeight;

      var isBorderBox = size.isBorderBox = !!( boxSizingProp &&
      style[ boxSizingProp ] && style[ boxSizingProp ] === 'border-box' );

      // get all measurements
      for ( var i=0, len = measurements.length; i < len; i++ ) {
        var measurement = measurements[i];
        var value = style[ measurement ];
        value = mungeNonPixel( elem, value );
        var num = parseFloat( value );
        // any 'auto', 'medium' value will be 0
        size[ measurement ] = !isNaN( num ) ? num : 0;
      }

      var paddingWidth = size.paddingLeft + size.paddingRight;
      var paddingHeight = size.paddingTop + size.paddingBottom;
      var marginWidth = size.marginLeft + size.marginRight;
      var marginHeight = size.marginTop + size.marginBottom;
      var borderWidth = size.borderLeftWidth + size.borderRightWidth;
      var borderHeight = size.borderTopWidth + size.borderBottomWidth;

      var isBorderBoxSizeOuter = isBorderBox && isBoxSizeOuter;

      // overwrite width and height if we can get it from style
      var styleWidth = getStyleSize( style.width );
      if ( styleWidth !== false ) {
        size.width = styleWidth +
          // add padding and border unless it's already including it
        ( isBorderBoxSizeOuter ? 0 : paddingWidth + borderWidth );
      }

      var styleHeight = getStyleSize( style.height );
      if ( styleHeight !== false ) {
        size.height = styleHeight +
          // add padding and border unless it's already including it
        ( isBorderBoxSizeOuter ? 0 : paddingHeight + borderHeight );
      }

      size.innerWidth = size.width - ( paddingWidth + borderWidth );
      size.innerHeight = size.height - ( paddingHeight + borderHeight );

      size.outerWidth = size.width + marginWidth;
      size.outerHeight = size.height + marginHeight;

      return size;
    }

// IE8 returns percent values, not pixels
// taken from jQuery's curCSS
    function mungeNonPixel( elem, value ) {
      // IE8 and has percent value
      if ( getComputedStyle || value.indexOf('%') === -1 ) {
        return value;
      }
      var style = elem.style;
      // Remember the original values
      var left = style.left;
      var rs = elem.runtimeStyle;
      var rsLeft = rs && rs.left;

      // Put in the new values to get a computed value out
      if ( rsLeft ) {
        rs.left = elem.currentStyle.left;
      }
      style.left = value;
      value = style.pixelLeft;

      // Revert the changed values
      style.left = left;
      if ( rsLeft ) {
        rs.left = rsLeft;
      }

      return value;
    }

    return getSize;

  }

// transport
  if ( typeof define === 'function' && define.amd ) {
    // AMD for RequireJS
    define( 'get-size/get-size',[ 'get-style-property/get-style-property' ], defineGetSize );
  } else if ( typeof exports === 'object' ) {
    // CommonJS for Component
    module.exports = defineGetSize( require('get-style-property') );
  } else {
    // browser global
    window.getSize = defineGetSize( window.getStyleProperty );
  }

})( window );

/**
 * matchesSelector helper v1.0.1
 *
 * @name matchesSelector
 *   @param {Element} elem
 *   @param {String} selector
 */

/*jshint browser: true, strict: true, undef: true, unused: true */
/*global define: false */

( function( global, ElemProto ) {



  var matchesMethod = ( function() {
    // check un-prefixed
    if ( ElemProto.matchesSelector ) {
      return 'matchesSelector';
    }
    // check vendor prefixes
    var prefixes = [ 'webkit', 'moz', 'ms', 'o' ];

    for ( var i=0, len = prefixes.length; i < len; i++ ) {
      var prefix = prefixes[i];
      var method = prefix + 'MatchesSelector';
      if ( ElemProto[ method ] ) {
        return method;
      }
    }
  })();

  // ----- match ----- //

  function match( elem, selector ) {
    return elem[ matchesMethod ]( selector );
  }

  // ----- appendToFragment ----- //

  function checkParent( elem ) {
    // not needed if already has parent
    if ( elem.parentNode ) {
      return;
    }
    var fragment = document.createDocumentFragment();
    fragment.appendChild( elem );
  }

  // ----- query ----- //

  // fall back to using QSA
  // thx @jonathantneal https://gist.github.com/3062955
  function query( elem, selector ) {
    // append to fragment if no parent
    checkParent( elem );

    // match elem with all selected elems of parent
    var elems = elem.parentNode.querySelectorAll( selector );
    for ( var i=0, len = elems.length; i < len; i++ ) {
      // return true if match
      if ( elems[i] === elem ) {
        return true;
      }
    }
    // otherwise return false
    return false;
  }

  // ----- matchChild ----- //

  function matchChild( elem, selector ) {
    checkParent( elem );
    return match( elem, selector );
  }

  // ----- matchesSelector ----- //

  var matchesSelector;

  if ( matchesMethod ) {
    // IE9 supports matchesSelector, but doesn't work on orphaned elems
    // check for that
    var div = document.createElement('div');
    var supportsOrphans = match( div, 'div' );
    matchesSelector = supportsOrphans ? match : matchChild;
  } else {
    matchesSelector = query;
  }

  // transport
  if ( typeof define === 'function' && define.amd ) {
    // AMD
    define( 'matches-selector/matches-selector',[],function() {
      return matchesSelector;
    });
  } else {
    // browser global
    window.matchesSelector = matchesSelector;
  }

})( this, Element.prototype );

/**
 * Outlayer Item
 */

( function( window ) {



// ----- get style ----- //

  var getComputedStyle = window.getComputedStyle;
  var getStyle = getComputedStyle ?
    function( elem ) {
      return getComputedStyle( elem, null );
    } :
    function( elem ) {
      return elem.currentStyle;
    };


// extend objects
  function extend( a, b ) {
    for ( var prop in b ) {
      a[ prop ] = b[ prop ];
    }
    return a;
  }

  function isEmptyObj( obj ) {
    for ( var prop in obj ) {
      return false;
    }
    prop = null;
    return true;
  }

// http://jamesroberts.name/blog/2010/02/22/string-functions-for-javascript-trim-to-camel-case-to-dashed-and-to-underscore/
  function toDash( str ) {
    return str.replace( /([A-Z])/g, function( $1 ){
      return '-' + $1.toLowerCase();
    });
  }

// -------------------------- Outlayer definition -------------------------- //

  function outlayerItemDefinition( EventEmitter, getSize, getStyleProperty ) {

// -------------------------- CSS3 support -------------------------- //

    var transitionProperty = getStyleProperty('transition');
    var transformProperty = getStyleProperty('transform');
    var supportsCSS3 = transitionProperty && transformProperty;
    var is3d = !!getStyleProperty('perspective');

    var transitionEndEvent = {
      WebkitTransition: 'webkitTransitionEnd',
      MozTransition: 'transitionend',
      OTransition: 'otransitionend',
      transition: 'transitionend'
    }[ transitionProperty ];

// properties that could have vendor prefix
    var prefixableProperties = [
      'transform',
      'transition',
      'transitionDuration',
      'transitionProperty'
    ];

// cache all vendor properties
    var vendorProperties = ( function() {
      var cache = {};
      for ( var i=0, len = prefixableProperties.length; i < len; i++ ) {
        var prop = prefixableProperties[i];
        var supportedProp = getStyleProperty( prop );
        if ( supportedProp && supportedProp !== prop ) {
          cache[ prop ] = supportedProp;
        }
      }
      return cache;
    })();

// -------------------------- Item -------------------------- //

    function Item( element, layout ) {
      if ( !element ) {
        return;
      }

      this.element = element;
      // parent layout class, i.e. Masonry, Isotope, or Packery
      this.layout = layout;
      this.position = {
        x: 0,
        y: 0
      };

      this._create();
    }

// inherit EventEmitter
    extend( Item.prototype, EventEmitter.prototype );

    Item.prototype._create = function() {
      // transition objects
      this._transn = {
        ingProperties: {},
        clean: {},
        onEnd: {}
      };

      this.css({
        position: 'absolute'
      });
    };

// trigger specified handler for event type
    Item.prototype.handleEvent = function( event ) {
      var method = 'on' + event.type;
      if ( this[ method ] ) {
        this[ method ]( event );
      }
    };

    Item.prototype.getSize = function() {
      this.size = getSize( this.element );
    };

    /**
     * apply CSS styles to element
     * @param {Object} style
     */
    Item.prototype.css = function( style ) {
      var elemStyle = this.element.style;

      for ( var prop in style ) {
        // use vendor property if available
        var supportedProp = vendorProperties[ prop ] || prop;
        elemStyle[ supportedProp ] = style[ prop ];
      }
    };

    // measure position, and sets it
    Item.prototype.getPosition = function() {
      var style = getStyle( this.element );
      var layoutOptions = this.layout.options;
      var isOriginLeft = layoutOptions.isOriginLeft;
      var isOriginTop = layoutOptions.isOriginTop;
      var x = parseInt( style[ isOriginLeft ? 'left' : 'right' ], 10 );
      var y = parseInt( style[ isOriginTop ? 'top' : 'bottom' ], 10 );

      // clean up 'auto' or other non-integer values
      x = isNaN( x ) ? 0 : x;
      y = isNaN( y ) ? 0 : y;
      // remove padding from measurement
      var layoutSize = this.layout.size;
      x -= isOriginLeft ? layoutSize.paddingLeft : layoutSize.paddingRight;
      y -= isOriginTop ? layoutSize.paddingTop : layoutSize.paddingBottom;

      this.position.x = x;
      this.position.y = y;
    };

// set settled position, apply padding
    Item.prototype.layoutPosition = function() {
      var layoutSize = this.layout.size;
      var layoutOptions = this.layout.options;
      var style = {};

      if ( layoutOptions.isOriginLeft ) {
        style.left = ( this.position.x + layoutSize.paddingLeft ) + 'px';
        // reset other property
        style.right = '';
      } else {
        style.right = ( this.position.x + layoutSize.paddingRight ) + 'px';
        style.left = '';
      }

      if ( layoutOptions.isOriginTop ) {
        style.top = ( this.position.y + layoutSize.paddingTop ) + 'px';
        style.bottom = '';
      } else {
        style.bottom = ( this.position.y + layoutSize.paddingBottom ) + 'px';
        style.top = '';
      }

      this.css( style );
      this.emitEvent( 'layout', [ this ] );
    };


// transform translate function
    var translate = is3d ?
      function( x, y ) {
        return 'translate3d(' + x + 'px, ' + y + 'px, 0)';
      } :
      function( x, y ) {
        return 'translate(' + x + 'px, ' + y + 'px)';
      };


    Item.prototype._transitionTo = function( x, y ) {
      this.getPosition();
      // get current x & y from top/left
      var curX = this.position.x;
      var curY = this.position.y;

      var compareX = parseInt( x, 10 );
      var compareY = parseInt( y, 10 );
      var didNotMove = compareX === this.position.x && compareY === this.position.y;

      // save end position
      this.setPosition( x, y );

      // if did not move and not transitioning, just go to layout
      if ( didNotMove && !this.isTransitioning ) {
        this.layoutPosition();
        return;
      }

      var transX = x - curX;
      var transY = y - curY;
      var transitionStyle = {};
      // flip cooridinates if origin on right or bottom
      var layoutOptions = this.layout.options;
      transX = layoutOptions.isOriginLeft ? transX : -transX;
      transY = layoutOptions.isOriginTop ? transY : -transY;
      transitionStyle.transform = translate( transX, transY );

      this.transition({
        to: transitionStyle,
        onTransitionEnd: {
          transform: this.layoutPosition
        },
        isCleaning: true
      });
    };

// non transition + transform support
    Item.prototype.goTo = function( x, y ) {
      this.setPosition( x, y );
      this.layoutPosition();
    };

// use transition and transforms if supported
    Item.prototype.moveTo = supportsCSS3 ?
      Item.prototype._transitionTo : Item.prototype.goTo;

    Item.prototype.setPosition = function( x, y ) {
      this.position.x = parseInt( x, 10 );
      this.position.y = parseInt( y, 10 );
    };

// ----- transition ----- //

    /**
     * @param {Object} style - CSS
     * @param {Function} onTransitionEnd
     */

// non transition, just trigger callback
    Item.prototype._nonTransition = function( args ) {
      this.css( args.to );
      if ( args.isCleaning ) {
        this._removeStyles( args.to );
      }
      for ( var prop in args.onTransitionEnd ) {
        args.onTransitionEnd[ prop ].call( this );
      }
    };

    /**
     * proper transition
     * @param {Object} args - arguments
     *   @param {Object} to - style to transition to
     *   @param {Object} from - style to start transition from
     *   @param {Boolean} isCleaning - removes transition styles after transition
     *   @param {Function} onTransitionEnd - callback
     */
    Item.prototype._transition = function( args ) {
      // redirect to nonTransition if no transition duration
      if ( !parseFloat( this.layout.options.transitionDuration ) ) {
        this._nonTransition( args );
        return;
      }

      var _transition = this._transn;
      // keep track of onTransitionEnd callback by css property
      for ( var prop in args.onTransitionEnd ) {
        _transition.onEnd[ prop ] = args.onTransitionEnd[ prop ];
      }
      // keep track of properties that are transitioning
      for ( prop in args.to ) {
        _transition.ingProperties[ prop ] = true;
        // keep track of properties to clean up when transition is done
        if ( args.isCleaning ) {
          _transition.clean[ prop ] = true;
        }
      }

      // set from styles
      if ( args.from ) {
        this.css( args.from );
        // force redraw. http://blog.alexmaccaw.com/css-transitions
        var h = this.element.offsetHeight;
        // hack for JSHint to hush about unused var
        h = null;
      }
      // enable transition
      this.enableTransition( args.to );
      // set styles that are transitioning
      this.css( args.to );

      this.isTransitioning = true;

    };

    var itemTransitionProperties = transformProperty && ( toDash( transformProperty ) +
      ',opacity' );

    Item.prototype.enableTransition = function(/* style */) {
      // only enable if not already transitioning
      // bug in IE10 were re-setting transition style will prevent
      // transitionend event from triggering
      if ( this.isTransitioning ) {
        return;
      }

      // make transition: foo, bar, baz from style object
      // TODO uncomment this bit when IE10 bug is resolved
      // var transitionValue = [];
      // for ( var prop in style ) {
      //   // dash-ify camelCased properties like WebkitTransition
      //   transitionValue.push( toDash( prop ) );
      // }
      // enable transition styles
      // HACK always enable transform,opacity for IE10
      this.css({
        transitionProperty: itemTransitionProperties,
        transitionDuration: this.layout.options.transitionDuration
      });
      // listen for transition end event
      this.element.addEventListener( transitionEndEvent, this, false );
    };

    Item.prototype.transition = Item.prototype[ transitionProperty ? '_transition' : '_nonTransition' ];

// ----- events ----- //

    Item.prototype.onwebkitTransitionEnd = function( event ) {
      this.ontransitionend( event );
    };

    Item.prototype.onotransitionend = function( event ) {
      this.ontransitionend( event );
    };

// properties that I munge to make my life easier
    var dashedVendorProperties = {
      '-webkit-transform': 'transform',
      '-moz-transform': 'transform',
      '-o-transform': 'transform'
    };

    Item.prototype.ontransitionend = function( event ) {
      // disregard bubbled events from children
      if ( event.target !== this.element ) {
        return;
      }
      var _transition = this._transn;
      // get property name of transitioned property, convert to prefix-free
      var propertyName = dashedVendorProperties[ event.propertyName ] || event.propertyName;

      // remove property that has completed transitioning
      delete _transition.ingProperties[ propertyName ];
      // check if any properties are still transitioning
      if ( isEmptyObj( _transition.ingProperties ) ) {
        // all properties have completed transitioning
        this.disableTransition();
      }
      // clean style
      if ( propertyName in _transition.clean ) {
        // clean up style
        this.element.style[ event.propertyName ] = '';
        delete _transition.clean[ propertyName ];
      }
      // trigger onTransitionEnd callback
      if ( propertyName in _transition.onEnd ) {
        var onTransitionEnd = _transition.onEnd[ propertyName ];
        onTransitionEnd.call( this );
        delete _transition.onEnd[ propertyName ];
      }

      this.emitEvent( 'transitionEnd', [ this ] );
    };

    Item.prototype.disableTransition = function() {
      this.removeTransitionStyles();
      this.element.removeEventListener( transitionEndEvent, this, false );
      this.isTransitioning = false;
    };

    /**
     * removes style property from element
     * @param {Object} style
     **/
    Item.prototype._removeStyles = function( style ) {
      // clean up transition styles
      var cleanStyle = {};
      for ( var prop in style ) {
        cleanStyle[ prop ] = '';
      }
      this.css( cleanStyle );
    };

    var cleanTransitionStyle = {
      transitionProperty: '',
      transitionDuration: ''
    };

    Item.prototype.removeTransitionStyles = function() {
      // remove transition
      this.css( cleanTransitionStyle );
    };

// ----- show/hide/remove ----- //

// remove element from DOM
    Item.prototype.removeElem = function() {
      this.element.parentNode.removeChild( this.element );
      this.emitEvent( 'remove', [ this ] );
    };

    Item.prototype.remove = function() {
      // just remove element if no transition support or no transition
      if ( !transitionProperty || !parseFloat( this.layout.options.transitionDuration ) ) {
        this.removeElem();
        return;
      }

      // start transition
      var _this = this;
      this.on( 'transitionEnd', function() {
        _this.removeElem();
        return true; // bind once
      });
      this.hide();
    };

    Item.prototype.reveal = function() {
      delete this.isHidden;
      // remove display: none
      this.css({ display: '' });

      var options = this.layout.options;
      this.transition({
        from: options.hiddenStyle,
        to: options.visibleStyle,
        isCleaning: true
      });
    };

    Item.prototype.hide = function() {
      // set flag
      this.isHidden = true;
      // remove display: none
      this.css({ display: '' });

      var options = this.layout.options;
      this.transition({
        from: options.visibleStyle,
        to: options.hiddenStyle,
        // keep hidden stuff hidden
        isCleaning: true,
        onTransitionEnd: {
          opacity: function() {
            // check if still hidden
            // during transition, item may have been un-hidden
            if ( this.isHidden ) {
              this.css({ display: 'none' });
            }
          }
        }
      });
    };

    Item.prototype.destroy = function() {
      this.css({
        position: '',
        left: '',
        right: '',
        top: '',
        bottom: '',
        transition: '',
        transform: ''
      });
    };

    return Item;

  }

// -------------------------- transport -------------------------- //

  if ( typeof define === 'function' && define.amd ) {
    // AMD
    define( 'outlayer/item',[
        'eventEmitter/EventEmitter',
        'get-size/get-size',
        'get-style-property/get-style-property'
      ],
      outlayerItemDefinition );
  } else {
    // browser global
    window.Outlayer = {};
    window.Outlayer.Item = outlayerItemDefinition(
      window.EventEmitter,
      window.getSize,
      window.getStyleProperty
    );
  }

})( window );

/*!
 * Outlayer v1.2.0
 * the brains and guts of a layout library
 * MIT license
 */

( function( window ) {



// ----- vars ----- //

  var document = window.document;
  var console = window.console;
  var jQuery = window.jQuery;

  var noop = function() {};

// -------------------------- helpers -------------------------- //

// extend objects
  function extend( a, b ) {
    for ( var prop in b ) {
      a[ prop ] = b[ prop ];
    }
    return a;
  }


  var objToString = Object.prototype.toString;
  function isArray( obj ) {
    return objToString.call( obj ) === '[object Array]';
  }

// turn element or nodeList into an array
  function makeArray( obj ) {
    var ary = [];
    if ( isArray( obj ) ) {
      // use object if already an array
      ary = obj;
    } else if ( obj && typeof obj.length === 'number' ) {
      // convert nodeList to array
      for ( var i=0, len = obj.length; i < len; i++ ) {
        ary.push( obj[i] );
      }
    } else {
      // array of single index
      ary.push( obj );
    }
    return ary;
  }

// http://stackoverflow.com/a/384380/182183
  var isElement = ( typeof HTMLElement === 'object' ) ?
    function isElementDOM2( obj ) {
      return obj instanceof HTMLElement;
    } :
    function isElementQuirky( obj ) {
      return obj && typeof obj === 'object' &&
        obj.nodeType === 1 && typeof obj.nodeName === 'string';
    };

// index of helper cause IE8
  var indexOf = Array.prototype.indexOf ? function( ary, obj ) {
    return ary.indexOf( obj );
  } : function( ary, obj ) {
    for ( var i=0, len = ary.length; i < len; i++ ) {
      if ( ary[i] === obj ) {
        return i;
      }
    }
    return -1;
  };

  function removeFrom( obj, ary ) {
    var index = indexOf( ary, obj );
    if ( index !== -1 ) {
      ary.splice( index, 1 );
    }
  }

// http://jamesroberts.name/blog/2010/02/22/string-functions-for-javascript-trim-to-camel-case-to-dashed-and-to-underscore/
  function toDashed( str ) {
    return str.replace( /(.)([A-Z])/g, function( match, $1, $2 ) {
      return $1 + '-' + $2;
    }).toLowerCase();
  }


  function outlayerDefinition( eventie, docReady, EventEmitter, getSize, matchesSelector, Item ) {

// -------------------------- Outlayer -------------------------- //

// globally unique identifiers
    var GUID = 0;
// internal store of all Outlayer intances
    var instances = {};


    /**
     * @param {Element, String} element
     * @param {Object} options
     * @constructor
     */
    function Outlayer( element, options ) {
      // use element as selector string
      if ( typeof element === 'string' ) {
        element = document.querySelector( element );
      }

      // bail out if not proper element
      if ( !element || !isElement( element ) ) {
        if ( console ) {
          console.error( 'Bad ' + this.constructor.namespace + ' element: ' + element );
        }
        return;
      }

      this.element = element;

      // options
      this.options = extend( {}, this.constructor.defaults );
      this.option( options );

      // add id for Outlayer.getFromElement
      var id = ++GUID;
      this.element.outlayerGUID = id; // expando
      instances[ id ] = this; // associate via id

      // kick it off
      this._create();

      if ( this.options.isInitLayout ) {
        this.layout();
      }
    }

// settings are for internal use only
    Outlayer.namespace = 'outlayer';
    Outlayer.Item = Item;

// default options
    Outlayer.defaults = {
      containerStyle: {
        position: 'relative'
      },
      isInitLayout: true,
      isOriginLeft: true,
      isOriginTop: true,
      isResizeBound: true,
      isResizingContainer: true,
      // item options
      transitionDuration: 'http://www.wholefoodsmarket.com/sites/default/files/advagg_js/0.4s',
      hiddenStyle: {
        opacity: 0,
        transform: 'scale(0.001)'
      },
      visibleStyle: {
        opacity: 1,
        transform: 'scale(1)'
      }
    };

// inherit EventEmitter
    extend( Outlayer.prototype, EventEmitter.prototype );

    /**
     * set options
     * @param {Object} opts
     */
    Outlayer.prototype.option = function( opts ) {
      extend( this.options, opts );
    };

    Outlayer.prototype._create = function() {
      // get items from children
      this.reloadItems();
      // elements that affect layout, but are not laid out
      this.stamps = [];
      this.stamp( this.options.stamp );
      // set container style
      extend( this.element.style, this.options.containerStyle );

      // bind resize method
      if ( this.options.isResizeBound ) {
        this.bindResize();
      }
    };

// goes through all children again and gets bricks in proper order
    Outlayer.prototype.reloadItems = function() {
      // collection of item elements
      this.items = this._itemize( this.element.children );
    };


    /**
     * turn elements into Outlayer.Items to be used in layout
     * @param {Array or NodeList or HTMLElement} elems
     * @returns {Array} items - collection of new Outlayer Items
     */
    Outlayer.prototype._itemize = function( elems ) {

      var itemElems = this._filterFindItemElements( elems );
      var Item = this.constructor.Item;

      // create new Outlayer Items for collection
      var items = [];
      for ( var i=0, len = itemElems.length; i < len; i++ ) {
        var elem = itemElems[i];
        var item = new Item( elem, this );
        items.push( item );
      }

      return items;
    };

    /**
     * get item elements to be used in layout
     * @param {Array or NodeList or HTMLElement} elems
     * @returns {Array} items - item elements
     */
    Outlayer.prototype._filterFindItemElements = function( elems ) {
      // make array of elems
      elems = makeArray( elems );
      var itemSelector = this.options.itemSelector;
      var itemElems = [];

      for ( var i=0, len = elems.length; i < len; i++ ) {
        var elem = elems[i];
        // check that elem is an actual element
        if ( !isElement( elem ) ) {
          continue;
        }
        // filter & find items if we have an item selector
        if ( itemSelector ) {
          // filter siblings
          if ( matchesSelector( elem, itemSelector ) ) {
            itemElems.push( elem );
          }
          // find children
          var childElems = elem.querySelectorAll( itemSelector );
          // concat childElems to filterFound array
          for ( var j=0, jLen = childElems.length; j < jLen; j++ ) {
            itemElems.push( childElems[j] );
          }
        } else {
          itemElems.push( elem );
        }
      }

      return itemElems;
    };

    /**
     * getter method for getting item elements
     * @returns {Array} elems - collection of item elements
     */
    Outlayer.prototype.getItemElements = function() {
      var elems = [];
      for ( var i=0, len = this.items.length; i < len; i++ ) {
        elems.push( this.items[i].element );
      }
      return elems;
    };

// ----- init & layout ----- //

    /**
     * lays out all items
     */
    Outlayer.prototype.layout = function() {
      this._resetLayout();
      this._manageStamps();

      // don't animate first layout
      var isInstant = this.options.isLayoutInstant !== undefined ?
        this.options.isLayoutInstant : !this._isLayoutInited;
      this.layoutItems( this.items, isInstant );

      // flag for initalized
      this._isLayoutInited = true;
    };

// _init is alias for layout
    Outlayer.prototype._init = Outlayer.prototype.layout;

    /**
     * logic before any new layout
     */
    Outlayer.prototype._resetLayout = function() {
      this.getSize();
    };


    Outlayer.prototype.getSize = function() {
      this.size = getSize( this.element );
    };

    /**
     * get measurement from option, for columnWidth, rowHeight, gutter
     * if option is String -> get element from selector string, & get size of element
     * if option is Element -> get size of element
     * else use option as a number
     *
     * @param {String} measurement
     * @param {String} size - width or height
     * @private
     */
    Outlayer.prototype._getMeasurement = function( measurement, size ) {
      var option = this.options[ measurement ];
      var elem;
      if ( !option ) {
        // default to 0
        this[ measurement ] = 0;
      } else {
        // use option as an element
        if ( typeof option === 'string' ) {
          elem = this.element.querySelector( option );
        } else if ( isElement( option ) ) {
          elem = option;
        }
        // use size of element, if element
        this[ measurement ] = elem ? getSize( elem )[ size ] : option;
      }
    };

    /**
     * layout a collection of item elements
     * @api public
     */
    Outlayer.prototype.layoutItems = function( items, isInstant ) {
      items = this._getItemsForLayout( items );

      this._layoutItems( items, isInstant );

      this._postLayout();
    };

    /**
     * get the items to be laid out
     * you may want to skip over some items
     * @param {Array} items
     * @returns {Array} items
     */
    Outlayer.prototype._getItemsForLayout = function( items ) {
      var layoutItems = [];
      for ( var i=0, len = items.length; i < len; i++ ) {
        var item = items[i];
        if ( !item.isIgnored ) {
          layoutItems.push( item );
        }
      }
      return layoutItems;
    };

    /**
     * layout items
     * @param {Array} items
     * @param {Boolean} isInstant
     */
    Outlayer.prototype._layoutItems = function( items, isInstant ) {
      var _this = this;
      function onItemsLayout() {
        _this.emitEvent( 'layoutComplete', [ _this, items ] );
      }

      if ( !items || !items.length ) {
        // no items, emit event with empty array
        onItemsLayout();
        return;
      }

      // emit layoutComplete when done
      this._itemsOn( items, 'layout', onItemsLayout );

      var queue = [];

      for ( var i=0, len = items.length; i < len; i++ ) {
        var item = items[i];
        // get x/y object from method
        var position = this._getItemLayoutPosition( item );
        // enqueue
        position.item = item;
        position.isInstant = isInstant || item.isLayoutInstant;
        queue.push( position );
      }

      this._processLayoutQueue( queue );
    };

    /**
     * get item layout position
     * @param {Outlayer.Item} item
     * @returns {Object} x and y position
     */
    Outlayer.prototype._getItemLayoutPosition = function( /* item */ ) {
      return {
        x: 0,
        y: 0
      };
    };

    /**
     * iterate over array and position each item
     * Reason being - separating this logic prevents 'layout invalidation'
     * thx @paul_irish
     * @param {Array} queue
     */
    Outlayer.prototype._processLayoutQueue = function( queue ) {
      for ( var i=0, len = queue.length; i < len; i++ ) {
        var obj = queue[i];
        this._positionItem( obj.item, obj.x, obj.y, obj.isInstant );
      }
    };

    /**
     * Sets position of item in DOM
     * @param {Outlayer.Item} item
     * @param {Number} x - horizontal position
     * @param {Number} y - vertical position
     * @param {Boolean} isInstant - disables transitions
     */
    Outlayer.prototype._positionItem = function( item, x, y, isInstant ) {
      if ( isInstant ) {
        // if not transition, just set CSS
        item.goTo( x, y );
      } else {
        item.moveTo( x, y );
      }
    };

    /**
     * Any logic you want to do after each layout,
     * i.e. size the container
     */
    Outlayer.prototype._postLayout = function() {
      this.resizeContainer();
    };

    Outlayer.prototype.resizeContainer = function() {
      if ( !this.options.isResizingContainer ) {
        return;
      }
      var size = this._getContainerSize();
      if ( size ) {
        this._setContainerMeasure( size.width, true );
        this._setContainerMeasure( size.height, false );
      }
    };

    /**
     * Sets width or height of container if returned
     * @returns {Object} size
     *   @param {Number} width
     *   @param {Number} height
     */
    Outlayer.prototype._getContainerSize = noop;

    /**
     * @param {Number} measure - size of width or height
     * @param {Boolean} isWidth
     */
    Outlayer.prototype._setContainerMeasure = function( measure, isWidth ) {
      if ( measure === undefined ) {
        return;
      }

      var elemSize = this.size;
      // add padding and border width if border box
      if ( elemSize.isBorderBox ) {
        measure += isWidth ? elemSize.paddingLeft + elemSize.paddingRight +
        elemSize.borderLeftWidth + elemSize.borderRightWidth :
        elemSize.paddingBottom + elemSize.paddingTop +
        elemSize.borderTopWidth + elemSize.borderBottomWidth;
      }

      measure = Math.max( measure, 0 );
      this.element.style[ isWidth ? 'width' : 'height' ] = measure + 'px';
    };

    /**
     * trigger a callback for a collection of items events
     * @param {Array} items - Outlayer.Items
     * @param {String} eventName
     * @param {Function} callback
     */
    Outlayer.prototype._itemsOn = function( items, eventName, callback ) {
      var doneCount = 0;
      var count = items.length;
      // event callback
      var _this = this;
      function tick() {
        doneCount++;
        if ( doneCount === count ) {
          callback.call( _this );
        }
        return true; // bind once
      }
      // bind callback
      for ( var i=0, len = items.length; i < len; i++ ) {
        var item = items[i];
        item.on( eventName, tick );
      }
    };

// -------------------------- ignore & stamps -------------------------- //


    /**
     * keep item in collection, but do not lay it out
     * ignored items do not get skipped in layout
     * @param {Element} elem
     */
    Outlayer.prototype.ignore = function( elem ) {
      var item = this.getItem( elem );
      if ( item ) {
        item.isIgnored = true;
      }
    };

    /**
     * return item to layout collection
     * @param {Element} elem
     */
    Outlayer.prototype.unignore = function( elem ) {
      var item = this.getItem( elem );
      if ( item ) {
        delete item.isIgnored;
      }
    };

    /**
     * adds elements to stamps
     * @param {NodeList, Array, Element, or String} elems
     */
    Outlayer.prototype.stamp = function( elems ) {
      elems = this._find( elems );
      if ( !elems ) {
        return;
      }

      this.stamps = this.stamps.concat( elems );
      // ignore
      for ( var i=0, len = elems.length; i < len; i++ ) {
        var elem = elems[i];
        this.ignore( elem );
      }
    };

    /**
     * removes elements to stamps
     * @param {NodeList, Array, or Element} elems
     */
    Outlayer.prototype.unstamp = function( elems ) {
      elems = this._find( elems );
      if ( !elems ){
        return;
      }

      for ( var i=0, len = elems.length; i < len; i++ ) {
        var elem = elems[i];
        // filter out removed stamp elements
        removeFrom( elem, this.stamps );
        this.unignore( elem );
      }

    };

    /**
     * finds child elements
     * @param {NodeList, Array, Element, or String} elems
     * @returns {Array} elems
     */
    Outlayer.prototype._find = function( elems ) {
      if ( !elems ) {
        return;
      }
      // if string, use argument as selector string
      if ( typeof elems === 'string' ) {
        elems = this.element.querySelectorAll( elems );
      }
      elems = makeArray( elems );
      return elems;
    };

    Outlayer.prototype._manageStamps = function() {
      if ( !this.stamps || !this.stamps.length ) {
        return;
      }

      this._getBoundingRect();

      for ( var i=0, len = this.stamps.length; i < len; i++ ) {
        var stamp = this.stamps[i];
        this._manageStamp( stamp );
      }
    };

// update boundingLeft / Top
    Outlayer.prototype._getBoundingRect = function() {
      // get bounding rect for container element
      var boundingRect = this.element.getBoundingClientRect();
      var size = this.size;
      this._boundingRect = {
        left: boundingRect.left + size.paddingLeft + size.borderLeftWidth,
        top: boundingRect.top + size.paddingTop + size.borderTopWidth,
        right: boundingRect.right - ( size.paddingRight + size.borderRightWidth ),
        bottom: boundingRect.bottom - ( size.paddingBottom + size.borderBottomWidth )
      };
    };

    /**
     * @param {Element} stamp
     **/
    Outlayer.prototype._manageStamp = noop;

    /**
     * get x/y position of element relative to container element
     * @param {Element} elem
     * @returns {Object} offset - has left, top, right, bottom
     */
    Outlayer.prototype._getElementOffset = function( elem ) {
      var boundingRect = elem.getBoundingClientRect();
      var thisRect = this._boundingRect;
      var size = getSize( elem );
      var offset = {
        left: boundingRect.left - thisRect.left - size.marginLeft,
        top: boundingRect.top - thisRect.top - size.marginTop,
        right: thisRect.right - boundingRect.right - size.marginRight,
        bottom: thisRect.bottom - boundingRect.bottom - size.marginBottom
      };
      return offset;
    };

// -------------------------- resize -------------------------- //

// enable event handlers for listeners
// i.e. resize -> onresize
    Outlayer.prototype.handleEvent = function( event ) {
      var method = 'on' + event.type;
      if ( this[ method ] ) {
        this[ method ]( event );
      }
    };

    /**
     * Bind layout to window resizing
     */
    Outlayer.prototype.bindResize = function() {
      // bind just one listener
      if ( this.isResizeBound ) {
        return;
      }
      eventie.bind( window, 'resize', this );
      this.isResizeBound = true;
    };

    /**
     * Unbind layout to window resizing
     */
    Outlayer.prototype.unbindResize = function() {
      if ( this.isResizeBound ) {
        eventie.unbind( window, 'resize', this );
      }
      this.isResizeBound = false;
    };

// original debounce by John Hann
// http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/

// this fires every resize
    Outlayer.prototype.onresize = function() {
      if ( this.resizeTimeout ) {
        clearTimeout( this.resizeTimeout );
      }

      var _this = this;
      function delayed() {
        _this.resize();
        delete _this.resizeTimeout;
      }

      this.resizeTimeout = setTimeout( delayed, 100 );
    };

// debounced, layout on resize
    Outlayer.prototype.resize = function() {
      // don't trigger if size did not change
      // or if resize was unbound. See #9
      if ( !this.isResizeBound || !this.needsResizeLayout() ) {
        return;
      }

      this.layout();
    };

    /**
     * check if layout is needed post layout
     * @returns Boolean
     */
    Outlayer.prototype.needsResizeLayout = function() {
      var size = getSize( this.element );
      // check that this.size and size are there
      // IE8 triggers resize on body size change, so they might not be
      var hasSizes = this.size && size;
      return hasSizes && size.innerWidth !== this.size.innerWidth;
    };

// -------------------------- methods -------------------------- //

    /**
     * add items to Outlayer instance
     * @param {Array or NodeList or Element} elems
     * @returns {Array} items - Outlayer.Items
     **/
    Outlayer.prototype.addItems = function( elems ) {
      var items = this._itemize( elems );
      // add items to collection
      if ( items.length ) {
        this.items = this.items.concat( items );
      }
      return items;
    };

    /**
     * Layout newly-appended item elements
     * @param {Array or NodeList or Element} elems
     */
    Outlayer.prototype.appended = function( elems ) {
      var items = this.addItems( elems );
      if ( !items.length ) {
        return;
      }
      // layout and reveal just the new items
      this.layoutItems( items, true );
      this.reveal( items );
    };

    /**
     * Layout prepended elements
     * @param {Array or NodeList or Element} elems
     */
    Outlayer.prototype.prepended = function( elems ) {
      var items = this._itemize( elems );
      if ( !items.length ) {
        return;
      }
      // add items to beginning of collection
      var previousItems = this.items.slice(0);
      this.items = items.concat( previousItems );
      // start new layout
      this._resetLayout();
      this._manageStamps();
      // layout new stuff without transition
      this.layoutItems( items, true );
      this.reveal( items );
      // layout previous items
      this.layoutItems( previousItems );
    };

    /**
     * reveal a collection of items
     * @param {Array of Outlayer.Items} items
     */
    Outlayer.prototype.reveal = function( items ) {
      var len = items && items.length;
      if ( !len ) {
        return;
      }
      for ( var i=0; i < len; i++ ) {
        var item = items[i];
        item.reveal();
      }
    };

    /**
     * hide a collection of items
     * @param {Array of Outlayer.Items} items
     */
    Outlayer.prototype.hide = function( items ) {
      var len = items && items.length;
      if ( !len ) {
        return;
      }
      for ( var i=0; i < len; i++ ) {
        var item = items[i];
        item.hide();
      }
    };

    /**
     * get Outlayer.Item, given an Element
     * @param {Element} elem
     * @param {Function} callback
     * @returns {Outlayer.Item} item
     */
    Outlayer.prototype.getItem = function( elem ) {
      // loop through items to get the one that matches
      for ( var i=0, len = this.items.length; i < len; i++ ) {
        var item = this.items[i];
        if ( item.element === elem ) {
          // return item
          return item;
        }
      }
    };

    /**
     * get collection of Outlayer.Items, given Elements
     * @param {Array} elems
     * @returns {Array} items - Outlayer.Items
     */
    Outlayer.prototype.getItems = function( elems ) {
      if ( !elems || !elems.length ) {
        return;
      }
      var items = [];
      for ( var i=0, len = elems.length; i < len; i++ ) {
        var elem = elems[i];
        var item = this.getItem( elem );
        if ( item ) {
          items.push( item );
        }
      }

      return items;
    };

    /**
     * remove element(s) from instance and DOM
     * @param {Array or NodeList or Element} elems
     */
    Outlayer.prototype.remove = function( elems ) {
      elems = makeArray( elems );

      var removeItems = this.getItems( elems );
      // bail if no items to remove
      if ( !removeItems || !removeItems.length ) {
        return;
      }

      this._itemsOn( removeItems, 'remove', function() {
        this.emitEvent( 'removeComplete', [ this, removeItems ] );
      });

      for ( var i=0, len = removeItems.length; i < len; i++ ) {
        var item = removeItems[i];
        item.remove();
        // remove item from collection
        removeFrom( item, this.items );
      }
    };

// ----- destroy ----- //

// remove and disable Outlayer instance
    Outlayer.prototype.destroy = function() {
      // clean up dynamic styles
      var style = this.element.style;
      style.height = '';
      style.position = '';
      style.width = '';
      // destroy items
      for ( var i=0, len = this.items.length; i < len; i++ ) {
        var item = this.items[i];
        item.destroy();
      }

      this.unbindResize();

      delete this.element.outlayerGUID;
      // remove data for jQuery
      if ( jQuery ) {
        jQuery.removeData( this.element, this.constructor.namespace );
      }

    };

// -------------------------- data -------------------------- //

    /**
     * get Outlayer instance from element
     * @param {Element} elem
     * @returns {Outlayer}
     */
    Outlayer.data = function( elem ) {
      var id = elem && elem.outlayerGUID;
      return id && instances[ id ];
    };


// -------------------------- create Outlayer class -------------------------- //

    /**
     * create a layout class
     * @param {String} namespace
     */
    Outlayer.create = function( namespace, options ) {
      // sub-class Outlayer
      function Layout() {
        Outlayer.apply( this, arguments );
      }
      // inherit Outlayer prototype, use Object.create if there
      if ( Object.create ) {
        Layout.prototype = Object.create( Outlayer.prototype );
      } else {
        extend( Layout.prototype, Outlayer.prototype );
      }
      // set contructor, used for namespace and Item
      Layout.prototype.constructor = Layout;

      Layout.defaults = extend( {}, Outlayer.defaults );
      // apply new options
      extend( Layout.defaults, options );
      // keep prototype.settings for backwards compatibility (Packery v1.2.0)
      Layout.prototype.settings = {};

      Layout.namespace = namespace;

      Layout.data = Outlayer.data;

      // sub-class Item
      Layout.Item = function LayoutItem() {
        Item.apply( this, arguments );
      };

      Layout.Item.prototype = new Item();

      // -------------------------- declarative -------------------------- //

      /**
       * allow user to initialize Outlayer via .js-namespace class
       * options are parsed from data-namespace-option attribute
       */
      docReady( function() {
        var dashedNamespace = toDashed( namespace );
        var elems = document.querySelectorAll( '.js-' + dashedNamespace );
        var dataAttr = 'data-' + dashedNamespace + '-options';

        for ( var i=0, len = elems.length; i < len; i++ ) {
          var elem = elems[i];
          var attr = elem.getAttribute( dataAttr );
          var options;
          try {
            options = attr && JSON.parse( attr );
          } catch ( error ) {
            // log error, do not initialize
            if ( console ) {
              console.error( 'Error parsing ' + dataAttr + ' on ' +
              elem.nodeName.toLowerCase() + ( elem.id ? '#' + elem.id : '' ) + ': ' +
              error );
            }
            continue;
          }
          // initialize
          var instance = new Layout( elem, options );
          // make available via $().data('layoutname')
          if ( jQuery ) {
            jQuery.data( elem, namespace, instance );
          }
        }
      });

      // -------------------------- jQuery bridge -------------------------- //

      // make into jQuery plugin
      if ( jQuery && jQuery.bridget ) {
        jQuery.bridget( namespace, Layout );
      }

      return Layout;
    };

// ----- fin ----- //

// back in global
    Outlayer.Item = Item;

    return Outlayer;

  }

// -------------------------- transport -------------------------- //

  if ( typeof define === 'function' && define.amd ) {
    // AMD
    define( 'outlayer/outlayer',[
        'eventie/eventie',
        'doc-ready/doc-ready',
        'eventEmitter/EventEmitter',
        'get-size/get-size',
        'matches-selector/matches-selector',
        './item'
      ],
      outlayerDefinition );
  } else {
    // browser global
    window.Outlayer = outlayerDefinition(
      window.eventie,
      window.docReady,
      window.EventEmitter,
      window.getSize,
      window.matchesSelector,
      window.Outlayer.Item
    );
  }

})( window );

/**
 * Isotope Item
 **/

( function( window ) {



// -------------------------- Item -------------------------- //

  function itemDefinition( Outlayer ) {

// sub-class Outlayer Item
    function Item() {
      Outlayer.Item.apply( this, arguments );
    }

    Item.prototype = new Outlayer.Item();

    Item.prototype._create = function() {
      // assign id, used for original-order sorting
      this.id = this.layout.itemGUID++;
      Outlayer.Item.prototype._create.call( this );
      this.sortData = {};
    };

    Item.prototype.updateSortData = function() {
      if ( this.isIgnored ) {
        return;
      }
      // default sorters
      this.sortData.id = this.id;
      // for backward compatibility
      this.sortData['original-order'] = this.id;
      this.sortData.random = Math.random();
      // go thru getSortData obj and apply the sorters
      var getSortData = this.layout.options.getSortData;
      var sorters = this.layout._sorters;
      for ( var key in getSortData ) {
        var sorter = sorters[ key ];
        this.sortData[ key ] = sorter( this.element, this );
      }
    };

    var _destroy = Item.prototype.destroy;
    Item.prototype.destroy = function() {
      // call super
      _destroy.apply( this, arguments );
      // reset display, #741
      this.css({
        display: ''
      });
    };

    return Item;

  }

// -------------------------- transport -------------------------- //

  if ( typeof define === 'function' && define.amd ) {
    // AMD
    define( 'isotope/js/item',[
        'outlayer/outlayer'
      ],
      itemDefinition );
  } else {
    // browser global
    window.Isotope = window.Isotope || {};
    window.Isotope.Item = itemDefinition(
      window.Outlayer
    );
  }

})( window );

( function( window ) {



// --------------------------  -------------------------- //

  function layoutModeDefinition( getSize, Outlayer ) {

    // layout mode class
    function LayoutMode( isotope ) {
      this.isotope = isotope;
      // link properties
      if ( isotope ) {
        this.options = isotope.options[ this.namespace ];
        this.element = isotope.element;
        this.items = isotope.filteredItems;
        this.size = isotope.size;
      }
    }

    /**
     * some methods should just defer to default Outlayer method
     * and reference the Isotope instance as `this`
     **/
    ( function() {
      var facadeMethods = [
        '_resetLayout',
        '_getItemLayoutPosition',
        '_manageStamp',
        '_getContainerSize',
        '_getElementOffset',
        'needsResizeLayout'
      ];

      for ( var i=0, len = facadeMethods.length; i < len; i++ ) {
        var methodName = facadeMethods[i];
        LayoutMode.prototype[ methodName ] = getOutlayerMethod( methodName );
      }

      function getOutlayerMethod( methodName ) {
        return function() {
          return Outlayer.prototype[ methodName ].apply( this.isotope, arguments );
        };
      }
    })();

    // -----  ----- //

    // for horizontal layout modes, check vertical size
    LayoutMode.prototype.needsVerticalResizeLayout = function() {
      // don't trigger if size did not change
      var size = getSize( this.isotope.element );
      // check that this.size and size are there
      // IE8 triggers resize on body size change, so they might not be
      var hasSizes = this.isotope.size && size;
      return hasSizes && size.innerHeight !== this.isotope.size.innerHeight;
    };

    // ----- measurements ----- //

    LayoutMode.prototype._getMeasurement = function() {
      this.isotope._getMeasurement.apply( this, arguments );
    };

    LayoutMode.prototype.getColumnWidth = function() {
      this.getSegmentSize( 'column', 'Width' );
    };

    LayoutMode.prototype.getRowHeight = function() {
      this.getSegmentSize( 'row', 'Height' );
    };

    /**
     * get columnWidth or rowHeight
     * segment: 'column' or 'row'
     * size 'Width' or 'Height'
     **/
    LayoutMode.prototype.getSegmentSize = function( segment, size ) {
      var segmentName = segment + size;
      var outerSize = 'outer' + size;
      // columnWidth / outerWidth // rowHeight / outerHeight
      this._getMeasurement( segmentName, outerSize );
      // got rowHeight or columnWidth, we can chill
      if ( this[ segmentName ] ) {
        return;
      }
      // fall back to item of first element
      var firstItemSize = this.getFirstItemSize();
      this[ segmentName ] = firstItemSize && firstItemSize[ outerSize ] ||
        // or size of container
      this.isotope.size[ 'inner' + size ];
    };

    LayoutMode.prototype.getFirstItemSize = function() {
      var firstItem = this.isotope.filteredItems[0];
      return firstItem && firstItem.element && getSize( firstItem.element );
    };

    // ----- methods that should reference isotope ----- //

    LayoutMode.prototype.layout = function() {
      this.isotope.layout.apply( this.isotope, arguments );
    };

    LayoutMode.prototype.getSize = function() {
      this.isotope.getSize();
      this.size = this.isotope.size;
    };

    // -------------------------- create -------------------------- //

    LayoutMode.modes = {};

    LayoutMode.create = function( namespace, options ) {

      function Mode() {
        LayoutMode.apply( this, arguments );
      }

      Mode.prototype = new LayoutMode();

      // default options
      if ( options ) {
        Mode.options = options;
      }

      Mode.prototype.namespace = namespace;
      // register in Isotope
      LayoutMode.modes[ namespace ] = Mode;

      return Mode;
    };


    return LayoutMode;

  }

  if ( typeof define === 'function' && define.amd ) {
    // AMD
    define( 'isotope/js/layout-mode',[
        'get-size/get-size',
        'outlayer/outlayer'
      ],
      layoutModeDefinition );
  } else {
    // browser global
    window.Isotope = window.Isotope || {};
    window.Isotope.LayoutMode = layoutModeDefinition(
      window.getSize,
      window.Outlayer
    );
  }


})( window );

/*!
 * Masonry v3.1.5
 * Cascading grid layout library
 * http://masonry.desandro.com
 * MIT License
 * by David DeSandro
 */

( function( window ) {



// -------------------------- helpers -------------------------- //

  var indexOf = Array.prototype.indexOf ?
    function( items, value ) {
      return items.indexOf( value );
    } :
    function ( items, value ) {
      for ( var i=0, len = items.length; i < len; i++ ) {
        var item = items[i];
        if ( item === value ) {
          return i;
        }
      }
      return -1;
    };

// -------------------------- masonryDefinition -------------------------- //

// used for AMD definition and requires
  function masonryDefinition( Outlayer, getSize ) {
    // create an Outlayer layout class
    var Masonry = Outlayer.create('masonry');

    Masonry.prototype._resetLayout = function() {
      this.getSize();
      this._getMeasurement( 'columnWidth', 'outerWidth' );
      this._getMeasurement( 'gutter', 'outerWidth' );
      this.measureColumns();

      // reset column Y
      var i = this.cols;
      this.colYs = [];
      while (i--) {
        this.colYs.push( 0 );
      }

      this.maxY = 0;
    };

    Masonry.prototype.measureColumns = function() {
      this.getContainerWidth();
      // if columnWidth is 0, default to outerWidth of first item
      if ( !this.columnWidth ) {
        var firstItem = this.items[0];
        var firstItemElem = firstItem && firstItem.element;
        // columnWidth fall back to item of first element
        this.columnWidth = firstItemElem && getSize( firstItemElem ).outerWidth ||
          // if first elem has no width, default to size of container
        this.containerWidth;
      }

      this.columnWidth += this.gutter;

      this.cols = Math.floor( ( this.containerWidth + this.gutter ) / this.columnWidth );
      this.cols = Math.max( this.cols, 1 );
    };

    Masonry.prototype.getContainerWidth = function() {
      // container is parent if fit width
      var container = this.options.isFitWidth ? this.element.parentNode : this.element;
      // check that this.size and size are there
      // IE8 triggers resize on body size change, so they might not be
      var size = getSize( container );
      this.containerWidth = size && size.innerWidth;
    };

    Masonry.prototype._getItemLayoutPosition = function( item ) {
      item.getSize();
      // how many columns does this brick span
      var remainder = item.size.outerWidth % this.columnWidth;
      var mathMethod = remainder && remainder < 1 ? 'round' : 'ceil';
      // round if off by 1 pixel, otherwise use ceil
      var colSpan = Math[ mathMethod ]( item.size.outerWidth / this.columnWidth );
      colSpan = Math.min( colSpan, this.cols );

      var colGroup = this._getColGroup( colSpan );
      // get the minimum Y value from the columns
      var minimumY = Math.min.apply( Math, colGroup );
      var shortColIndex = indexOf( colGroup, minimumY );

      // position the brick
      var position = {
        x: this.columnWidth * shortColIndex,
        y: minimumY
      };

      // apply setHeight to necessary columns
      var setHeight = minimumY + item.size.outerHeight;
      var setSpan = this.cols + 1 - colGroup.length;
      for ( var i = 0; i < setSpan; i++ ) {
        this.colYs[ shortColIndex + i ] = setHeight;
      }

      return position;
    };

    /**
     * @param {Number} colSpan - number of columns the element spans
     * @returns {Array} colGroup
     */
    Masonry.prototype._getColGroup = function( colSpan ) {
      if ( colSpan < 2 ) {
        // if brick spans only one column, use all the column Ys
        return this.colYs;
      }

      var colGroup = [];
      // how many different places could this brick fit horizontally
      var groupCount = this.cols + 1 - colSpan;
      // for each group potential horizontal position
      for ( var i = 0; i < groupCount; i++ ) {
        // make an array of colY values for that one group
        var groupColYs = this.colYs.slice( i, i + colSpan );
        // and get the max value of the array
        colGroup[i] = Math.max.apply( Math, groupColYs );
      }
      return colGroup;
    };

    Masonry.prototype._manageStamp = function( stamp ) {
      var stampSize = getSize( stamp );
      var offset = this._getElementOffset( stamp );
      // get the columns that this stamp affects
      var firstX = this.options.isOriginLeft ? offset.left : offset.right;
      var lastX = firstX + stampSize.outerWidth;
      var firstCol = Math.floor( firstX / this.columnWidth );
      firstCol = Math.max( 0, firstCol );
      var lastCol = Math.floor( lastX / this.columnWidth );
      // lastCol should not go over if multiple of columnWidth #425
      lastCol -= lastX % this.columnWidth ? 0 : 1;
      lastCol = Math.min( this.cols - 1, lastCol );
      // set colYs to bottom of the stamp
      var stampMaxY = ( this.options.isOriginTop ? offset.top : offset.bottom ) +
        stampSize.outerHeight;
      for ( var i = firstCol; i <= lastCol; i++ ) {
        this.colYs[i] = Math.max( stampMaxY, this.colYs[i] );
      }
    };

    Masonry.prototype._getContainerSize = function() {
      this.maxY = Math.max.apply( Math, this.colYs );
      var size = {
        height: this.maxY
      };

      if ( this.options.isFitWidth ) {
        size.width = this._getContainerFitWidth();
      }

      return size;
    };

    Masonry.prototype._getContainerFitWidth = function() {
      var unusedCols = 0;
      // count unused columns
      var i = this.cols;
      while ( --i ) {
        if ( this.colYs[i] !== 0 ) {
          break;
        }
        unusedCols++;
      }
      // fit container to columns that have been used
      return ( this.cols - unusedCols ) * this.columnWidth - this.gutter;
    };

    Masonry.prototype.needsResizeLayout = function() {
      var previousWidth = this.containerWidth;
      this.getContainerWidth();
      return previousWidth !== this.containerWidth;
    };

    return Masonry;
  }

// -------------------------- transport -------------------------- //

  if ( typeof define === 'function' && define.amd ) {
    // AMD
    define( 'masonry/masonry',[
        'outlayer/outlayer',
        'get-size/get-size'
      ],
      masonryDefinition );
  } else {
    // browser global
    window.Masonry = masonryDefinition(
      window.Outlayer,
      window.getSize
    );
  }

})( window );

/*!
 * Masonry layout mode
 * sub-classes Masonry
 * http://masonry.desandro.com
 */

( function( window ) {



// -------------------------- helpers -------------------------- //

// extend objects
  function extend( a, b ) {
    for ( var prop in b ) {
      a[ prop ] = b[ prop ];
    }
    return a;
  }

// -------------------------- masonryDefinition -------------------------- //

// used for AMD definition and requires
  function masonryDefinition( LayoutMode, Masonry ) {
    // create an Outlayer layout class
    var MasonryMode = LayoutMode.create('masonry');

    // save on to these methods
    var _getElementOffset = MasonryMode.prototype._getElementOffset;
    var layout = MasonryMode.prototype.layout;
    var _getMeasurement = MasonryMode.prototype._getMeasurement;

    // sub-class Masonry
    extend( MasonryMode.prototype, Masonry.prototype );

    // set back, as it was overwritten by Masonry
    MasonryMode.prototype._getElementOffset = _getElementOffset;
    MasonryMode.prototype.layout = layout;
    MasonryMode.prototype._getMeasurement = _getMeasurement;

    var measureColumns = MasonryMode.prototype.measureColumns;
    MasonryMode.prototype.measureColumns = function() {
      // set items, used if measuring first item
      this.items = this.isotope.filteredItems;
      measureColumns.call( this );
    };

    // HACK copy over isOriginLeft/Top options
    var _manageStamp = MasonryMode.prototype._manageStamp;
    MasonryMode.prototype._manageStamp = function() {
      this.options.isOriginLeft = this.isotope.options.isOriginLeft;
      this.options.isOriginTop = this.isotope.options.isOriginTop;
      _manageStamp.apply( this, arguments );
    };

    return MasonryMode;
  }

// -------------------------- transport -------------------------- //

  if ( typeof define === 'function' && define.amd ) {
    // AMD
    define( 'isotope/js/layout-modes/masonry',[
        '../layout-mode',
        'masonry/masonry'
      ],
      masonryDefinition );
  } else {
    // browser global
    masonryDefinition(
      window.Isotope.LayoutMode,
      window.Masonry
    );
  }

})( window );

( function( window ) {



  function fitRowsDefinition( LayoutMode ) {

    var FitRows = LayoutMode.create('fitRows');

    FitRows.prototype._resetLayout = function() {
      this.x = 0;
      this.y = 0;
      this.maxY = 0;
    };

    FitRows.prototype._getItemLayoutPosition = function( item ) {
      item.getSize();

      // if this element cannot fit in the current row
      if ( this.x !== 0 && item.size.outerWidth + this.x > this.isotope.size.innerWidth ) {
        this.x = 0;
        this.y = this.maxY;
      }

      var position = {
        x: this.x,
        y: this.y
      };

      this.maxY = Math.max( this.maxY, this.y + item.size.outerHeight );
      this.x += item.size.outerWidth;

      return position;
    };

    FitRows.prototype._getContainerSize = function() {
      return { height: this.maxY };
    };

    return FitRows;

  }

  if ( typeof define === 'function' && define.amd ) {
    // AMD
    define( 'isotope/js/layout-modes/fit-rows',[
        '../layout-mode'
      ],
      fitRowsDefinition );
  } else {
    // browser global
    fitRowsDefinition(
      window.Isotope.LayoutMode
    );
  }

})( window );

( function( window ) {



  function verticalDefinition( LayoutMode ) {

    var Vertical = LayoutMode.create( 'vertical', {
      horizontalAlignment: 0
    });

    Vertical.prototype._resetLayout = function() {
      this.y = 0;
    };

    Vertical.prototype._getItemLayoutPosition = function( item ) {
      item.getSize();
      var x = ( this.isotope.size.innerWidth - item.size.outerWidth ) *
        this.options.horizontalAlignment;
      var y = this.y;
      this.y += item.size.outerHeight;
      return { x: x, y: y };
    };

    Vertical.prototype._getContainerSize = function() {
      return { height: this.y };
    };

    return Vertical;

  }

  if ( typeof define === 'function' && define.amd ) {
    // AMD
    define( 'isotope/js/layout-modes/vertical',[
        '../layout-mode'
      ],
      verticalDefinition );
  } else {
    // browser global
    verticalDefinition(
      window.Isotope.LayoutMode
    );
  }

})( window );

/*!
 * Isotope v2.0.1
 * Filter & sort magical layouts
 * http://isotope.metafizzy.co
 */

( function( window ) {



// -------------------------- vars -------------------------- //

  var jQuery = window.jQuery;

// -------------------------- helpers -------------------------- //

// extend objects
  function extend( a, b ) {
    for ( var prop in b ) {
      a[ prop ] = b[ prop ];
    }
    return a;
  }

  var trim = String.prototype.trim ?
    function( str ) {
      return str.trim();
    } :
    function( str ) {
      return str.replace( /^\s+|\s+$/g, '' );
    };

  var docElem = document.documentElement;

  var getText = docElem.textContent ?
    function( elem ) {
      return elem.textContent;
    } :
    function( elem ) {
      return elem.innerText;
    };

  var objToString = Object.prototype.toString;
  function isArray( obj ) {
    return objToString.call( obj ) === '[object Array]';
  }

// index of helper cause IE8
  var indexOf = Array.prototype.indexOf ? function( ary, obj ) {
    return ary.indexOf( obj );
  } : function( ary, obj ) {
    for ( var i=0, len = ary.length; i < len; i++ ) {
      if ( ary[i] === obj ) {
        return i;
      }
    }
    return -1;
  };

// turn element or nodeList into an array
  function makeArray( obj ) {
    var ary = [];
    if ( isArray( obj ) ) {
      // use object if already an array
      ary = obj;
    } else if ( obj && typeof obj.length === 'number' ) {
      // convert nodeList to array
      for ( var i=0, len = obj.length; i < len; i++ ) {
        ary.push( obj[i] );
      }
    } else {
      // array of single index
      ary.push( obj );
    }
    return ary;
  }

  function removeFrom( obj, ary ) {
    var index = indexOf( ary, obj );
    if ( index !== -1 ) {
      ary.splice( index, 1 );
    }
  }

// -------------------------- isotopeDefinition -------------------------- //

// used for AMD definition and requires
  function isotopeDefinition( Outlayer, getSize, matchesSelector, Item, LayoutMode ) {
    // create an Outlayer layout class
    var Isotope = Outlayer.create( 'isotope', {
      layoutMode: "masonry",
      isJQueryFiltering: true,
      sortAscending: true
    });

    Isotope.Item = Item;
    Isotope.LayoutMode = LayoutMode;

    Isotope.prototype._create = function() {
      this.itemGUID = 0;
      // functions that sort items
      this._sorters = {};
      this._getSorters();
      // call super
      Outlayer.prototype._create.call( this );

      // create layout modes
      this.modes = {};
      // start filteredItems with all items
      this.filteredItems = this.items;
      // keep of track of sortBys
      this.sortHistory = [ 'original-order' ];
      // create from registered layout modes
      for ( var name in LayoutMode.modes ) {
        this._initLayoutMode( name );
      }
    };

    Isotope.prototype.reloadItems = function() {
      // reset item ID counter
      this.itemGUID = 0;
      // call super
      Outlayer.prototype.reloadItems.call( this );
    };

    Isotope.prototype._itemize = function() {
      var items = Outlayer.prototype._itemize.apply( this, arguments );
      // assign ID for original-order
      for ( var i=0, len = items.length; i < len; i++ ) {
        var item = items[i];
        item.id = this.itemGUID++;
      }
      this._updateItemsSortData( items );
      return items;
    };


    // -------------------------- layout -------------------------- //

    Isotope.prototype._initLayoutMode = function( name ) {
      var Mode = LayoutMode.modes[ name ];
      // set mode options
      // HACK extend initial options, back-fill in default options
      var initialOpts = this.options[ name ] || {};
      this.options[ name ] = Mode.options ?
        extend( Mode.options, initialOpts ) : initialOpts;
      // init layout mode instance
      this.modes[ name ] = new Mode( this );
    };


    Isotope.prototype.layout = function() {
      // if first time doing layout, do all magic
      if ( !this._isLayoutInited && this.options.isInitLayout ) {
        this.arrange();
        return;
      }
      this._layout();
    };

    // private method to be used in layout() & magic()
    Isotope.prototype._layout = function() {
      // don't animate first layout
      var isInstant = this._getIsInstant();
      // layout flow
      this._resetLayout();
      this._manageStamps();
      this.layoutItems( this.filteredItems, isInstant );

      // flag for initalized
      this._isLayoutInited = true;
    };

    // filter + sort + layout
    Isotope.prototype.arrange = function( opts ) {
      // set any options pass
      this.option( opts );
      this._getIsInstant();
      // filter, sort, and layout
      this.filteredItems = this._filter( this.items );
      this._sort();
      this._layout();
    };
    // alias to _init for main plugin method
    Isotope.prototype._init = Isotope.prototype.arrange;

    // HACK
    // Don't animate/transition first layout
    // Or don't animate/transition other layouts
    Isotope.prototype._getIsInstant = function() {
      var isInstant = this.options.isLayoutInstant !== undefined ?
        this.options.isLayoutInstant : !this._isLayoutInited;
      this._isInstant = isInstant;
      return isInstant;
    };

    // -------------------------- filter -------------------------- //

    Isotope.prototype._filter = function( items ) {
      var filter = this.options.filter;
      filter = filter || '*';
      var matches = [];
      var hiddenMatched = [];
      var visibleUnmatched = [];

      var test = this._getFilterTest( filter );

      // test each item
      for ( var i=0, len = items.length; i < len; i++ ) {
        var item = items[i];
        if ( item.isIgnored ) {
          continue;
        }
        // add item to either matched or unmatched group
        var isMatched = test( item );
        // item.isFilterMatched = isMatched;
        // add to matches if its a match
        if ( isMatched ) {
          matches.push( item );
        }
        // add to additional group if item needs to be hidden or revealed
        if ( isMatched && item.isHidden ) {
          hiddenMatched.push( item );
        } else if ( !isMatched && !item.isHidden ) {
          visibleUnmatched.push( item );
        }
      }

      var _this = this;
      function hideReveal() {
        _this.reveal( hiddenMatched );
        _this.hide( visibleUnmatched );
      }

      if ( this._isInstant ) {
        this._noTransition( hideReveal );
      } else {
        hideReveal();
      }

      return matches;
    };

    // get a jQuery, function, or a matchesSelector test given the filter
    Isotope.prototype._getFilterTest = function( filter ) {
      if ( jQuery && this.options.isJQueryFiltering ) {
        // use jQuery
        return function( item ) {
          return jQuery( item.element ).is( filter );
        };
      }
      if ( typeof filter === 'function' ) {
        // use filter as function
        return function( item ) {
          return filter( item.element );
        };
      }
      // default, use filter as selector string
      return function( item ) {
        return matchesSelector( item.element, filter );
      };
    };

    // -------------------------- sorting -------------------------- //

    /**
     * @params {Array} elems
     * @public
     */
    Isotope.prototype.updateSortData = function( elems ) {
      this._getSorters();
      // update item sort data
      // default to all items if none are passed in
      elems = makeArray( elems );
      var items = this.getItems( elems );
      // if no items found, update all items
      items = items.length ? items : this.items;
      this._updateItemsSortData( items );
    };

    Isotope.prototype._getSorters = function() {
      var getSortData = this.options.getSortData;
      for ( var key in getSortData ) {
        var sorter = getSortData[ key ];
        this._sorters[ key ] = mungeSorter( sorter );
      }
    };

    /**
     * @params {Array} items - of Isotope.Items
     * @private
     */
    Isotope.prototype._updateItemsSortData = function( items ) {
      for ( var i=0, len = items.length; i < len; i++ ) {
        var item = items[i];
        item.updateSortData();
      }
    };

    // ----- munge sorter ----- //

    // encapsulate this, as we just need mungeSorter
    // other functions in here are just for munging
    var mungeSorter = ( function() {
      // add a magic layer to sorters for convienent shorthands
      // `.foo-bar` will use the text of .foo-bar querySelector
      // `[foo-bar]` will use attribute
      // you can also add parser
      // `.foo-bar parseInt` will parse that as a number
      function mungeSorter( sorter ) {
        // if not a string, return function or whatever it is
        if ( typeof sorter !== 'string' ) {
          return sorter;
        }
        // parse the sorter string
        var args = trim( sorter ).split(' ');
        var query = args[0];
        // check if query looks like [an-attribute]
        var attrMatch = query.match( /^\[(.+)\]$/ );
        var attr = attrMatch && attrMatch[1];
        var getValue = getValueGetter( attr, query );
        // use second argument as a parser
        var parser = Isotope.sortDataParsers[ args[1] ];
        // parse the value, if there was a parser
        sorter = parser ? function( elem ) {
          return elem && parser( getValue( elem ) );
        } :
          // otherwise just return value
          function( elem ) {
            return elem && getValue( elem );
          };

        return sorter;
      }

      // get an attribute getter, or get text of the querySelector
      function getValueGetter( attr, query ) {
        var getValue;
        // if query looks like [foo-bar], get attribute
        if ( attr ) {
          getValue = function( elem ) {
            return elem.getAttribute( attr );
          };
        } else {
          // otherwise, assume its a querySelector, and get its text
          getValue = function( elem ) {
            var child = elem.querySelector( query );
            return child && getText( child );
          };
        }
        return getValue;
      }

      return mungeSorter;
    })();

    // parsers used in getSortData shortcut strings
    Isotope.sortDataParsers = {
      'parseInt': function( val ) {
        return parseInt( val, 10 );
      },
      'parseFloat': function( val ) {
        return parseFloat( val );
      }
    };

    // ----- sort method ----- //

    // sort filteredItem order
    Isotope.prototype._sort = function() {
      var sortByOpt = this.options.sortBy;
      if ( !sortByOpt ) {
        return;
      }
      // concat all sortBy and sortHistory
      var sortBys = [].concat.apply( sortByOpt, this.sortHistory );
      // sort magic
      var itemSorter = getItemSorter( sortBys, this.options.sortAscending );
      this.filteredItems.sort( itemSorter );
      // keep track of sortBy History
      if ( sortByOpt !== this.sortHistory[0] ) {
        // add to front, oldest goes in last
        this.sortHistory.unshift( sortByOpt );
      }
    };

    // returns a function used for sorting
    function getItemSorter( sortBys, sortAsc ) {
      return function sorter( itemA, itemB ) {
        // cycle through all sortKeys
        for ( var i = 0, len = sortBys.length; i < len; i++ ) {
          var sortBy = sortBys[i];
          var a = itemA.sortData[ sortBy ];
          var b = itemB.sortData[ sortBy ];
          if ( a > b || a < b ) {
            // if sortAsc is an object, use the value given the sortBy key
            var isAscending = sortAsc[ sortBy ] !== undefined ? sortAsc[ sortBy ] : sortAsc;
            var direction = isAscending ? 1 : -1;
            return ( a > b ? 1 : -1 ) * direction;
          }
        }
        return 0;
      };
    }

    // -------------------------- methods -------------------------- //

    // get layout mode
    Isotope.prototype._mode = function() {
      var layoutMode = this.options.layoutMode;
      var mode = this.modes[ layoutMode ];
      if ( !mode ) {
        // TODO console.error
        throw new Error( 'No layout mode: ' + layoutMode );
      }
      // HACK sync mode's options
      // any options set after init for layout mode need to be synced
      mode.options = this.options[ layoutMode ];
      return mode;
    };

    Isotope.prototype._resetLayout = function() {
      // trigger original reset layout
      Outlayer.prototype._resetLayout.call( this );
      this._mode()._resetLayout();
    };

    Isotope.prototype._getItemLayoutPosition = function( item  ) {
      return this._mode()._getItemLayoutPosition( item );
    };

    Isotope.prototype._manageStamp = function( stamp ) {
      this._mode()._manageStamp( stamp );
    };

    Isotope.prototype._getContainerSize = function() {
      return this._mode()._getContainerSize();
    };

    Isotope.prototype.needsResizeLayout = function() {
      return this._mode().needsResizeLayout();
    };

    // -------------------------- adding & removing -------------------------- //

    // HEADS UP overwrites default Outlayer appended
    Isotope.prototype.appended = function( elems ) {
      var items = this.addItems( elems );
      if ( !items.length ) {
        return;
      }
      var filteredItems = this._filterRevealAdded( items );
      // add to filteredItems
      this.filteredItems = this.filteredItems.concat( filteredItems );
    };

    // HEADS UP overwrites default Outlayer prepended
    Isotope.prototype.prepended = function( elems ) {
      var items = this._itemize( elems );
      if ( !items.length ) {
        return;
      }
      // add items to beginning of collection
      var previousItems = this.items.slice(0);
      this.items = items.concat( previousItems );
      // start new layout
      this._resetLayout();
      this._manageStamps();
      // layout new stuff without transition
      var filteredItems = this._filterRevealAdded( items );
      // layout previous items
      this.layoutItems( previousItems );
      // add to filteredItems
      this.filteredItems = filteredItems.concat( this.filteredItems );
    };

    Isotope.prototype._filterRevealAdded = function( items ) {
      var filteredItems = this._noTransition( function() {
        return this._filter( items );
      });
      // layout and reveal just the new items
      this.layoutItems( filteredItems, true );
      this.reveal( filteredItems );
      return items;
    };

    /**
     * Filter, sort, and layout newly-appended item elements
     * @param {Array or NodeList or Element} elems
     */
    Isotope.prototype.insert = function( elems ) {
      var items = this.addItems( elems );
      if ( !items.length ) {
        return;
      }
      // append item elements
      var i, item;
      var len = items.length;
      for ( i=0; i < len; i++ ) {
        item = items[i];
        this.element.appendChild( item.element );
      }
      // filter new stuff
      /*
       // this way adds hides new filtered items with NO transition
       // so user can't see if new hidden items have been inserted
       var filteredInsertItems;
       this._noTransition( function() {
       filteredInsertItems = this._filter( items );
       // hide all new items
       this.hide( filteredInsertItems );
       });
       // */
      // this way hides new filtered items with transition
      // so user at least sees that something has been added
      var filteredInsertItems = this._filter( items );
      // hide all newitems
      this._noTransition( function() {
        this.hide( filteredInsertItems );
      });
      // */
      // set flag
      for ( i=0; i < len; i++ ) {
        items[i].isLayoutInstant = true;
      }
      this.arrange();
      // reset flag
      for ( i=0; i < len; i++ ) {
        delete items[i].isLayoutInstant;
      }
      this.reveal( filteredInsertItems );
    };

    var _remove = Isotope.prototype.remove;
    Isotope.prototype.remove = function( elems ) {
      elems = makeArray( elems );
      var removeItems = this.getItems( elems );
      // do regular thing
      _remove.call( this, elems );
      // bail if no items to remove
      if ( !removeItems || !removeItems.length ) {
        return;
      }
      // remove elems from filteredItems
      for ( var i=0, len = removeItems.length; i < len; i++ ) {
        var item = removeItems[i];
        // remove item from collection
        removeFrom( item, this.filteredItems );
      }
    };

    Isotope.prototype.shuffle = function() {
      // update random sortData
      for ( var i=0, len = this.items.length; i < len; i++ ) {
        var item = this.items[i];
        item.sortData.random = Math.random();
      }
      this.options.sortBy = 'random';
      this._sort();
      this._layout();
    };

    /**
     * trigger fn without transition
     * kind of hacky to have this in the first place
     * @param {Function} fn
     * @returns ret
     * @private
     */
    Isotope.prototype._noTransition = function( fn ) {
      // save transitionDuration before disabling
      var transitionDuration = this.options.transitionDuration;
      // disable transition
      this.options.transitionDuration = 0;
      // do it
      var returnValue = fn.call( this );
      // re-enable transition for reveal
      this.options.transitionDuration = transitionDuration;
      return returnValue;
    };

    // ----- helper methods ----- //

    /**
     * getter method for getting filtered item elements
     * @returns {Array} elems - collection of item elements
     */
    Isotope.prototype.getFilteredItemElements = function() {
      var elems = [];
      for ( var i=0, len = this.filteredItems.length; i < len; i++ ) {
        elems.push( this.filteredItems[i].element );
      }
      return elems;
    };

    // -----  ----- //

    return Isotope;
  }

// -------------------------- transport -------------------------- //

  if ( typeof define === 'function' && define.amd ) {
    // AMD
    define( [
        'outlayer/outlayer',
        'get-size/get-size',
        'matches-selector/matches-selector',
        'isotope/js/item',
        'isotope/js/layout-mode',
        // include default layout modes
        'isotope/js/layout-modes/masonry',
        'isotope/js/layout-modes/fit-rows',
        'isotope/js/layout-modes/vertical'
      ],
      isotopeDefinition );
  } else {
    // browser global
    window.Isotope = isotopeDefinition(
      window.Outlayer,
      window.getSize,
      window.matchesSelector,
      window.Isotope.Item,
      window.Isotope.LayoutMode
    );
  }

})( window );

/*!
 * cellsByRows layout mode for Isotope
 * http://isotope.metafizzy.co
 */

( function( window ) {

  'use strict';

  function cellsByRowDefinition( LayoutMode ) {

    var CellsByRow = LayoutMode.create( 'cellsByRow' );

    CellsByRow.prototype._resetLayout = function() {
      // reset properties
      this.itemIndex = 0;
      // measurements
      this.getColumnWidth();
      this.getRowHeight();
      // set cols
      this.cols = Math.floor( this.isotope.size.innerWidth / this.columnWidth );
      this.cols = Math.max( this.cols, 1 );
    };

    CellsByRow.prototype._getItemLayoutPosition = function( item ) {
      item.getSize();
      var col = this.itemIndex % this.cols;
      var row = Math.floor( this.itemIndex / this.cols );
      // center item within cell
      var x = ( col + 0.5 ) * this.columnWidth - item.size.outerWidth / 2;
      var y = ( row + 0.5 ) * this.rowHeight - item.size.outerHeight / 2;
      this.itemIndex++;
      return { x: x, y: y };
    };

    CellsByRow.prototype._getContainerSize = function() {
      return {
        height: Math.ceil( this.itemIndex / this.cols ) * this.rowHeight
      };
    };

    return CellsByRow;

  }

  if ( typeof define === 'function' && define.amd ) {
    // AMD
    define( [
        'isotope/js/layout-mode'
      ],
      cellsByRowDefinition );
  } else {
    // browser global
    cellsByRowDefinition(
      window.Isotope.LayoutMode
    );
  }

})( window );;/**/
