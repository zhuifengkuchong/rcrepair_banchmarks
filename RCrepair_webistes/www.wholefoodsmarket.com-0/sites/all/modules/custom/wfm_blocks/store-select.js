/**
 * @file
 * Behavior for store select forms.
 */

//Self-invoking function
(function($){
  "use strict";
  $.fn.WFMstoreSelect = function() {
    var storeselect = {

      settings: {
        storeoptions: '',
      },

      /**
       * Add listeners to dom elements
       */
      addListeners: function() {
        $('.state-select').change(function() {
          storeselect.enableStoreSelect(this);
          storeselect.setStoreOptionsByState(this);
          storeselect.disableSubmit(this);
        });
        $('.store-select').change(function() {
          storeselect.enableSubmit(this);
        });
      },

      ajaxCallBack: function(storeObject) {
        var store = Drupal.WholeFoods.removeStoreNidKey(storeObject);
        storeselect.setUserDefaults(store);
      },

      /**
       * Disable store-select form store drop-down.
       * @param object state_select
       *   DOM element of state select. 
       */
      disableStoreSelect: function(state_select) {
        var form_id = '#' + $(state_select).parents('form').attr('id'), 
            selected_state = $(state_select).val();
        if (!selected_state || selected_state == '0') { //Zero as a string. Intentional.
          $(form_id + ' .store-select').addClass('disabled').prop('disabled', true);
        }
      },

      /**
       * Disable store-select form submit.
       * @param object state_select
       *   DOM element of state select. 
       */
      disableSubmit: function(state_select) {
        var form_id = '#' + $(state_select).parents('form').attr('id'),
            selected_store = $(form_id + ' .store-select').val();
        if (!selected_store) {
          $(form_id + ' .store-select-submit').addClass('disabled').prop('disabled', true);
        }
      },

      /**
       * Enable store-select form store drop-down.
       * @param object state_select
       *   DOM element of state select. 
       */
      enableStoreSelect: function(state_select){
        var form_id = '#' + $(state_select).parents('form').attr('id');
        if ($(state_select).val() && $(state_select).val() !== '0') { // Zero as a string. Intentional.
          $(form_id + ' .store-select').removeClass('disabled').prop('disabled', false);
        }
      },

      /**
       * Enable store-select form submit.
       * @param object store_select
       *   DOM element of store select.
       */
      enableSubmit: function(store_select) {
        var form_id = '#' + $(store_select).parents('form').attr('id');
        $(form_id + ' .store-select-submit').prop('disabled', false).removeClass('disabled');
      },

      /**
       * Build <option> element.
       * @param object values
       *   Attributes for the option element in key/value pairs.
       * @return object
       *   <option> DOM element
       */
      getOption: function(values) {
        var opt = document.createElement('OPTION'),
            option_values = {},
            defaults = {
              label: Drupal.t('Please Select a Store'),
              text: Drupal.t('Please Select a Store'),
              value: ''
            };
        if (typeof values == 'undefined') {
          values = {};
        } 
        option_values = $.extend(option_values, defaults, values);
        for (var attribute in option_values) {
          opt[attribute] = option_values[attribute];
        }
        return opt;
      },

      /**
       * Get store option values from the store select drop-down and save them
       * in storeselect.settings.storeoptions variable.
       */
      getStoreOptions: function() {
        var options = {};
        $('.store-select').first().children('optgroup').each(function(){
          var label = $(this).attr('label');
          options[label] = {};
          $(this).children('option').each( function(){
            var value = $(this).val();
            options[label][value] = {};
            options[label][value]['name'] = $(this).text();
            options[label][value]['selected'] = false;
            if($(this).prop('selected') === true) {
              options[label][value]['selected'] = true;
            }
          });
        });
        storeselect.settings.storeoptions = options;
      },

      /**
       * Initialization function.
       */
      init: function() {
        if ($('form#store-select-form').length > 1) {
          $('form#store-select-form').each(function(i){
            this.setAttribute("id", "store-select-form-" + i);
          });
        }
        storeselect.addListeners();
        storeselect.getStoreOptions();
        $('.state-select').each(function(){
          storeselect.disableStoreSelect(this);
          storeselect.setStoreOptionsByState(this);
          storeselect.disableSubmit(this);
        })
        storeselect.prepareUserDefaults();
      },

      /**
       * Determine if user has a store selected
       */
      prepareUserDefaults: function() {
        var storenid = '',
            state = '',
            storeinfo = '';
        if (Drupal.WholeFoods.getCookie('local_store')) {
          storenid = Drupal.WholeFoods.getCookie('local_store');
        }
        if (storenid) {
          Drupal.WholeFoods.getStoreInfo(storenid, storeselect.ajaxCallBack);
        }
      },

      /**
       * Filter store select options by selected state.
       * @param object state_select
       *   State select DOM object
       */
      setStoreOptionsByState: function(state_select){
        var options = storeselect.settings.storeoptions,
            stateabbr = $(state_select).val(),
            defaultopt = storeselect.getOption(),
            form_id = '#' + $(state_select).parents('form').attr('id'),
            store_select_element = $(form_id + ' .store-select');

        store_select_element.find('option').each(function(){
          $(this).detach();
        });
        store_select_element.find('optgroup').each(function(){
          $(this).detach();
        });
        store_select_element.append(defaultopt);
          
        //loop through options object, create optgroups and options
        $.each(options, function(index, value) {
          var abbr = index.substring(0, 2),
              optgroup = {};
          if (abbr == stateabbr) {
            optgroup = document.createElement('optgroup');
            optgroup.label = index.substring(2);
            store_select_element.append(optgroup);
            $.each(value, function(optindex, optvalue) {
              var option_values = {
                    label: optvalue.name,
                    text: optvalue.name,
                    value: optindex,
                    selected: optvalue.selected
                  },
                  opt = storeselect.getOption(option_values);
              store_select_element.find('optgroup').last().append(opt);
            });
          }
        });
      }, //setStateOptions

      /**
       * Set default state/store values from user's selected store.
       * @param object store
       *   Store info object from Drupal.setting.WholeFoods.stores
       */
      setUserDefaults: function(store) {
        if (Object.keys(store).length === 1) {
          store = Drupal.WholeFoods.removeStoreNidKey(store);
        }
        $('.state-select option[value="' + store.location.stateabbr + '"]').prop('selected', true);

        //This trigger is a FF fix.
        $('.state-select').trigger('change');

        $('.store-select').removeClass('disabled').prop('disabled', false);
        $('.store-select option[value="' + store.nid + '"]').prop('selected', true);
        $('.store-select').each(function(){
          storeselect.enableSubmit(this);
        });
      },

    }; //storeselect
    storeselect.init();
    Drupal.WholeFoods.setStoreSelectStore = storeselect.setUserDefaults;
  };

})(jQuery);


/**
 * Attach to Drupal behaviors.
 */
Drupal.behaviors.wfmstoreselect = {
  attach: function () {
    jQuery('body').once('ss-attach', function () {
      jQuery('.store-select-form').WFMstoreSelect();
    });
  }
};
