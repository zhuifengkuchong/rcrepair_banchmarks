/**
 * @file
 * TODO (msmith): Add file docs.
 */

(function ($) {

Drupal.settings.WholeFoods = {};
Drupal.WholeFoods = {};

/**
 * Method to be ran on page load.
 */
Drupal.WholeFoods.pageLoad = function () {
  Drupal.WholeFoods._setupLocalStore();
  Drupal.WholeFoods._fixAddressReturnKey();
  Drupal.WholeFoods.setupAttachBehaviors();
  Drupal.WholeFoods.setupUserSettings();
};

/**
 * Setup events to be fired on Drupal's attach behavior.
 */
Drupal.WholeFoods.setupAttachBehaviors = function() {
  Drupal.behaviors.WFM = {
    attach: function (context, settings) {
      Drupal.WholeFoods._fixAddressReturnKey();
    }
  };
}

/**
 * Return a cookie value if set.
 *
 * @param c_name
 *   The name of the coookie to lookup.
 *
 * @return
 *   string - The value of the cookie, if it's available.
 * @return
 *   null - The cookie is not available.
 */
Drupal.WholeFoods.getCookie = function(c_name) {
  var i, x, y, ARRcookies = document.cookie.split(";");
  for (i = 0; i < ARRcookies.length; i++) {
    x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
    y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
    x = x.replace(/^\s+|\s+$/g, "");
    if (x == c_name) {
      return unescape(y);
    }
  }
  return null;
}

/**
 * Expire a cookie if it is set.
 *
 * @param c_name
 *   The name of the coookie to lookup.
 * @param c_path
 *   The path of the coookie to expire; Defaults to '.wholefoodsmarket.com'.
 * @param c_domain
 *   The domain of the coookie to expire; Defaults to '/'.
 *
 * @return
 *   string - The value of the cookie, if it's available.
 * @return
 *   null - The cookie is not available.
 */
Drupal.WholeFoods.deleteCookie = function(c_name, c_path, c_domain) {
  if (!Drupal.WholeFoods.getCookie(c_name)) { return false; }
  document.cookie = encodeURIComponent(c_name) + 
    "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + 
    (c_domain ? "; domain=" + c_domain : "; domain=/") + 
    (c_path ? "; path=" + c_path : "http://www.wholefoodsmarket.com/sites//all//modules//features//core//js//; path=.wholefoodsmarket.com");
  return true;
}

/**
 * Expires user authentication cookies.
 */
Drupal.WholeFoods.deleteAuthCookies = function() {
  Drupal.WholeFoods.deleteCookie('HD');
  Drupal.WholeFoods.deleteCookie('at');
  Drupal.WholeFoods.deleteCookie('gtc');
  Drupal.WholeFoods.deleteCookie('gtct');
  Drupal.WholeFoods.deleteCookie('GiftCardPortalPartnerAuth');
  Drupal.WholeFoods.deleteCookie('juuid');
}

/**
 * Get the Three Letter Code (tlc) of a store by Drupal nid.
 *
 * @param integer
 *   nid of the store to get the TLC for.
 *
 * @return string
 *   The TLC of the nid.
 * @return null
 *   There is no TLC for that nid.
 */
Drupal.WholeFoods.getStoreTlc = function(nid) {
  if (Drupal.settings.WholeFoods.stores[nid]) {
    var tlc = Drupal.settings.WholeFoods.stores[nid].tlc;
    if (typeof tlc == 'undefined') {
      return null;
    }
    return tlc;
  }
}

/**
 * Pull Drupal.settings.WholeFoods.localStore from cookie if available.
 */
Drupal.WholeFoods._setupLocalStore = function() {
  var localStore = Drupal.WholeFoods.getCookie('local_store');
  if (localStore != null) {
    Drupal.settings.WholeFoods.localStore = localStore;
  }
  return;
}

/**
 * Fix input form [name=address] to submit the form on pressing "return key".
 */
Drupal.WholeFoods._fixAddressReturnKey = function () {
  $('input.form-text[name=address]').each(function() {
    $(this).keypress(function(event) {
      if (event.which == 13) {
        var $form = $(this).parents('form');
        var $submit = $('input.form-submit', $form);
        $submit.click()
        event.preventDefault();
      }
    });
  });
}


/**
 * Define and / or clear the Drupal.settings.WholeFoods.user object.
 */
Drupal.WholeFoods.clearUserObj = function() {
  Drupal.settings.WholeFoods.user = {
    janrain: null,
    name: null,
    wfmid: null,
    email: null,
  };
}

/**
 * Initialize client side objects and cookies based on authentication status.
 */
Drupal.WholeFoods.setupUserSettings = function() {
  var hdCookie = Drupal.WholeFoods.getCookie('HD');
  if ($("body").hasClass("not-logged-in")) {
    //SIGNED OUT
    if (hdCookie) {
      Drupal.WholeFoods.deleteAuthCookies();
    }
    Drupal.WholeFoods.clearUserObj();
  } else if ($("body").hasClass("logged-in")) {
    //SIGNED IN
    if (hdCookie) {
      var juuid = Drupal.WholeFoods.getCookie('juuid');
      var hdSplit = hdCookie.split('|');
      Drupal.settings.WholeFoods.user = {
        janrain: juuid,
        name: hdSplit[1],
        wfmid: hdSplit[2],
        email: hdSplit[3],
        pluck: hdSplit[0],
      };
    } else {
      //SIGNED IN BUT NO HD COOKIE
      Drupal.WholeFoods.clearUserObj();
    }
  }
  var uidCookie = Drupal.WholeFoods.getCookie('http://www.wholefoodsmarket.com/sites//all//modules//features//core//js//Drupal.visitor.uid');
  if (uidCookie) {
    if (typeof Drupal.settings.WholeFoods.user === 'object') {
      Drupal.settings.WholeFoods.user.drupalUID = uidCookie;
    }
  }
}

/**
 * @param string storenid
 *   store node id
 * @param function callback
 *   Callback function that accept an object of store info as a parameter.
 *   Parameter will be given to callback like so:
 *   {
 *     storenid: { 
 *        key: value,
 *        ...
 *     }
 *   } 
 */
Drupal.WholeFoods.getStoreInfo = function(storenid, callback) {
  var storenid = (typeof storenid === "undefined") ? NULL : storenid,
      url = '',
      return_obj = {};
  if (Drupal.settings.WholeFoods.stores) {
    if (storenid) {
      return_obj[storenid] = Drupal.settings.WholeFoods.stores[storenid];
      callback(return_obj);
    }
  }
  else {
    if (storenid) {
      url = '/ajax/stores/' + storenid;
    }
    else {
      url = 'ajax/stores/';
    }
    $.getJSON(url)
      .done(function doneCallback(json){
        callback(json);
      })
      .fail(function( jqxhr, textStatus, error ) {
        var err = textStatus + ", " + error;
        console.log( "Request Failed: " + err );
      })
  }
};

/**
 * Removes node key from store info response.
 * @param object store
 *   {
 *     storenid: { 
 *        key: value,
 *        ...
 *     }
 *   }  
 * @return object
 *   Store info with nid key removed.
 *   {
 *      key: value,
 *      ...
 *   }  
 */
Drupal.WholeFoods.removeStoreNidKey = function(store) {
  for (var nid in store) {
    return store[nid];
  }
}

/**
 * Get stores in a region
 * @param string region two-letter abbreviation
 * @return array
 *   array of store objects
 */
Drupal.WholeFoods.getStoresByRegion = function(region) {
  var stores           = Drupal.settings.WholeFoods.stores,
      stores_in_region = [],
      i                = null;
  for( i in stores ) {
    if( !stores.hasOwnProperty(i) ) {
      continue;
    }
    if( stores[i].region == region ) {
      stores_in_region.push(stores[i]);
    }
  }
  return stores_in_region;
}

/**
 * Get a store's region by three-letter-code.
 * @param string store tlc
 * @return string region abbreviation.
 */
Drupal.WholeFoods.getStoreRegionByTlc = function(tlc) {
  var stores           = Drupal.settings.WholeFoods.stores,
      i                = null;
  for( i in stores ) {
    if( !stores.hasOwnProperty(i) ) {
      continue;
    }
    if( stores[i].tlc == tlc ) {
      return stores[i].region;
    }
  }
  return null;
};

/**
 * Get a store's region by it's path.
 * @param string storePath
 *   Store path, lamar, gateway, for example
 * @return string
 *   2-letter region code
 */
Drupal.WholeFoods.getRegionByStorePath = function(storePath) {
  var stores           = Drupal.settings.WholeFoods.stores,
      i                = null;
  for( i in stores ) {
    if( !stores.hasOwnProperty(i) ) {
      continue;
    }
    if( stores[i].path == storePath ) {
      return stores[i].region;
    }
  }
  return null;
};

/**
 * Get a store's region by a store node id.
 * @param int nid
 *   Store node id
 * @return string
 *   2-letter region code
 */
Drupal.WholeFoods.getRegionByDrupalNid = function(nid) {
  var stores           = Drupal.settings.WholeFoods.stores,
      i                = null;
  for( i in stores ) {
    if( !stores.hasOwnProperty(i) ) {
      continue;
    }
    if( stores[i].nid == nid ) {
      return stores[i].region;
    }
  }
  return null;
};

/**
 * Run on window load.
 */
$(window).load(function() {
  Drupal.WholeFoods.pageLoad();
});

})(jQuery);
