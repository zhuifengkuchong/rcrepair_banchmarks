/**
 * @file
 * Brand camp likes javascript handlers
 */

(function ($) {
  /**
   * Loads dynamic links per user settings.
   */
  Drupal.behaviors.brand_camp_likes = {
    attach: function(context, settings) {
      $(window).load(function(){

        var isTouchDevice = 'ontouchstart' in document.documentElement;

        if (isTouchDevice) {
          //code for touch devices - tablets etc.
          $('.views-row').live("click", function(e) {

            if ($(this).find('article.node-bc-ugc-image').length == 0) {
              //this is not UGC tile. Process click right away
              console.log('redirect'); // don't remove please it makes iOS to really process click
              return true;
            }

            //In case it's a UGC tile
            if ($(this).hasClass('hover')) {
              // hover already visible. Process click
              return true;
            } else {
              // show hover. Do not process click
              $(this).addClass('hover');
              e.stopPropagation();
              return false;
            }
          });
        } else {
          // code for desktop
          $('.views-row')
            .live("mouseenter", function() { $(this).addClass('hover'); })
            .live("mouseleave", function() { $(this).removeClass('hover'); });
        }

        // Init spinner for loading.
        var spinner = new Spinner({color: '#fff', top: '42%'}).spin();

        Drupal.behaviors.brand_camp_likes.setLikesTiles();

        var tilesList = $("div.view-brand-camp-list div.view-content .node-bc-ugc-image");
        var likesList = tilesList.find('.node-content .field-name-field-bc-src-likes-count');
        $('body').delegate('.field-name-field-bc-src-likes-count', 'click', function clickLikeCallback( e ){
          e.stopPropagation();
          var _thisLike = $(this);
          var _thisTile = _thisLike.parent().parent();
          var _thisLikeField = _thisLike.find(".field-item");

          // Prevent clicking if exist loader.
          if (_thisLike.find('.spinner').length) {
            return;
          }
          _thisLike.append(spinner.el);

          var nodeId = _thisTile.attr('id').substr(5);
          var ajaxUrl = settings.basePath + 'brand-camp/chute-like/' + nodeId;

          if (!_thisTile.hasClass('chute-liked')) {
            $.ajax({
              url: ajaxUrl + '/add',
              dataType: 'json',
              error: function() {
                _thisLike.find('.spinner').remove();
              },
              success: function (data) {
                if (data) {
                  _thisTile.addClass('chute-liked');
                  _thisLikeField.html(data.count);
                  _thisLike.find('.spinner').remove();
                }
              }
            });
          } else {
            $.ajax({
              url: ajaxUrl + '/remove',
              dataType: 'json',
              error: function() {
                _thisLike.find('.spinner').remove();
              },
              success: function (data) {
                if (data) {
                  _thisTile.removeClass('chute-liked');
                  _thisLikeField.html(data.count);
                  _thisLike.find('.spinner').remove();
                }
              }
            });
          }
        });

        // Add behaviour on click 'load more'.
        $('#isopager').click(function(e) {
          Drupal.behaviors.brand_camp_likes.setLikesTiles();
        });
      });
    },
    setLikesTiles: function () {
      var chute_likes = jQuery.cookie('chute_likes');
      var chute_likes_json = {};
      if (chute_likes && chute_likes.length > 0) {
        try {
          chute_likes_json = JSON.parse(chute_likes);
        }
        catch(e){
          return;
        }

        for (var nid in chute_likes_json) {
          $("#node-" + nid).addClass('chute-liked');
        }
      }
    }
  };
})(jQuery);
