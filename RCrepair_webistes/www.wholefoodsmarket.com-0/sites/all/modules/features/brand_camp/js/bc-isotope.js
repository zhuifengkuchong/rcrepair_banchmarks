var $isotopeContainer;
var $isotopePager = {};
var filter;

(function($){
  // Function includes Isotope bindings to provide filtering
  // and custom code to provide paging
  $(document).ready(function isotopeBindings() {
    //easily configurable options
    var $itemSelector = '.views-row';

    var $frontPage = $('body').hasClass('page-values-matter');

    filter = {
      showme: 0,
      iwantto: 0,
      ugc: 0,
      ugcSearch: '',
      prepended: [],
      setFilterValue: function(_class, value) {
        if (_class == 'showme') {
          this.setShowme(value);
        }
        if (_class == 'iwantto') {
          this.setIwantto(value);
        }
        if (_class == 'ugc') {
          //this.setUGC(value);
          if (value == 1) {
            window.location = "http://www.wholefoodsmarket.com/values-matter/your-stories";
          } else {
            window.location = "http://www.wholefoodsmarket.com/values-matter";
          }
          return;
        }
        this.updateFilteredList();
      },
      updateFilteredList: function() {
        Drupal.settings.brandCamp.lists.mainFiltered = [];
        var list = Drupal.settings.brandCamp.lists.main;
        var filtered = Drupal.settings.brandCamp.lists.mainFiltered;
        if (Drupal.settings.brandCamp.lists.prepend) {
          for(var index = 0; index < Drupal.settings.brandCamp.lists.prepend.length; index++) {
            this.prepended.push(Drupal.settings.brandCamp.lists.prepend[index].nid);
          }
        }
        for(var index = 0; index < list.length; index++) {
          if (this.isMatch(list[index])) {
            filtered.push(index);
          }
        }
      },
      setShowme: function(value) {
        this.showme = value;
      },
      setIwantto: function(value) {
        this.iwantto = value;
      },
      setUGC: function(value) {
        this.ugc = value;
      },
      setUGCSearch: function(value) {
        value = value.trim();
        if (value.charAt(0) == '@') {
          value = value.substr(1);
        }
        if (value == this.ugcSearch) {
          return;
        }
        this.ugcSearch = value;
        if (value) {
          Drupal.settings.brandCamp.lists.ugcSearch = [];
          Drupal.settings.brandCamp.pages.ugcSearch = 1;
          $isotopePager.pagesLoaded.ugcSearch = 0;
        }
      },
      reset: function() {
        this.showme = this.iwantto = 0;
      },
      isMatch: function(element) {
        return (this.prepended.indexOf(element.nid) < 0) && (this.iwantto == 0 || element.iwantto.indexOf(this.iwantto) >= 0) && (this.showme == 0 || element.showme.indexOf(this.showme) >= 0);
      }
    };

    $isotopePager = {
      $id: 'isopager',
      currentPage: 1,
      perPage: 12,
      curItem: {ugc: 0, connect: 0, main: 0},
      pageStartIndexes: [],
      $detached: null,
      pagesLoaded: {ugc: 1, main: 1, ugcSearch: 0},
      ajaxing: false,
      ajaxFailCounter: 0,
      curtainColors: ['#99aea5', '#29634f', '#e1781d', '#d0b657', '#a49e87'],
      animating: true,
      badRows: [],
      prependedLength: 0, // 3 nodes that are prepended before main list on Node page

      next: function() {
        var lists = Drupal.settings.brandCamp.lists;
        var elems = [];
        var startIndex = Math.min(Math.max($isotopeContainer.find($itemSelector).length - this.prependedLength - this.currentPage * this.perPage, 0), 12);
        var endOfContent = false;
        var thisPager = this;

        this.showPager();
        this.pageStartIndexes[this.currentPage] = jQuery.extend({},this.curItem);

        if (this.currentPage < 0) {
          // this is node view page that has additional 3 nodes prepended before main list
          this.prependedLength = lists.prepend.length;
          startIndex = this.perPage - this.prependedLength;
        }

        for (var index = startIndex; index < this.perPage && !endOfContent; index++) {
          var weight = this.currentPage * this.perPage + index;
          var type, info, element;
          if (this.currentPage < 0) {
            // this is node view page that has additional 3 nodes prepended before main list
            type = 'article';
            info = lists.prepend[weight + this.prependedLength];
          }
          else if (lists.connect.length && (index == 2 || index == 6)) {
            // Insert special blocks on any of BC pages
            type = 'connect';
            info = lists.connect[this.curItem.connect];
            this.curItem.connect = ++this.curItem.connect % lists.connect.length;
          }
          else if (filter.ugc == 1) { // filter.ugc == 1 on "Your Stories" page
            type = 'ugc-image';
            var list;
            if (filter.ugcSearch) {
              list = lists.ugcSearch;
            }
            else {
              list = lists.ugc;
            }
            info = list[this.curItem.ugc];
            // check whether we need to preload more items if only 20 left in the array of available ones
            if (this.curItem.ugc + 20 >= list.length) {
              if (filter.ugcSearch) {
                this.loadMore('ugcSearch');
              }
              else {
                this.loadMore('ugc');
              }
            }
            // increments curItem counter and checks if that's last item among available ones
            if (++this.curItem.ugc >= list.length) {
              this.hidePager();
              this.curItem.ugc = list.length;
              endOfContent = true;
            }
          }
          else if (lists.ugc.length && (index == 1 || index == 8)) {
            type = 'ugc-image';
            info = lists.ugcShuffle[this.curItem.ugc];
            this.curItem.ugc = ++this.curItem.ugc % lists.ugc.length;
          }
          else {
            type = 'article';
            info = lists.main[lists.mainFiltered[this.curItem.main]];
            if (this.curItem.main + 20 >= lists.mainFiltered.length) {
              this.loadMore('main');
            }
            if (++this.curItem.main >= lists.mainFiltered.length) {
              this.hidePager();
              endOfContent = true;
            }
          }

          if (!info) {
            this.hidePager();
            break;
          }
          var colorInd = Math.floor((Math.random() * 5) + 1) -1;
          info.curtain_color = $isotopePager.curtainColors[colorInd];

          if (this.$detached && (element = this.$detached.filter('.views-row[nid=' + info.nid + ']').first()) && element.length) {
            $isotopeContainer.append(element.attr('weight', weight));
          }
          else {
            element = $(Drupal.theme('brandCampListRow', type, info)).attr('weight', weight);
            if (!elems.length) {
              elems = element;
            }
            else {
              elems = elems.add(element);
            }

            $isotopeContainer.append(element);

            onLoad = function() {
              $(this).parents('.views-row')
                .addClass('animate')
                .one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function() {
                  $(this).removeClass('animate').addClass('shown');
                });
            }
            element.find('img').load(onLoad).error(onLoad);
            element.find('.ugc_img')
              .load(function() {
                var w = $(this)[0].naturalWidth, h = $(this)[0].naturalHeight;
                if (w > 0 && h > 0) {
                  if (w > h) {
                    $(this).css({
                      width: 100 * w / h + '%',
                      left: -50 * (w / h - 1) + '%',
                      maxWidth: 'none'
                    });
                  }
                  else if (w < h) {
                    $(this).css('top', -50 * (h / w - 1) + '%');
                  }
                }
              })
              .error(function() {
                thisPager.badRows.push($(this).parents('.views-row'));
                $(this).remove();
                thisPager.removeBadRows();
              });
          }
        } //endfor

        this.currentPage++;
        if (this.$detached) {
          if (jQuery().isotope) {
            this.$detached.each(function () {
              if (!$(this).parent().length) {
                $isotopeContainer.isotope('remove', $(this)).isotope();
              }
            })
          }
          this.$detached = null;
        }

        if (jQuery().isotope) {
          thisPager.animating = true;
          // re-insert elements into isotope updating sort as they might be wrongly ordered after manipulations
          $isotopeContainer.isotope('updateSortData').isotope('insert', elems).isotope();
        }

        if ($frontPage && !this.ajaxing && !$isotopeContainer.find($itemSelector).length) {
          $isotopeContainer.append($('<div class="no-matches">No matches found</div>'));
        }
      },

      reset: function() {
        this.$detached = $isotopeContainer.find($itemSelector).detach();
        this.currentPage = this.curItem.ugc = this.curItem.connect = this.curItem.main = 0;
        if (Drupal.settings.brandCamp.lists.prepend) {
          // this is node view page that has additional 3 nodes prepended before main list
          this.currentPage--;
        }
        this.showPager();
        this.next();

      },

      loadMore: function(type) {
        if (this.ajaxing || this.pagesLoaded[type] >= Drupal.settings.brandCamp.pages[type]) return;
        this.ajaxing = true;
        thisPager = this;
        url = '/values-matter/ajax/' + (type == 'main' ? 'main' : 'ugc') + '/' + this.pagesLoaded[type];
        if (type == 'ugcSearch' && filter.ugcSearch) {
          url += '/' + filter.ugcSearch
        }
        $.ajax(url)
          .done(function( data ) {
            Drupal.settings.brandCamp.lists[type] = Drupal.settings.brandCamp.lists[type].concat(data);

            filter.updateFilteredList();
            thisPager.pagesLoaded[type]++;
            if (type == 'ugcSearch' && data.length == 100) {
              Drupal.settings.brandCamp.pages[type]++;
            }
            thisPager.ajaxing = false;
            thisPager.ajaxFailCounter = 0;
            thisPager.layout();
          })
          .fail(function() {
            if (thisfilter.ajaxFailCounter++ < 3) {
              setTimeout(function() {
                thisPager.ajaxing = false;
                thisPager.loadMore();
              }, 1000);
            }
          });
      },

      layout: function() {
        this.currentPage--;
        //this.showPager();
        this.next();
      },

      hidePager: function() {
        $('#' + this.$id).hide();
      },

      showPager: function() {
        $('#' + this.$id).show();
        $isotopeContainer.find('.no-matches').remove();
      },

      removeBadRows: function() {
        thisPager = $isotopePager;
        if (!thisPager.badRows.length) {
          return;
        }
        if (thisPager.animating) {
          setTimeout(thisPager.removeBadRows, 100);
          return;
        }

        var nids = {};
        for (var i = 0; i < thisPager.badRows.length; i++) {
          nids[thisPager.badRows[i].attr('nid')] = true;
        }

        for (var i = 0; i < Drupal.settings.brandCamp.lists.ugc.length; i++) {
          if (nids[Drupal.settings.brandCamp.lists.ugc[i].nid]) {
            Drupal.settings.brandCamp.lists.ugc.splice(i, 1);
            Drupal.settings.brandCamp.lists.ugcShuffle.splice(i, 1);
          }
        }

        thisPager.currentPage--;
        thisPager.$detached = $isotopeContainer.find($itemSelector).slice(thisPager.currentPage * thisPager.perPage).detach();
        thisPager.curItem = jQuery.extend({}, thisPager.pageStartIndexes[thisPager.currentPage]);
        thisPager.next();
      }
    };

    $isotopeContainer = $('.view-brand-camp-list > .view-content').html('');

    if (jQuery().isotope) {
      //create isotope context on element
      $isotopeContainer.isotope({
        itemSelector: $itemSelector,
        layoutMode: 'cellsByRow',
        cellsByRow: {
          columnWidth: 360,
          isFitWidth: true
        },
        getSortData: {
          name: 'weight',
          weight: function (element) {
            return parseInt($(element).attr('weight'));
          }
        },
        sortBy: 'weight',
        hiddenStyle: { opacity: 0 },
        visibleStyle: { opacity: 1 },
        isJQueryFiltering: false //keep this disabled for jQuery < 1.6
      });
    }

    Drupal.settings.brandCamp.lists.ugcShuffle = Drupal.settings.brandCamp.lists.ugc.slice(0);
    shuffle(Drupal.settings.brandCamp.lists.ugcShuffle);
    if ($('body').hasClass('node-type-bc-ugc-image') || window.location.pathname == '/values-matter/your-stories') {
      filter.setUGC(1);
      var name = getParameterByName('name')
      if (name) {
        name = html_sanitize(name);
        filter.setUGCSearch(name);
      }
    }
    filter.updateFilteredList();
    $isotopePager.reset();

    if (jQuery().isotope) {
      $isotopeContainer.isotope( 'on', 'layoutComplete', function() {
        $isotopePager.animating = false;
      });
    }

    //function to create filters block
    (function createFiltersBlock($) {
      var theme = Drupal.settings.WholeFoods.theme,
          filter_type = '';
      switch (theme) {
        case 'wholefoods_mobile':
          filter_type = 'mobile';
          break;
        case 'brand_camp_theme':
          filter_type = 'desktop';
          break;
        default:
          filter_type = 'desktop';
      };
      $('<div class="isoFilters ' + filter_type + '">' +
      (!filter.ugc ?
      '<div class="showme isoFilters-widget">' +
      '<div class="title">Show me</div>' +
      '<div class="filters" data-filter="showme">' +
      '<div class="filter" data-filter="1">Food</div>' +
      '<div class="filter" data-filter="2">People</div>' +
      '<div class="filter" data-filter="3">Planet</div>' +
      '<div class="filter selected" data-filter="0">All</div>' +
      '</div>' +
      '</div>' +
      '<div class="iwantto isoFilters-widget">' +
      '<div class="title">I want to</div>' +
      '<div class="filters" data-filter="iwantto">' +
      '<div class="filter" data-filter="1">Learn</div>' +
      '<div class="filter" data-filter="2">Do</div>' +
      '<div class="filter selected" data-filter="0">Both</div>' +
      '</div>' +
      '</div>' :
      '<div class="ugc-search isoFilters-widget">' +
      '<div class="title">SEARCH BY @NAME</div>' +
      '<form id="ugc-search-form"><input id="ugc-search-value" type="text" name="name" value="' + filter.ugcSearch + '"><input id="ugc-search-submit" type="submit" value="FIND IT!"></form>' +
      '</div>') +
      '<div class="ugc isoFilters-widget">' +
      '<div class="title">View</div>' +
      '<div class="filters" data-filter="ugc">' +
      '<div class="filter" data-filter="0">Our Stories</div>' +
      '<div class="filter" data-filter="1">Your Stories</div>' +
      '</div>' +
      '</div>' +
      '</div>')
        .insertBefore('#block-system-main .view-brand-camp-list .view-content');
      $('.ugc.isoFilters-widget .filter[data-filter="' + filter.ugc + '"]').addClass('selected');
      $('<div id="' + $isotopePager.$id + '" data-page="1">Load more</div>')
        .insertAfter('.view-brand-camp-list .view-content');
    })($);

    if ($frontPage) {
      $('#ugc-search-form').submit(function() {
        filter.setUGCSearch($('#ugc-search-value').val());
        $isotopePager.reset();
        return false;
      });
    }

    //assign events to filters block
    $('.isoFilters .filter').click(function() {
      if (this.hasAttribute('data-filter')) {
        filter.setFilterValue($(this).parent('.filters').attr('data-filter'), $(this).attr('data-filter'));
      }
      $(this).parent('div').find('.filter').removeClass('selected');
      $(this).addClass('selected');

      $isotopePager.reset();
    });

    //pager event
    $('#isopager').click(function() {
      $isotopePager.next();
    });

    // check URL args for direct linking to specific filter combinations
    var params, paramsRe = /field_bc_(\w+)_value=(\d+)/g;
    while ((params = paramsRe.exec(window.location.search)) != null) {
      $('.filters[data-filter="' + params[1] + '"] .filter[data-filter="' + params[2] + '"]').click();
    }

    function shuffle(array) {
      var currentIndex = array.length, temporaryValue, randomIndex ;

      // While there remain elements to shuffle...
      while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }

      return array;
    }

    function getParameterByName(name) {
      name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
      return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
  });

})(jQuery);


Drupal.theme.prototype.brandCampListRow = function(type, elem) {
  var image, 
      content='', 
      showme = '', 
      names = ['', 'Food', 'People', 'Planet'],
      dimensions = [],
      theme = Drupal.settings.WholeFoods.theme;

  dimensions.ugc = [];
  dimensions.bc = [];
  dimensions.connect = [];

  switch(theme) {
    case 'wholefoods_mobile':
      dimensions.ugc.width = 640;
      dimensions.ugc.height = 640;
      dimensions.bc.width = 640;
      dimensions.bc.height = 510;
      dimensions.connect.width = 640;
      dimensions.connect.height = 640;
      break;
    case 'brand_camp_theme':
      dimensions.ugc.width = 340;
      dimensions.ugc.height = 340;
      dimensions.bc.width = 340;
      dimensions.bc.height = 270;
      dimensions.connect.width = 340;
      dimensions.connect.height = 340;
      break;
    default:
      dimensions.ugc.width = 340;
      dimensions.ugc.height = 340;
      dimensions.bc.width = 340;
      dimensions.bc.height = 270;
      dimensions.connect.width = 340;
      dimensions.connect.height = 340;
  }

  if (type == 'article') {
    image = 'height="' + dimensions.bc.height + '" src="/sites/default/files/styles/bc_image_' + dimensions.bc.width +'x' + dimensions.bc.height + '/public'+ elem.image + '"';
    for (var index = 0; index < elem.showme.length; ++index) {
      showme += '<div class="field-item odd field-key-' + elem.showme[index] + '" title="' + names[elem.showme[index]] + '">' + names[elem.showme[index]] + '</div>';
    }
    content = '<div class="field field-name-field-bc-showme field-type-list-text field-label-hidden view-mode-teaser"><div class="field-items">' + showme + '</div></div>'
        + '<div class="field field-name-field-bc-summary field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + elem.summary + '</div></div></div>';
  }
  else if (type == 'ugc-image') {
    if (theme == 'brand_camp_theme') {
      image = 'height="' + dimensions.ugc.height + '" src="'+ elem.image + '" class="ugc_img"';
    } 
    else if (theme == 'wholefoods_mobile') {
      image = 'height="' + dimensions.ugc.height + '" src="'+ elem.image_full + '" class="ugc_img"';
    }
    else {
      image = 'height="' + dimensions.ugc.height + '" src="'+ elem.image + '" class="ugc_img"';
    }
    if (theme == 'brand_camp_theme') {
      content = '<div class="field field-name-field-bc-src-username field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even"><a href="/values-matter/your-stories?name=' + elem.user + '">@' + elem.user + '</a></div></div></div>' +
        '<div class="field field-name-field-bc-album-name field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + elem.album_name + '</div></div></div>' +
        '<div class="field field-name-field-bc-src-service field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + elem.service + '</div></div></div>' +
        '<div class="field field-name-field-bc-src-likes-count field-type-number-integer field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + elem.likes + '</div></div></div>';
    }
    else if (theme == 'wholefoods_mobile') {
      content = '<div class="field field-name-field-bc-src-username field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even"><a href="/values-matter/your-stories?name=' + elem.user + '">@' + elem.user + '</a></div></div></div>' +
        '<div class="field field-name-field-bc-album-name field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + elem.album_name + '</div></div></div>' +
        '<div class="field field-name-field-bc-src-likes-count field-type-number-integer field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + elem.likes + '</div></div></div>' +
        '<div class="field field-name-field-bc-src-service field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + Drupal.t('Via ') + elem.service + '</div></div></div>';
    }
    else {
      content = '<div class="field field-name-field-bc-src-username field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even"><a href="/values-matter/your-stories?name=' + elem.user + '">@' + elem.user + '</a></div></div></div>' +
        '<div class="field field-name-field-bc-album-name field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + elem.album_name + '</div></div></div>' +
        '<div class="field field-name-field-bc-src-likes-count field-type-number-integer field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + elem.likes + '</div></div></div>' +
        '<div class="field field-name-field-bc-src-service field-type-text field-label-hidden view-mode-teaser"><div class="field-items"><div class="field-item even">' + elem.service + '</div></div></div>';
    }
  }
  else {
    image = 'height="' + dimensions.connect.height + '" src="/sites/default/files/styles/bc_image_' + dimensions.connect.width + 'x' + dimensions.connect.height + '/public'+ elem.image + '"';
  }
  return '<div class="views-row" nid="' + elem.nid + '"><article id="node-' + elem.nid + '" class="node node-bc-' + (elem.type ? elem.type : type) + ' node-teaser article clearfix">'
      + '<header class="node-header"><h1 property="dc:title" datatype="" class="node-title"><a href="' + elem.url + '" rel="bookmark" class="no-colorbox-node" data-inner-width="1060" data-inner-height="754">'
      + (type == 'article' ? elem.title + ' <span class="sub-title">' + elem.sub_title + '</span>' : 'title') + '</a></h1></header>'
      + '<div class="node-content"><div class="field field-name-field-bc-cover-image field-type-image field-label-hidden view-mode-teaser"><div class="field-items"><figure class="clearfix field-item even">'
      + '<a href="' + elem.url + '"><img width="' + dimensions.bc.width + '" '+ image +'></a></figure></div></div>'
      + content
      + '</div></article><div class="curtain" style="background-color:' + elem.curtain_color + '"></div></div>';
}
