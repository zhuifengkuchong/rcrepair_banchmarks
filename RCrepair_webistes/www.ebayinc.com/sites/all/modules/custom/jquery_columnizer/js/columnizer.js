(function($) {
  Drupal.behaviors.columnizer = {
    settings: {},
    attach: function(context) {
      this.settings = Drupal.settings.columnizer;
      var classes = ['columnizer-enabled', 'columnized-'+this.settings.columns];
      $(this.settings.selector).addClass(classes.join(' ')).columnize({
        columns: this.settings.columns,
        // width: 1080,
        // ignoreImageLoading: false
      });
    }
  }
})(jQuery);