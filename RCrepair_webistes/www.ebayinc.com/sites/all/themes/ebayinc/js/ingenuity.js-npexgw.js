/** Ingenuity Design **/

/**
 *
 *  Authors: Stephen Parente <stephen@ingenuitydesign.com> and Keith Nitsch <keith@ingenuitydesign.com>
 *  Made with love and many late nights in Norwalk, Connecticut.
 *
 *  Responsive menu made and inspired by Google Web Starter Kit
 *  Copyright 2014 Google Inc. All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 *
 * Other includes made by Pitch Interactive Data Visualizations
 */

var Ingenuity = {};

jQuery(function($) {
    (function () {
        'use strict';

        var navdrawerContainer = document.querySelector('.navdrawer-container');
        var appbarElement = document.querySelector('.app-bar');
        var menuBtn = document.querySelector('.menu');
        var main = document.querySelector('main');

        function closeMenu() {
            appbarElement.classList.remove('open');
            navdrawerContainer.classList.remove('open');
            jQuery('.has-drop-down').removeClass('hover');
        }

        function toggleMenu() {
            appbarElement.classList.toggle('open');
            navdrawerContainer.classList.toggle('open');
        }

        main.addEventListener('ontouchstart', closeMenu);
        main.addEventListener('click', closeMenu);
        menuBtn.addEventListener('click', toggleMenu);
        navdrawerContainer.addEventListener('click', function (event) {
            if (event.target.nodeName === 'A' || event.target.nodeName === 'LI') {
                //closeMenu();
            }
        });

    })();

    (function() {
        'use strict';
        var navItems = $('.has-drop-down > a', '#nav');
        navItems.each(function() {
            var href = $(this).attr('href');
            $(this).attr('data-href', href);
            $(this).attr('href', 'javascript: void(0);');
        })
        navItems.click(function(e) {
            console.log('this has been clicked');
            e.stopPropagation();
            /* now check if this one is already open */
            if ($(this).parents('.has-drop-down').hasClass('hover')) {
                //now go to the link
                window.location.href = $(this).data('href');
                return;
            } else {
                $('.has-drop-down').removeClass('hover');
                $(this).parents('.has-drop-down').addClass('hover');
            }
        });
        $('body').click(function() {
            $('.has-drop-down', '#nav').removeClass('hover');
        });
    })();

    Ingenuity.hash = function() {

        var dispatch = function() {
            //the doer
            var currentHash = read(),
                sel;

            if (currentHash.modal) {
                sel = jQuery('#'+currentHash.modal);
                if (sel)
                    sel.modal( 'show' );
            }

            if (currentHash.tab) {
                sel = jQuery('a[href^="#'+currentHash.tab+'"]')
                if (sel)
                    sel.click()
            }

        }

        var read = function() {
            //the reader
            var hash,
                hashObj,
                returnObj,
                splitPieces,
                index,
                value;

            hash = document.location.hash;
            //need to remove the first #
            if (hash) {
                hash = hash.substring(1);
                //hash array
                hashObj = hash.split("/");
                returnObj = {}
                for (x in hashObj) {
                    //we need to find if it is carrying a variable by splitting it by equals
                    if (hashObj[x].indexOf("=") > -1) {
                        //awesomeu
                        splitPieces = hashObj[x].split("=", 2);
                        index = splitPieces[0];
                        value = splitPieces[1];
                        if (typeof value == "string") {
                            if (value.indexOf(',') > -1) {
                                value = value.split(",");
                            }
                        }
                        if (value == false) continue;
                    } else {
                        index = hashObj[x];
                        value = true;
                    }
                    returnObj[index] = value;
                }
                return returnObj;
            } else return {};
        }

        var addHash = function(Path, override) {
            //the writer
            var returnObj,
                index,
                str,
                value;
            if (!override) returnObj = read();
            else returnObj = {};

            if (Path) {
                for (x in Path) {
                    if (Path[x]!=null) value = Path[x]; //value
                    else value = true;

                    index = x;
                    returnObj[index] = value; //index
                }
            }
            str = "#";
            for (x in returnObj) {
                if (returnObj[x]!=null) value = returnObj[x]; //value
                else value = true;

                if (value!=false) {
                    index = x;
                    str += index;
                    if (value) {
                        str += "="+value;
                    }
                    str += "/";
                }
            }
            //prune last one
            return str.substring(0, str.length-1);
        }

        return {
            dispatch: dispatch
        };

    }

    Ingenuity.hash().dispatch();
    jQuery('.modal').on('hidden.bs.modal', function(e) {
        var iFrame = jQuery('iframe', this);
        if (iFrame.length > 0) {
            iFrame.attr('src', iFrame.attr('src')); //reload iFrame
        }

    }).on('show.bs.modal', function(e) {
        stButtons.locateElements();
    });
    jQuery('.view-home-latest-news .views-field-title span').dotdotdot();

    var videoSegmentScroller = jQuery('.scroll', '.devin');
    if (videoSegmentScroller.length > 0) {
        (function ($) {
            videoSegmentScroller.perfectScrollbar({
                suppressScrollX:true
            });
        }(jQuery));
    }

    /*
     * Asyncronous loading of image assets
     */

    (function() {
        $('img[data-idsrc]').each(function() {
            var src = $(this).attr('data-idsrc');
            $(this).attr('src', src);
            $(this).show();
        });
    })();

    var pager = $('.pager');
    if (pager && pager.length > 0) {
        $('.view-footer').css('margin-top', '-36px');
    }
    var formFrom = $('#edit-created,#edit-created-1');
    if (formFrom.length > 0) {
        $('.view-filters').css('display', 'block');
        formFrom.datepicker();
        formFrom.parents('.views-exposed-widget').click(function() {
            $('input', this).focus();
        })
    }

    (function (i,s,o,g) {
        var x,y;
        x = s.createElement(o);x.src=i;y=s.getElementsByTagName(o)[0];
        y.parentNode.insertBefore(x,y); g();
    }('https://www.youtube.com/iframe_api', document, 'script', function() {

        var done = false;

        $('.youtube-flash-embed').each(function(index) {
            var videoId = $(this).attr('data-id');

            var ID = $(this).attr('id');
            if (!ID || !videoId) {
                console.log("Missing id or video id");
            } else {
                window.setTimeout(function() {
                //643 378
                var width = $('.col-md-8', '.slide-inner').width();
                var height = $('.ER-slideshow').height();

                var params = { menu: "false", allowScriptAccess: "always", allowfullscreen: "true", wmode: "opaque"};
                var atts = { id: ID };
                swfobject.embedSWF("https://www.youtube.com/v/" + videoId + "?enablejsapi=1&rel=0&playerapiid="+ID+"&version=3",
                    ID, width, height, "8", null, null, params, atts);

                }, 500);
            }
        });

    }));



})

function addControls(id) {
    if (id.indexOf('clone') > 1) return;
    var par = jQuery('#'+id).parent();
    if (par.children('.video-controls').length < 1) {
        console.log('adding controls for ' + id);
        jQuery('.video-controls').clone().prependTo(par);
    }

}

function onYouTubePlayerReady(id) {
    var yt = document.getElementById(id);
    var ytJQ = jQuery('#'+id);
    yt.addEventListener('onStateChange', 'onYouTubePlayerStateChange'); // onYouTubePlayerStateChange(newState)
    yt.addEventListener('onError', 'onYouTubePlayerError'); // onYouTubePlayerError(errorCode)

    var playback = {
        player: yt,
        first: true,
        play: function(override) {
            ytJQ.parents('.flexslider').flexslider('stop');
            this.playing = true;
            if (!override && this.first) {
                this.first = false;
                this.player.seekTo(0);
            }
            this.player.playVideo();
        },
        pause: function() {
            ytJQ.parents('.flexslider').flexslider('play');
            this.playing = false;
            this.player.pauseVideo();
        },
        playing: false,
        toggleVideo: function() {
            if (this.playing) this.pause();
            else this.play();
            return this;
        },
        removeButton: function() {
            this.player.seekTo(4);
            this.play(true);
            this.pause();
        },
        fullscreen: function() {
            console.log('requesting');
            this.player.webkitRequestFullScreen();
        }
    }
    //addControls(id);
    //playback.removeButton();

    var controls = ytJQ.parent().children('.video-controls');
    controls.children('.play-button').click(function() {
        jQuery(this).toggleClass('hidden-btn');
        playback.toggleVideo();

    });
    if (webkitSupportsFullscreen())
        playback.fullscreen();
    //yt.click(playback.toggleVideo);

}

function onYouTubePlayerStateChange(evt) {
    switch (evt) {
        case 1:
            jQuery('.flexslider').flexslider('stop');
            break;
        case 0:
            jQuery('.flexslider').flexslider('play');
            break;
    }
}

function onYouTubePlayerError(evt) {
    console.log(evt);
}

function webkitSupportsFullscreen() {
    return !(typeof document.webkitIsFullScreen == "undefined")
}