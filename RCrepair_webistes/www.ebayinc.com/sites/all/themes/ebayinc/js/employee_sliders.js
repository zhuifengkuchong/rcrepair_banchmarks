jQuery(function() {
	jQuery('.field-name-field-engage-slides-1').append(jQuery("<div class='engage_count'><p class='engageSlider1-CaptionLabel'>/p></div>"));	
	jQuery('.field-name-field-engage-slides-2').append(jQuery("<div class='engage_count'><p class='engageSlider2-CaptionLabel'></p></div>"));	
	
	jQuery('.field-name-field-engage-slides-1').attr('class', 'engageSlider1 engageSliderContainer col-md-10 col-sm-12 col-md-offset-1');
	jQuery('.field-name-field-engage-slides-2').attr('class', 'engageSlider2 engageSliderContainer col-md-10 col-sm-12 col-md-offset-1');
	jQuery('.engageSlider1 .item-list ul').attr('id', 'engageSlider1').attr('class', 'bxslider');
	jQuery('.engageSlider2 .item-list ul').attr('id', 'engageSlider2').attr('class', 'bxslider');
	jQuery('#engageSlider1 li, #engageSlider2 li').attr('class', 'engageSlide');
	
	var engageSlider1;
	var engageSlider2;
	
	var tmpCounter1 = jQuery('#engageSlider1 > li').length;
	jQuery('.engageSlider1-CaptionLabel').html("1 / " + tmpCounter1);
	
	var tmpCounter2 = jQuery('#engageSlider2 > li').length;
	jQuery('.engageSlider2-CaptionLabel').html("1 / " + tmpCounter2);
	
	engageSlider1 = jQuery('.engageSliderContainer #engageSlider1').bxSlider({
		pager: false,
		infiniteLoop: false,
		moveSlides: 1,
		onSlideAfter: function($slideElement, oldIndex, newIndex){
			jQuery('.engageSlider1-CaptionLabel').html( (newIndex+1) + ' / '+ engageSlider1.getSlideCount() );
		},
	});  
	engageSlider2 = jQuery('.engageSliderContainer #engageSlider2').bxSlider({
		pager: false,
		infiniteLoop: false,
		moveSlides: 1,	
		onSlideAfter: function($slideElement, oldIndex, newIndex){
			jQuery('.engageSlider2-CaptionLabel').html( (newIndex+1) + ' / '+ engageSlider2.getSlideCount() );
		},
	});   
});