// 

// var NTPT_PGEXTRA = '';
// var NTPT_PGREFTOP = false;
// var NTPT_NOINITIALTAG = false;

// NTPT_SET_IDCOOKIE, when set to true, activates the setting of the identification cookie.
// Setting this variable globally is recommended.
var NTPT_SET_IDCOOKIE = true;

// NTPT_PGCOOKIES is an array of cookie names to be captured specifically for this page.
var NTPT_PGCOOKIES = [ "csx1", "csx2" ];

// For example, the following lines create the cookies, named in NTPT_PGCOOKIES, to be captured for this page.
document.cookie = "csx1=testvalue1; path=/";
document.cookie = "csx2=testvalue2; path=/";

function unicaFunctions(command, args) {
	if (null == args) {
		return;
	}
	var tmpargs = args.split(",");
	if (command == "ntptEventTag") {
		(0 == tmpargs[0].length) ? ntptEventTag() : ntptEventTag(tmpargs[0]);
	} else if (command == "ntptAddPair") {
		if ( 2 != tmpargs.length ) {
			return;
		} else {
			ntptAddPair(tmpargs[0], tmpargs[1]);
		}
	} else if (command == "ntptDropPair") {
		if ( 1 != tmpargs.length ) {
			return;
		} else {
			ntptDropPair(tmpargs[0]);
		}
	}
}

function FlashTracking(func, args) {
	ExternalInterface.call("unicaFunctions", func, args);
}