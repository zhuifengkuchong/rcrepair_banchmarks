$(document).ready(function () {
	MODEL.feedContent();
});

var ECUST = typeof ECUST === 'undefined' ? {} : ECUST;
var MODEL = typeof MODEL === 'undefined' ? {} : MODEL;

ECUST.bindMyAccountForm = function(binding) {
	ECUST.bindForm({
		'binding': binding,
		'atgComponent': '/com/charter/userprofiling/existingcustomer/AuthenticationFacade',
		'atgMethod':'authenticateWithMyAccount',
		'arguments': [
              'username',
              'password',
              'zipcode',
              'livingAt'
        ],
        'success': ECUST.handleLoginResponse
	});
}



ECUST.bindSecurityCodeForm  = function(binding) {
	ECUST.bindForm({
		'binding': binding,
		'atgComponent': '/com/charter/userprofiling/existingcustomer/AuthenticationFacade',
		'atgMethod':'authenticateWithSecurityCode',
		'arguments': [
              'securitycode',
              'accountnumber',
              'livingAt'
        ],
        'success': ECUST.handleLoginResponse
	});
}

ECUST.bindExplicitSecurityCodeValidationForm  = function(binding) {
	ECUST.bindForm({
		'binding': binding,
		'atgComponent': '/com/charter/userprofiling/existingcustomer/AuthenticationFacade',
		'atgMethod':'authenticateWithExplicitSecurityCodeValidation',
		'arguments': [
              'securitycode',
              'accountnumber',
              'validateSecurityCode',
              'livingAt'
        ],
        'success': ECUST.handleLoginResponse
	});
}

ECUST.bindPersonalInfoForm  = function(binding) {
	ECUST.bindForm({
		'binding': binding,
		'atgComponent': '/com/charter/userprofiling/existingcustomer/AuthenticationFacade',
		'atgMethod':'authenticateWithPersonalInfo',
		'arguments': [
              'lastname',
              'phonenumber',
              'livingAt'
        ],
        'success': ECUST.handleLoginResponse
	});
}

ECUST.bindLogoutForm  = function(binding) {
	ECUST.bindForm({
		'binding': binding,
		'atgComponent': '/com/charter/userprofiling/existingcustomer/AuthenticationFacade',
		'atgMethod':'logout',
		'arguments': [],
        'success': ECUST.handleLogoutResponse
	});
}



ECUST.bindMutualLoginButton= function(buttonId, formIds){

	$('#'+buttonId).on('click', function(){
		for(i = 0; i < formIds.length; ++i) {
			var $form = $('#'+formIds[i]);			
			var $inputs = $form.find('input').filter(function(){
				$input=$(this);
				return $input.attr("isemptyfield")===undefined && $input.val()  ;
			});
			$form.forceValidate();
			if($inputs.length>0 && $form.data('isValidateOk') === 'true'){
				$form.submit();
				break;
			}
		}
		return false;
	});
}

ECUST.handleLoginResponse = function(data,binding) {
	if (data.success) {
		window.location = data.redirectURL; 
	} else {
		var errMessage = "<ul>";
		for (index = 0; index < data.errors.length; ++index) {
			errMessage += "<li>"+data.errors[index].message+"</li>";
			var $errorDivision = $("#"+binding.errorId);
			$errorDivision.html(errMessage);
			$errorDivision.show();
		}
		errMessage += "</ul>"
	}
}


ECUST.handleLogoutResponse = function(data,binding) {
	if (data.success) {
		window.location = data.redirectURL;
	} else {
		for (index = 0; index < data.errors.length; ++index) {
			var errMessage = data.errors[index].message;
			var $errorDivision = $("#"+binding.errorId);
			$errorDivision.html(errMessage);
			$errorDivision.show();
		}
	}
}

/* -- START: GENERIC REST CODE -- */
ECUST.bindForm = function(formContext) {
	var $form = $('form#'+formContext.binding.formId);
	formContext.binding['form'] = $form;
	$form.validator({
		'submitButtonId': formContext.binding.buttonId,
		'onValidateOK':function() {  
			$form.data('isValidateOk', 'true');
		},
		'onValidateError': function() {
			$form.data('isValidateOk', 'false');
        }
	});
	
	$form.on('submit', function(){
		if($form.data('isValidateOk') === 'true') {
			$("#"+formContext.binding.errorId).hide();
			
			ECUST.submitForm(formContext);
		}
		return false;
	});
}

ECUST.submitForm = function(formContext) {
	ECUST.showLoadingIndicator(true);
	var $form = formContext.binding.form;
	
	var inputData={
			'atg-rest-depth':'2'
	};
	
	for(i = 0; i < formContext.arguments.length; ++i) {
		inputData['arg'+(i+1)]=$form.find("[name='"+formContext.arguments[i]+"']").val();
	}

	$.ajax({
		type: 'POST',
		url : '/rest/bean'+formContext.atgComponent+'/'+formContext.atgMethod,
		contentType : 'application/json',      
		data        : JSON.stringify(inputData),
        success: function (data){
        	ECUST.showLoadingIndicator(false);
        	formContext.success(data,formContext.binding);
        },
        error: function (){
        	ECUST.showLoadingIndicator(false);
        }
	});
}

var LOADING_MODAL = typeof $.ModalWindow === 'undefined' ? null : LOADING_MODAL;

ECUST.showLoadingIndicator = function(show) {
	if(LOADING_MODAL === undefined || LOADING_MODAL === null){
		LOADING_MODAL = new $.ModalWindow('loading-indicator');
	}
	if(show){
		LOADING_MODAL.open();
	} else {
		LOADING_MODAL.close();
	}
}
/* -- END: GENERIC REST CODE -- */

/*Login box interaction*/

ECUST.bindLoginBoxBehaviour = function (){

	var $mainContainer=$('div.main-nav');
	var $loginBox = $mainContainer.find('.alt-login');
	var $optionsList = $mainContainer.find('.localize-options ul');
	var $expandButton = $mainContainer.find('.deploy-options');
	var $collapseButton = $mainContainer.find('.collapse-options');
	var $localizationBox = $mainContainer.find('.login');
	var $signInButton = $mainContainer.find('#sign-in');
	var $goBackButton = $mainContainer.find('.go-back');
	var $errorBox = $mainContainer.find('div.error-box');
	
	
	$expandButton.live('click', function (event) {
		event.preventDefault();
		ECUST.toggleLocalizationBoxOptions($optionsList,$collapseButton,$expandButton); 
	});
	$collapseButton.live('click', function (event) {
		event.preventDefault();
		ECUST.toggleLocalizationBoxOptions($optionsList,$collapseButton,$expandButton); 
	});
	$signInButton.live('click', function (event) {
		event.preventDefault();
		ECUST.toggleLoginBox($localizationBox, $loginBox, $errorBox);
	});
	$goBackButton.live('click', function (event) {
		event.preventDefault();
		ECUST.toggleLoginBox($localizationBox, $loginBox, $errorBox);
	});
	
	ECUST.toggleLocalizationBoxOptions($optionsList,$collapseButton,$expandButton); 
}


ECUST.toggleLocalizationBoxOptions = function($optionsList, $collapseButton, $expandButton ){
	$optionsList.slideToggle('slow',function(){
		if ($optionsList.is(':hidden')) {
			$collapseButton.show();
			$expandButton.hide();
		} else {
			$collapseButton.hide();
			$expandButton.show();
		}
	})
};
ECUST.toggleLoginBox = function($localizationBox, $loginBox, $errorBox ){
	if ($loginBox.is(':hidden')) {
		$errorBox.hide();
		$localizationBox.hide();
		$loginBox.show();
	} else {
		$localizationBox.show();
		$loginBox.hide();
		$errorBox.hide();
	}
};

MODEL.feedContent= function(){
	if(typeof _data_model === 'undefined' ){
		return;
	}

	var data = MODEL.listModelProps(null , _data_model);
	
	$containers = $(".bind-data-model");
	for(var i=0; i<data.length ; ++i){
		//console.log(data[i].key+'   =  '+data[i].value);
		$containers.find("[name='"+data[i].key+"']").html(data[i].value);
	}
	
}

MODEL.listModelProps = function(prefix, dataModel){
	if(prefix == null ){
		prefix = '';
	}else{
		prefix+='.';
	}
	
	var list = [];	
	for (var prop in dataModel) {
		var propValue = dataModel[prop];
		var propKey = prefix+prop;
		if(propValue instanceof Object ){
			list=list.concat(MODEL.listModelProps(propKey, propValue));
		}else{
			var keyValue= {key : propKey, value : propValue};
			list.push(keyValue);
			//console.log(keyValue);
		}
	}
	return list;
}
