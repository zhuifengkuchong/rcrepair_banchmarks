if (typeof String.prototype.trim !== 'function') {
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g, ''); 
	}
}

(function($) {
	var ValidatorPlugin = function(element, options) {

		var defaults = {
			'label': true,
			'requiredMarkPlacement': 'label',
			'errorClassPlacement': 'label',
			'onValidateOK': null,
			'onValidateError': null,
			'onFormSubmit' : null,
			'submitButtonId' : 'submit-form',
			'markerClass' : 'error'
		};
		
		var settings = $.extend(defaults, options || {});
		var $form = $(element);
		var $requiredFields = $form.find('input.required, select.required, textarea.required');
		var $optionalFields = $form.find('input.optional, textarea.optional');
		var $labels = $form.find('label');
		var $submitButton = $form.find('input[type=submit]').length !== 0 ? $form.find('input[type=submit]') : $('#' + settings.submitButtonId);
		var skipInput = $submitButton.data('skipInput');	
		var triedSubmission = false;
		var checkers = CHARTER.VALIDATORS;
		var $firstError = null;
		var oldErrorsCounter = 0;
		
		var validateMiniFun = function validateMiniFun() {
			if (triedSubmission) {
				validate(false);
			}
		};
		
		var validateOneField = function validateOneField() {
			if (!triedSubmission) {
				validateField(this);
			}
		}

		var setProps = function($el) {
			$el.each(function(n) {
				var $item = $(this);
				$item.data({
					'name': $item.attr('name'),
					'type': $item.get(0).nodeName.toLowerCase() !== 'input' ? $item.get(0).nodeName.toLowerCase() : $item.attr('type'),
					'$label': typeof $item.attr('data-label') !== 'undefined' ? $($item.attr('data-label')) : $labels.filter('[for="' + $item.attr('id') + '"]'),
					'errorText': typeof $item.attr('data-error-text') !== 'undefined' ? $item.attr('data-error-text') : 'This field is required',
					'$relatedFields': typeof $item.attr('data-related-fields') !== 'undefined' ? $($item.attr('data-related-fields')): []
				});

				var i;
				var changeExists = false;
				var keyupExists = false;
				var blurExists = false;
				
				
				if ($item.data('events') && $item.data('events').change) {
					for (i = 0; i < $item.data('events').change.length; i++) {
						if ($item.data('events').change[i].handler === validateMiniFun) {
							changeExists = true;
							break;
						}
					}
				}
				if ($item.data('events') && $item.data('events').keyup) {
					for (i = 0; i < $item.data('events').keyup.length; i++) {
						if ($item.data('events').keyup[i].handler === validateMiniFun) {
							keyupExists = true;
							break;
						}
					}
				}
				if ($item.data('events') && $item.data('events').blur) {
					for (i = 0; i < $item.data('events').blur.length; i++) {
						if ($item.data('events').blur[i].handler === validateOneField) {
							blurExists = true;
							break;
						}
					}
				}
				// console.log(changeExists)
				// console.log(keyupExists)
				if (!changeExists) {
					$item.bind('change', validateMiniFun);
				}
				if (!keyupExists) {
					$item.bind('keyup', validateMiniFun);
				}
				if (!blurExists) {
					$item.bind('blur', validateOneField);
				}				
			});
		};
		
		setProps($requiredFields);
		setProps($optionalFields);

		// bind form submit
		if ($form.get(0).nodeName.toLowerCase() === 'form') {
			$form.bind('submit', function() {
				var result = validate(true);
				if (!result) {
	        	    gotoErrorAnchor();
				}
	        	return result;

			});
			$submitButton.bind('click', function(e) {
				var skipInput = $(this).data('skipInput');
				if(skipInput !== undefined) {
					var filterExpr = ":not('input[type=" + skipInput + "]')";
					$requiredFields = $requiredFields.filter(filterExpr);
				}
				$form.trigger('submit');
				e.preventDefault();
			});
		} else {
			$submitButton.bind('click', function(e) {
				var skipInput = $(this).data('skipInput');
				if(skipInput !== undefined) {
					var filterExpr = ":not('input[type=" + skipInput + "]')";
					$requiredFields = $requiredFields.filter(filterExpr);
				}
				var result = validate(true);
				if (!result) {
	        	    gotoErrorAnchor();
				}
	        	return result;
			});
		}
		
		var addErrorMarker = function($item, type) {
			switch (type) {
				case 'text':
				case 'select':
					$item.addClass(settings.markerClass);
				case 'checkbox':
				case 'radio':
				default:
					$item.data('$label').addClass(settings.markerClass);
			}
			if ($item.attr('data-errMsgId') !== 'undefined') {
				$errMsgId = $($item.attr('data-errMsgId'));
				$errMsgId.removeClass('hidden');
			}
			

		};

		var removeErrorMarker = function($item, type) {
			switch (type) {
				case 'text':
				case 'select':
					$item.removeClass(settings.markerClass);
				case 'checkbox':
				case 'radio':
				default:
					$item.data('$label').removeClass(settings.markerClass);
			}
			if ($item.attr('data-errMsgId') !== 'undefined') {
				$errMsgId = $($item.attr('data-errMsgId'));
				$errMsgId.addClass('hidden');
			}

		};

		var validateRequiredField = function($input, errors, fieldsInError) {
			var type = $input.data('type');
			var name = $input.data('name');
			var rules;
			var fieldErrors = 0;
			var value = $input.val().length === 0 ? '' : $input.val().trim();

			if (type === 'text' || type === 'select' || type === 'number') {
				if ($input.attr('data-validate') !== undefined) {
				    rules = $input.attr('data-validate').split(' ');
			    }
			    if (!checkers['has-value'].apply(this, [value])) {
				    fieldErrors++;
			    }
			    if (fieldErrors === 0) {
				    fieldErrors += checkValidationRules(value, rules);
			    }
			    if (fieldErrors > 0) {
				    errors++;
				    addErrorMarker($input, 'text');
				    if (fieldsInError) {
					    fieldsInError.push($input.attr('name'));
				    }
			    } else {
				    removeErrorMarker($input, 'text');
			    }
		    } else if (type === 'checkbox') {
		    	if (!$input.is(':checked')) {
				    errors++;
				    addErrorMarker($input, 'checkbox');
				    if (fieldsInError) {
					    fieldsInError.push($input.attr('name'));
				    }
		    	} else {
		    		removeErrorMarker($input, 'checkbox');
                }
		    } else if (type === 'radio') {
		    	var filter = $requiredFields.filter('[name=' + name + ']:checked');
		    	if (filter.length === 0) {
		    		errors++;
		    		addErrorMarker($input, 'radio');
				    if (fieldsInError) {
					    fieldsInError.push($input.attr('name'));
				    }
		    	} else {
		    		removeErrorMarker($input, 'radio');
		    	}
		    }
			return errors;
		}

		var validateOptionalField = function($input, errors, fieldsInError) {
			var type = $input.data('type');
			var name = $input.data('name');
			var rules;
			var fieldErrors = 0;
			var value = $input.val().length === 0 ? '' : $input.val().trim();

			if (type === 'text' || type === 'number') {
			    if ($input.attr('data-validate') !== undefined) {
				    rules = $input.attr('data-validate').split(' ');
			    }

			    if (value.length > 0) {
				    fieldErrors += checkValidationRules(value, rules);
			    }

			    if (fieldErrors > 0) {
				    errors++;
				    addErrorMarker($input, 'text');
				    if (fieldsInError) {
					    fieldsInError.push($input.attr('name'));
				    }
			    } else {
				    removeErrorMarker($input, 'text');
			    }
		    }
			return errors;
		}

		var validateField = function(input) {
			var $input = $(input);
			var errors = 0;
			
			if ($input.hasClass('required')) {
				validateRequiredField($input, errors, null);
			} else if ($input.hasClass('optional')) {
				validateOptionalField($input, errors, null);
			}
		}
		
		var validate = function(isFormSubmit) {
			if (!triedSubmission) {
				triedSubmission = true;	
			}
			var errors = 0;
			
			var fieldsInError = [];
			
			$requiredFields.each(function() {
				errors += validateRequiredField($(this), errors, fieldsInError);
			});

			$optionalFields.each(function() {
				errors += validateOptionalField($(this), errors, fieldsInError);
			});

			if (errors > 0) {
				$firstError = findFirstUserActionBarErrorMsg(fieldsInError);
				if (typeof settings.onValidateError === 'function') {
					settings.onValidateError(errors, isFormSubmit, oldErrorsCounter);
				}
			} else {
				$firstError = null;
				if (typeof settings.onValidateOK === 'function') {
					settings.onValidateOK();
				}
				if (isFormSubmit && typeof settings.onFormSubmit === 'function') {
					settings.onFormSubmit();
				}
			}
			$('.content-block-wrapper')
				.removeClass('wrapper-error')
				.has('.error')
				.addClass('wrapper-error');
			oldErrorsCounter = errors;
			return errors === 0;
		};
		
		var gotoErrorAnchor = function() {
			if ($firstError != null && $('#errorUserActionBarMsg')) {
			    $('#errorUserActionBarMsg').html($firstError.html());
			    if ($firstError.attr('data-anchor-id')) {
			        location.hash = '#' + $firstError.attr('data-anchor-id');
			    }
			}
		}

		var findFirstUserActionBarErrorMsg = function(fieldsInError) {
			var $errorSelectors = $('.uab-errMsg');
			var errorsArray = $.makeArray( $errorSelectors );
			
			var $localFirstError = null;
			for (var i = 0; i < errorsArray.length; i++) {
			    $errorDiv = $(errorsArray[i]);
				for (var j = 0; j < fieldsInError.length; j++) {
					if ($errorDiv.attr('data-field-name') == fieldsInError[j]) {
						$localFirstError = $errorDiv;
						break;
					}
				}
				if ($localFirstError != null) {
					break;
				}
			}
			
			return $localFirstError;
		}

		var checkValidationRules = function(value, rules) {
			var fieldErrors = 0;
			
			for (var i in rules) {
				var args;
				if (rules[i].indexOf(':') !== -1) {
					args = rules[i].split(':')[1].split(',');
					args.splice(0, 0, value);
				} else {
					args = [value];
				}
				try {
					if (!checkers[rules[i].replace(/:.*/, '')].apply(this, args)) {
						fieldErrors++;
						break;
					}
				} catch(e) {
					window.console && console.log('checker "' + rules[i].replace(/:.*/, '') + '" has not been defined');
				}
			};
			
			return fieldErrors;
		};
		
		// Public method
		this.reassess = function() {
			$requiredFields = $form.find('input.required, select.required, textarea.required');
			setProps($requiredFields);
			
			$optionalFields = $form.find('input.optional, textarea.optional')
			setProps($optionalFields);
		};
		
		// Public method
		this.forceValidate = function() {
			validate(false);
		};
	};
	
	$.fn.validator = function(options) {
		return this.each(function() {
            var element = $(this);

            // Return early if this element already has a plugin instance
            if (element.data('validator-plugin')) return;

            // Pass options to plugin constructor
            var validatorPlugin = new ValidatorPlugin(this, options);

        	// Store plugin object in this element's data
            element.data('validator-plugin', validatorPlugin);
		});
	};
	
	$.fn.reassessValidator = function(options) {
		return this.each(function() {
            var element = $(this);
            var validatorPlugin = element.data('validator-plugin');

            // Return early if this element has no plugin instance
            if (validatorPlugin == undefined) return;

			validatorPlugin.reassess();
		});
	};
	
	$.fn.forceValidate = function(options) {
		return this.each(function() {
            var element = $(this);
            var validatorPlugin = element.data('validator-plugin');

            // Return early if this element has no plugin instance
            if (validatorPlugin == undefined) return;

			validatorPlugin.forceValidate();
		});
	};
	
})(jQuery);
