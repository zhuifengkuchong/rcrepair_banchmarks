function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + "; " + expires;
}
function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1);
		if (c.indexOf(name) == 0)
			return c.substring(name.length, c.length);
	}
	return "";
}
if (navigator.appName.indexOf("Internet Explorer") != -1) { //using IE
	var badBrowser = (
		navigator.appVersion.indexOf("MSIE 9") == -1 && //v9 is ok
		navigator.appVersion.indexOf("MSIE 1") == -1 && //v10, 11, 12, etc. is fine too
		navigator.appVersion.indexOf("MSIE 2") == -1 //v20, 21, 22, etc. is fine too
	);
	if (badBrowser) {
		var isie8 = getCookie('ie8user');
		if (isie8 == '') {
			alert('You are using an outdated version of Internet Explorer. Some features of Boeing.com may not display as intended. For the best experience, please use an alternative web browser.');
			setCookie('ie8user', 'yes', 7);
		}
	}
}
