(function ($) {
Drupal.settings.views = Drupal.settings.views || {'ajax_path': '/views/ajax'};

Drupal.quicktabs = Drupal.quicktabs || {};

Drupal.quicktabs.getQTName = function (el) {
  return el.id.substring(el.id.indexOf('-') +1);
}

Drupal.behaviors.quicktabs = {
  attach: function (context, settings) {  
    
  /*-------------------Footer Starts-----------------------------*/  
  /*********TRW.COM Dev team unfortunately need to override this as per Requirement*********************/
  	$('.popup-holder').hide();
	$('.f-popup img').hide();
	$.extend(true, Drupal.settings, settings);
    $('#footer', context).once(function(){	
      Drupal.quicktabs.footer(this);
    });
  
  /*-------------------Tabbed Slider Start------------------------------------*/ 
    
    $('.quicktabs-wrapper', context).once(function(){
	
      Drupal.quicktabs.prepare(this);
    });	
	return false;
  }
}

// Setting up the inital behaviours
/*********TRW.COM Dev team unfortunately need to create Drupal.quicktabs.footer this as per Requirement*********************/
Drupal.quicktabs.footer = function(el) {


 var $ul_left = $(el).find('http://www.trw.com/sites//all//modules//quicktabs//js//ul.left');  
 $ul_left.find('li a').each(function(i, element){		
		$("#footer .popup-tr-"+(i+1)).click(function(){	
		
		if($('.popup-holder-'+(i+1)).is(":visible")){ //alert('Show');
		
			$('.popup-holder-'+(i+1)).slideToggle();
			$("html,body").animate({ scrollTop: 1000000 });
			$('#footer').toggleClass('active-footer');
			
		}else{
			li_size=$(".left li").size();		
			for(var count=1;count<=li_size;count++){
				$('.popup-holder-'+count).hide();				
			}
			
			$('.popup-holder-'+(i+1)).slideToggle();
			$("html,body").animate({ scrollTop: 1000000 });
			$('#footer').toggleClass('active-footer');
			}
				   		
		return false;
			    
	   });  
	}); 
 
}



// Setting up the inital behaviours
Drupal.quicktabs.prepare = function(el) {

  // el.id format: "quicktabs-$name"
  var qt_name = Drupal.quicktabs.getQTName(el);   
//  alert(qt_name); tabbed_slider
  
  var $ul = $(el).find('ul.quicktabs-tabs:first');
   $ul.find('li').each(function(i, element){     
	$(element).addClass('img-'+i);    
  });
  var $ul = $(el).find('ul.quicktabs-tabs:first');

/* code snippet for Tab slider short title and entire slot click starts here for ajax off*/
$('#quicktabs-tabpage-tabbed_slider-0 > div > div > div > div > div > div > div > ul ').find('li').each(function(i){

	var href_whole = $('#quicktabs-tabpage-tabbed_slider-0 > div > div > div > div > div > div > div > ul li[jcarouselindex=1] div[class="tab-slider-title"] a').attr('href');
	jQuery('#quicktabs-tabbed_slider > div ul li[class^= "active first"] a').attr('href',href_whole);
	jQuery('#quicktabs-tabbed_slider > div ul li[class^= "first"] a').attr('href',href_whole);
	
});
/*
//Function for the first tab in tabbed slider
$('#quicktabs-tabpage-tabbed_slider-0 > div > div > div > div > div > div > div > ul ').find('li').each(function(i){

	//Get the feild uri for the image
	$('#quicktabs-tabpage-tabbed_slider-0 > div > div > div > div > div > div > div > ul li[jcarouselindex='+(i+1)+'] div[class="views-field views-field-field-field-uri"] ').hide();
	var field_image = $('#quicktabs-tabpage-tabbed_slider-0 > div > div > div > div > div > div > div > ul li[jcarouselindex='+(i+1)+'] div[class="views-field views-field-field-field-uri"] div').html();
	
	field_image = field_image.replace("https","http"); //Replaced https by http for avoiding the image loading issues
	$('#quicktabs-tabpage-tabbed_slider-0 > div > div > div > div > div > div > div > ul li[jcarouselindex='+(i+1)+'] div[class="views-field views-field-field-field-uri"]').html("<img src='"+ field_image+ "' style='width: 250px; height: 150px;'/>");
	$('#quicktabs-tabpage-tabbed_slider-0 > div > div > div > div > div > div > div > ul li[jcarouselindex='+(i+1)+'] div[class="views-field views-field-field-field-uri"] ').show();
	
	// External link for the content
	$('#quicktabs-tabpage-tabbed_slider-0 > div > div > div > div > div > div > div > ul li[jcarouselindex='+(i+1)+'] div[class="views-field views-field-field-external-link"] ').hide();
	var href_whole = $('#quicktabs-tabpage-tabbed_slider-0 > div > div > div > div > div > div > div > ul li[jcarouselindex='+(i+1)+'] div[class="views-field views-field-field-external-link"] div').html();
	
	href_whole = href_whole.split('?')[0] + '?utm_source=mainTRW&utm_medium=web&utm_campaign=Safety_News';
	
	// Wrapping the whole jcarousal element to <a></a>
	$('#quicktabs-tabpage-tabbed_slider-0 > div > div > div > div > div > div > div > ul li[jcarouselindex='+(i+1)+']').wrapInner("<a href='"+ href_whole+ "' target='_blank'></a>");
	jQuery('#quicktabs-tabpage-tabbed_slider-0 > div > div > div > div > div > div > div > ul li a').css('text-decoration','none');

});*/

//Function for all the tabs except first tab in tabbed slider	
jQuery('#quicktabs-tabbed_slider > div > ul').find('li').each(function(j){
	$('#quicktabs-tabpage-tabbed_slider-'+(j+1)+' > div > div > div > div > div > div > div > ul ').find('li').each(function(i){
		
		//Hiding the long title
		$('#quicktabs-tabpage-tabbed_slider-'+(j+1)+' > div > div > div > div > div > div > div > ul li[jcarouselindex='+(i+1)+'] div[class="tab-slider-title"] a').hide();
		
		// Getting the external url
		$('#quicktabs-tabpage-tabbed_slider-'+(j+1)+' > div > div > div > div > div > div > div > ul li[jcarouselindex='+(i+1)+'] div[class="views-field views-field-field-external-url"] ').hide();
		var field_value = $('#quicktabs-tabpage-tabbed_slider-'+(j+1)+' > div > div > div > div > div > div > div > ul li[jcarouselindex='+(i+1)+'] div[class="views-field views-field-field-external-url"] div').html();
		
		//Checking for external url if any
		if(field_value== ' '||field_value== ''||field_value== null){
			var href_whole = $('#quicktabs-tabpage-tabbed_slider-'+(j+1)+' > div > div > div > div > div > div > div > ul li[jcarouselindex='+(i+1)+'] div[class="tab-slider-title"] a').attr('href');
		}else{
			var href_whole = $('#quicktabs-tabpage-tabbed_slider-'+(j+1)+' > div > div > div > div > div > div > div > ul li[jcarouselindex='+(i+1)+'] div[class="views-field views-field-field-external-url"] div').html();
		}
		
		// Wrapping the whole jcarousal element to <a></a>
		$('#quicktabs-tabpage-tabbed_slider-'+(j+1)+' > div > div > div > div > div > div > div > ul li[jcarouselindex='+(i+1)+']').wrapInner("<a href='"+ href_whole+ "'></a>");
		jQuery('#quicktabs-tabpage-tabbed_slider-'+(j+1)+' > div > div > div > div > div > div > div > ul li a').css('text-decoration','none');
	
	});
});
/* code snippet for Tab slider short title ends here */

	$ul.find('li a').each(function(i, element){
	
    element.myTabIndex = i;
    element.qt_name = qt_name;
    var tab = new Drupal.quicktabs.tab(element);
    var parent_li = $(element).parents('li').get(0);	
    if ($(parent_li).hasClass('active')) { 	  
      $(element).addClass('quicktabs-loaded');
    }
	
	
	
    /*********TRW.COM Dev team unfortunately need condition for this as per Requirement*********************/
	if(qt_name == 'tabbed_slider')  {			
	    $(element).once(function() {$(this).bind('mouseenter', {tab: tab}, Drupal.quicktabs.clickHandler);});			
	}else{
		$(element).once(function() {$(this).bind('click', {tab: tab}, Drupal.quicktabs.clickHandler);});
	}
  });
}


Drupal.quicktabs.clickHandler = function(event) { 
  var tab = event.data.tab;
  var element = this;
  // Set clicked tab to active.
  $(this).parents('li').siblings().removeClass('active');
  $(this).parents('li').addClass('active');  
  // Show footer tab container
   tab.container.removeClass('quicktabs-hide');
  // Hide all tabpages.
  tab.container.children().addClass('quicktabs-hide');
  
  if (!tab.tabpage.hasClass("quicktabs-tabpage")) {
    tab = new Drupal.quicktabs.tab(element);
	
  }

  tab.tabpage.removeClass('quicktabs-hide');
  return false;
}

// Constructor for an individual tab
Drupal.quicktabs.tab = function (el) {
  this.element = el;
  this.tabIndex = el.myTabIndex;
  var qtKey = 'qt_' + el.qt_name;
  var i = 0;
  for (var key in Drupal.settings.quicktabs[qtKey].tabs) {
    if (i == this.tabIndex) {
      this.tabObj = Drupal.settings.quicktabs[qtKey].tabs[key];
      this.tabKey = key;
    }
    i++;
  }
  this.tabpage_id = 'quicktabs-tabpage-' + el.qt_name + '-' + this.tabKey;
  this.container = $('#quicktabs-container-' + el.qt_name);
  this.tabpage = this.container.find('#' + this.tabpage_id);
}

if (Drupal.ajaxtabs) {
  /**
   * Handle an event that triggers an AJAX response.
   *
   * We unfortunately need to override this function, which originally comes from
   * misc/ajaxtabs.js, in order to be able to cache loaded tabs, i.e. once a tab
   * content has loaded it should not need to be loaded again.
   *
   * I have removed all comments that were in the original core function, so that
   * the only comments inside this function relate to the Quicktabs modification
   * of it.
   */
  Drupal.ajaxtabs.prototype.eventResponse = function (element, event) {
    var ajax = this;

    if (ajax.ajaxing) {
      return false;
    }
  
    try {
      if (ajax.form) {
        if (ajax.setClick) {alert(element);
          element.form.clk = element;
        }
  
        ajax.form.ajaxSubmit(ajax.options);
      }
      else {
        // Do not perform an ajax request for already loaded Quicktabs content.
        if (!$(element).hasClass('quicktabs-loaded')){
          ajax.beforeSerialize(ajax.element, ajax.options);
          $.ajax(ajax.options);		  
          if ($(element).parents('ul').hasClass('quicktabs-tabs')) {
            $(element).addClass('quicktabs-loaded');
			
          }
        }
      }
    }
    catch (e) {
      ajax.ajaxing = false;
      alert("An error occurred while attempting to process " + ajax.options.url + ": " + e.message);
    }
    return false;
  };
}


})(jQuery);
