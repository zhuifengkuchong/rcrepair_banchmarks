
function bubblediv(selector){
	var bubble = '';
	if(selector == '.view-calendar'){
		bubble += '<div id="select_country" class="event-bubble">';
	}
	else{
		bubble += '<div id="select_country" class="bubble">';
	}
	
	jQuery(selector + ' .views-exposed-form .views-widget').find('select option').each(function(i){
		if(jQuery(this).is(':selected')){
			bubble += '<div class="bubblediv active">'+jQuery(this).text()+'</div>';
		}
		else{
			bubble += '<div class="bubblediv">'+jQuery(this).text()+'</div>';
		}
		
	});
	bubble += '</div>';
	//console.log(bubble);
	return bubble;
}

function exposed_form_submit(selector,selector_submit){
	jQuery(selector + ' #select_country .bubblediv').live('click', function(){
		
		var text = jQuery(this).text();
		var view_widget = jQuery(selector + ' .views-exposed-form').find('.views-widget div');
		
		jQuery(selector + '#select_country').find('div.active').removeClass("active");
		jQuery(this).addClass("active");
		view_widget.find('select option').each(function(i){			
			jQuery(this).removeAttr('selected');
			if(jQuery(this).text() == text){
				jQuery(this).attr('selected','selected');
			}
		});
		jQuery(selector + ' '+ selector_submit).trigger("click");
	});
}