// use browserAlert as namespace
// defined it in case it does not exist
(function(undefined) {

    this.BrowserAlert = this.BrowserAlert || {};
	
}).call(window);

(function(undefined) {
	
	var fn;
	
	// Constructor
	this.AlertBox = function() {	
    this._$win = $(window);
		this._$alertbox = null;
		this._$container = null;
		this._$closeButton = null;
		this._$closeOkButton = null;
		this._width = 400;
		this._buildUI();
		this._registerEventListeners();
	};
	
	// shorthand for AlertBox prototype
	fn = this.AlertBox.prototype;
	
	// pop up AlertBox
	fn.pop = function(title,message,submitText) {
		var lb = this;
		lb._$alertbox.fadeIn('fast', function() {
			lb._fillMessage(title,message,submitText);
            lb._centerContent();
			lb._showContent();
		});
	};

	fn._fillMessage = function(title,message,submitText) {
    var messageTitle = $('#MessageHeader');
    messageTitle.html(unescape(title));
	var text = $('#MessageText');
    text.html(unescape(message));
    
    var SubmitBtn = $('#submitButton');
    SubmitBtn.text(unescape(submitText));
	};

	// dismiss AlertBox
	fn.dismiss = function() {
		this._$alertbox.remove();
	};


	fn.checkBrowserSupport = function() {
		        // check if cookie not exist
		            var isValid = false;
                 	if($.browser.msie==true)
                    {                        
                        if($.browser.version >= 7)
                        {
                           isValid = true;
                        }
                    }
                    else if($.browser.mozilla==true)
                    {
                        // for firefox 3.6 Gecko version is 1.9.2.*
                        var firefox36 = [1,9,1];
                        var currentVersion = $.browser.version.split(".",3);
                        // checking if the browser's gecko version is greater than 1.9.1
                        for (i=0;i<3;i++)
                        {
                            if(currentVersion[i]>firefox36[i])
                            {
                                isValid = true;
                                break;
                            }
                            else if(currentVersion[i] < firefox36[i])
                            {
                                isValid=false;
                                break;
                            }
                            // continue loop in case currentVersion[i] == firefox36[i]
                        }
                    }
               
                    // safari and chrome both return $.browser.safari
                    else if($.browser.safari==true)
                    {
                        //check for chrome
                        if(navigator.userAgent.toLowerCase().indexOf('chrome') > -1)
                        {   
                            // for chrome 7.0 and newer version $.browser.version (webkit) is 534.7 or greater
                            indexOfDecimal=$.browser.version.lastIndexOf(".");
                            if($.browser.version.substring(0,indexOfDecimal) > 534)
                            {                    
                                isValid = true;
                            }
                            else if($.browser.version.substring(0,indexOfDecimal) == 534 && $.browser.version.substring(indexOfDecimal+1,$.browser.version.length) >=7)
                            {                     
                                isValid = true;
                            }
                        }
                        else
                        {
                            // safari 5.* uses webkit version 533 Or Above*
                            if($.browser.version.split(".",1)[0] >=533)
                            {                            
                                isValid = true;
                            }
                        }
                    }
                    return isValid;
	};
	
    //Author:PR
    //Modified On: 08/01/2011
    //Purpose:change the height/width of alertbox background 
	// build the AlertBox UI
	fn._buildUI = function() {
		var lb = this;		
		var $b = $('body');
		var $alertbox = $('<div class="alertbox_message" style="top:10px"><div><a class="close" href="">close</a>'
                    +'<div class="messageBox">'
                    +'<p id="MessageHeader" class="title"></p>'
                    +'<div class="messageText"><div id="MessageText"></div>'
                    +'<div  class="messageClose"><a id="submitButton" href="#">Ok</a></div>'
                    +'</div></div>');
		$b.append($alertbox);

        //make the alert box height/width same as bodyContainer size
		$('.alertbox_message').height($('#s4-bodyContainer').height()).width($('#s4-bodyContainer').width());

		lb._$container = $alertbox.find('>div').css({
			'width': lb._width+'px',
			'height': lb._height+'px'
		});
		lb._$alertbox = $alertbox;
		lb._$closeButton = $alertbox.find('a.close');
        lb._$closeOkButton = $alertbox.find('#submitButton');
	};
	
	// register event handlers of AlertBox inner UI controls
	fn._registerEventListeners = function() {
		var lb = this;
		
		lb._$alertbox.click(function(evt) {
			var target = evt.target;
			if($(target).hasClass('alertbox_message')) {
				lb.dismiss();
			}
		});	
		
		lb._$closeButton.click(function(evt) {
			evt.preventDefault();
			lb.dismiss();
		});

		lb._$closeOkButton.click(function(evt) {
			evt.preventDefault();
			lb.dismiss();
		});

		lb._$win.resize(function() {
			lb._centerContent();
		}).scroll(function() {
			lb._centerContent();
		});		
	}
	
	// center the alertbox on the screen
	fn._centerContent = function() {
		var $container = this._$container;
		var $win = this._$win;
		var top = ($win.height() - $container.outerHeight()) / 2 + $win.scrollTop()-50;
		var left = ($win.width() - $container.outerWidth()) / 2 + $win.scrollLeft();
		$container.css({
			'top': top + 'px',
			'left': left + 'px'
		});
	};
	
	fn._showContent = function() {
		this._$container.show();
	};
	
}).call(window.BrowserAlert);
