var usMap;

$(document).ready(function() {
	initializeData();
	displayFacilities();
	displayFilters();
	createMap();
	$('#secondary-navigation a[data-toggle="tab"]').on('http://www.andersonsinc.com/assets/js/shown.bs.tab', function (e) {
		facilityRows();
	});
	$('#clear-filters').click(function(e){
		e.preventDefault();
	    $('.facility').show();
	    facilityRows();
	});
	$('#state-filters a').click(function(e){
		e.preventDefault();
	    code = $(this).attr('data-state-filter');
	    stateFilter(code);
	});
	$('#group-filters a').click(function(e){
		e.preventDefault();
	    code = $(this).attr('data-group-filter');
	    groupFilter(code);
	});

});

function crossDomainAjax (url, successCallback) {

    // IE8 & 9 only Cross domain JSON GET request
    if ('XDomainRequest' in window && window.XDomainRequest !== null) {
        var xdr = new XDomainRequest(); // Use Microsoft XDR
        xdr.open('get', url);
        xdr.onload = function () {
            var dom  = new ActiveXObject('Microsoft.XMLDOM'),
                JSON = $.parseJSON(xdr.responseText);

            dom.async = false;

            if (JSON == null || typeof (JSON) == 'undefined') {
                JSON = $.parseJSON(data.firstChild.textContent);
            }

            successCallback(JSON); // internal function
        };

        xdr.onerror = function() {
            _result = false;  
        };
        xdr.send();
    } 

    // IE7 and lower can't do cross domain
    else if (navigator.userAgent.indexOf('MSIE') != -1 &&
             parseInt(navigator.userAgent.match(/MSIE ([\d.]+)/)[1], 10) < 8) {
       return false;
    }    

    // Do normal jQuery AJAX for everything else          
    else {
        $.ajax({
            url: url,
            cache: false,
            dataType: 'json',
            type: 'GET',
            async: false, // must be set to false
            success: function (data, success) {
                successCallback(data);
            }
        });
    }
}

function initializeData(){
	crossDomainAjax('http://locations.andersonsinc.com/datafeed/tree', function(data){
		facilities = data; 
	});
}

function displayFacilities(){
	locationList = getLocations(facilities);
	facilitySort();
}

function displayFilters(){
	stateList = getAllStates(facilities);
	$("#state-filters").html(stateList);
	
	serviceList = getAllServices(facilities);
	$("#service-filters").html(serviceList);
	
	groupList = getAllGroups(facilities);
	$("#group-filters").html(groupList);
}



function facilitySort(){
	$("#maumee-ohio").prependTo("#facilities");
}

function facilityRows(){
	$('.facility:visible').each(function(index){
		if ((index+1) % 2 == 0){
			$(this).removeClass('odd');
			$(this).addClass('even');
		}
		else{
			$(this).removeClass('even');
			$(this).addClass('odd');
		}
	});
}

function getAllStates(facilities){
	var output = '';
	for (var i in facilities.states){
		state = facilities.states[i];
		output+='<li><a href="#" data-state-filter="' + state.code + '">' + state.name + '</a></li>';
	}
	return output;
}

function getAllServices(facilities){
	var output = '';
	for (var i in facilities.services){
		service = facilities.services[i];
		output+='<li><a href="#" data-service-filter="' + service.id + '">' + service.name + '</a></li>';
	}
	return output;
}

function getAllGroups(facilities){
	var output = '';
	for (var i in facilities.groups){
		group = facilities.groups[i];
		output+='<li><a href="#" data-group-filter="' + group.id + '">' + group.name + '</a></li>';
	}
	return output;
}

function getLocations(facilities){
	for (var i in facilities.cities){
		city = facilities.cities[i];
		
		var latitude = city.groups[0].locations[0].lat;
		var longitude = city.groups[0].locations[0].long;
		
		var cityID = city.name;
		cityID = cityID.replace(/ /g,'');
		cityID = cityID.replace(",", "-");
		cityID = cityID.toLowerCase();
		
		var facilityCount = 0;
		var groupList = '';
		for (var j in city.groups){
			group = city.groups[j];
			groupList += group.groupId + ' ';
			facilityCount += group.locations.length;	
		}

		groupList.slice(0,-1);
		
		$('<div/>', {
			id: cityID,
		    class: 'facility panel panel-defaut'
		}).attr('data-state',city.stateCode).attr('data-groups',groupList).appendTo('#facilities');
		
		$('<div/>', {
		    class: 'row facility-summary collapsed',
		    href: '#facility-' + i,
		}).attr('data-toggle','collapse').attr('data-parent','#facilities').appendTo('#' + cityID);
		
		$('<div/>', {
		    class: 'col-md-3 facility-toggle hidden-sm hidden-xs',
		}).appendTo('#' + cityID + ' .facility-summary');
		
		$('<div/>', {
		    class: 'ss-icon ss-gizmo',
		    html: '&#x002B;'
		}).appendTo('#' + cityID + ' .facility-toggle');

		$('<div/>', {
		    class: 'col-md-6 facility-overview col-sm-12',
		}).appendTo('#' + cityID + ' .facility-summary');		

		$('<h2/>', {
		    html: city.name,
		}).appendTo('#' + cityID + ' .facility-overview');
		
		$('<ul/>', {}).appendTo('#' + cityID + ' .facility-overview');
		
		for (var j in city.groups){
			group = city.groups[j];
			$('<li/>', {html: groupName(group.groupId)}).appendTo('#' + cityID + ' .facility-overview ul');
		}

		$('<div/>', {
		    class: 'col-md-3 facility-count hidden-sm hidden-xs',
		}).appendTo('#' + cityID + ' .facility-summary');
		
		$('<div/>', {
		    class: 'badge',
		    html: facilityCount
		}).appendTo('#' + cityID + ' .facility-count');	
		
		$('<div/>', {
		    class: 'label',
		    html: (facilityCount === 1 ? 'Facility' : 'Facilities')
		}).appendTo('#' + cityID + ' .facility-count');	

		$('<div/>', {
			id: 'facility-' + i,
		    class: 'row facility-details panel-collapse collapse'
		}).attr('role','tabpanel').appendTo('#' + cityID);	
		
		$('<div/>', {
		    class: 'col-sm-4 col-md-3 facility-group-nav'
		}).appendTo('#' + cityID + ' .facility-details');	
		
		$('<h2/>', {
		    html: city.name,
		}).appendTo('#' + cityID + ' .facility-group-nav');
		
		$('<ul/>', {}).attr('role','tablist').appendTo('#' + cityID + ' .facility-group-nav');
		
		for (var j in city.groups){
			group = city.groups[j];
			var listItem = $('<li/>', {}).appendTo('#' + cityID + ' .facility-group-nav ul');
			$('<a/>', {
				href: '#group-' + i + j,
				html: groupName(group.groupId) + ' <div class="badge">' + group.locations.length + '</div>'
			}).attr('role','tab').attr('data-toggle','tab').appendTo(listItem);	
		}
		
		$('#' + cityID + ' .facility-group-nav ul li').first().addClass('active');

		$('<div/>', {
		    class: 'col-sm-8 col-md-6 tab-content facility-content'
		}).appendTo('#' + cityID + ' .facility-details');

		for (var j in city.groups){
			group = city.groups[j];
			$('<div/>', {
				id: 'group-' + i + j,
		    	class: 'tab-pane fade'
			}).appendTo('#' + cityID + ' .facility-content');	
			
			for (var k in group.locations){
				facility = group.locations[k];
				
				locationDiv = $('<div/>', {
					class: 'location'
				}).appendTo('#group-' + i + j);
				
				$('<h3/>', {
					html: facility.name,
				}).appendTo(locationDiv);
				
				var output = '';
				if (facility.address1){
					output+='<b>Address:</b> ' + facility.address1;
				}
				if (facility.address2){
					output+=' ' + facility.address2;
				}
				if (facility.address3){
					output+=' ' + facility.address3;
				}
				if (facility.cityState){
					output+=', ' + facility.cityState;
				}
				if (facility.postalCode){
					output+=' ' + facility.postalCode;
				}
				
				if (facility.mailingAddress1){
					output+='<br/>';
					output+='<b>Mailing Address:</b> ' + facility.mailingAddress1;				
					if (facility.mailingAddress2){
						output+=' ' + facility.mailingAddress2;
					}
					if (facility.mailingAddress3){
						output+=' ' + facility.mailingAddress3;
					}
					if (facility.mailingCityState){
						output+=', ' + facility.mailingCityState;
					}
					if (facility.mailingPostalCode){
						output+=' ' + facility.mailingPostalCode;
					}
				}
				
				output+='<br/>';
				if (facility.phone1Value){
					output+='<b>' + facility.phone1Label + ':</b> ' + facility.phone1Value;
				}
				if (facility.phone2Value){
					output+='<br/><b>' + facility.phone2Label + ':</b> ' + facility.phone2Value;
				}
				if (facility.phone3Value){
					output+='<br/><b>' + facility.phone3Label + ':</b> ' + facility.phone3Value;
				}
				if (facility.phone4Value){
					output+='<br/><b>' + facility.phone4Label + ':</b> ' + facility.phone4Value;
				}
				if (facility.phone5Value){
					output+='<br/><b>' + facility.phone5Label + ':</b> ' + facility.phone5Value;
				}
				
				$('<div/>', {
		    		html: output
				}).appendTo(locationDiv);
			}
			footnote = groupFootnote(group.groupId);
			if (footnote){
				$('<p/>', {
		    		html: footnote
				}).appendTo(locationDiv);
			}
			
			if (groupName(group.groupId) == 'Grain'){
				url="http://www.andersonsgrain.com/";
			}
			else if (groupName(group.groupId) == 'Turf & Specialty'){
				url="http://www.andersonspro.com/";
			}
			else if (groupName(group.groupId) == 'Plant Nutrient'){
				url="http://www.andersonsnutrients.com/";
			}
			else if (groupName(group.groupId) == 'Retail'){
				url="http://www.andersonsstore.com/";
			}
			else if (groupName(group.groupId) == 'Rail'){
				url="http://www.andersonsrail.com/";
			}
			else if (groupName(group.groupId) == 'Ethanol'){
				url="http://www.andersonsethanol.com/";
			}
			else{
				url="../../index.htm"/*tpa=http://www.andersonsinc.com/*/;
			}
				
				
			$('<div/>', {
		    	html: '<br/><a class="outline-button" href="' + url + '" target="_blank">Visit ' + groupName(group.groupId) + ' Website</a>'
			}).appendTo(locationDiv);
		}
		
		$('#' + cityID + ' .facility-content .tab-pane').first().addClass('active').addClass('in');
		
		$('<div/>', {
		    class: 'col-md-3 hidden-sm location-map'
		}).appendTo('#' + cityID + ' .facility-details');
		
		$('<img>', {
		    class: 'img-responsive full-width',
		    src: 'https://maps.googleapis.com/maps/api/staticmap?center=' + latitude + ',' + longitude + '&zoom=6&markers=icon:http://goo.gl/YLh3P3%7C' + latitude + ',' + longitude + '&size=720x720&zoom=6&style=feature:all%7Celement:all%7Csaturation:-100&style=feature:water%7Celement:all%7Clightness:-20&style=feature:poi%7Celement:all%7Cvisibility:off&key=AIzaSyBnuhMh45Nsy6qSjA6zZHA3iDLbRfZDnTQ'
		}).appendTo('#' + cityID + ' .location-map');
	}
	return true;
}
							
function createMap() {
	usMap = new jvm.WorldMap({
		map: 'us_mill_en',
		container: $('#vmap'),
		backgroundColor: 'transparent',
		zoomOnScroll: false,
		panOnDrag:false,
		setFocus:{
			scale:4
		},
		regionStyle: {
			initial: {
		    	fill: '#f5e1b3',
				"fill-opacity": 1,
				stroke: 'white',
				"stroke-width": 3,
				"stroke-opacity": 0
		  	},
		  	hover: {
			  	"fill-opacity": 1
		  	},
		  	selected: {
			  	
		  	},
		  	selectedHover: {
			  	
		  	}
		},
		onRegionLabelShow: function(e, label, code){
			if (stateIsActive(code)){
				label.html(getIcons(code));
				label.css('opacity',1);
			}
			else{
				label.css('opacity',0);
			}          
    	},
		onRegionClick: function(e, code){
			if (stateIsActive(code)){
				code = shortCode(code);
				stateFilter(code);
				$('a[href="#list"]').tab('show');
			}
		},
		onRegionOut: function(e, code){
			if (stateIsActive(code)){
				getStateObject(code).attr('fill','#fdb813');		
			}
		}
	});
	activateStates();
}

function stateFilter(code){
	$('.facility').hide();
	$('.facility[data-state="' + code +'"]').show();
	facilityRows();
}

function groupFilter(code){
	$('.facility').hide();
	$('.facility[data-groups~="' + code +'"]').show();
	facilityRows();
}

function getStateObject(code){
	return $('#vmap .jvectormap-region[data-code="' + code +'"]');
}
	
function shortCode(code){
	var split = code.split('-');
	return split[1];
}

function activateStates(){
	for (var i in facilities.states){
		$('#vmap .jvectormap-region[data-code="US-' + facilities.states[i].code  +'"]').attr('data-active','true');
	}
}

function stateIsActive(code){
	code = shortCode(code);
	for (var i in facilities.states){
		if (facilities.states[i].code == code){
			return true;
		}
	}
	return false;
}

function getIcons(code){
	var output='';
	code = shortCode(code)
	groupIds = getGroups(code);
	for (var i in groupIds){
		name = groupName(groupIds[i]);
		if (name == "Grain"){
			output+="<div class='map-icon map-icon-grain'></div>";
		}
		else if (name == "Ethanol"){
			output+="<div class='map-icon map-icon-ethanol'></div>";
		}
		else if (name == "Plant Nutrient"){
			output+="<div class='map-icon map-icon-plant'></div>";
		}
		else if (name == "Rail"){
			output+="<div class='map-icon map-icon-rail'></div>";
		}
		else if (name == "Turf & Specialty"){
			output+="<div class='map-icon map-icon-turf'></div>";
		}
		else if (name == "Retail"){
			output+="<div class='map-icon map-icon-retail'></div>";
		}
	}
	return output;
}

function getGroups(code){
	for (var i in facilities.states){
		if (facilities.states[i].code == code){
			return facilities.states[i].groupIds;
		}
	}
}

function groupName(groupId){
	for (var i in facilities.groups){
		if (facilities.groups[i].id == groupId){
			return facilities.groups[i].name;
		}
	}
	return false;
}	

function groupFootnote(groupId){
	for (var i in facilities.groups){
		if (facilities.groups[i].id == groupId){
			return facilities.groups[i].footnote;
		}
	}
	return false;
}		