/*
 * Autocomplete engine selector and script loader
 *
 * The contents of this file must be the same in both:
 *      search.js
 *      jquery.autocomplete.staples.js
 *
 */

(function(window, document, $, undefined) {
	'use strict';
	var STAPLES = ( function(STAPLES) {
		STAPLES.Autocomplete = ( function() {
			// private functions
			function loadAutocompleteJs(autocompleteJs) {
				var scriptPath = getScriptPath() + autocompleteJs;
				var s = document.createElement('script');
			    s.type = 'text/javascript';
			    s.async = true;
			    s.src = scriptPath;
			    var x = document.getElementsByTagName('script')[0];
			    x.parentNode.insertBefore(s, x);	
			}

			function getACSettings() {
				var settings = {};
				if(typeof(Storage) !== "undefined") {
					settings.autocompleteJs = window.sessionStorage.searchACJs || "";
					settings.autocompleteUrl = window.sessionStorage.searchACUrl || "";
					settings.UX = window.sessionStorage.UX || "";
				} else {
				    console.log("fall back mechanism needed");
				}
				
				return settings;
			}
			
			function getScriptPath() {
				var scriptPath = "/search/resources/js/"; // set default that should work as backup
				var scriptName = "jquery.autocomplete.staples.js"/*tpa=http://assets.staples-static.com/search/resources/js/jquery.autocomplete.staples.js*/;
				var mySrc = $('script[src$="/' + scriptName + '"]').attr('src');
				if (mySrc != null) {
					scriptPath = mySrc.substring(0, mySrc.lastIndexOf("/") + 1);
				}
				return scriptPath;
			}
			
			return {
				init: function() { 
					if (console) { console.log("Inititializing STAPLES.Autocomplete."); }
					STAPLES.Onload.addLoadEvent(function(){
						var searchTracking = STAPLES.Autocomplete.getSearchTracking();
						var acJs = STAPLES.Autocomplete.getACJs();
						var UXCookie = STAPLES.Cookies.getCookie('asgardTheme') || "";
						var UX = STAPLES.Autocomplete.getUX() || "";
						if (typeof searchTracking === "undefined" || searchTracking == ""
						    || typeof acJs === "undefined" || acJs == "" || UX != UXCookie) {
							STAPLES.Autocomplete.setSearchEngine();
						} else {
							loadAutocompleteJs(acJs);
						}
					});
				},
				setSearchEngine: function() {
	                    var storeId = document.getElementById("seoStoreId").value;
						$.ajax({
							url: "/search/resources/switches/engine?storeId="+storeId,
							dataType: "json",
							success: function(jsonResponse) {
								var searchTracking = jsonResponse.name;
								var searchACJs = jsonResponse.autocompleteJs;
								var searchACUrl = jsonResponse.autocompleteUrl;
								var UXCookie = STAPLES.Cookies.getCookie('asgardTheme');
								STAPLES.Cookies.setCookie("searchTracking", searchTracking);
								if(typeof(Storage) !== "undefined") {
									window.sessionStorage.searchACJs = searchACJs;
									window.sessionStorage.searchACUrl = searchACUrl;
									window.sessionStorage.UX = UXCookie;
								} else {
								    console.log("fall back mechanism needed may be add old text file AC");
								}
								loadAutocompleteJs(searchACJs);
							},
							error: function(err) {
								if (console) {
									console.warn("Error fetching search engine - " + err);
								}
							}		
						});
				},
				
				getSearchTracking: function() {
					var reMatch = /searchTracking= *([^;]*)/.exec(document.cookie);
					var trackingCookie = "";
					if (typeof reMatch !== 'undefined' && reMatch != null && reMatch.length > 1) {
						trackingCookie = reMatch[1];
					}
					return trackingCookie;
				},
				
				getACUrl: function() {
					var acSettings = getACSettings();
					return acSettings.autocompleteUrl;
				},
				
				getACJs: function() {
					var acSettings = getACSettings();
					return acSettings.autocompleteJs;
				},
				getUX: function() {
					var acSettings = getACSettings();
					return acSettings.UX;
				}
			};
		})();
		return STAPLES;
    }(window.STAPLES || {}));
    window.STAPLES = STAPLES;
}(window, document, jQuery));


function submitSearch(formId , searchKey){

    var storeId = $('#seoStoreId').val();
    var langId = $('#seoLangId').val();
    var catalogId = $('#seoCatId').val();

    //trim the spaces in search term
    searchKey = $.trim(searchKey).replace(/\s+/g, ' ');
    //for decoding the html elements to french accents
    if(langId==2){
        searchKey = STAPLES.Utilities.htmlDecode(searchKey);
    }
    //if searchKey is only white spaces or no characters entered , no need to submit the form to initiate search.
    if(searchKey === ""){
        return false;
    }
    //to format the search key
    searchKey = formatSearchTerm(searchKey);
    // setting 'searchKey' to variable 'searchTerm', since the second part of the URL
    // need to encode before submit
    var searchTerm = searchKey;
    
    //remove all % in the search term to prevent issue while decoding
    //searchTerm = searchTerm.replace(/\%/g,"");
    //encoding search term and replacing /,*,),(,?,+,_ with respective double encoded values
    searchTerm = encodeURIComponent(searchTerm).replace(/[!']/g, escape).split("%25").join("%2525").replace(/\/|%2F/ig,'%252F').replace(/\*/g,'%252A')
        .replace(/\)/g,'%2529').replace(/\(/g,'%2528').replace(/\?|%3F/ig,'%253F').replace(/\+|%2B/ig,'%252B').replace(/\_/g,'%255F').replace(/\%23/g,'%2523');
    //replace all spaces with '+'
    searchTerm = searchTerm.replace(/%20/g, "+");
    searchTerm = searchTerm.replace(/%252B/g, "+");
    // the below code is used to trim down all the unwanted parameters from the URL. For .com,
    //storeId, catelogId,langId are not required as parameters(after directory_<<searchTerm>>?) while submit
    $('form#'+ formId +' input').each(function(){
        var nameAttr = $(this).attr("name");
        $(this).filter(function() {
            return  nameAttr == 'searchkey' || nameAttr == 'storeId'|| nameAttr == 'langId' || nameAttr == 'catalogId';
        }).prop("disabled", true);
    });
    //try disabling the submit button to fix the duplicateKeyException for IE
    //var submitBtn = document.getElementById("hsearch");
    //submitBtn.disabled = true;

    if(storeId == 10001){
        //submitting the form with the required SEO URL
        document.forms[formId].action = '/'+searchKey+'/directory_'+searchTerm;
        document.forms[formId].submit();
    } else if (storeId == 20001){
        //submitting the form with the required SEO URL
        document.forms[formId].action = '/' + searchKey + '/directory_' + searchTerm + '_' + catalogId + '_' + langId + '_' + storeId ;
        document.forms[formId].submit();
    }
};

//Method used for formating the search key. All special characters and space
//will be converted to '+'
function formatSearchTerm(searchKey){
    //replace all special characters with '+'
    var langId = document.getElementById('seoLangId').value;
    //This is to prevent french accented characters from getting removed
    if(langId==2){
    	searchKey =  searchKey.replace(/[^a-zA-Z0-9À-ÿ]+/g,'+');
        searchKey = encodeURIComponent(searchKey);
        searchKey =  searchKey.replace(/%2B/g, "+");
    } else {
        searchKey =  searchKey.replace(/[^a-zA-Z0-9]+/g,'+');
    }


    //To remove + , if it is the last character
    if(searchKey.charAt(searchKey.length - 1) == "+"){
        searchKey = searchKey.slice(0,-1);
    }
    //To remove +, if it is the first character
    if(searchKey.charAt(0) == "+"){
        searchKey = searchKey.slice(1);
    }
    //To return +, if search term contains only special character
    if(searchKey === ""){
        searchKey = "+" ;
    }
    return searchKey;
};

function setAutoSearch(item, form) {
    if(item && item[0] && $.trim(item[0])){
        var formId = simulateForm(item[0], {'autocompletesearchkey': item[0]});
        submitSearch(formId, item[0]);
    }
};

function simulateForm(searchTerm, hiddenFields) {
    var formId = "ux2Form";

    var f = document.createElement("form");
    f.setAttribute('id',formId);
    if(hiddenFields) {
        for (var key in hiddenFields) {
            if (hiddenFields.hasOwnProperty(key)) {
                f.appendChild(createHiddenInput(key, hiddenFields[key]));
            }
        }
    }
    document.body.appendChild(f);

    return formId;
};



function createHiddenInput(name, value) {
    var input = document.createElement("input");
    input.setAttribute('type',"hidden");
    input.setAttribute('name',name);
    input.setAttribute('value', value);
    return input;
};

$(document).ready(function () {
	var $searchInput = $("input[name='TrackedSearchInput']");
	var $searchBtn = $("#ux2SearchBtn");

	$searchInput.keyup(function(event) {
		if (event.keyCode == 13) {
			var highlightedLi = $("#ux2DropDown").find("li").filter(".stp--bg-light-blue");
			if(!highlightedLi || highlightedLi.length == 0) {
				$searchBtn.click();
			}
		}
	});


	$searchBtn.click(function(event){
		STAPLES.Main.onClickHijack(event);
		var term = $searchInput.val();
		if(term && $.trim(term)) {
			var formId = simulateForm(term);
			submitSearch(formId, term);
		}
	});

	var $cartridgeBtn = $('.stp--btn-cartridge');
	var $cartridgeInput = $(".cartridge-number");

	var inkTonerCategoryId = $('#ux2-inktoner-categoryid').val();
	if(!inkTonerCategoryId) {
		inkTonerCategoryId = '12328';
	}

	$cartridgeBtn.click(function(event){
		var term = $cartridgeInput.val();
		if(term && $.trim(term)) {
			var formId = simulateForm(term, {storeId: '10001', catalogId: '10051', langId: '-1', categoryId: inkTonerCategoryId});
			submitSearch(formId, term);
		}
	});

	$cartridgeInput.keyup(function(event) {
		if (event.keyCode == 13) {
			$cartridgeBtn.click();
		}
	});

	var $modelBtn = $('.stp--btn-model');
	var $modelInput = $(".model-number");

	$modelBtn.click( function(event){
		var term = $modelInput.val();
		if(term && $.trim(term)) {
			var formId = simulateForm(term, {modelsearchkey: term, storeId: '10001', catalogId: '10051', langId: '-1', categoryId: inkTonerCategoryId, errorUrl: 'pmm1', identifier: 'SC43', inkAndTonerStyle: null});
			document.forms[formId].action = '/office/supplies/StaplesSearchPrinterByModelNumber?';
			document.forms[formId].submit();
		}
	});

	$modelInput.keyup(function(event) {
		if (event.keyCode == 13) {
			$modelBtn.click();
		}
	});

//	persist the search term to the search page
	var searchTerm = '';
	if($("input[name='searchTerm']").length > 0) {
		searchTerm = $("input[name='searchTerm']").val();
	}else{
		var urlPath = decodeURIComponent(window.location.href);
		if(urlPath &&  urlPath.indexOf('directory_') > -1) {
			var subPath = urlPath.split("directory_")[1];
			if(subPath.indexOf('?') > -1){
				searchTerm = subPath.split('?')[0];
			}else if(subPath.indexOf('#') > -1){
				searchTerm = subPath.split('#')[0];
			}else{
				searchTerm = subPath;
			}
		}
	}
	if(searchTerm) {
		$searchInput.val(searchTerm);
	}
	var STAPLES = window.STAPLES || {};
	STAPLES.Header = STAPLES.Header || {};
	STAPLES.Autocomplete.init();
	STAPLES.Header.submitSearch = function(formId , searchKey) {
		submitSearch(formId , searchKey);
	};
	
	STAPLES.SearchAC = STAPLES.SearchAC || {};
    STAPLES.SearchAC.submitSearch = submitSearch;
});