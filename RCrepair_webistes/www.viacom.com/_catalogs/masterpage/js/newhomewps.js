
var todayDate=new Date();
(function(){
    var D= new Date('2011-06-02T09:34:29+02:00');
    if(!D || +D!== 1307000069000){
        Date.fromISO= function(s){
            var day, tz,
            rx=/^(\d{4}\-\d\d\-\d\d([tT ][\d:\.]*)?)([zZ]|([+\-])(\d\d):(\d\d))?$/,
            p= rx.exec(s) || [];
            if(p[1]){
                day= p[1].split(/\D/);
                for(var i= 0, L= day.length; i<L; i++){
                    day[i]= parseInt(day[i], 10) || 0;
                };
                day[1]-= 1;
                day= new Date(Date.UTC.apply(Date, day));
                if(!day.getDate()) return NaN;
                if(p[5]){
                    tz= (parseInt(p[5], 10)*60);
                    if(p[6]) tz+= parseInt(p[6], 10);
                    if(p[4]== '+') tz*= -1;
                    if(tz) day.setUTCMinutes(day.getUTCMinutes()+ tz);
                }
                return day;
            }
            return NaN;
        }
    }
    else{
        Date.fromISO= function(s){
			max.setHours(23,59,59);        
            return new Date(s);
        }
    }
})();
function LoadRSS() {
    jQuery('#blogRSS').html('');
    jQuery.ajax({
        url: "http://ajax.googleapis.com/ajax/services/feed/load",
        type: "GET",
        dataType: "jsonp",
        data: {
            v: "1.0",
            num: "3",
            q: "http://blog.viacom.com/feed/",
            output: 'xml'
        },
        success: function (data) {
            xmlDoc = jQuery.parseXML(data.responseData.xmlString),
            $xml = $(xmlDoc),
            $xml.find("item").each(function () {
                $title = $(this).find('title');
                $postimg = $(this).find('postimage');
                $link = $(this).find('link');

                var imgURL = $postimg.text();
                if (imgURL.length == 0) {
                    imgURL = "../../../PublishingImages/viacom_blog.gif"/*tpa=http://www.viacom.com/PublishingImages/viacom_blog.gif*/;
                    $(this).find('category').each(function () {
                        if ($(this).text() == 'Research')
                            imgURL = "../../../PublishingImages/SIR.gif"/*tpa=http://www.viacom.com/PublishingImages/SIR.gif*/;
                    });
                }

                $('#blogRSS').append('<div class="color BlogBottomHeadline "><img class="blogwidth" src="' + imgURL + '"/><div class="blogstyles"><a  target="_blank" href="' + $link.text() + '">' + $title.text() + '</a></div></div><div style="height:10px;"></div><img class="hrmargin" src="../../../PublishingImages/homeline.png"/*tpa=http://www.viacom.com/PublishingImages/homeline.png*//><div style="height:12px;"</div>');

            });
        },
        error: function () {
          //  alert('error-Blog');
        }

    });
}

function LoadFinance() {
    $('#finance').html('');
    $.ajax({
        url: "/_api/web/lists/getbytitle('3.FINANCE')/items?$select=FinanceDesc,Title,PublishDate&$orderby=PublishDate desc,OrderNo",
        method: "GET",
        headers: {
            "Accept": "application/json; odata=verbose"
        },
        success: function (data) {
            //alert("Success : "+data); // Returns JSON collection of the results
            if(typeof data==="string")
	            data=jQuery.parseJSON(data);
	            var count=0;
            for (i = 0; i < data.d.results.length; i++) {
                //  alert(data.d.results[i].Title + " : "+data.d.results[i].URL0.Url);
                if(count>2)
                	break;
				if(data.d.results[i].PublishDate!=null){
				 var pubDate=Date.fromISO(data.d.results[i].PublishDate);
				 //new Date(data.d.results[i].PublishDate);
	                if(pubDate<=todayDate)
	                {

	                $('#finance').append('<div class="FinancialBottomHeadline"><div class="color FBottomHeadline newsfont">' + data.d.results[i].FinanceDesc + '</div><img class="hrmargin" src="../../../PublishingImages/homeline.png"/*tpa=http://www.viacom.com/PublishingImages/homeline.png*//></div>');
	                count++;
	                }
                }
            }
        },
        error: function (data) {
            console.log("Error-Finance : " + data);

        }
    });
}

function LoadNews() {
    $('#news').html('');

    
        
    $.ajax({
        url: "/_api/web/lists/getbytitle('http://www.viacom.com/_catalogs/masterpage/js/2.NEWS')/items?$select=Title,URL0,Publish_x0020_Date,NewWindow&$orderby=Publish_x0020_Date desc,Order",
        method: "GET",
        headers: {
            "Accept": "application/json; odata=verbose"
        },
        success: function (data) {
            //alert("Success : "+data); // Returns JSON collection of the results
            if(typeof data==="string")
            data=jQuery.parseJSON(data);
			var count=0;
			var newsHTML='';
            for (i = 0; i < data.d.results.length; i++) {
                //  alert(data.d.results[i].Title + " : "+data.d.results[i].URL0.Url);
                if(count>2)
                	break;
                if(data.d.results[i].Publish_x0020_Date!=null){
	                var pubDate=Date.fromISO(data.d.results[i].Publish_x0020_Date);

	                //new Date(data.d.results[i].Publish_x0020_Date);
	                if(pubDate<=todayDate)
	                {
						var target="_self";
						if(data.d.results[i].NewWindow)
							target="_blank";
						newsHTML+='<div class="color HomeBottomHeadline"><div class="NewsBottomHeadline"><a class="newsfont" target="'+target+'" href="' + data.d.results[i].URL0.Url + '">' + data.d.results[i].Title.substring(0, 190) + '</a></div></div><div style="height:13px;"></div><img class="hrmargin" src="../../../PublishingImages/homeline.png"/*tpa=http://www.viacom.com/PublishingImages/homeline.png*//><div style="height:9px;"></div></div>'
		                
		                count++;
	                }
                }
            }
            $('#news').html(newsHTML);
        },
        error: function (data) {
            console.log("Error-News: " + data);

        }
    });
}


function getSliderContent()
{

  clientContext = SP.ClientContext.get_current(); 
  this.oList = clientContext.get_web().get_lists().getByTitle('1.HERO ASSETS LIBRARY');
  var query = "<View><Query><Where><Leq><FieldRef Name=\'Publish_x0020_Date\'/><Value Type=\'DateTime\'><Today/></Value></Leq></Where><OrderBy><FieldRef Name=\'OrderNo\' Ascending=\'TRUE\'/><FieldRef Name=\'Created_x0020_Date\' Ascending=\'FALSE\'/></OrderBy><GroupBy></GroupBy></Query></View>";
  var camlQuery = new SP.CamlQuery();
  camlQuery.set_viewXml(query);
  this.collListItem = oList.getItems(camlQuery);
  clientContext.load(collListItem );  
  clientContext.executeQueryAsync(Function.createDelegate(this, this.onQuerySucceeded), Function.createDelegate(this, this.onQueryFailed));
  
}

function onQuerySucceeded() {
 var listItemInfo = '';
 var listItemEnumerator = collListItem.getEnumerator();
 var liformat="";
   
    while (listItemEnumerator.moveNext()) {
  
        var oListItem = listItemEnumerator.get_current();
        var alignCss = 'leftDown';
        if(oListItem.get_item('RightSide') == true)
		{
   			alignCss = 'rightDown';
	    }

		var textColorCss = '';
		if(oListItem.get_item('BlackText')==true){
			textColorCss = 'HeroDarkText';
		}
		
		
		liformat += "<li>"+"<div class='cont1' style='background-image:url(/heroAssets/"+escape(oListItem.get_item('FileLeafRef'))+ ")'>"+
        				"<div class='container "+alignCss +"'><div class='easyBox flowContent'>";
		
		
	    if(oListItem.get_item('wic_System_Copyright') != null)	
        {		
	        liformat +=	"<p class='HeroBrandName "+textColorCss +"'>"+oListItem.get_item('wic_System_Copyright')+"</p>";
        }
    				
        if(oListItem.get_item('Title') != null)
		{
   			liformat +=	"<p class='HeroArticleTitle "+textColorCss +"'>"+oListItem.get_item('Title')+"</p>";
        }
    
        if(oListItem.get_item('Keywords') != null)
		{
			liformat +=	"<p class='HeroBriefSummary "+textColorCss +"'>"+oListItem.get_item('Keywords')+"</p>"+"<br/>";
		}
		
		
		var altThumb=oListItem.get_item('AlternateThumbnailUrl').get_url();
		if(altThumb.lastIndexOf('/')===altThumb.length-1)
			altThumb=altThumb.substring(0,altThumb.length-1);
		if(oListItem.get_item('Video') == true)
						{
						liformat +=	"<a class='HeroMoreLink' href='javascript:PlayVideo(\""+altThumb+"\"\,\""+oListItem.get_item('FileLeafRef')+"\");' alt='"+"'>"+oListItem.get_item('AlternateThumbnailUrl').get_description()+ " &gt;</a>"+
        							"</div></div></div>"+"</li>";
        				}
        				else if (oListItem.get_item('Video') == false && oListItem.get_item('NewWindow') == true)
        				{
        				liformat +=	"<a class='HeroMoreLink' target='_blank' href="+altThumb+">"+oListItem.get_item('AlternateThumbnailUrl').get_description()+ " &gt;</a>"+
        							"</div></div></div>"+"</li>";
        				}
        				else
        				{
        				liformat +=	"<a class='HeroMoreLink' href="+altThumb+">"+oListItem.get_item('AlternateThumbnailUrl').get_description()+ " &gt;</a>"+
        							"</div></div></div>"+"</li>";
        				}
    }
    
   $('.slides').append(liformat);
 //function for flexslider
 $('.flexslider').flexslider({
         animation: "slide",
         controlsContainer: ".flex-container",
         start: function(slider) {
           $('.total-slides').text(slider.count);
         },
         after: function(slider) {
           $('.current-slide').text(slider.currentSlide);
         }
    });
    

    
 // alert( $('.slides').html());



   
}
function onQueryFailed(sender, args) {
console.log('Request failed.-Get Slider Content ' + args.get_message() + '\n' + args.get_stackTrace());
}  





$(document).ready(function () {
 SP.SOD.executeOrDelayUntilScriptLoaded(getSliderContent,'Unknown_83_filename'/*tpa=http://www.viacom.com/_catalogs/masterpage/js/sp.js*/);
    LoadRSS();
    LoadNews();
    LoadFinance();
   
    $('#macSharePointPlugin').remove();
});