$(function(){

    $('.col > .content-box, .last > .content-box, #footer-functional-container > div,#utility-links-list > li,#utility-links-list > li.has-dropdown,#utility-links-list > li > a,#utility-links-list > .has-dropdown > a,#utility-links-list > li > a > span,#utility-links-list > .has-dropdown > a > span,#utility-links-list > li > a,#utility-links-list > .has-dropdown > a,#utility-links-list > .has-dropdown > a > span,#utility-links-list > li.hover,#utility-links-list > li.hover > a,#nav-primary-list > li,#nav-primary-list > li:last > a,#nav-primary-list > li > a,#nav-primary-list > li > a,#nav-primary-list > .hover > a,.nav-primary-dropdown > div,.sub-nav-list > li,.nav-list-links > li,.has-dropdown > a, #content-extended > .info-block, #content-extended > .info-block, #content-extended > .info-block h3, #content-extended > .info-block h2, #content-extended > .info-block .info-block-header-title, #content-extended > .info-block .info-block-header-title > span').each(function(){
        $(this).addClass('ie6'+$(this).attr('tagName').toLowerCase());
    });

    $('.hig-quick-form input[type=text]').each(function(){
        $(this).addClass('ie6input-type-text');
    });
    
    $('.link-section-2-last,.link-section-3-last').after('<div class="clear-both">&nbsp;</div>');
    $('.button-type-video').prepend('<span class="button-type-video-before"></span>');
    
    $('.more-dropdown-options > a').wrapInner('<span/>');

    $('#nav-primary-list,#utility-links-list').show(1, function () {
        $('#page-body').css('position','relative');
    });
    
    $('.hig-accordian-wrapper .hig-accordian:first-child').addClass('first');

    $('.content-wrapper-inner p:last-child ').addClass('last-child');

    $('.list-links-2 li .icon-pdf').css('zoom','1');
    $('.list-links-2 li .icon-word').css('zoom','1');
    $('.list-links-2 li .icon-xls').css('zoom','1');

    $('.icon-arrow-right, .link-section-more, .utility-links-dropdown .list-links-2 > li > a').css({'paddingRight':'0','background-image':'none'}).after('<span class="span-arrow-right">&nbsp;</span>');

    $('#search-submit').attr('src','../imgs/search-submit.png'/*tpa=http://s0.hfdstatic.com/sites/build_4/imgs/search-submit.png*/).show();
});

if(jQuery.browser.msie && parseInt(jQuery.browser.version, 10) == 6) {
  try {
    document.execCommand("BackgroundImageCache", false, true);
  } catch(err) {}
}