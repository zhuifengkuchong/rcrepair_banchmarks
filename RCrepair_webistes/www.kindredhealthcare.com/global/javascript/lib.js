//check to see if it's really ie8 or in ie7 compatibility mode
if ($j.browser.msie && parseInt($j.browser.version) == 8) {
    var browserVersion = navigator.appVersion.toString().toLowerCase();
    if (browserVersion.indexOf('msie 7') != -1) {
        $j.browser.version = "7.0";
    }
}

$j(document).ready(function() {
    $j('ignore,shape').addClass('ignore');

	//move menus to higher zindex
	var czIndex = $j('#content').css('z-index');
	$j('#menu').mouseenter(function(event) {
		$j('#content').css('z-index', 1);
	});
	$j('#menu').mouseleave(function(event) {
		$j('#content').css('z-index', czIndex);
	});

    //fix the menu fonts for ie
    fixMainNav();
    
    //make pop's clickable anywhere
    $j('.facility-pop').click(onPopClick);

    //fix enter press on search
    $j('#search').find('input[type=text]').keypress(function(e) {
        if (e.which == 13) {
            $j('#search').find('input[type=image]').click();
            return false;
        }
    });

    //textbox default switch
    var $inputs = $j('input[type=text][title]');
    $inputs.focus(function() {
        if (this.value == this.title) {
            this.value = '';
        }
    });
    $inputs.blur(function() {
        if (this.value.length == 0) {
            this.value = this.title;
        }
    });

    //font-size selector
    $j('#font-selector').click(function(event) {
        event.preventDefault();
        var size = 0;
        var $elm = $j(event.target);
        if ($elm.hasClass('font-selector-sm')) {
            size = 1;
        } else if ($elm.hasClass('font-selector-md')) {
            size = 1.2;
        } else if ($elm.hasClass('font-selector-lg')) {
            size = 1.4;
        }
        if (size > 0) {
            $j('#page').css('fontSize', size + 'em');
            fixMainNav();

            //save the fontSize in a cookie for the site
            var exdate = new Date();
            exdate.setDate(exdate.getDate() + 900);
            document.cookie = 'fontSelectorSize=' + size + ';expires=' + exdate.toGMTString() + ';path=/';

            return false;
        }
    });

    //check for an already selected font size in the cookie and set it
    if (document.cookie.length > 0) {
        var c_name = 'fontSelectorSize';
        var c_start = document.cookie.indexOf(c_name + '=');
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(';', c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
            var size = parseFloat(document.cookie.substring(c_start, c_end));

            $j('#page').css('fontSize', size + 'em');
            fixMainNav();
        }
    }

    //document download images
    $j('a[href$=.pdf]').attr('target', '_blank').filter(':not(.noicon)').after('<img src="../images/shared/icon-pdf.gif"/*tpa=http://www.kindredhealthcare.com/global/images/shared/icon-pdf.gif*/ alt="Download PDF" />');

    //fix the z-index ordering in ie6
    if ($j.browser.msie) {
        var $menuItems = $j('#nav > li');
        var count = $menuItems.length;
        $menuItems.each(function(index, elm) {
            $elm = $j(elm);
            $elm.css('z-index', $elm.css('z-index') + count - index);
        });
    }
	
	//turn special links into lightbox popups
    $j('a.lightbox-image').lightBox();

    //adjust height of iframe to content
    $j('iframe.autoheight').iFrameResize();

    //setup drop downs that link to other pages
    $j('select.link-dropdown').change(function() {
        if (this.value != '') {
            window.location = this.value;
        }
    });
});

function onPopClick(event) {
	if (event.target.tagName != 'a') {
		var $anchor = $j('.pop-header a', this);
		var link = $anchor.attr('href');
		if (link.length > 0 && $j('.pop-btn:visible', this).length == 0) {
			if ($anchor.attr('target') == '_blank') {
				window.open(link);
			} else {
				window.location = link;
			}
		}
	}
}

function inputDefaultSwitch()
{
    //textbox default switch
	var $inputs = $j('input[type=text][title]');
	$inputs.focus(function() {
		if (this.value == this.title) {
			this.value = '';
		}
	});
	$inputs.blur(function() {
		if (this.value.length == 0) {
			this.value = this.title;
		}
	});
}

function fixMainNav() {
	if ($j.browser.msie) {
		$j('#nav > li ul').each(function() {
			$j('li', this).width('auto');
			var menuWidth = $j(this).width();
			$j('li', this).width(menuWidth);
		});
	}
}

var Tog = {
	check : function (a, b) {
		for (var i = 0; i < a.length; i++) {
			if (document.getElementById(a[i]).checked) {
				Tog.toggle(b[i]);
			}
		}
	}, 
	checkRadioSelected : function (radiolist, toggleElement) {
		for(var i=0; i < radiolist.length;i++)
		{
			if( radiolist[i].checked)
			{
			  if(radiolist[i].value == "3")
			   {
				 Tog.toggle(toggleElement);
			   }
			}
		}
	},
	swap : function (a, b) {
		a = document.getElementById(a);
		b = document.getElementById(b);
		
		if (!a || !b) return false;
		
		if (a.className.indexOf("closed") != -1) {
			oldClass = a.className;
			newClass = oldClass.replace(/closed/g, "");
			a.className = newClass;
			
			b.className += " closed";
		} else {
			oldClass = b.className;
			newClass = oldClass.replace(/closed/g, "");
			b.className = newClass;
			
			a.className += " closed";
		}
	},
	toggle : function (a) {
		a = document.getElementById(a);
		if (!a) return false;
		
		if (a.className.indexOf("closed") != -1) {
			oldClass = a.className;
			newClass = oldClass.replace(/closed/g, "");
			a.className = newClass;
		} else {
			a.className += " closed";
		}
	},
	show : function (a) {
	    a = document.getElementById(a);
	    if (!a) return false;
	    
	    if (a.className.indexOf("closed") != -1) {
	        oldClass = a.className;
	        newClass = oldClass.replace(/closed/g, "");
	        a.className = newClass;
	    }
	},
	hide : function (a) {
	    a = document.getElementById(a);
	    if (!a) return false;
	    
	    if (a.className.indexOf("closed") == -1) {
	        a.className += " closed";
	    }
	},
	showVisibility : function (a) {
	    a = document.getElementById(a);
	    if (!a) return false;
	    
	    if (a.className.indexOf("off") != -1) {
	        oldClass = a.className;
	        newClass = oldClass.replace(/off/g, "");
	        a.className = newClass;
	    }
	},
	hideVisibility : function (a) {
	    a = document.getElementById(a);
	    if (!a) return false;
	    
	    if (a.className.indexOf("off") == -1) {
	        a.className += " off";
	    }
	},
	togglePropertyValue : function (a, prop, value1, value2) {
		a = document.getElementById(a);
		
		if (!a) return false;
		
		if (a[prop].indexOf(value1) != -1) {
			a[prop] = value2;
		} else {
			a[prop] = value1;
		}
	},
	appendClass : function (a, classValue) {
	    a = $j(a);
	    if (!a) return false;
	
	    if (!a.hasClassName(classValue)) {
	        a.addClassName(classValue);
	    }
	},
	removeClass : function (a, classValue) {
	    a = $j(a);
	    if (!a) return false;
	
	    if (a.hasClassName(classValue)) {
	        a.removeClassName(classValue);
	    }
	}
};

