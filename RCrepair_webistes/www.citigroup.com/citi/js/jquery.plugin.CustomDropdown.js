$.fn.customDropdown = function(params) {
	/*
	_param = {
		container:DOMElement,
		slideUpDur:Number,
		slideDownDur:Number,
		showOriginal:Boolean,
		noBottomPadDiv:Boolean,
		defaultLabel:String,
		width:Number,
		height:Number
	}
	*/
	
	if(!params) params = {};
	
	var _this = this;
	var theParent = $(_this).parent();
	
	// generate uid to link all sub elements
	var dateObj = new Date();
	var uid = _this.uid = 'custom-dropdown-' + dateObj.getTime();
	
	// this is the whole thing
	var theBuild = _this.theBuild = $('<div id="'+uid+'" data-uid="'+uid+'" class="custom-dropdown"></div>');
	
	// the header
	var theHeader = _this.theHeader = $('<div class="custom-dropdown-head" data-uid="'+uid+'"></div>');
	theHeader.bind({
		'click':function(e){
			if(isDropdownShown){
				_this.hideDropdown();
			}else{
				_this.showDropdown();
			}
		}
	})
	
	// for the animation of the dropdown
	var theAnimationWrapper = $('<div class="custom-dropdown-animationWrapper" data-uid="'+uid+'"></div>');
	theAnimationWrapper.css({
		'over-flow':'hidden'
	});
	var isDropdownShown = false;
	
	// the dropdown that contains all option items
	var theDropdown = $('<div class="custom-dropdown-dropdown" data-uid="'+uid+'"></div>');
	var theDropdownWrapper = $('<div class="custom-dropdown-dropdown-item-wrapper"></div>');
	
	theDropdown.append(theDropdownWrapper);
	theAnimationWrapper.append(theDropdown);
	
	popuplateCustomDropdown();
	
	theBuild.append(theHeader);
	theBuild.append(theAnimationWrapper);
	
	if(params.showOriginal != true) $(_this).hide();
	$(_this).bind({
		'change':function(e){
			trace("select:change:"+$(_this)[0].selectedIndex)
			_this.update(parseInt($(_this)[0].selectedIndex), true);
		}
	});
	
	theAnimationWrapper.hide();
	
	$(_this).after(theBuild);
	
	$('html').bind({
		'click':function(e){
			if(e.target!=theHeader.get()[0] && e.target!=$(theBuild).find('.jspDrag')[0] && e.target!=$(theBuild).find('.jspTrack')[0] && e.target!=$(theBuild).find('.jspPane')[0]){
				_this.hideDropdown();
			}
		}
	});
	
	/*-------------------------------------------------------------------------
	 * methods
	 *-------------------------------------------------------------------------*/
	
	function getItemIdByValue(value){
		return theBuild.find('.custom-dropdown-item[data-value="'+value+'"]').attr('data-id');
	}
	
	_this.updateByValue = function(value, quietUpdate){
		var id = $(theBuild.find('.custom-dropdown-item[data-value="'+value+'"]')[0]).attr('data-id');
		_this.update(id, quietUpdate);
	}
	
	_this.update = function(itemId, quietUpdate, params){
		// update the header with the passed item id
		
		itemId = parseInt(itemId);
		theHeader.text(theBuild.find('.custom-dropdown-item[data-id="'+itemId+'"]').text());
		if(params && params.defaultLabel){
			theHeader.text(params.defaultLabel);
		}
		
		theBuild.find('.custom-dropdown-item').each(function(){
			if($(this).attr('data-id')==itemId){
				$(this).addClass('custom-dropdown-item-selected');
			}else{
				$(this).removeClass('custom-dropdown-item-selected');
			}
		});
		theBuild.attr('data-value', theBuild.find('.custom-dropdown-item[data-id="'+itemId+'"]').attr('data-value'));
		theBuild.attr('data-selectedIndex',theBuild.find('.custom-dropdown-item[data-id="'+itemId+'"]').attr('data-id'));
		$(_this).val(theBuild.find('.custom-dropdown-item[data-id="'+itemId+'"]').attr('data-value'));
		
		_this.hideDropdown();
		
	}
	
	_this.getValue = function(){
		return theBuild.attr('data-value');
	}
	
	_this.setWidth = function(p){
		if(params && params.width){
			theHeader.width(params.width);
		}
		if(p){
			theHeader.width(pp);
		}
		theAnimationWrapper.width(theHeader.width() + ((navigator.appVersion.indexOf("MSIE") >= 0)?(0):(2)));
		theDropdown.width(theHeader.width() + ((navigator.appVersion.indexOf("MSIE") >= 0)?(0):(2)));
	}
	
	_this.showDropdown = function(){
		
		theAnimationWrapper.show();
		
		_this.setWidth();
		
		theDropdown.css('top',-theDropdown.height()*1.5).stop(true,false).animate({
			top:0
		},{
			easing:'easeOutQuart',
			duration:params.slideDownDur
		});
		
		isDropdownShown = true;
		
		applyJScrollPane();
		applyJScrollPane();
		
	}
	
	_this.hideDropdown = function(){
		theDropdown.stop(true,false).animate({
			top:-theDropdown.height()*1.5
		},{
			easing:'easeInQuad',
			duration:params.slideUpDur,
			complete:function(){
				theAnimationWrapper.hide();
			}
		});
		isDropdownShown = false;
	}
	
	function applyJScrollPane(){
		trace('applyJScrollPane()...');
		
		if(params.noScroll!=true){
			theDropdownWrapper.jScrollPane({
				verticalDragMaxHeight:37,
				verticalDragMinHeight:37
			});
			theDropdownWrapper.find('.jspContainer').height(theDropdown.find('.jspPane').height());
		}else{
			
		}
		
	}
	
	function reinitializeJScrollPane(){
		//trace('reinitializeJScrollPane()...');
		if(params.noScroll==true){
			theDropdownWrapper.find('.jspContainer').remove();
			theDropdownWrapper.data('jsp',null);
			applyJScrollPane();
			return;
			var jspAPI = theDropdownWrapper.data('jsp');
			if(jspAPI){
				jspAPI.reinitialise();
			}
		}
	}
	
	function popuplateCustomDropdown(customLabel, quietUpdate){
		
		if(!customLabel) customLabel = 0;
		
		//theBuild.find('.jspContainer').remove();
		theBuild.find('.custom-dropdown-dropdown-bottom').remove();
		theBuild.find('.custom-dropdown-item').unbind('click').remove();
		
		var optionCount = 0;
		var newItem;
		$(_this).find('option').each(function(){
			newItem = $('<div class="custom-dropdown-item" data-id="'+optionCount+'" data-value="'+$(this).attr('value')+'" data-uid="'+uid+'">'+$(this).text()+'</div>');
			optionCount++;
			newItem.bind({
				'click':function(){
					$(_this)[0].selectedIndex = parseInt($(this).attr('data-id'));
					$(_this).trigger('change');
				}
			});
			if(theDropdownWrapper.find('.jspPane').length>0){
				theDropdownWrapper.find('.jspPane').append(newItem);
			}else{
				theDropdownWrapper.append(newItem);
			}
		});
		if(newItem) newItem.addClass('custom-dropdown-dropdown-item-lastchild');
		
		if(params.noBottomPadDiv!=true){
			theDropdown.append('<div class="custom-dropdown-dropdown-bottom"><div></div></div>');
		}
		
		if(_this.update) _this.update(customLabel, quietUpdate,{
			defaultLabel:customLabel
		});
		
	}
	_this.repopulateCustomDropdown = popuplateCustomDropdown;
	
	if(!params.defaultLabel) params.defaultLabel = 0;
	_this.update(0, true,{
		defaultLabel:params.defaultLabel
	});
	
	theDropdown.css('top',-theDropdown.height()*1.5);
	
	//applyJScrollPane();
	//theAnimationWrapper.hide();
	reinitializeJScrollPane();
	
	_this.setWidth();
	
	return _this;
	
}
