
var slideVideoController = (function(obj){
	
 /**************************************************************************
* DEFINE ROUTE SCOPE
*/	

	var _this = obj;
	
 /**************************************************************************
* DEFINE WHAT THE PROPER VIDEO INDEXES IN A JSON OBJECT ARRAY
*/
	
	var videosAtIndex = [
	    {
	        id:0,
			index: 12,
	        url: "http://www.youtube.com/embed/FS1xusMh9H0?&rel=0&modestbranding=1&autoplay=1",
			thumb: '<a href="#" onclick="slideVideoController.playVideo(0); getMetrics(\'200-19\', this); return false;"><img src="../images/landingPage/microfinance-thumb.jpg"/*tpa=http://www.citigroup.com/citi/images/landingPage/microfinance-thumb.jpg*/ /></a>'
	        
	    },
	   {	
			id:1,
	        index: 0,
	        url: "http://www.youtube.com/embed/ue3024EekwY?&rel=0&modestbranding=1&autoplay=1",
			thumb: '<a href="#" onclick="slideVideoController.playVideo(1); getMetrics(\'200-0\', this); return false;"><img src="../images/landingPage/timeline/citi_cover_image.jpg"/*tpa=http://www.citigroup.com/citi/images/landingPage/timeline/citi_cover_image.jpg*/ /></a>'

	    }
	]
	
	var totalVideosCount = 2;
	
	
	
 /**************************************************************************
* DEFINE GLOBAL VARIABLES
*/

    var setKillIndex;
    var shouldKillVideo = false;
	
 /**************************************************************************
* DEFINE LOCAL METHOD FOR EMBEDDING YOUTUBE VIDEO BASED OFF OF INDEX
*/

    function embedVideo(index, url) {

		var height = parseInt($('ul.expanded-slides li.expanded-slide').eq(index).find('div.video-embed-holder').css('height'));
		var width = parseInt($('ul.expanded-slides li.expanded-slide').eq(index).find('div.video-embed-holder').css('width'));
        
        var embed =	'<iframe width="' + width + '" height="' + height + '" src="' + url + '" frameborder="0" allowfullscreen></iframe>';
        $('ul.expanded-slides li.expanded-slide').eq(index).find('div.video-embed-holder').html("");
        $('ul.expanded-slides li.expanded-slide').eq(index).find('div.video-embed-holder').html(embed);
    }

	_this.playVideo = function(index) {
		
		
		var height = parseInt($('ul.expanded-slides li.expanded-slide').eq(videosAtIndex[index].index).find('div.video-embed-holder').css('height'));
		var width = parseInt($('ul.expanded-slides li.expanded-slide').eq(videosAtIndex[index].index).find('div.video-embed-holder').css('width'));
		
        var embed =	'<iframe width="' + width + '" height="' + height + '" src="' + videosAtIndex[index].url + '" frameborder="0" allowfullscreen></iframe>';
        $('ul.expanded-slides li.expanded-slide').eq(videosAtIndex[index].index).find('div.video-embed-holder').html("");
        $('ul.expanded-slides li.expanded-slide').eq(videosAtIndex[index].index).find('div.video-embed-holder').html(embed);
    }

	function embedThumb(index) {

        var embed =	videosAtIndex[index].thumb;
		//alert(embed);
        $('ul.expanded-slides li.expanded-slide').eq(videosAtIndex[index].index).find('div.video-embed-holder').html("");
        $('ul.expanded-slides li.expanded-slide').eq(videosAtIndex[index].index).find('div.video-embed-holder').html(embed);
    }

	
    
 /**************************************************************************
* DEFINE PUBLIC METHOD FOR KILLING YOUTUBE PLAYER 
*/
   /* _this.killPlayingVideo = function() {
       
    }*/
 /**************************************************************************
* DEFINE PUBLIC METHOD FOR KILLING ALL VIDEOS
*/   
    _this.resetAll = function(index) {
        for(i = 0; i < totalVideosCount; i++) { 
 	
           embedThumb(i);
            
        }
    }
    
    
 /**************************************************************************
* DEFINE PUBLIC METHOD FOR CHECKING IF SLIDE HAS A YOUTUBE VIDEO ON IT
*/  

    _this.checkIfPlaying = function(index) {
        var activeVideo = false;
        var activeVideoIndex;
        for(i = 0; i < totalVideosCount; i++) { 
            if(videosAtIndex[i].index == index) {
                activeVideo = true;
                activeVideoIndex = i;
            }
        }
        
        
        if(activeVideo == true) {

            setKillIndex = activeVideoIndex;
            shouldKillVideo = true;
        }
        
        else {
            setKillIndex = "undefined";
            shouldKillVideo = false;
        } 
        
    }
    
 /**************************************************************************
* DEFINE PUBLIC METHOD FOR HIDING YOUTUBE VIDEO PLAYERS - Iframes cannot be
fadded with jquery
*/
    _this.hideVideos = function(index) {
        slideVideoController.resetAll();
        $('div.video-embed-holder').css({left:-5000})
    }
    
    _this.showVideos = function(index) {
         $('div.video-embed-holder').css({left:0})
    }
	

	
	return obj;
	
})(slideVideoController || {});



slideCallbackExtension.addEventListener("slide_complete",function(eventObj){
    
    var index = eventObj.index;
    slideVideoController.resetAll();
    slideVideoController.checkIfPlaying(index);
   
   
});

/*slideCallbackExtension.addEventListener("slide_to",function(eventObj){
    var index = eventObj.index;
    slideVideoController.simple
    
    
});*/

