/*
OnlineOpinion v5.8.3
Released: 08/25/2014. Compiled 08/25/2014 11:42:12 AM -0500
Branch: master 1df595f72e14470fd6e8ee17101ac621a6f7bf30
Components: Full
UMD: disabled
The following code is Copyright 1998-2014 Opinionlab, Inc. All rights reserved. Unauthorized use is prohibited. This product and other products of OpinionLab, Inc. are protected by U.S. Patent No. 6606581, 6421724, 6785717 B1 and other patents pending. http://www.opinionlab.com
*/

/* global window, OOo */

(function (w, o) {
    'use strict';
	
    OOo.neverShow = function () {
        OOo.createCookie('oo_inv_reprompt', 1, 31557600);
        OOo.hidePrompt();
    };

      o.oo_feedback = new o.Ocode({
            // newWindowSize: [400, 400]
     });
    
   var OpinionLabInitInvite = function () {
        /* Run the Invitation instance */
        var oo_invite = new OOo.Invitation({

            // Path on your web server to the invitation assets in this code set
            pathToAssets: '/onlineopinionV58/',

            // Percentage of visitors who will receive the invite prompt
            responseRate: 20,

            // Time in seconds (30 days) before a client will be re-prompted
            repromptTime: 2592000,

            // Time in seconds before the user will see the invite prompt
            promptDelay: 2,

            // 'popup' displays over current window
            // 'popunder' displays underneath current window
            popupType: 'popunder',

            // Path to the banner_citigroup.jpg included in this code set
            companyLogo: 'Unknown_83_filename'/*tpa=http://www.citigroup.com/onlineopinionV58/banner_citigroup.jpg*/,

            // newWindowSize: [380, 270],

            /*
            Will rewrite the "referer" passed to the OpinionLab server, 
            which is the basis for card activations and report groups. 
            If not used the code will simply pass the current URL.
            */
            referrerRewrite: {
                searchPattern: /:\/\//g,
                replacePattern: '://inviteblog.citigroup.com'    }
            // The key value pairs for customVariables can be modified to pass in custom values
            /*
            customVariables: {
                Name1: 'foo',
                Name2: 'bar'
            }*/
        });
    };
    
  

    o.addEventListener(w, 'load', OpinionLabInitInvite, false);

    var OpinionLabInitTab = function () {

        o.oo_tab = new o.Ocode({
            tab: {
            }
            // ,newWindowSize: [400, 400]
        });

    };

    o.addEventListener(w, 'load', OpinionLabInitTab, false);
    
})(window, OOo);

var oo_feedback = new OOo.Ocode({
	onPageCard: {
	  closeWithOverlay: {}
	}
});

