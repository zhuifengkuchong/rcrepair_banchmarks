(function( $ )
{
	$.fn.homeSliderControl = function() {
		

/**************************************************************************
* ESTABLISH ROUTE SCOPE
*/
 	var obj = this;
 	
	
/**************************************************************************
* DEFINE GLOBAL VARIABLES
*/
    
    //initial settings
    var numSlides           =       obj.find('li.panel').size();  //get total number of slides
    var panelWidth          =       obj.find('li.panel').width();  //get the panel width of each item
    var panelMargin         =       parseInt(obj.find('li.panel').css('margin-right')); //get the panel margin to adjust for anim distance
    var animDistance        =       panelMargin + panelWidth;
    var totalWidth          =       animDistance * numSlides;
    var slidesVisible       =       4;
    
    var panelOpen           =       false;
    
    //animationBooleans
    var isAnimating         =       false;
    
    //set index
    var currentIndex        =       0;
    
    
    
/**************************************************************************
* MAIN INTRO ANIMATION
*/

    if(!isIE()) {
        delay = 0;
        obj.find('li.panel').each(function() {
            $(this).delay(delay).fadeTo(1000,1);
            delay += 100;
        });
    } else {
        obj.find('li.panel').show();
    }

       
        
    
    
 
 /**************************************************************************
* DEFINE MAIN FUNCTIONS
*/
    /*on initialization checks whether or not to display arrows
    and sets their current opacity*/
    function setArrows() {
        if(numSlides <= slidesVisible) {
             $('a.arrow-right').remove();
             $('a.arrow-left').remove();
        } else {
            checkArrows();
        }
    }
    
    
    //utility function to delegate arrow display on animation
    function checkArrows() {
        if(currentIndex == (numSlides - slidesVisible)) {
            //animation for IE browsers - no fades
            if(isIE()) {
                $('a.arrow-right').hide();
                $('a.arrow-left').show();
            }
            else {
                $('a.arrow-right').fadeTo(600, 0.3);
                $('a.arrow-left').fadeTo(600, 1);
            }
        } else if(currentIndex == 0) {
            //animation for IE browsers - no fades
            if(isIE()) {
                $('a.arrow-left').hide();
                $('a.arrow-right').show();
            } else {
                $('a.arrow-left').fadeTo(600, 0.3);
                $('a.arrow-right').fadeTo(600, 1);
            }
        } else {
            //animation for IE browsers - no fades
            if(isIE()) {
                $('a.arrow-left').show();
                $('a.arrow-right').show();
            } else {
                $('a.arrow-right').fadeTo(600, 1);
                $('a.arrow-left').fadeTo(600, 1);
            }
        }
    }
    
    
    
    //main function that slides panel left or right
    function SlideController(direction) {
        if(isAnimating == false) {
            
            if((direction === 'rt') && (currentIndex  < (numSlides - slidesVisible))) {
                isAnimating = true;
                obj.find('ul.slide-control').animate({left: '-=' + animDistance}, 
                    function() {
                        isAnimating = false;
                        currentIndex ++;
                        checkArrows();
                });
            
            } else if((direction === 'lt') && (currentIndex > 0)) {
                isAnimating = true;
                obj.find('ul.slide-control').animate({left: '+=' + animDistance}, 
                    function() {
                        isAnimating = false;
                        currentIndex --;
                        checkArrows();
                });
            }
        }
    }
    
  $.fn.homeSliderControl.showController = function() {
      //obj.css({display : 'block'});
      obj.fadeTo(500, 1);
  }
  
  function showSlider(index) {
      socialMediaShelf.hideTab();
      slideCallbackExtension.slideTo(index);
      $.fn.expandedSliderControl.expandLargeSlider(index);
  }
  
  $.fn.expandedSliderControl.showSliderAtIndex = function(index) {
  		//if(index==6) {
  		//	index= 14;
  		//}else if(index == 14) {
  		//	index= 6;
		//}
      showSlider(index);
  }
    

 
/**************************************************************************
* BIND EVENT LISTENERS 
*/	
 
 /*bind listeners for controller arrows*/
    obj.find('a.arrow-right').click(function(e) {
        e.preventDefault();
        getMetrics('rt-20');
        SlideController('rt');
    });
    obj.find('a.arrow-left').click(function(e) {
        e.preventDefault();
        getMetrics('lt-20');
        SlideController('lt');
      });
 
    obj.find('ul.slide-control li.panel').eq(0).find('.slide-trigger').click(function() {
       getMetrics('progress26');
       //showSlider(1);
       window.location='http://www.citigroup.com/citi/progress/js/progress/index.htm';
    });
    
    obj.find('ul.slide-control li.panel').eq(1).find('.slide-trigger').click(function() {
       getMetrics('200-5');
       //showSlider(7);
       window.location='http://www.citigroup.com/citi/about/history/105.htm';
    });
    
    obj.find('ul.slide-control li.panel').eq(2).find('.slide-trigger').click(function() {

        //reversed position with ingenuity
        getMetrics('200-3');
        window.location='http://www.citigroup.com/citi/about/history/103.htm';
        //window.open('http://everystep.citi.com/','_blank');
        //showSlider(6);
    });

     obj.find('ul.slide-control li.panel').eq(3).find('.slide-trigger').click(function() {
       getMetrics('200-13');
       //showSlider(4);
       window.open('http://www.citigroup.com/citi/about/history/104.htm','_blank');
    }); 
 
     obj.find('ul.slide-control li.panel').eq(4).find('.slide-trigger').click(function() {
       getMetrics('200-6');
       //showSlider(4);
       window.location='http://www.citigroup.com/citi/about/history/106.htm';
    }); 

     obj.find('ul.slide-control li.panel').eq(5).find('.slide-trigger').click(function() {
       getMetrics('200-0');
       //showSlider(4);
       window.location='http://www.citigroup.com/citi/about/history/index.htm';
    }); 

    //obj.find('ul.slide-control li.panel').eq(4).find('.slide-trigger').click(function() {
    //   getMetrics('200-13');
    //   window.open('http://everystep.citi.com/','_blank');
        //getMetrics('200-5');
       //showSlider(14);
    //});
 
 
 /* main functionality for hover states/animation */
    var panelDelay = 200;
    var headerDelay = 1000;
    
    obj.find('.slide-trigger').mouseenter(function() {
        
        if(panelOpen === false) {
		    
			//$('html').trigger('click');
			CitiGroupGlobal.closeAllGlobalPanels();
			CitiGroupGlobal.landingPageButtons.isAnyOpen = true;
			
		    panelOpen = true;
		    
    		$(this).dispatchEvent('home_slider_open',{
    			target:$(this)
    		});
			
            index = obj.find('.slide-trigger').index(this);
        
            //animation for IE browsers - no fades
            if(isIE()) {
                obj.find('li.panel').eq(index).find('#news-slide-out').show();
            //animate for none IE browsers - with fades
            } else {
                obj.find('li.panel').eq(index).find('#news-slide-out').stop(true, true).delay(panelDelay).fadeTo(400, 1);
                $('#progress-informed').stop(true, false).fadeTo(200, 0.15);
            }
        
        }
    });
    
    obj.find('li.panel').mouseleave(function() {
        
        if(panelOpen === true) {
        
			CitiGroupGlobal.landingPageButtons.isAnyOpen = false;

        panelOpen = false;    
        
        //animation for IE browsers - no fades
        if(isIE()) {
            $(this).find('#news-slide-out').hide();
            //$('#progress-informed').show();
        } else {
            $('#progress-informed').stop(true, false).fadeTo(200, 1);
            $(this).find('#news-slide-out').stop(true, true).fadeTo(800, 0, function(){
                $(this).hide();
            });
            //$('#progress-informed').stop(true, false).fadeTo(1000, 1);
        }
        
        }
    });
    
    
    
    
    
 /* bind click event to launch ajax load and expand view */
  /*  obj.find('li.panel a').click(function(e) {
        e.preventDefault(); //prevent link from firing
        socialMediaShelf.hideTab();
        index = $(this).closest('li.panel').index(); //get index of relative panel
        //ajaxController.ajaxCheck(index);
        slideCallbackExtension.slideTo(index);
        $.fn.expandedSliderControl.expandLargeSlider(index);*/
       
       /*if(landingModel.urlLoaded[index] == false) { //check if URL has been loaded
          
           path = landingModel.getPath(index); // get appropriate path for ajax url from the model
           alert(path);
           landingModel.urlLoaded[index] = true;
           
           $('ul.ajax-test li').eq(index).html(path);
        
        } else {
            
            return;
            
        }*/
       
   /* });*/
    
   /* obj.find('.slide-trigger').click(function(e) {
        e.preventDefault(); //prevent link from firing
        socialMediaShelf.hideTab();
        index = $(this).closest('li.panel').index(); //get index of relative panel
        //ajaxController.ajaxCheck(index);
        slideCallbackExtension.slideTo(index);
        $.fn.expandedSliderControl.expandLargeSlider(index);
       
    });*/
    

    
    

    
/**************************************************************************
* INITIALIZE PAGE SETTINGS
*/

  
  setArrows()
  
  /*set total width of the slider*/
  obj.find('ul.slide-control').css({width : totalWidth});
  
  
  /*Loop through newsfeed controller and push href tags to Model Array*/
  for(i = 0; i< numSlides; i++) {
  
      link = obj.find('ul.slide-control li.panel').eq(i).find('a').eq(0).attr('href') ;
      landingModel.ajaxPaths[i] = link;
      landingModel.urlLoaded[i] = false;
    }
    
    //overwrite custom slides that are in the DOM
    landingModel.urlLoaded[0] = true;
    
		EventDispatcher.implementEventDispatcher(this)
	
 
		return this;
	
	};
})( jQuery );// JavaScript Document