function set_cookie(fontsize) {
	var search = "textsize="
	var returnvalue = "";
	var the_cookie = "textsize=" + fontsize; 
	the_cookie = the_cookie + '; expires=Thu, 2 Aug 2025 20:47:11 UTC; path=/'; 
	document.cookie = the_cookie; 
}


function get_cookie() {
	
	//LeftNavconvertTrees();
    //LeftNavexpandToItem('tree1','expandednode');

	var search = "textsize="
	var returnvalue = "";

	
	if (document.cookie.length > 0) {
		offset = document.cookie.indexOf(search);
		// if cookie exists
		if (offset != -1) { 
		
			offset += search.length;
			// set index of beginning of value
			end = document.cookie.indexOf(";", offset);
			// set index of end of cookie value
			if (end == -1) end = document.cookie.length;
			returnvalue=unescape(document.cookie.substring(offset, end));
			resizeTextsetpage(returnvalue);
		
		}
	}

if (document.getElementById("footer"))
{
	var footer = document.getElementById("footer");
	if (footer.parentNode)
	{
			var firstParent = footer.parentNode;			
			if (firstParent.parentNode)
			{
				var secondParent = firstParent.parentNode;
				if (secondParent.parentNode)				{
					var thirdParent = secondParent.parentNode;

					if (thirdParent.parentNode)
					{
						var fourthParent = thirdParent.parentNode;
						fourthParent.style.clear="both";

											
					}
				}
			}
	}
}

}


function resizeTextsetpage(multiplier) {

  if (document.body.style.fontSize == "") {
    document.body.style.fontSize = "http://www.avoncompany.com/js/1.0em";
  }

document.body.style.fontSize =  multiplier + "em";
}


function resizeText(multiplier) {

if (multiplier == .85){

	if (document.getElementById("footer"))
        {
	var footer = document.getElementById("footer");
	if (footer.parentNode)
	{
			var firstParent = footer.parentNode;			
			if (firstParent.parentNode)
			{
				var secondParent = firstParent.parentNode;
				if (secondParent.parentNode)				{
					var thirdParent = secondParent.parentNode;

					if (thirdParent.parentNode)
					{
						var fourthParent = thirdParent.parentNode;
						fourthParent.style.clear="both";	
						fourthParent.style.paddingTop="50px";
										
					}
				}
			}
	}
}
}else{

	if (document.getElementById("footer"))
        {
	var footer = document.getElementById("footer");
	if (footer.parentNode)
	{
			var firstParent = footer.parentNode;			
			if (firstParent.parentNode)
			{
				var secondParent = firstParent.parentNode;
				if (secondParent.parentNode)				{
					var thirdParent = secondParent.parentNode;

					if (thirdParent.parentNode)
					{
						var fourthParent = thirdParent.parentNode;
						fourthParent.style.clear="both";	
						fourthParent.style.paddingTop="0px";						
										
					}
				}
			}
	}
}

}

  if (document.body.style.fontSize == "") {
    document.body.style.fontSize = "http://www.avoncompany.com/js/7.5em";
  }
  document.body.style.fontSize =  multiplier + "em";
  set_cookie(multiplier);
}

function search(){

	if (document.searchForm){
	    var searchtxt = document.searchForm.zoom_query.value;
        document.searchForm.action="/search/default.aspx?zoom_query="+searchtxt;
//		document.searchForm.action="http://www.avoncompany.com/search/default.aspx";
        document.searchForm.submit();
	}	
}

function SearchRKey(evt) {
	var evt  = (evt) ? evt : ((event) ? event : null);
	var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
	if ((evt.keyCode == 13) && (node.type=="text")) { 
	   if (document.searchForm){
	var searchtxt = document.searchForm.zoom_query.value;

        document.searchForm.action="/search/default.aspx?zoom_query="+searchtxt;
//		document.searchForm.action="http://www.avoncompany.com/search/default.aspx";
		   document.searchForm.submit();
	   }
	}
}
document.onkeypress = SearchRKey;


// Used for the product lifecycle to show the information based on the hover effect of the anchor tags.
$(document).ready(function(){
	$("#cycle_box a").hover(function() {
		var content = $(this).attr('title');
		$(".cycle_box_info").html(content);
	},
	function() {
		$(".cycle_box_info").html('');
	});
	$("#cycle_box a#cycle_box_six").click(function(){ return false; });



	$('.pageSlider').anythingSlider({
		easing: "easeInOutExpo",// Anything other than "linear" or "swing" requires the easing plugin
		autoPlay: true,// This turns off the entire FUNCTIONALY, not just if it starts running or not.
		delay: 3500, // How long between slide transitions in AutoPlay mode. User selected less than 0, setting default 5000
		startStopped:  false,    // If autoPlay is on, this can force it to start stopped
		animationTime: 600,  // How long the slide transition takes. User selected less than 0, setting default 600
		hashTags: false,// Should links change the hashtag in the URL?
		buildNavigation: true,  // If true, builds and list of anchor links to link to each slide
		pauseOnHover: true,// If true, and autoPlay is enabled, the show will pause on hover
		startText: "&gt;",   // Start text
		stopText: "||"
	});
	
	//slideShow_inPage();
});
function slideShow_inPage() {
	$('#gallery p').css({opacity: 0.0});
	$('#gallery p:first').css({opacity: 1.0});
	$('#gallery .caption').css({opacity: 1.0});
	$('#gallery .caption').css({width: $('#gallery p').find('img').css('width')});
	$('#gallery .content').html($('#gallery p:first').find('img').attr('alt')).animate({opacity: 1.0}, 800);
	setInterval('gallery_inPage()',3000);
}
function gallery_inPage() {
	var current = ($('#gallery p.show')?  $('#gallery p.show') : $('#gallery p:first'));
	var next = ((current.next().length) ? ((current.next().hasClass('caption'))? $('#gallery p:first') :current.next()) : $('#gallery p:first'));
	var caption = next.find('img').attr('alt');	
	next.css({opacity: 0.0}).addClass('show').animate({opacity: 1.0}, 300);
	current.animate({opacity: 0.0}, 300).removeClass('show');
	$('#gallery .content').html(caption);
	$('#gallery .caption').animate({opacity: 0.0}, { queue:false, duration:0 }).animate({height: '1px'}, { queue:true, duration:200 });	
	$('#gallery .caption').animate({opacity: 1.0},200 ).animate({height: '45px'},200 );
}