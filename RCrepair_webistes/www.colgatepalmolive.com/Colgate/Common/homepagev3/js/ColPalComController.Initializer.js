$( document ).ready( function () {
	log( 'I---->Initializing COL_PAL_COM_CONTROLLER.initialize()' );
	COL_PAL_COM_CONTROLLER.initialize();
	log( 'I---->Initializing COL_PAL_COM_CONTROLLER.initOmnitureModule()' );
	COL_PAL_COM_CONTROLLER.initOmnitureModule();
});

/**
 *	- FAKE OMNITURE FOR TESTING LOCALLY
 *	- For localhost testing of Omniture
 */
if ( location.host.indexOf( 'localhost' ) !== -1 ) {
	var _omniture = {};
	_omniture.pageName = 'http://www.colgatepalmolive.com/Colgate/US/HomePage.cvsp';
	var sendPageViewEvent = function ( str, page ) {
		log( '----><!>FAKE - sendPageViewEvent:\nstr: ' + str + '\npage: ' + page );
	};
}