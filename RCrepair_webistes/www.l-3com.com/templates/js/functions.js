jQuery.noConflict();


// Begin jQuery Functionality
jQuery(document).ready(function() { 

  // Site Navigation. Superfish implementation.
  jQuery('#nav ul.menu').superfish({
    hoverClass: 'sfhover',
    delay: 0,
    animation: {opacity:'show'},
    autoArrows: true,
    speed: 'fast'
  }); 
  
  
  // Slideshow functionality
  jQuery('#slideshow').cycle({ 
    fx: 'fade', 
    speed: 500, 
    timeout: 6000,
    cleartype: 1,
    cleartypeNoBg: 1,
    next: '#next-slide', 
    containerResize: false,
    prev: '#previous-slide',
    pager: '#pager-items',
    pagerAnchorBuilder: function(idx, slide) { 
      return '<li class="pager-item"><a href="#"><img src="../../images/blank.gif"/*tpa=http://www.l-3com.com/images/blank.gif*/ alt="" /></a></li>'; 
    }
  });
  
  // Careers Slideshow functionality
  jQuery('#careers-slideshow').after('<div id="careers-slideshow-nav"></div>').cycle({ 
    fx: 'fade', 
    speed: 500, 
    timeout: 6000,
    containerResize: false,
    pager: '#careers-slideshow-nav'
  });
  
  
  // Rotate Module functionality
  jQuery('.rotate.content-module.module-middle-padding').cycle({ 
    fx: 'fade', 
    speed: 500, 
    timeout: 10000,
    next: '#next-rotate',
    prev: '#previous-rotate',
    containerResize: false
  });  
  
  jQuery('.rotate').cycle({ 
    fx: 'fade', 
    speed: 500, 
    timeout: 5000,
    next: '#next-rotate',
    prev: '#previous-rotate',
    containerResize: false
  });

  jQuery(".webcasts-module p").each(function(i) {
    $(this).addClass('row'+(i+1));
  });
  
  /*
  // Special Nav Drop Downs
  jQuery(".item3").tooltip({ 
    tip: '#item3-table', 
    position: 'bottom middle', 
    lazy: true, 
    offset: [0, -175], 
    delay: 300
  });
*/

  jQuery("a.fancybox").fancybox({
    'overlayColor': '#000000'
  });
  
  // Exit Site Pop-Up
  jQuery("a.redirect").fancybox({
    'padding': 0,
    'autoDimensions': false,
    'width': 600,
    'height': 300,
    'autoScale': true,
    'transitionIn': 'none',
    'transitionOut': 'none',
    'showNavArrows': false,
    'overlayShow': false,
    'type': 'iframe',
    'showCloseButton': false
  });
  jQuery('#fancyclose').click(function(){
    jQuery.fancybox.close();
  });
      
  // Togglin'
  jQuery("#collapse-capabilities").click(function () {
    jQuery("#capabilities-list").slideToggle();
    jQuery(this).text(jQuery(this).text() == 'Show All' ? 'Show Less' : 'Show All');
  }); 
  jQuery("#collapse-div-list").click(function () {
    jQuery("#x-div-list").slideToggle();
    jQuery(this).text(jQuery(this).text() == 'Show All' ? 'Show Less' : 'Show All');
  }); 
  
  jQuery("#collapse-list-link").click(function () {
    jQuery("#collapse-list").slideToggle();
    jQuery(this).text(jQuery(this).text() == 'Show All' ? 'Show Less' : 'Show All');
  }); 
  
  jQuery("#toggle-corporate-operations").click(function () { jQuery("#corporate-operations").slideToggle(); }); 
 
  jQuery(".toggle-location-country").click(function () {
    var mysel = '#' + jQuery(this).attr('id') + '-div';
    jQuery(mysel).slideToggle();
  }); 
  jQuery("#informationrequest").validate();
  jQuery(".tablesorter").tablesorter({sortList: [[0,0]] });
  
  jQuery("#collapse-solutions").click(function () {
    jQuery("#solutions-list").slideToggle();
    jQuery(this).text(jQuery(this).text() == 'Show All' ? 'Show Less' : 'Show All');
  }); 
    
  if(jQuery("#pr_year").length > 0) {
    jQuery("#pr_year").change(function() {
      var year = jQuery(this).find("option:selected").val();
      window.location = jQuery(this).attr("class") + "&pr_year=" + year; 
    });
  };
  
  jQuery(".accordion ul").hide();
  jQuery(".accordion > li").click(function(){
    jQuery("ul",this).slideToggle();
  });

  

}); 

//Iframe friendly printing
function iprint(ptarget) {
  ptarget.focus();
  ptarget.print();
} 
