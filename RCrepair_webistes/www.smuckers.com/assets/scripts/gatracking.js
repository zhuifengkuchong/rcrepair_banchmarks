var ga;

var Ga = function () {
    function triggerTrackEvent(category, action, opt_label) {
        _gaq.push(['_trackEvent', category, action, opt_label]);
    }

    function buildTrackEvent(event) {
        var $this = $(this);
        var linkLocation = getLinkLocation($this);
        var label = getLinkText($this);

        triggerTrackEvent(linkLocation, 'Click', label);
        console.log(linkLocation +" "+ label);
    }

    function getLinkLocation($this) {
        if ($this.parents('#nav-container').length > 0) {
            return 'Header';
        }

        else if ($this.parents('#footer-container').length > 0) {
            return 'Footer';
        }

        else if ($this.data('trackingcategory') != null) {
            return $this.data('trackingcategory');
        }

        else if ($this.data('pin-config') != null && $this.parent().data('trackingcategory') != null) {
            return $this.parent().data('trackingcategory');
        }
        else if ($this.is("#official-rules-sunshine"))
        {
            return 'Spread Sunshine';
        }
        else {
            return 'Outbound Link';
        }
    }

    function getLinkText($this) {
        if ($this.data('trackinglabel') != null) {
            return $this.data('trackinglabel');
        }

        else if ($this.data('pin-config') != null && $this.parent().data('trackinglabel') != null) {
            return $this.parent().data('trackinglabel');
        }

        else if ($this.html() != '') {
            return $this.html();
        }
    }

    function initialize() {
        $('a[rel="external"], a[rel="alternate"], a[target="_blank"], a[target="_new"], a[data-capture="click"]').on('click', buildTrackEvent);
    }

    initialize();

    return {
        triggerTrackEvent: triggerTrackEvent,
        buildTrackEvent: buildTrackEvent
    };
};

$(function () {
    ga = new Ga();
});
