$(document).ready(function() {
    $("#under-slider-container .slider-pagination").on("click", function(){
        $(".social .pinitbutton").css('opacity', 0);
        $(".social .fb-like").css('opacity', 0);
        $("#recipes-content-container .fb-like, #recipes-content-container .pinitbutton").css('opacity', 0);
		FB.XFBML.parse(document.getElementById(".social #fb-root"));
		FB.Event.subscribe('xfbml.render',
            function(response){
                $('.social .fb-like, .social .pinitbutton').animate({'opacity': 1}, 500);
                $("#recipes-content-container .fb-like, #recipes-content-container .pinitbutton").animate({'opacity': 1}, 500);
            });
    });
});
