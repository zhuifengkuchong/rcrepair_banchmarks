FSR.surveydefs = [{
    name: 'browse',
    section: 'public',
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 30,
        lf: 3
    },
    include: {
        urls: ['https://www.suntrust.com/Static/foresee/www.suntrust.com']
    }
}];FSR.properties = {
   repeatdays: 90,

   repeatoverride: false,

   altcookie: {
   },

   language: {
      locale: 'en'
   },

   exclude: {
   },

   zIndexPopup: 1000000,

   ignoreWindowTopCheck: false,

   reverseButtons: false,

   ipexclude: 'fsr$ip',

   invite: {
      /* desktop */
      content: '<div id=\"fsrinvite\"><div id=\"fsrcontainer\"><div class=\"fsri_sitelogo\"><img src=\"{%baseHref%}sitelogo.jpg\" alt=\"Site Logo\"></div><div class=\"fsri_fsrlogo\"><img src=\"{%baseHref%}fsrlogo.gif\" alt=\"Site Logo\"></div></div><div class=\"fsri_body\">\
      <div style=\"padding:0 0 8px 0;font-size:medium;font-weight:bold;\">We\'d welcome your feedback!</div>\
      <div style=\"padding:0 0 8px 0;\">Thank you for visiting our website. You have been selected to participate<br>in a brief customer satisfaction survey to let us know how we can improve<br>your experience.</div>\
      <div style=\"font-weight:bold;\">The survey is designed to measure your entire experience, please look for it at the <u>conclusion</u> of your visit.</div>\
      </div></div>',
      
      /*
      content: '<div id=\"fsrinvite\"><div id=\"fsrcontainer\"><div class=\"fsri_sitelogo\"><img src=\"{%baseHref%}sitelogo.gif\" alt=\"Site Logo\"></div><div class=\"fsri_fsrlogo\"><img src=\"{%baseHref%}fsrlogo.gif\" alt=\"Site Logo\"></div></div><div class=\"fsri_body\">\
      <div style=\"padding:0 0 8px 0;font-size:medium;font-weight:bold;\">We\'d welcome your feedback!</div>\
      <div style=\"padding:0 0 8px 0;\">Thank you for visiting our website. You have been selected to participate<br>in a brief customer satisfaction survey to let us know how we can improve<br>your experience.</div>\
      </div></div>',
      */

      /* mobile
      content: '<div id=\"fsrinvite\"><div id=\"fsrcontainer\"><div class=\"fsri_sitelogo\"><img src=\"{%baseHref%}sitelogo.gif\" alt=\"Site Logo\"></div><div class=\"fsri_fsrlogo\"><img src=\"{%baseHref%}fsrlogo.gif\" alt=\"Site Logo\"></div></div><div class=\"fsri_body\">\
      <div style=\"padding:0 0 5px 0;font-size:medium;font-weight:bold;\">We\'d welcome your feedback!</div>\
      <div style=\"padding:0 0 0 0;\">Thank you for visiting our website. You have been selected to participate in a brief customer satisfaction survey to let us know how we can improve your experience.</div>\
      </div></div>',
      */
      
      /* desktop */
      footer: '<div div id=\"fsrcontainer\"><div style=\"float:left;width:80%;font-size:8pt;text-align:left;line-height:12px;\">This survey is conducted by an independent company ForeSee,<br>on behalf of the site you are visiting.</div><div style=\"float:right;font-size:8pt;\"><a target="_blank" title="Validate TRUSTe privacy certification" href="https://privacy-policy.truste.com/click-with-confidence/ctv/en/www.foreseeresults.com/seal_m"><img border=\"0\" src=\"{%baseHref%}truste.png\" alt=\"Validate TRUSTe Privacy Certification\"></a></div></div>',

      /* mobile
      footer: '<div div id=\"fsrcontainer\"><div style=\"float:left;width:50%;font-size:8pt;text-align:left;line-height:12px;\">Conducted by ForeSee</div><div style=\"float:right;font-size:8pt;text-align:right;\"><a target="_blank" title="Validate TRUSTe privacy certification" href="https://privacy-policy.truste.com/click-with-confidence/ctv/en/www.foreseeresults.com/seal_m"><img style=\"width:50%;\" border=\"0\" src=\"{%baseHref%}truste.png\" alt=\"Validate TRUSTe Privacy Certification\"></a></div></div>',
      */
      
      exclude: {
            local: ['https://www.suntrust.com/server.pt?space=Login', 'https://www.suntrust.com/server.pt?space=CommunityPage&control=SetCommunity&CommunityID=1894', 'https://www.suntrust.com/server.pt?space=Redirect&CommunityID=1616', 'https://www.suntrust.com/server.pt?space=CommunityPage&cached=true&parentname=CommunityPage&parentid=1&in_hi_userid=2&control=SetCommunity&CommunityID=1685&PageID=0', 'https://www.suntrust.com/server.pt?space=Redirect&CommunityID=1805', 'https://www.suntrust.com/server.pt?open=512&objID=1239&PageID=0&cached=true&mode=2', 'https://www.suntrust.com/server.pt?open=512&objID=1418&PageID=0&cached=true&mode=2', 'https://www.suntrust.com/server.pt?open=512&objID=284&PageID=0&cached=true&mode=2', 'https://www.suntrust.com/server.pt?open=512&objID=1250&PageID=0&cached=true&mode=2', '/microsites/', '/gateway/', 'https://www.suntrust.com/Static/foresee/www2.suntrust.com', '/community/online_access/1327'],
            referrer: []
        },
      include: {
         local: ['.']
      },

      /* desktop */
      width: '500',
      /* mobile
      width: {p: '260', l: '380'},
      text: {p: '100%', l: '70%'},
      */
      bgcolor: '#333',
      opacity: 0.7,
      x: 'center',
      y: 'center',
      delay: 0,
      timeout: 0,
      buttons: {
         accept: "Yes, I'll give feedback",
         decline: 'No thanks'
      },
      hideOnClick: false,
      /* desktop */
      css: 'Unknown_83_filename'/*tpa=https://www.suntrust.com/Static/foresee/foresee-dhtml.css*/,
      /* mobile
      css: 'Unknown_83_filename'/*tpa=https://www.suntrust.com/Static/foresee/foresee-dhtml-mobile.css*/,
      */
      hide: [],
      type: 'dhtml',
      /* desktop */
      url: 'https://www.suntrust.com/Static/foresee/invite.html'
      /* mobile
      url: 'https://www.suntrust.com/Static/foresee/invite-mobile.html'
      */
   },

   tracker: {
	  width: '690',
	  height: '415',
      timeout: 3,
      adjust: true,
      alert: {
         enabled: true,
         message: 'The survey is now available.'
      },
      url: 'https://www.suntrust.com/Static/foresee/tracker.html'
   },

   survey: {
      width: 690,
      height: 600
   },

   qualifier: {
      width: '690',
      height: '500',
      bgcolor: '#333',
      opacity: 0.7,
      x: 'center',
      y: 'center',
      delay: 0,
      buttons: {
         accept: 'Continue'
      },
      hideOnClick: false,
      css: false,
      url: 'https://www.suntrust.com/Static/foresee/qualifying.html'
   },

   cancel: {
      url: 'https://www.suntrust.com/Static/foresee/cancel.html',
      width: '690',
      height: '400'
   },

   pop: {
      what: 'survey',
      after: 'leaving-site',
      pu: false,
      tracker: true
   },

   meta: {
      referrer: true,
      terms: true,
      ref_url: true,
      url: true,
      url_params: false
   },

   events: {
      enabled: true,
      id: true,
      codes: {
         purchase: 800,
         items: 801,
         dollars: 802,
         followup: 803,
         information: 804,
         content: 805
      },
      pd: 7,
      custom: {}
   },

   pool: 100,

   previous: false,

   analytics: {
      google: false
   },

   cpps: {},

   mode: 'first-party'
};
