// JavaScript Document
function myfunction() {
    var elems = document.getElementsByTagName("input");
    for (var i = 0; i < elems.length; i++) {
        if (elems[i].type == "text")elems[i].value = "";
    }
}

function clearKeyword() {
    var text = document.getElementById("header_txtSearch");
    if ('Enter Keyword' == text.value) {
        text.value = "";
    }
}

// Parse host name for banner and  environ value
banner = unescape(window.location.hostname);

if (banner.indexOf('localhost') < 0) {
    var array = banner.split('.');
    banner = array[1];
    environ = array[0];
} else { // for testing on development environment
    banner = 'Dominicks';
    environ = 'www';
}

function image_hover(divid) {
    document.getElementById(divid).innerHTML = "<img src='../../images/t2_hover_arrow.gif'/>";
}
function image_mouseout(divid1) {
    document.getElementById(divid1).innerHTML = "<img src='../../images/t2_static_arrow.gif'/>";
}


function submitform(name) {
//dijit.byId('dialog1').style.display = "none";

    name.submit();
}
function createDialog(dialogName) {

    dijit.byId(dialogName).show();
}

function clearKeywordws(Id) {
    var text = document.getElementById(Id);
    text.value = "";
}
function borderfix(containerID) {
    var flashContainer = document.getElementById(containerID);
    var flashMovie = document.createElement("div");
    flashMovie.innerHTML = flashContainer.innerHTML.replace(/</g, "<").replace(/>/g, ">");
    flashContainer.parentNode.insertBefore(flashMovie, flashContainer);
    flashContainer.parentNode.removeChild(flashContainer);
    flashMovie.setAttribute("id", containerID);
}
function pop_win(url, size) {
    var newwindow;
    if (size == '')
        newwindow = window.open(url, '_new');
    else
        newwindow = window.open(url, 'IFL', size);
    if (window.focus) {
        newwindow.focus()
    }
}

function readcookie(name, delimit) {
    if (document.cookie == '') {
        //alert("cookie not set");
        return false;
    }
    else {
        var fC, lC;
        var mcookie = unescape(document.cookie);
        fC = mcookie.indexOf(name);
        var ph = fC + name.length;
        if ((fC != -1) && (mcookie.charAt(ph) == '=')) {
            fC += name.length + 1;
            lC = mcookie.indexOf(delimit, fC);
            if (lC == -1)
                lC = mcookie.length;
            return unescape(mcookie.substring(fC, lC));
        }
        else {
            return false;
        }
    }
}
function recipeReadCookie() {
    var c1 = readcookie("recipe", "#");
    //alert(c1);
}


function writecookie(name, value, delimit) {
    if (document.cookie == '') {
        //alert("no cookie found");
        document.cookie = escape("domain=.content.safeway.com;" + name + "=" + value);
        //alert(document.cookie);
        return false;
    }
    else {
        //alert("found");
        var B4, Af, fC, lC;
        var mcookie = unescape(document.cookie);
        var ncookie;
        fC = mcookie.indexOf(name);
        var ph = fC + name.length;
        if ((fC != -1) && (mcookie.charAt(ph) == '=')) {
            B4 = mcookie.substring(0, fC);
            fC += name.length + 1;
            lC = mcookie.indexOf(delimit, fC);
            if (lC == -1) lC = mcookie.length;
            if (lC == mcookie.length) {
                ncookie = B4 + name + "=" + value;
                document.cookie = escape(ncookie);
                return true;
            }
            else {
                Af = mcookie.substring(lC, mcookie.length);
                ncookie = B4 + name + "=" + value + Af;
                document.cookie = escape(ncookie);
                return true;
            }
        }
        else {
            ncookie = mcookie + delimit + name + "=" + value;
            document.cookie = escape(ncookie);
            return false;
        }
    }
}

function recipeWriteCookie(recipeid) {
    if (recipeid != '') {
        writecookie("recipe", recipeid, "#");
        window.location = "http://www.safeway.com/CMS/assets/javascript/recipereadcookie.htm";
    }
    else {
        return false;
    }
}

function slReadCookie() {
    var c1 = readcookie("slzip", "#");
    var c2 = readcookie("sldept", "#");
    //alert(c1 + ' ' + c2);
}

function slWriteCookie() {
    if (document.getElementById("zip").value != '' && document.getElementById("dept").value != '') {
        writecookie("slzip", document.getElementById("zip").value, "#");
        writecookie("sldept", document.getElementById("dept").value, "#");
        window.location = "http://www.safeway.com/CMS/assets/javascript/slreadcookie.htm";
    }
    else {
        return false;
    }
}

function wsReadCookie() {
    var c1 = readcookie("wszip", "#");
    alert(c1);
}

function wsWriteCookie() {
    if (document.getElementById("weekly_homepage_text_input").value != '') {
        writecookie("wszip", document.getElementById("weekly_homepage_text_input").value, "#");
        window.location = "http://www.safeway.com/ShopStores/WS-Store-Results";
    }
    else {
        return false;
    }
}

function navigate(page, url) {
    if (url.indexOf("?") != -1) {
        url = url + "&banner=" + banner + "&env=" + environ;
    }
    else {
        url = url + "?banner=" + banner + "&env=" + environ;
    }

    page = "/ShopStores/" + page;

    var hiddenElement = document.createElement("input");
    hiddenElement.setAttribute("type", "hidden");
    hiddenElement.setAttribute("name", "ifrm_url");
    hiddenElement.setAttribute("id", "ifrm_url");
    hiddenElement.setAttribute("value", url);
    document.frmJavascript.appendChild(hiddenElement);
    document.frmJavascript.action = page;
    document.frmJavascript.submit();
}

function getval(inputname) {
    var retval = "";
    if (document.getElementById(inputname).value != '') {
        retval = document.getElementById(inputname).value;
    }
    return retval;
}

function wsnavigate() {
    var zip = getval("weekly_homepage_text_input");
    if (zip == 'Enter city and state, or zip code' || zip == '' || zip == '00000' || zip == '99999') {
        alert('Please enter valid city and state or zip code.');
        clearKeywordws('weekly_homepage_text_input');
        document.getElementById('weekly_homepage_text_input').focus();
    }
    else {
        var url = "http://hosted.where2getit.com/safeway/weekly720.html?form=locator_search&search=Go&addressline=" + zip;
        navigate("WS-Store-Results", url);
    }
}
function locatornavigate() {
    var dept = getval("store_locator_combobox");
    var zip = getval("store_locator_initial_txt");
    if (zip == '' || zip == '00000' || zip == '99999') {
        alert('Please enter valid city and state or zip code.');
        clearKeywordws('store_locator_initial_txt');
        document.getElementById('store_locator_initial_txt').focus();
    }
    else {
        var url = "http://hosted.where2getit.com/safeway/index720.html?form=locator_search&search=Go&" + dept + "=1&addressline=" + zip;
        navigate("Store-Locator-Results", url);
    }
}

function searchnavigate() {
    var searchterm = Trim(getval("header_txtSearch"));
    searchterm = searchterm.replace(/,/g, "");
    searchterm = searchterm.replace(/^\s*|\s*$/g, "")
    if (searchterm == '' || searchterm == 'Enter Keyword') {
        alert('To search, type something into the search box and click go');
        return false;
    }
    //var url = "http://promosearch.atomz.com/search/?sp_a=sp1003369a&sp_a=sp1003369a&sp_t=search_new&sp_k=" + banner + "&sp-sfvl-field=col&sp_q=" + searchterm;
    var url = "http://promosearch.atomz.com/search/?sp_a=sp10033374&sp_k=" + banner + "&sp-sfvl-field=col&sp_q=" + searchterm;
    navigate("Search-Results", url);
}

// Utility function to trim spaces from both ends of a string
function Trim(inString) {
    var retVal = "";
    var start = 0;
    while ((start < inString.length) && (inString.charAt(start) == ' ')) {
        ++start;
    }
    var end = inString.length;
    while ((end > 0) && (inString.charAt(end - 1) == ' ')) {
        --end;
    }
    retVal = inString.substring(start, end);
    return retVal;
}


function resizeIframeOld() {
    document.getElementById("_iframe").height = 768;
    document.getElementById("_iframe").width = 520;
    document.getElementById('_iframe').height = document.getElementById('_iframe').document.body.scrollHeight;
}


function hideContent() {

    document.getElementById("iFrameWaitState").style.visibility = "visible";
    document.getElementById("iFrameWaitState").style.display = "block";
    document.getElementById('_iframe').style.visibility = "hidden";
    document.getElementById("_iframe").style.display = "none";
}

function showContent() {
    document.getElementById('iFrameWaitState').style.visibility = "hidden";
    document.getElementById("iFrameWaitState").style.display = "none";
    document.getElementById("_iframe").style.visibility = "visible";
    document.getElementById("_iframe").style.display = "block";
    document.location.href = "#iframetop";

}

function resizeIframe(height) {
    var iframe = document.getElementById('_iframe');
    var ori_height = iframe.getAttribute('height');

    if (ori_height < height) {
        iframe.setAttribute('height', height);
    }
}

function getiframeurl(url) {
    if (typeof(banner) != "undefined") {
        if (url.indexOf("?") != -1) {
            url = url + "&banner=" + banner;
        }
        else {
            url = url + "?banner=" + banner;
        }
    }
    if (typeof(environ) != "undefined") {
        if (url.indexOf("?") != -1) {
            url = url + "&env=" + environ;
        }
        else {
            url = url + "?env=" + environ;
        }
    }
    return url;
}

// ***
// * This function reads the currunt URL and returns the value for the parameter specified in strParamName, if it is in the URL.
// * It will return the defaultValue if there is no param with specified name in the URL.
// */
function getURLParam(strParamName, defaultValue) {
    var strParamName = strParamName.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + strParamName + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    //alert(results);
    if (results == null) {
        return defaultValue;
    }
    else {
        return unescape(results[1].replace(/\+/g, " "));
    }
}

// This function checks for zip code of the US and submits the form
// if the zipcode is valid.  The form must have an input named contentURL
// form and addressInfo are required, department is optional.
function submitLocatorForm(form, addressInfo, department) {
    var zip = addressInfo.value;
    if (!isPostalCodeValid(zip, 'US')) {
        alert('Please enter valid city and state or zip code.');
        addressInfo.value = '';
        addressInfo.focus();
        return false;
    } else {
        var contentURL = form.contentURL.value;
        // parameters required for w2gi to locate a store or weekly specials.
        contentURL = contentURL + zip + '&form=locator_search&banner=' + banner + '&env=' + environ + '&hostname=' + unescape(window.location.hostname) + '&';
        if (department != null && department != '') {
            contentURL = contentURL + department + '=1&';
        }
        form.contentURL.value = contentURL;
    }
    return true;
}

/*** This section is common_validator.js file ****/
// common_validation.js
// This class contain common functions which validate any field in the site.

//Checking whether string contains only numbers
//Even spaces are not allowed
function isNumeric(strVal) {
    for (var i = 0; i < strVal.length; i++) {
        var key = strVal.charCodeAt(i);
        if (((key < '48') || (key > '57')) && (key != '46')) {
            return 1;
        }
    }

    return 0;
}

function isPostalCodeValid(theZip, theCountry) {
    var validate = '';
    if (theZip.indexOf(',') > -1) {
        return true; // check if user entered city, state.
    } else if (theZip == 'Enter city and state, or zip code' || theZip == '' || theZip == '00000' || theZip == '99999') {
        return false; // check for common invalid zips.
    } else if (theCountry == 'US') {
        validate = /(^\d{5}$)|(^\d{9}$)/;
    } else if (theCountry == 'CA') {
        validate = /^\s*[a-ceghj-npr-tvxy|A-CEGHJ-NPR-TVXY]\d[a-z|A-Z](\s)?\d[a-z|A-Z]\d\s*$/;
    } else {
        return true;
    }

    return validate.test(theZip);
}

function isEmailValid(theEmail) {
    var regExp = /^[a-zA-Z0-9]+[\w&@#\-\*\.\(\)]*[a-zA-Z0-9]@[a-zA-Z0-9]*[\w&@#\-\*\.\(\)]*[a-zA-Z0-9]\.[a-zA-Z]+$/;
    return regExp.test(theEmail);
}

function isAlphabetic(name) {
    var validate = /^[a-zA-Z]+$/;
    return validate.test(name);
}

function isPhoneValid(phone) {
    phone = setNumberFormat(phone);
    if (phone.charAt(0) == '1' && phone.length > 10) {
        phone = phone.substring(1);
        //window.alert(phone);
    }
    var regExp = /^\d{10}$/;
    return regExp.test(phone);
}

function isClubCardValid(clubCard) {
    clubCard = setNumberFormat(clubCard);
    var validate = /(^\d{10}$)|(^\d{11}$)|(^\d{14}$)|(^\d{16}$)|(^\d{19}$)/;
    return validate.test(clubCard);
}

function setNumberFormat(number) {
    var newNumber = '';
    for (i = 0; i < number.length; i++) {
        if (number.charAt(i) >= '0' && number.charAt(i) <= '9') {
            newNumber = newNumber + number.charAt(i);
        }
    }
    //alert(newPhone);
    return newNumber;
}


function TimeOutRedirect() {
    try {
        if (self.parent.frames.length != 0)
            self.parent.location = document.location;
    }
    catch (Exception) {
    }
}

function setCookie(c_name, value, expiredays) {
    var exdate = new Date();
    if (expiredays == 0) {
        var minutes = exdate.getMinutes();
        minutes += 30; // Add 30 minutes to the time
        exdate.setMinutes(minutes); // Reset to new value
    }

    exdate.setDate(exdate.getDate() + expiredays);
    document.cookie = c_name + "=" + escape(value) + ((expiredays == null) ? ";expires=Thu, 01-Jan-1970 00:00:01 GMT;" : ";expires=" + exdate.toGMTString() + ";") + ";path=/;";
}

/* new functions */

function isPasswordValid(password) {
    var validate = /^[^<]{8,12}$/;
    return validate.test(password);
}

function isNameValid(input) {
    var regExp = /^[a-zA-Z]+[a-zA-Z\s]*$/;
    return regExp.test(input);
}


function isAddressValid(input) {
    var regExp = /^[a-zA-Z0-9#]+[a-zA-Z0-9,_&@#\-\*\.\(\)\s]*$/;
    return regExp.test(input);
}

function isSecurityAnswerValid(input) {
    var regExp = /^[^<]{3}[^<]*$/;
    return regExp.test(input);
}

// Trim whitespace on both ends of an input field, replacing the original value
function trimField(field) {
    if (field && field.value && field.value.length > 0) {
        field.value = field.value.replace(/^\s*/, "").replace(/\s*$/, "");
        //window.alert(field.value);
    }
}

// Strip whitespace " ", hyphens "-", and parenthesis "()" from an input field, replacing the original value
function stripField(field) {
    if (field && field.value && field.value.length > 0) {
        var val = field.value;
        while (val.indexOf('-') > -1) {
            val = val.replace('-', '');
        }
        while (val.indexOf('(') > -1) {
            val = val.replace('(', '');
        }
        while (val.indexOf(')') > -1) {
            val = val.replace(')', '');
        }
        while (val.indexOf(' ') > -1) {
            val = val.replace(' ', '');
        }
        if (val.length > 10 && val.indexOf("1") == 0) {
            val = val.substring(1);
        }
        field.value = val;
    }
}


//crossSiteSpecialCharacterCheck

//Functionality to check for special unallowable characters in either the text field or name field

function crossSiteSpecialCharacterCheck(inputString) {

    var invalidChars = "{}<>[]:!@#$%^&;?+=_~.*\\/|'\"";

    var myBoolean = new Boolean(false);


    for (var i = 0; i < inputString.length; i++) {

        if (invalidChars.indexOf(inputString.charAt(i)) != -1) {

            myBoolean = true;

            break;

        }

    }

    return myBoolean;

}


function setLowerCaseField(field) {

    trimField(field);

    if (field && field.value) {

        field.value = field.value.toLowerCase();

    }

}


//There's a need to hard code some of this label to display the correct text on Primary Nav once click
//The struts tiles does not support special character like & so it is best to have this cross reference table in JS
//to make sure that any changes / addition will not require WCS code updates
function getPrinavText(pn) {
    var sPN = new String(pn);
    var sCaption = new String(pn);

    var aFind = new Array("Community", "HealthyLiving", "Shop", "GroceryDelivery", "Recipesandmeals", "SummerGrilling");
    var aReplace = new Array("Blog", "Healthy Living", "Our Store", "Grocery Delivery", "Recipes & Meals", "Summer Grilling");

    sCaption = '';

    for (var i = 0; i < aFind.length; i++) {
        if (aFind[i].toString().toUpperCase() == sPN.toUpperCase()) {
            sCaption = aReplace[i];
            break;
        }
    }
    return sCaption;
}
//checking IE & IE versions
function isIE () {
    return !isNaN(document.documentMode);
}

function getIEVersion(){
    return document.documentMode;
}
// add/remove of  aria states aria-selected

function addAriaState (){

    var adaOnly = "<span class='ada-only'>Selected</span>";

    $("#globalui_inner_header_middle_links a > .ada-only, #globalui_header_nav2_content a > .ada-only,#globalui_rightHeader a >.ada-only").remove();
    var selectedPrim=document.querySelectorAll("#globalui_inner_header_middle_links li span[class$='_selected'] a");
    var selectedSec = document.querySelectorAll("#globalui_header_nav2_content li span[class$='_selected'] a");

    if (getIEVersion()== 8) {
        $("#Offers-Landing-IMG a").append("<span class='ada-only'>Just for u</span>");
    }
    $(selectedPrim[0]).append(adaOnly);
    $(selectedSec[0]).append(adaOnly);

    //Adding selected on right header quick links
    $("#globalui_rightHeader li a.highlight").append(adaOnly);

    //for weekly ad which lacks _selected, state added is based on path
    var path =window.location.href;
    if ((path.indexOf("weeklyspecials") != -1) || (path.indexOf("Weekly-Specials")!= -1)){
        document.getElementById("geo-weekly-ad-link").setAttribute("aria-selected","true");
    }
}



//This is for checking of j4u enabled banner because a different header is requried for those
//This will dictate which banners to display the J4U icon on the top right of the heading
//this needs to be updated for each banner rollout
function isJ4UEnabled(url) {
    //alert(url);
    if (url.toString().toUpperCase() == "DOMINICKS" || url.toString().toUpperCase() == "SAFEWAY")
        return true;
    else
        return false;
}

// whether to show Gas Rewards!
function isGasEnabledHeader(banner) {
    var url = banner ? banner.replace(/\W/,'') : location.host.toString();
    return !!(url.toString().match(/safeway|carrs|pavilions|randalls|tomthumb|vons/i));
}

function isJ4UEnabledHeader(banner) {
    var url = banner ? banner.replace(/\W/,'') : location.host.toString();
    return !!(url.toString().match(/safeway|pavilions|carrs|vons|randalls|tomthumb/i));
}

// Scenario 1 - Commerce SIGNED
// Scenario 2 - Commerce GUEST
// Scenario 3 - Non Commerce SIGNED
// Scenario 4 - Non Commerce GUEST

//This function returns the appropriate right-header-links class based on different scenarios
function updateRightHeaderLink(scenario, banner) {
    return "globalui_header_right";
}

//This function returns the appropriate right-header-links contents
function generateRightHeaderByScenario(scenario, banner, shopurl) {
    var isLoggedIn = (scenario===1 || scenario===3),
        guest = !isLoggedIn,
        j4u = isJ4UEnabledHeader(banner),
        gas = isGasEnabledHeader(banner),
        html="";

    (scenario===1 || scenario===2) ? html = '<li><a class="toplinks_compact" id="grocery_delivery" href="' + shopurl + '" target="commerce" title="Grocery Delivery Home"><img alt="Grocery Delivery" src="../media/images/header/header_groceryDelivery_icon.png"/*tpa=http://www.safeway.com/CMS/assets/media/images/header/header_groceryDelivery_icon.png*/ ><span>Grocery Delivery</span></a><span class="sep"></span></li>'
        : html="";

    var grocery =html;
    myCardCls = (document.location.pathname.indexOf("http://www.safeway.com/CMS/assets/javascript/MyCard.page") !== -1) ? 'highlight' : '';
    myListCls = (document.location.pathname.indexOf("http://www.safeway.com/CMS/assets/javascript/MyList.page") !== -1) ? 'highlight' : '';
    j4uCls    = (document.location.pathname.indexOf("http://www.safeway.com/CMS/assets/javascript/Justforu-Coupons.page") !== -1) ? 'highlight' : '';

    gasRewardsCls = (document.location.pathname.indexOf("http://www.safeway.com/CMS/assets/javascript/RewardPoints.page") !== -1) ? 'highlight' : '';


    if (guest && j4u && gas) {
        // 1 - Signed out state with both J4U and Gas Rewards
        html += '<li><a class="toplinks_compact ' + j4uCls + '" id="shopStoreOfferLandingPage" href="javascript:goURL(\'/ShopStores/Justforu-Coupons.page\');" title="Just for U : Learn more"><span class="font-red paddingRight2">Savings</span>' +
            '<img alt="just for u" src="../media/images/header/just4u_bw_toplinks.png"/*tpa=http://www.safeway.com/CMS/assets/media/images/header/just4u_bw_toplinks.png*/ class="j4u_bw"><span class="small-font">Learn More</span></a><span class="sep"></span></li>' +
            '<li><a class="toplinks_compact last ' + gasRewardsCls + '" id="shopStoreRewardPointsPage" href="javascript:goURL(\'/ShopStores/RewardPoints.page\');" title="Gas Rewards! Learn More"><span class="font-red">Gas Rewards!</span><span class="small-font">Learn More</span></a></li>';

    } else if (isLoggedIn && j4u && gas) {
        // 2 - Signed in state with both J4U and Gas Rewards
        html = '<strong>' + grocery + '</strong>' +
            '<li><a class="toplinks_compact '+ myCardCls + '" id="shopStoreMyCardPage" href="javascript:goURL(\'/ShopStores/MyCard.page\');"><span class="font-black bold">My Card</span><span id="headerMyCardCount" class="font-red">&nbsp;</span></a><span class="sep"></span></li>' +
            '<li><a class="toplinks_compact '+ myListCls + '" id="shopStoreMyListPage" href="javascript:goURL(\'/ShopStores/MyList.page\');"><span class="font-black bold">My List</span><span id="headerMyListCount" class="font-red">&nbsp;</span></a><span class="sep"></span></li>' +
            '<li id="headerHasGasRewardsLi"><a class="toplinks_compact last ' + gasRewardsCls + '" id="headerHasGasRewards" href="javascript:goURL(\'/ShopStores/MyCard.page#GasRewards\');" title="Gas Rewards! Learn More"><span id="headerRewardsAvailable" class="font-red"></span><span class="gas-link-underlined font-black bold">Gas Rewards</span></a></li>' +
            '<li id="headerZeroGasRewardsLi"><a class="toplinks_compact last ' + gasRewardsCls + '" id="headerZeroGasRewards" href="javascript:goURL(\'/ShopStores/RewardPoints.page\');" title="Gas Rewards! Learn More"><span class="font-black bold">Gas Rewards!</span><span class="small-font">Learn More</span></a></li>';

    } else if (isLoggedIn && j4u) {
        // 7 - Signed in state with only J4U
        html = '<strong>' + grocery + '</strong>' +
            '<span class="label"> Total Offers Added:</span>' +
            '<li><a class="toplinks_compact '+ myCardCls + '" id="shopStoreMyCardPage" href="javascript:goURL(\'/ShopStores/MyCard.page\');"><span class="font-black bold">My Card</span><span class="font-red" id="headerMyCardCount"> </span></a><span class="sep"></span></li>' +
            '<li><a class="toplinks_compact '+ myListCls + '" id="shopStoreMyListPage" href="javascript:goURL(\'/ShopStores/MyList.page\');"><span class="font-black bold">My List</span><span class="font-red" id="headerMyListCount"> </span></a></li>' +
            '&nbsp;&nbsp;&nbsp;';

    } else if (isLoggedIn && gas) {
        // Signed in state with only Gas Rewards
        html = '<strong>' + grocery + '</strong>' +
            '<li id="headerHasGasRewardsLi"><a class="toplinks_compact last ' + gasRewardsCls + '" id="headerHasGasRewards" href="javascript:goURL(\'/ShopStores/RewardPoints.page\');" title="Gas Rewards! Learn More"><span class="font-red" id="headerRewardsAvailable"></span> <span class="font-black red gas-link-underlined">Gas Rewards</span></a></li>' +
            '<li id="headerZeroGasRewardsLi><a class="toplinks_compact last ' + gasRewardsCls + '" id="headerZeroGasRewards" href="javascript:goURL(\'/ShopStores/RewardPoints.page\');" title="Gas Rewards! Learn More"><span class="font-red">Save at the Pump and Earn Gas Rewards!</span></a></li>';

    } else if (gas) {
        // 4 - Banner with only Gas Rewards program available
        html += '<li><a class="toplinks_compact last '+ gasRewardsCls + '" id="shopStoreRewardPointsPage" href="javascript:goURL(\'/ShopStores/RewardPoints.page\');" title="Gas Rewards! Learn More"><span class="font-red">Save at the Pump and Earn Gas Rewards!</span> <span class="small-font">Learn More</span></a></li>';

    } else if (j4u) {
        // 5 - Banner with only J4U program available
        html += '<li id="header-j4u-alone"><a class="toplinks_compact last ' + j4uCls + '" id="shopStoreOfferLandingPage" href="javascript:goURL(\'/ShopStores/Justforu-Coupons.page\');" title="Just for U : Learn more">' +
            '<img alt="just for u" src="../media/images/header/just4u_toplinks.png"/*tpa=http://www.safeway.com/CMS/assets/media/images/header/just4u_toplinks.png*/>' +
            '<span class="font-red">Save up to 20% More</span> <span class="small-font" alt="Just for U : Learn more" title="Just for U : Learn more">Learn More</span></a></li>';

    } else {
        // 6 - No programs
        var signInEl = document.getElementById('globalui_signin_text2');
        if (signInEl) signInEl.className += ' no-program';
        return '';
    }



    /*  return '<div id="globalui_rightHeader" class="globalui_header_toplinks">' + html + '</div><div class="globalui_header_toplinks_cap"></div>';*/
    return '<div class="globalui_header_toplinks ieRoundedCorner"><ul id="globalui_rightHeader" class="globalui_header_toplinks_middle">' + html + '</ul></div>';

}

function getDomainForBanner(storeName){
	return storeName.replace(/\W/g,'').replace(/carrs/i,'carrsqc').toLowerCase() + '.com' }

function initWeeklyAdHeaderLink(domain) {
    if (window.jQuery && window.GeoAPI) GeoAPI.getStoreData(function(store) {
        var link = document.getElementById('geo-weekly-ad-link');
        if (link && store && store.name && store.storeId) {
            link.target="_top";
           link.href = 'http:///'+(domain?domain:'weeklyspecials.' + getDomainForBanner(store.name) )+ '/customer_Frame.jsp?drpStoreID=' + store.storeId + '&amp;showFlash=false';
        }
    });
}

function goURL(url) {
    url = adjustURL(url);
    if (window != window.top) {
        top.location.href = url;
    } else {
        window.location.href = url;
    }
    //temp fix to resolve gas rewards highlight
    if(url.indexOf("#GasRewards") != -1) { gasRewardsCls ="highlight"; }
}

// determines if this page is a Standalone view
function isStandalone() {
    return (window.location.href.indexOf('StandaloneHeaderView') > -1
        || window.location.href.indexOf('StandaloneFooterView') > -1);
}


/*
 Override goURL: converts relative URL to absolute if this is a standalone
 - this is necessary for IE9 security
 */
function adjustURL(url) {
    if (isStandalone() && url.indexOf(':') < 0) {
        var adjustedUrl = window.location.protocol + '//' + window.location.hostname;
        if (url.charAt(0) != '/') adjustedUrl = adjustedUrl + '/';
        adjustedUrl = adjustedUrl + url;
        return adjustedUrl;
    } else {
        return url;
    }
}

function updatePrimaryNav() {
    var aEls = document.getElementById('globalui_header_middle').getElementsByTagName('a');
    for (var i = 0, aEl; aEl = aEls[i]; i++) {
        hrefText = aEl.href;
        if (hrefText.indexOf("javascript") <= -1) {
            aEl.href = "javascript:goURL('" + hrefText + "')";
        }
    }
}

function checkContintueUrl(url) {
    if (url == "AdMatchRedirect")
        url = 'Home';

    return url;
}

//the return url needs to be a relative path
function checkAbortUrl(page, url) {
    if (page == 'LOGIN') {
        if (url == "AdMatchRedirect") {
            url = 'Home';
        }
    } else if (page == 'REGISTRATION') {
        if (url == "AdMatchRedirect") {
            url = 'Home';
        }
    }
    /*else if(page=='UPDATECONTACTINFO'){
     if (url == "AdMatchRedirect"){
     url = 'Home';
     }
     }*/

    return url;
}

function roundedCorner(){
    if(getIEVersion() <9 ){
        if(window.PIE){
            //common class name used in Portal,RSS & Email OPt pages
            $(".ieRoundedCorner").each(function(){
                PIE.attach(this);
            });

            //from footer - applies to global buttons & headings - will eventually convert to .ieRoundedCorners
            $('.lt-button-primary, .button_primary_arrow, .button_primary, .button_primary_disabled, .button_secondary, .button_tertiary').each(function(){
                PIE.attach(this);
            });
            $('.one_cm_520_first_row, .one_cm_720_first_row, .featured_brands_first_row, .cpg_landing_header, .titleHead').each(function(){
                PIE.attach(this);
            })
        }
    }
}

function windowLoaded(e){
    addAriaState();
    removeListener(e, windowLoaded, false);
}

function windowOnLoad(e) {
    roundedCorner();
    var isExt = (window.Ext !== undefined);
    // debug
    console.log('<!---', 'isExt=' + isExt + '  ' + e.type);

    if (isExt) {
        if (window.removeEventListener) {
            window.removeEventListener('load', windowOnLoad, false);
            document.removeEventListener('DOMContentLoaded', windowOnLoad, false);
        }
        else if (window.detachEvent) {
            window.detachEvent('onload', windowOnLoad);
            document.detachEvent('onreadystatechange', windowOnLoad);
        }

        Ext.core.Element.prototype.getAttribute =
            (Ext.isIE && !(!(Ext.isIE6 || Ext.isIE7 || Ext.isIE8) && document.documentMode >= 9)) ?
                function(name, ns) {
                    var d = this.dom,
                        type;
                    if(ns) {
                        type = typeof d[ns + ":" + name];
                        if (type != 'undefined' && type != 'unknown') {
                            return d[ns + ":" + name] || null;
                        }
                        return null;
                    }
                    if (name === "for") {
                        name = "htmlFor";
                    }
                    return d[name] || null;
                }: function(name, ns) {
                var d = this.dom;
                if (ns) {
                    return d.getAttributeNS(ns, name) || d.getAttribute(ns + ":" + name);
                }
                return d.getAttribute(name) || d[name] || null;
            };
    } else {
        removeListener(e,windowOnLoad,false);
    }
}
//Generic removeEventListener cross-browser

function removeListener(event,listener,useCapture){

    if (window.removeEventListener) {
        window.removeEventListener(event.type, listener, useCapture);
    } else if (window.detachEvent) {
        window.detachEvent(event.type, listener);
    }
}


if (window.addEventListener) {
    window.addEventListener('load', windowLoaded, false);
    document.addEventListener('DOMContentLoaded', windowOnLoad, false);
} else if (window.attachEvent) {
    window.attachEvent('onload', windowLoaded);
    document.attachEvent('onreadystatechange', windowOnLoad);
}

//Safeway namespaced utilities
(function(){

    //Setup the namespace
    var swy = window.swca || (window.swca = {}),
    util = swy.utilities || (swy.utilities = {});

    //locals constants

    var EXPIRE_COOKIE_DATE = new Date(0).toUTCString();

    util.allowsSessionCookies = function(){
        var testcookie = "testcookie"+new Date().getTime();
        document.cookie = testcookie + "=true";
        var canWrite = document.cookie.indexOf(testcookie) > -1;
        document.cookie = testcookie+"=; expires="+EXPIRE_COOKIE_DATE;
        return canWrite;
    }

})();

//OpinionLab Utilities - we can break this out later, but for now why not.
(function(){
    var swy = window.swca || (window.swca = {}),
        ol = swy.OpinionLab || (swy.OpinionLab = {});

    //stock install text.
    var opinionLabsInclude = '<!-- BEGIN: OnlineOpinion v5+\
* The following code is Copyright 1998-2013 Opinionlab, Inc.\
* All rights reserved. Unauthorized use is prohibited. \
* This product and other products of OpinionLab, Inc. are protected by U.S. Patent No. US 6606581, 6421724,6785717 B1 and other patents pending.\
* http://www.opinionlab.com\
-->\
<!-- MAIN OL STYLESHEET -->\
<link rel="stylesheet" type="text/css" href="../vendor/opinionlab/onlineopinionV5/oo_style.css"/*tpa=http://www.safeway.com/CMS/assets/vendor/opinionlab/onlineopinionV5/oo_style.css*/ />\
<!-- MAIN OL ENGINE -->\
<script language="javascript" type="text/javascript" charset="windows-1252" src="../vendor/opinionlab/onlineopinionV5/oo_engine.min.js"/*tpa=http://www.safeway.com/CMS/assets/vendor/opinionlab/onlineopinionV5/oo_engine.min.js*/></script>\
<!-- FEEDBACK CONFIGURATION -->\
<script language="javascript" type="text/javascript" charset="windows-1252" src="../vendor/opinionlab/onlineopinionV5/oo_conf.js"/*tpa=http://www.safeway.com/CMS/assets/vendor/opinionlab/onlineopinionV5/oo_conf.js*/></script>\
<script language="javascript" type="text/javascript" charset="windows-1252" src="../vendor/opinionlab/onlineopinionV5/oo_conf_invite.js"/*tpa=http://www.safeway.com/CMS/assets/vendor/opinionlab/onlineopinionV5/oo_conf_invite.js*/></script>\
<!-- END: OnlineOpinion v5+ -->';

    //private variable on whe
    var opinionLabable = !!!window.location.pathname.match(/standalone(header|footer)view.page$/i);
    var opinionLabIncluded = false;

    ol.writeInclude = function(element){
        var e = element || document.body;
        if(opinionLabable && !opinionLabIncluded) {
            $(document).ready(function(){
                $(e).append(opinionLabsInclude);
                opinionLabIncluded = true;
                $('.swy_opinion_lab_link').on('click',function(){oo_feedback.show()});
            });

        };
    }

    ol.excludeThisPage = function(){
        opinionLabable = false;
        $('.swy_opinion_lab_link').remove();
    }

    ol.isIncluded = function(){
        return opinionLabIncluded;
    };


})();
