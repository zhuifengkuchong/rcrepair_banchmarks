/*
OnlineOpinion v5.8.2
Released: 02/27/2014. Compiled 04/23/2014 09:51:37 AM -0500
Branch: CLIENTREQ-219 Apr
Components: Full
UMD: disabled
The following code is Copyright 1998-2014 Opinionlab, Inc.  All rights reserved. Unauthorized use is prohibited. This product and other products of OpinionLab, Inc. are protected by U.S. Patent No. 6606581, 6421724, 6785717 B1 and other patents pending. http://www.opinionlab.com
*/

/* global window, OOo */

/* Invitation configuration */
(function (w, o) {
    'use strict';

    /* Shim Date.now() for older browsers */
    if (!Date.now) {
        Date.now = function() { return new Date().getTime(); };
    }

    var rp, logo;
    var domain = w.location.hostname.split('.');
    switch(domain.length > 1 ?domain[domain.length -2]+"."+domain[domain.length -1]: w.location.hostname) {
        case 'http://www.safeway.com/CMS/assets/vendor/opinionlab/onlineopinionV5/carrsqc.com':
            rp = '://invite.carrsqc.com';
            logo = '../../../media/images/carrsqcLogo.gif'/*tpa=http://www.safeway.com/CMS/assets/media/images/carrsqcLogo.gif*/;
            break;

        case 'http://www.safeway.com/CMS/assets/vendor/opinionlab/onlineopinionV5/pavilions.com':
            rp = '://invite.pavilions.com';
            logo = '../../../media/images/pavilionsLogo.gif'/*tpa=http://www.safeway.com/CMS/assets/media/images/pavilionsLogo.gif*/;
            break;

        case 'http://www.safeway.com/CMS/assets/vendor/opinionlab/onlineopinionV5/randalls.com':
            rp = '://invite.randalls.com';
            logo = '../../../media/images/randallsLogo.gif'/*tpa=http://www.safeway.com/CMS/assets/media/images/randallsLogo.gif*/;
            break;

        case 'http://www.safeway.com/CMS/assets/vendor/opinionlab/onlineopinionV5/tomthumb.com':
            rp = '://invite.tomthumb.com';
            logo = '../../../media/images/tomthumbLogo.gif'/*tpa=http://www.safeway.com/CMS/assets/media/images/tomthumbLogo.gif*/;
            break;

        case 'http://www.safeway.com/CMS/assets/vendor/opinionlab/onlineopinionV5/vons.com':
            rp = '://invite.vons.com';
            logo = '../../../media/images/vonsLogo.gif'/*tpa=http://www.safeway.com/CMS/assets/media/images/vonsLogo.gif*/;
            break;

        default:
            rp = '://invite.safeway.com';
            logo = '../../../media/images/safewayLogo.gif'/*tpa=http://www.safeway.com/CMS/assets/media/images/safewayLogo.gif*/;
    }

    w.oo_invite = new o.Invitation({
    /* REQUIRED - Asset identification */
        pathToAssets: '/CMS/assets/vendor/opinionlab/onlineopinionV5/',
    /* OPTIONAL - Configuration */
        responseRate: 0, // percent of users
        repromptTime: 7776000, // 90 days
        promptDelay: 3, // seconds
        popupType: 'popunder',
        companyLogo: logo,
        referrerRewrite: {
            searchPattern: /:\/\/[^\/]*/,
            replacePattern: rp
        },
        customVariables: {
            s_vi: o.readCookie('s_vi'),
            timestamp: o.readCookie('s_vi') + Date.now()
        }
    });

})(window, OOo);