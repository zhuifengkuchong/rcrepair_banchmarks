/*
OnlineOpinion v5.8.2
Released: 02/27/2014. Compiled 04/23/2014 09:51:37 AM -0500
Branch: CLIENTREQ-219 Apr
Components: Full
UMD: disabled
The following code is Copyright 1998-2014 Opinionlab, Inc.  All rights reserved. Unauthorized use is prohibited. This product and other products of OpinionLab, Inc. are protected by U.S. Patent No. 6606581, 6421724, 6785717 B1 and other patents pending. http://www.opinionlab.com
*/

/* global window, OOo */


(function (w, o) {
    'use strict';

    var OpinionLabInit = function () {

        var rp;

        switch(w.location.hostname) {
            case 'http://www.safeway.com/CMS/assets/vendor/opinionlab/onlineopinionV5/www.carrsqc.com':
                rp = '://banner.carrsqc.com';
                break;

            case 'http://www.safeway.com/CMS/assets/vendor/opinionlab/onlineopinionV5/www.pavilions.com':
                rp = '://banner.pavilions.com';
                break;

            case 'http://www.safeway.com/CMS/assets/vendor/opinionlab/onlineopinionV5/www.randalls.com':
                rp = '://banner.randalls.com';
                break;

            case 'http://www.safeway.com/CMS/assets/vendor/opinionlab/onlineopinionV5/www.tomthumb.com':
                rp = '://banner.tomthumb.com';
                break;

            case 'http://www.safeway.com/CMS/assets/vendor/opinionlab/onlineopinionV5/www.vons.com':
                rp = '://banner.vons.com';
                break;

	    case 'http://www.safeway.com/CMS/assets/vendor/opinionlab/onlineopinionV5/ngcp-qa2.carrsqc.com':
                rp = '://banner.carrsqc.com';
                break;

            case 'http://www.safeway.com/CMS/assets/vendor/opinionlab/onlineopinionV5/ngcp-qa2.pavilions.com':
                rp = '://banner.pavilions.com';
                break;

            case 'http://www.safeway.com/CMS/assets/vendor/opinionlab/onlineopinionV5/ngcp-qa2.randalls.com':
                rp = '://banner.randalls.com';
                break;

            case 'http://www.safeway.com/CMS/assets/vendor/opinionlab/onlineopinionV5/ngcp-qa2.tomthumb.com':
                rp = '://banner.tomthumb.com';
                break;

            case 'http://www.safeway.com/CMS/assets/vendor/opinionlab/onlineopinionV5/ngcp-qa2.vons.com':
                rp = '://banner.vons.com';
                break;

            default:
                rp = '://banner.safeway.com';
        }

        /* Shim Date.now() for older browsers */
        if (!Date.now) {
            Date.now = function() { return new Date().getTime(); };
        }
        /* Inline configuration */

        w.oo_feedback = new o.Ocode({
            customVariables: {
                s_vi: o.readCookie('s_vi'),
                timestamp: o.readCookie('s_vi') + Date.now()
            }
        });

         /* Tab configuration */

        w.oo_tab = new o.Ocode({
            tab: {
                position: 'right',
                wcagBasePath: '/CMS/assets/vendor/opinionlab/onlineopinionV5/'
            },
            disableMobile: true,
            customVariables: {
                s_vi: o.readCookie('s_vi'),
                timestamp: o.readCookie('s_vi') + Date.now()
            }

       });

        w.oo_banner = new o.Ocode({
            customVariables: {
                s_vi: o.readCookie('s_vi'),
                timestamp: o.readCookie('s_vi') + Date.now()
            },
             referrerRewrite: {
                  searchPattern: /:\/\/[^\/]*/,
                  replacePattern: rp
            }
        });
    };

    o.addEventListener(w, 'load', OpinionLabInit, false);

})(window, OOo);