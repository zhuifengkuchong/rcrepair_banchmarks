/* exported identity, newState, bindClick, Chef, ifTablet */

/* "name": "Philip's Utils",
 * "version": "0.8.0",
 * "updated": "Oct. 23, 2014",
 * "description": "Various utility functions Philip uses for abstracting common tasks.",
 * "components": {
 *   "newState/1": "Creates a tiny state machine.",
 *   "bindClick/3": "Binds a click to a function and state.",
 *   "Chef": {
 *     "description": "A wrapper to translate JavaScript data structures into animations using TimelineMax.",
 *     "mix/1": "Concatenates an array of arrays.",
 *     "bake/2": "Adds an array of instructions to an existing TimelineMax instance.",
 *     "run/*": "Takes a dynamic amount of arrays as arguments, parses them as instructions, adds them to a new TimelineMax instance, and plays the timeline.
 *   }
 * }
 * */

function identity(n) {
  return n;
}

function newState(init) {
 return {
   state: typeof init === 'undefined' ? null : init,
   resolve: function() {
     return this.state;
   },
   update: function(new_val) {
     this.state = new_val(this.state);
     return this.resolve();
   }
 };
}

function bindClick(trigger, func, state) {
  $(trigger).click(function(e) {
    e.preventDefault();
    func(state, e);
  });
  return state;
}

var Chef = {
  _makeTween: function (specs) {
    if ($.isArray(specs[0])) {
      var tweens = [];
      for (var i = 0; i < specs.length; i++) {
        tweens.push(this._makeTween(specs[i]));
      }
      return tweens;
    } else {
      if (specs[0] === 'delay') return TweenLite.to('', specs[1]);
      var kind = specs[0], elem = specs[1],
           dur = specs[2], prop = specs[3];
      return TweenLite[kind](elem, dur, prop);
    }
  },
  mix: function (array) {
    return $.map(array, function (n) { return n; });
  },
  bake: function (timeline, animations) {
    timeline.stop();
    for (var i = 0; i < animations.length; i++) {
      timeline.add(this._makeTween(animations[i]));
    }
    return timeline;
  },
  run: function () {
    var timeline = new TimelineMax();
    this.bake(timeline, arguments);
    timeline.play();
    return timeline;
  }
};

function ifTablet(yes, no) {
  if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) return yes();
  return no();
}
