var caseys = {
	locator_lookup_table: null,
	// mobile nav menu controls
	mobNavOpen: false,
	setCookie: function(c_name,value,exdays){
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=c_name + "=" + c_value;
	},
	getCookie: function(c_name){
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++){
			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
			x=x.replace(/^\s+|\s+$/g,"");
			if (x==c_name){
				return unescape(y);
				}
		}
	},
	closeMobileNav : function() {
		$('#pageWrapper').css({
			'height' : 'auto',
			'overflow' : 'visible',
			'margin-left' : '0'
		});
		$(window).off('resize.mobNav');
		caseys.mobNavOpen = false;
	},
	openMobileNav : function() {
		$('#pageWrapper').css({
			'height' : $('#mobileNav').height(),
			'overflow' : 'hidden',
			'margin-left' : -$('#mobileNav').outerWidth()
		});
		$(window).on('resize.mobNav', function() {
			if ($(window).width() > 633) {
				caseys.closeMobileNav();
			}
		});
		caseys.mobNavOpen = true;
	},
	toggleMobileNav : function() {
		if (!caseys.mobNavOpen) {
			caseys.openMobileNav();
		} else {
			caseys.closeMobileNav();
		}
	},
	// generic toggling function for showing and hiding content
	toggleThings : function(containerSelector, togglerSelector, contentSelector) {
		$('body').on('click', togglerSelector, function(event) {
			event.preventDefault();
			$this = $(this);
			var $parent = $this.parents(containerSelector);
			var $content = $parent.find(contentSelector);
			if ($parent.hasClass('open')) {
				$parent.removeClass('open');
				$content.slideUp();
			} else {
				$parent.addClass('open');
				$content.slideDown();
			}
			$this = null;
			$parent = null;
			$content = null;
		});
	},
	// generic toggling function for showing and hiding content
	toggleExistingThings : function(containerSelector, togglerSelector, contentSelector) {
		$(togglerSelector).on('click', function(event) {
			event.preventDefault();
			$this = $(this);
			var $parent = $this.parents(containerSelector);
			var $content = $parent.find(contentSelector);
			if ($parent.hasClass('open')) {
				$parent.removeClass('open');
				$content.slideUp();
			} else {
				$parent.addClass('open');
				$content.slideDown();
			}
			$this = null;
			$parent = null;
			$content = null;
		});
	},
	// function to make sure the dropdown menus don't extend past the screen edge
	// TODO this doesn't work correctly because the font isn't loaded the first time it runs - refactor after fonts are purchased
	positionDropdownMenus : function(width) {
		$('#primaryNav .secondary').each(function(i) {
			$this = $(this);
			$this.css({
				left: '-7px',
				right: 'auto'
			});
			elRight = (width - ($this.offset().left + $this.width() + 20));
			if (elRight < 0) {
				$this.css({
					left: 'auto',
					right: '0'
				});
			}
		});
	},
	sticky_navigation_offset_top: null,
	stickyNavigation : function() {
		var scroll_top = $(window).scrollTop(); // our current vertical position from the top
		if (scroll_top + 132 > caseys.sticky_navigation_offset_top) {
			$('.subSectionNav').css({ 'position': 'absolute', 'top': scroll_top -132 + 'px'});
		} else {
			$('.subSectionNav').css({ 'position': 'absolute', 'top': 0 });
		}
	},
	// brand timeline functions and variables
	slideHeadersCreated: false,
	initializedTimeline: false,
	initializeBrandTimeline : function() {
		$('#timeline').flexslider({
			animation: "slide",
			selector : ".slides > article",
			animation: "slide",
			animationLoop: false,
			slideshow: false,
			start : function() {
				caseys.initializedTimeline = true;
			}
		});
	},
	createTimelineHeaders : function() {
		$('#timeline .slides h1').each(function() {
			$this = $(this);
			theUrl = $this.data('image');
			$this.after('<img src="'+ theUrl +'" alt="'+$this.text()+'" />');
			caseys.slideHeadersCreated = true;
		});
	},
	// variables and functions for the homepage specials area
	specialsCreated: false,
	initializedSpecials: false,
	initializeSpecials : function() {
		$('#specials').flexslider({
			animation: "slide",
			selector : ".slides > article",
			animation: "slide",
			directionNav: false
		});
		caseys.initializedSpecials = true;
	},
	createSpecials : function(theWidth) {
		$('#specials .specialContent').each(function() {
			$this = $(this);
			$this.html('');
			var largeImg = $this.data('large');
			var smallImg = $this.data('small');
			if (theWidth <= 400) {
				$this.append('<img src="'+ smallImg +'" alt="" />');
			} else {
				$this.append('<img src="'+ largeImg +'" alt="" />');
			}
			caseys.specialsCreated = true;
		});
	},
	desktopSpecialsCreated : false,
	createDesktopSpecials : function(theWidth) {
		var isTheFirst = true;
		$('#desktopSpecialsWrap .specialContent').each(function() {
			$this = $(this);
			$this.html('');
			var largeImg = $this.data('large');
			var smallImg = $this.data('small');

			var largeImgCss = 'url(' +largeImg + ') no-repeat left top';
			var smallImgCss = 'url(' +smallImg + ') no-repeat 50% top';

			if (theWidth >= 900 && isTheFirst) {
				//$this.append('<img src="'+ largeImg +'" alt="" />');
				$this.css({
					'background' : largeImgCss
				});
				isTheFirst = false;
			} else {
				//$this.append('<img src="'+ smallImg +'" alt="" />');
				$this.css({
					'background' : smallImgCss
				});
			}
			caseys.desktopSpecialsCreated = true;
		});
	},
	// function that fires on domready and resize when width is < 650
	resizePhone : function(width) {
		// footer nav controls
		$('.navSection h2').addClass('navSectionToggler');
		// put the swipe action on the mobile nav menu (swiping the header opens the menu)
		$('#pageHeader').hammer({
		}).on('swipe', function(ev) {
			if (ev.direction == 'left') {
				caseys.openMobileNav();
			} else if (ev.direction == 'right') {
				caseys.closeMobileNav();
			}
		});
		// homepage specials
		if ($('#homepageSpecials').length > 0) {
			caseys.createSpecials(width);

			if ($('#specials').length > 0 ) {
				if (caseys.initializedSpecials != true) {
					caseys.initializeSpecials();
				} else {
					if ($('#specials').data('flexslider').playing == false ) {
						$('#specials').flexslider("play");
					}
				}
			}
		}

		if ($('#specialtyPizza').length > 0 ) {
			$('#specialtyPizza .menuItems li').css({
				'height': 'auto'
			});
		}
		if ($('.peopleGrid').length > 0 ) {
			$('.peopleGrid li').css({
				'height': 'auto'
			});
		}
	},
	// function that fires on domready and resize when width is >= 650
	resizeDesktop : function(width) {
		if($('#outerWrap').length > 0 ) {
			if ($('#outerWrap').data().length > 0 ) {
				$('#outerWrap').data('hammer').destroy();
			}
		}
		$('.navSection h2').removeClass('navSectionToggler');
		if ($('#homepageSpecials').length > 0) {
			if (caseys.initializedSpecials == true) {
				$('#specials').flexslider("pause");
			}
		}
		if ($('#desktopSpecialsWrap').length > 0) {
			caseys.createDesktopSpecials(width);
		}

		if ($('#timeline').length > 0 && $('#homeAbout').css('display') != 'none') {
			if (caseys.initializedTimeline != true) {
				caseys.initializeBrandTimeline();
			}
			if (caseys.slideHeadersCreated != true) {
				caseys.createTimelineHeaders();
			}
		}
		if ($('#specialtyPizza').length > 0 ) {
			$('#specialtyPizza .menuItems li').css({
				'height': 'auto'
			});
			caseys.equalHeight('#specialtyPizza .menuItems li');
		}

		if ($('.peopleGrid').length > 0 ) {
			$('.peopleGrid li').css({
				'height': 'auto'
			});
			caseys.equalHeight('.peopleGrid li');
		}

		caseys.positionDropdownMenus(width);
	},
	// need: id for div, container id, li selector, selector for content container
	menuDetailsToggler: function($this, itemsSelector, contentSelector) {
		var $parent = $this.parent();
		var $container = $parent.parent();
		if ($container.find('.activeNutrition').length > 0) {
			$container.find('.activeNutrition').slideUp(400, function() {
				$(this).remove();
				$.fragments = {};
				caseys.menuDetailsOpen($this, itemsSelector, contentSelector);
			});
		} else {
			caseys.menuDetailsOpen($this, itemsSelector, contentSelector);
		}
		$parent = null;
		$container = null;
	},
	menuDetailsClose : function($this) {
		$active = $this.parents('.activeNutrition');
		$container = $this.parents('.menuItemList');
		$items = $container.find('.menuItems > li');
		if ($active.length > 0) {
			$active.slideUp(400, function() {
				$($items).removeClass('open');
				$(this).remove();
			});
		}
		$active = null;
		$container = null;
		$this = null;
	},
	menuDetailsOpen : function($this, itemsSelector, contentSelector) {
		var $parent = $this.parent();
		var $container = $parent.parent();
		var $outerContainer = $container.parent();
		var containerPosition = $container.offset();
		var position = $parent.offset();
		var $theListItems = $outerContainer.find(itemsSelector);
		var theContent = $parent.find(contentSelector).html();
		if (!$parent.hasClass('open')) {
			if ($parent.index() != $theListItems.length - 1) {
				for (var i = $parent.index() + 1; i<$theListItems.length; i++ ) {
					var $theElem = $($theListItems[i]);
					var elemPosition = $theElem.offset();
					if (elemPosition.left - containerPosition.left == 0) {
						$theElem.before('<li class="activeNutrition" style="display: none;"><article>' + theContent + '</article></li>');
						$container.find('.activeNutrition').slideDown();
						$theListItems.removeClass('open');
						$parent.addClass('open');
						break;
					} else if(i == $theListItems.length -1) {
						$theElem.after('<li class="activeNutrition" style="display: none;"><article>' + theContent + '</article></li>');
						$container.find('.activeNutrition').slideDown();
						$theListItems.removeClass('open');
						$parent.addClass('open');
					}
				}
			} else {
				$parent.after('<li class="activeNutrition" style="display: none;"><article>' + theContent + '</article></li>');
				$container.find('.activeNutrition').slideDown();
				$theListItems.removeClass('open');
				$parent.addClass('open');
			}
		} else {
			$parent.removeClass('open');
		}
		$this = null;
		$theElem = null;
		$parent = null;
		$container = null;
		$outerContainer = null;
		containerPosition = null;
		position = null;
		$theListItems = null;
		theContent = null;
	},
	width : null,
	windowWidth: null,
	windowHeight: null,
	updateWidth : function() {
		caseys.width = Math.max( $(window).innerWidth(), (window.innerWidth || document.documentElement.clientWidth));
	},
	equalHeight : function(selector) {
		var max = 0;

		$(selector).each(function() {
		  max = Math.max( max, $(this).height() );
		});

		$(selector).css({
			'height' : max
		});
	},
	toggleTableRow : function(togglerSelector, contentSelector) {
		// toggler should be child of tr preceeding content tr
		$(togglerSelector).on('click', function(event) {
			event.preventDefault();
			$this = $(this);
			var $container = $this.closest('tr');
			var $parentContainer = $this.closest('.priceTable');
			var $content = $container.next(contentSelector).find('.nutritionInfo');
			var $contentSiblings = $parentContainer.find('tr');
			var $siblingContent = $contentSiblings.find('.nutritionInfo');
			if (!$container.hasClass('open')) {
				$contentSiblings.each(function() {
					$(this).removeClass('open');
				});
				$siblingContent.each(function() {
					$(this).slideUp();
				});
				$container.addClass('open');
				$content.slideDown();
			} else {
				$contentSiblings.each(function() {
					$(this).removeClass('open');
				});
				$siblingContent.each(function() {
					$(this).slideUp();
				});
			}

			$container = null;
			$parentContainer = null;
			$content = null;
			$contentSiblings = null;
			$siblingContent = null;
		});

	},
	tableRowClose : function($this) {
		$this.closest('.nutritionInfo').slideUp();
		$this.closest('.nutritionInfo').closest('.nutritionTable').prev().removeClass('open');
	},
	processContentImages :function(container) {
		var theImages = $(container).find('img');
		// setting margins on content images dummy
		var thePImages = $(container).find('p > img:only-child');
		thePImages.each(function(index, element) {
			$(this).unwrap();
		});

		theImages.each(function(index, element) {
			var $this = $(this);
			if($this.css('float') == 'left') {
				$this.addClass('alignLeft');
			} else if ($this.css('float') == 'right') {
				$this.addClass('alignRight');
			} else {
				$this.addClass('alignCenter');
			}
		});
	},
	scrollToId : function(element) {
		var $target = $(element);
		var targetLoc = $target.offset().top;
		var headerHeight = $('#headerContainer').height();
		var scrollDistance;
		if (caseys.width < 650) {
			scrollDistance = targetLoc;
		} else {
			scrollDistance = targetLoc - headerHeight -20;
		}

		$('html, body').animate({
			scrollTop: scrollDistance
		});
	},
	jumpNavPower : function(selector) {
		$(selector).on('click', function(event) {
			event.preventDefault();
			var targetIdUrl = $(this).attr('href');
			if (targetIdUrl.indexOf("#") >= 0) {
				var targetId = targetIdUrl.substring(targetIdUrl.lastIndexOf("#"));
				caseys.scrollToId(targetId);
			} else {
				window.location.href = targetIdUrl;
			}
		});
	},
	columnsForStupidBrowsers : function() {
		$('#footerNav .primaryNavList').each(function(i) {
			// write function here
			var $this = $(this);
			var theListItems = $this.find('> li');
			var theLength = $this.find('li').length;
			var columnOne = '';
			var columnTwo = '';
			var index = 0;
			theListItems.each(function(i) {
				if (index < theLength/2 - 1) {
					columnOne += '<li>' + $(this).html(); + '</li>';
				} else {
					columnTwo += '<li>' + $(this).html(); + '</li>';
				}
				var numKids = $(this).find('li').length;
				index += 1 + numKids;
			});

			$this.after('<div class="ieColumns"><ul class="columnOne">'+ columnOne +'</ul><ul class="columnTwo">'+columnTwo+'</ul></div>');
			//$
		});
	}
};

var site = {
	sizes: {
		mobile:function(){ var m = Math.max( $(window).innerWidth(), (window.innerWidth || document.documentElement.clientWidth)) < 650 ? true : false; return(m);},
		tablet:function(){ var m = Math.max( $(window).innerWidth(), (window.innerWidth || document.documentElement.clientWidth)) < 840 && Math.max( $(window).innerWidth(), (window.innerWidth || document.documentElement.clientWidth)) > 649 ? true : false; return(m);},
		aboveTablet:function(){ var m = Math.max( $(window).innerWidth(), (window.innerWidth || document.documentElement.clientWidth)) > 839 ? true : false; return(m);}
	},
	els: {
		resultList:null,
		body:null,
		scroller:null,
		api:null
	},
	resizeMap: function(height) {
		//gwaitForFinalEvent(function(){
			//if(fbAPI.initialized) { FB.Canvas.setSize({height:830}); }
			if(site.sizes.aboveTablet()){
			var theMap= $('#map');
			var theMapOffset = theMap.offset();
			var windowHeight = parseInt($(window).height());
			if(height){
				if(windowHeight > height) {height = windowHeight;}
			}else{
				height = windowHeight;
			}
			var num = parseInt(height-theMapOffset.top);
			//if(num < 400){num = 400}
			theMap.height(num);
		}//}, 100, "resized");
	},
	resizeResults: function() {
		waitForFinalEvent(function(){
			if(site.sizes.aboveTablet()){
				var p = $('#innerResults'); var r = $('#results'); var op = p.offset(); var or = r.offset();
				var tp = op.top;var tr = or.top;
				var diff = tp-tr; var h = r.height(); var v = h-diff-58;
				p.css('height', v);}
			}, 200, "resizeResults");
	},
	oldBrowser: function() {
		site.resizeResults();
		$(window).resize(function(){site.resizeResults();});
	},
	spinopts: {
		lines: 7, length: 8, width: 3, radius: 5, corners: 1, rotate: 0, color: '#860d1f',
		speed: 1, trail: 60, shadow: false, hwaccel: false, className: 'spinner', zIndex: 2e9,
		top: 'auto', left: 'auto'
	},

	// mapping related:
	initialMapLoad: true,
	currentPage: 0,
	skipResults: false,
	reinitPagination: true,
	rpp: 10,
	showFirstResult: false,
	resetResults:function(){
		site.els.resultList.html('');
		if(!site.els.resultList.find('.spinner').length){
			site.els.resultList.append('<li class="spin"/>');
			var spinner = $('<div class="spinner"/>').spin(site.spinopts);
			site.els.resultList.find('.spin').append(spinner);
		}
	},

	addPagination: function(index, totalCount) {
		site.reinitPagination = false;
		if(totalCount > site.rpp) {
			// setup paging here...
			$("#bottomPagination").pagination(totalCount, {
				items_per_page: site.rpp,
				callback: site.handlePageClick,
				current_page: index,
				num_display_entries: 5,
				next_text: ">",
				prev_text: "<",
				next_show_always: false,
				prev_show_always: false
			});
			$("#bottomPagination").show();
		} else {
			$("#bottomPagination").hide();
			site.handlePageClick(0);
		}
	},
	addResult: function(results, loc, addClass) {
		loc.delivery = false;
		loc.giftcard = false;
		loc.coupons = false;
		loc.pickup = true;

		if(loc.badges) {
			for(var i=0; i<loc.badges.length; i++) {
				var badge = loc.badges[i];
				switch(badge.badge_name) {
					case "delivery_available":
						loc.delivery = true;
						break;
					case "accepts_giftcards":
						loc.giftcard = true;
						break;
					case "accepts_coupons":
						loc.coupons = true;
						break;
				}
			}
		}

		var results = $(results);
			if(results.length) {
				if(results.get(0).tagName ===  'OL'){
				var result = $("<li/>");
			}else{var result = $('<div/>');}
				result.attr("data-id", loc.id).attr("class", "project");
			}

			if(addClass) {
				result.addClass("result"+addClass);
				result.append($("<div />").attr("class", "marker noselect aboveTabletOnly").html(addClass));
			}

			// create the result wrapper:
			var wrapper = $("<div />").attr("class", "placeWrapper");

			wrapper.append($("<h3 />").html(loc.title));
			var distance = loc.distance * 0.000621371;
			wrapper.append($("<span />").attr("class","distance").html(distance.toFixed(1) + " mi"));
			var addressText = loc.address;
			var unopened = ((loc.not_opened == 1)? 'opening soon' : '');
			var onlineOrdering = ((loc.online_ordering == 1 && ooEnabled == true)? '<a href="' + ooUrl + '" target="_blank" class="orderOnline" onClick="_gaq.push([\'_trackEvent\', \'Programs\', \'Online Ordering\', \'Locations Page\']);">ORDER ONLINE</a>' : '');

			wrapper.append($("<div />").attr("class","address").append($('<span class="openingSoon" />').text(unopened)).append($("<span />").html(addressText)).append($("<span />").html(loc.city + ", " + loc.state + " " + loc.zipcode)).append('<a href="tel:' + loc.phone + '">' + loc.phone + '</a>' + onlineOrdering));


			var myId = caseys.getCookie('myCaseysId');
			var isMyCaseys = false;
			if (loc.id == myId) {
				isMyCaseys = true;
			}
			var caseysClass = isMyCaseys ? '' : ' notMyCaseys';

			var myCaseys = '<span class="myCaseys '+ caseysClass +'">My Casey\'s</span><a href="#" class="myCaseysLink '+caseysClass+'" data-id="' + loc.id  + '" data-address="' + addressText + ' ' + loc.city + ',' + loc.state + ' ' + loc.zipcode + '" data-phone="'+loc.phone+'">Make this my Casey\'s</a>';
			wrapper.append(myCaseys);

			var userInput = $('#locator').val().toUpperCase();

		///// get values from locator_lookup web service

			var directionsLink = "//maps.google.com/maps?saddr="+userInput;
			directionsLink += "&daddr="+addressText+" "+loc.city+" "+loc.state+" "+loc.zipcode;

			wrapper.append($("<a />").attr("href",directionsLink).attr("target","_blank").attr('class', 'smArrow arrowButton').html("Directions"));

			wrapper.append($("<div />").attr("class", "hours").append($("<h4 />").html('Store Hours:')).append($('<span />').html(loc.hours)));

			// create the result footer:
			var footer = $("<footer />").attr("class", "placeFooter clearfix noselect");
			var footHead = $('<h3 />').html('Products &amp; Services');
			var dieselSpan = $("<span />").attr("class", "diesel").html("Diesel");
			var subsSpan = $("<span />").attr("class", "subs").html("Subs");
			var pizzaSpan = $("<span />").attr("class", "pizza").html("Pizza");
			var hourSpan = $("<span />").attr("class", "hour").html("24 Hour");
			var carwashSpan = $("<span />").attr("class", "carwash").html("Car Wash");
			var deliverySpan = $("<span />").attr("class", "delivery").html("Delivery");
			var bakerySpan = $("<span />").attr("class", "bakery").html("Donuts");
			var softServeSpan = $("<span />").attr("class", "softServe").html("Soft Serve*");
			var fuelSaverSpan = $("<span />").attr("class", "fuelSaver").html("Fuel Saver");

			loc.diesel == '1' ? dieselSpan.addClass("true") : dieselSpan.addClass("false");
			loc.subs == '1' ? subsSpan.addClass("true") : subsSpan.addClass("false");
			loc.pizza == '1' ? pizzaSpan.addClass("true") : pizzaSpan.addClass("false");
			loc.allday == '1' ? hourSpan.addClass("true") : hourSpan.addClass("false");
			loc.car_wash == '1' ? carwashSpan.addClass("true") : carwashSpan.addClass("false");
			loc.pizza_delivery == '1' ? deliverySpan.addClass("true") : deliverySpan.addClass("false");
			loc.bakery == '1' ? bakerySpan.addClass("true") : bakerySpan.addClass("false");
			loc.soft_serve == '1' ? softServeSpan.addClass("true") : softServeSpan.addClass("false");
			loc.fuel_saver == '1' ? fuelSaverSpan.addClass("true") : fuelSaverSpan.addClass("false");

			footer.append(footHead).append(dieselSpan).append(subsSpan).append(pizzaSpan).append(hourSpan).append(carwashSpan).append(deliverySpan).append(bakerySpan).append(softServeSpan).append(fuelSaverSpan).append('<small style="clear: both; display: block; font-style: italic; font-size: 10px; text-align: center;">*Seasonal Item</small>');
			result.append(wrapper).append(footer);

			// append newly created result to the result listing:
			results.append(result);
	},
	handlePageClick: function(index, element) {
		this.currentPage = index;
		site.currentPage = index;

		if(map.projects.length > 0) {
			map.plotProjects(site.currentPage * site.rpp, site.rpp, true);
		}

		$("body, #innerResults").scrollTop(0);
		site.populatePage();
		return false;
	},
	onMapEvent: function(event, data) {
		switch(event) {
			case "idle":
			/*
				if(map.ready && !map.infoWindow && !site.initialMapLoad) {
					var latLng = map.gmap.getCenter();
					map.initGMap({"coords":{"latitude":latLng.lat(),"longitude":latLng.lng()}});
				}
				*/
				break;
			case "ready":
				if(map.ready && !map.infoWindow) {

					site.initialMapLoad = false;
					site.addPagination(0, (Math.min(300,map.projects.length))); //Math.min(map.getProjectIndexesForBounds(3000).length, map.projects.length));
					if(site.showFirstResult) {
						site.showFirstResult = false;
						$(".project.resultA").click();
					}
				}
				break;
		}
	},
	populatePage: function() {
		$('#resultList').html('');
		var letters = ['A','B','C','D','E','F','G','H','I','J'];
		for(i=0; i<site.rpp; i++) {
			var loc = map.projects[map.projectIds[i+(site.currentPage*site.rpp)]];
			if(loc) {
				site.addResult("#resultList", loc, letters[i]);
			}
		}
		// adding a call to add the scroll bar
		if ($.browser.msie && $.browser.version == '8.0') {
			// sorry, you don't get fancy scrollbars
		} else {
			site.controlScroller();
		}
	},
	controlScroller : function() {
		if (site.sizes.aboveTablet() && site.els.scroller == null) {
			site.els.scroller = $('#innerResults').jScrollPane();
			site.els.api = site.els.scroller.data('jsp');
		} else if (site.sizes.aboveTablet()) {
			site.els.api.reinitialise();
		}
	},
	resizeScroller : function() {
		if (site.els.scroller != null && !site.sizes.aboveTablet()) {
			site.els.api.destroy();
			site.els.scroller = null;
		} else if(site.sizes.aboveTablet() && site.els.scroller == null) {
			site.els.scroller = $('#innerResults').jScrollPane();
			site.els.api = site.els.scroller.data('jsp');
		} else if (site.els.scroller != null && site.sizes.aboveTablet()) {
			site.els.api.reinitialise();
		}
	},
	prependMenu: function(){
		$('#primaryNav ul.primaryNavList').prepend('<li><a href="' + ooUrl + '" class="hiVis" target="_blank"><span>Order Online</span></a></li>');
		$('#mobileNav ul.primaryNavList').prepend('<li><a href="' + ooUrl + '" class="hiVis" target="_blank"><span>Order Online</span></a></li>');
		$('body.page-node-5 #myCaseys').addClass('ooEnabled');
		$('body.page-node-5 #myCaseys a.myCaseysPhone').addClass('ooEnabled');
		$('body.page-node-5 #myCaseys a.myCaseysPhone').before('<a href="' + ooUrl + '" id="myCaseysOO" class="myCaseysOO ooEnabled" target="_blank">Order Online</a>');
	},
	getOnOrd: function(prependMenu){


		// first check whether the user has cookie set
		
		var onOrdEnbl = caseys.getCookie('onOrd');

		if (onOrdEnbl){
			if (prependMenu){
				site.prependMenu();
			}
			return true;
		}
		
		// next check whether the user has a "My Casey's" Set

		var myCaseysId = caseys.getCookie('myCaseysId');

		if(myCaseysId){

			// myCaseysID is set, but onlOrd is not

			var s = site.getOnOrdStatus({storeId: myCaseysId});

			if (s == 1){
				if (prependMenu){
					site.prependMenu();
				}
			}

//			return site.getOnOrdStatus({storeId: myCaseysId});

			return s;

		}
		
		
		// If the user has not selected "My Casey's" Store, use browser location

		if (navigator.geolocation) {


			var ors = false;
			var lat = caseys.getCookie('caseysLat');
			var lon = caseys.getCookie('caseysLon');
			
			if (lat && lon){
				ors = site.getOnOrdStatus({lat: lat, lon: lon, prependMenu: prependMenu});
			}else{
				navigator.geolocation.getCurrentPosition(function(pos){
				
					caseys.setCookie('caseysLat',pos.coords.latitude,1);
					caseys.setCookie('caseysLon',pos.coords.longitude,1);
					ors = site.getOnOrdStatus({lat: pos.coords.latitude, lon: pos.coords.longitude, prependMenu: prependMenu});
				});

			}

			if (ors == 1 && prependMenu){
				site.prependMenu();
			}	
	
			return ors;
					
		}else{
			// last resort -- get location by IP address



			var s = site.getOnOrdStatus({getByIP: true, prependMenu: prependMenu});	
	
			if (s == 1 && prependMenu){
				site.prependMenu();
			}

//			return site.getOnOrdStatus({getByIP: true});		

			return s;

		}

	},
	getOnOrdStatus: function(data){
		var onlOrd = false;
		$.ajax('http://www.caseys.com/getOnOrdStatus.php',
			{	type: 'GET',
				data: data,
				success: function(d){
					if (d == true){


						if (data.prependMenu){
							site.prependMenu();
							$('.hiVis').show();
						}
		
						caseys.setCookie('onOrd',true,1);
						onlOrd = true;
					}
				}
			}
		);
		return onlOrd;
	}

};

var map = {
	initialInterval: null,

	skipBroadcast: false,
	gmap: null,
	geocoder: null,
	origin: null,
	projects: [],
	masterProjects: [],
	projectIds: [],
	subscribers: [],
	infoWindow: null,
	maxZIndex: 0,
	timeout: null,
	lastBounds: null,
	center: null,
	ready: false,
	postMiniMap: function() {
		$('#miniLocationsForm').submit();
	},
	addSubscriber: function(obj) {
		this.subscribers.push(obj);
	},
	broadcast: function(event, data) {
		for(i=0; i<this.subscribers.length; i++) {
			this.subscribers[i].onMapEvent(event, data);
		}
	},
	clearInfoWindow: function() {
		if(map.infoWindow) {
			map.infoWindow.setMap(null);
			map.infoWindow = null;
		}
		map.resetMarkerStates();
	},
	clearMarkers: function() {
		map.clearInfoWindow();
		for(i=0; i<this.projects.length; i++) {
			if(this.projects[i].marker) {
				this.projects[i].marker.setMap(null);
				this.projects[i].marker = null;
			}
		}
	},
	fitBounds: function() {
		if(map.gmap) {
			var mapBounds = new google.maps.LatLngBounds();
			for(var i=0; i<Math.min(10, map.projects.length); i++) {
				var project = map.projects[i];
				mapBounds.extend(new google.maps.LatLng(project.latitude, project.longitude));
			}

			map.skipBroadcast = true;
			map.gmap.fitBounds(mapBounds);
			map.skipBroadcast = false;
		}
	},
	getProjectIndexesForBounds: function(max) {
		if(!map.gmap || !map.gmap.getBounds()) {
			return;
		}

		var bounds = map.gmap.getBounds();
		var found = [];

		map.lastBounds = bounds;
		for(i=0; i<map.projects.length; i++) {
			var project = map.projects[i];
			if(bounds.contains(new google.maps.LatLng(project.latitude, project.longitude))) {
				found.push(i);
				if(found.length == max) {
					break;
				}
			}
		}
		return found;
	},
	initGMap: function(position, zoomIn, filters) {
		if(map.initializing) {
			return false; }
		map.initializing = true;
		var zoom = 7;

		// no position passed in, or geolocation was declined:
		if(!position || position.code == 1 || position.code == 2) {
			// default to a position:
			position = {"coords":{"latitude":"41.6937110","longitude":"-93.5731700"}};
			zoom = 7;
		}
		map.origin = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

		if(map.gmap) {
			map.clearInfoWindow();

			map.sortProjects(filters);
			map.gmap.setCenter(map.origin);
			if(zoomIn)
				map.gmap.setZoom(11);
// 	hmmm....removing this seems to fix the map issue.
			map.initializing = false;
			map.broadcast('ready');
		} else {
			map.geocoder = new google.maps.Geocoder();
			map.gmap = new google.maps.Map(document.getElementById("map"), {
				center: map.origin,
				zoom: zoom,
				minZoom: 7,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				disableDefaultUI: true,
				zoomControl:true,
				zoomControlOptions:{position:google.maps.ControlPosition.RIGHT_TOP}
			});

			// callback on the following events:
			// dragend, zoom_changed (idle will handle both?)
			// this will be used to trigger a re-display of projects/points:
			google.maps.event.addListener(map.gmap, 'dragend', function(event) {
				clearTimeout(map.timeout);
				if(map.infoWindow) {
					// if there's an infowindow but out of the viewport, close it:
					if(!map.gmap.getBounds().contains(map.infoWindow.latlng_)) {
						map.clearInfoWindow();
					}
				}
				map.timeout = setTimeout(function() {
					map.broadcast('idle', event);
				}, 1000);
			});
			google.maps.event.addListener(map.gmap, 'zoom_changed', function(event) {
				clearTimeout(map.timeout);

				// clear any info window:
				map.clearInfoWindow();

				if(!map.skipBroadcast) {
					map.broadcast('zoom_changed', event);

					map.timeout = setTimeout(function() {
						map.broadcast('idle', event);
					}, 1000);
				}
			});

			//current location marker
			/*var marker = new google.maps.Marker({
				position:map.origin,
				map:map.gmap,
				title:'You are here!'
			});*/

			var styles = [
			  {
				stylers: [
				  //{ hue: "#006400" }
				]
			  },{
					  "featureType": "road",
					  "stylers": [
						{ "hue": "#006400" },
						{ "saturation": -90 },
				  { "lightness": 10 }
				]
				},{
				featureType: "road",
				elementType: "labels"
			  }
			];

			map.gmap.setOptions({styles: styles});

			google.maps.event.addListener(map.gmap, 'dragstart', function(event) {
				clearTimeout(map.timeout);
				map.broadcast('dragstart', event);
			});

			google.maps.event.addListener(map.gmap, 'tilesloaded', function(event) {
				map.broadcast('tilesloaded', event);
			});
			$.ajax({
				url: "http://www.caseys.com/storefeed.php",
				type: "GET",
				cache: true
			}).done(function(data) {
				map.masterProjects = data;
				map.projects = data;
				map.sortProjects(filters);
				//map.fitBounds();
				map.initialInterval = setInterval(function() {
					if(map.gmap && map.gmap.getBounds()) {
						map.initializing = false;

						clearInterval(map.initialInterval);

						map.ready = true;
						// call back map loaded (so can search for projects provided the lat/lng)
						map.broadcast('ready');
					}
				}, 1000);
			});
		}

		return true;
	},
	plotProjects: function(specialIndex, specialCount, ignoreBounds) {
		map.clearMarkers();

		// show a max of 1000
		var boundProjectIndexs = [];
		var flag = 300;
			flag = Math.min(flag, map.projects.length);

		var xSprite = [0, 40, 80, 120, 160, 200, 240, 280, 320, 360];
		// plot all projects
		// first specialCount have "special" pin markers, others have smaller dots
		for(i=0; i<flag; i++) {
			var proj = null;
			var ix = i;
			if(!ignoreBounds && boundProjectIndexs) {
				ix = boundProjectIndexs[i];
			} else {
				map.projectIds.push(i);
			}
			proj =  map.projects[ix];

			if(map.gmap) {
				var image = new google.maps.MarkerImage('../img/spriteDesktop.png'/*tpa=http://www.caseys.com/sites/all/themes/caseys/img/spriteDesktop.png*/);
				var zI = 1;
				if(specialIndex != undefined && specialCount != undefined && i >= specialIndex && i < specialIndex+specialCount) {
					// should display a "special" marker:
					image.size = new google.maps.Size(40,55);
					image.origin = new google.maps.Point(xSprite[i%10], 218);
					image.anchor = new google.maps.Point(20, 51);
					zI = 2;
				} else {
					image.size = new google.maps.Size(18,13);
					image.origin = new google.maps.Point(0,200);
					image.anchor = new google.maps.Point(9,6);
					zI = 1;
				}

				var marker = new google.maps.Marker({
					position: new google.maps.LatLng(proj.lat, proj.lon),
					map: map.gmap,
					project: proj,
					icon: image,
					zIndex: zI
				});

				google.maps.event.addListener(marker, "click", function(e) {
					map.resetMarkerStates();
					// reset timeout so we don't re-search right after an info window is opened:
					clearTimeout(map.timeout);

					// close any open info window:
					var shouldOpen = (!map.infoWindow || (map.infoWindow && map.infoWindow.project_.id != this.project.id));
					map.clearInfoWindow();

					if(shouldOpen) {
						var projectInfoBox = new ProjectInfoWindow({project: this.project, latlng: this.getPosition(), map: map.gmap});
						map.infoWindow = projectInfoBox;
						this.zIndex = 3;
						map.broadcast('infowindow', e);
					} else {
						if(this.getIcon().size.width == 67) {
							this.zIndex = 2;
						} else {
							this.zIndex = 1;
						}
					}
					this.setIcon(this.getIcon());
				});
				map.projects[ix].marker = marker;
			}
		}
	},
	resetMap: function() {
		map.clearMarkers();
		map.projectIds = [];
	},
	resetMarkerStates: function() {
		for(i=0; i<this.projects.length; i++) {
			var marker = this.projects[i].marker;
			if(marker) { // && marker.getIcon().origin.y != 213) {
				//marker.getIcon().origin.y = 213;
				if(marker.getIcon().size.width == 67) {
					marker.zIndex = 2;
				} else {
					marker.zIndex = 1;
				}
				marker.zIndex = 1;
				//marker.setIcon(marker.getIcon());
			}
		}
	},
	showMarker: function(id) {
		for(i=0; i<this.projects.length; i++) {
			var project = this.projects[i];
			if(project.id == id && project.marker) {
				google.maps.event.trigger(project.marker, 'click');
				break;
			}
		}
	},
	applyFilters: function(filters) {
		// determine all filters set to "on"
		// set projects to an empty array
		map.resetMap();

		map.projects = [];
		// iterate through all masterProjects and add those matching all filters to the projects array
		for(i=0; i<map.masterProjects.length; i++) {
			var project = map.masterProjects[i];
			var addToList = true;
			for(j=0; j<filters.length; j++) {
				if(project[filters[j].name] == 0 || project[filters[j].name] == null) {
					addToList = false;
				}
			}
			if(addToList) {
				map.projects.push(project);
			}
		}
	},
	sortProjects: function(filters) {
		site.resetResults();
		if (filters != null) {
			map.applyFilters(filters);
		}
		map.projects.sort(function(a, b) {
			var aLatLng = new google.maps.LatLng(a.lat, a.lon);
			var bLatLng = new google.maps.LatLng(b.lat, b.lon);
			var aDistance, bDistance;

			aDistance = google.maps.geometry.spherical.computeDistanceBetween(aLatLng, map.origin);
			a.distance = aDistance;

			bDistance = google.maps.geometry.spherical.computeDistanceBetween(bLatLng, map.origin);
			b.distance = bDistance;

			return aDistance - bDistance;
		});
	},
	submitForm: function () {
		if(map.initializing) {
			return false;
		} else {
			$('#locate').submit();
			return true;
		}
	}
};



$(function() {
	MBP.scaleFix();
	MBP.hideUrlBarOnLoad();

	$('#miniLocationsForm .desktopSpecialsLink').on('click',function(){
		map.postMiniMap();
	});

	$('#mapContent').on('click','.myCaseysLink', function(){
		caseys.setCookie('myCaseysAddress',$(this).attr('data-address'),365);
		caseys.setCookie('myCaseysPhone',$(this).attr('data-phone'),365);
		caseys.setCookie('myCaseysId',$(this).attr('data-id'),365);
		$('#resultList span.myCaseys, #resultList a.myCaseysLink').addClass('notMyCaseys');
		$(this).removeClass('notMyCaseys').parent().find('.myCaseys').removeClass('notMyCaseys');
	});

	$('.jsHide').hide();

	$('a.formSubmit').on('click', function(event) {
		event.preventDefault();
		$(this).closest('form').submit();
	});

	$('#mobileNavToggler').on('click', function(event) {
		event.preventDefault();
		caseys.toggleMobileNav();
	});

	if($('.jumpNav').length > 0) {
		caseys.jumpNavPower('.jumpNav a');
	}

	if($('.node-type-careers-info-page .subSectionNav .secondary').length > 0) {
		caseys.jumpNavPower('.subSectionNav .secondary a');
	}

	// get window height and width for resize testing
	caseys.windowHeight = $(window).height();
	caseys.windowWidth = $(window).width();

	// check the width in a way that matches media queries
	caseys.updateWidth();

	if(caseys.width < 650) {
		caseys.resizePhone(caseys.width);
	} else {
		caseys.resizeDesktop(caseys.width);
	}

	$(window).resize(function() {

		if($(window).height() != caseys.windowHeight || $(window).width() != caseys.windowWidth){
			caseys.windowHeight = $(window).height();
			caseys.windowWidth = $(window).width();
			caseys.updateWidth();
			if(caseys.width < 650) {
				caseys.resizePhone(caseys.width);
			} else {
				caseys.resizeDesktop(caseys.width);
			}
		}
	});
	if ($('.communityContent').length > 0 ) {
		caseys.processContentImages('.communityContent');
	}
	if ($('.corporateMainContent').length > 0 ) {
		caseys.processContentImages('.corporateMainContent');
	}
	if ($('.aboutContent').length > 0 ) {
		caseys.processContentImages('.aboutContent');
	}
	if ($('.careersMainContent').length > 0 ) {
		caseys.processContentImages('.careersMainContent');
	}

	caseys.toggleThings('.navSection', '.navSectionToggler', '.primaryNavList');
	if ($('#locationFeatureOptions').length > 0) {
		caseys.toggleThings('#locate', '#toggleLocationOptions', '#locationFeatureOptions');
	}
	if ($('#moreOptionsOpener').length > 0) {
		caseys.toggleThings('#locatorForm', '.moreOptionsToggler', '#moreOptions');
	}

	if ($('#careersFAQ').length > 0) {
		caseys.toggleThings('#careersFAQ div', '.faqToggler', '.faqAnswer');
	}

	if ($('.positionDescriptions').length > 0) {
		caseys.toggleThings('.positionDescriptions > div', '.posDescToggler', '.positionDescriptionContent');
	}

	if ($('.multiSelect').length > 0) {
		caseys.toggleThings('.multiSelect', '.multiSelectToggler', '.multiSelectContent');
		$('.contentForm input, .contentForm select, .contentForm a').on('focus', function() {
			if($(document.activeElement).parents('.multiSelect').length < 1) {
				$('.multiSelect.open .multiSelectToggler').click();
			}
		});
		$('.multiSelect').on('change', '.multiSelectContent input[type="checkbox"]', function() {
			var container = $(this).parents('.multiSelect');
			var n = container.find( "input:checked" ).length;


			if (n > 0) {
				container.find('.multiSelectToggler').text( n + " selected" );
				if ($('#locationsTerms').val().length > 0){
					$('#jobSearchButton').removeClass('disabledArrow');
				}else{
					$('#jobSearchButton').addClass('disabledArrow');
				}
			} else {

				$multiSelectToggler = container.find('.multiSelectToggler');
				// if($multiSelectToggler.attr('data-default') !== '') {
					// $multiSelectToggler.text($multiSelectToggler.attr('data-default'));
				// } else {
				  $multiSelectToggler.text('Select Position(s)');

				$('#jobSearchButton').addClass('disabledArrow');
				// }
			}
		});
	}

	if ($('#breadcrumb a:last').text() == $('#breadcrumb span:last').text()) {
		$('#breadcrumb a:last').prev('span').remove();
		$('#breadcrumb a:last').remove();
		$('#breadcrumb span:last').prev('.breadSep').remove();
	}

	if ($('.menuSection').length > 0) {
		caseys.toggleExistingThings('.menuSection', '.menuSectionToggler', '.menuSectionContent');
		caseys.toggleThings('.activeNutrition', '.ingredientsToggler', '.ingredientsContent');
		caseys.toggleThings('.nutritionInfo', '.ingredientsToggler', '.ingredientsContent');
		caseys.toggleTableRow('.nutritionTableToggler', '.nutritionTable');

		$('.nutritionInfoToggler').on('click', function(event) {
			event.preventDefault();
			if (caseys.width > 649) {
				$this = $(this);
				caseys.menuDetailsToggler($this, '.menuItems > li', '.nutritionInfo');
			}
		});

		$('.menuItemList').on('click', '.closeNutrition' , function(event) {
			event.preventDefault();
			var $this = $(this);
			caseys.menuDetailsClose($this);
		});
		$('.closeNutritionTable').on('click', function(event) {
			event.preventDefault();
			var $this = $(this);
			caseys.tableRowClose($this);
		});
		$('#specialtyPizza').on('click', '.menuSectionToggler' , function(event) {
			event.preventDefault();
			if (caseys.width > 649) {
				$('#specialtyPizza .menuItems li').css({
					'height': 'auto'
				});
				setTimeout('caseys.equalHeight(\'#specialtyPizza .menuItems li\')', 400);
			} else {
				$('#specialtyPizza .menuItems li').css({
					'height': 'auto'
				});
			}
		});
	}

	if ($('.subSectionNav .secondary a.active').length > 0) {
		var $activeLink = $('.subSectionNav .secondary a.active').first().closest('.secondary');
		$activeLink.css({'display' : 'block'});
		$activeLink.prev('a').addClass('active');
	}

	if($('.subSectionNav').length > 0) {
		//caseys.sticky_navigation_offset_top = $('.subSectionNav').offset().top;
		// hard coding this because it was getting set too early in ie8, before page fully loaded
		caseys.sticky_navigation_offset_top = 280;
		caseys.stickyNavigation();
		$(window).scroll(function() {
			caseys.stickyNavigation();
		});
	}

	if ($.browser.msie && $.browser.version < 10) {
		$('body').addClass('upidStayOwserBray');
		caseys.columnsForStupidBrowsers();
	}

	if ($('#map').length > 0) {
		site.resizeMap();
		if(!Modernizr.opacity){
			site.oldBrowser();
		}
		if(!Modernizr.csscalc){
			site.resizeResults();
			$(window).resize(function(){
				site.resizeResults();
			});
		}
		$(window).resize(function(){
			site.resizeMap();
			// mess with the scroller
			if ($.browser.msie && $.browser.version == '8.0') {
				// sorry, you don't get fancy scrollbars
			} else {
				site.resizeScroller();
			}
		});

		var initialFilters = $('#locate').serializeArray();
		var openAdvancedOptions = false;
		for(j=0; j<initialFilters.length; j++) {
			if(initialFilters[j].value == 'on') {
				openAdvancedOptions = true;
			}
		}
		if (openAdvancedOptions) {
			$('#toggleLocationOptions').click();
		}
		map.initGMap(null, null, initialFilters);

		site.els.resultList = 	$('#resultList');
		site.els.body = $('body');


		// display the marker when you click on a project within results:
		$("#results").on("click", ".project", function(e) {
			var a = $(e.target);
			if(!a.attr('href')){
				map.showMarker($(this).data("id"));
			}
		});

		$("body").on('click', '.closeWin', function() {
			map.clearInfoWindow();
			return false;
		});

		$("#locate").on("submit", function() {

			// use geolocation:
			var searchVal = $("#locator").val();
			var searchVal = $('#locator').val().toUpperCase();

			if (caseys.locator_lookup_table[searchVal]){
				searchVal = caseys.locator_lookup_table[searchVal];
			}

			var formFilters = $('#locate').serializeArray();
			if (searchVal == "") {
				var theCoords = null;
				if (map.gmap) {
					var latLng = map.gmap.getCenter();
					theCoords = {"coords":{"latitude":latLng.lat(),"longitude":latLng.lng()}};
				}
				map.initGMap( theCoords, 7, formFilters );
			} else {
				if(map.geocoder && (searchVal != "")) {
					map.geocoder.geocode( {'address':searchVal}, function(results, status) {
						switch(status) {
							case google.maps.GeocoderStatus.OK:
								site.showFirstResult = true;
								map.initGMap( {"coords":{"latitude":results[0].geometry.location.lat(),"longitude":results[0].geometry.location.lng()}}, 7, formFilters );
								break;
							case google.maps.GeocoderStatus.ZERO_RESULTS:
							case google.maps.GeocoderStatus.INVALID_REQUEST:
								alert("No results found. Please check your input and try again!");
								break;
							case google.maps.GeocoderStatus.OVER_QUERY_LIMIT:
							case google.maps.GeocoderStatus.REQUEST_DENIED:
								alert("We've detected too many requests. Please wait a few minutes and try again.");
								break;
							default:
								alert("Search was not successful for the following reason: " + status);
						}
					});
				}
			}
			// set timeout to close the advanced options
			if ($('#locate').hasClass('open')) {
				closeTimeout = setTimeout(function() {
					$('#locate').removeClass('open');
					$('#locationFeatureOptions').slideUp();
				}, 2000);
			}

			return false;
		});

		site.els.body.on('click', '.pw .closeBtn', function(e){
			e.preventDefault();
			map.clearInfoWindow();
		});

		if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
		  var viewportmeta = document.querySelector('meta[name="viewport"]');
		  if (viewportmeta) {
			viewportmeta.content = 'width=device-width, minimum-scale=1.0, maximum-scale=1.0';
			document.body.addEventListener('gesturestart', function() {
			  viewportmeta.content = 'width=device-width, minimum-scale=0.25, maximum-scale=1.6';
			}, false);
		  }
		}
		var searchVal = $("#locator").val();

		if (searchVal.length > 0) {
			var initInterval = setInterval(function() {
				var started = map.submitForm();
				if(started) {
					clearInterval(initInterval);
				}
			}, 500);
		} else {
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(pos){
					var initInterval = setInterval(function() {
						var started = map.initGMap(pos,1);
						if(started) {
							clearInterval(initInterval);
						}
					}, 500);
				});
			}
		}
	}

	$("a").filter(function() {
		return this.hostname && this.hostname.replace(/^www\./, '') !== location.hostname.replace(/^www\./, '');
		}).each(function() {
		   $(this).attr({
			   target: "_blank"
			});
		});
	});

	var waitForFinalEvent = (function () {
		var timers = {};
		return function (callback, ms, uniqueId) {
			if (!uniqueId) {
				uniqueId = "Don't call this twice without a uniqueId";
			}
			if (timers[uniqueId]) {
				clearTimeout (timers[uniqueId]);
			}
			timers[uniqueId] = setTimeout(callback, ms);
		};
	})();

	// tell the map to broadcast events to the site object:
	map.addSubscriber(site);

	var theLand = [];
	var numPerPage = 10;
	var priceRange = '';
	var theVehicles;
	var theLeases;
	var isInitialized = false;

	function handlePaginationClick(index, element) {
		var html = '';
		var listHTML = '';

		for(var i=0;i<numPerPage;i++ ) {
			var thisItem = theLand[index * numPerPage + i];
			if (thisItem) {
				var price = parseInt(thisItem.node.price.replace(/\,/g,''));
				var photo = (thisItem.node.Photo.length > 0) ? 'hasPhoto' : '';

				if (price >= parseInt(priceRange[0]) && price <= parseInt(priceRange[1])){
					html += '<tr><td class="'+photo+'">' + thisItem.node.address + ', ' + thisItem.node.City + ' ' + thisItem.node.State + '</td><td>' + thisItem.node.lot_size + '</td><td>$' + thisItem.node.price +'</td><td><a href="javascript:;" class="viewDetails" data-nodeid="' + thisItem.node.Nid + '">View Details</a></td></tr>';
					listHTML += '<li><p class="landAddress"><strong>Address:</strong> ' + thisItem.node.address  + ', ' + thisItem.node.City + ' ' + thisItem.node.State + '</p><p><strong>Lot Size:</strong><span class="lotSize"> ' + thisItem.node.lot_size + '</span></p><p><strong>Price:</strong> <span class="landPrice"> $' + thisItem.node.price +'</span></p><a href="javascript:;" class="viewDetails" data-nodeid="' + thisItem.node.Nid + '">View Details</a></li>';

				}
			}
		}
		if (html != '') {
			$('#landForSaleResults .forSaleTable tbody').html(html);
		} else {
			$('#landForSaleResults .forSaleTable tbody').html('<tr><td><strong>No Results Found</strong></td></tr>');
		}

		if (listHTML != '') {
			$('#mobLandForSaleResults').html(listHTML);
		} else {
			$('#mobLandForSaleResults').html('<li><strong>No Results Found</strong></li>');
		}
		if (isInitialized == true) {
			$('body,html').animate({ scrollTop: $('.corporateContent').offset().top}, 500);
		}
		return false;
	}

	function getLandForSale(){

		var state = $('#landFilterState').val();
		var price = $('#landFilterPrice').val();
		priceRange = price.split('-');

		var lowPrice = parseInt(priceRange[0]);
		var highPrice = parseInt(priceRange[1]);

		forSaleShowSpinner();

		$.get('/land-feed', {field_state_tid: state}, function(ret){
			theLand = [];
			$.each(ret.nodes,function(key,val){

				if (parseInt(val.node.price.replace(',','')) >= lowPrice && parseInt(val.node.price.replace(',','')) < highPrice){
					theLand.push(val);
				}

			});


			var html = '<table class="forSaleTable"><thead><tr><th>ADDRESS</th><th>LOT SIZE</th><th>PRICE</th><th></th></tr></thead><tbody>';
			html += '</tbody></table>';
			$('#landForSaleResults').empty();
			$('#landForSaleResults').append(html);
			$("#bottomPagination").pagination(theLand.length, {
				items_per_page: numPerPage,
				callback: handlePaginationClick,
				current_page: 0,
				num_display_entries: 5,
				next_text: ">",
				prev_text: "<",
				next_show_always: false,
				prev_show_always: false
			});

			if (theLand.length <= numPerPage) {
				$('#bottomPagination').addClass('hidden');
			} else {
				$('#bottomPagination').removeClass('hidden');
			}


			forSaleHideSpinner();
		});

	}


	function handleLeasePaginationClick(index, element) {
		var html = '';
		var listHTML = '';

		for(var i=0;i<numPerPage;i++ ) {
			var thisItem = theLeases[index * numPerPage + i];
			if (thisItem) {
				var price = parseInt(thisItem.node.price.replace(/\,/g,''));
				var photo = (thisItem.node.Photo.length > 0) ? 'hasPhoto' : '';

				if (price >= parseInt(priceRange[0]) && price <= parseInt(priceRange[1])){
					html += '<tr><td class="'+photo+'">' + thisItem.node.address + ', ' + thisItem.node.City + ' ' + thisItem.node.State + '</td><td>' + thisItem.node.lot_size + '</td><td>$' + thisItem.node.price +'</td><td><a href="javascript:;" class="viewDetails" data-nodeid="' + thisItem.node.Nid + '">View Details</a></td></tr>';
					listHTML += '<li><p class="landAddress"><strong>Address:</strong> ' + thisItem.node.address  + ', ' + thisItem.node.City + ' ' + thisItem.node.State + '</p><p><strong>Lot Size:</strong><span class="lotSize"> ' + thisItem.node.lot_size + '</span></p><p><strong>Price:</strong> <span class="landPrice"> $' + thisItem.node.price +'</span></p><a href="javascript:;" class="viewDetails" data-nodeid="' + thisItem.node.Nid + '">View Details</a></li>';

				}
			}
		}
		if (html != '') {
			$('#landForLeaseResults .forSaleTable tbody').html(html);
		} else {
			$('#landForLeaseResults .forSaleTable tbody').html('<tr><td><strong>No Results Found</strong></td></tr>');
		}

		if (listHTML != '') {
			$('#mobLandForLeaseResults').html(listHTML);
		} else {
			$('#mobLandForLeaseResults').html('<li><strong>No Results Found</strong></li>');
		}
		if (isInitialized == true) {
			$('body,html').animate({ scrollTop: $('.corporateContent').offset().top}, 500);
		}
		return false;
	}

	function getLeaseLocations(){
		var state = $('#landFilterState').val();

		$.get('/lease-feed', {field_state_tid: state}, function(ret){
			theLeases = [];
			$.each(ret.nodes,function(key,val){
				theLeases.push(val);
			});


			var html = '<table class="forLeaseTable"><thead><tr><th>ADDRESS</th><th>LOT SIZE</th><th>PRICE</th><th></th></tr></thead><tbody>';
			html += '</tbody></table>';
			$('#landForLeaseResults').empty();
			$('#landForLeaseResults').append(html);
			$("#bottomPagination").pagination(theLand.length, {
				items_per_page: numPerPage,
				callback: handleLeasePaginationClick,
				current_page: 0,
				num_display_entries: 5,
				next_text: ">",
				prev_text: "<",
				next_show_always: false,
				prev_show_always: false
			});

			if (theLeases.length <= numPerPage) {
				$('#bottomPagination').addClass('hidden');
			} else {
				$('#bottomPagination').removeClass('hidden');
			}


			forSaleHideSpinner();
		});
	

	}

	function getLandInfo(nid){


		forSaleShowSpinner();

		$.get('/land-feed-individual', {nid: nid}, function(val){

			n = val.nodes[0].node;

			var html = '<a href="javascript:;" class="backToSearchResults">BACK TO SEARCH RESULTS</a>' +
				'<ul class="propertyDetails">' +
				'<li><strong class="detailTerm">Address of Property: </strong><span class="detailData">' + n.address + '</span></li>' +
				'<li><strong class="detailTerm">Asking Price: </strong><span class="detailData">' + n.price + '</span></li>' +
				'<li><strong class="detailTerm">Taxes: </strong><span class="detailData">' + n.Taxes + '</span></li>' +
				'<li><strong class="detailTerm">Lot Size: </strong><span class="detailData">' + n.lot_size + '</span></li>' +
				'<li><strong class="detailTerm">Building Size: </strong><span class="detailData">' + n.building_size + '</span></li>' +
				'</ul>' +
				'<p>' + n.Description + '</p>';

				if (n.Photo != '') {
					html += '<img src="' + n.Photo + '">';
				}


			$('#landForSale').hide();
			$('#detailedInfo').show();

			$('#detailedInfo').html(html);

			forSaleHideSpinner();

		});



	}

	function forSaleShowSpinner(){
		$('#searchSpinner').show();
	}

	function forSaleHideSpinner(){
		$('#searchSpinner').hide();
	}

	$(document).ready(function(){
	
		var onlineOrderingAvailability = site.getOnOrd(true);

		if (onlineOrderingAvailability){
			if (ooEnabled == 1){}
			$('.hiVis').show();
			// add order online button to menu page
		}

		if (document.URL.match(/locations/)){
			$.getJSON('http://www.caseys.com/locator_lookup.php', function(data){
				caseys.locator_lookup_table = data;
			});
		}

		$('#locationsTerms').keyup(function(){
			var container = $('.multiSelect');
			var n = container.find( "input:checked" ).length;

			if (n > 0) {
				container.find('.multiSelectToggler').text( n + " selected" );
				if ($('#locationsTerms').val().length > 0){
					$('#jobSearchButton').removeClass('disabledArrow');
				} else{
					$('#jobSearchButton').addClass('disabledArrow');
				}
			}
		});

		// limit textarea length on charitable contributions page
		$('textarea').textareaMaxlength({countdown: true});

		// hook up checkboxes on job application form to populate selection list
		var referralsString = [];
		var $referralInputs = $('.referral_source input');
		var $referralSource = $('#referral_source');
		var referralStringReplace = function(string) {
			var patterns = {
					comma: /([,])/ig,
					pipe: /([\|])/ig,
					colon: /([\:])/ig
			};

			string = string.replace(patterns.comma, '<COMMA>');
			string = string.replace(patterns.pipe, '<PIPE>');
			string = string.replace(patterns.colon, '<COLON>');

			return string;
		};

		$referralInputs.on('change', function(){
			var $input = $(this),
				inputID = $input.data('value'),
				inputsWithMatches = [5, 9],
				indexInReferrals = $.inArray(inputID, referralsString),
				$matches;

			if ( $input.is(':checked') ) {
				if ( $.inArray(inputID, inputsWithMatches) != -1 ) {
					$matches = $('*[data-input-match="' + inputID + '"]');
					
					$matches.show();
					$matches.filter('input[type="text"]').addClass('required');
				}
				if ( indexInReferrals == -1 ) {
					referralsString.push(inputID);
				}
			} else {
				if ( $.inArray(inputID, inputsWithMatches) != -1 ) {
					$matches = $('*[data-input-match="' + inputID + '"]');
					
					$matches.hide();
					$matches.filter('input[type="text"]').removeClass('required');
				}
				if ( indexInReferrals !== -1 ) {
					referralsString.splice(indexInReferrals, 1);
				}
			}
			$referralSource.val(referralsString.join(','));
			$referralSource.keyup();
		});
		
		// add the corresponding text input values to the referral list string
		$referralInputs.filter('[type="text"]').on('blur', function() {
			var $input = $(this),
				matchID = $input.data('input-match'),
				matchIndex = referralsString.indexOf(matchID),
				$matchCheckbox = $referralInputs.filter('[id="rs' + matchID + '"]'),
				val = referralStringReplace($input.val()),
				newItemInList = val.length ? matchID + ':' + val : matchID;

			if (matchIndex !== -1) {
				referralsString.splice(matchIndex, 1, newItemInList);
				$referralSource.val(referralsString.join(','));
			}
		});

		// check the "other" checkbox when there is data in the corresponding text input
		$referralInputs.filter('[id="rs13"]').on('keyup', function() {
			var $input = $(this),
				matchID = $input.data('input-match'),
				$matchCheckbox = $referralInputs.filter('[id="rs' + matchID + '"]'),
				val = $input.val();

			if (val.length) {
				$matchCheckbox.attr('checked', true);
			}
		}).on('blur', function() {
			var $input = $(this),
				matchID = $input.data('input-match'),
				$matchCheckbox = $referralInputs.filter('[id="rs' + matchID + '"]'),
				val = $input.val();

			if (!val.length) {
				$matchCheckbox.attr('checked', false);
			}
		});

		$('.convictedSelector input').on('change', function(){
			if ($('#convictedYes').is(':checked')){
				$('.convictedSelect').show();
			}else{
				$('.convictedSelect').hide();
			}
		});


		var myCaseysAddress = caseys.getCookie('myCaseysAddress');
		var myCaseysPhone = caseys.getCookie('myCaseysPhone');

		if (myCaseysAddress && myCaseysAddress.length > 0){
			$('#myCaseys').css({visibility: 'visible'});
		}else{
			$('#myCaseys').css({visibility: 'hidden'});
		}


		$('.myCaseysAddress').html(myCaseysAddress);
		$('.myCaseysAddress').attr('title',myCaseysAddress);
		var phoneTelUrl = 'tel:' + myCaseysPhone;
		$('.myCaseysPhone').attr('href', phoneTelUrl);
		$('.myCaseysPhoneNumber').html(myCaseysPhone);
		

		if ($('#landForSale').length > 0 || $('#vehiclesForSale').length > 0) {
			$('#goLandSale').click(function(){
				getLandForSale();
			});

			$(document).on('click', '.viewDetails', function(){
				var nid = $(this).attr('data-nodeid');
				getLandInfo(nid);
			});

			$(document).on('click', '.backToSearchResults', function(){
				$('#landForSale, #vehiclesForSale').addClass('hiddenElem');
				$('.hiddenElem').hide();
				$('#landForSale').show();
			});

			$('#landForSale').show();
		}
		if($('#goLeaseLocations').length > 0) {	
			$('#goLeaseLocations').click(function(){
				getLeaseLocations();
			});
		}

	});
