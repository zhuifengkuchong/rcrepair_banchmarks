// custom project info window:
function ProjectInfoWindow(opts) {
	google.maps.OverlayView.call(this);
	this.latlng_ = opts.latlng;
	this.map_ = opts.map;
	this.offsetVertical_ = -395;
	this.offsetHorizontal_ = -226;
	this.height_ = 345;
	this.width_ = 403;
	
	this.project_ = opts.project;
	
	var me = this;
	this.boundsChangedListener_ = google.maps.event.addListener(this.map_, "bounds_changed", function() {
		return me.panMap.apply(me);
	});
	
	// Once the properties of this OverlayView are initialized, set its map so that we can display it. This will trigger calls to panes_changed and draw
	this.setMap(this.map_);
}
ProjectInfoWindow.prototype = new google.maps.OverlayView();
ProjectInfoWindow.prototype.remove = function() {
	if(this.div_) {
		this.div_.parentNode.removeChild(this.div_);
		this.div_ = null;
	}
};
ProjectInfoWindow.prototype.draw = function() {
	this.createElement();
	if (!this.div_) return;
	
	var pixPosition = this.getProjection().fromLatLngToDivPixel(this.latlng_);
	if(!pixPosition) return;
	
	// position our div based on the div coords of our bounds
	this.div_.style.width = this.width_ + "px";
	this.div_.style.left = (pixPosition.x + this.offsetHorizontal_) + "px";
	this.div_.style.height = this.height_ + "px";
	this.div_.style.top = (pixPosition.y + this.offsetVertical_) + "px";
	this.div_.style.display = 'block';
};
ProjectInfoWindow.prototype.createElement = function() {
	var panes = this.getPanes();
	var div = this.div_;
	if(!div) {
		// This does not handle changing panes. You can set the map to be null and then reset the map to move the div.
		div = this.div_ = document.createElement("div");
		div.className = "pw";
		div.style.position = "absolute";
		div.style.width = this.width_ + "px";
		div.style.height = this.height_ + "px";
		var contentDiv = document.createElement("div");
		contentDiv.className = "pwContent project " + "project" + this.project_.id;
		site.addResult(contentDiv, this.project_);
		
	  var closeImg = $("<a class='closeBtn wt'>Close</a>");
	  $(div).append(closeImg);

		div.appendChild(contentDiv);
		div.style.display = "none";
		panes.floatPane.appendChild(div);
	
		this.panMap();
				
	} else if (div.parentNode != panes.floatPane) {
		// The panes have changed. Move the div.
		div.parentNode.removeChild(div);
		panes.floatPane.appendChild(div);
	} else {
		// The panes have not changed, so no need to create or move the div.
	}
};
ProjectInfoWindow.prototype.panMap = function() {
	// if we go beyond map, pan map
	var map = this.map_;
	var bounds = map.getBounds();
	if(!bounds) return;
	
	// The position of the info window
	var position = this.latlng_;
	
	// The dimensions of the info window
	var iwWidth = this.width_;
	var iwHeight = this.height_;
	
	// The offset position of the info window
	var iwOffsetX = this.offsetHorizontal_;
	var iwOffsetY = this.offsetVertical_;
	
	// Padding on the info window
	var padX = 40;
	var padY = 40;
	
	// The degrees per pixel
	var mapDiv = map.getDiv();
	var mapWidth = mapDiv.offsetWidth;
	var mapHeight = mapDiv.offsetHeight;
	var boundsSpan = bounds.toSpan();
	var longSpan = boundsSpan.lng();
	var latSpan = boundsSpan.lat();
	var degPixelX = longSpan / mapWidth;
	var degPixelY = latSpan / mapHeight;

	// The bounds of the map
	var mapWestLng = bounds.getSouthWest().lng();
	var mapEastLng = bounds.getNorthEast().lng();
	var mapNorthLat = bounds.getNorthEast().lat();
	var mapSouthLat = bounds.getSouthWest().lat();

	// The bounds of the infowindow
	// Changed this to 0 because it worked: add an additional west padding of 400 px since we'll be overlaying search results:
	var iwWestLng = position.lng() + (iwOffsetX - padX) * degPixelX - 0 * degPixelX;
	var iwEastLng = position.lng() + (iwOffsetX + iwWidth + padX) * degPixelX;
	if(mapWidth > 1040) { 	
		if(mapWidth < 1250) { 
			iwEastLng += 250 * degPixelX;
		} else {
			iwEastLng += 400 * degPixelX; 
		}
	}
	var iwNorthLat = position.lat() - (iwOffsetY - padY) * degPixelY;
	var iwSouthLat = position.lat() - (iwOffsetY + iwHeight + padY) * degPixelY;

	// calculate center shift
	var shiftLng =
	    (iwWestLng < mapWestLng ? mapWestLng - iwWestLng : 0) +
	    (iwEastLng > mapEastLng ? mapEastLng - iwEastLng : 0);
	var shiftLat =
	    (iwNorthLat > mapNorthLat ? mapNorthLat - iwNorthLat : 0) +
	    (iwSouthLat < mapSouthLat ? mapSouthLat - iwSouthLat : 0);
	
	// The center of the map
	var center = map.getCenter();

	// The new map center
	var centerX = center.lng() - shiftLng;
	var centerY = center.lat() - shiftLat;
	
/*
	var shiftLat = (iwOffsetY + iwHeight + padY - 50) * degPixelY;
	var centerX = position.lng();
	var centerY = position.lat() - shiftLat;
*/
	// center the map to the new shifted center
	map.setCenter(new google.maps.LatLng(centerY, centerX));

	// Remove the listener after panning is complete.
	google.maps.event.removeListener(this.boundsChangedListener_);
	this.boundsChangedListener_ = null;	
};