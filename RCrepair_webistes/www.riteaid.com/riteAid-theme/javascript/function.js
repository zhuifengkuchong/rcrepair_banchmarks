jQuery(document).ready(function(){
    //reduce sensitivity on Mega Menus
        jQuery("li.item .nav-item-wrapper").mouseover(function () {
        var navItemHover = $(this).find("#navItemHover").val();      
        
        if (navItemHover == 'wellness+') {
        	$(this).find("#wellnessPlentiItem").attr("src", "../images/custom/wellness+Plenti-tab-hover.png"/*tpa=https://www.riteaid.com/riteAid-theme/images/custom/wellness+Plenti-tab-hover.png*/);
        	$(this).addClass("menu-hover-background");
        } else {        
	    	$(this).addClass("menu-hover-background");
	    }	
	});
	jQuery("li.item .nav-item-wrapper ").mouseout(function () {
	
	  	var navItemHover = $(this).find("#navItemHover").val();
        var navItemSelected = $(this).find("#navItemSelected").val();
        
		if (navItemHover == 'wellness+') {
        	$(this).find("#wellnessPlentiItem").attr("src", "../images/custom/wellness+Plenti-tab-default.png"/*tpa=https://www.riteaid.com/riteAid-theme/images/custom/wellness+Plenti-tab-default.png*/);
        	$(this).removeClass("menu-hover-background");
        } else {
	    	$(this).removeClass("menu-hover-background");
	    }
	});
    
    // Top navigation drop down
    jQuery("#main-nav-links> ul > li > #big-menu-hidden").css({"display":"none"});
    jQuery("#main-nav-links> ul > li").mouseover(function(){
    	jQuery(this).find("#big-menu-hidden").css({"display":"block"});
    });
    jQuery("#main-nav-links> ul > li ").mouseout(function(){
    	jQuery(this).find("#big-menu-hidden").css({"display":"none"});
    });
    jQuery("#store-locator-search .hours ul.hours-header li:last-child").css({"margin-left":"70px"});
	
	  // Log In, My Name drop down
		$(".top-nav-login,.top-nav-name-area").hover(function () {
		$(".top-nav-login, .top-nav-name-area").addClass("hover");
		$("#login-area, #login-area .login-content, #logged-area-home, #logged-area-home .login-content").fadeIn(500);
	  },function () {
			$("#login-area, #login-area .login-content, #logged-area-home, #logged-area-home .login-content").css({"display":"none"});
			$(".top-nav-login, .top-nav-name-area").removeClass("hover");
	  });
	
	$('#UserTextboxBt, #LoginTextboxBt').focus(function() {
		$(".top-nav-login, .top-nav-name-area").addClass("active");
		
	}).focusout(function () {
		$(".top-nav-login, .top-nav-name-area").removeClass("active");
	});
	
	//Adding default text to search box.
	$('#SearchTextboxBt').watermark('Search');

	
	$('#scrollbar1').tinyscrollbar();	
	$('#scrollbar2').tinyscrollbar();
	$('#scrollbar4').tinyscrollbar({sizethumb: 52 });
	$('#storeDirectionsScrollbar').tinyscrollbar({sizethumb: 52 });
	

	
	//Example 2
	var oScroll2 = $('#scrollbar3, #scrollbar6, #scrollbar7, #scrollbar8, #scrollbar9');
	if(oScroll2.length > 0){
	oScroll2.tinyscrollbar({ axis: 'x' });
	}
		
	var thumbwidth = $('#scrollbar8 .overview ul').width();
	$('#scrollbar8 .overview').width(thumbwidth);
	$('#scrollbar8').tinyscrollbar({ axis: 'x', sizethumb: 180, size: 960  });
	var pos = Math.round(($('#scrollbar8 .track').width() - $('#scrollbar8 .thumb').width()) / 2 );
	$('#scrollbar8 .thumb').css({"left":pos});
	
	var thumbwidth = $('#scrollbar9 .overview ul').width();
	$('#scrollbar9 .overview').width(thumbwidth);
	$('#scrollbar9 ').tinyscrollbar({ axis: 'x', sizethumb: 180, size: 647  });
	
	var shiftVal = Math.round(($('#scrollbar8 .overview').width() - $(document).width())/ 2);
	$('#scrollbar8 .overview').css({"left" : -shiftVal});
	//$('#scrollbar8 .overview').delay(1000).animate({left:'-960px'}, 2000, 'easeOutCubic');
	
	$('#scrollbar6, #scrollbar7').tinyscrollbar({ axis: 'x', sizethumb: 180 });
	
	if ( $(".leftnav-level2 a").hasClass("active") ) {
		$(".leftnav-level2").parent("li").addClass("selected");
	}
	
	$("#UserTextboxBt,#LoginTextboxBt").keypress(function(e){
	  if(e.which == 13){
		$('.login-bt').focus();
		}
	});

	/**
	 * Draw the logo on each child menu so that menu doesn't draw on top of the logo.
	 *  This cannot be fixed with a z-index, because hovering over the logo causes the 
	 *  menu to collapse.
	 **/
	var mainLogoInner = $(".main-logo-inner"),
		mainLogoOriginalZIndex = mainLogoInner.css("z-index"),
		mainLogo = mainLogoInner.find("a img");
	
	function checkAndPaint(navItem) {
		var child = navItem.children(".child-menu");

		if (child.is(":visible") && !child.children("img.child-menu-main-logo").length) {
			//don't call this function anymore for this element
			navItem.off("hover", null, checkAndPaintWrapper);
			
			//paint the new logo
			mainLogo.clone()
				.addClass("child-menu-main-logo")
				.css("position", "absolute")
				.prependTo(child)
				.position({of: mainLogo});
			
			mainLogoInner.css("z-index", "auto");
			return true;
		}
		
		return false;
	}
	/*
	 * <= IE 10 and lower are erratic in how they call the hover function: sometimes they 
	 * call it before painting the child menu, sometimes after, and sometimes both times.
	 * This wrapper function attempts to work around that issue. 
	 */
	function checkAndPaintWrapper() {
		var $this = $(this); 
		if (!checkAndPaint($this)) {
			mainLogoInner.css("z-index", mainLogoOriginalZIndex);
			window.setTimeout(function() {
				checkAndPaint($this);
			}, 100);
		}
	}

	
	$("li .nav-item-wrapper")
		//paint the logo as soon as the submenu is opened for the first time
		.hover(checkAndPaintWrapper)

		//if the user hovers before the document is ready, paint the logo now
		.each(function() {
			checkAndPaint($(this)); 
		});
}); 

$(function(){
			$('form.jqtransformboxes').jqTransform({imgPath:'jqtransformplugin/img/'});
		});

//modal popup
$(function() {
	// a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
	$( "#dialog:ui-dialog" ).dialog( "destroy" );

	$( "#dialog-modal" ).dialog({
		autoOpen: false,
		height: 442,
		width: 700,
		modal: true
	});
	
	$( "#featuredtab .tabContent .viewport .overview li .quick_view" ).click(function() {
		$( "#dialog-modal" ).dialog( "open" );
		return false;
	});
	
	$( "#featuredtab2 .tabContent2 .viewport .overview li .quick_view" ).click(function() {
		$( "#dialog-modal" ).dialog( "open" );
		return false;
	});
});
		
//$(document).ready(function(){
//	$('html').css("overflow-x", "hidden");
//	$('.main-banner').animate({
//		opacity: 0.25,
//		left: '+=50',
//		height: 100
//	  }, 5000, function() {
//		$(this).animate({
//			opacity: 1,
//			left: 0,
//			height: 445
//		})
//	  });
//});
$(function() {
	$("#featuredtab ul").idTabs("tabs1");
	$('#quick-link').click(function() {
	});
	
	$("#featuredtab2 ul").idTabs("tabs3");
	$('#quick-link').click(function() {
	});
});
//Load the map by passing latitude,longitude.
//Default type of map will be road type and default zoom is 10

var pinInfoBox;
var infoboxLayer;
var pinLayer;
var map = null; 

function GetMap(latitude, longitude, address, city, state,zipCode, phone, title, storeCount, bingMapKey) {
	
	pinInfoBox = new Microsoft.Maps.Infobox(new Microsoft.Maps.Location(0, 0), { visible: false });
	infoboxLayer = new Microsoft.Maps.EntityCollection();
	infoboxLayer.push(pinInfoBox);
	pinLayer = new Microsoft.Maps.EntityCollection();
	
	map = new Microsoft.Maps.Map(document.getElementById("myMap"), 
				{credentials: bingMapKey,
				enableSearchLogo: false,
				center: new Microsoft.Maps.Location(latitude, longitude),
				mapTypeId: Microsoft.Maps.MapTypeId.road,
				zoom: 10});
	
	if(address != '' || city != '' || state != '' || zipCode!= '')
	{
		var pushpinOptions = {icon : "../images/custom/map1.png"/*tpa=https://www.riteaid.com/riteAid-theme/images/custom/map1.png*/ , text : ''+(storeCount)+'' };  
		var pushpin= new Microsoft.Maps.Pushpin(map.getCenter(), pushpinOptions); 
		pushpin.Title = title;
		pushpin.Description = address+"<br/>"+city+", "+ state+" "+zipCode+"<br/>Phone:"+ phone;
		Microsoft.Maps.Events.addHandler(pushpin, 'mouseover', displayInfobox);
		Microsoft.Maps.Events.addHandler(pushpin, 'mouseout', hideInfobox);
		pinLayer.push(pushpin);
	}
	
	map.entities.push(pinLayer);
    map.entities.push(infoboxLayer);
}

function GetDirection(startLatitude,startLongitude,destinationLatitude,destinationLongitude,routeType,bingMapKey)
{
	map = new Microsoft.Maps.Map(document.getElementById("myMap"), 
			{credentials: bingMapKey,
			enableSearchLogo: false,
			mapTypeId: Microsoft.Maps.MapTypeId.road
			});
	
	pinInfoBox = new Microsoft.Maps.Infobox(new Microsoft.Maps.Location(0, 0), { visible: false });
	infoboxLayer = new Microsoft.Maps.EntityCollection();
	infoboxLayer.push(pinInfoBox);
	pinLayer = new Microsoft.Maps.EntityCollection();
	
	var routeRequest = null;
	if(routeType == "Quickest")
	{
		routeRequest = "//dev.virtualearth.net/REST/V1/Routes/Driving?du=mi&wp.0="+startLatitude+","+startLongitude+ "&wp.1=" + destinationLatitude + "," + + destinationLongitude + "&optimize=time&routePathOutput=Points&output=json&jsonp=RouteCallback&key=" + bingMapKey;
		//routeRequest = "http://dev.virtualearth.net/REST/V1/Routes/Driving?du=mi&wp.0="+fromaddress+ "&wp.1=" +toaddress+ "&optimize=time&routePathOutput=Points&output=json&jsonp=RouteCallback&key=" + bingMapKey;
		CallRestService(routeRequest);
	}
	else
	{
		routeRequest = "//dev.virtualearth.net/REST/V1/Routes/Driving?du=mi&wp.0="+startLatitude+","+startLongitude + "&wp.1=" + destinationLatitude + "," + + destinationLongitude + "&optimize=distance&routePathOutput=Points&output=json&jsonp=RouteCallback&key=" + bingMapKey;
		//routeRequest = "http://dev.virtualearth.net/REST/V1/Routes/Driving?du=mi&wp.0="+fromaddress+ "&wp.1=" +toaddress+ "&optimize=distance&routePathOutput=Points&output=json&jsonp=RouteCallback&key=" + bingMapKey;
        CallRestService(routeRequest);
	}
}

function RouteCallback(result) {
	 var turns = "";
	 if (result &&
	          result.resourceSets &&
	          result.resourceSets.length > 0 &&
	          result.resourceSets[0].resources &&
	          result.resourceSets[0].resources.length > 0) {

	         // Set the map view
	         var bbox = result.resourceSets[0].resources[0].bbox;
	         var viewBoundaries = Microsoft.Maps.LocationRect.fromLocations(new Microsoft.Maps.Location(bbox[0], bbox[1]), new Microsoft.Maps.Location(bbox[2], bbox[3]));
	         if (result.resourceSets[0].resources.length > 5) {
	         viewBoundaries.height = viewBoundaries.height + 0.1;
	         viewBoundaries.width = viewBoundaries.width + 0.1;
	         }
	         map.setView({ bounds: viewBoundaries });

	         // Draw the route
	         var routeline = result.resourceSets[0].resources[0].routePath.line;
	         var routepoints = new Array();
	         for (var i = 0; i < routeline.coordinates.length; i++) {
	           routepoints[i] = new Microsoft.Maps.Location(routeline.coordinates[i][0], routeline.coordinates[i][1]);
	         }

	         // Draw the route on the map
	         var options = { strokeColor: new Microsoft.Maps.Color(200, 0, 0, 200),
	           strokeThickness: 5
	         };
	         var routeshape = new Microsoft.Maps.Polyline(routepoints, options);
	         map.entities.push(routeshape);

	         //Draw the instructions-points
	         var directions = result.resourceSets[0].resources[0].routeLegs[0].itineraryItems;
	         
	         var totaldistance = 0.0;
	         var totalduration = 0.0;
	         for (var i = 0; i < directions.length; i++) {
	           var location = new Microsoft.Maps.Location(directions[i].maneuverPoint.coordinates[0], directions[i].maneuverPoint.coordinates[1]);
	           var pushpin = new Microsoft.Maps.Pushpin(location);
	           var pushpinOptions = { icon: "../images/custom/map1.png"/*tpa=https://www.riteaid.com/riteAid-theme/images/custom/map1.png*/,
	             anchor: new Microsoft.Maps.Point(15, 15),
	             text: (i + 1).toString()
	           };
	           pushpin.setOptions(pushpinOptions);
	           pushpin.Title = directions[i].instruction.text;
	           pushpin.Description = "Distance: " + directions[i].travelDistance + "miles<br>Time: " + directions[i].travelDuration + "s";
	           pinLayer.push(pushpin);
	           Microsoft.Maps.Events.addHandler(pushpin, 'mouseover', displayInfobox);
	   		   Microsoft.Maps.Events.addHandler(pushpin, 'mouseout', hideInfobox);

	           turns += "<div class=\"map1\">"+(i + 1) + "&nbsp;&nbsp;&nbsp;" + directions[i].instruction.text;
	           turns += " (" + directions[i].travelDistance + " miles)</div>";
	           
	           totaldistance += directions[i].travelDistance;
	           totalduration += directions[i].travelDuration;
	        }
	         
	         totalduration = totalduration/60;
	         
	         map.entities.push(pinLayer);
	         map.entities.push(infoboxLayer);
	         
	       	 //searchResultBar("Distance: " + totaldistance.toFixed(2) + " mi Estimated Time: " + totalduration.toFixed(2) + " Minutes");
	       }
		   //directionResult(turns);
}


function CallRestService(request) {
	var script = document.createElement("script");
	script.setAttribute("type", "text/javascript");
    script.setAttribute("src", request);
    document.body.appendChild(script);
}

function directionResult(result)
{
	$('#storedir').html(result);
	$('#storeDirectionsScrollbar').tinyscrollbar({sizethumb: 52});
}

function searchResultBar(result)
{
	 $('#searchResultBarId').html(result);
	 $('#scrollbar4').tinyscrollbar({sizethumb: 52});
}

function displayInfobox(e) {
	pinInfoBox.setOptions({title: e.target.Title, description: e.target.Description, visible:true, offset: new Microsoft.Maps.Point(0,25)});
	pinInfoBox.setLocation(e.target.getLocation());
    
  //A buffer limit to use to specify the infobox must be away from the edges of the map.

    var buffer = 25;

    var infoboxOffset = pinInfoBox.getOffset();

    var infoboxAnchor = pinInfoBox.getAnchor();

    var infoboxLocation = map.tryLocationToPixel(e.target.getLocation(), Microsoft.Maps.PixelReference.control);

    var dx = infoboxLocation.x + infoboxOffset.x - infoboxAnchor.x;

    var dy = infoboxLocation.y - 25 - infoboxAnchor.y;

    if (dy < buffer) {    //Infobox overlaps with top of map.

        //Offset in opposite direction.
        dy *= -1;

        //add buffer from the top edge of the map.

        dy += buffer;

    } else {

        //If dy is greater than zero than it does not overlap.

        dy = 0;
    }

    if (dx < buffer) {    //Check to see if overlapping with left side of map.

        //Offset in opposite direction.
        dx *= -1;
        //add a buffer from the left edge of the map.

        dx += buffer;

    } else {              //Check to see if overlapping with right side of map.

        dx = map.getWidth() - infoboxLocation.x + infoboxAnchor.x - pinInfoBox.getWidth();
     //If dx is greater than zero then it does not overlap.

        if (dx > buffer) {

            dx = 0;

        } else {

            //add a buffer from the right edge of the map.
            dx -= buffer;
        }
    }
    
    //Adjust the map so infobox is in view
    if (dx != 0 || dy != 0) {

    	map.setView({ centerOffset: new Microsoft.Maps.Point(dx, dy), center: map.getCenter() });
    }
}

function hideInfobox(e) {
	pinInfoBox.setOptions({ visible: false });
}

//for search result
//HIDE ALL
$(function() {
  $(".brands .hide_all").toggle(function (){
      jQuery(this).addClass("active");
		jQuery(".brands .content").slideUp("fast");
      jQuery("span", this).text("Show All")
      .stop();
  }, function(){
		jQuery(this).removeClass("active");
      jQuery(".brands .content").slideDown("fast");
      jQuery("span", this).text("Hide All")
      .stop();
  });
	
	$(".categories .hide_all").toggle(function (){
      jQuery(this).addClass("active");
		jQuery(".categories .content").slideUp("fast");
      jQuery("span", this).text("Show All")
      .stop();
  }, function(){
		jQuery(this).removeClass("active");
      jQuery(".categories .content").slideDown("fast");
      jQuery("span", this).text("Hide All")
      .stop();
  });
	
	$(".ratings .hide_all").toggle(function (){
      jQuery(this).addClass("active");
		jQuery(".ratings .content").slideUp("fast");
      jQuery("span", this).text("Show All")
      .stop();
  }, function(){
		jQuery(this).removeClass("active");
      jQuery(".ratings .content").slideDown("fast");
      jQuery("span", this).text("Hide All")
      .stop();
  });
  
  $(".price .hide_all").toggle(function (){
      jQuery(this).addClass("active");
		jQuery(".price .content").slideUp("fast");
      jQuery("span", this).text("Show All")
      .stop();
  }, function(){
		jQuery(this).removeClass("active");
      jQuery(".price .content").slideDown("fast");
      jQuery("span", this).text("Hide All")
      .stop();
  });
  
  $('#seeToolTipForRememberMe').showTooltip({
			delayIn: 0,
			delayOut: 1,
			border: true,
			trigger: 'hover'
		});
  
  $("#login-area").hover(function () {
		$("#seeToolTipForRememberMe span.toolTipArrow").addClass("toolTipArrowNew");
		$("#seeToolTipForRememberMe span.toolTipArrowNew").removeClass("toolTipArrow");
	  });
	
});

//For Password strength
function passwordStrength(password) {
	var desc = new Array();
	desc[0] = "Password not entered";
	desc[1] = "Weak";	
	desc[2] = "Medium";
	desc[3] = "Strong";
	desc[4] = "Invalid character entered";	

	var score = 0;
	
	var passwordStrengthWeak = $('input#passwordStrengthWeak').val(); 
	var passwordStrengthMediumLowercase = $('input#passwordStrengthMediumLowercase').val(); 
	var passwordStrengthMediumUppercase = $('input#passwordStrengthMediumUppercase').val();
	var passwordStrengthStrong = $('input#passwordStrengthStrong').val();
	var passwordStrengthInvalidCharacters = $('input#passwordStrengthInvalidCharacters').val();  

	var score = 0;
	
	//if password greater than or equal to 8 give 1 point
	if (password.length >= passwordStrengthWeak) score++;

	//if password has both lower and uppercase characters give 1 point	
	if ((password.length >= passwordStrengthWeak) && (password.match(passwordStrengthMediumLowercase)) && (password.match(passwordStrengthMediumUppercase))) {
	 	score++;
	 	if (password.match(passwordStrengthStrong)) {
	 		score++;
	 	}
     }
     
     if (password.match(passwordStrengthInvalidCharacters)) {        
        score = 4;
     }
          	
	 document.getElementById("passwordDescription").innerHTML = desc[score];
	 document.getElementById("passwordStrength").className = "strength" + score;
}

function setupPlaySound() {
    if (navigator.appName == "Microsoft Internet Explorer") {
        var snd = document.createElement("bgsound");
        document.getElementsByTagName("body")[0].appendChild(snd);
        playSound = function(url) {
        	snd.src = "";
            snd.src = url;
        }
    }
    else {
        playSound = function(url) {
            var obj = document.createElement("object");
            obj.setAttribute('width','0px');
            obj.setAttribute('height','0px');
            obj.setAttribute('type','audio/x-wav');
            obj.setAttribute('data', url);
            obj.setAttribute('id', 'securityAudio');
            var param = document.createElement("param");
            param.setAttribute('name', 'src');
            param.setAttribute('value',url);
            obj.appendChild(param);
            
            var body = document.getElementsByTagName("body")[0];
            body.appendChild(obj);
        }
    }
}
function changeImage(element, sourcePath, captchaAnswer) {
	var randomNumber = Math.floor(Math.random()*101);
	document.getElementById(captchaAnswer).value='';
	document.getElementById(element).src = sourcePath +"?"+ randomNumber;
	return false;
}

/**
 Function to allow only numbers in a input	
*/

function isNumeric(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

/*jslint browser: true, plusplus: true */
/*global $:false */
(function ($) {
    "use strict";

    var placeholderSupport = document.createElement('input').placeholder !== undefined,
    	originalVal = $.fn.val;

	if (!placeholderSupport) {
	    $.fn.val = function () {
	        var first,
	            actualVal;
	
			//setting value
	        if (arguments.length) {
	            originalVal.apply(this, arguments);
	
	            this.filter("input[riteaid-placeholder]").each(function () {
	                if (this.value === "") {
	                    this.value = this.getAttribute("placeholder");
	                    $(this).addClass("riteaid-placeholder");
	                }
	            });
	
	            return this;
	        }
	        
	        //getting value
	        first = this.eq(0);
	        if (first.attr('riteaid-placeholder')) {
	            actualVal = originalVal.apply(this);
	            if (actualVal === first[0].getAttribute('placeholder') && first.hasClass("riteaid-placeholder")) {
	                return "";
	            }
	            return actualVal;
	        }
	
	        return originalVal.apply(this);
	    };
    }
    /******************************************
     * placeholder support for browsers that  *
     * don't support it                       *
     ******************************************/
    $.fn.raPlaceholder = function (settings) {
    
        if (!placeholderSupport) {
    
	        //settings defaults
	        settings = $.extend(
	            {
	                //automatically clear the input field when its form is submitted?
	                "clearOnFormSubmit": true
	            },
	            settings
	        );
    
            this.filter("[placeholder]")
                .attr("riteaid-placeholder", "true")
                .on('drop', function (e) {
                    var maxLength,
                        data,
                        $this = $(this);

                    if ($this.val()) {
                        data = e.originalEvent.dataTransfer.getData("text");
                        if (data) {
                            maxLength = $(this).attr("maxlength");
                            if (maxLength) {
                                data = data.substring(0, maxLength);
                            }
                            this.value = data;
                            $this.removeClass("riteaid-placeholder");
                            e.preventDefault();
                        }
                    }
                })
                .on('click focus', function () {
                    var $this = $(this);
                    if (!$this.val()) {
                    	this.value = "";
                        $this.removeClass("riteaid-placeholder");
                    }
                })
                .on('blur', function () {
                	//trigger val change in $.fn.val
                    $(this).val(this.value);                    
                })
                .each(function () {
                    var element = this,
                    	$this = $(element);

                    if (settings.clearOnFormSubmit) {
                        $this.closest("form").submit(function () {
                            var placeholder = $this.attr("placeholder");
                            if (element.value === placeholder) {
                            	element.value = "";
                            }
                        });
                    }
                })

                .not(":focus").blur();
        }
        return this;
    };

    /******************************************
     * autotab                                *
     ******************************************/
    $(document).ready(function () {
        var formatMap = {
            "alpha": "A-Za-z",
            "apos": "\\'",
            "colon": "\\:",
            "comma": "\\,",
            "hash": "\\#",
            "hyphen": "\\-",
            "number": "\\d",
            "percent": "%",
            "period": "\\.",
            "plus": "\\+",
            "quote": "\\\"",
            "slash": "\\\\",
            "space": " "
        };

        $("input[data-autotab-to]").each(function () {
            var to = $("#" + $(this).attr("data-autotab-to"));

            if (!to.attr("data-autotab-to")) {
                to.attr("data-autotab-to", "");
            }
        });
        $("input[data-autotab-to],input[data-format-options]").each(function () {
            var options = {},
                formatRegex = "",
                $this = $(this),
                target = $this.attr("data-autotab-to"),
                id = $this.attr("id");

            $.each(($this.attr("data-format-options") || "").split(/\s*[\,\s]+\s*/), function () {
                formatRegex += (formatMap[this] || "");
            });
            if (formatRegex) {
                options.format = "custom";
                options.pattern = "[^" + formatRegex + "]";
            }

            if (target) {
                options.target = "#" + target;
            }

            if (id) {
                options.previous = "input[data-autotab-to=\'" + id + "\']";
            }

            $this.autotab(options);
        });

        /******************************************************
         * Clickable elements                                  *
         ******************************************************/
        $("[data-click-form]").click(function () {
            $("form#" + $(this).attr("data-click-form")).submit();
        });

        $("[data-click-href]").click(function () {
            window.location.href = $(this).attr("data-click-href");
        });
    });
}($));

/* $(function() {
	jQuery(".default .main-nav-links ul .item").mouseover(function() {
	jQuery('#wrapper').append('<div class="navHoverBlock" style="width: 100%; height: 390px; background: #013b74; display: block; position: absolute; top: 93px; z-index: 5; opacity: 0.85;" />');
	}).mouseout(function() {
	jQuery('.navHoverBlock').hide();
 	})
});
*/


/*$(function() {
	$( "#slider-range" ).slider({
		range: true,
		min: 0,
		max: 50,
		values: [ 2, 47 ],
		slide: function( event, ui ) {
			$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
		}
	});
	$( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
		" - $" + $( "#slider-range" ).slider( "values", 1 ) );
});
*/