;(function(){

  App.start = function()
    {

      this.initHeader();
      this.initModules();

    };

  App.initModules = function()
    {

      for( var module in this.Modules )
        {

          this[module] = new this.Modules[module];
          this[module].start();

        }

    };  

  App.initHeader = function()
    {

      this.$_headerNav = $('#HeaderNav');
      this.$_headerNavLinks = this.$_headerNav.find('a.header-nav-link');

      this.$_headerNavLinks.each(function(i, navLink){
        var $_navLink = $(navLink);

        if( location.pathname.indexOf( $_navLink.attr('href') ) > -1 )
          {
            $_navLink.addClass('active');
          }
      })

    };

  window.App = App;

})();