$ = jQuery;
$(document).ready(function() {
	var $quicktabs = $('#quicktabs-research_categories').find('.quicktabs-tabs li a');
	//console.log($quicktabs.length);
	var baseURL = window.location.pathname;
	$quicktabs.on("click", function() {
		window.history.pushState("object or string", "Title", baseURL + '#' + $(this).attr('href').split('#')[1].replace("--", "-"));
	});
});


// fixes width of articles that appear in quicktab related dropdowns
// $('.flexslider').flexslider({
    // animation: "slide", 
    // start: function(slider){
        // $('.flexslider').resize();
    // }
// });