var txt1 = "";
var txt2 = "";

function mchoiceResize(){

    if (jQuery(window).width() < 1024 ) {
        jQuery('.benefit-wrap').after(jQuery('.side'));
    } else {
        jQuery('.video-embed').after(jQuery('.side'));
    }

    if (jQuery(window).width() > 725){
        jQuery('.views-row-3').after(jQuery('.resource-content'));
    } else if (jQuery(window).width() < 725 && jQuery(window).width() > 554) {
        jQuery('.views-row-3').after(jQuery('.resource-content'));
    }
    else if (jQuery(window).width() < 554 && jQuery(window).width() > 373){
        jQuery('.views-row-2').after(jQuery('.resource-content'));
    } else {
        jQuery('.views-row-1').after(jQuery('.resource-content'));
    }
}

(function($){

	/* ================================================
		 Document Ready
		 ================================================ */
	$(document).ready(function() {

        if (jQuery('body').hasClass('node-type-cvs-video-landing-page')){
            jQuery('.view-cvs-video .views-row').each(function(){
                if(jQuery(this).is(':hidden')){
                    jQuery(this).remove();
                }
            })
            jQuery('.view-cvs-video .views-row:nth-child(3)').addClass('last');
        }

        if (jQuery('body').hasClass('page-node-81')) {
            jQuery('.view-mosaic .view-footer').addClass('clear');
        }

        //Custom Banner Slide Background Color
        if (jQuery('body').hasClass('front')) {

            jQuery('.region-hero .flexslider li.flexslider_views_slideshow_slide').each(function(){
                var hero_slide_background = "#" + jQuery(this).find('.banner-slide.wrapper').attr('rel');
                jQuery(this).css('background-color', hero_slide_background);
            })

        }

        // CVS Health at a glance Breadcrumb FIX
        if (jQuery('body').hasClass('page-node-1875')){
            jQuery('.easy-breadcrumb_segment-2').html('Our Story');
        }

        // mChoice

        if (jQuery('body').hasClass('node-type-mchoice-page')) {

            mchoiceResize();

            jQuery(window).resize(function(){
                mchoiceResize();
            })

            jQuery('.trigger, .close-vid').click(function(){
                jQuery('.resource-content').slideToggle();
                jQuery('.trigger, .close-vid').toggleClass('active');
            })
        }

		jQuery('#quicktabs-tab-research_categories-2').html('Quality <br> &amp; Delivery');

		/** for hiding carousel if image count is less or equal to 3**/
		var no_of_images = $('.flexslider-views-slideshow-main-frame li').length;
		if(no_of_images <= 4){
			$('.carousel-controls').css('display','none');
		}

		var window_width_rs = jQuery(window).width();
		var videoheight_ratio = (window_width_rs/16)*9;
		var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/i) ? true : false );
		var iosphone = ( navigator.userAgent.match(/(iPhone)/i) ? true : false );
		var obec_id = '';
		if(window_width_rs > 319 && window_width_rs < 979 ){
			jQuery('.node-type-article object').width(window_width_rs);
		}

		//set email subject
		$('.node-type-article .social-media .email a').attr('href', 'mailto:?subject=' + $('.page-title').html());

		/* ======================
		   Pull Quotes
		   ====================== */

	    $('http://www.cvshealth.com/sites/all/themes/cvs/js/span.pq').each(function() {
			var $parentParagraph = $(this).parent('p');
			$parentParagraph.css('position', 'relative');
			$(this).clone().addClass('pullquote').prependTo($parentParagraph);
	    }); // end each

		/* ======================
			Scroll to the top
			====================== */
		$('.fixed-btn-holder .top,.fixed-btn-holder .back-to-top').click(function(){
			//$(this).parent().addClass('going-up').hide();
			$("html, body").animate({ scrollTop: 0 }, "slow", function() {
				$('.fixed-btn-holder').removeClass('going-up');
			});
		});

		/* ==================================================
		 * Fixed holder stuff : puts fixed holder in position on page load.
		 * ================================================== */
		var $header = $('.navbar-header');
		var $fixed_holder = $('.fixed-btn-holder');
		var $footer = $('.footer-links-wrapper-holder')/*Modernizr.mq('only screen and (min-width: 820px') ? $('.footer-container') : $('.footer-container')*/;
		// put fixed holder into position
		$footer.before($fixed_holder);
		/* ==================================================
		 * Footer checker : puts the scroll to top in the correct dom position.
		 * ================================================== */
		var $footer_check = function() {
			/*if(Modernizr.mq('only screen and (min-width: 820px') && !$footer.hasClass('footer-links-wrapper-holder')) {
				$footer = $('.footer-links-wrapper-holder');
				$footer.before($fixed_holder);
			} else if(!Modernizr.mq('only screen and (min-width: 820px') && !$footer.hasClass('.footer-container')) {
				$footer = $('.footer-links-wrapper-holder');
				$footer.before($fixed_holder);
			}*/
		};

		/* ==================================================
		 * The fixed position scroll top thingy. This handles putting it in place properly.
		 * ================================================== */
		// var $fixed_check = function() {
		// 	var $top = $(window).scrollTop();
		// 	if($top < $header.outerHeight(true)) {
		// 		// show
		// 		$fixed_holder.slideUp('slow');
		// 	} else {
		// 		// not visible
		// 		$fixed_holder.slideDown('slow');
		// 	}
		// 	// lock to footer top
		// 	var $footer_top = $footer.offset().top;
		// 	var $footer_left = $footer.offset().left;

		// 	if(parseInt($footer_top) <= ($top + $(window).height())) {
	 //  			if($(window).width() >= 910) {
	 //  				$fixed_holder.css({'position': 'static', 'margin-right': ($(window).width() - $('.wrapper').outerWidth() ) / 2});
		// 		} else {
	 //  				$fixed_holder.css({'position': 'static', 'margin-right' : ($(window).width() / 2) - 50});
		// 		}
		// 		$fixed_holder.wrap('<div class="btn-holder"></div>');
		// 	} else {
	 //    		if($(window).width() >= 910) {
	 //  				$fixed_holder.css({'position': 'fixed', 'right': ($(window).width() - $('.wrapper').outerWidth() ) / 2, 'margin-right':0});
		// 		} else {
	 //  				$fixed_holder.css({'position': 'fixed', 'right': ($(window).width() - $('.wrapper').outerWidth() ) / 2, 'margin-right':-50});
		// 		}
		// 	}

		// };
		// $fixed_check();
		// $(window).scroll(function() {
		// 	if(!$fixed_holder.hasClass('going-up')) {
		// 		$fixed_check();
  //               // console.log($top);
		// 	}
		// });

		/* ======================
			 Hero Slide CTA
			 ====================== */
		$('#flexslider_views_slideshow_main_slideshow-block_0 .slide-container').each(function(index, el) {
			var $btn = $(el).find('.btn');
			// If there is a .btn link
			if ($btn) {
				var href = $btn.attr('href');
				var target = $btn.attr('target');
				var onclick = $btn.attr('onclick');
				// Make icon a link
				$(el).find('.image-holder img').wrap('<a href="'+href+'" target="'+target+'" onclick="'+onclick+'"/>');
				$(el).find('h2').wrapInner('<a href="'+href+'" target="'+target+'" onclick="'+onclick+'"/>');
			};
		});


		/* =================
		   Careers
        /* ================= */

        jQuery('#edit-submit.btn.rArrow.form-submit').val('Update');
        jQuery('#quicktabs-tab-research_categories-2').html('Quality <br> &amp; Delivery');



        /** for hiding carousel if image count is less or equal to 3**/
        var no_of_images = $('.flexslider-views-slideshow-main-frame li').length;
        if(no_of_images <= 4){
            $('.carousel-controls').css('display','none');
        }

        var window_width_rs = jQuery(window).width();
        var videoheight_ratio = (window_width_rs/16)*9;
        var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/i) ? true : false );
        var iosphone = ( navigator.userAgent.match(/(iPhone)/i) ? true : false );
        var obec_id = '';
        if(window_width_rs > 319 && window_width_rs < 979 ){
            jQuery('.node-type-article object').width(window_width_rs);
        }


        //social media links
        $('.google-plus-share-button').click(function() {
          var title = $(this).data('title');
          var url = $(this).data('url');
          MyWindow=window.open('https://plus.google.com/share?title=' + title + '&url=' + url ,'MyWindow','top=200, left=500,width=500,height=400');
          return false;
        });

        $('.facebook-share-button').click(function() {
          var title = $(this).data('title');
          var url = $(this).data('url');
          MyWindow=window.open('https://facebook.com/sharer/sharer.php?app_id=541013719339102&sdk=joey&u=' + url ,'MyWindow','top=200, left=500,width=500,height=400');
          return false;
        });

        $('.twitter-share-button').click(function() {
          var title = $(this).data('title');
          var url = $(this).data('url');
          MyWindow=window.open('https://twitter.com/share?text=' + title + '&amp;url=' + bitly_url ,'MyWindow','top=200, left=500,width=500,height=400');
          return false;
        });



        //set email subject
        $('.node-type-article .social-media .email a').attr('href', 'mailto:?subject=' + $('.page-title').html() + '&body=' + document.URL);

        /* ======================
           Pull Quotes
           ====================== */

        $('http://www.cvshealth.com/sites/all/themes/cvs/js/span.pq').each(function() {
            var $parentParagraph = $(this).parent('p');
            $parentParagraph.css('position', 'relative');
            $(this).clone().addClass('pullquote').prependTo($parentParagraph);
        }); // end each

        /* ======================
            Scroll to the top
            ====================== */
        $('.fixed-btn-holder .top,.fixed-btn-holder .back-to-top').click(function(){
            //$(this).parent().addClass('going-up').hide();
            $("html, body").animate({ scrollTop: 0 }, "slow", function() {
                $('.fixed-btn-holder').removeClass('going-up');
            });
        });

        /* ==================================================
         * Fixed holder stuff : puts fixed holder in position on page load.
         * ================================================== */
        var $header = $('.navbar-header');
        var $fixed_holder = $('.fixed-btn-holder');
        var $footer = $('.footer-links-wrapper-holder')/*Modernizr.mq('only screen and (min-width: 820px') ? $('.footer-container') : $('.footer-container')*/;
        // put fixed holder into position
        $footer.before($fixed_holder);
        $fixed_holder.wrap('<div class="btn-holder"></div>');
        /* ==================================================
         * Footer checker : puts the scroll to top in the correct dom position.
         * ================================================== */
        var $footer_check = function() {
            /*if(Modernizr.mq('only screen and (min-width: 820px') && !$footer.hasClass('footer-links-wrapper-holder')) {
                $footer = $('.footer-links-wrapper-holder');
                $footer.before($fixed_holder);
            } else if(!Modernizr.mq('only screen and (min-width: 820px') && !$footer.hasClass('.footer-container')) {
                $footer = $('.footer-links-wrapper-holder');
                $footer.before($fixed_holder);
            }*/
        };

        /* ==================================================
         * The fixed position scroll top thingy. This handles putting it in place properly.
         * ================================================== */
        // var $fixed_check = function() {
        //     var $top = $(window).scrollTop();
        //     if($top < $header.outerHeight(true)) {
        //         // show
        //         $fixed_holder.slideUp('slow');
        //     } else {
        //         // not visible
        //         $fixed_holder.slideDown('slow');
        //     }
        //     // lock to footer top
        //     var $footer_top = $footer.offset().top;
        //     var $footer_left = $footer.offset().left;

        //     if(parseInt($footer_top) <= ($top + $(window).height())) {
        //         if($(window).width() >= 910) {
        //             $fixed_holder.css({'position': 'static', 'margin-right': ($(window).width() - $('.wrapper').outerWidth() ) / 2});
        //         } else {
        //             $fixed_holder.css({'position': 'static', 'margin-right' : ($(window).width() / 2) - 50});
        //         }
        //         $fixed_holder.wrap('<div class="btn-holder"></div>');
        //     } else {
        //         if($(window).width() >= 910) {
        //             $fixed_holder.css({'position': 'fixed', 'right': ($(window).width() - $('.wrapper').outerWidth() ) / 2, 'margin-right':0});
        //         } else {
        //             $fixed_holder.css({'position': 'fixed', 'right': ($(window).width() - $('.wrapper').outerWidth() ) / 2, 'margin-right':-50});
        //         }
        //     }




        // };
        // $fixed_check();
        // $(window).scroll(function() {
        //     if(!$fixed_holder.hasClass('going-up')) {
        //         $fixed_check();
        //         // console.log($top);
        //     }
        // });

        /* ======================
             Hero Slide CTA
             ====================== */
        $('#flexslider_views_slideshow_main_slideshow-block_0 .slide-container').each(function(index, el) {
            var $btn = $(el).find('.btn');
            // If there is a .btn link
            if ($btn) {
                var href = $btn.attr('href');
                var target = $btn.attr('target');
                var onclick = $btn.attr('onclick');
                // Make icon a link
                $(el).find('.image-holder img').wrap('<a href="'+href+'" target="'+target+'" onclick="'+onclick+'"/>');
                $(el).find('h2').wrapInner('<a href="'+href+'" target="'+target+'" onclick="'+onclick+'"/>');
            };
        });


        /* =================
           Careers
        ====================*/

    $('#what-we-offer').waypoint(function() {
      $('.fixed-sidebar-menu .what-we-offer').addClass('active');
      $('.fixed-sidebar-menu .who-we-are').removeClass('active');
    }, {offset: 100});

    $('#who-we-are').waypoint(function() {
      $('.fixed-sidebar-menu .who-we-are').addClass('active');
      $('.fixed-sidebar-menu .what-we-offer').removeClass('active');
    }, {offset: 100});

    $('.fixed-sidebar-menu').waypoint('sticky');


        /* =============
            Definition links click functionality
        ========================= */
        var pathname = window.location.search;
        var pathparts = pathname.split('&');
        var title="";
        if(pathparts.length > 1){
            var titlevalue = pathparts[1].split("=")
            title = titlevalue[1];
            title = title.replace(/%20/g," ");
        }

        /* ====================
         *
         * ==================== */
        // TOOD: Chris, do you know what this is for?
        jQuery(".view-definitions-page a").removeClass('active');
        jQuery(".view-definitions-page a").each(function(){
            if(jQuery(this).attr("title")== title) {
                jQuery(this).addClass("active");
                if(jQuery(".view-definitions-page").size()){
                    callDefinitions(jQuery(this));
                }
            }
        });

        /* ======================
             Mosaic Filter Ajax Filtering
             ====================== */
        // If the page has a mosaic
        if ($('.main .view-mosaic').length > 0) {
            $(document).ajaxStart(function(){
                if(!$('.preloader').length > 0){
                    $('.view-mosaic .view-content').prepend('<div class="grid_1 preloader excerpt_block" style="display:none;"></div>');
                }
                // Give container a height to prevent jump when .excerpt-blocks are set to display none
                var currentHeight = $('.view-mosaic').height() - $('.view-mosaic .pager').height();
                $('.view-mosaic .view-content').height(currentHeight);

                $('.view-mosaic .excerpt-block').fadeTo(300, 0, function(){
                    $('.preloader').show();
                    $('.view-mosaic .excerpt-block').css('display', 'none');
                });
            });
            $(document).ajaxSuccess(function(){
                $(this).remove();
                $('.view-mosaic .excerpt-block').css('display', 'block').fadeTo(300, 1);
                picturefill();
            });
        };

        /* ====================
         * Call Definitions
         * ==================== */
        $(".view-definitions-page .view-content a").click(function(){
            callDefinitions($(this));
        });

        /* ======================
             Jump Menu
             ====================== */
        $('.jump-menu a').click(function(e) {
            var target = $($(this).attr("href"));
            if (target) {
                $('.jump-menu a').removeClass('active')
                $(this).addClass('active');
                $('html,body').animate({scrollTop: target.offset().top - 20},'slow');
            };

        });

        /* ======================
             Make link active based on current URL
             ====================== */
        $('.active-url a').each(function(){
            if($(this).attr('href') == window.location.pathname) {
                $(this).addClass('active').click(function(e) {
                    e.preventDefault();
                });
            }
        });

        /* ====================
         Data Hub
         ==================== */
        $("area.basic").cluetip({dropShadow: false, hoverIntent:false});
        $("a.definition-title").cluetip({dropShadow: false,hoverIntent:false,closeText: "<img src=\"../img/close_icon.png\"/*tpa=http://www.cvshealth.com/sites/all/themes/cvs/img/close_icon.png*/ alt=\"\"/>",sticky:"yes",onShow:function(){
            $("#cluetip #cluetip-outer").addClass("outer-def");
            $("#cluetip #cluetip-inner").addClass("inner-titles");
        }});

        /* ====================
         * Handle #state location change.
         * ==================== */
        $("#state").bind("change", function(){
            window.location = (jQuery(this).val());
        });

        /* ====================
         CVS Caremark At a Glance
         ==================== */
        $(".page-node-1875").ajaxComplete(function(){
            if($("#edit-field-us-state-value").val() != "All"){
            $(".view-fact-sheets .view-content").show();
            $(".pane-fact-sheets .views-widget").css("background-image","url(/sites/all/themes/cvs/img/dotborder.png)");
            }
            else{
                $(".view-fact-sheets .view-content").hide();
                             $(".pane-fact-sheets .views-widget").css("background-image","none");
            }
        });

        /* ====================
             Media Download
             ==================== */
        jQuery(".gallery_download input").click(function(){
            if(jQuery("#terms_agree").attr('checked')) {
                jQuery(".error-mes").hide();
                // track download event
                var pageTitle =  $('h1.page-title').text();
                    _gaq.push(['_trackEvent', 'Media Gallery', pageTitle , 'Download']);
                window.open (jQuery(this).attr("href"));
                return true;
            } else{
                jQuery(".gallery_policy").append("<div class='error-mes'>Terms of Use is required field</div>");
                return false;
            }
        });

        /* ======================
         Search Toggle
         ====================== */
        $('#block-search-form label').click(function(){
            // Calculate width of search input
            var secMenu = $('.global-menu').outerWidth();
            var navBarLink = $('.navbar-link').outerWidth();
            var inputPadding = $(this).next('.form-text').outerWidth() - $(this).next('.form-text').width();
            var total = secMenu + navBarLink + inputPadding;
            if ($(this).hasClass('open')) {
                $(this).removeClass('open').next('.form-text').hide();
            } else {
                $(this).addClass('open').next('.form-text').width(total).show();
            }
            $('#edit-search-block-form--2').width('auto');
            $('#edit-search-block-form--2').attr('size',30);
        });

        $('#edit-search-block-form--2').live("mouseup mousedown keyup keydown",function() {
            var strLength = $(this).val().length;
            if(strLength < 30) {
                $(this).attr('size',30);
            } else if(strLength < 64){
                $(this).attr('size',strLength);
            } else {
                $(this).attr('size',64);
            }
        });

        /* ======================
            Menus
            ====================== */
        $('#menu-toggle').sidr({
            name: 'sidr-left',
            side: 'right',
            source: '.global-menu, .primary-menu, #block-search-form, #nav-copy, #navbar-social'
        });
        $('#info-toggle').sidr({
            name: 'sidr-right',
            side: 'left',
            source: '#info'
        });

        /* ====================
         * NOTE: sidr doesn't have a callback that I know of.
         * ==================== */
        //change to css later
        $('#site-name').css('display','inline-block');
        $('.navbar-header').css('width','100%');
        $('#menu-toggle').css('float','right');

        $('#menu-toggle').click(function() {
            $(this).toggleClass('active');
            //if($('.navbar-header').css('position') != 'fixed'){
                $('.navbar-header').css('position','fixed');
                $('.navbar-header').css('z-index','10');
                $('.navbar-header').css('left','0px');
                $('.main-container').css('margin-top','185px');
                $('#sidr-left').css('margin-top','86px');
        });

        $(window).resize(function() {
            if($(window).width() > 909 && $('.navbar-header').css('left') == '0px'){
                $('.navbar-header').css('position','static');
                $('.main-container').css('margin-top','0px');
                $('.navbar-header').css('left','');
                $('#menu-toggle').removeClass('active');
            }
        });
        $('#info-toggle').click(function() {
            $(this).toggleClass('active');
        });

        /* ======================
             Research
             ====================== */
        if($('.view-research-by-category').length > 0) {
            function setPagerBtns(){
                $('.view-research-by-category .prev-btn').each(function() {
                    if($(this).parents('.view-research-by-category').find('.pager-previous a').length > 0) {
                        if($(this).hasClass('disabled')) {
                            $(this).removeClass('disabled');
                        }
                    } else if(!$(this).hasClass('disabled')) {
                        $(this).addClass('disabled');
                    }
                });
                $('.view-research-by-category .next-btn').each(function() {
                    if($(this).parents('.view-research-by-category').find('.pager-next a').length > 0) {
                        if($(this).hasClass('disabled')) {
                            $(this).removeClass('disabled');
                        }
                    } else if(!$(this).hasClass('disabled')) {
                        $(this).addClass('disabled');
                    }
                });
            }
            setPagerBtns();
            $( document ).ajaxComplete(function() {
                setPagerBtns();
            });
            $('#quicktabs-container-research_categories').on('click', '.view-research-by-category .prev-btn', function() {
                $(this).parents('.view-research-by-category').find('.pager-previous a').click();
            });

            $('#quicktabs-container-research_categories').on('click', '.view-research-by-category .next-btn', function() {
                $(this).parents('.view-research-by-category').find('.pager-next a').click();
            });

            $('.mobile').on('click', '.prev-btn', function() {
                $(this).parents('.mobile').find('.pager-previous a').click();
            });

            $('.mobile').on('click', '.next-btn', function() {
                $(this).parents('.mobile').find('.pager-next a').click();
            });

            $('.mobile').on('click', '.close-button', function() {
                $(this).parents('.quicktabs-tabpage').addClass('quicktabs-hide');
                $(this).parents('.mobile').find('ul.quicktabs-tabs li').each(function() {
                    $(this).removeClass('active');
                });
            });
        }

        /* ======================
             History
        ====================== */

             // Change default Our History slide

        var historyCopy = jQuery('#quicktabs-tabpage-our_history-0 .view-content .item-list li .field-content').html();
        jQuery('#quicktabs-tabpage-our_history-0 .splash-title').html("<strong>Our History</strong>").css({'margin-top':'0', 'text-align': 'left'}).after(historyCopy);
        jQuery('#quicktabs-tabpage-our_history-0 .image').css({'margin-left': '331px'});


        if($('.view-history').length > 0){

            function checkArrows() {

                if ($('#quicktabs-container-our_history .quicktabs-tabpage:not(.quicktabs-hide) .view-history .item-list .pager .pager-last a').length > 0) {
                    $('.quicktabs-tabpage:not(.quicktabs-hide)').find('.view-header .next-btn.next-page').css({
                        'background-position':'-15px -20px',
                        'height' :'39px'
                    });
                } else {
                    $('.quicktabs-tabpage:not(.quicktabs-hide)').find('.view-header .next-btn.next-page').css({
                        'background-position':'-15px -54px',
                        'height' :'34px'
                    });
                }
            }

            var page = 0;
            $('.view-history .next-btn.first-page').css('display', 'inline-block');
            $('.view-history .next-btn.next-page').css('display', 'none');
            $('.view-history .prev-btn.prev-page').css('display', 'inline-block');
            $('.view-history .prev-btn.front-page').css('display', 'none');
            $('.view-history .front-slide').css('display', 'block');
            $('.view-history .view-content').css('display', 'none');
            // hideDuplicateContent();

            $( document ).ajaxComplete(function() {
                checkArrows();
               if(page == 1){
                    $('.view-history .next-btn.first-page').css('display', 'none');
                    $('.view-history .next-btn.next-page').css('display', 'inline-block');
                    $('.view-history .prev-btn.prev-page').css('display', 'none');
                    $('.view-history .prev-btn.front-page').css('display', 'inline-block');
                }
                if(page == 0){
                    $('.view-history .next-btn.first-page').css('display', 'inline-block');
                    $('.view-history .next-btn.next-page').css('display', 'none');
                    $('.view-history .prev-btn.prev-page').css('display', 'inline-block');
                    $('.view-history .prev-btn.front-page').css('display', 'none');
                    $('.view-history .front-slide').css('display', 'block');
                    $('.view-history .view-content').css('display', 'none');
                }
            });

            // Prev Btn Click
            $('#quicktabs-container-our_history').on('click', '.view-history .prev-btn.prev-page', function() {
                page--;
                $('.quicktabs-tabpage:not(.quicktabs-hide) .view-history .pager-previous a').click();
            });

            //Next Btn Click
            $('#quicktabs-container-our_history').on('click', '.view-history .next-btn.next-page', function() {
                if($('.quicktabs-tabpage:not(.quicktabs-hide) .view-history .pager-next').length > 0){

                    $('.quicktabs-tabpage:not(.quicktabs-hide) .view-history .pager-next a').click();
                    page++;
                    console.log(page);
                }
            });

            // Front Page Btn Click
            $('#quicktabs-container-our_history').on('click', '.view-history .prev-btn.front-page', function() {
                $('.view-history .next-btn.first-page').css('display', 'inline-block');
                $('.view-history .next-btn.next-page').css('display', 'none');
                $('.view-history .prev-btn.prev-page').css('display', 'inline-block');
                $('.view-history .prev-btn.front-page').css('display', 'none');
                $('.view-history .front-slide').css('display', 'block');
                $('.view-history .view-content').css('display', 'none');
                page--;
                console.log(page);
            });

            // First Page click
            $('#quicktabs-container-our_history').on('click', '.view-history .next-btn.first-page', function() {
                $('.view-history .next-btn.first-page').css('display', 'none');
                $('.view-history .next-btn.next-page').css('display', 'inline-block');
                $('.view-history .prev-btn.prev-page').css('display', 'none');
                $('.view-history .prev-btn.front-page').css('display', 'inline-block');
                $('.view-history .front-slide').css('display', 'none');
                $('.view-history .view-content').css('display', 'block');
                page++;
                console.log(page);
            });

            // Tab Click
            $('.quicktabs-tabs li:not(.active) a').click(function(){
                    page=0;
                    console.log(page);
                    $('.quicktabs-tabpage .view-history .pager-first a').click();
                    $('.quicktabs-tabpage .view-history .front-slide').css('display', 'block');
                    $('.quicktabs-tabpage .view-history .view-content').css('display', 'none');
                    $('.view-history .next-btn.first-page').css('display', 'inline-block');
                    $('.view-history .next-btn.next-page').css('display', 'none');
                    $('.view-history .prev-btn.prev-page').css('display', 'inline-block');
                    $('.view-history .prev-btn.front-page').css('display', 'none');
            });

            var decadeIndex = 0;

            $('.mobile-prev').click(function() {
                if(decadeIndex > 0){
                    decadeIndex--;
                    $('.view-history.view-display-id-block_2 .view-content .view-grouping').css('display','none');
                    $('.view-history.view-display-id-block_2 .view-content .view-grouping').eq(decadeIndex).css('display', 'block');
                }
            });

            $('.mobile-next').click(function() {
                if(decadeIndex < $('.view-history.view-display-id-block_2 .view-content .view-grouping').length - 1){
                    decadeIndex++;
                    console.log(decadeIndex);
                    $('.view-history.view-display-id-block_2 .view-content .view-grouping').css('display','none');
                    console.log($('.view-history.view-display-id-block_2 .view-content .view-grouping').get(decadeIndex));
                    $('.view-history.view-display-id-block_2 .view-content .view-grouping').eq(decadeIndex).css('display', 'block');
                }
            });

        }

        /* ======================
             Press Release
             ====================== */
        if($('.view-newsroom-press-releases').length > 0){
            $('select#edit-field-press-release-category-tid option').each(function() {
                if($(this).attr('selected') == 'selected' && $(this).val() != 'All'){
                    $('.view-newsroom-press-releases .views-field-field-press-release-category img').css('display', 'none');
                    $('.views-row-1 .header-image-placeholder').html('<img src="Unknown_83_filename"/*tpa=http://www.cvshealth.com/sites/all/themes/cvs/img/press-release-tid-%27 + $(this).val() + %27.jpg*//>');
                    $('.view-newsroom-press-releases .views-field-field-press-release-category').css('float', 'none');
                    $('.view-newsroom-press-releases .press-release-header.filter').css('display', 'block');
                    $('.view-newsroom-press-releases .press-release-header.no-filter').css('display', 'none');
                }
            });


            $('.view-newsroom-press-releases #edit-title-wrapper #edit-title').attr('placeholder','Enter Keywords (optional)');

            $('#edit-field-press-release-category-tid').parent().append('<div class="select-header">Choose A Type</div><ul id="category-selector" name="category-selector"></ul>');
            $('#edit-field-press-release-category-tid option').each(function(){
                $('#category-selector').append('<li value="' + $(this).val() + '">'+$(this).text()+'</li>');
            });
            var nav = $('.select-header');
            var selection = $('#category-selector');
            var select = selection.find('li');
            var loadSelection = $('#edit-field-press-release-category-tid').val();

            select.each(function(){
                if($(this).val() == loadSelection){
                    $(this).addClass('active');
                    nav.text($(this).text());
                }
            });

            nav.focusout(function() {
                //alert('blur');
                nav.removeClass('active');
                selection.stop().slideUp(200);
            });

            nav.click(function(event) {
                if (nav.hasClass('active')) {
                    nav.removeClass('active');
                    selection.stop().slideUp(200);
                } else {
                    nav.addClass('active');
                    selection.stop().slideDown(200);
                }
                event.preventDefault();
            });

            select.click(function(event) {
                // updated code to select the current language
                select.removeClass('active');
                $(this).addClass('active');
                nav.removeClass('active');
                nav.text($(this).text());
                selection.stop().slideUp(200);
                var taxVal = $(this).val();
                $('#edit-field-press-release-category-tid').val($(this).val());
            });


        }

        /* ======================
             Press Release
             ====================== */
        if($('#quicktabs-view__banner_video__default').length > 0){
            $('#quicktabs-view__banner_video__default .quicktabs-tabs a').click(function() {
                $("iframe").each(function() {
                    if($(this).parents('.quicktabs-tabpage').hasClass('quicktabs-hide')){
                        var src= $(this).attr('src');
                        $(this).attr('src',src);
                    }
                });
            });
        }

        /* ======================
             See Also
             ====================== */
        if ($('.relatedPage').length > 0) {

            // Wrap links with .relatedPage with .see-also markup
            var text = $('.relatedPage').text();
            var link = $('.relatedPage').attr('href');
            var target = $('.relatedPage').attr('target');
            var markup = '  <div class="see-also">\
                                                <div class="heading left">See Also</div>\
                                                <div class="content right">\
                                                    <a href="'+link+'" class="rArrow" target="'+target+'">'+text+'</a>\
                                                </div>\
                                            </div>';
            $('.relatedPage').replaceWith(markup);

            // Create Pop Up Widget if window is larger than 768
            if ($(window).width() > 768) {
                var widgetMarkup = '<div class="see-also widget" style="position: absolute; top: 6147px; right: 0px;">\
                                                            <a class="close" href="#">X</a>\
                                                            <div class="heading left">See Also</div>\
                                                            <div class="content right">\
                                                                <a href="'+link+'" class="rArrow" target="'+target+'">'+text+'</a>\
                                                            </div>\
                                                        </div>';
                $('.main-container').after(widgetMarkup);


                if($('.main-container aside').length > 0){
                    var $aside = $('.main-container aside');
                    var $top = $aside.offset().top + $aside.outerHeight(true) - $('.see-also.widget .close').outerHeight(true);
                    //console.log($top);
                    var $widget = $('.see-also.widget');
                    $widget.css({
                        position:'absolute',
                        top: Math.ceil($top)+'px',
                        right: -($widget.outerWidth(true))
                    });
                }
            };


            // handle scroll
            // NOTE: this will change when it's in drupal. this is just for show. it's all fake.
            $(window).scroll(function() {
                var $pos = $(window).scrollTop();
                if($pos + 500 >= $top) {
                    $widget.animate({right: 0}, 500);
                }
            });

            // of course this would be where the code for it is when it does get loaded
            // so ignore the simpleness
            $('.see-also.widget .close').click(function($evt) {
                $evt.preventDefault();
                $(this).parent().fadeOut('slow', function() {
                    $(this).remove();
                });
            });

        }

        /* ===================================
         Newsroom-filter-menu
         =================================== */
        $('.accordion .toggle').click(function(e) {
            e.preventDefault();
            var $this = $(this);
            $this.parents('.accordion').find('.content').slideToggle('fast', function(){
                if ($this.hasClass('expanded')) {
                    $this.removeClass('expanded').addClass('collapsed');
                } else if ($this.hasClass('collapsed')) {
                    $this.removeClass('collapsed').addClass('expanded');
                }
            });
        });

        /* ======================
         Google Analytics Event Tracking
         ====================== */

        // Scroll Depth Tracking
        $.scrollDepth();

        // Global Menu Investors link
        trackEventHelper('#investors-link', 'Investors', 'External-Link', 'Click');

        // Search Jobs Button on Careers page
        trackEventHelper('#search-jobs-link', 'Careers', 'Jobs', 'Search');

        // Clicking refine exposed filters
        $('.cvs-exposed-form .form-type-bef-checkbox').click(function(e) {
            var label = $(this).find('label.helper').text();
            var category =  $('h1.page-title').text();
            _gaq.push(['_trackEvent', category, 'Refine Theme', label]);
        });

        // --------------------- //
        // Takeover tracking
        // --------------------- //

        //Close Btn
        trackEventHelper('.takeover .header a.close','Takeover','click','Takeover - Continue to Full Site');

        //Title Copy
        trackEventHelper('.takeover .intro h1 a','Takeover','click','Takeover - Banner Copy');

        //Social Media Buttons
        trackEventHelper('.takeover .social-wrapper a.facebook','Takeover','click','Takeover - Facebook');
        trackEventHelper('.takeover .social-wrapper a.twitter','Takeover','click','Takeover - Twitter');
        trackEventHelper('.takeover .social-wrapper a.linkedin','Takeover','click','Takeover - LinkedIn');
        trackEventHelper('.takeover .social-wrapper a.youtube','Takeover','click','Takeover - YouTube');

        //Icons
        trackEventHelper('.takeover .main-content .icons #icon2','Takeover','click','Takeover - Icon - Health is Everything');
        trackEventHelper('.takeover .main-content .icons #icon1','Takeover','click','Takeover - Icon - We\'re Tobacco Free');
        trackEventHelper('.takeover .main-content .icons #icon3','Takeover','click','Takeover - Icon - Let\'s Quit Together');
        trackEventHelper('.takeover .main-content .icons #icon4','Takeover','click','Takeover - Icon - Research');

        //Learn More Buttons
        trackEventHelper('.takeover .section-content .icon2 a','Takeover','click','Takeover - Health is Everything - Learn More');

        trackEventHelper('.takeover .section-content .icon1 a','Takeover','click','Takeover - We\'re Tobacco Free - Learn More');

        trackEventHelper('.takeover .section-content .icon3 a','Takeover','click','Takeover - Let\'s Quit Together - Learn More');

        trackEventHelper('.takeover .section-content .icon4 a','Takeover','click','Takeover - Research - Learn More');

        //Article Buttons
        trackEventHelper('.takeover .tri-article .our-story a.imag','Takeover','click','Takeover - Articles - Our Story Mosaic');
         trackEventHelper('.takeover .tri-article .our-story a.titl','Takeover','click','Takeover - Articles - Our Story Title');
         trackEventHelper('.takeover .tri-article .our-name a.imag','Takeover','click','Takeover - Articles - Our Name Has Changed Mosaic');
         trackEventHelper('.takeover .tri-article .our-name a.titl','Takeover','click','Takeover - Articles - Our Name Has Changed Title');
         trackEventHelper('.takeover .tri-article .last a.imag','Takeover','click','Takeover - Articles - #Onegoodreason Mosaic');
         trackEventHelper('.takeover .tri-article .last a.titl','Takeover','click','Takeover - Articles - #Onegoodreason Title');

        $.address.tracker(null); // Prevent double tracking

        /**
         * NOTE: FB & Twitter tracking are in the fbload event.
         */

        /* ================================================
             Window Resize
             ================================================ */

        var time = false;
        $(window).resize(function() {

            if(time !== false){
                clearTimeout(time);
            }
            time = setTimeout(doneResize, 200);

            // Timeout is needed so the doneResize() is only fired once when the browser stop resizing.
            function doneResize(){

                // Split sort-menu & filter-menu for mobile
                splitDoubleMenu();

                // Initiate sticky sort-menu & filter-menu if it is visible
                stickyMenu();

                // Reset sort-menu & filter-menu border
                resetMenuBorder();

                // Since the margins change on carousel items, adjust FlexSlider margins
                flexsliderFix();


                // Only toggle sidr if below 820
                if (Modernizr.mq('only screen and (min-width: 910px)')) {
                    // Close slidr above 820
                    $.sidr('close', 'sidr-left');
                    $.sidr('close', 'sidr-right');
                }

                // check footer
                //$footer_check();
                //$fixed_check();


                // Split-columns
                splitCol();

                // ie7 layout fix
                layoutFix();

                // Footer links
                formatFooterLinks();
            }
        });

    });

    /* ================================================
         Window Load
         ================================================ */
    $(window).load(function(){

        // scroll to
        $('#info-toggle').click(function($evt) {
            $evt.preventDefault();
            if($(window).width() <= 820) { return; }
            var $target = this.href.split('#')[1];
            var $top = $('#'+$target).offset().top;
            $('html,body').animate({scrollTop: $top}, 1500);
        });

        // fix pagination
        $('.carousel-control-paging').each(function() {
            var $this = $(this);
            var $carousel = $this.closest('.view');
            if($carousel.length) {
                $this.css('top', Math.ceil($carousel.height()-($this.height()/2))+'px');
            }
        });

        // Since the margins change on carousel items, adjust FlexSlider margins
        flexsliderFix();

        // Split sort-menu & filter-menu for mobile
        splitDoubleMenu();

        // Initiate sticky sort-menu & filter-menu if it is visible
        stickyMenu();

        // Split-columns
        splitCol();

        // Sort-menu & Filter-menu (radio & checkboxes menu)
        collapsiableMenu();

        // jump-menu-holder
        setJumpMenu();

        // ie7 layout fix
        layoutFix();

        // Footer links
        getFooterTxt();
        footerLinks();
        formatFooterLinks();

    });


    /* ================================================
         Functions
         ================================================ */

    /* =========================
            stickyMenu()
        ========================= */
    function stickyMenu(){
        //If there is any sort-menu or filter-menu and not press-release filter, add Stickem
        // stickem will not work on IE8 & 7
        if($('.views-exposed-widget .form-checkboxes').length > 0
         || $('.views-exposed-widget .form-radios').length > 0
         && !$('#edit-field-press-release-category-tid-wrapper').length > 0){

        $('body').find('.region-content').addClass('stickem-container');
        $('.views-exposed-widget').addClass('stickem');
         } else {
         //console.log('no stickem ');
         }

        // jump-menu sticky
        if($('.jump-menu-holder').length > 0) {
            $('body').find('.main-container').addClass('stickem-container');
            $('.jump-menu-holder').addClass('stickem');
        }

        // for mobile, the sticky menu won't be initialized if the menu height is larger than the browser height
        if( Modernizr.mq('only all and (max-width: 767px)')){
            var windowH = $(window).height();
            var sortMenuH = $('.views-exposed-widget .form-radios').parents('.views-exposed-widget').height();
            var filterMenuH = $('.views-exposed-widget .form-checkboxes').parents('.views-exposed-widget').height();

            if( sortMenuH < windowH && filterMenuH < windowH) {
                $('.main-container').stickem();
            }else {
                $('.views-exposed-widget').removeClass('stickit').addClass('stickit-end');
                var sticky = $('.main-container').stickem();
                sticky.destroy();
            }
            menuTitleColorSwap($('.views-exposed-widget .form-radios'));
            menuTitleColorSwap($('.views-exposed-widget .form-checkboxes'));
            menuTitleColorSwap($('.views-exposed-widget .has-pretty-child'));

        }else if(Modernizr.mq('only all and (min-width: 768px)')){
            //desktop
            $('.views-exposed-widget').removeClass('stickit').addClass('stickit-end');
            $('.main-container').stickem().destroy();

            // Make sure the sort-menu & filter-menu are expanded when the browser window is resized from mobile to desktop.
            $('.views-exposed-widgets .form-radios, .views-exposed-widgets .form-checkboxes, .views-exposed-widgets .has-pretty-child').show();
            menuTitleColorSwap($('.views-exposed-widget .form-radios'));
            menuTitleColorSwap($('.views-exposed-widget .form-checkboxes'));
            menuTitleColorSwap($('.views-exposed-widget .has-pretty-child'));
        }else {
            // lt-ie9
            //console.log('no stickem!');
        }

        // jump-menu
        // the sticky menu won't be initialized if the menu height is larger than the browser height
        if( Modernizr.mq('only all and (max-width: 767px)') && $('.jump-menu').is(':visible')){
            var windowH = $(window).height();
            var jumpMenuH = $('.jumb-menu-holder').height();

            if( jumpMenuH < windowH ){
                $('.main-container').stickem();
            }else {
                $('.jump-menu-holder').removeClass('stickit').addClass('stickit-end');
                var sticky = $('.main-container').stickem();
                sticky.destroy();
            }
        }else if(Modernizr.mq('only all and (min-width: 768px)')){
            //desktop
            $('.jump-menu-holder').removeClass('stickit').addClass('stickit-end');
            $('.main-container').stickem().destroy();

            // Make sure the jump-menu is expanded when the browser window is resized from mobile to desktop
            $('.jump-menu-holder .jump-menu').show();
            if($('.jump-menu').is(':visible')){
                $('.jump-menu-holder h2').css('background-color', '#ebebeb').addClass('expanded').removeClass('collapsed');
            }else {
                $('.jump-menu-holder h2').css('background-color', '#ffffff').addClass('collapsed').removeClass('expanded');
            }
        }else {
            // lt-ie9
        }
    }

    /* ==================================================
     * menuTitleColorSwap() : swaps the title background color.
     * ================================================== */
    function menuTitleColorSwap($this){
        if($this.is(':visible')) {
            $this.parents('.views-exposed-widget').find('label:first-child').css('background-color', '#ebebeb').addClass('expanded').removeClass('collapsed');
        } else {
            $this.parents('.views-exposed-widget').find('label:first-child').css('background-color', '#ffffff').addClass('collapsed').removeClass('expanded');
        }
    }

    /* ======================================
            Sort-menu & Filter-menu + Jump-menu
         ====================================== */
    function collapsiableMenu(){
        // hover event
        $('.views-exposed-widget label:first-child').hover(
        function(e){
            if($('body .helper-small-screen').is(':visible')){
            $(this).css('background-color', '#ebebeb');
            }
        }, function(e) {
            if($('body .helper-small-screen').is(':visible')){
            // form-radios
            if( $(this).parent().children('.form-radios').is(':hidden')){
                $(this).css('background-color', '#ffffff');
            }
            // checkboxes
            if( $(this).parent().find('.form-checkboxes').is(':hidden')){
                $(this).css('background-color', '#ffffff');
            }
            }
        }
        );

        // click event
        $('.views-exposed-widget label:first-child').click(function(e){

            //mobile
            if($('body .helper-small-screen').is(':visible')){
                // form-radios
                if( $(this).parent().children('.form-radios').length > 0){
                $(this).parent().find('.form-radios').slideToggle('fast', function(){
                    menuTitleColorSwap($('.views-exposed-widget .form-radios'));
                    addMenuBorder($(this), $('.views-exposed-widget .form-radios'));
                });
                }
                // checkboxes
                if( $(this).parent().find('.form-checkboxes').length > 0){
                var $thisChildren = $(this).parent().find('.form-checkboxes');
                $thisChildren.slideToggle('fast', function(){
                    menuTitleColorSwap($thisChildren);
                    addMenuBorder($(this), $thisChildren);
                });
                }
            }

            // addMenuBorder()
            function addMenuBorder($this, $thisChildren){
                var index = $this.parents('.views-exposed-widget').index();
                var $thisWidget = $this.parents('.views-exposed-widget');

                // EXPANDED : small-screen with combine-menu && when menu is EXPANDED : add border to content, remove label border
                if($('body .helper-small-screen').is(':visible') && $('.views-exposed-widget').hasClass('combine-menu') && $thisChildren.is(':visible')){
                    // remove label border-right
                    $('.views-exposed-widget:visible').find('label:first-child').css('border-right', '0');

                    // add border to the opened menu
                    if(index == 0){
                        // if the other menu is opened
                        if($('.views-exposed-widget:visible').eq('1').find('.form-checkboxes').is(':visible') ||
                            $('.views-exposed-widget:visible').eq('1').find('.form-radios').is(':visible')){
                            //console.log('the other one is opened');
                            compareMenuHeight();
                        }else {
                            // remove label border-right
                            $('.views-exposed-widget:visible').find('label:first-child').css('border-right', '0');
                            $thisWidget.css('border-right', '1px #bfbfbf solid');
                        }
                    }
                    if(index == 1){
                        // if the other menu is opened
                        if($('.views-exposed-widget:visible').eq('0').find('.form-checkboxes').is(':visible') ||
                            $('.views-exposed-widget:visible').eq('0').find('.form-radios').is(':visible')){
                            //console.log('the other one is opened');
                            compareMenuHeight();
                        }else {
                            // remove label border-right
                            $('.views-exposed-widget:visible').find('label:first-child').css('border-right', '0');
                            $thisWidget.css('border-left', '1px #bfbfbf solid');
                        }
                    }
                }

                // COLLAPSED : small-screen with combine-menu && menu is COLLAPSED : remove content border, add label border
                if($('body .helper-small-screen').is(':visible') && $('.views-exposed-widget').hasClass('combine-menu') && $thisChildren.is(':hidden')){
                    // TODO if there is another combine-menu opened, don't add label border.

                    $thisWidget.css({
                        'border-right': '0',
                        'border-left': '0'
                    });

                    // TODO add border to the other opened combine-menu if it's still opened without the border
                    if(index == 0){
                        // if the other menu is opened and it doesn't have border, add border to the remaining opened menu
                        if($('.views-exposed-widget:visible').eq('1').find('.form-checkboxes').is(':visible') ||
                            $('.views-exposed-widget:visible').eq('1').find('.form-radios').is(':visible')){
                            //console.log('menu1 is still opened');

                            var menu1border = parseInt($('.views-exposed-widget:visible').eq('1').css('border-left'),10);
                            if(menu1border == 0){
                                //console.log('no border');
                                $('.views-exposed-widget:visible').eq('1').css('border-left', '1px #bfbfbf solid');
                            }else{
                                //console.log('has border');
                            }
                        }else {
                            //console.log('all closed');
                            $('.views-exposed-widget:visible').find('label:first-child').css('border-right', '1px #bfbfbf solid');
                        }
                    }
                    if(index == 1){
                        // if the other menu is opened and it doesn't have border, add border to the remaining opened menu
                        if($('.views-exposed-widget:visible').eq('0').find('.form-checkboxes').is(':visible') ||
                            $('.views-exposed-widget:visible').eq('0').find('.form-radios').is(':visible')){
                            //console.log('menu0 is still opened');

                            var menu0border = parseInt($('.views-exposed-widget:visible').eq('0').css('border-right'),10);
                            if(menu0border == 0){
                                //console.log('no border');
                                $('.views-exposed-widget:visible').eq('0').css('border-right', '1px #bfbfbf solid');
                            }else{
                                //console.log('has border');
                            }
                            //$('.views-exposed-widget:visible').eq('0').css('border-right', '1px #bfbfbf solid');
                        }else {
                            //console.log('all closed');
                            //$thisWidget.css('border-left', '1px #bfbfbf solid');
                            $('.views-exposed-widget:visible').find('label:first-child').css('border-right', '1px #bfbfbf solid');
                        }
                    }

                }
            }
        });

        // form-radios : slideToggle form-radios content when the children div is clicked
        $('.views-exposed-widget .form-item label').click(function(e){
            if($('body .helper-small-screen').is(':visible')){
                // form-radios
                if($(this).parents('.form-radios').length > 0 ) {
                    $(this).parents('.form-radios').slideToggle('fast', function(){
                        menuTitleColorSwap($('.views-exposed-widget .form-radios'));
                    });
                }
            }
        });

        // jump-menu : hover event
        $('.jump-menu-holder h2').hover(
            function(e){
                if($('body .helper-small-screen').is(':visible')){
                    $(this).css('background-color', '#ebebeb');
                }
            }, function(e) {
                if($('body .helper-small-screen').is(':visible')){
                    if( $(this).parent('.jump-menu-holder').find('.jump-menu').is(':hidden')){
                        $(this).css('background-color', '#ffffff');
                    }
                }
            }
        );

        // jump-mneu : click event
        $('.jump-menu-holder h2').click(function(e){
            if($('body .helper-small-screen').is(':visible')){
                if($(this).parent('.jump-menu-holder').find('.jump-menu').length > 0 ) {
                    $(this).parent('.jump-menu-holder').find('.jump-menu').slideToggle('fast', function(){
                        if($('.jump-menu').is(':visible')){
                            $('.jump-menu-holder h2').css('background-color', '#ebebeb').addClass('expanded').removeClass('collapsed');
                        }else {
                            $('.jump-menu-holder h2').css('background-color', '#ffffff').addClass('collapsed').removeClass('expanded');
                        }
                    });
                }
            }
        });
        // jump-menu : slideToggle jump-menu-holder content when the children div is clicked
        $('.jump-menu li').click(function(e){
            if($('body .helper-small-screen').is(':visible')){
                if($(this).parent('.jump-menu').length > 0 ) {
                    $(this).parent('.jump-menu').slideToggle('fast', function(){
                        if($('.jump-menu').is(':visible')){
                            $('.jump-menu-holder h2').css('background-color', '#ebebeb').addClass('expanded').removeClass('collapsed');
                        }else {
                            $('.jump-menu-holder h2').css('background-color', '#ffffff').addClass('collapsed').removeClass('expanded');
                        }
                    });
                }
            }
        });
    }


    /* ==============================================
        Sort-menu & Filter-menu : splitDoubleMenu()
         ============================================== */
    function splitDoubleMenu(){
        // See if the page has .js-hide and hide its parent if its parent is .views-exposed-widget
        if($('.js-hide').length > 0){
            $('.js-hide').parent('.views-exposed-widget').css('display', 'none');
        }
        if(($('.views-exposed-widget:visible').length === 2)){
            if($('body .helper-small-screen').is(':visible')){
                // Add .combine-menu class to .views-exposed-widget
                $('.views-exposed-widget').addClass('combine-menu').removeClass('combine-menu-large-screen');
            } else {
                $('.views-exposed-widget').removeClass('combine-menu').addClass('combine-menu-large-screen');
            }
        }
    }

    /* ==================================================
     * compareMenuHeight() : when 2 combine-menu is opened, compare each height and add border to the menu which height is higher.
     * ================================================== */
    function compareMenuHeight(){
        // first menu
        var firstMenuCheckboxesH = $('.views-exposed-widget:visible').eq('0').find(".form-checkboxes").height();
        var firstMenuRadioH = $('.views-exposed-widget:visible').eq('0').find(".form-radios").height();
        // second menu
        var secondMenuCheckboxesH = $('.views-exposed-widget:visible').eq('1').find(".form-checkboxes").height();
        var secondMenuRadioH = $('.views-exposed-widget:visible').eq('1').find(".form-radios").height();
        // if 1st menu height > 2nd menu height, remove label border, add border to the menu which height is higher
        if((firstMenuCheckboxesH || firstMenuRadioH) > (secondMenuCheckboxesH || secondMenuRadioH)){
            //console.log('1st > 2nd ');
            $('.views-exposed-widget:visible').eq('0').find('label').css('border-right', '0');
            $('.views-exposed-widget:visible').eq('1').css('border-left', '0');
            $('.views-exposed-widget:visible').eq('0').css('border-right', '1px #bfbfbf solid');
        } else if((firstMenuCheckboxesH || firstMenuRadioH) < (secondMenuCheckboxesH || secondMenuRadioH)){
            // if 2nd menu height > 1st menu height
            //console.log('2nd > 1st ');
            $('.views-exposed-widget:visible').eq('0').find('label').css('border-right', '0');
            $('.views-exposed-widget:visible').eq('0').css('border-right', '0');
            $('.views-exposed-widget:visible').eq('1').css('border-left', '1px #bfbfbf solid');
        } else {
            /** else **/
        }
    }

    /* ==============================================
        Sort-menu & Filter-menu : resetMenuBorder()
         ============================================== */
    function resetMenuBorder(){
        // desktop: when .combine-menu-large-screen exists, add and remove corresponding border to both menu
        if($('.views-exposed-widget:visible').hasClass('combine-menu-large-screen')){
            $('.views-exposed-widget:visible').find('label').css('border-right', '0');
            $('.views-exposed-widget:visible').css('border', '1px #bfbfbf solid');
            $('.views-exposed-widget:visible').eq(0).css('border-bottom', '0');
        }
        // mobile, when .combine-menu exists, add and remove corresponding border both menu
        if($('.views-exposed-widget:visible').hasClass('combine-menu')){
            $('.views-exposed-widget:visible').css('border', '0');
            compareMenuHeight();
        }
    }

    /* =========================
        footer : footerLinks()
        ========================= */
    function getFooterTxt(){
        txt1 =  $('.linksMenu .col-2 h2').eq(0).text();
        txt2 =  $('.linksMenu .col-2 h2').eq(1).text();
    }
    /* ==================================================
     * footerLinks() : adds click to footer links.
     * ================================================== */
    function footerLinks(){
        $('.linksMenu .col-2 h2').click(function(e){
        var $this = $(this);
        var $siblings = $this.parents('.col-2').siblings();
        e.preventDefault();

        if($('body .helper-small-screen').is(':visible')){
            // Toggle sub-content
            $this.siblings('.content').slideToggle('fast', function(){
            if($this.parents('.block-menu').hasClass('collapsed')){
                $this.parents('.block-menu').removeClass('collapsed').addClass('expanded');
            }else if($this.parents('.block-menu').hasClass('expanded')) {
                $this.parents('.block-menu').removeClass('expanded').addClass('collapsed');
            } else {
            $this.parents('.block-menu').addClass('expanded');
            }
            });

            // Have siblings content slideUp when another .linksMenu h2 is clicked
            $siblings.find('.block-menu .content').slideUp('fast', function(){
            $siblings.removeClass('expanded').addClass('collapsed');
            // special case for .company and .privacy
            $siblings.find('.privacy').removeClass('reposition');
            });
        }
        });


    }

    /* ==================================================
     * formatFooterLinks : formats the footer links.
     * ================================================== */
    function formatFooterLinks(){

        /* footer & linksMenu (split-columns) */
        // Make sure footer sub-content is collapsed in mobile and
        // all expanded in desktop when window resizes.

        // only run this if it's not .lt-ie9
        if(!($('html.lt-ie9').length > 0)){
            if( $('body .helper-small-screen').is(':visible')){
            } else {
                $('.linksMenu .content').css('display', 'block');
            }
        }

        // make sure all footerlinks .col has a .collapsed class when the subContent is hidden
        if($('.linksMenu .col .content').is(':hidden')){
            $('.linksMenu .col').addClass('collapsed clearfix').removeClass('expanded');
        } else {
            $('.linksMenu .col').addClass('expanded').removeClass('collapsed clearfix');
        }
        //
        // Combine col-2 two h2 names as one for mobile
        if($('body .helper-small-screen').is(':visible') && $('.linksMenu .col-2 h2').length === 2){
            $('.linksMenu .col-2 h2').eq(0).html(txt1 + ' & ' + txt2);
        } else if($('body .helper-small-screen').is(':hidden') && $('.linksMenu .col-2 h2').length === 2){
            $('.linksMenu .col-2 h2').eq(0).html(txt1);
            // make sure .privacy has the correct margin-top
            $('.linksMenu .privacy').removeClass('reposition');
        } else {
            /** else **/
        }
    }

    /* =========================
            toogleContent()
        ========================= */
    //toggle sub-content
    function toggleContent($this){
        $this.find('ul').slideToggle('fast', function(){
            if($this.hasClass('collapsed')){
                $this.removeClass('collapsed').addClass('expanded');
            } else {
                $this.removeClass('expanded').addClass('collapsed');
            }
        });
    }

    /* =======================
        Split-columns
         ======================= */
    /* .split-columns menu */
    function splitCol(){
        if ($('.split-columns').length > 0) {
        //if less then ie9 or <767px
        if($('html').hasClass('lt-ie9') || Modernizr.mq('only all and (max-width: 767px)')){
            $(".split-columns").columnGenerator(1);
        }else if (Modernizr.mq('only all and (max-width: 1023px)')){
            $(".split-columns").columnGenerator(3);
        }else if (Modernizr.mq('only all and (min-width: 1024px)')){
            $(".split-columns").columnGenerator(4);
        }else {
            // .lt-ie9
        }

        // TODO: can modify split-columns markup, make it looks more like the footer rather than the submenu-nav and combine the following code with #footer-links code

        // click
        $('.split-columns ul>li.title').click(function(e){
            e.preventDefault();
            var $this = $(this);
            var $siblingsUL = $this.siblings().find('ul');

            if($('html').hasClass('lt-ie9') || Modernizr.mq('only all and (max-width: 767px)')){
            //toggle sub-content
            toggleContent($this);

            // have siblings li slideUp when another li is clicked
            $siblingsUL.slideUp('fast', function(){
                $siblingsUL.parent('.title').removeClass('expanded').addClass('collapsed');
            });
            }
        });
        }
    }

    /* =======================================================================================
            setJumpMenu() : add .collapsed or .expanded class depends if the menu is opened or not
         ======================================================================================= */
    function setJumpMenu(){
        if($('.jump-menu-holder').length > 0 ){
            if($('.jump-menu').is(':visible')){
                $('.jump-menu-holder .pane-title').addClass('expanded').removeClass('collapsed');
            }
            if($('.jump-menu').is(':hidden')){
                $('.jump-menu-holder .pane-title').addClass('collapsed').removeClass('expanded');
            }
        }
    }

    /* ==================================================
     IE7 layout fix (temp until better solution is found)
    ===================================================== */
    function layoutFix(){
         /* change markup to make it work */
        if(($('.lt-ie8 .front').length > 0) || $('.lt-ie8 .template-1-3').length > 0) {
            if($('.helper-large-screen').is(':visible')){
                $('.views-exposed-widgets').parents('.wrapper.sub').removeClass('wrapper sub').addClass('float-left a-first');
                $('.a-first').next('.main.wrapper').removeClass('wrapper').addClass('float-left template-1-3-content a-second');
                $('.a-first, .a-second').wrapAll('<div class="wrapper sub"></div>')
            } else {
                /** else **/
            }
        }
    }

    /* ==================================================
     * callDefinitions() :
     * ================================================== */

    function callDefinitions(obj) {
        jQuery(".view-definitions-page .view-content a").removeClass('active');
        jQuery(obj).addClass("active");
        jQuery.ajax({
            type: "POST",
            url: "/definitions",
            data: "title="+jQuery(obj).attr("title"),
            success: function(mes){
                jQuery(".definitions-content").html(mes);
                var window_width_rs = jQuery(window).width();
                if(window_width_rs > 319 && window_width_rs < 768){
                    jQuery('html, body').animate({
                    scrollTop: jQuery(".definitions").offset().top
                    }, 1000);
                }
            }
        });
    }

    /**
     * Flexslider Functions.
     */

    /* ==================================================
     * Fixes flexslider columns.
     ===================================================== */
    function flexsliderFix() {
        // Since the margins change on carousel items, adjust FlexSlider margins
        if ($('.grid_3 .flexslider, .grid_4 .flexslider').length > 0){
            if (Modernizr.mq('only screen and (min-width: 480px) and (max-width: 768px)')) {
                $('.grid_3 .flexslider, .grid_4 .flexslider').data('flexslider').setOpts({itemMargin: 12});
            } else {
                $('.grid_3 .flexslider, .grid_4 .flexslider').data('flexslider').setOpts({itemMargin: 20});
            }
        }
    }

    /**
     * Analytics Functions.
     */

    /* ======================
     Track Event Helper
     ====================== */
    function trackEventHelper(target,category,action,label){
        $(target).click(function(){
            _gaq.push(['_trackEvent', category, action, label]);
        });
    };

    /* ======================
     * Custom fbload event to handle when FB is ready.
     * ====================== */
    $(document).on('fbload', function(){

        // Facebook Likes
        FB.Event.subscribe('edge.create', function(targetUrl) {
            _gaq.push(['_trackSocial', 'facebook', 'like', targetUrl]);
        });

        // Facebook Shares
        FB.Event.subscribe('http://www.cvshealth.com/sites/all/themes/cvs/js/message.send', function(targetUrl) {
            _gaq.push(['_trackSocial', 'facebook', 'send', targetUrl]);
        });

        FB.Event.subscribe('edge.remove', function(targetUrl) {
            _gaq.push(['_trackSocial', 'facebook', 'unlike', targetUrl]);
        });

    });

    $(window).load(function() {
        /* ======================
         * trackTweet : tracks twitter.
         * ====================== */

        //Wrap event bindings - Wait for async js to load
        /*twttr.ready(function (twttr) {
            //event bindings
            twttr.events.bind('tweet', trackTwitter);
        });*/

        function trackTwitter(intent_event) {
            if (intent_event) {
                var opt_pagePath;
                if (intent_event.target && intent_event.target.nodeName == 'IFRAME') {
                            opt_target = extractParamFromUri(intent_event.target.src, 'url');
                }
                _gaq.push(['_trackSocial', 'twitter', 'tweet', opt_pagePath]);
            }
        }

        function extractParamFromUri(uri, paramName) {
          if (!uri) {
            return;
          }
          var regex = new RegExp('[\\?&#]' + paramName + '=([^&#]*)');
          var params = regex.exec(uri);
          if (params != null) {
            return unescape(params[1]);
          }
          return;
        }

        //Wrap event bindings - Wait for async js to load
        /*twttr.ready(function (twttr) {
            //event bindings
            twttr.events.bind('tweet', trackTwitter);
        });*/

        /* ======================
         * LinkedIn Tracking
         * ====================== */
        var pathname = window.location.pathname;
        $('li.linkedin').click(function() {
        _gaq.push(['_trackSocial', 'Linkedin', 'Share', pathname]);
        });

    });
})(jQuery);
