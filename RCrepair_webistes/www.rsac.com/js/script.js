$(function() {


	// this always needs to be here
	// $(".custom .dropdown").removeClass("default-selectbox");
	
	
	var ww = document.body.clientWidth;
	
	var base_url = window.location.protocol +"//" + window.location.hostname+"/";
	
		size_format();
	
	function isMobile() {
		if (navigator.userAgent.indexOf('iPhone') != -1 || navigator.userAgent.indexOf('iPad') != -1 || navigator.userAgent.indexOf('Blackberry') != -1 || navigator.userAgent.indexOf('Android') != -1) {
			return true;
		} else {
			return false;	
		}
	}	
	
	
	
	function size_format() {
		
		ww = document.body.clientWidth;
		
		if (ww > 767) {
			$("#mobile-nav, #search-mobile-top").hide();	
			$("#product-search-forms").show();
			$("#process-search-forms").show();
	
		} else {
			$("#product-search-forms").hide();
			$("#process-search-forms").hide();
		}
		
	} // end size_format();

	$('input, textarea').placeholder();
	
		
	$(window).bind('resize orientationchange', function() {
		size_format();
	});
	
	
	
	$('a:not(a[href*="' + base_url + '"])').click(function(e) {
		
		var current_href=$(this).attr("href");
		
		
		
		var allowed=false;
		
		if ($(this).parent().hasClass("dropdown")==false) {
			if (current_href=="#") allowed=true;
			if (current_href.substr(0,1)=="#") { e.preventDefault(); allowed=true; }
		} else {
			allowed=true;
		}	
		
		$("#link-whitelist .whitelist-link").each(function(index,element) {
			var whitelist_link=$(this).attr("data-link");
			
			whitelist_link = $.trim(whitelist_link);
			if( current_href.substring(0,1) == "/" )
				current_href = base_url + current_href;
			if (current_href.indexOf(whitelist_link) > -1) {	
			
				allowed=true;	
			}
		});
		
		if (allowed==false) {
			$(this).attr("target","_blank");
			var link_continue = confirm("This link will take you away from the Reliance web site. Press OK to continue to this link, or press Cancel to stay at rsac.com.");
			
			if (link_continue==false)		
				e.preventDefault();
		}
	
	});


	
 	$("#quote-inquiry-lightbox, #quote-inquiry-lightbox-1, #quote-inquiry-lightbox-2, #quote-inquiry-lightbox-3, #quote-inquiry-lightbox-4, #selected-products-lightbox-wrapper").bind("opened",function(){
		
		$(document).scrollTop(0);
		
		$.post("/index.php/ajax/selected_products_count/", function(data) {
			$("#selected-products-link span").html(data);
		});
		
		
		$('input, textarea').placeholder();
		$(document).foundation();
	});
	
	$("#quote-inquiry-process-lightbox, #quote-inquiry-process-lightbox-1, #quote-inquiry-process-lightbox-2, #quote-inquiry-process-lightbox-3, #quote-inquiry-process-lightbox-4, #selected-processes-lightbox-wrapper").bind("opened",function(){
		$(document).scrollTop(0);
		$.post("/index.php/ajax/selected_process_count/", function(data) {
			$("#selected-processes-link span").html(data);
		});
		$('input, textarea').placeholder();
		$(document).foundation();
	});


	$(".pager-button").click(function() {
		$(".pager-button").removeClass("active");
		$(this).addClass("active");
	});

	$(".slide-subpager li").click(function() {
		$(".slide-subpager li").removeClass("active");
		$(this).addClass("active");
	});



	$("#btn-mobile-menu").click(function() {
			
		if ($("#search-mobile-top").is(":visible"))	$("#search-mobile-top").hide();
			
			if ($("#mobile-nav").is(":visible"))
				$("#mobile-nav").slideUp();	
			else 
				$("#mobile-nav").slideDown();	
			
		});
	
	
	
	$("#mobile-nav ul li a").click(function(e) {
		
		var submenu_check=$(this).parent().has("ul");
		
		
		if (submenu_check.length) {
			
			e.preventDefault();
			
			var submenu = submenu_check.find("ul");
			if (submenu.is(":visible")) {
				submenu.slideUp();
				$(this).parent().removeClass("active")
				$(this).css({'background':"url('" + base_url + "img/arrow-white-down-small.png') no-repeat #2a6f59 85% center"});
			} else {
				$(this).css({'background':"url('" + base_url + "img/arrow-white-up-small.png') no-repeat #134B6B 85% center"});
				submenu.slideDown();
				$(this).parent().addClass("active");
			}
			
		}
		
	});
	
// HOVER DROPDOWN - MAIN NAV
var tmrMenu; 
$("#nav a.has-subnav").mouseenter(function(){  
    $(".subnav-arrow").hide();
	clearTimeout(tmrMenu); 
    var menu_index = $("#nav a.has-subnav").index($(this));
	var listitem = $(this).parent();  
	listitem.find(".subnav-arrow").show(); 
	listitem.addClass("active");
	
	$("#sub-nav .row").hide();
	$("#sub-nav .row:eq(" + menu_index +")").show();
	$("#sub-nav").slideDown();
});


$("#nav a.has-subnav").mouseleave(function(){ 
	
	var menu_index = $("#nav a.has-subnav").index($(this));
	var listitem = $(this).parent();  
	tmrMenu = setTimeout(function() {
          listitem.find(".subnav-arrow").hide();  
		  $("#nav a.has-subnav").parent().removeClass("active"); 
		  // listitem.removeClass("active"); 
	 	  $("#sub-nav").slideUp(); 
	 	  $("#sub-nav .row:eq(" + menu_index +")").hide();		
    }, 500); 
	
	

});
 
$("#sub-nav .row").mouseenter(function(){  
    clearTimeout(tmrMenu); 
	// alert(tmrMenu);
	$(this).show(); 
});

$("#sub-nav").mouseleave(function(){ 
    var $this = $(this);
    tmrMenu = setTimeout(function() { 
			$this.hide(); 
			$(".subnav-arrow").hide(); 
			$("#nav a.has-subnav").parent().removeClass("active"); 
	 }, 500);
});
// 
				 

/* CLICK for DROPDOWN MENU	
	$("#nav a.has-subnav").click(function(e) {
		
		var menu_index = $("#nav a.has-subnav").index($(this));
		var listitem = $(this).parent();
		
		if ($("#sub-nav").is(":visible")) {
			
			if (listitem.hasClass("active")) {
				$("#nav li").removeClass("active");
				$("#sub-nav").slideUp();
				$(".subnav-arrow").hide();//fadeOut(500);	
			} else {
				$("#nav li").removeClass("active");
				$(".subnav-arrow").hide();//fadeOut(500);
				$("#sub-nav .row:visible").fadeOut(500, function() {
				$("#sub-nav .row:eq(" + menu_index +")").fadeIn(500);
					listitem.addClass("active");
					listitem.find(".subnav-arrow").show();//fadeIn(500);		
				});
			}
			
		} else {
			listitem.find(".subnav-arrow").show();//fadeIn(500);
			listitem.addClass("active");
			$("#sub-nav .row").hide();
			$("#sub-nav .row:eq(" + menu_index +")").show();
			$("#sub-nav").slideDown();
		}
	});
*/

	
			if (isMobile() || $('html').hasClass('lt-ie9')) {
				
				$(".product-search select").show();
			}
		$(".product-search").on("change", "#lst-group", function() {
			
			var group_val=0;
			var shape_val=0;
			var type_val=0;
			
			if ($("#lst-group").val().length>0)		group_val=$("#lst-group").val();
			if ($("#lst-shape").val().length>0)		shape_val=$("#lst-shape").val();
			if ($("#lst-type").val().length>0)		type_val=$("#lst-type").val();
	
			$(".lst-type-wrapper").html('<div class="ajax-loader text-center" style="width: 80%; text-align: center;"><img src="../img/ajax-loader.gif"/*tpa=http://www.rsac.com/img/ajax-loader.gif*/></div>');
				
			function runajax() {
				$.post("/index.php/ajax/product_relevant_type_options/" + encodeURIComponent(shape_val) + "/" + encodeURIComponent(group_val), function(data_type) {
					$("#lst-type").remove();
					$(".lst-type-wrapper").html(data_type);
					$("#lst-type").hide().removeAttr("disabled");
					if (document.body.clientWidth >= 767 && (isMobile()==false && !$('html').hasClass('lt-ie9') ) ) { 
						$(document).foundation();
					} else {
						if (isMobile() ) 
						{
							$("#product-search-forms select").show().css("display","block !important");	
						}
						else if ($('html').hasClass('lt-ie9'))
						{
							$(".product-search select").show();
						}
						else
						{
							$(document).foundation();
							$("#product-search-forms").show();
						}
						
						
					}
				}); // end post
			} // end runajax
				
			setTimeout(runajax, 300);				
	
			$(".lst-shape-wrapper").html('<div class="ajax-loader text-center" style="width: 80%; text-align: center;"><img src="../img/ajax-loader.gif"/*tpa=http://www.rsac.com/img/ajax-loader.gif*/></div>');
			
			function runajax_2() {
				$.post("/index.php/ajax/product_relevant_shape_options/" + encodeURIComponent(type_val) + "/" + encodeURIComponent(group_val), function(data_shape) {
					$("#lst-shape").remove();
					$(".lst-shape-wrapper").html(data_shape);
					$("#lst-shape").hide().removeAttr("disabled");
					if (document.body.clientWidth >= 767 && (isMobile()==false && !$('html').hasClass('lt-ie9') ))  { 
						$(document).foundation();
					} else {
						if (isMobile()) 
						{
							$("#product-search-forms select").show().css("display","block !important");	
						}
						else if ($('html').hasClass('lt-ie9'))
						{
							$(".product-search select").show();
						}
						else
						{
							$(document).foundation();
							$("#product-search-forms").show();
						}					
					}
				}); // end post
			} // end runajax_2
			
			setTimeout(runajax_2, 300);
			
		}); // end lst-group on change
	
		$(".product-search").on("change", "#lst-type", function() {
			
			var group_val=0;
			var shape_val=0;
			var type_val=0;
			
			if ($("#lst-group").val().length>0)		group_val=$("#lst-group").val();
			if ($("#lst-shape").val().length>0)		shape_val=$("#lst-shape").val();
			if ($("#lst-type").val().length>0)		type_val=$("#lst-type").val();
	
				$(".lst-shape-wrapper").html('<div class="ajax-loader text-center" style="width: 80%; text-align: center;"><img src="../img/ajax-loader.gif"/*tpa=http://www.rsac.com/img/ajax-loader.gif*/></div>');
				//alert(base_url+ "index.php/ajax/product_relevant_shape_options/" + encodeURIComponent(type_val) + "/" + encodeURIComponent(group_val));
				setTimeout(runajax, 300);
								
				function runajax() {
					$.post("/index.php/ajax/product_relevant_shape_options/" + encodeURIComponent(type_val) + "/" + encodeURIComponent(group_val), function(data_shape) {
						//alert(data_shape);
						$("#lst-shape").remove();
						$(".lst-shape-wrapper").html(data_shape);
						$("#lst-shape").hide().removeAttr("disabled");
						if (document.body.clientWidth >= 767  && (isMobile()==false && !$('html').hasClass('lt-ie9') ))  { 
							$(document).foundation();
						} else {
							if (isMobile()) 
							{
								$("#product-search-forms select").show().css("display","block !important");	
							}
							else if ($('html').hasClass('lt-ie9'))
							{
								$(".product-search select").show();
							}
							else
							{
								$(document).foundation();
								$("#product-search-forms").show();
							}
						}
				}); // end post
				} // end runajax
						
		}); // end product type change



		$(".process-search").on("change", "#lst-group", function() {
			
			var group_val=0;
			var type_val=0;
			
			if ($("#lst-group").val().length>0)		group_val=$("#lst-group").val();
			if ($("#lst-type").val().length>0)		type_val=$("#lst-type").val();
			
		
			//if (type_val==0) {
				$("#lst-type").attr("disabled", "disabled");
				$(".lst-type-wrapper").html('<div class="ajax-loader text-center" style="width: 80%; text-align: center;"><img src="../img/ajax-loader.gif"/*tpa=http://www.rsac.com/img/ajax-loader.gif*/></div>');
				$.post("/index.php/ajax/process_relevant_type_options/" + encodeURIComponent(group_val) + "/", function(data_type) {
					$("#lst-type").remove();
					$(".lst-type-wrapper").html(data_type);
					$("#lst-type").hide().removeAttr("disabled");
					if (document.body.clientWidth >= 767 && (isMobile()==false && !$('html').hasClass('lt-ie9') ))  { 
						$(document).foundation();
					} else {
						if (isMobile()) 
						{
							$("#process-search-forms select").show().css("display","block !important");	
						}
						else if ($('html').hasClass('lt-ie9'))
						{
							$(".process-search select").show();
						}
						else
						{
							$(document).foundation();
							$("#process-search-forms").show();
						}

					}
				}); // end post
			//}
			
			
		}); // end process group change


		$(".process-search").on("change", "#lst-type", function() {
			
			var group_val=0;
			var type_val=0;
			
			if ($("#lst-group").val().length>0)		group_val=$("#lst-group").val();
			if ($("#lst-type").val().length>0)		type_val=$("#lst-type").val();
			
		
			if (group_val==0) {
				$("#lst-group").attr("disabled", "disabled");
				$(".lst-group-wrapper").html('<div class="ajax-loader text-center" style="width: 80%; text-align: center;"><img src="../img/ajax-loader.gif"/*tpa=http://www.rsac.com/img/ajax-loader.gif*/></div>');
				$.post("/index.php/ajax/process_relevant_group_options/" + encodeURIComponent(type_val) + "/", function(data_group) {
					$("#lst-group").remove();
					$(".lst-group-wrapper").html(data_group);
					$("#lst-group").hide().removeAttr("disabled");
					if (document.body.clientWidth >= 767  && (isMobile()==false && !$('html').hasClass('lt-ie9') ))  { 
						$(document).foundation();
					} else {
						if (isMobile()) 
						{
							$("#process-search-forms select").show().css("display","block !important");	
						}
						else if ($('html').hasClass('lt-ie9'))
						{
							$(".process-search select").show();
						}
						else
						{
							$(document).foundation();
							$("#process-search-forms").show();
						}
					}
				}); // end post
			}
			
			
		}); // end process type change

		
		$("#product-search-keyword #txt-keyword").keypress(function(e) {
			
			if (e.which==13) {
				e.preventDefault();
				$("#button-products-keyword-search").trigger("click");
			}
			
		});
		
		$("#process-search-keyword #txt-keyword").keypress(function(e) {
			
			if (e.which==13) {
				e.preventDefault();
				$("#button-process-keyword-search").trigger("click");
			}
			
		});
		
	
		
		$("#quote-inquiry-lightbox").on("click", "#quote-add-product", function(e) {
			e.preventDefault();
			
			if ($(this).hasClass("disabled")) {
				// do nothing
			} else {
			
				var product_id=$("#add-product-info").attr("data-product-id");
				var location_id=$("#add-product-info").attr("data-location-id");
				var comments = $("#txt-comments").val();
				
				if (typeof(comments)=="undefined")	comments='';
				
				var selected_products_count=parseInt($("#selected-products-link span").text());
				
				//alert(base_url+ "index.php/ajax/product_add_to_inquiry_with_list/" + product_id + "/" + location_id + '/' + encodeURIComponent(comments));
				
				$.post("/index.php/ajax/product_add_to_inquiry_with_list/" + product_id + "/" + location_id + '/' + encodeURIComponent(comments), function(data) {
					
					if (data.indexOf("false") > -1) {
					
						$("#quote-inquiry-window h3.blue").after('<div class="quote-status-message error">There Was An Error While Adding Product. Please make sure cookies are enabled in your browser and try again.</div>');
					
					} else if(data.indexOf("already_added") > -1) {
						$("#quote-inquiry-window h3.blue").after('<div class="quote-status-message error">You Have Already Added This Product To This Inquiry!</div>');
					} else if(data.indexOf("bad_location") > -1) {
						$("#quote-inquiry-window h3.blue").after('<div class="quote-status-message error">You Can Only Add Products From The Same Location!</div>');					
					} else {
						
						if (isMobile()) 						
							$('.quote-inquiry-product-content-stuff').before('<p>Tip: Scroll in the window below to see more products available at this location.</p>');
							
						$("#quote-inquiry-window h3").text("Other Products Available At This Location");
						
						$("#quote-inquiry-step-1").html(data);
						$("#selected-products-link span").text(parseInt(selected_products_count + 1));
						$("#quote-add-product").addClass("disabled");
						$("#quote-finalize").removeClass("disabled");	
					}
					setTimeout(function() { $('.quote-status-message').fadeOut("slow"); }, 2000);
				}); // end post
			
			}
			
		});
		
		$("#quote-inquiry-lightbox").on("click", ".add-product-small", function(e) {
				var element_id=$(this).attr("id");
				var product_id=element_id.substr(element_id.lastIndexOf("-")+1);
				var location_id=$("#add-product-info").attr("data-location-id");
				var comments = $("#txt-comments").val();
				
				if (typeof(comments)=="undefined")	comments='';
				
				var selected_products_count=parseInt($("#selected-products-link span").text());
				
				//alert("index.php/ajax/product_add_to_inquiry_with_list/" + product_id + "/" + location_id + '/' + encodeURIComponent(comments));
				
				$.post("/index.php/ajax/product_add_to_inquiry_with_list/" + product_id + "/" + location_id + '/' + encodeURIComponent(comments), function(data) {
					//alert(data);
					if (data.indexOf("false") > -1) {
					
						$("#quote-inquiry-window h3.blue").after('<div class="quote-status-message error">There Was An Error While Adding Product. Please make sure cookies are enabled in your browser and try again.</div>');
					
					} else if (data.indexOf("already_added") > -1) {
					
						$("#quote-inquiry-window h3.blue").after('<div class="quote-status-message error">You Have Already Added This Product To This Inquiry!</div>');
					
					} else if(data.indexOf("bad_location") > -1) {
						
						$("#quote-inquiry-window h3.blue").after('<div class="quote-status-message error">You Can Only Add Products From The Same Location!</div>');
					
					} else {
						$("#quote-inquiry-step-1").html(data);
						
						if (isMobile()) 						
							$('.quote-inquiry-product-content-stuff').before('<p>Tip: Scroll in the window below to see more products available at this location.</p>');
						
						$("#quote-inquiry-window h3").text("Other Products Available At This Location");
						
						$("#selected-products-link span").text(parseInt(selected_products_count + 1));
						$("#quote-add-product").addClass("disabled");
						$("#quote-finalize").removeClass("disabled");
					}
					setTimeout(function() { $('.quote-status-message').fadeOut("slow"); }, 2000);
				}); // end post
			
			
		});


		$("#quote-inquiry-process-lightbox").on("click", "#quote-add-process", function(e) {
			e.preventDefault();
			
			if ($(this).hasClass("disabled")) {
				// do nothing
			} else {
			
				var process_id=$("#add-process-info").attr("data-process-id");
				var location_id=$("#add-process-info").attr("data-location-id");
				var comments = $("#txt-comments").val();
				
				if (typeof(comments)=="undefined")	comments='';
				
				var selected_processes_count=parseInt($("#selected-processes-link span").text());
				
				$.post("/index.php/ajax/process_add_to_inquiry_with_list/" + process_id + "/" + location_id + '/' + encodeURIComponent(comments), function(data) {
					
					if (data.indexOf("false") > -1) {
						$("#quote-inquiry-window h3.blue").after('<div class="quote-status-message error">There Was An Error While Adding Process. Please make sure cookies are enabled in your browser and try again.</div>');
					
					} else if (data.indexOf("already_added") > -1) {
						$("#quote-inquiry-window h3.blue").after('<div class="quote-status-message error">You Have Already Added This Process To This Inquiry!</div>');
					} else if(data.indexOf("bad_location") > -1) {
						$("#quote-inquiry-window h3.blue").after('<div class="quote-status-message error">You Can Only Add Processes From The Same Location!</div>');	
					} else {
						$("#quote-inquiry-step-1").html(data);
						
						if (isMobile()) 						
							$('.quote-inquiry-process-content-stuff').before('<p>Tip: Scroll in the window below to see more processes available at this location.</p>');
							
						$("#quote-inquiry-window h3").text("Other Processes Available At This Location");
						
						$("#selected-processes-link span").text(parseInt(selected_processes_count + 1));
						$("#quote-add-process").addClass("disabled");
						$("#quote-finalize").removeClass("disabled");	
					}
					setTimeout(function() { $('.quote-status-message').fadeOut("slow"); }, 2000);
				}); // end post
			
			}
			
		});
		
		$("#quote-inquiry-process-lightbox").on("click", ".add-process-small", function(e) {
				var element_id=$(this).attr("id");
				var process_id=element_id.substr(element_id.lastIndexOf("-")+1);
				var location_id=$("#add-process-info").attr("data-location-id");
				var comments = $("#txt-comments").val();
				
				if (typeof(comments)=="undefined")	comments='';
				
				var selected_processes_count=parseInt($("#selected-processes-link span").text());
				
				$.post("/index.php/ajax/process_add_to_inquiry_with_list/" + process_id + "/" + location_id + '/' + encodeURIComponent(comments), function(data) {
					
					if (data.indexOf("false") > -1) {
					
						$("#quote-inquiry-window h3.blue").after('<div class="quote-status-message error">There Was An Error While Adding Process. Please make sure cookies are enabled in your browser and try again.</div>');
					
					} else if (data.indexOf("already_added") > -1) {
					
						$("#quote-inquiry-window h3.blue").after('<div class="quote-status-message error">You Have Already Added This Process To This Inquiry!</div>');
					
					} else if(data.indexOf("bad_location") > -1) {
						
						$("#quote-inquiry-window h3.blue").after('<div class="quote-status-message error">You Can Only Add Processes From The Same Location!</div>');
					
					} else {					
						$("#quote-inquiry-step-1").html(data);
						
						if (isMobile()) 						
							$('.quote-inquiry-process-content-stuff').before('<p>Tip: Scroll in the window below to see more processes available at this location.</p>')
							
							;$("#quote-inquiry-window h3").text("Other Processes  Available At This Location");
						
						$("#selected-processes-link span").text(parseInt(selected_processes_count + 1));
						$("#quote-add-process").addClass("disabled");
						$("#quote-finalize").removeClass("disabled");
					}
					
					setTimeout(function() { $('.quote-status-message').fadeOut("slow"); }, 2000);
				}); // end post
			
			
		});
	
		$("#quote-inquiry-process-lightbox").on("click", "#quote-finalize", function(e) {
			
			
			if ($(this).hasClass("disabled")) {
				// do nothing
			} else {

				var process_id=$("#add-process-info").attr("data-process-id");
				var location_id=$("#add-process-info").attr("data-location-id");
	
			if (isMobile() == true){		
				$('#quote-inquiry-process-lightbox-2').foundation('reveal', 'open', {
					url: '/index.php/quote/process/' + process_id + '/' + location_id + '/2',
					closeonbackgroundclick: false,
					success: function(data) {
						$('input, textarea').placeholder();	
					}
				});
								
		} else {			
				$('#quote-inquiry-process-lightbox-2').foundation('reveal', 'open', {
					url: '/index.php/quote/process/' + process_id + '/' + location_id + '/2',
					success: function(data) {
						$('input, textarea').placeholder();	
					}
				});
		}

			}
			
		});
	
		
		$("#quote-inquiry-process-lightbox-2").on("click", "#quote-confirm", function(e) {
			
			e.preventDefault();
			
			if ($(this).hasClass("disabled")) {
				// do nothing
			} else {
				
				var form_error=false;
				
				var txt_name=$("#txt-quote-name").val();
				var txt_company=$("#txt-quote-company").val();
				var txt_address=$("#txt-quote-address").val();
				var txt_city=$("#txt-quote-city").val();
				var lst_state=$("#lst-quote-state").val();
				var txt_zip=$("#txt-quote-zip").val();
				var txt_country=$("#txt-quote-country").val();
				var txt_delivery=$("#txt-quote-delivery").val();
				var lst_returnquote=$("#lst-return-quote").val();
				
				var txt_phone=$("#txt-quote-phone").val();
				var txt_fax=$("#txt-quote-fax").val();
				var txt_email=$("#txt-quote-email").val();
				var txt_address2=$("#txt-quote-address-2").val();
				var txt_city2=$("#txt-quote-city-2").val();
				var lst_state2=$("#lst-quote-state-2").val();
				var txt_zip2=$("#txt-quote-zip-2").val();	
				var txt_country2=$("#txt-quote-country-2").val();
				var txt_hours=$("#txt-quote-hours").val();
				var txt_comments=$("#txt-contact-comments").val();
				var txt_contact=$("#txt-quote-contact").val();
				var txt_title=$("#txt-quote-title").val();
				
			if (txt_name.length==0) { form_error=true; $(".name.form-error").css("display","block"); }
				   else {$(".name.form-error").hide();}
			if (txt_address2.length==0) { form_error=true; $(".addr.form-error").css("display","block"); }
				   else {$(".addr.form-error").hide();}
			if (txt_city2==0) { form_error=true; $(".city.form-error").css("display","block"); }
				   else {$(".city.form-error").hide();}
			if (lst_state2.length==0) { form_error=true; $(".state.form-error").css("display","block"); }
				   else {$(".state.form-error").hide();}
			if (txt_zip2.length==0) { form_error=true; $(".zip.form-error").css("display","block"); }
				   else {$(".zip.form-error").hide();}
			if (lst_returnquote.length==0) { form_error=true; $(".time.form-error").css("display","block");} 
				   else {$(".time.form-error").hide();}	
			
if (!txt_phone.match(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/)) {form_error=true; $(".phone.form-error").css("display","block"); }
				 else {$(".phone.form-error").hide();}	
if (!txt_email.match(/\S+@\S+\.\S+/)) { form_error=true; $(".email.form-error").css("display","block"); } 
				 else {$(".email.form-error").hide();}	
				
				
				if (form_error==false) {
				var process_id=$("#add-process-info").attr("data-process-id");
				var location_id=$("#add-process-info").attr("data-location-id");
				var comments = $("#txt-comments").val();
				
				
				
	if (isMobile() == true){		
				$('#quote-inquiry-process-lightbox-3').foundation('reveal', 'open', {
					url: '/index.php/quote/process/' + process_id + '/' + location_id + '/3',
					closeOnBackgroundClick: false,
					data: { name: txt_name, company: txt_company, address: txt_address, city: txt_city, state: lst_state, zip: txt_zip, country: txt_country, 
					contact: txt_contact, title: txt_title,
					phone: txt_phone, fax: txt_fax, email: txt_email, address2: txt_address2, city2: txt_city2, state2: lst_state2, zip2: txt_zip2, country2: txt_country2,
					returnquote: lst_returnquote, delivery: txt_delivery, hours: txt_hours, comments: txt_comments }
					 
				});
		} else {
				$('#quote-inquiry-process-lightbox-3').foundation('reveal', 'open', {
					url: '/index.php/quote/process/' + process_id + '/' + location_id + '/3',
					data: { name: txt_name, company: txt_company, address: txt_address, city: txt_city, state: lst_state, zip: txt_zip, country: txt_country, 
					contact: txt_contact, title: txt_title,
					phone: txt_phone, fax: txt_fax, email: txt_email, address2: txt_address2, city2: txt_city2, state2: lst_state2, zip2: txt_zip2, country2: txt_country2,
					returnquote: lst_returnquote, delivery: txt_delivery, hours: txt_hours, comments: txt_comments }
				});
				}
		}			
				
			
			}
			$('input, textarea').placeholder();
		
			
		});


		
		$("#quote-inquiry-lightbox").on("click", "#quote-finalize", function(e) {
			
			
			if ($(this).hasClass("disabled")) {
				// do nothing
			} else {

				var product_id=$("#add-product-info").attr("data-product-id");
				var location_id=$("#add-product-info").attr("data-location-id");
	
	if (isMobile() == true){			
				$('#quote-inquiry-lightbox-2').foundation('reveal', 'open', {
					url: '/index.php/quote/product/' + product_id + '/' + location_id + '/2',
					closeOnBackgroundClick: false,
					success: function(data) {
						$('input, textarea').placeholder();	
					}
				});
	} else {
				$('#quote-inquiry-lightbox-2').foundation('reveal', 'open', {
					url: '/index.php/quote/product/' + product_id + '/' + location_id + '/2',
					success: function(data) {
						$('input, textarea').placeholder();	
					}
				});
	}
			}
			
			
		});
		
		$("#quote-inquiry-lightbox-2, #quote-inquiry-process-lightbox-2").on("click", "#chk-same-address", function(e) {
			
			if ($(this).is(":checked")) {
				$("#txt-quote-address").val($("#txt-quote-address-2").val());
				$("#txt-quote-city").val($("#txt-quote-city-2").val());
				$("#txt-quote-zip").val($("#txt-quote-zip-2").val());
				$("#lst-quote-state").val($("#lst-quote-state-2").val());
				$("#txt-quote-country").val($("#txt-quote-country-2").val());
				$("#lst-quote-state,#lst-quote-state-2").trigger("change",true);
				
			} 			
			
		});
		
		$("#quote-inquiry-lightbox-2").on("click", "#quote-confirm", function(e) {
			
			e.preventDefault();
			
			if ($(this).hasClass("disabled")) {
				// do nothing
			} else {
				
				var form_error=false;
				
				var txt_name=$("#txt-quote-name").val();
				var txt_company=$("#txt-quote-company").val();
				var txt_address=$("#txt-quote-address").val();
				var txt_city=$("#txt-quote-city").val();
				var lst_state=$("#lst-quote-state").val();
				var txt_zip=$("#txt-quote-zip").val();
				var txt_country=$("#txt-quote-country").val();
				var txt_delivery=$("#txt-quote-delivery").val();
				var lst_returnquote=$("#lst-return-quote").val();
				var txt_phone=$("#txt-quote-phone").val();
				var txt_fax=$("#txt-quote-fax").val();
				var txt_email=$("#txt-quote-email").val();
				var txt_address2=$("#txt-quote-address-2").val();
				var txt_city2=$("#txt-quote-city-2").val();
				var lst_state2=$("#lst-quote-state-2").val();
				var txt_zip2=$("#txt-quote-zip-2").val();	
				var txt_country2=$("#txt-quote-country-2").val();
				var txt_hours=$("#txt-quote-hours").val();
				var txt_comments=$("#txt-contact-comments").val();
				var txt_contact=$("#txt-quote-contact").val();
				var txt_title=$("#txt-quote-title").val();
				
			if (txt_name.length==0) { form_error=true; $(".name.form-error").css("display","block"); }
				   else {$(".name.form-error").hide();}
			if (txt_address2.length==0) { form_error=true; $(".addr.form-error").css("display","block"); }
				   else {$(".addr.form-error").hide();}
			if (txt_city2==0) { form_error=true; $(".city.form-error").css("display","block"); }
				   else {$(".city.form-error").hide();}
			if (lst_state2.length==0) { form_error=true; $(".state.form-error").css("display","block"); }
				   else {$(".state.form-error").hide();}
			if (txt_zip2.length==0) { form_error=true; $(".zip.form-error").css("display","block"); }
				   else {$(".zip.form-error").hide();}
			if (lst_returnquote.length==0) { form_error=true; $(".time.form-error").css("display","block");} 
				   else {$(".time.form-error").hide();}	
			
if (!txt_phone.match(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/)) {form_error=true; $(".phone.form-error").css("display","block"); }
				 else {$(".phone.form-error").hide();}	
if (!txt_email.match(/\S+@\S+\.\S+/)) { form_error=true; $(".email.form-error").css("display","block"); } 
				 else {$(".email.form-error").hide();}		

				
				if (form_error==false) {
				var product_id=$("#add-product-info").attr("data-product-id");
				var location_id=$("#add-product-info").attr("data-location-id");
				var comments = $("#txt-comments").val();
				
		if (isMobile() == true){
					$('#quote-inquiry-lightbox-3').foundation('reveal', 'open', {
					url: '/index.php/quote/product/' + product_id + '/' + location_id + '/3',
					closeOnBackgroundClick: false,
					data: { name: txt_name, company: txt_company, address: txt_address, city: txt_city, state: lst_state, zip: txt_zip, country: txt_country, 
					contact: txt_contact, title: txt_title, phone: txt_phone, fax: txt_fax, email: txt_email, address2: txt_address2, city2: txt_city2, 
					state2: lst_state2, zip2: txt_zip2, country2: txt_country2, returnquote: lst_returnquote, delivery: txt_delivery, hours: txt_hours, comments: txt_comments }
					
				});
		} else {
					$('#quote-inquiry-lightbox-3').foundation('reveal', 'open', {
					url: '/index.php/quote/product/' + product_id + '/' + location_id + '/3',
					data: { name: txt_name, company: txt_company, address: txt_address, city: txt_city, state: lst_state, zip: txt_zip, country: txt_country, 
					contact: txt_contact, title: txt_title, phone: txt_phone, fax: txt_fax, email: txt_email, address2: txt_address2, city2: txt_city2, 
					state2: lst_state2, zip2: txt_zip2, country2: txt_country2, returnquote: lst_returnquote, delivery: txt_delivery, hours: txt_hours, comments: txt_comments }
				});
		}
				
				
				
				}
			
			}
			$('input, textarea').placeholder();
			$(document).foundation();
			
		});


		$("#quote-inquiry-lightbox-3").on("click", "#quote-send", function(e) {
			
			e.preventDefault();
			
			if ($(this).hasClass("disabled")) {
				// do nothing
			} else {
				
				//var form_error=false;
				
				var txt_name=$("#hidden-name").val();
				var txt_company=$("#hidden-company").val();
				var txt_address=$("#hidden-address").val();
				var txt_city=$("#hidden-city").val();
				var lst_state=$("#hidden-state").val();
				var txt_zip=$("#hidden-zip").val();
				var txt_country=$("#hidden-country").val();
				var txt_delivery=$("#hidden-delivery").val();
				var lst_returnquote=$("#hidden-returnquote").val();
				var txt_phone=$("#hidden-phone" ).val();
				var txt_fax=$("#hidden-fax").val();
				var txt_email=$("#hidden-email").val();
				var txt_address2=$("#hidden-address2").val();
				var txt_city2=$("#hidden-city2").val();
				var lst_state2=$("#hidden-state2").val();
				var txt_zip2=$("#hidden-zip2").val();	
				var txt_country2=$("#hidden-country2").val();
				var txt_hours=$("#hidden-hours").val();
				var txt_comments=$("#hidden-comments").val();
				var txt_contact=$("#hidden-contact").val();
				var txt_title=$("#hidden-title").val();
				
		if (isMobile() == true){		
				$('#quote-inquiry-lightbox-4').foundation('reveal', 'open', {
					url: '/index.php/quote/send_quote_inquiry_products/',
					closeOnBackgroundClick: false,
					data: { name: txt_name, company: txt_company, address: txt_address, city: txt_city, state: lst_state, zip: txt_zip, country: txt_country,
					contact: txt_contact, title: txt_title, phone: txt_phone, fax: txt_fax, email: txt_email, address2: txt_address2, city2: txt_city2, 
					state2: lst_state2, zip2: txt_zip2, country2: txt_country2, returnquote: lst_returnquote, delivery: txt_delivery, hours: txt_hours, comments: txt_comments }
					
				});
		} else {
				$('#quote-inquiry-lightbox-4').foundation('reveal', 'open', {
					url: '/index.php/quote/send_quote_inquiry_products/',
					data: { name: txt_name, company: txt_company, address: txt_address, city: txt_city, state: lst_state, zip: txt_zip, country: txt_country,
					contact: txt_contact, title: txt_title, phone: txt_phone, fax: txt_fax, email: txt_email, address2: txt_address2, city2: txt_city2, 
					state2: lst_state2, zip2: txt_zip2, country2: txt_country2, returnquote: lst_returnquote, delivery: txt_delivery, hours: txt_hours, comments: txt_comments }
				});
		}
				
				$("#selected-products-link span").text("0");
			
			}
		
			
		});
		
		$("#quote-inquiry-process-lightbox-3").on("click", "#quote-send", function(e) {
			
			e.preventDefault();
			
			if ($(this).hasClass("disabled")) {
				// do nothing
			} else {
				
				//var form_error=false;
				
				var txt_name=$("#hidden-name").val();
				var txt_company=$("#hidden-company").val();
				var txt_address=$("#hidden-address").val();
				var txt_city=$("#hidden-city").val();
				var lst_state=$("#hidden-state").val();
				var txt_zip=$("#hidden-zip").val();
				var txt_country=$("#hidden-country").val();
				var txt_delivery=$("#hidden-delivery").val();
				var lst_returnquote=$("#hidden-returnquote").val();
				var txt_phone=$("#hidden-phone").val();
				var txt_fax=$("#hidden-fax").val();
				var txt_email=$("#hidden-email").val();
				var txt_address2=$("#hidden-address2").val();
				var txt_city2=$("#hidden-city2").val();
				var lst_state2=$("#hidden-state2").val();
				var txt_zip2=$("#hidden-zip2").val();	
				var txt_country2=$("#hidden-country2").val();
				var txt_hours=$("#hidden-hours").val();
				var txt_comments=$("#hidden-comments").val();
				var txt_contact=$("#hidden-contact").val();
				var txt_title=$("#hidden-title").val();
				
		if (isMobile() == true){		
				$('#quote-inquiry-process-lightbox-4').foundation('reveal', 'open', {
					url: '/index.php/quote/send_quote_inquiry_process/',
					closeOnBackgroundClick: false,
					data: { name: txt_name, company: txt_company, address: txt_address, city: txt_city, state: lst_state, zip: txt_zip, country: txt_country,
					contact: txt_contact, title: txt_title, phone: txt_phone, fax: txt_fax, email: txt_email, address2: txt_address2, city2: txt_city2, 
					state2: lst_state2, zip2: txt_zip2, country2: txt_country2, returnquote: lst_returnquote, delivery: txt_delivery, hours: txt_hours, comments: txt_comments }
					
				});
		} else {
				$('#quote-inquiry-process-lightbox-4').foundation('reveal', 'open', {
					url: '/index.php/quote/send_quote_inquiry_process/',
					data: { name: txt_name, company: txt_company, address: txt_address, city: txt_city, state: lst_state, zip: txt_zip, country: txt_country,
					contact: txt_contact, title: txt_title, phone: txt_phone, fax: txt_fax, email: txt_email, address2: txt_address2, city2: txt_city2, 
					state2: lst_state2, zip2: txt_zip2, country2: txt_country2, returnquote: lst_returnquote, delivery: txt_delivery, hours: txt_hours, comments: txt_comments }
				});	
		}
				
				
			$("#selected-processes-link span").text("0");
			}
		
			
		});
		

		
		$("#quote-inquiry-lightbox-3").on("click", ".btn-remove", function(e) {
			e.preventDefault();
			var element=$(this);
			var element_id=$(this).attr("id");
			var index=$(this).attr("data-product-id");//element_id.substr(element_id.lastIndexOf("-")+1);
			var selected_products_count=parseInt($("#selected-products-link span").text());
			$.post("/index.php/ajax/product_remove_from_inquiry/" + index, function(data) {
				
				if (data.indexOf("true") > -1) {
					$("#selected-products-link span").text(parseInt(selected_products_count - 1));
					element.parent().parent().remove();
					
					if (parseInt(selected_products_count - 1)==0) {
						$("#button-wrapper").html('<div style="width: 35%; float: right;"><a id="quote-close" class="btn-search close-reveal-modal">Close</a></div>');
						$("#selected-heading").after('<p>There are no products selected. Please start a new inquiry to select products.</p>');
						//$("#quote-finalize").removeAttr("data-reveal-ajax").removeAttr("data-reveal-id").addClass("close-reveal-modal").attr("id","quote-close").text("Close");
					}
					
				}
				
			});
		
		}); // end
		
		$("#selected-products-lightbox-wrapper").on("click", ".btn-remove", function(e) {
			e.preventDefault();
			var element=$(this);
			var element_id=$(this).attr("id");
			var index=$(this).attr("data-product-id");//element_id.substr(element_id.lastIndexOf("-")+1);
			var selected_products_count=parseInt($("#selected-products-link span").text());
			$.post("/index.php/ajax/product_remove_from_inquiry/" + index, function(data) {
				
				if (data.indexOf("true") > -1) {
					$("#selected-products-link span").text(parseInt(selected_products_count - 1));
					element.parent().parent().remove();
					
					if (parseInt(selected_products_count - 1)==0) {
						$("#button-wrapper").html('<a id="quote-close" class="btn-search close-reveal-modal">Close</a>');
						$("#selected-heading-1").after("<p>There are no products selected. Please start a new inquiry to select products.</p>");
						//$("#quote-finalize").removeAttr("data-reveal-ajax").removeAttr("data-reveal-id").addClass("close-reveal-modal").attr("id","quote-close").text("Close");
					}
					
				}
				
			});
		
		}); // end
		
		$("#quote-inquiry-lightbox").on("click",".btn-remove", function(e) {
			
			e.preventDefault();
			var element=$(this);
			var element_id=$(this).attr("id");
			var index=$(this).attr("data-product-id") ||$(this).attr("data-process-id")  ;//element_id.substr(element_id.lastIndexOf("-")+1);
			var selected_products_count=parseInt($("#selected-products-link span").text());
			$.post("/index.php/ajax/product_remove_from_inquiry/" + index, function(data) {
				
				if (data.indexOf("true") > -1) {
					$("#selected-products-link span").text(parseInt(selected_products_count - 1));
					element.removeClass(".btn-remove").addClass("add-product-small").attr("id","add-product-small-" + index).text("Add");
				}
				
			});
		});
		
		
		$("#quote-inquiry-process-lightbox-3").on("click", ".btn-remove", function(e) {
			
			e.preventDefault();
			var element=$(this);
			var element_id=$(this).attr("id");
			var index=$(this).attr("data-process-id");//element_id.substr(element_id.lastIndexOf("-")+1);
			var selected_products_count=parseInt($("#selected-processes-link span").text());
			$.post("/index.php/ajax/process_remove_from_inquiry/" + index, function(data) {
				
				if (data.indexOf("true") > -1) {
					$("#selected-processes-link span").text(parseInt(selected_products_count - 1));
					element.parent().parent().remove();
				
					if (parseInt(selected_products_count - 1)==0) {
						$("#button-wrapper").html('<div style="width: 35%; float: right;"><a id="quote-close" class="btn-search close-reveal-modal">Close</a></div>');
						$("#selected-heading").after('<p>There are no processes selected. Please start a new inquiry to select processes.</p>');
						//$("#quote-finalize").removeAttr("data-reveal-ajax").removeAttr("data-reveal-id").addClass("close-reveal-modal").attr("id","quote-close").text("Close");
					}
					
				}
				
			});
		
		}); // end remove
		
		
		$("#selected-processes-lightbox-wrapper").on("click", ".btn-remove", function(e) {
			
			e.preventDefault();
			var element=$(this);
			var element_id=$(this).attr("id");
			var index=$(this).attr("data-process-id");//element_id.substr(element_id.lastIndexOf("-")+1);
			var selected_products_count=parseInt($("#selected-processes-link span").text());
			$.post("/index.php/ajax/process_remove_from_inquiry/" + index, function(data) {
				
				if (data.indexOf("true") > -1) {
					$("#selected-processes-link span").text(parseInt(selected_products_count - 1));
					element.parent().parent().remove();
					
					if (parseInt(selected_products_count - 1)==0) {
						$("#button-wrapper").html('<a id="quote-close" class="btn-search close-reveal-modal">Close</a>');
						$("#selected-heading-2").after("<p>There are no processes selected. Please start a new inquiry to select processes.</p>");
						//$("#quote-finalize").removeAttr("data-reveal-ajax").removeAttr("data-reveal-id").addClass("close-reveal-modal").attr("id","quote-close").text("Close");
					}
					
				}
				
			});
		
		}); // end remove
		
		$("#quote-inquiry-process-lightbox").on("click",".btn-remove", function(e) {
			
			e.preventDefault();
			var element=$(this);
			var element_id=$(this).attr("id");
			var index=$(this).attr("data-process-id");//element_id.substr(element_id.lastIndexOf("-")+1);
			var selected_products_count=parseInt($("#selected-products-link span").text());
			$.post("/index.php/ajax/process_remove_from_inquiry/" + index, function(data) {
				
				if (data.indexOf("true") > -1) {
					$("#selected-products-link span").text(parseInt(selected_products_count - 1));
					element.removeClass(".btn-remove").addClass("add-process-small").attr("id","add-process-small-" + $(this).attr("data-process-id")).text("Add");
				}
				
			});
		});
		
		
		
		
	/* -------------------------- [end] quote inquiry form stuff -----------------------------------*/	
	
	
	/* ---------------------------- home page (slideshow) -------------------------------------------*/
		
	var home_currentslide=1;
	
	if ($("body").hasClass("home")) {
		
    	$( "#accordion" ).accordion({
	 	 	active: false,
     		collapsible: true,
   		 });	

		
		//$("#our-family-wrapper").simplyScroll();
		
		$('.logo-wrapper[data-reveal-id="company-bio-37"],.logo-wrapper[data-reveal-id="company-bio-26"]').remove();
		
		
		$("#our-family-wrapper").smoothDivScroll({ 
				autoScrollingMode: "onStart",
				manualContinuousScrolling: true
			});
			
 		// Mouse in and out
		$("#our-family-wrapper").hover(function() {
			$(this).smoothDivScroll("stopAutoScrolling");
		 },function() {
			$(this).smoothDivScroll("startAutoScrolling");
		 });

	} // end home page check
	
	 
	
	if (($("body").hasClass("executive-management")) || ($("body").hasClass("board-of-directors"))) {

		$(".lt-ie9 .reveal-modal").bind("opened",function(){
			$(document).scrollTop(0); 
			$(document).foundation();
		});
	
	 } // end leadership page check
	 
	 
	
	
	if ($("body").hasClass("family-of-companies")) {	
		
		
		
		$(".lt-ie9 .reveal-modal").bind("opened",function(){
			$(document).scrollTop(0); 
			$(document).foundation();
		});
		
		var $coverflowContainer = $( '#coverflow' ),
			$coverflowItems = $coverflowContainer.children(),
			$imageCaption = $( '#image-caption' ),
			$slider = $( '#scrollbar' )
	
		

		$slider.slider({
			orientation: 'horizontal',
			min: 0,
			max : $coverflowItems.length - 1,
			slide: function( event, ui ) {
				$coverflowContainer.coverflow( 'select', ui.value );
			}
		});

		$coverflowContainer.coverflow({
			select : function( ev, ui ) {

				//var $item = $playlistItems.eq( ui.index );
				
				$imageCaption.text(
					ui.active.data( 'title' )
				);

				$slider.slider( 'value', ui.index );

				
			}
		});
		
		$(window).bind('resize orientationchange', function() {
			$( '#coverflow' ).coverflow('refresh');
		});
				
	
		$("#coverflow div").each(function(e) {
			var element_id=$(this).attr("id");
			var company_bio=element_id.substr(element_id.lastIndexOf("-")+1);
			$(this).attr("data-reveal-id", "company-bio-" + company_bio);
			//$('#company-bio-' + company_bio).foundation('reveal', 'open');
		});
		
		
	
	
	}  //end family page check	


	
	if ($("body").hasClass("history")) {
	
		$("#navigation-timeline-mobile").click(function() {
			
			if ($(this).hasClass("open")) {
				$(this).removeClass("open");
			} else {
				$(this).addClass("open");
			}
		});
		
		$("#decade-list li a").click(function() {
			var menu_index = $("#decade-list li").index($(this).parent());
			var decade = $(this).text();
			
			$('#background-slider-wrapper').cycle('goto', menu_index);
			$('#outer-slider').cycle('goto', menu_index);
		$(".cycle-slide-active .inner-slider").cycle('goto', 0);
			
			$("#current-decade").text(decade);
			
			$(".pager-button").removeClass("active");
			$(".pager-button:eq(" + menu_index + ")").addClass("active");
			
			setTimeout(
				function() {
					// $(".outerslider-slide.cycle-slide-active .slide-subpager li").removeClass("active"); 
					$(".outerslider-slide.cycle-slide-active .slide-subpager li:eq(0)").addClass("active");
					$(".outerslider-slide.cycle-slide-active .inner-slider .innerslider-slide").removeClass("cycle-slide-active"); 
					$(".outerslider-slide.cycle-slide-active .inner-slider .innerslider-slide:eq(0)").addClass("cycle-slide-active");
				}
			, 500);
			
			
		});
		
		$("#inner-slider-1930").on( 'cycle-after', function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
			var inner_index=parseInt($(incomingSlideEl).index("#inner-slider-1930 .innerslider-slide"))-1;
			$("#inner-slider-1930").prevAll(".slide-subpager").find("li").removeClass("active");
			$("#inner-slider-1930").prevAll(".slide-subpager").find("li:eq("+ inner_index +")").addClass("active");		
		});
		
		$("#inner-slider-1940").on( 'cycle-after', function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
			var inner_index=parseInt($(incomingSlideEl).index("#inner-slider-1940 .innerslider-slide"))-1;
			$("#inner-slider-1940").prevAll(".slide-subpager").find("li").removeClass("active");
			$("#inner-slider-1940").prevAll(".slide-subpager").find("li:eq("+ inner_index +")").addClass("active");		
		});
		
		$("#inner-slider-1950").on( 'cycle-after', function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
			var inner_index=parseInt($(incomingSlideEl).index("#inner-slider-1950 .innerslider-slide"))-1;
			$("#inner-slider-1950").prevAll(".slide-subpager").find("li").removeClass("active");
			$("#inner-slider-1950").prevAll(".slide-subpager").find("li:eq("+ inner_index +")").addClass("active");		
		});
		
		$("#inner-slider-1960").on( 'cycle-after', function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
			var inner_index=parseInt($(incomingSlideEl).index("#inner-slider-1960 .innerslider-slide"))-1;
			$("#inner-slider-1960").prevAll(".slide-subpager").find("li").removeClass("active");
			$("#inner-slider-1960").prevAll(".slide-subpager").find("li:eq("+ inner_index +")").addClass("active");		
		});
		
		$("#inner-slider-1970").on( 'cycle-after', function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
			var inner_index=parseInt($(incomingSlideEl).index("#inner-slider-1970 .innerslider-slide"))-1;
			$("#inner-slider-1970").prevAll(".slide-subpager").find("li").removeClass("active");
			$("#inner-slider-1970").prevAll(".slide-subpager").find("li:eq("+ inner_index +")").addClass("active");		
		});
		
		$("#inner-slider-1980").on( 'cycle-after', function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
			var inner_index=parseInt($(incomingSlideEl).index("#inner-slider-1980 .innerslider-slide"))-1;
			$("#inner-slider-1980").prevAll(".slide-subpager").find("li").removeClass("active");
			$("#inner-slider-1980").prevAll(".slide-subpager").find("li:eq("+ inner_index +")").addClass("active");		
		});
		
		$("#inner-slider-1990").on( 'cycle-after', function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
			var inner_index=parseInt($(incomingSlideEl).index("#inner-slider-1990 .innerslider-slide"))-1;
			$("#inner-slider-1990").prevAll(".slide-subpager").find("li").removeClass("active");
			$("#inner-slider-1990").prevAll(".slide-subpager").find("li:eq("+ inner_index +")").addClass("active");		
		});
		
		$("#inner-slider-2000").on( 'cycle-after', function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
			var inner_index=parseInt($(incomingSlideEl).index("#inner-slider-2000 .innerslider-slide"))-1;
			$("#inner-slider-2000").prevAll(".slide-subpager").find("li").removeClass("active");
			$("#inner-slider-2000").prevAll(".slide-subpager").find("li:eq("+ inner_index +")").addClass("active");		
		});
		
		$("#inner-slider-2010").on( 'cycle-after', function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
			var inner_index=parseInt($(incomingSlideEl).index("#inner-slider-2010 .innerslider-slide"))-1;
			$("#inner-slider-2010").prevAll(".slide-subpager").find("li").removeClass("active");
			$("#inner-slider-2010").prevAll(".slide-subpager").find("li:eq("+ inner_index +")").addClass("active");		
		});
		
		
		$(".pager-button").click(function() {
			var menu_index = $(".pager-button").index($(this));	
			var decade = $(this).text();
			$("#current-decade").text(decade);
			$('#background-slider-wrapper').cycle('goto', menu_index);
			$('#outer-slider').cycle('goto', menu_index);
			$(".cycle-slide-active .inner-slider").cycle('goto', 0);
			setTimeout(
				function() {
					$(".outerslider-slide.cycle-slide-active .slide-subpager li").removeClass("active"); 
					$(".outerslider-slide.cycle-slide-active .slide-subpager li:eq(0)").addClass("active");
					$(".outerslider-slide.cycle-slide-active .inner-slider .innerslider-slide").removeClass("cycle-slide-active"); 
					$(".outerslider-slide.cycle-slide-active .inner-slider .innerslider-slide:eq(0)").addClass("cycle-slide-active");
				}
			, 500);
					
		});
		
		
	} // end history page check

	if ($("body").hasClass("products")) {
		
		
		$("#lst-country-details").change(function(e) {	 
			if ($("#lst-state-details").val().length>0) {
			    if (($(this).val() == "US") || ($(this).val() == "")) {
					// do nothing 
				} else {
					 $("#txt-zipcode-details").val('');
				   	 $("#lst-state-details").val("");
				 	 $("#lst-state-wrapper-details .current").html("State");
					 $("#lst-state-wrapper-details li").removeClass('selected');
			    }
			 }
		});
		$("#txt-zipcode-details").focus(function(e) { 
			 $("#lst-country-details").val("");
			 $("#lst-country-wrapper-details .current").html("Country");
			 $("#lst-country-wrapper-details li").removeClass('selected');
			 $("#lst-state-details").val("");
			 $("#lst-state-wrapper-details .current").html("State");
			 $("#lst-state-wrapper-details li").removeClass('selected');		 
		});		
		
		$("#lst-state-details").change(function(e) {  
				if ($(this).val() == "") {
					// do nothing 
				} else {
					 $("#txt-zipcode-details").val('');
					 $("#lst-country-details").val("");
				 	 $("#lst-country-wrapper-details .current").html("Country");
					 $("#lst-country-wrapper-details li").removeClass('selected');
				}
					// $("#lst-country-details").val("USA");  
					// $("#lst-country-details").trigger("change",true); 			 
		});
		
		
		$("#content-tab-left.product-content-tab").click(function(){

			$(".product-content-tab").removeClass("active");		
			if ($("#product-search-forms").is(":visible")) {
				
				$("#product-search-forms").slideUp();
				
				
			} else { 
				$(this).addClass("active");
				$("#product-search-forms").slideDown();	
			}			
			
		});
		


		$("#content-tab-left.process-content-tab").click(function(){
			$(".process-content-tab").removeClass("active");		
			if ($("#process-search-forms").is(":visible")) {
				
				$("#process-search-forms").slideUp();
				
			} else { 
				$(this).addClass("active");
				$("#process-search-forms").slideDown();	
			}	
		});
		
	
		$("#button-products-search").click(function(e) {
			e.preventDefault();
			$("#product_search_form").val("product-material");
			$("#form-product-search").submit();
		});
		
		$("#button-products-keyword-search").click(function(e) {
			e.preventDefault();
			$("#lst-group, #lst-type, #lst-shape").val("");
			$("#product_search_form").val("product-keyword");
			$("#form-product-search").submit();
		});
		
		/*$("#button-location-search").click(function(e) {
			e.preventDefault();
			$("#form-location-search").submit();	
		});*/
		
		$("#button-location-search-details").click(function(e) {
			e.preventDefault();
			var form_action = $("#form-location-search-details").attr("action");
			var form_action_last = form_action.substring(form_action.lastIndexOf('/')+1);
			form_action = form_action.substring(0,form_action.lastIndexOf('/')+1);
			if( form_action_last != "" )
			{
				$("#form-location-search-details").attr("action", form_action);
			}
			$("#form-location-search-details").submit();	
		});
		
		$("#button-change-location").click(function(e) {
			e.preventDefault();
			$(".selected-location").fadeOut("fast", function() {
				$("#locations-search-products").fadeIn("fast");	
			});	
			
		});
	
		$("#button-locations-search-product-cancel").click(function(e) {
			e.preventDefault();
			$("#locations-search-products").fadeOut("fast", function() {
				$(".selected-location").fadeIn("fast");	
			});	
			
						
		});

		$("#button-process-search").click(function(e) {
			e.preventDefault();
			$("#process_search_form").val("process-type");
			$("#form-process-search").submit();
		});
		
		$("#button-process-keyword-search").click(function(e) {
			e.preventDefault();
			$("#lst-group, #lst-type").val("");
			$("#process_search_form").val("process-keyword");
			$("#form-process-search").submit();
		});
		
		
		$("tr[data-href]").click(function() {
			window.location=$(this).attr("data-href");	
		});
		
			
	} // end product page check	
	
	
	if ($("body").hasClass("products-search-results")) {
		
		var base_url2 = $(".result-info").attr("data-base-url") + 'index.php/products/search/';
		var sort_field = $(".result-info").attr("data-sort-field");
		var sort_direction=$(".result-info").attr("data-sort-direction"); 
		
		$(".pager a").each(function(index) {
			var href=$(this).attr("href");
			var link_page=href.substr(href.lastIndexOf("/")+1);
			var form_action=base_url2 + sort_field + "-" + sort_direction + "/" + link_page;
	
			$(this).bind("click", function(e) {
				e.preventDefault();
				
				$("#form-product-search").attr("action", form_action);
				$("#form-product-search").submit();	
			});
			
		});
		
		$(".results-table thead tr th a").each(function(index) {
			var href=$(this).attr("href");
			$(this).bind("click", function(e) {
				e.preventDefault();
				$("#form-product-search").attr("action", href);
				$("#form-product-search").submit();	
			});
		});
		
	}// end products search results check
	
	if ($("body").hasClass("processes-search-results")) {
		
		var base_url2 = $(".result-info").attr("data-base-url") + 'index.php/processes/search/';
		var sort_field = $(".result-info").attr("data-sort-field");
		var sort_direction=$(".result-info").attr("data-sort-direction"); 
		
		
		$(".pager a").each(function(index) {
			var href=$(this).attr("href");
			var link_page=href.substr(href.lastIndexOf("/")+1);
			var form_action=base_url2 + sort_field + "-" + sort_direction + "/" + link_page;
			
			$(this).bind("click", function(e) {
				e.preventDefault();
				$("#form-process-search").attr("action", form_action);
				$("#form-process-search").submit();	
			});
			
		});
		
		$(".results-table thead tr th a").each(function(index) {
			var href=$(this).attr("href");
			$(this).bind("click", function(e) {
				e.preventDefault();
				$("#form-process-search").attr("action", href);
				$("#form-process-search").submit();	
			});
		});
		
	}// end processes search results check

	if ($("body").hasClass("locations-search-results")) {
		
		var base_url2 = $(".result-info").attr("data-base-url") + 'index.php/locations/search/';
		var sort_field = $(".result-info").attr("data-sort-field");
		var sort_direction=$(".result-info").attr("data-sort-direction"); 
		
		
		$(".pager a").each(function(index) {
			var href=$(this).attr("href");
			var link_page=href.substr(href.lastIndexOf("/")+1);
			var form_action=base_url2 + link_page;
			
			$(this).bind("click", function(e) {
				e.preventDefault();
				$("#form-location-search").attr("action", form_action);
				$("#form-location-search").submit();	
			});
			
		});
		
		
	}// end locations search results check


if ($("body").hasClass("details-page")) {
		
		var zip_input = (document.getElementById('txt-zipcode-details'));
  			var autocomplete = new google.maps.places.Autocomplete(zip_input);
			
	} // end details page check


	if ($("body").hasClass("products") && $("body").hasClass("details-page")) {
		
		var current_action=$("#form-location-search-details").attr("action");
		
		current_action=current_action.substr(0, current_action.lastIndexOf('/'));
		// alert(current_action);
		
		
		$(".pager a").each(function(index) {
			var href=$(this).attr("href");
			var link_page=href.substr(href.lastIndexOf("/")+1);
			var form_action=current_action + "/" + link_page;
			
			$(this).bind("click", function(e) {
				e.preventDefault();
				$("#form-location-search-details").attr("action", form_action);
				$("#form-location-search-details").submit();	
			});
			
		});
		
		
	}// end locations search results check

	
	if ($("body").hasClass("contact")) {
		
		$(".lt-ie9 .reveal-modal").bind("opened",function(){
			$(document).scrollTop(0); 
			$(document).foundation();
		});
		
		
		    var zip_input = (document.getElementById('txt-contact-zipcode'));
  			var autocomplete = new google.maps.places.Autocomplete(zip_input);
		
		
		$("#button-contact-1").click(function() {
			$("#lst-contact-state").val('');
			$("#lst-contact-company").val('');
			$("#frm_contact_1").submit();	
		});
		$("#button-contact-2").click(function() {
			$("#txt-contact-zipcode").val('');
			$("#lst-contact-company").val('');
			$("#frm_contact_2").submit();	
		});
		$("#button-contact-3").click(function() {
			$("#lst-contact-state").val('');
			$("#txt-contact-zipcode").val('');
			$("#frm_contact_3").submit();	
		});
		
		$(".contact-tab a").click(function(e) {
			e.preventDefault();
			var contact=$(this).parent().attr("id");
			var contact_id=contact.substr(contact.lastIndexOf("-")+1);
			$(".contact-tab").removeClass("active");
			$(".contact-content").removeClass("active");
			$(this).parent().addClass("active");
			$("#contact-content-" + contact_id).addClass("active");
		});
		
		$('#tab-1').click(function(){
			$("#lst-contact-state").val('');
			$("#lst-contact-company").val('');
			$('#tab-content-1').show();
			$('#tab-content-2').hide();
			$('#tab-content-3').hide();
		});
		$('#tab-2').click(function(){
			$("#txt-contact-zipcode").val('');
			$("#lst-contact-company").val('');
			$('#tab-content-1').hide();
			$('#tab-content-2').show();
			$('#tab-content-3').hide();
		});
		$('#tab-3').click(function(){		
			$("#lst-contact-state").val('');
			$("#txt-contact-zipcode").val('');
			$('#tab-content-1').hide();
			$('#tab-content-2').hide();
			$('#tab-content-3').show();
		});

// IE8 hack for the FOR attr of LABEL - - - - - - - - - - - - - - - - - - -        I love IE8  <3 <3 <3 !
	$('.lt-ie9 #form-tabs .tab.init label').css({color: 'rgb(0, 82, 127)','background-color':'rgb(202, 219, 212)','z-index':2});
	
				
		$(".lt-ie9 #form-tabs .tab label").click(function(){
    	if ($(this).attr("for") != "")
        	$("#" + $(this).attr("for")).click();
			
			if ($(this).attr("for") == "tab-1"){
				$('.lt-ie9 #form-tabs .tab label').css({color: 'rgb(50, 112, 89)','background-color':'rgb(224, 234, 230)','z-index':2});
				$(this).css({color: 'rgb(0, 82, 127)','background-color':'rgb(202, 219, 212)','z-index':2});
			} else if ($(this).attr("for") == "tab-2"){
			   $('.lt-ie9 #form-tabs .tab label').css({color: 'rgb(50, 112, 89)','background-color':'rgb(224, 234, 230)','z-index':2});
				$(this).css({color: 'rgb(0, 82, 127)','background-color':'rgb(202, 219, 212)','z-index':2});
			} else if ($(this).attr("for") == "tab-3"){
			   $('.lt-ie9 #form-tabs .tab label').css({color: 'rgb(50, 112, 89)','background-color':'rgb(224, 234, 230)','z-index':2});
				$(this).css({color: 'rgb(0, 82, 127)','background-color':'rgb(202, 219, 212)','z-index':2});
			}
	//  $('#form-tabs label').removeclass('checked');
	//		   $(this).addclass('checked');
			
		});
	
				
		if (typeof(post["txt-contact-zipcode"])!='undefined') 		$("#tab-1").trigger("click");
		if (typeof(post["lst-contact-state"])!='undefined') 		$("#tab-2").trigger("click");
		if (typeof(post["lst-contact-company"]) !='undefined') 		$("#tab-3").trigger("click"); 
	
		if ($("body").hasClass("contact-all")==false) {
		
			var infowindow = new google.maps.InfoWindow({	content: ''	});
			
			function initialize() {
				
				
				var mapOptions = {	mapTypeId: google.maps.MapTypeId.ROADMAP };
				var map = new google.maps.Map(document.getElementById("contact-map"), mapOptions);
				var bounds = new google.maps.LatLngBounds ();
				var LatLngList = new Array ();
				var names=new Array();
				var markers=new Array();
				
				


				if ($(".location-info").length==1) {	// basically saying, if we are on the contact location details page and there is only 1 location	
					
					var lat=$(".location-info").attr("data-lat");
					var lng=$(".location-info").attr("data-lng");
					var name=$(".location-info").attr("data-name");
					var address=$(".location-info").attr("data-address");
					var city=$(".location-info").attr("data-city");
					var state=$(".location-info").attr("data-state");
					var zip=$(".location-info").attr("data-zip");
					var img=$(".location-info").attr("data-img");
					var id=$(".location-info").attr("data-locationid");
					
					var latlng=new google.maps.LatLng (lat,lng);
					
					var marker = new google.maps.Marker({
						position: latlng,
						map: map,
						title: name,
						content: '<div class="info-content"><h5 class="blue">' + name +'</h5><p>' + address + '<br>' + city + ', ' + state + ' ' + zip + '</p><img src="/img/locations/' + id +  '.jpg"></div>'
					});
					
					google.maps.event.addListener(marker, 'click', function() {
						infowindow.content=this.content;
						infowindow.open(map,this);
					});
					
					map.setCenter(latlng);
					map.setZoom(12);
					
					
				} else { // else if location_length isnt 1, so if we are on a contact location search results page where there is more than one location
				
					$(".location-info").each(function(index) {
						var lat=$(this).attr("data-lat");
						var lng=$(this).attr("data-lng");
						var name=$(this).attr("data-name");
						var address=$(this).attr("data-address");
						var city=$(this).attr("data-city");
						var state=$(this).attr("data-state");
						var zip=$(this).attr("data-zip");
						var img=$(this).attr("data-img");
						var id=$(this).attr("data-locationid");
						var latlng=new google.maps.LatLng (lat,lng);
						
						var marker = new google.maps.Marker({
							position: latlng,
							map: map,
							title: name,
							content: '<div class="info-content"><h5 class="blue">' + name +'</h5><p>' + address + '<br>' + city + ', ' + state + ' ' + zip + '</p><img src="/img/locations/' + id +  '.jpg"></div>'
						});
						
						google.maps.event.addListener(marker, 'click', function() {
							infowindow.content=this.content;
							infowindow.open(map,this);
						});
						
						names.push(name);
						markers.push(marker);
						LatLngList.push(latlng);
						bounds.extend(latlng); 
					}); // end foreach .location-info
			
					map.fitBounds (bounds);
				
				} // end 
			
			}
			google.maps.event.addDomListener(window, 'load', initialize);
		
		} // end check for contact-all
		




		
		$("#contact-confirm").click(function(e) {
			
			var form_error=false;
			
			var txt_contact_first=$("#txt-contact-first").val();
			var txt_contact_last=$("#txt-contact-last").val();
			var txt_contact_company=$("#txt-contact-company").val();
			var txt_contact_email=$("#txt-contact-email").val();
			var lst_contact_subject=$("#lst-contact-subject").val();
			var txt_contact_zip=$("#txt-contact-zip").val();
			var txt_contact_comments=$("#txt-contact-comments").val();
			
		    if (txt_contact_first.length==0) { form_error=true; $(".first.form-error").css("display","block");  }
				   else {$(".first.form-error").hide();}
			if (txt_contact_last.length==0) { form_error=true; $(".last.form-error").css("display","block");  }
				   else {$(".last.form-error").hide();}
		// if (txt_contact_company.length==0) { form_error=true;;$("#txt-contact-company").before('<span class="form-error">Please enter a company.</span>'); }
		//	   else {$(".name.form-error").hide();}
			if (!txt_contact_email.match(/\S+@\S+\.\S+/)) { form_error=true; $(".email.form-error").css("display","block"); } 
				 else {$(".email.form-error").hide();} 
			if (lst_contact_subject.length==0) { form_error=true; $(".subject.form-error").css("display","block");  }
				   else {$(".subject.form-error").hide();}
				   
		// if (txt_contact_zip.length==0) { form_error=true;;$("#txt-contact-zip").before('<span class="form-error">Please enter a zip code.</span>'); }
		//		   else {$(".name.form-error").hide();}
		// if (txt_contact_comments.length==0) { form_error=true;;$("#txt-contact-comments").before('<span class="form-error">Please enter a question/comment.</span>'); }
		//		   else {$(".name.form-error").hide();}
/*			
			if (txt_contact_first.length==0) { form_error=true;;$("#txt-contact-first").before('<span class="form-error">Please enter a first name.</span>'); }
			if (txt_contact_last.length==0) { form_error=true;;$("#txt-contact-last").before('<span class="form-error">Please enter a last name.</span>'); }
			if (txt_contact_company.length==0) { form_error=true;;$("#txt-contact-company").before('<span class="form-error">Please enter a company.</span>'); }
			if (txt_contact_email.length==0) { form_error=true;;$("#txt-contact-email").before('<span class="form-error">Please enter an e-mail address.</span>'); }
			if (lst_contact_subject.length==0) { form_error=true;;$("#lst-contact-subject").before('<span class="form-error">Please select a subject.</span>'); }
			if (txt_contact_zip.length==0) { form_error=true;;$("#txt-contact-zip").before('<span class="form-error">Please enter a zip code.</span>'); }
			if (txt_contact_comments.length==0) { form_error=true;;$("#txt-contact-comments").before('<span class="form-error">Please enter a question/comment.</span>'); }
*/			
			
			
			if (form_error==false) {
				//alert("here");
				$('#contact-lightbox-confirm').foundation('reveal', 'open', {
					url: '/index.php/contact/send_email',
					data: { first: txt_contact_first, last: txt_contact_last, company: txt_contact_company, email: txt_contact_email, subject: lst_contact_subject, zip: txt_contact_zip, comments: txt_contact_comments }
				});
				
				
				
			} // end form error check
			
			
		}); // end contact confirm click function
		
		
		
	} // end contact page check





	if ($("body").hasClass("contact-search-results") && $("body").hasClass("contact-all")==false) { // if the page is a contact search results PAGE but isnt the view all locations page
		
		$(".lt-ie9 .reveal-modal").bind("opened",function(){
			$(document).scrollTop(0); 
			$(document).foundation();
		});
		
		
		
		var new_url = '/index.php/contact/search/';		
		
		$(".pager a").each(function(index) {
			var href=$(this).attr("href");
			var link_page=href.substr(href.lastIndexOf("/")+1);
			var form_action=new_url + link_page;

			
			$(this).bind("click", function(e) {
				e.preventDefault();
				$("#frm_contact_search").attr("action", form_action);
				$("#frm_contact_search").submit();	
			});
			
		});
		
		
	}// end contacts search results check


	if ($("body").hasClass("contact") && $("body").hasClass("contact-all")==false) {
		
		var new_url = '/index.php/contact/search/';		
		
		$(".pager a").each(function(index) {
			var href=$(this).attr("href");
			var link_page=href.substr(href.lastIndexOf("/")+1);
			var form_action=new_url + link_page;
			
			$(this).bind("click", function(e) {
				e.preventDefault();
				$("#frm_contact_search").attr("action", form_action);
				$("#frm_contact_search").submit();	
			});
			
		});
		
		
	}// end contacts search results check
		if( isMobile() == true )
		{
			$('#contact-lightbox-confirm').css({'top':'100px'});
			$('.company-bio').css({'top':'100px'});
				
		}
	$(window).load(function(){
		
		$('#txt-zipcode-details').focus(function(){
				var dataid = $('#lst-state-details').attr('data-id');
				$('#lst-state-details').attr('selected','');
				$('['+dataid+']').find('li').removeClass('selected');				
		});
		
		
		if( $('.lt-ie9 .reveal-modal').length > 0 )
		{
			$('.close-reveal-modal').on({
				click:function(){
					$('html,body').animate({scrollTop: $(document).height()},0);
					
				}
			});
			$('.logo-wrapper').click(function(){
				$('html,body').animate({scrollTop: 50},'fast');
			});
		}
		
		$('.large-8').css({'width':'65%'});
	});
	
	 $(window).resize(function() {
		if(navigator.userAgent.indexOf('iPad') != -1)
		{
			if($(window).width() <  $(window).height()  ) 
			{// portrait
				$('.slider-content-title').css({'font-size':'26px'});
				
				$('.row .column, .row .columns').css({
					'padding-left': '.3em',
					'padding-right': '.3em'
				});
				$('h3.green').css({'font-size': '16px'});
				
				
				
			}
			else
			{
				$('.slider-content-title').css({'font-size':'32px'});
				
				$('.row .column, .row .columns').css({
					'padding-left': '0.9375em',
					'padding-right': '0.9375em'
				});
				$('h3.green').css({'font-size': '20px'});
			}
			
		}
	});
	if(navigator.userAgent.indexOf('iPad') != -1)
		{
			if($(window).width() <  $(window).height()  ) 
			{// portrait
				$('.slider-content-title').css({'font-size':'26px'});
				
				$('.row .column, .row .columns').css({
					'padding-left': '.3em',
					'padding-right': '.3em'
				});
				$('h3.green').css({'font-size': '16px'});
				
				
				
			}
			else
			{
				$('.slider-content-title').css({'font-size':'32px'});
				
				$('.row .column, .row .columns').css({
					'padding-left': '0.9375em',
					'padding-right': '0.9375em'
				});
				$('h3.green').css({'font-size': '20px'});
			}
			
		}
}); // end document ready