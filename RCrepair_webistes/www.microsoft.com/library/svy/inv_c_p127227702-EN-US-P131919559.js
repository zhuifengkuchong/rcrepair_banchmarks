/*
Copyright (c) 2012, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
											{ 	methodology: 2,
					projectId: 'p127227702',
					weight: 100,
					isRapidCookie: false,
					acceptUrl: 'http://survey2.surveysite.com/wix/p127227702.aspx',
					secureUrl: '',
					acceptParams: {
						raw: 'hsite=173',
						siteCode: '173',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: 'http://web.survey-poll.com/tc/CreateLog.aspx',
					viewParams: 'http://www.microsoft.com/library/svy/log=comscore/view/p127227702-view.log',
					content: '<table width="350" cellpadding="1" cellspacing="0" border="0" style="background:#CCCCCC"><tr><td> <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#999999"><tr><td> <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background:#FFFFFF"><tr valign="top"><td style="text-align:left"> <img src="new-windowsphone-logo.jpg"/*tpa=http://www.microsoft.com/library/svy/new-windowsphone-logo.jpg*/ /><table width="100%" cellpadding="0" cellspacing="0"><tr><td style="background:#E51400; border:0">  <div style="font-family: Segoe UI Semibold;	font-size: 12px; color: #FFFFFF; margin: 3px 4px 3px 4px;">Hi! <br />You can help us make windowsphone.com better. We\'d like to see which pages you visit today, and then we\'ll ask you to take a short survey (it\'ll only take a few minutes).<br /><br />Would you like to participate? </div>  <div align="center" style="margin: 10px 0; padding: 0"> <input type="image" src="yes-wp.jpg"/*tpa=http://www.microsoft.com/library/svy/yes-wp.jpg*/ onclick="@acceptHandler" />&nbsp;&nbsp; <input src="no-wp.jpg"/*tpa=http://www.microsoft.com/library/svy/no-wp.jpg*/  type="image" onclick="@declineHandler" /> </div>  <div style="font-family: Segoe UI Semibold;	font-size: 12px; color: #FFFFFF; margin: 3px 0 3px 4px;"><a href="http://www.microsoft.com/info/privacy.mspx" target="_blank" style="color:#FFFFFF;text-decoration:none">Privacy Statement</a></div>  </td></tr></table> </td></tr></table> </td></tr></table> </td></tr></table>   ',
					height: 225,
					width: 370,
					revealDelay: 1000,
					horizontalAlignment: 2,
					verticalAlignment: 2,
					horizontalMargin: 0,
					verticalMargin: 0,
					isHideBlockingElements: false,
					isAutoCentering: false,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 5
					,usesSilverlight: false
					
					//silverlight config
										
											,trackerWindow: {
							width: 400,
							height: 270,
							orientation: 1,
							offsetX: 0,
							offsetY: 0,
							hideToolBars: true,
							trackerPageConfigUrl: 'trackerConfig_p127227702-EN-US.js'/*tpa=http://www.microsoft.com/library/svy/trackerConfig_p127227702-EN-US.js*/
							// future feature: 
							//features: "location=0,menubar=0,resizable=1,scrollbars=1,toolbar=0"
						}
																				,Events: {
						beforeRender: function() {
														
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
						]
};
COMSCORE.SiteRecruit.Builder.run();
