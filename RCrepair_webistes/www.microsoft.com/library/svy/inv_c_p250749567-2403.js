/*
Copyright (c) 2014, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
											{ 	methodology: 2,
					projectId: 'p250749567',
					weight: 100,
					isRapidCookie: false,
					acceptUrl: 'http://survey2.surveysite.com/wix/p250749567.aspx',
					secureUrl: '',
					acceptParams: {
						raw: '',
						siteCode: '2403',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: 'http://b.scorecardresearch.com/b',
					viewParams: 'http://www.microsoft.com/library/svy/log=comscore/view/p87492647-view.log',
					content: '<div style="width:366px; border: 1px solid #3b3b3b;background-color:#FFF;"> 	<div style="position:relative; top:12px; left:56px"> 		<div style="height:44px;font-family:\'Segoe UI Light\'; font-size:24px; color:#000;"> 			Please help us improve. 			<div style="position:relative; top: -22pt; left:260px; width: 26; height: 26;"><a href="javascript:void(0);" onclick="@declineHandler"><img src="close_v3.gif"/*tpa=http://www.microsoft.com/library/svy/close_v3.gif*/ border="0"/></a></div> 		</div> 		<div style="font-family:\'Segoe UI\'; font-size:12px; color:#000; width:250px;line-height:14px;">Microsoft is conducting an online survey to better understand your needs of the Windows Embedded web site. To do so, we\'d like to ask you a few simple questions that will appear when you leave the site. <br /><br /> Would you like to participate?  		</div> 		<div style="width:300px;height:38px;margin-top:28px;"> 		 			<div style="width:91px;" > 			       	<input name="button" type="button" value=" I want to " style="border-style:none; background-color:#000000; font-family:\'Segoe UI\'; font-size:12px; color:#FFF; height:34px;width:91px;cursor:pointer;" onclick="@acceptHandler;"/>     	</div> 	       	<div style="position:relative; top: -34px; left:147px; width:91px;height:34px;">     		<input name="button" type="button" value=" No thanks " style="border-style:none; background-color:#000000; font-family:\'Segoe UI\'; font-size:12px; color:#FFF; height:34px;width:91px;cursor:pointer;" onclick="@declineHandler;"/>	     	</div> 	  		   	</div>  		<div style="margin-top:10px;"><img src="microsoft-logo-small.png"/*tpa=http://www.microsoft.com/library/svy/microsoft-logo-small.png*/ style="display:inline">  			<div style="position:relative; top: -20px; left:167px;width:150px;"><a href="http://www.microsoft.com/info/privacy.mspx" style="font-family:\'Segoe UI\'; font-size:10px; color:#0072bc; text-decoration:none;" target="_blank">Privacy Statement</a></div> 		</div><br /> </div></div> ',
					height: 268,
					width: 366,
					revealDelay: 1000,
					horizontalAlignment: 1,
					verticalAlignment: 1,
					horizontalMargin: 0,
					verticalMargin: 0,
					startingHorizontalAlignment: 1,
					startingVerticalAlignment: 1,
					startingHorizontalMargin: 0,
					startingVerticalMargin: 0,
					isHideBlockingElements: false,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 5,
					usesSilverlight: false
					
					//silverlight config
										
											,trackerWindow: {
							width: 400,
							height: 270,
							orientation: 1,
							offsetX: 0,
							offsetY: 0,
							hideToolBars: true,
							trackerPageConfigUrl: 'trackerConfig_p250749567-2403.js'/*tpa=http://www.microsoft.com/library/svy/trackerConfig_p250749567-2403.js*/
							// future feature: 
							//features: "location=0,menubar=0,resizable=1,scrollbars=1,toolbar=0"
						}
																				,Events: {
						beforeRender: function() {
							var SR_url = window.location.toString().toLowerCase();
							if(/internet-of-things\.aspx/.test(SR_url)){	
								COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.siteCode ='2629';
							}else if(/internet-of-things-retail\.aspx/.test(SR_url)){
								COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.siteCode ='2630';
							}else if(/internet-of-things-manufacturing\.aspx/.test(SR_url)){
								COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.siteCode ='2631';
							}else if(/internet-of-things-health\.aspx/.test(SR_url)){
								COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.siteCode ='2632';
							}else if(/internet-of-things-citynext\.aspx/.test(SR_url)){
								COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.siteCode ='2633';
							}else if(/internet-of-things-customer-stories\.aspx\?id=1/.test(SR_url)){
								COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.siteCode ='2634';
							}else if(/internet-of-things-customer-stories\.aspx\?id=2/.test(SR_url)){
								COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.siteCode ='2635';
							}else if(/internet-of-things-customer-stories\.aspx\?id=3/.test(SR_url)){
								COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.siteCode ='2636';
							}else if(/internet-of-things-customer-stories\.aspx\?id=4/.test(SR_url)){
								COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.siteCode ='2637';
							}else if(/internet-of-things-customer-stories\.aspx\?id=5/.test(SR_url)){
								COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.siteCode ='2638';
							}else if(/internet-of-things-customer-stories\.aspx\?id=6/.test(SR_url)){
								COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.siteCode ='2639';
							}else if(/internet-of-things-customer-stories\.aspx\?id=7/.test(SR_url)){
								COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.siteCode ='2640';
							}else if(/internet-of-things-customer-stories\.aspx\?id=8/.test(SR_url)){
								COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.siteCode ='2641';
							}else if(/windows-embedded-8\.aspx/.test(SR_url)){
								COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.siteCode ='2642';
							}else if(/windows-embedded-8-industry\.aspx/.test(SR_url)){
								COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.siteCode ='2643';
							}else if(/windows-embedded-8-standard\.aspx/.test(SR_url)){
								COMSCORE.SiteRecruit.Builder.invitation.config.acceptParams.siteCode ='2644';
							}
						}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}
						]
};

if(/internet-of-things-customer-stories\.aspx.*id=.*enav=.*autoplay=1.*/i.test(SR_url)){
 	setTimeout("COMSCORE.SiteRecruit.Builder.run();", 110000);
}else{
	COMSCORE.SiteRecruit.Builder.run();
}