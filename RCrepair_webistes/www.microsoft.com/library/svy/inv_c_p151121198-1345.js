/*
Copyright (c) 2014, comScore Inc. All rights reserved.
version: 5.0.3
*/
COMSCORE.SiteRecruit.Builder.config = {
	version: "5.0.3",
	
	// invitations' settings
	invitation: [
							
											{ 	methodology: 2,
					projectId: 'p151121198',
					weight: 100,
					isRapidCookie: false,
					acceptUrl: 'https://survey2.securestudies.com/wix/p151121198.aspx',
					secureUrl: 'https://survey2.securestudies.com/wix/p151121198.aspx',
					acceptParams: {
						raw: 'hsite=1345',
						siteCode: '1345',
												cookie: [
													],
												otherVariables: [
													]
					},
					viewUrl: 'http://b.scorecardresearch.com/b',
					viewParams: 'http://www.microsoft.com/library/svy/log=comscore/view/p151121198-view.log',
					content: '<table width="360" cellpadding="3" cellspacing="0" border="0" bgcolor="#FFFFFF"><tr><td style="padding: 3px;"> <table width="360" cellpadding="1" cellspacing="0" border="0" bgcolor="#999999"><tr><td style="padding: 1px;"> <table width="360" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF"><tr valign="top"><td> <img src="logo-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/logo-stripe.gif*/ /><a href="javascript:void(0);" onclick="@declineHandler"><img border="0" src="close.gif"/*tpa=http://www.microsoft.com/library/svy/close.gif*/ /></a><br /> <img src="top-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/top-stripe.gif*/ /> <table width="340" cellpadding="5"><tr><td>  <div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 11px; color: #000000; margin: 0 0 15px 0;">Microsoft is conducting an online survey to understand your opinions of the Microsoft.com Download Center.  If you choose to participate, the online survey will be presented to you when you leave the Microsoft.com Download Center. <br /><br /> Would you like to participate?  </div>  <div align="center" style="margin: 0; font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 11px; padding: 0"> <input style="margin: 0; padding: 0" type="button" value="  Yes  " onclick="@acceptHandler" />&nbsp;&nbsp; <input style="margin: 0; padding: 0"  type="button" value=" No " onclick="@declineHandler" /> </div>  <div style="font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 11px; color: #000000; margin: 15px 0 2px 0;"><a href="http://privacy.microsoft.com/en-us/default.mspx" target="_blank">Privacy Statement</a></div>  </td></tr></table> <img src="bottom-stripe.gif"/*tpa=http://www.microsoft.com/library/svy/bottom-stripe.gif*/ /></td></tr></table> </td></tr></table> </td></tr></table>   ',
					height: 210,
					width: 390,
					revealDelay: 1000,
					horizontalAlignment: 1,
					verticalAlignment: 1,
					horizontalMargin: 0,
					verticalMargin: 0,
					startingHorizontalAlignment: 1,
					startingVerticalAlignment: 0,
					startingHorizontalMargin: 0,
					startingVerticalMargin: 0,
					isHideBlockingElements: true,
					isAutoCentering: true,
					url: 'http://www.microsoft.com/library/svy/SiteRecruit_Tracker.htm',
					trackerGracePeriod: 5,
					usesSilverlight: false
					
					//silverlight config
										
											,trackerWindow: {
							width: 400,
							height: 270,
							orientation: 1,
							offsetX: 0,
							offsetY: 0,
							hideToolBars: true,
							trackerPageConfigUrl: 'trackerConfig_p151121198.js'/*tpa=http://www.microsoft.com/library/svy/trackerConfig_p151121198.js*/
							// future feature: 
							//features: "location=0,menubar=0,resizable=1,scrollbars=1,toolbar=0"
						}
																				,Events: {
						beforeRender: function() {
														document.onkeyup = function(evt) {
    evt = evt || window.event;
    if (evt.keyCode == 27) {
        COMSCORE.SiteRecruit.Builder.invitation.decline(this);
    }
};
													}
						,beforeDestroy: function() {
														
													}
						,beforeAccept: function() {
														
													}
						,beforeDecline: function() {
														
													}
						,beforeRenderUpdate: function() {
														
													}
						,afterRender: function() {
														
													}
					}
				}

						]
};
COMSCORE.SiteRecruit.Builder.run();