/*
OnlineOpinion v5.7.3
Released: 6/4/2013. Compiled 06/04/2013 08:37:25 AM -0500
Branch: master 122a760d8e979af7090004b5d3cb086d5b0896be
Components: Full
The following code is Copyright 1998-2013 Opinionlab, Inc.  All rights reserved. Unauthorized use is prohibited. This product and other products of OpinionLab, Inc. are protected by U.S. Patent No. 6606581, 6421724, 6785717 B1 and other patents pending. http://www.opinionlab
*/
if(typeof opinionLabOrderId != 'undefined'){
	var oo_orderComplete = new OOo.Ocode({
		customVariables: {
		  coreID6: OOo.readCookie('CoreID6')
		, timestamp: new Date().getTime()
		, orderId: opinionLabOrderId
		, email: opinionLabEmail
		, total: opinionLabTotal
		, promotion: opinionLabPromoAmount
		, giftWrap: opinionLabGiftWrapTotal
		, numberOfItems: opinionLabNumberOfItems
	  }, referrerRewrite: {
          searchPattern: /:\/\/[^\/]*/
        , replacePattern: '://online.postpurchasesurvey.dillards.com'
      }
	});
}
var oo_feedback = new OOo.Ocode({
  customVariables: {
	  coreID6: OOo.readCookie('CoreID6')
	, timestamp: new Date().getTime()
  }
});

/* [+] Tab Icon configuration */
var oo_tab = new OOo.Ocode({
  tab: {}
, customVariables: {
	  coreID6: OOo.readCookie('CoreID6')
	, timestamp: new Date().getTime()
  }
});
/*global window, document, OOo*/
