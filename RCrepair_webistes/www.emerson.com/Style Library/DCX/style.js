;DCXEMERSON = {
    name : "DCXEMERSON",

    sbSocialLinks: function(){
        try{
	        var targetURL   = window.location.href;
	        var targetTitle = document.title;
	        var pipeCleaner = '';
			
			if (targetTitle != undefined){
                pipeCleaner = targetTitle.split('|');
            }

			var linkedinGrab    = $("div.emerson-content-linkedin")
			var linkedinContent = "";
			var facebookGrab    = $("div.emerson-content-facebook");
			var facebookContent = "";
			var twitterGrab     = $("div.emerson-content-twitter");
			var twitterContent  = "";
			
			if(linkedinGrab.length > 0){
				linkedinContent = linkedinGrab.get(0).textContent;
			}
			if(facebookGrab.length > 0){
				facebookContent = facebookGrab.get(0).textContent;
			}
			if(twitterGrab.length > 0){
				twitterContent  = twitterGrab.get(0).textContent;
			}			

            var linkedInUrl = "https://www.linkedin.com/shareArticle?mini=true&url=" + targetURL +  "&title=" + linkedinContent;
            var facebookUrl = "http://www.facebook.com/sharer.php?u=" + targetURL + "&t=" + facebookContent;
			var twitterUrl  = "https://twitter.com/intent/tweet?url=" + targetURL + "&text="  + twitterContent;
            var mailTo = "mailto:?subject=" + pipeCleaner[0] + pipeCleaner[1] + "&body=" + targetURL;       
            var twitterTag = $("span.icon-square-twitter").get(0);
            var facebookTag = $("span.icon-square-facebook").get(0);
            var linkedinTag = $("span.icon-square-linkedin").get(0);
            var sharedLink = $("a.lnk-share").get(0);
            var printLink = $("span.icon-printer").get(0);

            if (twitterTag != undefined){
                var twitterAnchor = twitterTag.parentNode;
                if (twitterAnchor != undefined && twitterAnchor.tagName.toLowerCase() == "a") {
                    twitterAnchor.href = twitterUrl;
                    twitterAnchor.target = "_blank";
                }
            }
            if (facebookTag != undefined){
                var facebookAnchor = facebookTag.parentNode;
                if (facebookAnchor != undefined && facebookAnchor.tagName.toLowerCase() == "a") {
                    facebookAnchor.href = facebookUrl;
                    facebookAnchor.target = "_blank";
                }
            }
            if (linkedinTag != undefined){
                var linkedinAnchor = linkedinTag.parentNode;
                if (linkedinAnchor != undefined && linkedinAnchor.tagName.toLowerCase() == "a") {
                    linkedinAnchor.href = linkedInUrl;
                    linkedinAnchor.target = "_blank";
                }
            }
            if (printLink != undefined){
                var printanchor = printLink.parentNode;
                if (printanchor != undefined && printanchor.tagName.toLowerCase() == "a") {
                    printanchor.target = "_blank";
                    printanchor.onclick = function() {
                        window.print();
                    };
                }
            }

            if (sharedLink != undefined){
                sharedLink.href = mailTo;
                sharedLink.target = "_blank";
            }
        }catch(err){
            //console.log("Error Caught: "+err.message);
        }finally{
            //console.log("Ran sbSocialLinks");
        }        
    },
    
    offcanvasNav : function(){
        try{
            var $mm = $('.mobile-menu');
            var $submenus = $('ul', $mm);
            $submenus.each(function() {
                var $toggler = $(this).siblings('a').first();
                var $action = $toggler.attr('href');
                var $submenu = $(this);
                var $parentAccordion = $toggler.closest('ul');
                $toggler.closest('li').addClass("panel");
                $toggler.attr('data-parent', "#" + $parentAccordion.attr("id"))
                        .attr('data-target', "#" + $submenu.attr('id'))
                        .attr('data-toggle', "collapse")
                        .addClass('collapsed toggler');
                $submenu.addClass('collapse submenu');
                
                $toggler.on('click', function(event){
                    if($toggler.hasClass('collapsed')){
                    event.preventDefault();
                    }           
                });
                        
            });
            $('#menu-toggler').on('click', function(event) {
                $nav = $('.header-nav .mm');
                $nav.toggleClass('open');
                event.preventDefault();
            });

            var offcanvas_open = false;
            $('[data-toggle="offcanvas"]').click(function(e) {
                $('.row-offcanvas').toggleClass('active');
                offcanvas_open = !offcanvas_open;
                e.preventDefault();
                e.stopPropagation();
            });
            $('.row-offcanvas-content').on('click', function(e) {
                if (offcanvas_open) {
                    $('.row-offcanvas').toggleClass('active');
                    offcanvas_open = false;
                }
            });
            $('.mobile-social').append($('.header-social-items li').clone());
            $('.mobile-menu-secondary').append($('.menu-secondary ul.links > li').clone());
        }catch(err){
            //console.log("Error Caught: "+err.message);
        }finally{
            //console.log("Ran offcanvasNav");
        }     
    },
    
    hero : function(){
       try{
	        $('.hero-img').each(function() {
	            var $heroImg = $('img', this);
	            $(this).css('background-image', 'url(' + $heroImg.attr('src') + ')');
	            $heroImg.remove();
	        }); 
        }catch(err){
        	//console.log("Error Caught: "+err.message);
        }finally{
        	//console.log("Ran hero");
        }     
    },

    decoration : function(){
	    try{
	        $('[id^="container-dp"] .inside > ul.spots > *:not(:last-child)').each(function() {
	            $(this).closest('ul').prepend("<li class='division-decoration'></li>");
	        });
        }catch(err){
        	//console.log("Error Caught: "+err.message);
        }finally{
        	//console.log("Ran decoration");
        }     
    },

    megaMenu : function(){       
        try{
			$('#DeltaTopNavigation li.static.dynamic-children').each(function() {
			    var $originalDeptItems = $("> ul.dynamic", this);
			    var $deptItems = $(">li", $originalDeptItems);
			    $deptItems.removeClass().find("*").removeClass();
			    $originalDeptItems.find(">li>a").addClass("menuLink");
			    var $wrapper = $("<div class='departmentMenu'></div>");
			    for (var i = 0; i < $deptItems.length; i += 3) {
			        var temp = $deptItems.slice(i, i + 3);
			        var tempWrapped = $("<ul class='categoryList'></ul>").append(temp);
			        $wrapper.append(tempWrapped);
			    }
			    $(this).append($wrapper);
			    $originalDeptItems.remove();
			});
			
			departmentImgs = $('#DeltaTopNavigation li.static.dynamic-children a[href*=_api]').toArray();
			DCXEMERSON.xmlNavLoad(departmentImgs);
		}catch(err){
        	//console.log("Error Caught: "+err.message + "\n" +err.stack);
        }finally{
        	//console.log("Ran megamenu");
        }     

    },

    xmlNavLoad : function(departmentImgs){
	   	try{
	   		if(departmentImgs.length === 0){
	   			//console.log('Finished loading all the XMLs for the Nav DepartmentImages');
	   		}else{
	   			var xmlSrc = $(departmentImgs[departmentImgs.length-1]).attr('href');
	   			//console.log('Loading XML for '+xmlSrc);
	   			var xmlRequest = $.ajax({
				  type: "GET",
				  dataType: "xml",
				  url: xmlSrc
				});
				 
				xmlRequest.success(function(data){
			    	var $link = $(departmentImgs.pop());
			    	var $parent = $link.closest('.categoryList');
			    	$link.closest('li').remove();
			    	var reusableHtml = $('content', data).find("d\\:ReusableHtml, ReusableHtml").first();
			    	var $reusableHtml = $("<div/>").html(reusableHtml).text();
			    	$parent.closest('.departmentMenu').prepend($reusableHtml);
			    	
                    if( $parent.find('li').length === 0){
			    		$parent.remove();
			    	}

			    	DCXEMERSON.xmlNavLoad(departmentImgs);
			    });
				 
				xmlRequest.error( function(data){
			    	var $link = departmentImgs.pop();
			    	xmlNavLoad();
			    	//console.err('Error while retrieving XML '+$link.attr('href'));
			    });
	   		}
		}catch(err){
        	//console.log("Error Caught: "+err.message + "\n" +err.stack);
        }finally{
        	//console.log("Ran megamenu");
        }  		
   	},
     
    ads : function(){
	    try{
	        var $promotionSpots = $('.ev-promotion-spot');
	        if (typeof $promotionSpots !== "undefined") {
	            var columns = 12 / $promotionSpots.length;
	            $promotionSpots.addClass('col-sm-' + columns);
	            $('#ev-promotion-bar').slideDown('slow');
	        }
        }catch(err){
        	//console.log("Error Caught: "+err.message);
        }finally{
        	//console.log("Ran ads");
        }     
    },

    emptyLinks : function(){
	    try{
	        $("a[href='#'], a[href='']").click(function(event) {
	            event.preventDefault();
	        });
        }catch(err){
        	//console.log("Error Caught: "+err.message);
        }finally{
        	//console.log("Ran emptyLinks");
        }     
    },

    adjustBootstrapTabs : function(){
        try{
	        // Bootstrap tab adjust -
	        // http://stackoverflow.com/questions/18978545/bootstrap-nav-header-height
	        var highest = null;

	        $(".nav-tabs a")
	            .each(
	                function() {
	                    var h = $(this).height();
	                    if (h > highest) {
	                        highest = $(this).height();
	                        highest = $(this).height();
	                    }
	                    $(this)
	                        .wrapInner(
	                            '<span style="display:table-cell;vertical-align: middle;"></span>');
	                    $(this)
	                        .wrapInner(
	                            '<span style="display:table;width: 100%;"></span>');
	                });
	        $(".nav-tabs a > span").height(highest); //set all your links to that height.    
    	}catch(err){
        	//console.log("Error Caught: "+err.message);
        }finally{
        	//console.log("Ran adjustBootstrapTabs");
        }     
    },

    pdpAdjustment : function(){
        try{
	        $('.quantity_section').after($('.field_group.ev-prd-availability'));
        }catch(err){
        	//console.log("Error Caught: "+err.message);
        }finally{
        	//console.log("Ran pdpAdjustment");
        }     
    },

    sidebarLeft : function(){
        try{
	        if($('.sidebar-left').length>0){
	            var sidebarID = 'ev-sidebar-left';
	            var $sidebar = $('.sidebar-left');
	            $sidebar
	                .addClass('collapse')
	                .attr('id',sidebarID);
	            var $sidebar_toggler = $('.hero-wrapper')
	            $sidebar_toggler.addClass('sidebar-toggler');
	            $sidebar_toggler.attr('data-target','#'+sidebarID);
	            $sidebar_toggler.attr('data-toggle','collapse');    
	        }
    	}catch(err){
        	//console.log("Error Caught: "+err.message);
        }finally{
        	//console.log("Ran sidebarLeft");
        }     
    },
    
    shareThisSpan : function( spanClass, spanDisplayText ){
        return $('<span>', {"class" : spanClass, 
                            "displayText" : spanDisplayText
        });            
    },

    socialEndCap : function(){
        try{           
            $shareThisContainer = $("<div>", {
                class:"shareThisContainer"
            });

            $shareThisContainer.append(DCXEMERSON.shareThisSpan('st_linkedin_hcount', 'LinkedIn'));
            $shareThisContainer.append(DCXEMERSON.shareThisSpan('st_facebook_hcount', 'Facebook'));
            $shareThisContainer.append(DCXEMERSON.shareThisSpan('st_twitter_hcount', 'Tweet'));
            $shareThisContainer.append(DCXEMERSON.shareThisSpan('st_googleplus_hcount', 'Google +'));
            $shareThisContainer.append(DCXEMERSON.shareThisSpan('st_pinterest_hcount', 'Pinterest'));
            $shareThisContainer.append(DCXEMERSON.shareThisSpan('st_email_hcount', 'Email'));
            $('#container-dpe .spot-7, #container-dpd .spot-3').append($shareThisContainer);            

            var switchTo5x=true;
            $.getScript("../../../w.sharethis.com/button/buttons.js"/*tpa=http://w.sharethis.com/button/buttons.js*/, function(){
               stLight.options({publisher: "e668094b-708c-42e7-9f18-7ddabeb0e934", doNotHash: false, doNotCopy: false, hashAddressBar: false});
            });
        }catch(err){
            //console.log("Error Caught: "+err.message);
        }finally{
            //console.log("Ran socialEndCap");
        }     
    },

    seo : function(){
    	try{
	    	//Accordion SEO and Meta
			var $seo = $('#ctl00_PlaceHolderMain_EditModePanel1');
			var $seoToggler = $('<a id="ms-seo-toggler">SEO and Meta Info</a>');
			var $seoTogglerContainer = $('<div id="ms-seo-toggler-container" class="container"></div>');
			$seoTogglerContainer.append($seoToggler);
			$seo.before($seoTogglerContainer);
			$seoToggler.attr('href', $seo.attr('id'));
			$seoToggler.on('click', function(e) {
			    $seo.slideToggle()
			    e.preventDefault();
			});
			$seo.hide();
		}catch(err){
        	//console.log("Error Caught: "+err.message);
        }finally{
        	//console.log("Ran seo");
        }     
    },

    tabControl : function (){
    	try{

			var tabchcking = document.getElementById('HdnTabInformation')
			if (typeof(tabchcking) != 'undefined' && tabchcking!= null){
				var $tabControl = $('#' + (tabchcking).value.substring(2));
			    $tabControl.each(function(){
			        var $tabControl_List = $('ul', this);
			        var $tabContent = $('.innercontent', this);
			        var $tabContent_Elements = $('.innercontent > div', this);

			        $tabControl_List.addClass('nav nav-tabs nav-justified nav-pills');
			        $tabContent.addClass('tab-content');
			        $tabContent_Elements.addClass('tab-pane');
			        var tabLength = $('li', $tabControl_List).length;

			        $('a', $tabControl_List).each(function(index, value){
			            var $linkedContent = $tabContent_Elements.eq(index);
			            var $link = $linkedContent.attr('id');
			            $(this).attr('href',"#"+$link);
			            $(this).click(function(e){
			                e.preventDefault();
			                $(this).tab('show');
			            });            
			            if(index==0){
			                $(this).click();
			            }
			        });
			    });
			}
		}catch(err){
        	//console.log("Error Caught: "+err.message);
        }finally{
        	//console.log("Ran tabControl");
        }     
    },

    linkHandler : function(){
	    $("a").each(function () {
	        var targetTest = $(this).attr("href");
	        if (targetTest != null) {
	            if (targetTest.indexOf('?popup=true') >= 0) {
	                $(this).attr("target", "_blank");
	            }
	        }
	    });
    },

    viewMoreBlocks : function(){
		try{
			if($('#container-lpf').length !== 0){
				var spots = '#container-lpf #main-content .inside [class^="spot-"]';
				var btnMore = '#btnMore';
				$(spots).hide();
				var countSpots = $(spots).length;
				var initialActiveSpots = 6;
				var currentActiveSpots = 0;
				var stepVisibleSpots = 8;
				function activateSpots(counter) {
					$(spots + ':not(.active)').slice(0, counter)
							.addClass('active').fadeIn();
					currentActiveSpots = $(spots + '.active').length;
					if (currentActiveSpots >= countSpots) {
						$(btnMore).hide();
					}
				}
				$(btnMore).on('click', function(event) {
					activateSpots(stepVisibleSpots);
					event.preventDefault();
				});
				activateSpots(initialActiveSpots);
			}
		}catch(err){
        	//console.log("Error Caught: "+err.message);
        }finally{
        	//console.log("Ran viewMoreBlocks");
        }  
    },
    
    variationStuff: function(){
		try{
			var windowURL = window.location.href;
			var windowURLSplit = windowURL.split(".com/");
			var leftOversSplit = windowURLSplit[1].split("/");
			var denotedLanguage = leftOversSplit[0];
			var menuPanelGrabENUS = document.getElementsByClassName('grabbin-da-panel-en-us');
			var menuPanelGrabZHCN = document.getElementsByClassName('grabbin-da-panel-zh-cn');
			var menuPanelGrabENCN = document.getElementsByClassName('grabbin-da-panel-en-cn');
			var menuPanelGrabENIN = document.getElementsByClassName('grabbin-da-panel-en-in');
			var menuPanelGrabENSEA = document.getElementsByClassName('grabbin-da-panel-en-sea');
			var menuPanelGrabESLA = document.getElementsByClassName('grabbin-da-panel-es-la');
			var count = 0;
			var rrr = 0;
			switch (denotedLanguage) {
				case "en-us":
					count = menuPanelGrabENUS.length;
					for(rrr = 0; rrr <= count ; rrr++){
						menuPanelGrabENUS[rrr].style.display = "";
					}
					break;
				case "zh-cn":
					count = menuPanelGrabZHCN.length;
					for(rrr = 0 ; rrr <= count ; rrr++){
						menuPanelGrabZHCN[rrr].style.display = "";
					}
					break;
				case "en-cn":
					count = menuPanelGrabENCN.length;
					for(rrr = 0; rrr <= count ; rrr++){
						menuPanelGrabENCN[rrr].style.display = "";
					}
					break;
				case "en-in":
					count = menuPanelGrabENIN.length;
					for(rrr = 0; rrr <= count ; rrr++){
						menuPanelGrabENIN[rrr].style.display = "";
					}
					break;
				case "en-sea":
					count = menuPanelGrabENSEA.length;
					for(rrr = 0; rrr <= count ; rrr++){
						menuPanelGrabENSEA[rrr].style.display = "";
					}
					break;
				case "es-la":
					count = menuPanelGrabESLA.length;
					for(rrr = 0; rrr <= count ; rrr++){
						menuPanelGrabESLA[rrr].style.display = "";
					}
					break;
			}//end switch
		}catch(err){
			//console.log("Error:" + err.message);
		}finally{
			//console.log("ran variationStuff" + denotedLanguage)
		}//end try catch finally 
	},//end variationStuff function
	
	fixTheBrokenStuff: function(){
		try{
			var menuPanelGrabENUS = document.getElementsByClassName('grabbin-da-panel-en-us');
			var menuPanelGrabZHCN = document.getElementsByClassName('grabbin-da-panel-zh-cn');
			var nodesArray = Array.prototype.slice.call(menuPanelGrab);
			var iii = '';
			for(iii = 0 ; iii <= nodesArray.length ; iii++){
				if(nodesArray[iii].id.length > 0){
					nodesArray.splice(iii, 1);
					iii = iii - 1;
				}
			}
		}catch(err){
			//console.log("Error:" + err.message);
		}finally{
			//console.log("ran fixTheBrokenStuff")
		}//end try catch finally 
	},//end fixTheBrokenStuff function
	
	stockTickerLinkFix: function(){
		try{
			var grabWindowURL = window.location.href;
			var grabWindowURLSplit = grabWindowURL .split(".com/");
			var grabbedURLCheck = 'http://www.emerson.com/Style Library/DCX/en-us/Investors/Pages/investors.aspx';
			var stockRangeText = $('span.stock-range-text');
			if(grabWindowURLSplit[1] == grabbedURLCheck){
				$('a.stock-link').attr('href','http://www.emerson.com/en-us/Investors/Pages/stock-quote-chart.aspx');
				stockRangeText[0].innerText = '52wk High:';
				stockRangeText[1].innerText = '52wk Low: ';
			}
		}catch(err){
			//console.log("Error:" + err.message);
		}finally{
			//console.log("ran stockTickerLinkFix")
		}//end try catch finally 
	},//end stockTickerLinkFix function

	megaMenuLinkFix: function(){
		try{
			var grabbinDaMenuLinks     = $( ".menuLink");
			var grabbinDaMenuLinksText = '';
			var iii = '';
			for(iii = 0 ; iii < grabbinDaMenuLinks.length ; iii++){
				grabbinDaMenuLinksText = grabbinDaMenuLinks[iii].innerText;
				grabbinDaMenuLinks[iii].innerText = grabbinDaMenuLinksText;
			}
			var grabThemListAttributeTexts = $(".categoryList").find("ul li a").get();
			var themListItemsText = '';
			var iELineBreakCheck = '';
			var ppp = '';
			for(ppp = 0 ; ppp < grabThemListAttributeTexts .length ; ppp++){
				themListItemsText = grabThemListAttributeTexts[ppp].innerText;
				iELineBreakCheck = themListItemsText.split('\r\n');
				if(iELineBreakCheck.length === 1){
					grabThemListAttributeTexts[ppp].innerText = themListItemsText;
				}
				if(iELineBreakCheck.length === 2){
					grabThemListAttributeTexts[ppp].innerText = iELineBreakCheck[1];
				}
			}
		}catch(err){
			//console.log("Error:" + err.message);
		}finally{
			//console.log("ran megaMenuLinkFix")
		}//end try catch finally 
	}//end fixTheBrokenStuff function
}

$(document).ready(function(){	
    DCXEMERSON.variationStuff();
    DCXEMERSON.offcanvasNav();
    DCXEMERSON.hero();
    DCXEMERSON.decoration();
    DCXEMERSON.seo();
    DCXEMERSON.megaMenu();
    DCXEMERSON.tabControl();
    DCXEMERSON.linkHandler();
    DCXEMERSON.sbSocialLinks();
    DCXEMERSON.socialEndCap();	
    //DCXEMERSON.ads();
    DCXEMERSON.sidebarLeft();
	DCXEMERSON.viewMoreBlocks();
	DCXEMERSON.stockTickerLinkFix();
	DCXEMERSON.megaMenuLinkFix();
});