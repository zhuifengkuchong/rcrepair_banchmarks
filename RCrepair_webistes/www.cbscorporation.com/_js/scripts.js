$(document).ready(function(){


$("#nav ul li:first-child").css("background","none");
$("#nav ul.drop-meu li:first-child").css("border-top","none");
$("#nav li#navi-portfolio ul.drop-meu li:first-child").next("li").css("border-top","none");
$("#footerLinks li:first-child").css("border-left","none");
$("#sub-navi ul li:first-child").css("border-top","none");
//$("#nav ul li.current a").parent("li").next("li").css("background","none");


if($("#nav li")){
$("#nav li").not($("#nav li li")).mouseover(
	function() {
		var cssObj = {
      'display' : 'block',
			'position' : 'absolute',
      'z-index' : '10000'
    }
		$('ul', this).css(cssObj);
		$(this).addClass('hover');			
	}
);
$('#nav li').not($("#nav li li")).mouseout(
	function() { 
		var cssObj = {
      'display' : 'none',
      'z-index' : '10000'
    }
		$('ul', this).css(cssObj);
		$(this).removeClass('hover');	
	}
);
}





// ///// HOME NEWS 
if($("#newsStory-2")){
$("#newsStory-2").hide();
$("#newsStory-3").hide();
$("#newsStory-4").hide();
$("#newsStory-5").hide();

$(".newsStoryCont-navi-button").click(function(){
	$(".newsStoryCont-navi-button").removeClass("selected");
	$(this).addClass("selected");
	var objid = $(this).attr('id').substring(19);
	$(".newsStory").hide(0);
	$("#newsStory-"+objid).fadeIn(300);
});
}







// ///// INPUT TEXT TREATMENT
$("http://www.cbscorporation.com/_js/input.text").focus(function() {
	$(this).addClass("onfocus");
	if(this.value==this.defaultValue || this.value=='') {
		this.value='';
	}
});
//Place values back on blur
$("http://www.cbscorporation.com/_js/input.text").blur(function() {
	if(this.value==this.defaultValue || this.value=='') {
		this.value=this.defaultValue;
		$(this).removeClass("onfocus");
	}
});


$("textarea").focus(function() {
	$(this).addClass("onfocus");
	if(this.value==this.defaultValue || this.value=='') {
		this.value='';
	}
});
//Place values back on blur
$("textarea").blur(function() {
	if(this.value==this.defaultValue || this.value=='') {
		this.value=this.defaultValue;
		$(this).removeClass("onfocus");
	}
});



// ///// LAUNCH VIDEO 
if($.fancybox){
	$(".launch-video").fancybox({
		'titlePosition'		: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'autoScale'				: 'false',
		'width'						: '500px',
		'height'					: '360px',
		'overlayOpacity'	: '0.6',
		'overlayColor'		: '#000',
		'showCloseButton'	: 'true'
	});
	$(".launch-video").click(function(){
		var so = new SWFObject('../player.swf'/*tpa=http://www.cbscorporation.com/player.swf*/,'mpl','480','380','9');
		so.addParam('allowfullscreen','true');
		so.addParam('allowscriptaccess','always');
		so.addParam('wmode','opaque');
		so.addVariable('autostart','true');
		so.addVariable('file','http://www.cbscorporation.com/_videos/More_than_meets_the_eye_MPEG4_480x360.m4v');
		so.addVariable('image','Unknown_83_filename'/*tpa=http://www.cbscorporation.com/_videos/MoreThanMeetsTheEye_cover_image.jpg*/);
		so.write('mediaspace');
	});
};



// ///// LEAVING THE PAGE WARMING in SUPPLIER PAGE

$("a#inline").click(function(){
	window.location.hash = '#overlay-message';
})


if($.fancybox){
	$("a#inline").fancybox();
};
$("#overlay-notice-1 a.close").click(function(){
	$.fancybox.close();
	window.location.hash = false;
});
$("#overlay-notice-2 a.close").click(function(){
	$.fancybox.close();
});




//alert('$!');

}); // END OF $;