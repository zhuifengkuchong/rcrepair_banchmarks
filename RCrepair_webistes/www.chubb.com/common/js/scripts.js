// image preload scripts
var imagePath = "/common/images/";
//preload images
if (document.images) {
	var imageNames = new Array("tn_business_insurance","tn_personal_insurance","tn_at_chubb","tn_business_insurance_ko","tn_personal_insurance_ko", "tn_enter_claim_system","tn_report_a_loss", "tn_request_a_quote","tn_report_a_claim", "tn_lineas_comerciales", "tn_lineas_personales", "tn_bedrijven", "tn_particulieren");
	var imageExtension = ".gif";
	var rolloverImages = new Array(imageNames.length);
	var curName = "";
	var gWhichGlowing = "";

	//preload rollover images
	for (var i=0; i<imageNames.length; i++) {
		curName = imageNames[i];
		rolloverImages[curName] = new Array("over","out");
		rolloverImages[curName]["out"] = new Image();
		rolloverImages[curName]["out"].src = imagePath+curName+imageExtension;
		rolloverImages[curName]["over"] = new Image();
		rolloverImages[curName]["over"].src = imagePath+curName+"-over"+imageExtension;
	}
}

//rollover scripts

function Glow(whichImage) {
	if (document.images) {
		document[whichImage].src = rolloverImages[whichImage]["over"].src;
		gWhichGlowing = whichImage;
	}
}

function DeGlow(){
	if (document.images) {
		document[gWhichGlowing].src = rolloverImages[gWhichGlowing]["out"].src;
		gWhichGlowing = "";
	}
}

function MakeArray(n){
	this.length = n
	return this
}
monthNames = new MakeArray(12)
monthNames[1]="January"
monthNames[2]="February"
monthNames[3]="March"
monthNames[4]="April"
monthNames[5]="May"
monthNames[6]="June"
monthNames[7]="July"
monthNames[8]="August"
monthNames[9]="September"
monthNames[10]="October"
monthNames[11]="November"
monthNames[12]="December"

function customDateString(oneDate){
	var theMonth = monthNames[oneDate.getMonth() + 1]
	var theYear = oneDate.getFullYear()
	return theMonth + " " + oneDate.getDate() + ", " + theYear
}

function TodaysDate(){
	var Today = new Date();
	return customDateString(Today);
}

function emailURL(){
	window.location = "mailto:"+"?subject=Information from the Chubb Web Site" + "&body=This information might interest you.  You can view this page from the Chubb Web site at: %0D%0A"+window.location.href+"%0D%0A%0D%0A%0D%0A";
}

function PrintPage(){ 
	window.print();
}

function printPage(){ 
	window.print();
}

//chubb.js script

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

//mm_css_menu.js script
var mmOpenContainer = null;
var mmOpenMenus = null;
var mmHideMenuTimer = null;

function MM_menuStartTimeout(hideTimeout) {
	mmHideMenuTimer = setTimeout("MM_menuHideMenus()", hideTimeout);	
}

function MM_menuHideMenus() {
	MM_menuResetTimeout();
	if(mmOpenContainer) {
		var c = document.getElementById(mmOpenContainer);
		c.style.visibility = "inherit";
		mmOpenContainer = null;
	}
	if( mmOpenMenus ) {
		for(var i in mmOpenMenus) {
			var m = document.getElementById(mmOpenMenus[i]);
			m.style.visibility = "hidden";			
		}
		mmOpenMenus = null;
	}
}

function MM_menuHideSubmenus(menuName) {
	if( mmOpenMenus ) {
		var h = false;
		var c = 0;
		for(var i in mmOpenMenus) {
			if( h ) {
				var m = document.getElementById(mmOpenMenus[i]);
				m.style.visibility = "hidden";
			} else if( mmOpenMenus[i] == menuName ) {
				h = true;
			} else {
				c++;
			}
		}
		mmOpenMenus.length = c+1;
	}
}

function MM_menuOverMenuItem(menuName, subMenuSuffix) {
	MM_menuResetTimeout();
	MM_menuHideSubmenus(menuName);
	if( subMenuSuffix ) {
		var subMenuName = "" + menuName + "_" + subMenuSuffix;
		MM_menuShowSubMenu(subMenuName);
	}
}

function MM_menuShowSubMenu(subMenuName) {
	MM_menuResetTimeout();
	var e = document.getElementById(subMenuName);
	e.style.visibility = "inherit";
	if( !mmOpenMenus ) {
		mmOpenMenus = new Array;
	}
	mmOpenMenus[mmOpenMenus.length] = "" + subMenuName;
}

function MM_menuResetTimeout() {
	if (mmHideMenuTimer) clearTimeout(mmHideMenuTimer);
	mmHideMenuTimer = null;
}

function MM_menuShowMenu(containName, menuName, xOffset, yOffset, triggerName) {
	MM_menuHideMenus();
	MM_menuResetTimeout();
	MM_menuShowMenuContainer(containName, xOffset, yOffset, triggerName);
	MM_menuShowSubMenu(menuName);
}

function MM_menuShowMenuContainer(containName, x, y, triggerName) {	
	var c = document.getElementById(containName);
	var s = c.style;
	s.visibility = "inherit";
	
	mmOpenContainer = "" + containName;
}

