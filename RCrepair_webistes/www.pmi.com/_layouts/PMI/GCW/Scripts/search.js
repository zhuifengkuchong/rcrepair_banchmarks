// JavaScript Document
var intCurrentPageSize = queryStr("res");
var strCurrentSelectedScope = null;
var intCurrentSelectedDocType = queryStr("dt");
var intCurrentSelectedLanguage = queryStr("l");
var intCurrentSelectedSubsite = queryStr("w");
var strCurrentSelectedWebText = "";
var strCurrentSelectedLanguageText = "";
var LanguagesControl = null;
var SubSitesControl = null;
var SearchBoxFilter = null;

function SubmitToSearchPage(strURL, txtId, strDefaultTextValue) {
    var strSearchText = document.getElementById(txtId).value;

    if (!(strSearchText == "") && !(strSearchText==strDefaultTextValue)) {
        arrQueryStringPairs = new Array();
        if (SubSitesControl) {
            strCurrentSelectedWebText = SubSitesControl.options[SubSitesControl.selectedIndex].value;
        }

        if (LanguagesControl && strCurrentSelectedLanguageText == "") { // the language is set from the search page
            strCurrentSelectedLanguageText = LanguagesControl.options[LanguagesControl.selectedIndex].value;
        }

        if (!(strCurrentSelectedWebText == "") && (SubSitesControl)) {
            strCurrentSelectedScope = strCurrentSelectedLanguageText + "_" + strCurrentSelectedWebText;
        }
        else {
            strCurrentSelectedScope = strCurrentSelectedLanguageText;
        }


        if (!(strSearchText == ""))
        { strKPair = new Array("k", encodeURIComponent(strSearchText)); arrQueryStringPairs.push(strKPair); }

        strPagePair = new Array("page", "1"); arrQueryStringPairs.push(strPagePair);
        strResPair = new Array("res", getCurrentPageSize()); arrQueryStringPairs.push(strResPair);

        if (strCurrentSelectedScope)
        { strScopePair = new Array("s", encodeURIComponent(strCurrentSelectedScope)); arrQueryStringPairs.push(strScopePair); }

        if (!(intCurrentSelectedDocType == null))
        { strDocTypePair = new Array("dt", intCurrentSelectedDocType); arrQueryStringPairs.push(strDocTypePair); }

        if (!(intCurrentSelectedLanguage == null))
        { strLanguagePair = new Array("l", intCurrentSelectedLanguage); arrQueryStringPairs.push(strLanguagePair); }

        if (!(intCurrentSelectedSubsite == null))
        { strWebPair = new Array("w", intCurrentSelectedSubsite); arrQueryStringPairs.push(strWebPair); }

        strURL = GetUrlWithQueryString(strURL, arrQueryStringPairs);
        window.location.href = strURL;
    }
}

function PageSizeChange(strEncodedURL, elem, targetElemId) {
    strCurrentPageSize = elem.selectedIndex;
    document.getElementById(targetElemId).selectedIndex = elem.selectedIndex;
}

function ResultsPageChange(strEncodedURL, strKeyword, intPageIndex, intPageSize, intScope, intDocType, intLanguage, intSubSite) {
    arrQueryStringPairs = new Array();
    strKPair = new Array("k", escape(strKeyword)); arrQueryStringPairs.push(strKPair);
    strPagePair = new Array("page", intPageIndex); arrQueryStringPairs.push(strPagePair);
    strResPair = new Array("res", intPageSize); arrQueryStringPairs.push(strResPair);
    strScopePair = new Array("s", intScope); arrQueryStringPairs.push(strScopePair);
    strDocTypePair = new Array("dt", intDocType); arrQueryStringPairs.push(strDocTypePair);
    strLanguagePair = new Array("l", intLanguage); arrQueryStringPairs.push(strLanguagePair);
    strSubSitePair = new Array("w", intSubSite); arrQueryStringPairs.push(strSubSitePair);

    window.location.href = GetUrlWithQueryString(strEncodedURL, arrQueryStringPairs);
}

function setCurrentParameters() {
    if (!(queryStr("res") == null)) {
        var i;
        var sel1 = document.getElementById("results-on-page1");
        var sel2 = document.getElementById("results-on-page2");
        for (i = 0; i < sel1.options.length; i++) {
            if (sel1.options[i].text == queryStr("res")) {
                sel1.selectedIndex = i;
                sel2.selectedIndex = i;
            }
        }
    }

    if (!(queryStr("k") == null)) {
        var txt1 = document.getElementById("search-site");
        var txt2 = document.getElementById("filter-keyword");

        if (!(txt1 == null))
        { txt1.value = decodeURIComponent(queryStr("k")); }

        if (!(txt2 == null))
        { txt2.value = decodeURIComponent(queryStr("k")); }
    }
}

function queryStr(key) {
    hu = window.location.search.substring(1);
    gy = hu.split("&");
    for (i = 0; i < gy.length; i++) {
        ft = gy[i].split("=");
        if (ft[0] == key) {
            return ft[1];
        }
    }

    return null;
}

function getCurrentPageSize() {
    var sel = document.getElementById("results-on-page1");
    if (!(sel == null)) {
        var strSize = sel.options[sel.selectedIndex].text;
        return strSize;
    }
    else {
        return 10;
    }
}

function GetUrlWithQueryString(url, arr) {
    var strUrlWithQueryString = "";
    var IsFirst = true;
    var i;

    strUrlWithQueryString += url;

    //Every array element represents a key,value row
    for (i = 0; i < arr.length; i++) {
        strPair = Array();
        strPair = arr[i];
        var strQueryKey = strPair[0] + "=" + strPair[1];
        if (IsFirst) {
            strUrlWithQueryString += "?" + strQueryKey;
            IsFirst = false;
        }
        else {
            strUrlWithQueryString += "&" + strQueryKey;
        }
    }

    return strUrlWithQueryString;

}

function ScopeChange(elem) {
    intCurrentSelectedSubsite = elem.selectedIndex;
    strCurrentSelectedWebText = elem.options[elem.selectedIndex].value;
}

function DocTypeChange(elem) {
    intCurrentSelectedDocType = elem.selectedIndex;
}

function LanguageChange(elem) {
    intCurrentSelectedLanguage = elem.selectedIndex;
    strCurrentSelectedLanguageText = elem.options[elem.selectedIndex].value;
}

function SetTextBoxValue(elem, strDefaultValue, strEvent) {
    var textbox = elem;

    if (textbox.value == strDefaultValue && strEvent == 'onclick') {
        textbox.value = "";
    }
    else if (textbox.value == '' && strEvent == 'onblur') {
        textbox.value = strDefaultValue;
    }
}

function disableEnterKey(e) {
    var key;
    if (window.event)
        key = window.event.keyCode; //IE
    else
        key = e.which; //firefox      

    return (key != 13);
}

function IsEnter(e) {

    if (e) {
        if (e.which) {
            return (e.which == 13);
        }
        else {
            return (window.event.keyCode == 13);
        }

    }

}

function SetSearchBoxPreFilter(scopeLanguage, intLanguage) {
    intCurrentSelectedLanguage = intLanguage;
    strCurrentSelectedLanguageText = scopeLanguage;
    SearchBoxFilter = true;
}
