/* Home Page Javascript */

// Document Ready
jQuery(document).ready(function() {
	FeatNav.roll.init();
	SectionNav.rollover.init(); 
	
	SwapImg.rollover.init();
	
	initNewsTicker();
});


// Feature Navigation drop down menus
FeatNav = {};
FeatNav.roll =
{
	init:		function()
	{
		jQuery('.feat-nav').hoverIntent(
			function() { FeatNav.roll.showDropDown(jQuery(this));}, 
			function() { FeatNav.roll.hideDropDown(jQuery(this));}
		);		
	},
	
	showDropDown:	function(target)
	{
		var dropDown = jQuery(target).children('.feat-nav-on');
		jQuery(dropDown).parent().children('.btn-on').show();
		jQuery(dropDown).show();
		jQuery(target).css('zIndex', '3');
	},
	
	hideDropDown:	function(target)
	{
		var dropDown = jQuery(target).children('.feat-nav-on');
		jQuery(dropDown).parent().children('.btn-on').hide();
		jQuery(dropDown).hide();
		jQuery(target).css('zIndex', '1');
	}
};


var shadowY = new Array(1, 37, 74, 111, 148, 184);

// Section Navigation
SectionNav = {};
SectionNav.rollover = {
	init: function() {
		jQuery('.section-nav').mouseover(this.over);
	},

	over: function() {
		SectionNav.rollover.clearAll();

		// Show section
		var target = "#section-" + jQuery(this).attr('id');
		jQuery(target).show();

		// Change image to rollover
		jQuery(this).attr('src', SectionNav.rollover.newimage(jQuery(this).attr('src')));

		// Update shadow
		var curBtn = 1;
		for (var i = 1; i < jQuery("#sectionNav").children().size(); i++) {
			if (jQuery("#sn" + i).attr("src") == jQuery(this).attr('src')) {
				curBtn = i;
				break;
			}
		}

		if (curBtn == jQuery("#sectionNav").children().size() - 1) {
			jQuery("#sectionNavShadow").children(".shadow2").show();
		} else {
			jQuery("#sectionNavShadow").children(".shadow").css("top", shadowY[curBtn - 1]);
			jQuery("#sectionNavShadow").children(".shadow").show();
		}			
	},

	clearAll: function() {
		// Set sections to default state
		jQuery(".section").hide();
		
		// Hide shadows
		jQuery("#sectionNavShadow").children(".shadow").hide();
		jQuery("#sectionNavShadow").children(".shadow").css("top", 1);
		jQuery("#sectionNavShadow").children(".shadow2").hide();
		
		// Set buttons to default state
		for (var i = 1; i < jQuery("#sectionNav").children().size(); i++) {
			SectionNav.rollover.defaultImage(jQuery("#sn" + i));
		}
	},

	newimage: function(src) {
		if (src.substring(src.length - 4, src.length - 7) == '_ro') {
			return src;
		} else {
			return src.substring(0, src.search(/(\.[f,g,i,n,p]+)/)) + '_ro' + src.match(/(\.[f,g,i,n,p]+)/)[0];
		}
	}, 
	
	defaultImage: function(img) {
		var src = jQuery(img).attr("src");
		
		if (src.substring(src.length - 4, src.length - 7) == '_ro') {
			jQuery(img).attr('src', jQuery(img).attr('src').replace(/_ro/, ''));
		}
	}
}


// Initialize NewsTicker Flash
function initNewsTicker() {
	var flashvars = {};
	
	var params			=	{};
	params.menu			=	"false";
	params.salign		=	"t";
	params.scale		=	"noscale";
	params.wmode		=	"opaque"

	var attributes		=	{};

	swfobject.embedSWF("Unknown_83_filename"/*tpa=http://www.cardinal.com/mps/CacheProxyServlet/colorPalette/default/browserVendor/Netscape/browserName/Navigator/browserVersion/unknown/locale/en/forwardurl/mps/themes/html/Cardinal/js/swf/NewsTicker.swf*/, "newsFlashDiv", "938", "36", "9.0.0", "", flashvars, params, attributes);
}