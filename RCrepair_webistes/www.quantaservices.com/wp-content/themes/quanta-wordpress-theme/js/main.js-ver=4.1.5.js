(function() {

  var transitionend = {
    'WebkitTransition' : 'webkitTransitionEnd',
    'MozTransition'    : 'transitionend',
    'OTransition'      : 'oTransitionEnd',
    'msTransition'     : 'MSTransitionEnd',
    'transition'       : 'transitionend'
  }[ Modernizr.prefixed('transition') ] || 'transitionend';
  var animationend = {
    'WebkitAnimation' : 'webkitAnimationEnd',
    'MozAnimation'    : 'animationend',
    'OAnimation'      : 'oAnimationEnd',
    'msAnimation'     : 'MSAnimationEnd',
    'animation'       : 'animationend'
  }[ Modernizr.prefixed('animation') ] || 'animationend';//(Modernizr.prefixed('animation') + "End").replace(/^ms/, "MS").replace(/^Webkit/, "webkit").replace(/^Moz.*/, "animationend");

  Modernizr.addTest("pointerevents",function(){
    var a=document.createElement("x");
    a.style.cssText="pointer-events:auto";
    return a.style.pointerEvents==="auto";
  });
  /*
  if ( !Modernizr.pointerevents ) {
    console.log("No pointer events.");
    $.fn.passThrough = function (target) {
      var $target = $(target);
      return this.each(function () {
        var style = this.style;
        if ('pointerEvents' in style) {
          style.pointerEvents = style.userSelect = style.touchCallout = 'none';
        } else {
          $(this).on('click tap mousedown mouseup mouseenter mouseleave', function (e) {
            $target.each(function() {
              var rect = this.getBoundingClientRect();
              if (e.pageX > rect.left && e.pageX < rect.right &&
                e.pageY > rect.top && e.pageY < rect.bottom)
                $(this).trigger(e.type);
            });
          });
        }
      });
    };
    $('.banner-overlay').passThrough('.banner-slide');
  }*/

  var scrollToElement = function() {
    var id = this.hash;
    var currentTop = win.scrollTop();
    if ( this === location || ( this.hostname === location.hostname && this.pathname === location.pathname ) ) {
      var endTop = 0;
      if ( id ) {
        var el = $(id);
        if ( ! el.length ) return;
        endTop = el.offset().top - header.height();
      } else {
        id = '';
      }
      scrollable.scrollTop(currentTop).animate({ scrollTop: endTop }, Math.abs( currentTop - endTop ) / 3, function() {
        if ( location.hash || id ) {
          location.hash = id;
        }
      });
      return false;
    }
  };

  var win = $(window).on('resize', function() {
    $('#banner').height(win.height());
    var currentEntity = body.find('section[typeof="Organization"].is-current');
    if ( currentEntity.length ) {
      var padding = currentEntity.find('.entity-description').outerHeight();
      currentEntity.css('padding-bottom', padding + 8);
    }
  }).on('load.scroll', function() {
    scrollToElement.call(location);
  }).on('scroll.initial', function() {
    win.off('scroll.initial').off('load.scroll');
  });
  var scrollable = $('html, body');
  var html = $(document.documentElement);
  var body = $(document.body).on('click', 'a', scrollToElement);
  var main = html.find('main');
  var header = body.find('header:first');
  win.trigger('resize');

  $('#s').on('focus', function() {
    $(this).closest('.search').addClass('is-focused');
  });

  body.find('.timeline').on('click', 'li', function() {
    $(this).addClass('is-current').siblings('.is-current').removeClass('is-current');
  });

  var canHover = true;
  var videobox = body.find('.videobox').on('click', 'a', function(ev) {
	  //console.log('videobox');
    var el = $(this);
    if ( el.hasClass('is-current') ) {
      // start the video
      //console.log('current')
      //return false;
    }
    canHover = false;
    el.removeClass('is-shortened').addClass('is-current');
    el.siblings().removeClass('is-current is-shortened is-hovered');
    el[el.is(':nth-child(odd)') ? 'next' : 'prev']().addClass('is-shortened');
    //return false;
  }).on('mouseenter mouseleave', 'a', function(ev) {
    if ( ! canHover ) return;
    var el = $(this);
    var hovered = ev.type === 'mouseenter';
    el.toggleClass('is-hovered', hovered);
    el[el.is(':nth-child(odd)') ? 'next' : 'prev']().toggleClass('is-shortened', hovered || el.hasClass('is-current'));
  });

  body.on('click', function(ev) {
	  
    if ( ! $(ev.target).closest('.videobox, .tile.is-current, section[typeof="Organization"]').length ) {
      videobox.find('a').removeClass('is-current is-shortened is-hovered');
      body.find('.tile, section[typeof="Organization"]').removeClass('is-current is-shifted').filter('[typeof]').animate({
        paddingBottom: 0
      }, 300).find('.entity-description').slideUp(300);
      body.find('.col').removeClass('has-open');
      canHover = true;
    }
  });
  
  ////////////////////////////////////////
  // Vimeo Modal
  body = $(document.body);
  var isVideo = function (video) {
    var videoID,
        videoURL;
    
    if ( video.match(/vimeo.com/) ) {
      videoID = video.match(/vimeo\.com\/([0-9]+)/i)[1];
      videoURL = '//player.vimeo.com/video/'+videoID+'?title=0&amp;byline=0&amp;portrait=0&amp;color=969696&amp;autoplay=1';
    } else if ( video.match(/youtu(be\.com|\.be)/gi) ) {
      videoID = video.match(/youtu(be\.com\/(watch\?v=|embed\/|v\/)|\.be\/)([^\&\?\/]+)/i)[3];
      videoURL = '//youtube.com/embed/'+videoID+'?autohide=1&autoplay=1&rel=0&showinfo=0&modestbranding=1&controls=0';
    } else {
      return false;
    }
//    console.log("is Video!");
    return videoURL;
  };
  
  var showModal = function (modal) {
    //console.log("showing modal");
    var $modal = $(modal);
    
    if ( $modal.length ) {
      var $overlay = $('<div>', {
        'id': 'quanta-overlay',
        'class': 'close-Modal'
      });
      body.append($overlay);
      setTimeout(function() {
        $overlay.append($modal).addClass('is-ready');
      }, 0);
      
      html.on('keyup.control-overlay', function(ev) {
        if ( ev.which === 27 ) {
          $overlay.trigger('click');
          return false;
        }
      });
      
      return true;
    } else {
      return false;
    }
  };
  
  var hideModal = function (modal) {
    var $modal = $(( modal ? modal : "#quanta-overlay" ));
    $modal.removeClass('is-ready').one(transitionend, function() {
      $modal.detach();
      html.off('keyup.control-overlay');
    });
    
    if ( !Modernizr.csstransitions ) {
      $modal.trigger(transitionend);
    }
  };
  
  body
    .on( "click" , ".open-Modal" , function(e){
	  
      e.stopPropagation();
      var videoURL = isVideo(this.href),
        $target = $(this.hash),
        $modal;
      
      if ( videoURL ) {
        $modal = $('<div class="Modal Video Table-Center"><div class="Modal-Container Table-Cell"><button class="icon-close"></button> <span class="visuallyhidden">Close</span></button><div class="Responsive-Embed Center"><iframe id="player1" src="' + videoURL + '" width="720" height="405" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div></div></div>');
      } else if ( $target.length ) {
        $modal = $target;
      }
      
      if ( showModal($modal) ) {
        e.preventDefault();
      } else {
        window.location = this.href;
        return false;
      }
    })
    .on( "click" , ".close-Modal" , function(){ hideModal(); });
    
/*  
    ////////////////////////////////////////
    // Toggle video based on data attribute
    $body.on("click","[data-videotoggle]",function(e){
      var $this = $(this).toggleClass("is-Clicked"),
        video = $this.attr("href"),
        $target = $($this.data("videotoggle")).first(),
        $parent = $target.parent();
        
      scrollToElement.call($parent);
      
      if ( ! $target.hasClass(openClass) ) {
        var videoID,
          videoURL,
          $embed = "";
        
        if ( video.match(/vimeo.com/) ) {
          videoID = video.match(/vimeo\.com\/([0-9]+)/i)[1];
          videoURL = '//player.vimeo.com/video/'+videoID+'?title=0&amp;byline=0&amp;portrait=0&amp;color=969696&amp;autoplay=1';
        } else if ( video.match(/youtu(be\.com|\.be)/gi) ) {
          videoID = video.match(/youtu(be\.com\/(watch\?v=|embed\/|v\/)|\.be\/)([^\&\?\/]+)/i)[3];
          videoURL = '//youtube.com/embed/'+videoID+'?autohide=1&autoplay=1&rel=0&showinfo=0&modestbranding=1&controls=0';
        } else {
          return false;
        }
        
        $embed = $('<div class="Responsive-Embed Video"><div class="Loader Absolute-Center"></div><iframe width="640" height="360" src="'+videoURL+'" frameborder="0" allowfullscreen></iframe></div>')
          .appendTo($preloader);
        
        $parent.css({ "min-height" : "100px", "max-height" : "2000px" });
        $target.append($embed).fadeIn(maxAnimation);
        $target.toggleClass(openClass);
        
      } else {
        $parent.addClass("no-Transition").css({ "min-height" : $parent.outerHeight() });
        setTimeout(function(){
          $parent.removeClass("no-Transition").css({ "min-height" : "", "max-height" : "" });
          $target.fadeOut(maxAnimation,function(){
            $target.empty();
            $target.toggleClass(openClass);
          });
        },1);
      }
      
      
  //		height = ( $embed.length ? "inherit" : "" );
  //		console.log("height",height);
  //		$parent.css({ "min-height" : height });
  
  //		$parent.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',
  //			function(){
  //		});
      
      
    
      e.preventDefault();
      
    });
*/


  body.find('.scrolldown').on('click', function() {
    scrollable.animate({ scrollTop: $(this).closest('section').next().position().top - header.height() }, 300);
    return false;
  });

  var createPagingNav = function(pages, options) {
    var classes = ['paging-nav'];
    if ( options.classes ) {
      classes = classes.concat(options.classes);
    }
    return $('<nav>', {
      'class': classes.join(' '),
      append: function() {
        return $.map(pages, function(v, i) {
          return '<button' + ( ! i ? ' class=is-active' : '' ) + ' />';
        }).join('');
      }
    }).on('click', 'button', function(ev) {
      if ( options.onButtonClick ) {
        options.onButtonClick.call(this, ev);
      }
    });
  };

  body.find('.about-feed').each(function() {
    var feeds = $(this);
    var articles = feeds.find('article');
    if ( articles.length > 1 ) {
      var current = articles.filter('.is-current');
      var index = articles.index(current);
      var articleNext = function() {
        feeds.trigger('page:change', [( index + 1 < articles.length ? index + 1 : 0 )]);
      };
      var feedNavBtns = createPagingNav(articles, {
        onButtonClick: function() {
          feeds.trigger('page:change', [feedNavBtns.index(this)]);
        }
      }).appendTo(feeds).find('button');
      feeds.on('page:change', function(ev, newIndex) {
        if ( newIndex === index ) return;
        index = newIndex;
        feedNavBtns.filter(':eq(' + newIndex + ')').addClass('is-active').siblings().removeClass('is-active');
        //if ( Modernizr.csstransitions ) {
          current.removeClass('is-current');
          current = articles.filter(':eq(' + newIndex + ')').addClass('is-current');
        //}
      });
      feeds.on('click', 'header button', function() {
        var next = current[this.getAttribute('rel')]('article');
        if ( next.length ) {
          feeds.trigger('page:change', [index + ( this.getAttribute('rel') == 'next' ? 1 : -1 )]);
        }
      });
    }
  });

  body.find('#banner, .banner').each(function() {
    var banner = $(this);
    var slides = banner.find('.banner-slide');
    var delay = 7000;
    var animationLength = 600;
    var started = false;
    var t;
    var startCarousel = $.noop;
    if ( slides.length > 1 ) {
      var current = slides.filter('.is-current');
      var past;
      var index = slides.index(current);
      var slideNext = function() {
        banner.trigger('slide:change', [( index + 1 < slides.length ? index + 1 : 0 )]);
      };
      var bannerNavBtns = createPagingNav(slides, {
        classes: ['banner-nav'],
        onButtonClick: function() {
          clearTimeout(t);
          banner.trigger('slide:change', [bannerNavBtns.index(this)]);
        }
      }).appendTo(banner).find('button');
      banner.on('slide:change', function(ev, newIndex) {
        if ( newIndex === index ) return;
        index = newIndex;
        bannerNavBtns.filter(':eq(' + newIndex + ')').addClass('is-active').siblings().removeClass('is-active');
        if ( Modernizr.cssanimations ) {
          past = current.addClass('out').one(animationend, function() {
            $(this).removeClass('is-current out');
          });
          current = slides.filter(':eq(' + newIndex + ')').addClass('in').one(animationend, function() {
            current.removeClass('in').addClass('is-current');
            t = setTimeout(slideNext, delay);
          });
        } else {
          current.animate({
            left: '-100%'
          }, animationLength, function() {
            $(this).removeClass('is-current').css('left', '');
          });
          current = slides.filter(':eq(' + newIndex + ')').animate({
            left: 0
          }, animationLength, function() {
            current.addClass('is-current').css('left', '');
            t = setTimeout(slideNext, delay);
          });
        }
      });
      startCarousel = function() {
        t = setTimeout(slideNext, delay);
        started = true;
      };
    }
    win.on('scroll.show-banner', function() {
      banner.toggleClass('invisible', !Modernizr.touch && win.scrollTop() > banner.outerHeight() - header.height());
      if ( started && banner.hasClass('invisible') ) {
        clearTimeout(t);
        started = false;
      } else if ( slides.length && ! started ) {
        clearTimeout(t);
        startCarousel();
      }
    }).trigger('scroll.show-banner');
  });


  /* Homepage Stock Ticker */
  var $stocks = $("#Stocks");

  // Get stock data via YQL query
  var getStocks = function (stockSymbol) {
    var wsql = "select * from yahoo.finance.quotes where symbol in ('"+stockSymbol+"')",
        stockYQL = 'http://query.yahooapis.com/v1/public/yql?q='+encodeURIComponent(wsql)+'&env=http%3A%2F%2Fdatatables.org%2Falltables.env&format=json&callback=?';
    return $.ajax({
      url: stockYQL,
      dataType: 'json'
    });
  };

  // Format Numbers
  var getRepString = function (rep) {
    rep = rep+''; // coerce to string
    if (rep >= 1000000000) {
      return (rep / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
    } else if (rep >= 1000000) {
      return (rep / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
    } else if (rep >= 1000) {
      return (rep / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
    } else {
      return rep;
    }
  };

  // Replace children with [data-replace] attribute given a data object
  $.fn.dataReplace = function(data) {
    var $replacers = this.find("[data-replace]");
    if ( $replacers.length ) {
      $replacers.each(function(){
        var $this = $(this);
        var replace = $this.data("replace");
        var content = data[replace];
        if ( replace === 'Name' ) {
          content = content.replace(/\W/gi, ' ');
        } else if ( replace === 'Volume' ) {
          content = getRepString(content);
        }
        $this.html(content);
      });
      var updown = data['Change'];
      if (updown < 0)
      {	
	      //$('.change-direction').html(' <img src = "../../../uploads/2014/10/Decrease.svg_.png"/*tpa=http://www.quantaservices.com/wp-content/uploads/2014/10/Decrease.svg_.png*/ title = "The Stock Price Is Decreasing" style = "height:8px;">');	
          $('.change-direction').html(' <img src = "../../../uploads/2014/10/Decrease.svg_.png"/*tpa=http://www.quantaservices.com/wp-content/uploads/2014/10/Decrease.svg_.png*/ title = "The Stock Price Is Decreasing" style = "height:8px;">');
      }
      else
      {
	  	  //$('.change-direction').html(' <img src = "../../../uploads/2014/10/Increase.svg_.png"/*tpa=http://www.quantaservices.com/wp-content/uploads/2014/10/Increase.svg_.png*/ title = "The Stock Price Is Increasing" style = "height:8px;">');
          $('.change-direction').html(' <img src = "../../../uploads/2014/10/Increase.svg_.png"/*tpa=http://www.quantaservices.com/wp-content/uploads/2014/10/Increase.svg_.png*/ title = "The Stock Price Is Increasing" style = "height:8px;">');
      }
      return true;
    } else { return false; }
  };

  // Update on Click
  $stocks.on("update",function(){
    var _this = this;
    var $this = $(this);
    var _uniqueID = "stockLoad.unique"+Math.floor(Math.random() * (100 - 1) + 1);
    var stockSymbol = $this.data("stocksymbol");

    $this
      .removeClass("is-Loaded")
      .addClass("is-Loading");

    if ( $this.hasClass("is-Visible") ) {
      $this.css("animation-play-state", "running");
    }

    $this
      .one(_uniqueID,function(data){
//        console.log(_uniqueID," triggered");
//        var stockInterval = window.setInterval(function(){
//          console.log("updated. now showing");
          $this.dataReplace($this.data("quote"));
          
          $this
            .removeClass("is-Loading")
            .addClass("is-Loaded");
            
          if ( ! $this.is(":visible") ) {
//            console.log("Not visible");
            $this.addClass("is-Visible").slideDown(1000);
          }
            
//          clearInterval(stockInterval);
//        }, 600);
      })
      .on(animationend,function(event){ 
        $this.css("animation-play-state","paused");
      });

    getStocks(stockSymbol).done(function(data){
//      console.log("data",data);
      $this.data("quote",data.query.results.quote);
      if ( Modernizr.cssanimations && $this.css("animation-play-state") === "running" ) {
        $this.on(animationend,function(event){ $this.trigger(_uniqueID); });
        window.setTimeout(function(){ $this.trigger(_uniqueID); },0);
      } else {
        window.setTimeout(function(){ $this.trigger(_uniqueID); },0);
      }
    });
  });
  
  $stocks.hide().trigger("update");
  
  $(".stock-ticker .btn").click(function(){ $stocks.trigger("update"); });

  /* Homepage Interactive Animation *
  body.find('img.interactive-animation-frame').each(function() {
    if ( ! Modernizr.canvas ) return;
    var $this = $(this);
    var section = $this.closest('section');
    var canvas = document.createElement('canvas');
    var $canvas = $(canvas);
    var ctx = canvas.getContext('2d');
    var frames = [];
    var frameRate = 30;
    var onload = function() {
      var data = { x: 0, y: 0, frames: 1, src: this };
      ctx.drawImage(this, data.x, data.y);
      canvas.width = this.width;
      canvas.height = this.height;
      frames.unshift(data);
      $this.replaceWith(canvas);
    };
    if ( this.complete ) {
      onload.call(this);
    } else {
      this.onload = onload;
    }
    $.ajax('http://www.quantaservices.com/wp-content/themes/quanta/js/interactive-data.json').done(function(data) {
      var i, l, lastIndex, animating, startTime, raf, length = 0, duration, stopIndex = 0;
      frames = frames.concat(data);
      i = frames.length;
      while ( i-- ) {
        var numFrames = frames[i].frames;
        length += numFrames;
        if ( numFrames > 1 ) {
          while ( numFrames-- ) {
            frames.splice(i, 0, {});
          }
        }
      }
      duration = length / 30 * 1000;
      var startAnimation = function() {
        startTime = new Date();
        raf = requestAnimationFrame(function play() {
          var now = new Date();
          var diff = now - startTime;
          var index = stopIndex + ( Math.floor(diff / duration * (length - 1)) % length );
          var frame = frames[index];
          if ( lastIndex === index || ! frame || ! frame.src ) {
            raf = requestAnimationFrame(play);
            return;
          }
          lastIndex = index;
          var draw = function() {
            lastFrame = frame;
            ctx.drawImage(frame.src, frame.x, frame.y);
            raf = requestAnimationFrame(play);
          };
          if ( ! ( frame.src instanceof Image || frame.src instanceof HTMLImageElement ) ) {
            var img = new Image();
            img.onload = function() {
              frame.src = img;
              draw();
            };
            img.src = frame.src;
          } else {
            draw();
          }
        });
      };
      var stopAnimation = function() {
        cancelAnimationFrame(raf);
        stopIndex = lastIndex || 0;
      };
      win.on('scroll.animation', function() {
        var viewportTop = win.scrollTop();
        var viewportBottom = viewportTop + win.height();
        var sectionTop = $canvas.offset().top;
        var sectionBottom = sectionTop + $canvas.outerHeight();
        if ( sectionTop < viewportBottom && sectionBottom > viewportTop ) {
          if ( ! animating ) {
            startAnimation();
            animating = true;
          }
        } else {
          stopAnimation();
          animating = false;
        }
      }).trigger('scroll.animation');
    });
  });
  */

  body.find('.show-hide').each(function() {
    var content = $(this).addClass('hidden');
    $('<a>', {
      href: '#',
      text: 'Read more',
      click: function() {
        content.toggleClass('hidden');
        $(this).text('Read ' + ( content.hasClass('hidden') ? 'more' : 'less' ));
        if ( $(".lt-ie9").length ) {
          content.click();
        }
        return false;
      }
    }).insertAfter(content);
  });

  body.find('.gallery').each(function() {
    var images;
    var moving = false;
    var fig, prev = [], next = [];
    var figure = $('<figure>', {
      css: {
        width: '100%',
        position: 'absolute',
        top: 0
      },
      html: '<img>'
    });
    var overlay = $('<div>', {
      id: 'quanta-overlay'
    }).on('click', function(ev) {
      var target = $(ev.target);
      if ( ! moving && overlay.hasClass('is-ready') && ( target.hasClass('icon-close') || ! target.closest('.overlay-gallery').length ) ) {
        overlay.removeClass('is-ready').one(transitionend, function() {
          overlay.detach();
        });
        html.off('keyup.control-overlay');
      }
    }).on('click', '.grid button', function() {
      var src = $('img', this).data('src');
      $.capture(src).done(function(img) {
        overlay.find('figure').css('background-image', 'url(' + img.src + ')').find('img').attr('src', img.src);
        win.triggerHandler('resize.lightbox');
      });
    });
    overlay.on('movestart', function(ev) {
      if ( body.width() >= 768 || moving ) return false;
      moving = true;
      overlay.addClass('is-moving');
      fig = overlay.find('figure');
      fig.outerHeight(fig.outerHeight());
      var curImage = fig.find('img');
      var gallery = overlay.find('.overlay-gallery');
      var img = images.filter('[data-src$="' + curImage.attr('src').split('/').pop() + '"]');
      var index = images.index(img);
      var src;
      if ( index > 0 ) {
        prev = figure.clone(true).insertAfter(fig);
        src = img.parent().prev().find('img').data('src');
        $.capture(src).done(function(img) {
          var height = ( body.width() - 20 ) / img.width * img.height;
          if ( height === 0 ) {
            gallery.css('height', 'auto');
          } else if ( height > gallery.height() ) {
            gallery.css('height', height);
          }
          prev.css({
            'background-image': 'url(' + img.src + ')',
            'right': '100%',
            'height': height
          }).find('img').attr('src', img.src);
        });
      } else {
        prev = [];
      }
      if ( index < images.length - 1 ) {
        next = figure.clone(true).insertAfter(fig);
        src = img.parent().next().find('img').data('src');
        $.capture(src).done(function(img) {
          var height = ( body.width() - 20 ) / img.width * img.height;
          if ( height === 0 ) {
            gallery.css('height', 'auto');
          } else if ( height > gallery.height() ) {
            gallery.css('height', height);
          }
          next.css({
            'background-image': 'url(' + img.src + ')',
            'left': '100%',
            'height': height
          }).find('img').attr('src', img.src);
        });
      } else {
        next = [];
      }
    }).on('move', function(ev) {
      if ( body.width() >= 768 ) return false;
      overlay.find('figure').css('transform', 'translateX(' + ev.distX + 'px)');
    }).on('moveend', function(ev) {
      if ( body.width() >= 768 ) return false;
      overlay.removeClass('is-moving');
      if ( Math.abs(ev.distX) > body.width() / 3 ) {
        if ( ev.distX > 0 && prev.length ) {
          overlay.find('figure').css('transform', 'translateX(100%)');
          fig = prev;
        } else if ( ev.distX < 0 && next.length ) {
          overlay.find('figure').css('transform', 'translateX(-100%)');
          fig = next;
        } else {
          overlay.find('figure').css('transform', 'translateX(0)');
        }
      } else {
        overlay.find('figure').css('transform', 'translateX(0)');
      }
      fig.on(transitionend, function() {
        overlay.addClass('is-moving');
        var img = $('img', this);
        var fig = overlay.find('figure:first').css({
          'background-image': 'url(' + img[0].src + ')',
          'transform': ''
        }).outerHeight($(this).outerHeight());
        fig.find('img').attr('src', img[0].src);
        fig.nextAll('figure').remove();
        setTimeout(function() {
          overlay.removeClass('is-moving');
          moving = false;
        }, 0);
      });
    });
    var tmpl = Mustache.compile('<div class=overlay-gallery><button class=icon-close></button><figure style="background-image:url({{firstsrc}});"><img src="{{firstsrc}}" alt=""></figure><nav><h2>{{group}}</h2><div class="grid">{{#all}}<button><img src="{{#thumb}}{{.}}{{/thumb}}" data-src="{{.}}"></button> {{/all}}</div><p>For permission to use these photos, please contact the Communications Department.</p></div></nav></div>');
    win.on('resize.lightbox orientationchange.lightbox', function() {
      if ( overlay.parents('body').length ) {
        var gallery = overlay.find('.overlay-gallery').css('height', '');
        var fig = gallery.find('figure');
        fig.css('height', '').outerHeight(fig.outerHeight());
        var height = Math.floor(fig.find('img')[0].getBoundingClientRect().height);
        if ( height === 0 ) {
          height = 'auto';
        }
        gallery.css('height', height);
      }
    });
    $(this).on('click', 'a', function() {
      body.append(overlay);
      setTimeout(function() {
        overlay.addClass('is-ready');
      }, 0);
      var el = $(this);
      var category = el.data('category');
      if ( category != overlay.data("category") ) {
        overlay.empty().data("category",category);
        var group = el.text();
        var json_file = '/api/mla/category/?category=' + category;
        //var json_file = 'js/photos-' + el.data('category') + '.json';
        var request = el.data('request');
        if ( ! request ) {
          request = $.getJSON(json_file);
        }
        request.done(function(response) {
          var data = {
            firstsrc: response.results[0],
            all: response.results,
            group: group,
            thumb: function() {
              return function(text, render) {
                return render(text).replace(/(?:-\d+x\d+)?(\.\w{1,4})$/, '-124x124$1');
              };
            }
          };
          overlay.append(tmpl(data));
          $.capture(data.firstsrc).done(function(img) {
            images = overlay.find('.grid img');
            win.triggerHandler('resize.lightbox');
            $.capture(data.all.slice(1));
          });
        });
      } else {
        images = overlay.find('.grid img');
        win.triggerHandler('resize.lightbox');
      }
      html.on('keyup.control-overlay', function(ev) {
        if ( ev.which === 27 ) {
          overlay.trigger('click');
          return false;
        }
        var curImage = overlay.find('figure img');
        var img = images.filter('[data-src$="' + curImage.attr('src').split('/').pop() + '"]');
        var index = images.index(img);
        switch( ev.which ) {
          case 37:
            if ( index > 0 ) {
              img.parent().prev().trigger('click');
            }
            return false;
          case 39:
            if ( index < images.length - 1 ) {
              img.parent().next().trigger('click');
            }
            return false;
        }
      });
      return false;
    });
  });

  var generatedStyles = $('<style />').appendTo('head');
  var $cols = body.find('.col').each(function(i) {
    var $col = $(this);
    var $tiles = $col.find('.tile').on('click', function(ev) {
      if ( ev.target.href && ev.target.nodeName !== 'IMG' ) return true;
      if ( this.className.match(/\bis-current\b/) ) return false;
      $cols.find('.tile').removeClass('is-current is-shifted').find('a').attr('tabindex', -1);
      var $tile = $(this).addClass('is-current');
      $col.addClass('has-open').siblings().removeClass('has-open');
      if ( shouldShift && i === 0 && $tile.next('.tile').length ) {
        $tiles.last().addClass('is-shifted');
      }
      $tile.find('a').attr('tabindex', 0);
      return false;
    });
  });
  var shouldShift = $cols.find('.tile').length / 2 % 1 !== 0;
  var adjustTileStyles = function() {
    var prefixes = ['-webkit-', '-moz-', '-ms-', ''];
    var styles = '';
    var tileCount = 0;
    $cols.each(function(i) {
      var $col = $(this);
      var $tiles = $col.find('.tile').each(function(j) {
        var $tile = $(this);
        var id = $tile[0].id || 'js-tile-' + (++tileCount);
        var h2 = $tile.find('h2');
        var offset = h2.position();
        var transform = ( $col.width() - h2.width() ) / 2 - offset.left;
        $tile.attr('id', id);
        styles += '#' + id + ':hover h2,#' + id + ':focus h2{' + $.map(prefixes, function(prefix) {
          return prefix + 'transform:translate(' + transform + 'px,15px)';
        }).join(';') + ';}';
      });
      if ( i !== 0 ) {
        if ( $col.offset().left === $cols.eq(i-1).offset().left ) {
          styles += '.col.has-open + .col{' + $.map(prefixes, function(prefix) {
            return prefix + 'transform:translateY(' + 100 / ( $tiles.length + 1 ) + '%)';
          }).join(';') + ';}';
        }
      }
    });
    var newStyles = $('<style>' + styles + '</style>');
    generatedStyles.replaceWith(newStyles);
    generatedStyles = newStyles;
  };
  if ( $cols.length ) {
    win.on('resize.tileStyles', function() {
      generatedStyles.empty();
      adjustTileStyles();
    }).trigger('resize.tileStyles');
  }
  if ( ! Modernizr.csstransforms ) {
    body.find('http://www.quantaservices.com/wp-content/themes/quanta-wordpress-theme/js/a.tile').each(function() {
      $(this).on('mouseenter mouseleave', function(ev) {
        var figcaption = $('figcaption', this);
        var marginTop = figcaption.data('marginTop');
        var entering = ev.type === 'mouseenter';
        if ( ! marginTop ) {
          marginTop = parseInt(figcaption.css('margin-top'), 10);
          figcaption.data('marginTop', marginTop);
        }
        figcaption.css({
          marginTop: entering ? -figcaption.outerHeight() : marginTop
        });
      });
    });
  }


  // MixItUp
  var entities = body.find('section[typeof="Organization"]').on('click', function() {
    var entity = $(this);
    if ( entity.hasClass('is-current') ) return;
    var height = entity.addClass('is-current').find('.entity-description').show().outerHeight();
    entity.animate({
      paddingBottom: height + 8
    }, 300).find('.entity-description').hide().slideDown(300);
    entity.siblings('.is-current').removeClass('is-current').animate({
      paddingBottom: 0
    }, 300).find('.entity-description').slideUp(300);
  }).on('click', 'h1', function(ev) {
    var entity = $(this).closest('section');
    if ( entity.hasClass('is-current') ) {
      ev.stopPropagation();
      entity.removeClass('is-current').animate({
        paddingBottom: 0
      }, 300).find('.entity-description').slideUp(300);
    }
  });

  body.find('div.filterable[vocab]').each(function() {
    var hash = location.hash.substr(1),
        grid = $(this).mixitup({
          targetSelector: '[typeof="Organization"]',
          filterSelector: '[data-filter]',
          showOnLoad: ( hash && body.find('[data-filter="' + hash + '"]').length ? hash : 'all' ),
          onMixLoad: function() {
            var activeFilter = $filters.find('.active').text(),
                $currentFilter = $filters.find('.current-filter').removeClass("is-filled");
            if ( activeFilter ) {
              $currentFilter
                .addClass("is-filled")
                .text(activeFilter);
            }
          },
          onMixStart: function() {
            var activeFilter = $filters.find('.active').text(),
                $currentFilter = $filters.find('.current-filter').removeClass("is-filled");
            activeFilter = $.trim(activeFilter.toLowerCase()) === 'show all' ? '' : activeFilter;
            $currentFilter.one(transitionend, function() {
              $currentFilter
                .addClass("is-filled")
                .text(activeFilter);
            });
            window.setTimeout(function(){
              $currentFilter.trigger(transitionend);
            },300);
          }
        }),
        $filters = grid.prevAll('.filters');
    /*
    grid.prevAll('.filters').on('click', '.dropdown', function(ev, initial) {
      var $this = $(this);
      var activeFilter = $this.find('.active').text();
      var $currentFilter = $this.next('.current-filter').removeClass("is-filled");
      console.log(activeFilter);
      if ( ! initial ) {
        $this.find('ul')
          .show();
      }
      $currentFilter.one(transitionend, function() {
        $currentFilter
          .addClass("is-filled")
          .text(activeFilter);
      });
      window.setTimeout(function(){
        $currentFilter
          .addClass("is-filled")
          .text(activeFilter);
      },300);

      ev.stopPropagation();
    }).find('.dropdown').trigger('click', [true]);
    */
  });



  // Accessibility-specific stuff
  var mainnav = $('#mainnav');
  mainnav.find('.nav-links').attr('role', 'menubar').each(function() {
    var menubar = $(this);
    menubar.attr('aria-activedescendant', menubar.find('[aria-selected="true"]').attr('id'));
  });
  var folders = mainnav.find('.folder:not(.menu)').each(function(i) {
    var folder = $(this).attr('role', 'presentation').on('tab:blur', function() {
      folder.removeClass('is-focused').find('>a,>button').attr('tabindex', 0);
      folderSub.attr({
        role: 'menu',
        'aria-hidden': 'true',
        'aria-expanded': 'false',
        'aria-labelledby': labelledby.attr('id')
      }).find('a,input').attr('tabindex', -1);
    }).on('keydown', function(ev) {
      if ( 36 < ev.which && ev.which < 41 ) {
        if ( ev.which === 37 ) {
          folders.eq( i - 1 ).find('>a,>button').trigger('focus');
        } else if ( ev.which === 39 ) {
          folders.eq( i + 1 ).find('>a,>button').trigger('focus');
        } else if ( folder.attr('aria-haspopup') ) {
          var focused = folderSub.find('a:focus');
          if ( ev.which === 40 ) {
            if ( ! focused.length ) {
              focused = folderSub.find('a:first');
            } else {
              focused = focused.next();
            }
          } else if ( ev.which === 38 ) {
            if ( ! focused.length ) {
              focused = folderSub.find('a:last');
            } else {
              focused = focused.prev();
            }
          }
          if ( focused.length ) {
            focused.trigger('focus');
          }
        }
        return false;
      } else if ( ev.target.href && ev.which === 32 ) {
        if ( ev.target.pathname !== location.pathname ) {
          location.href = ev.target.href;
        } else {
          $(ev.target).trigger('click');
        }
        ev.stopPropagation();
        ev.preventDefault();
      }
    });
    var labelledby = folder.find('a,button').attr('role', 'menuitem').first().on('focus', function() {
      folder.siblings().trigger('tab:blur');
    });
    var folderSub = folder.has('.folder-sub').attr('aria-haspopup', 'true').on('focusin', '>a,>button', function(ev) {
      folder.addClass('is-focused').siblings().trigger('tab:blur');
      if ( this.nodeName.toLowerCase() === 'button' ) {
        $(this).attr('tabindex', -1);
      }
      folderSub.attr({
        'aria-hidden': 'false',
        'aria-expanded': 'true'
      }).find('input').attr('tabindex', 0).first().trigger('focus');
    }).on('focusout', function() {
      setTimeout(function() {
        if ( ! folder.find(':focus').length ) {
          folder.trigger('tab:blur');
        }
      });
    }).find('.folder-sub');
    folder.trigger('tab:blur');
  });
  var tiles = body.find('http://www.quantaservices.com/wp-content/themes/quanta-wordpress-theme/js/div.tile').each(function() {
    $(this).attr('tabindex',0)
      .find('a').attr('tabindex', -1);
  });


  if($('.timeline').length>0)
  {
	  sizeTimes = 100 / $('.timeline li').length;
	  console.log(sizeTimes);
	  $('.timeline li time').css('width', sizeTimes+'%');
  }


})(jQuery);