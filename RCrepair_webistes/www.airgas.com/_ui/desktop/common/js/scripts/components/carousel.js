/* jshint camelcase: false */
'use strict';

/**
 * Carousel component will handle various types of carousels and invoke the slidesjs plugin which is included as a
 * dependency in the require statement.
 */
require(['main'], function() {
	require(['domReady', 'jquery', 'carousel'], function (domReady, $) {
		domReady(function carousel() {
			var carouselInit = function () {

				// declare variables to be used throughout the app
				var breakpoint = 600,
                    carousels = $('.carousel'),
					fiveColCar = $('.five-col-carousel'),
					tabsFifthChild = $('.content-tabs'),
					singleCar,
					currentCarouselElement,
					maxHeight = 0,
					maxWidth = 0,
					windowWidth;

				// cache the window width for handling a responsive carousel
				windowWidth = $(window).width();

                //if window is mobile/small - force carousel option type to single
                singleCar = (windowWidth < breakpoint);

				// if function to get height
				function getTallestItem(item) {
					var height = item.height();
					if (height > maxHeight) {
						maxHeight = height;
					}
				}
				// function to set height wherever applied
				function setFixedHeight(item) {
					if (carousels){
						var divObj = item.find('.slidesjs-control, .slidesjs-container');
						divObj.height('375');
						return true;
					}
				}
				// remove border on fifth child for content tabs carousels (pdp)
				function removeBorder(item) {
					if (carousels){
						var divObj = item.find('.product-tile:nth-child(5)');
						divObj.css('border','0');
						return true;
					}
				}
				// remove extra classes on home page feature carousels
				function removeExtraClasses() {
					if (carousels){
						var divObj = $('.page-homepage .carousel');
						divObj.removeClass('responsive');
						return true;
					}
				}
				// for 5-colcar, if one product and viewing in mobile, expand product tile 100% width
				function singleCarInFiveCar() {
					var windowWidth = $(window).width();
					var divObj = $('.five-col-carousel');
					var numProd = $('.product-tile').length;
					if (windowWidth < 600 && numProd === 1) {
						divObj.addClass('single-step');
						return true;
					} else {
						divObj.removeClass('single-step');
					}
				}
				
				// iterate through carousels in the dom to handle dom manipulation and invoking plugin
				carousels.each(function carouselsLoop(index, value) {
					// declare variables
					var $obj = $(value),
                        children = $obj.children('.tile'),
                        opts = $obj.data('options');
					
					if (children.length <= 1) {
						children.show();
						return true;
					}
					//if window is mobile/small - force carousel option type to single
					if (singleCar === true) {
						opts.type = 'single';
						//add a class here to size height in css for mobile and override slidejs inline height declr
						carousels.addClass('single-step');
						carousels.removeClass('responsive');

					}
					else if (!singleCar && opts.type === 'single') {
					// weird logic to bypass for home hero carousel because of top level boolean
					}
					else {
						// TODO: search for class on carousel
						opts.type = 'five';
						carousels.removeClass('single-step');
					}
					
					// for each carousel  as determine how to wrap the carousel tiles around the content
					switch (opts.type) {
						case 'single':
							children = $obj.children('.tile');
							maxWidth = $obj.width();
							opts.width = maxWidth;
							children.each(function () {
								getTallestItem($(this));
								// TODO Determine height is set
								//return opts.height = maxHeight + 40;
							});
							// TODO: unwrapping/undo function for other opts.types
							break;
						case 'three':
							// get all the children with class .product-tile. Note: this filters .pagination child nodes
							children = $obj.children('.tile');
							// iterate through the children to handle grouping 3 children nodes per carousel tile
							children.each(function (index, value) {
								// for every 3rd child node create a new div and append the current child node
								if ((index % 3) === 0) {
									// for the initial and third index create the carousel container div
									currentCarouselElement = $(document.createElement('div'));
									currentCarouselElement.addClass('carousel-tile three');
									// append the current childe node to container div
									currentCarouselElement.append(value);
									// append the carousel-tile div to the current .carousel div
									$obj.append(currentCarouselElement);
								} else {
									// for children in between assume a carousel is created and append current node to it.
									currentCarouselElement.append(value);
								}
							});
							break;
						case 'four':
							// get all the children with class .product-tile. Note: this filters .pagination child nodes
							children = $obj.children('.tile');
							// iterate through the children to handle grouping 5 children nodes per carousel tile
							children.each(function (index, value) {
								// for every 4th child node create a new div and append the current child node
								if ((index % 4) === 0) {
									// for the initial and fourth index create the carousel container div
									currentCarouselElement = $(document.createElement('div'));
									currentCarouselElement.addClass('carousel-tile four');
									// append the current childe node to container div
									currentCarouselElement.append(value);
									// append the carousel-tile div to the current .carousel div
									$obj.append(currentCarouselElement);
								} else {
									// for children in between assume a carousel is created and append current node to it.
									currentCarouselElement.append(value);
								}
							});
							break;
						case 'five':
							// get all the children with class .tile. Note: this filters .pagination child nodes
							children = $obj.children('.tile');
							// iterate through the children to handle grouping 5 children nodes per carousel tile
							children.each(function (index, value) {
								// for every 5th child node create a new div and append the current child node
								if ((index % 5) === 0) {
									// for the initial and fifth index create the carousel container div
									currentCarouselElement = $(document.createElement('div'));
									currentCarouselElement.addClass('carousel-tile five');
									// append the current childe node to container div
									currentCarouselElement.append(value);
									// append the carousel-tile div to the current .carousel div
									$obj.append(currentCarouselElement);
								} else {
									// for children in between assume a carousel is created and append current node to it.
									currentCarouselElement.append(value);
								}
							});

							break;
						default:
							break;
					}
                    // invoke slides js plugin with options
					$obj.slidesjs({
						width: opts.width,
						height: opts.height,
						aspectRatio:false,
						navigation: {active: opts.navigation},
						pagination: {active: opts.pagination},
						play: {
							active:false,
							auto: opts.autoplay,
							interval:5000,
							pauseOnHover:true,
							restartDelay:2500
						}
					});

                    // bind mouse hover events to carousel for desktop
                    if (windowWidth > breakpoint) {
                        // bind mouse over event to handle displaying left/right navigation arrows
                        $obj.mouseover(function carouselsMouseOver() {
                            if ($(this).data('options').navigation) {
                                if ($('html').hasClass('lt-ie9')) {
                                    $(this).find('.slidesjs-navigation').show();
                                } else {
                                    $(this).find('.slidesjs-navigation').fadeIn(500);
                                }
                            }
                        });

                        // bind mouse leave event to handle displaying left/right navigation arrows
                        $obj.mouseleave(function carouselsMouseLeave() {
                            if ($('html').hasClass('lt-ie9')) {
                                $(this).find('.slidesjs-navigation').hide();
                            } else {
                                $(this).find('.slidesjs-navigation').fadeOut(500);
                            }
                        });
                    }
					
				});
				
				setFixedHeight(fiveColCar);
				removeBorder(tabsFifthChild);
				removeExtraClasses();
				singleCarInFiveCar();
			};
			carouselInit();
			$(window).resize(function() {
				carouselInit();
			});
		});
	});
});