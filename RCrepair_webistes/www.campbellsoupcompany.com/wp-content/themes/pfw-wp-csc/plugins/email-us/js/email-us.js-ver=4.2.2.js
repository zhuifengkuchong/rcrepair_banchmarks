/**
 *   Email Us Validation
 */
jQuery(document).ready(function() {
	
	jQuery("#rbProductFeedback").prop("checked", true); 

	jQuery('#frm_productFeedback').show();
	jQuery('#frm_questions, #frm_shopfeedback, #frm_compliment').hide();

    //show and hide form based on selection of radio button
	jQuery('input[name="rbtn_emailus_form_type"]').click(function(){
            if(jQuery(this).attr("value")=="rbProductFeedback"){
               jQuery('#frm_productFeedback').show();
               jQuery('#frm_questions, #frm_shopfeedback, #frm_compliment').hide();
            }
            if(jQuery(this).attr("value")=="rbCompliment"){
               jQuery('#frm_compliment').show();
               jQuery('#frm_productFeedback, #frm_questions, #frm_shopfeedback').hide();
            }
            if(jQuery(this).attr("value")=="rbQuestions"){
               jQuery('#frm_questions').show();
               jQuery('#frm_productFeedback, #frm_shopfeedback, #frm_compliment').hide();
            }
             if(jQuery(this).attr("value")=="rbShopFeedback"){
               jQuery('#frm_shopfeedback').show();
               jQuery('#frm_productFeedback, #frm_questions, #frm_compliment').hide();
            }
        });
	
	//Product Feedback Form validation 
	jQuery("#frm_productFeedback").validate({
				errorClass: 'error',
				rules: {
					like_text: "required",
					brand: "required",
					product_name: "required",
					upc_code: {
						required: false,
						number: true
					},
					firstname: {
						required: true,
						textonly: true
					},
					lastname: { 
						required: true,
						textonly: true
					},
					email_address: {
						required: true,
						email: true
					},
					address1: "required",
					city: { 
						required: true,
						textonly: true
					},
					state: "required",
					zip: {
						required: true,
						//zipcode_valid: true
						number: true
					}
				},
				messages: {
					like_text: "Please input a message",
					brand: "Please select a brand",
					product_name: "Please select a product name",
					upc_code: "Please enter a valid UPC Code",
					firstname: {
						required: "Please input your First Name"
					},
					lastname: {
						required: "Please input your Last Name",
					},
					email_address: {
						required: "Please enter a email address",
					},
					address1: "Please enter your address",
					city: {
						required: "Please enter city"
					},
					state: "Please enter state",
					zip: {
						required: "Please enter zip code"
					}
				}
		
	});
	
	//Compliment Form validation 
	jQuery("#frm_compliment").validate({
				errorClass: 'error',
				rules: {
					like_text: "required",
					firstname: {
						required: true,
						textonly: true
					},
					lastname: { 
						required: true,
						textonly: true
					},
					email_address: {
						required: true,
						email: true
					},
					city: { 
						required: false,
						textonly: true
					},
					zip: {
						required: false,
						number: true
					}
				},
				messages: {
					like_text: "Please input a message",
					firstname: {
						required: "Please input your First Name"
					},
					lastname: {
						required: "Please input your Last Name",
					},
					email_address: {
						required: "Please enter a email address",
					},
					address1: "Please enter your address"
						
				}
		
	});

	//Question Form validation 
	jQuery("#frm_questions").validate({
				errorClass: 'error',
				rules: {
					like_text: "required",
					firstname: {
						required: true,
						textonly: true
					},
					lastname: { 
						required: true,
						textonly: true
					},
					email_address: {
						required: true,
						email: true
					},
					city: { 
						required: false,
						textonly: true
					},
					zip: {
						required: false,
						number: true
					}
				},
				messages: {
					like_text: "Please input a message",
					firstname: {
						required: "Please input your First Name"
					},
					lastname: {
						required: "Please input your Last Name",
					},
					email_address: {
						required: "Please enter a email address",
					},
					address1: "Please enter your address"
						
				}
		
	});

	//Shop FeedBack Form validation 
	jQuery("#frm_shopfeedback").validate({
				errorClass: 'error',
				rules: {
					like_text: "required",
					firstname: {
						required: true,
						textonly: true
					},
					lastname: { 
						required: true,
						textonly: true
					},
					email_address: {
						required: true,
						email: true
					}
				},
				messages: {
					like_text: "Please input a message",
					firstname: {
						required: "Please input your First Name"
					},
					lastname: {
						required: "Please input your Last Name",
					},
					email_address: {
						required: "Please enter a email address",
					}
				},
				submitHandler: function(form) { 
				
						var contact_me = "NO";
						
						if(jQuery( "#contact_me" ).prop( "checked" )){
							contact_me = "Yes";
						}
												
						var frm_data = jQuery(form).serialize()+"&contact_me="+contact_me;
						
						jQuery.ajax({ url: "http://images.epiinc.com/csmail/emailcap.php", 
							type: "POST", 
							data: frm_data, 
							success: function(response) { 
							}, 
							error: function(response) { 
							}
						}); 
					form.submit();
				}
		
	});
	
	
	jQuery.validator.addMethod("textonly", function(value, element) {
		// allow text and spaces only
		return this.optional( element ) || /^\s*[a-zA-Z,\s]+\s*$/.test( value );
		}, 'The input should be text');
		
	jQuery.validator.addMethod("email", function(value, element) { 
		return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value); 
	}, "Please enter a valid email address.");

	jQuery.validator.addMethod("zipcode_valid", function(value, element) { 
		return this.optional(element) || /^\d{5}$/i.test(value);
	}, "The zipcode must contain 5 digits");
	
});


function get_product_name(value){
		jQuery('#csc_brand_name').empty().val(jQuery('#brand option:selected' ).text()); 
	    jQuery.ajax({
	    type: 'POST',   // Adding Post method
	    url: ajaxurl, // Including ajax file
	    data: {"action": "brand_product_name", "postid":value}, 
	    success: function(data){ 
	        jQuery('#product_name').empty().html(data);
	    }
	    });
}