
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:addthis="http://www.addthis.com/help/api-spec">
<head>
    <meta http-equiv="Content-Language" content="en-us" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Error 404: Page Not Found | Lear Corporation</title>
	<meta name="description" content="The Official Site of Lear Corporation. Leading global supplier of automotive seating systems and electrical power management systems" />
	<meta name="keywords" content="" />
	<meta name="pubdate" content="7/29/2010 8:21:34 PM" />
	<meta name="rating" content="General" />
	<meta name="revisit-after" content="14 days" />
	<meta name="robots" content="all, index, follow" />
	<meta name="classification" content="" />
	<meta name="distribution" content="Global" />
	<meta name="location" content="USA, Michigan" />
	<meta name="copyright" content="Copyright 2015 Lear All rights reserved. Material herein is the property of Lear and may not be reproduced in any form without prior written permission from Lear." />
	<meta http-equiv="imagetoolbar" content="no" />
    
	<link href="/assets/css/master.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/assets/css/print.css" rel="stylesheet" type="text/css" media="print" />
    
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script>
	<script src="/assets/js/jquery.tools.min.js" type="text/javascript"></script>
	<script src="/assets/js/jquery.nivo.slider.pack.js" type="text/javascript"></script>
	<script src="/assets/js/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script src="/assets/js/organictabs.jquery.js" type="text/javascript"></script>
    <script src="/assets/js/jquery.easyListSplitter.js" type="text/javascript"></script>
	<script src="/assets/js/jquery.validate.pack.js" type="text/javascript"></script>
	<script src="/assets/js/swfobject.js" type="text/javascript"></script>
	<script src="/assets/js/ready.js" type="text/javascript"></script>
	<script src="/assets/js/XHConn.js" type="text/javascript"></script>

	<!--[if lte IE 8]><link href="/assets/css/ie.css" rel="stylesheet" type="text/css" media="all" />
<![endif]-->
	<!--[if lte IE 7]><link href="/assets/css/ie7.css" rel="stylesheet" type="text/css" media="all" />
<![endif]-->
	<!--[if lte IE 6]><link href="http://universal-ie6-css.googlecode.com/files/ie6.1.1.css" rel="stylesheet" type="text/css" media="screen, projection">
<![endif]-->

</head>

<body id="misc" class="sub">
	
		
	<!-- begin wrapper -->
	<div id="wrapper">
			
			
		<!-- header -->
		<div id="header">
			<h1 id="logo"><a href="/en/"><img src="/assets/images/logo.gif" alt="Lear" /></a></h1>

			<ul id="nav-utility" class="inline">
              <li class="addthis">
					<div class="addthis_toolbox addthis_default_style">
						<a href="http://addthis.com/bookmark.php?v=250&amp;username=xa-4c2cbf57015fb32d" class="addthis_button icons replace">Add This</a>
					</div>
					<script type="text/javascript">
						if ($.browser.safari) {
							var addthis_config = {
								ui_language: "en",
								ui_delay: 0, 
								ui_offset_left:-135,
								ui_offset_top:-18
							}
						} else {
							var addthis_config = {
								ui_language: "en",
								ui_delay: 0, 
								ui_offset_left:-135,
								ui_offset_top:-18
							}
						};
					</script>
					<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4c2cbf57015fb32d"></script>
				</li>
            	
              <li class="in replace clickable">LinkedIn<a href="http://www.linkedin.com/company/3701?trk=tyah&trkInfo=tarId%3A1398342685672%2Ctas%3Alear%20c%2Cidx%3A4-1-7" rel="external" alt="LinkedIn" title="LinkedIn"></a></li>
              <li class="yt replace clickable">YouTube<a href="https://www.youtube.com/channel/UCKgRMEqryl1ojLnu37LqtHA" rel="external" alt="YouTube" title="YouTube"></a></li>
				<li class="rss"><a href="/en/feeds/" class="icons replace" alt="RSS Feed" title="RSS Feed">RSS</a></li>
				<li class="languages">
					<a href="#" class="icons replace">Languages</a>
					<ul>
					
						 <li><a href="/en">English</a></li>
						
						 <li><a href="/cn">简体中文</a></li>
						
					</ul>
				</li>
			</ul>
	
			<form id="search" method="get" action="/search/">
				<fieldset>
					<input type="text" id="search-query" name="search" />
					<input type="submit" class="icons submit" value="" />
				</fieldset>
			</form>
	
			<ul id="nav-main" class="inline">
				<li id="nav-home">
					<a href="/en/">Home</a>
				</li>
				
				<li>
					<a href="/en/about/">Company</a>
					<ul>					
						<li><a href="/en/about/history.aspx">History</a></li>					
						<li><a href="/en/about/leadership.aspx">Leadership</a></li>					
						<li><a href="/en/about/recognition.aspx">Recognition</a></li>					
						<li><a href="/en/about/community-relations.aspx">Community Relations</a></li>					
						<li><a href="/en/resources/corporate-information.aspx">Corporate Information</a></li>
					</ul>
				</li>
				
				<li>
					<a href="/en/diversity/">Diversity</a>
					<ul>					
						<li><a href="/en/diversity/supplier-diversity.aspx">Supplier Diversity</a></li>					
						<li><a href="/en/careers/diversity.aspx">Workforce Diversity</a></li>					
						<li><a href="/en/about/community-relations.aspx">Community Relations</a></li>
					</ul>
				</li>
				
				<li>
					<a href="/en/seating/">Seating</a>
					<ul>					
						<li><a href="/en/seating/structure-systems.aspx">Structure Systems</a></li>					
						<li><a href="/en/seating/adjusters.aspx">Adjusters</a></li>					
						<li><a href="/en/seating/mechanisms.aspx">Mechanisms</a></li>					
						<li><a href="/en/seating/foam.aspx">Foam</a></li>					
						<li><a href="/en/seating/fabrics.aspx">Fabrics</a></li>					
						<li><a href="/en/seating/aventino-collection.aspx">Leather</a></li>					
						<li><a href="/en/seating/head-restraints.aspx">Head Restraints</a></li>					
						<li><a href="/en/seating/system-features.aspx">System Features</a></li>					
						<li><a href="/en/seating/innovations.aspx">Innovations</a></li>
					</ul>
				</li>
				
				<li>
					<a href="/en/electrical/">Electrical</a>
					<ul>					
						<li><a href="/en/electrical/high-power.aspx">High Power</a></li>					
						<li><a href="/en/electrical/electrical-distribution-systems.aspx">Electrical Distribution Systems</a></li>					
						<li><a href="/en/electrical/terminals-and-connectors.aspx">Terminals and Connectors</a></li>					
						<li><a href="/en/electrical/junction-box.aspx">Junction Box</a></li>					
						<li><a href="/en/electrical/wireless-technology.aspx">Wireless Technology</a></li>					
						<li><a href="/en/electrical/body-electronics.aspx">Body Electronics</a></li>					
						<li><a href="/en/electrical/lighting.aspx">Lighting</a></li>					
						<li><a href="/en/electrical/audio.aspx">Audio</a></li>					
						<li><a href="/en/electrical/environmental-innovations.aspx">Environmental Innovations</a></li>
					</ul>
				</li>
				
				<li>
					<a href="/en/careers/">Careers</a>
					<ul>					
						<li><a href="/en/careers/faces.aspx">Faces of Lear</a></li>					
						<li><a href="/en/careers/diversity.aspx">Diversity</a></li>					
						<li><a href="https://learcareers.silkroad.com" rel="external">Job Opportunities - US</a></li>					
						<li><a href="https://learconnect.lear.com/employment" rel="external">Job Opportunities - Global</a></li>
					</ul>
				</li>
				
				<li>
					<a href="/en/contact/">Contact</a>
					<ul>					
						<li><a href="/en/contact/">Contact Form</a></li>					
						<li><a href="/en/contact/locations.aspx">Global Locations</a></li>
					</ul>
				</li>
				
			</ul>

			
		

			<ul id="nav-secondary" class="inline">
			

			 
			
				<li><a href="http://www.eagleottawa.com/" rel="external  ">Eagle Ottawa</a></li>
				<li><a href="http://www.guilfordtextiles.com/" rel="external  ">Guilford Textiles</a></li>
				<li><a href="/en/ideaportal/">Idea Portal</a></li>
				<li><a href="http://ir.lear.com/">Investor Relations</a></li>
				<li><a href="/en/supplier_info/">Supplier Information</a></li>
				<li><a href="/en/press/">Press Room</a></li>
                
                <!--<li><a href="http://www.lear.com/blog/" target="_blank">Blog</a></li>-->
                
			</ul>
			
			
			

			<hr />	

		</div>
		<!-- end header -->
		
		
			
			
		<!-- begin content -->
		<div id="content">
			
			
			<!-- begin main content -->
			<div id="content-primary" class="inline">
				
				
				

				
				<!-- begin article -->
				<div class="article inline wide">
			

					<!-- headline -->
					<h1>Error 404: Page Not Found</h1>
				

					<!-- begin body -->
					<p>The page that you are looking for doesn't exist. It may have been removed from our site, had its name changed, or is temporarily unavailable.</p>
<ul class="condensed">
    <li>Return to the <a href="/en/">home page</a></li>
    <li>Use the site search box at the top of the page</li>
    <li>View the <a href="/en/site-map/">site map</a></li>
    <li>If you think this is our mistake, please <a href="/en/contact/form.aspx">contact us</a> to report the error.</li>
</ul>


					<!-- end body -->
					
					
					

					
				</div>
				<!-- end article -->
				
				
			</div>
			<!-- end main content -->
			
			
			<!-- section sidebar -->
			<div id="section" class="sidebar inline">
  			


			</div>
			<!-- end section sidebar -->

			
		</div>
		<!-- end content -->


		<!-- footer -->
		<div id="footer">
			
			<hr />

			<ul id="nav-footer" class="inline">
		   		<li><a href="/en/">Home</a></li>
		   		<li><a href="/en/about/">Company</a></li>
		   		<li><a href="/en/diversity/">Diversity</a></li>
		   		<li><a href="/en/seating/">Seating</a></li>
		   		<li><a href="/en/electrical/">Electrical</a></li>
		   		<li><a href="/en/careers/">Careers</a></li>
		   		<li><a href="/en/contact/">Contact</a></li>
		   		<li><a href="/en/supplier_info/">Supplier Information</a></li>
		   		<li><a href="/en/press/">Press</a></li>
		   		<li><a href="/en/ideaportal/">Idea Portal</a></li>	<li><a href="/en/resources/">Resources</a></li>
			</ul>
	
			<!--<p id="moncur"><span>web site by Moncur Associates, a branding firm.</span> <a class="icons replace" href="http://www.moncurassociates.com" rel="external">web site by Moncur Associates, a branding firm.</a></p>-->
			
			<p>&copy; 2015 Lear Corporation. All rights reserved. <a href="/en/privacy-policy/">Privacy Policy</a> <a href="/en/site-map/">Site Map</a> <a href="/user_area/uploads/Lear-CA-Supply-Chains-Act-Disclosure-4-24-2015.pdf">California Supply Chain Act</a></p>

		</div>
		<!-- end footer -->
		


<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6071370-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script> 


	</div>
	<!-- end wrapper -->


</body>
</html>