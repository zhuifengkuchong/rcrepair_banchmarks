$(document).ready(function () {

    /*
    ----------------------------------------------------------
    GENERAL
    ---------------------------------------------------------- */

    // EXTERNAL WINDOWS
    $('a[rel*="external"]').click(function (e) {
		window.open($(this).attr('href'));		
		return false;
    });


    // MONCUR LOGO ANIM
    $("#moncur a").hover(function () {
        $(this).prev("span").animate({ opacity: "show" }, "slow");
    }, function () {
        $(this).prev("span").animate({ opacity: "hide" }, "fast");
    });


    // SET HOMEPAGE 'HOME' NAV ITEM TO ACTIVE
    if ($("body").attr("id") == "home") { $("#nav-home a").addClass("active"); }


    // APPEND ICONS
    $("li.append").append("<span class='icons'></span>");
    $("div.append").append("<span class='icons'></span>");


    // MAKE ENTIRE AREA CLICKABLE
    $(".clickable").click(function () {
        if ($(this).find("a").attr("rel") == "external") {
            window.open($(this).find("a").attr("href"))
        } else {
            window.location = $(this).find("a").attr("href"); return false;
            return;
        }
    });


    // CLEAR VALUE ON FOCUS
    $('.labelvalue').each(function () {
        var default_value = this.value;
        $(this).focus(function () {
            if (this.value == default_value) {
                this.value = '';
            }
        });
        $(this).blur(function () {
            if (this.value == '') {
                this.value = default_value;
            }
        });
    });


    /*
    ----------------------------------------------------------
    SHOW / HIDE CONTENT TOGGLE
    ---------------------------------------------------------- */
    $(".extranet-category ul").hide();

    $('.extranet-category a').click(function () {
        $(this).toggleClass("active");
        $(this).next("ul").slideToggle("normal");
        return true;
    });

    $(".extranet-category.active ul").show(function () {
        $(this).prev("a").addClass("active");
    });
	
	
	$(".aventino-list_title ul").hide();

    $('.aventino-list_title a').click(function () {
        $(this).toggleClass("active");
        $(this).next("ul").slideToggle("normal");
        return false;
    });

    $(".aventino-list_title.active ul").show(function () {
        $(this).prev("a").addClass("active");
    });
	
	$('.twocolumnlist').easyListSplitter({
        colNumber: 2,
        direction: 'vertical'
    });


    /*
    ----------------------------------------------------------
    SET EQUAL HEIGHT COLUMNS: HOMEPAGE
    ---------------------------------------------------------- */
    function setEqualHeight(columns) {
        var tallestcolumn = 0;
        columns.each(
		function () {
		    currentHeight = $(this).height();
		    if (currentHeight > tallestcolumn) {
		        tallestcolumn = currentHeight;
		    }
		}
		);
        columns.height(tallestcolumn);
    }

    // INITIALIZE EQUAL HEIGHT
    setEqualHeight($("#section-intros > div"));



    /*
    ----------------------------------------------------------
    CONFIGURE SCROLLABLES: NEWS TICKER, HOMEPAGE CAROUSEL
    ---------------------------------------------------------- */
    $("#ticker div.scrollable").scrollable({ circular: true, next: '#nav-news a.forward', prev: '#nav-news a.back', speed: 1500 }).autoscroll({ autoplay: true, interval: 8000 });
    $("#carousel.scrollable").scrollable({ circular: true, speed: 2000 }).navigator({ navi: '#nav-carousel' }).autoscroll({ interval: 10000 });


    /*
    ----------------------------------------------------------
    FACES OF LEAR
    ---------------------------------------------------------- */
    $("#faces div.inline p.toggle a").toggle(function () {
        $(this).parents().next("blockquote").animate({ top: "-=200" }, "normal", "linear");
        $(this).parent().animate({ bottom: "+=200" }, "normal", 'linear');
        $(this).addClass("open");
    }, function () {
        $(this).parents().next("blockquote").animate({ top: "+=200" }, "normal", "linear");
        $(this).parent().animate({ bottom: "-=200" }, "normal", "linear");
        $(this).removeClass("open");
    });

    $("#faces div.inline:nth-child(3n)").addClass("nomargin");
    $("#faces div.inline:nth-child(3n+1)").addClass("clear");


    /*
    ----------------------------------------------------------
    SUPPLIER DIVERSITY FORM / AVENTINO COLLECTION TABS
    ---------------------------------------------------------- */
    $(function () {

        $("#formTabs").organicTabs({
            "speed": 200
        });

    });
	
	$(function () {

        $("#collectionTabs").organicTabs({
            "speed": 200
        });

    });


    /*
    ----------------------------------------------------------
    FORM VALIDATION
    ---------------------------------------------------------- */
    $.validator.addMethod("NumbersOnly", function (value, element) {
        return this.optional(element) || /^[0-9\-\+]+$/i.test(value);
    }, "Phone must contain only numbers, + and -.");

    $.validator.addMethod("DistinctEmail", function (value, element) {

        if (this.optional(element)) {
            return true;
        } else if ($("#dfUID").val() == '0') {

            var validateResult = false;

            $.ajax({
                url: 'http://www.lear.com/assets/inc/ajax/DiversityFormCheckEmail.aspx',
                type: 'POST',
                data: { email: $('#df-email').val() },
                async: false,
                success: function (data) {
                    if (data == '1') {
                        validateResult = true;
                    }

                }
            });

            return validateResult;
        }
        return true;
    }, "^ Email address already used with another account");



    $("#contact-form").validate();

    $("#errorMsg").hide();

    $("#diversity-form").validate({
        rules: {
            password: {
                required: function (element) {
                    return $("#dfUID").val() == '0';
                },
                minlength: 6
            },
            password2: {
                required: function (element) {
                    return $("#dfUID").val() == '0' || $("#df-password").val() != '';
                },
                equalTo: "#df-password"
            },
            email: {
                DistinctEmail: true
            }
        },
        messages: {
            email: {
                DistinctEmail: "^ Email address already used with another account",
                required: "^ Please enter an email address"
            }
        },
        ignore: ".ignore",
        invalidHandler: function (form, validator) {
            $("#errorMsg").show();
            $(".nav li a").removeClass("current");
            $(".nav .tab-one a").addClass("current");
            $(".list-wrap ul").hide();
            $(".list-wrap ul#gen").show();
            $(".list-wrap").height(980);
        },
        submitHandler: function (form) {

            //alert('submit here');
            form.submit();
        }
    });


    /*
    ----------------------------------------------------------
    PRODUCT SLIDESHOWS: FANCYBOX
    ---------------------------------------------------------- */
    $("#slider div a").fancybox({
        'cyclic': 'true',
        'titlePosition': 'inside',
        // 'showCloseButton'	: false,
        'onStart': pauseSlideshow,
        'onClosed': resumeSlideshow,
        'titleFormat': formatTitle
    });


    // FANCYBOX CAPTIONS
    function formatTitle(title, currentArray, currentIndex, currentOpts) {
        return '<div class="caption">' + (title && title.length ? '<strong>' + title + '</strong>' : '') + 'Image ' + (currentIndex + 1) + ' of ' + currentArray.length + '</div>';

        // return '<div class="caption"><span><a href="javascript:;" onclick="$.fancybox.close();">Close X</a></span>' + (title && title.length ? '<strong>' + title + '</strong>' : '' ) + 'Image ' + (currentIndex + 1) + ' of ' + currentArray.length + '</div>';
    }


    // PAUSE SLIDESHOW WHEN FANCYBOX OPENS
    function pauseSlideshow() {
        $('#slider div').data('nivo:vars').stop = true;
    }


    // RESUME SLIDESHOW WHEN FANCYBOX CLOSES
    function resumeSlideshow() {
        $('#slider div').data('nivo:vars').stop = false;
    }
	
});

// END ON READY



/*
----------------------------------------------------------
PRODUCT SLIDESHOWS: NIVO SLIDER
---------------------------------------------------------- */
$(window).load(function() {
	$('#slider div').nivoSlider({
		effect:'fade', //Specify sets like: 'fold,fade,sliceDown'
		slices:1,
		animSpeed:500,
		pauseTime:3000,
		directionNav:false, //Next & Prev
		directionNavHide:true, //Only show on hover
		controlNav:true, //1,2,3...
		keyboardNav:false, //Use left & right arrows
		pauseOnHover:true, //Stop animation while hovering
	    controlNavThumbs:false, //Use thumbnails for Control Nav			
		manualAdvance:false, //Force manual transitions
		captionOpacity:1 //Universal caption opacity
	});
	$('#slider2 div').nivoSlider({
		effect:'fade', //Specify sets like: 'fold,fade,sliceDown'
		slices:1,
		animSpeed:500,
		pauseTime:3000,
		directionNav:false, //Next & Prev
		directionNavHide:true, //Only show on hover
		controlNav:false, //1,2,3...
		keyboardNav:false, //Use left & right arrows
		pauseOnHover:true, //Stop animation while hovering
	    controlNavThumbs:false, //Use thumbnails for Control Nav			
		manualAdvance:false, //Force manual transitions
		captionOpacity:0 //Universal caption opacity
	});
	$('#slider3 div').nivoSlider({
		effect:'fade', //Specify sets like: 'fold,fade,sliceDown'
		slices:1,
		animSpeed:100,
		pauseTime:500,
		startSlide:0, //Set starting Slide (0 index)
		slideshowEnd: function(){
			$('#slider3 div').data('nivo:vars').stop = true;
			$('#slider3 div.nivo-slice').addClass("stop");
		}
	});
	$('#slider4 div').nivoSlider({
		effect:'fade', //Specify sets like: 'fold,fade,sliceDown'
		slices:1,
		animSpeed:250,
		pauseTime:1500,
		startSlide:0, //Set starting Slide (0 index)
		slideshowEnd: function(){
			$('#slider4 div').data('nivo:vars').stop = true;
			$('#slider4 div.nivo-slice').addClass("stop");
		}
	});
	$('#slider5 div').nivoSlider({
		effect:'fade', //Specify sets like: 'fold,fade,sliceDown'
		slices:1,
		animSpeed:100,
		pauseTime:750,
		startSlide:0, //Set starting Slide (0 index)
		slideshowEnd: function(){
			$('#slider5 div').data('nivo:vars').stop = true;
			$('#slider5 div.nivo-slice').addClass("stop");
		}
	});
});
