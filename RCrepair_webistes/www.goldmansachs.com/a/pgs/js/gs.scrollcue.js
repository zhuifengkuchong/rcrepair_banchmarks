//todo: update this later.
var GS = window['GS'] || {};

GS.Scrollcue = (function($) {

	function initCue($doc, $window) {
		var $doc = $(document),
			$scrollCue = $('<div class="scrollCue">More</div>');
		$doc.find('body').append($scrollCue);

		centerBottom($scrollCue);

		$(window).scroll(function(){
			$scrollCue.fadeOut(1000);
		});

		$(window).on('resize', function(){
			centerBottom($scrollCue);
		});

	}

	function centerBottom($el) {
		var newTop =   $(window).height() - $el.height();
		var newLeft = ($(window).width()  - $el.width()) / 2;

			$el.css({
		    	'position': 'fixed',
		    	'left': newLeft,
		    	'top': newTop
		    });

		    $el.addClass('active');
	}

	$(document).ready(function(){
		if($(window).scrollTop() === 0) {
			initCue();
		}
	})
})(jQuery);
