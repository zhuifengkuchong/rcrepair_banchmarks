humana.responsive = (function () {
	var responsive = {};

	var
		// cache of callbacks
		callbacks = [],
		// stores the current matching profile
		currentProfile = null;

	// default device profile widths
	var widths = {
		//        min, max
		small:   [0, 767],
		medium:  [768, 979],
		large:   [980]
	};

	// determines if the specified profile is valid for the current resolution
	var isValidProfile = function (profile) {
		if (typeof widths[profile] !== 'undefined') {
			return Response.band.apply(Response, widths[profile]);
		} else {
			return false;
		}
	};

	// fires onback and offback function when profile(s) either match or no longer
	// match, respectively
	var when = function (profiles, onback, offback) {
		var thisCallback;

		// force profiles to be an array
		profiles = Array.isArray(profiles) ? profiles : [profiles];

		// check to see if the specified profiles match the current profile and
		// invoke the onback function
		if (currentProfile !== null && profiles.indexOf(currentProfile) !== -1) {
			onback();
		}

		// create a new callback object and store it in the callback cache
		thisCallback = {
			profiles: profiles,
			onback: onback,
			offback: offback
		};

		callbacks.push(thisCallback);

		// return a cancel function to unsubscribe on/offbacks
		return function () {
			callbacks.forEach(function (callback, index) {
				// if this callback object matches our current callback object, remove it
				// from the callbacks list and cancel the forEach loop
				if (_.isEqual(callback, thisCallback)) {
					callbacks.splice(index, 1);
					return false;
				}
			});
		};
	};

	// handle resize/ready events, emits events on change
	Response.action(function () {
		var newProfile;

		// determine which device profile matches based on the current width
		// if a profile matches, set it as the new profile
		// the first profile to match will be set as the current profile, the rest
		// will be discarded even if they may match
		Object.keys(widths).forEach(function (profile) {
			if (isValidProfile(profile)) {
				newProfile = profile;
				return false;
			}
		});

		// if the potentially new matching profile is not the current profile
		if (newProfile !== currentProfile) {
			// invoke the appropriate on/offbacks of the callbacks in the cache only
			// if the new profile is in the callback object's list of profiles and the
			// previous matching profile is not in the callback object's list of
			// profiles

			// call all appropriate offbacks first
			callbacks.forEach(function (callback) {
				if (!inArray(newProfile, callback.profiles) && inArray(currentProfile, callback.profiles)) {
					if (callback.offback) {
						callback.offback();
					}
				}
			});

			callbacks.forEach(function (callback) {
				// now call all appropriate onbacks
				if (inArray(newProfile, callback.profiles) && !inArray(currentProfile, callback.profiles)) {
					if (callback.onback) {
						callback.onback();
					}
				}
			});

			currentProfile = newProfile;
		}
	});

	function inArray(needle, haystack) {
		return haystack.indexOf(needle) !== -1;
	}

	// exporting
	responsive.getCurrentProfile = function () { return currentProfile; };
	responsive.when = when;
	responsive.widths = widths;

	return responsive;
}());
