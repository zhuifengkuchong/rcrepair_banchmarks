var MP = {
    /* mp_trans_disable_start */
    Version: 'https://www.humana.com/library/scripts/lib/1.0.23',
    Domains: { 'https://www.humana.com/library/scripts/lib/es-humana.com': 'https://www.humana.com/library/scripts/lib/espanol.humana.com',
        'https://www.humana.com/library/scripts/lib/es-care-plus-health-plans.com': 'https://www.humana.com/library/scripts/lib/es.care-plus-health-plans.com',
        'https://www.humana.com/library/scripts/lib/es-cacmedicalcenters.com': 'https://www.humana.com/library/scripts/lib/espanol.cacmedicalcenters.com'
    },
    SrcLang: 'en',
    domain: (function (a) {
        var b = a.match("[^.]+.(([^.]{2,3}.)?[^.]{2}|[^.]{3,})$");
        return b == null ? a : b[0];
    })(location.host),
    /* mp_trans_disable_end */
    UrlLang: 'mp_js_current_lang',
    SrcUrl: decodeURIComponent('mp_js_orgin_url'),
    /* mp_trans_disable_start */
    init: function () {
        if (MP.UrlLang.indexOf('p_js_') == 1) {
            MP.SrcUrl = window.top.document.location.href;
            MP.UrlLang = MP.SrcLang;
        }
        if (document.getElementById("hookBack") !== null) {
            $.ajax({
                type: 'GET',
                url: document.getElementById("hookBack").name,
                async: false,
                success: function (response) {
                }
            });
        }
    },
    getCookie: function (name) {
        var start = document.cookie.indexOf(name + '=');
        if (start < 0) return null;
        start = start + name.length + 1;
        var end = document.cookie.indexOf(';', start);
        if (end < 0) end = document.cookie.length;
        while (document.cookie.charAt(start) == ' ') { start++; }
        return decodeURIComponent(document.cookie.substring(start, end));
    },
    setCookie: function (name, value, path, domain) {
        var cookie = name + '=' + encodeURIComponent(value);
        if (path) cookie += '; path=' + path;
        if (domain) cookie += '; domain=' + domain;
        document.cookie = cookie;
    },
    switchLanguage: function (lang) {
        //console.log("domain:" + lang + "-" + MP.domain);
        if (lang != MP.SrcLang) {
            var script = document.createElement('SCRIPT');
            script.src = location.protocol + '//' + MP.Domains[lang + "-" + MP.domain] + '/' + MP.SrcLang + lang + '/?1023749632;' + encodeURIComponent(MP.SrcUrl);
            document.body.appendChild(script);
        } else if (lang == MP.SrcLang && MP.UrlLang != MP.SrcLang) {
            var script = document.createElement('SCRIPT');
            script.src = location.protocol + '//' + MP.Domains[MP.UrlLang + "-" + MP.domain] + '/' + MP.SrcLang + MP.UrlLang + '/?1023749634;' + encodeURIComponent(location.href);
            document.body.appendChild(script);
        }
        return false;
    },
    switchToLang: function (url) {
        window.top.location.href = url;
    }
    /* mp_trans_disable_end */
};