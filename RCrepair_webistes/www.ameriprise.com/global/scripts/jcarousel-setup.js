"use strict";

$(document).ready(function () {
    function jbannersInit(car) {
        $("#jbanners-nav a").click(function (e) {
            e.preventDefault();
            var n = jQuery.jcarousel.intval($(this).find("span").text());
            car.scroll(n);
        });

        var prev = $("#tax-banners .jcarousel-prev").wrap('<li class="prev" />').parent(),
            next = $("#tax-banners .jcarousel-next").wrap('<li class="next"/>').parent();

        $("#jbanners-nav").append(next).prepend(prev);
    }

    function jbannersLoad(car, state) {
        $("#jbanners-nav a.active").removeClass("active");
        $("#jbanners-nav li:eq(" + car.first + ") a").addClass("active");
    }

    $("#tax-banners").jcarousel({
        auto: 7,
        scroll: 1,
        animation: 'slow',
        wrap: 'both',
        buttonNextHTML: '<a href="#"><span>next</span></a>',
        buttonPrevHTML: '<a href="#"><span>prev</span></a>',
        itemFallbackDimension: '700',
        initCallback: jbannersInit,
        itemLoadCallback: jbannersLoad
    });
});