FSR.surveydefs = [{
    name: 'tablet',
    platform: 'tablet',
    invite: {
        when: 'onentry',
        dialogs: [[{
            reverseButtons: false,
            headline: "We'd welcome your feedback!",
            blurb: "You have been selected to participate in a brief customer satisfaction survey to let us know how we can improve your experience.",
            attribution: "Conducted by ForeSee.",
            declineButton: "No, thanks",
            acceptButton: "Yes, I'll help"
        }]]
    },
    pop: {
        when: 'now'
    },
    criteria: {
        sp: 75,
        lf: 1
    },
    include: {
        urls: ['https://www.ameriprise.com/client-login/logout.asp', 'https://www.qa.ameriprise.com/client-login/logout.asp']
    }
}, {
    name: 'browse',
    section: 'autohome',
    pin: '1',
    invite: false,
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 0,
        lf: 1
    },
    links: {
        attach: [{
            tag: 'input',
            attribute: 'src',
            patterns: ['Unknown_83_filename'/*tpa=https://www.ameriprise.com/global/scripts/forsee/2009/retrieve-btn.gif*/, 'Unknown_83_filename'/*tpa=https://www.ameriprise.com/global/scripts/forsee/2009/retrieve.gif*/, 'Unknown_83_filename'/*tpa=https://www.ameriprise.com/global/scripts/forsee/2009/getQuote_ford.gif*/, 'Unknown_83_filename'/*tpa=https://www.ameriprise.com/global/scripts/forsee/2009/get-quote-btn.gif*/, 'Unknown_83_filename'/*tpa=https://www.ameriprise.com/global/scripts/forsee/2009/getquote.gif*/, 'Unknown_83_filename'/*tpa=https://www.ameriprise.com/global/scripts/forsee/2009/getQuote.gif*/],
            sp: 25,
            when: 'later'
        }, {
            tag: 'input',
            attribute: 'id',
            patterns: ['getquote', 'login-btn'],
            sp: 25,
            when: 'later'
        }, {
            tag: 'a',
            attribute: 'class',
            patterns: ['retrieve'],
            sp: 25,
            when: 'later'
        }]
    },
    include: {
        urls: ['www.qa.ameriprise.com/auto-home-insurance/', 'www.ameriprise.com/auto-home-insurance/']
    }
}, {
    name: 'browse',
    section: 'serviceonline',
    pin: '1',
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 25,
        lf: 2
    },
    include: {
        urls: ['https://www.ameriprise.com/global/scripts/forsee/2009/serviceonline-new.ameriprise.com']
    }
}, {
    name: 'ameriprise-secure',
    pin: '1',
    invite: {
        when: 'onentry'
    },
    pop: {
        when: 'later'
    },
    criteria: {
        sp: 10,
        lf: 2
    },
    pin: 1,
    include: {
        urls: ['https://www.ameriprise.com/global/scripts/forsee/2009/ameripriseadvisors.com', 'https://www.ameriprise.com/global/scripts/forsee/2009/ameriprise.com']
    }
}];
FSR.properties = {
    repeatdays: 90,
    
    repeatoverride: false,
    
    altcookie: {},
    
    language: {
        locale: 'en'
    },
    
    exclude: {},
    
    zIndexPopup: 10000,
    
    ignoreWindowTopCheck: false,
    
    ipexclude: 'fsr$ip',
    
    mobileHeartbeat: {
        delay: 60, /*mobile on exit heartbeat delay seconds*/
        max: 3600 /*mobile on exit heartbeat max run time seconds*/
    },
    
    invite: {
    
        // For no site logo, comment this line:
        siteLogo: "sitelogo.gif"/*tpa=https://www.ameriprise.com/global/scripts/forsee/2009/sitelogo.gif*/,
        
        //alt text fore site logo img
        siteLogoAlt: "",
        
        dialogs : [[{
            reverseButtons: false,
            headline: "We'd welcome your feedback!",
            blurb: "Thank you for visiting our website. You have been selected to participate in a brief customer satisfaction survey to let us know how we can improve your experience.",
            noticeAboutSurvey: "The survey is designed to measure your entire experience, please look for it at the <u>conclusion</u> of your visit.",
            attribution: "This survey is conducted by an independent company ForeSee, on behalf of the site you are visiting.",
            closeInviteButtonText: "Click to close.",
            declineButton: "No, thanks",
            acceptButton: "Yes, I'll give feedback"
        }]],
        
        exclude: {
            urls: ['https://www.ameriprise.com/logout-brokerage.asp', 'PartnerID=progressive'],
            referrers: [],
            userAgents: [],
            browsers: [],
            cookies: [],
            variables: []
        },
        include: {
            local: ['.']
        },
        
        delay: 0,
        timeout: 0,
        
        hideOnClick: false,
        
        hideCloseButton: false,
        
        css: 'foresee-dhtml.css'/*tpa=https://www.ameriprise.com/global/scripts/forsee/2009/foresee-dhtml.css*/,
        
        hide: [],
        
        hideFlash: false,
        
        type: 'dhtml',
        /* desktop */
        // url: 'https://www.ameriprise.com/global/scripts/forsee/2009/invite.html'
        /* mobile */
        url: 'https://www.ameriprise.com/global/scripts/forsee/2009/invite-mobile.html',
        back: 'url'
    
        //SurveyMutex: 'SurveyMutex'
    },
    
    tracker: {
        width: '690',
        height: '415',
        timeout: 3,
        adjust: true,
        alert: {
            enabled: true,
            message: 'The survey is now available.'
        },
        url: 'https://www.ameriprise.com/global/scripts/forsee/2009/tracker.html'
    },
    
    survey: {
        width: 690,
        height: 600
    },
    
    qualifier: {
        footer: '<div div id=\"fsrcontainer\"><div style=\"float:left;width:80%;font-size:8pt;text-align:left;line-height:12px;\">This survey is conducted by an independent company ForeSee,<br>on behalf of the site you are visiting.</div><div style=\"float:right;font-size:8pt;\"><a target="_blank" title="Validate TRUSTe privacy certification" href="https://privacy-policy.truste.com/click-with-confidence/ctv/en/www.foreseeresults.com/seal_m"><img border=\"0\" src=\"{%baseHref%}truste.png\" alt=\"Validate TRUSTe Privacy Certification\"></a></div></div>',
        width: '690',
        height: '500',
        bgcolor: '#333',
        opacity: 0.7,
        x: 'center',
        y: 'center',
        delay: 0,
        buttons: {
            accept: 'Continue'
        },
        hideOnClick: false,
        css: 'foresee-dhtml.css'/*tpa=https://www.ameriprise.com/global/scripts/forsee/2009/foresee-dhtml.css*/,
        url: 'https://www.ameriprise.com/global/scripts/forsee/2009/qualifying.html'
    },
    
    cancel: {
        url: 'https://www.ameriprise.com/global/scripts/forsee/2009/cancel.html',
        width: '690',
        height: '400'
    },
    
    pop: {
        what: 'survey',
        after: 'leaving-site',
        pu: false,
        tracker: true
    },
    
    meta: {
        referrer: true,
        terms: true,
        ref_url: true,
        url: true,
        url_params: false,
        user_agent: false,
        entry: false,
        entry_params: false
    },
    
    events: {
        enabled: true,
        id: true,
        codes: {
            purchase: 800,
            items: 801,
            dollars: 802,
            followup: 803,
            information: 804,
            content: 805
        },
        pd: 7,
        custom: {}
    },
    
    previous: false,
    
    analytics: {
        google_local: false,
        google_remote: false
    },
    
    cpps: {
        PartnerID: {
            source: 'parameter',
            name: 'PartnerID'
        },
        TLSessionID: {
            source: 'cookie',
            name: 'TLTSID'
        }
    },
    
    mode: 'hybrid'
};
