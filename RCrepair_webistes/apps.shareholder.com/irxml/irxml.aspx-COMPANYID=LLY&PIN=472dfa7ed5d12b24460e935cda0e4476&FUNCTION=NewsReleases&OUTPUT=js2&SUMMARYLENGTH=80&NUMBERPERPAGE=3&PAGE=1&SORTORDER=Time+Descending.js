var irxmlnewsreleases = new Array();
irxmlnewsreleases[0]={
  "attachmentfileid":834287,
  "attachmentfilekey":"38df0322-680d-488b-bcff-fec221d0f102",
  "attachmentfilename":"http://apps.shareholder.com/irxml/LLY_News_2015_6_8_Product.pdf",
  "attachmentfilesize":106040,
  "id":1,
  "linktoreleasehtml":"http://apps.shareholder.com/irxml/irxml.aspx?CompanyID=LLY&Format=HTML&Function=NewsReleaseDetail&Output=js2&ReleaseID=916888&PIN=2108357d840f473dbbf0f5f4443c63f4",
  "linktoreleasetxt":"http://apps.shareholder.com/irxml/irxml.aspx?CompanyID=LLY&Format=TEXT&Function=NewsReleaseDetail&Output=js2&ReleaseID=916888&PIN=8019f4895b7c34734220b2d035b3a4f4",
  "releasedate":new Date(2015, 6-1, 8, 09, 15, 0),
  "releaseid":916888,
  "releasetype":"Product",
  "subtitle":"Data presented at 75th American Diabetes Association Scientific Sessions&#174; bolster existing information on Trulicity's safety and efficacy",
  "summary":"INDIANAPOLIS, June 8, 2015 /PRNewswire/ -- Trulicity&#8482; 1.5 mg and 0.75 mg p",
  "title":"New Data Show Superiority of Lilly's Once-Weekly Trulicity&#8482; (dulaglutide) to Lantus&#174; (insulin glargine) in Patients with Type 2 Diabetes",
  "totalrecords":1369,
  "year":"2015"
};
irxmlnewsreleases[1]={
  "attachmentfileid":834186,
  "attachmentfilekey":"d836aaf4-7506-4648-9438-6748d5b6c26c",
  "attachmentfilename":"http://apps.shareholder.com/irxml/LLY_News_2015_6_6_Product.pdf",
  "attachmentfilesize":153530,
  "id":2,
  "linktoreleasehtml":"http://apps.shareholder.com/irxml/irxml.aspx?CompanyID=LLY&Format=HTML&Function=NewsReleaseDetail&Output=js2&ReleaseID=916759&PIN=0cccbc32d316db5e33acb96093c29923",
  "linktoreleasetxt":"http://apps.shareholder.com/irxml/irxml.aspx?CompanyID=LLY&Format=TEXT&Function=NewsReleaseDetail&Output=js2&ReleaseID=916759&PIN=c36833ae7ecdb934608bff7abd1c91c1",
  "releasedate":new Date(2015, 6-1, 6, 14, 15, 0),
  "releaseid":916759,
  "releasetype":"Product",
  "subtitle":"Phase III Data Presented at the 2015 American Diabetes Association Scientific Sessions&#174;",
  "summary":"INDIANAPOLIS, June 6, 2015 /PRNewswire/ -- In newly released Phase III trial dat",
  "title":"Basal Insulin Peglispro Studies Demonstrate Superiority to Insulin Glargine Across Multiple Measures in People with Type 1 Diabetes",
  "totalrecords":1369,
  "year":"2015"
};
irxmlnewsreleases[2]={
  "attachmentfileid":834185,
  "attachmentfilekey":"775ca5b7-fefa-4472-b0db-28e0069b525c",
  "attachmentfilename":"http://apps.shareholder.com/irxml/LLY_News_2015_6_6_Product.pdf",
  "attachmentfilesize":155461,
  "id":3,
  "linktoreleasehtml":"http://apps.shareholder.com/irxml/irxml.aspx?CompanyID=LLY&Format=HTML&Function=NewsReleaseDetail&Output=js2&ReleaseID=916758&PIN=d3f58eb971ec19c7d367cefde183f090",
  "linktoreleasetxt":"http://apps.shareholder.com/irxml/irxml.aspx?CompanyID=LLY&Format=TEXT&Function=NewsReleaseDetail&Output=js2&ReleaseID=916758&PIN=5b0bc21148ae0118a193a7f7ded0b6d5",
  "releasedate":new Date(2015, 6-1, 6, 13, 45, 0),
  "releaseid":916758,
  "releasetype":"Product",
  "subtitle":"Data from Phase III Clinical Trial Program Presented at the 2015 American Diabetes Association Scientific Sessions&#174;",
  "summary":"INDIANAPOLIS, June 6, 2015 /PRNewswire/ -- Eli Lilly and Company's (NYSE: LLY) b",
  "title":"Lilly's Basal Insulin Peglispro Shows Superiority to Insulin Glargine in Reducing HbA1c in People with Type 2 Diabetes",
  "totalrecords":1369,
  "year":"2015"
};
var IRXMLSTATUS="No Error",IRXMLSTATUSCODE="0";