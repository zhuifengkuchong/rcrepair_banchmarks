var irxmlnewsreleases = new Array();
irxmlnewsreleases[0]={
  "attachmentfileid":829925,
  "attachmentfilekey":"2032f8ce-3515-41f3-ad7f-aeb9c21f1b36",
  "attachmentfilename":"http://apps.shareholder.com/irxml/NGLS_News_2015_5_15_Targa_Resources_Partners_LP.pdf",
  "attachmentfilesize":11855,
  "id":1,
  "releasedate":new Date(2015, 5-1, 15, 00, 0, 0),
  "releaseid":913484,
  "releasetype":"Targa Resources Partners LP",
  "subtitle":"",
  "summary":null,
  "title":"Targa Resources Partners LP to Present at the National Association of Publicly Traded Partnerships 2015 MLP Investor Conference",
  "totalrecords":301,
  "year":"2015"
};
irxmlnewsreleases[1]={
  "attachmentfileid":828252,
  "attachmentfilekey":"d5b6420d-c39a-406d-8793-974f9dcb9354",
  "attachmentfilename":"http://apps.shareholder.com/irxml/NGLS_News_2015_5_11_Targa_Resources_Partners_LP.pdf",
  "attachmentfilesize":11095,
  "id":2,
  "releasedate":new Date(2015, 5-1, 11, 00, 0, 0),
  "releaseid":912159,
  "releasetype":"Targa Resources Partners LP",
  "subtitle":"",
  "summary":null,
  "title":"Targa Resources Partners LP Announces Final Results of Exchange Offer",
  "totalrecords":301,
  "year":"2015"
};
irxmlnewsreleases[2]={
  "attachmentfileid":826308,
  "attachmentfilekey":"9d45c35f-e71b-4978-a739-5d21bca86ef8",
  "attachmentfilename":"http://apps.shareholder.com/irxml/NGLS_News_2015_5_5_Targa_Resources_Partners_LP.pdf",
  "attachmentfilesize":133000,
  "id":3,
  "releasedate":new Date(2015, 5-1, 5, 00, 0, 0),
  "releaseid":910751,
  "releasetype":"Targa Resources Partners LP",
  "subtitle":"",
  "summary":null,
  "title":"Targa Resources Partners LP and Targa Resources Corp. Report First Quarter 2015 Financial Results",
  "totalrecords":301,
  "year":"2015"
};
irxmlnewsreleases[3]={
  "attachmentfileid":823797,
  "attachmentfilekey":"8d722b64-c2d4-48e8-926c-7843b10efa67",
  "attachmentfilename":"http://apps.shareholder.com/irxml/NGLS_News_2015_4_27_Targa_Resources_Partners_LP.pdf",
  "attachmentfilesize":13865,
  "id":4,
  "releasedate":new Date(2015, 4-1, 27, 00, 0, 0),
  "releaseid":908646,
  "releasetype":"Targa Resources Partners LP",
  "subtitle":"",
  "summary":null,
  "title":"Targa Resources Partners LP Announces Interim Results of Exchange Offer and Consent Solicitation",
  "totalrecords":301,
  "year":"2015"
};
irxmlnewsreleases[4]={
  "attachmentfileid":822634,
  "attachmentfilekey":"47797b76-00a4-4f9d-9a2e-3f8f287d1ebf",
  "attachmentfilename":"http://apps.shareholder.com/irxml/NGLS_News_2015_4_21_Targa_Resources_Corp..pdf",
  "attachmentfilesize":15968,
  "id":5,
  "releasedate":new Date(2015, 4-1, 21, 00, 0, 0),
  "releaseid":907815,
  "releasetype":"Targa Resources Partners LP",
  "subtitle":"",
  "summary":null,
  "title":"Targa Resources Announces Increase in Quarterly Dividend and Distribution, Provides Revised 2015 Outlook, Publishes Updated Investor Presentation and Announces Timing of First Quarter 2015 Earnings Releases and Conference Call",
  "totalrecords":301,
  "year":"2015"
};
var IRXMLSTATUS="No Error",IRXMLSTATUSCODE="0";