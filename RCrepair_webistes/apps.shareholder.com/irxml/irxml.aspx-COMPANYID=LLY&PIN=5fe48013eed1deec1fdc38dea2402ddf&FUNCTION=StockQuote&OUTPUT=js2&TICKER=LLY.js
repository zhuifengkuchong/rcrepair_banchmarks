var irxmlstockquote = new Array();
irxmlstockquote[0]={
  "ask":78.700,
  "bid":78.690,
  "change":0.570,
  "companyname":"Eli Lilly and Company",
  "datetime":new Date(2015, 6-1, 8, 12, 28, 47),
  "dividend":0.500,
  "eps":2.050,
  "exchange":"NYSE",
  "high":78.720,
  "lastdatetime":new Date(2015, 6-1, 8, 12, 28, 47),
  "lastprice":78.690,
  "longname":"Eli Lilly and Company",
  "low":77.830,
  "open":77.980,
  "pchange":0.730,
  "pe":35.000,
  "previousclose":78.120,
  "shares":1111005,
  "shortname":"LLY",
  "ticker":"LLY",
  "trades":12516,
  "volume":1693700,
  "yearhigh":79.850,
  "yearlow":58.500,
  "yield":2.560
};
var IRXMLSTATUS="No Error",IRXMLSTATUSCODE="0";