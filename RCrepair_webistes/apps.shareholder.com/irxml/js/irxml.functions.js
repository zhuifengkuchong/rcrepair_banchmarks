var irxmlfunctions = {
  /** Constants **/
  monthNames: new Array(
    "January","February","March","April","May","June",
    "July","August","September","October","November","December"
  ),
  shortMonthNames: new Array(
    "Jan","Feb","Mar","Apr","May","Jun",
    "Jul","Aug","Sep","Oct","Nov","Dec"
  ),
  dayNames: new Array(
    "Sunday","Monday","Tuesday","Wednesday",
    "Thursday","Friday","Saturday"
  ),
  shortDayNames: new Array(
    "Sun","Mon","Tue","Wed",
    "Thu","Fri","Sat"
  ),
  
  fileSizeSuffix: new Array(
    "B", "KB", "MB", "GB", "TB"
  ),
  /** /Constants **/

  // leading zeros
  padZeros: function( val, zeros, placement ){ 
    if( typeof placement == 'undefined' ) placement='left';
    if( ('' + val).length < zeros ) {
      if( placement == 'left' ) {
        return irxmlfunctions.padZeros('0' + val, zeros, placement); 
      } else {
        return irxmlfunctions.padZeros(val + '0', zeros, placement); 
      }
    }
    return val;
  },
  formatDate: function( dtObj, formatString ){
    if( typeof formatString == 'undefined' ) formatString = 'yyyy-MM-dd h:mm:ss tt';

    var pz = irxmlfunctions.padZeros;
    var dtValues = {
       "y"    : dtObj.getFullYear() % 100
      ,"yyyy" : dtObj.getFullYear()
      ,"M"    : dtObj.getMonth() + 1
      ,"MMM"  : irxmlfunctions.shortMonthNames[dtObj.getMonth()]
      ,"MMMM" : irxmlfunctions.monthNames[dtObj.getMonth()]
      ,"d"    : dtObj.getDate()
      ,"ddd"  : irxmlfunctions.shortDayNames[dtObj.getDay()]
      ,"dddd" : irxmlfunctions.dayNames[dtObj.getDay()]
      ,"h"    : (dtObj.getHours() % 12 == 0 ? 12 : dtObj.getHours() % 12)
      ,"H"    : dtObj.getHours()
      ,"m"    : dtObj.getMinutes()
      ,"s"    : dtObj.getSeconds()
      ,"t"    : (dtObj.getHours() >= 12 ? 'p' : 'a')
    };
    var value = {
       //date
       "y"     : dtValues.y                     // 1-2 digits
      ,"yy"    : pz(dtValues.y,2)               // 2 digits
      ,"yyyy"  : dtValues.yyyy                  // 4 digits
      ,"M"     : dtValues.M                     // 1-2 digits
      ,"MM"    : pz(dtValues.M,2)               // 2 digits
      ,"MMM"   : dtValues.MMM                   // Short month name
      ,"MMMM"  : dtValues.MMMM                  // Full month name
      ,"d"     : dtValues.d                     // 1-2 digits
      ,"dd"    : pz(dtValues.d,2)               // 2 digits
      ,"ddd"   : dtValues.ddd                   // Short day name
      ,"dddd"  : dtValues.dddd                  // Full day name

       // time
      ,"h"     : dtValues.h                     // 1-2 digits
      ,"hh"    : pz(dtValues.h,2)               // 2 digits
      ,"H"     : dtValues.H                     // 1-2 digits (24-hour)
      ,"HH"    : pz(dtValues.H,2)               // 2 digits (24-hour)
      ,"m"     : dtValues.m                     // 1-2 digits
      ,"mm"    : pz(dtValues.m,2)               // 2 digits
      ,"s"     : dtValues.s                     // 1-2 digits
      ,"ss"    : pz(dtValues.s,2)               // 2 digits
      ,"t"     : dtValues.t                     // (a/p)
      ,"tt"    : dtValues.t + 'm'               // (am/pm)
      ,"T"     : dtValues.t.toUpperCase()       // (A/P)
      ,"TT"    : dtValues.t.toUpperCase() + 'M' // (AM/PM)
    };

    var result = '', iCnt = 0;
    while (iCnt < formatString.length) {
      ch = formatString.charAt(iCnt);
      token = "";
      bIgnore = false;
      if( ch == '\\' ){
        token = formatString.charAt(++iCnt);
        iCnt++;
        bIgnore = true;
      } else {
        while ((formatString.charAt(iCnt) == ch) && (iCnt < formatString.length)) {
          token += formatString.charAt(iCnt++);
        }
      }
      if (!bIgnore && value[token] != null) {
        result += value[token];
      } else { 
        result += token;
      }
    }
    return result;
  },
  // this will color the value based on positive/negative/zero
  currencyFormatColored: function( upcolor, zerocolor, downcolor, val, decimalplaces, commachar, decimalchar, dollarchar ){
    var retVal = irxmlfunctions.currencyFormat( val, decimalplaces, commachar, decimalchar, dollarchar );
    if( val > 0 ){
      if( upcolor.length ){
        retVal = "<span style=\"color:" + upcolor + "\">" + retVal + "</span>";
      }
    } else if( val < 0 ){
      if( downcolor.length ){
        retVal = "<span style=\"color:" + downcolor + "\">" + retVal + "</span>";
      }
    } else {
      if( zerocolor.length ){
        retVal = "<span style=\"color:" + zerocolor + "\">" + retVal + "</span>";
      }
    }
    return retVal;
  },
  // this will add the dollar sign + decimal place + commas
  currencyFormat: function( val, decimalplaces, commachar, decimalchar, dollarchar ){
    var pz = irxmlfunctions.padZeros;
    if( typeof decimalplaces == 'undefined' ) decimalplaces = 2;
    if( typeof commachar     == 'undefined' ) commachar     = ',';
    if( typeof decimalchar   == 'undefined' ) decimalchar   = '.';
    if( typeof dollarchar    == 'undefined' ) dollarchar    = '$';

    if( isNaN( val ) ) return val;

    var result = dollarchar, decimalresult = '', num;
    val = parseInt(val * Math.pow(10, decimalplaces)) / Math.pow(10, decimalplaces);
    num = ('' + val).split( decimalchar );

    // add commas
    val = irxmlfunctions.numberFormat(num[0]);

    // get decimalplaces
    if( num.length > 1 ) decimalresult = num[1];
    // make sure it has zeros up to the "decimalplaces" spot
    decimalresult = pz(decimalresult, decimalplaces, 'right');

    return result + val + ( decimalplaces > 0 ? decimalchar + decimalresult : '' );
  },
  // just adds commas to the number
  numberFormat: function( val, commachar ){
    if( typeof commachar == 'undefined' ) commachar = ',';

    var rx = /(\d+)(\d{3})/,
        result = '' + val;
    while (rx.test(result)) {
      result = result.replace(rx, '$1' + commachar + '$2');
    }
    return result;
  },
  formatFileSize: function(size,startIndex){
    if( typeof startIndex == 'undefined' ) startIndex = 0;
    var sizeLen = irxmlfunctions.fileSizeSuffix.length;

    for(;startIndex<sizeLen && size > 900;startIndex++){
      size /= 1024;
    }

    return irxmlfunctions.currencyFormat(size, 1, ',', '.', '') 
        + ' ' + irxmlfunctions.fileSizeSuffix[startIndex];
  }
};