var com = com || {
    chevron: {}
};


com.chevron.versaTag = {
    versaTagTrack: function (id) {
        if (typeof versaTagObj !== 'undefined') {
            var loc = window.location.protocol + '//' + window.location.hostname + "/"; //this should include http or https
            versaTagObj.generateRequest(loc + id);
        }
    }
};


(function ($, w, vt) {

    $(function () {


        //***********************Input Tracking***********************

        $("input[data-vt-id]").each(function() {
            var $this = $(this),
        		url = $this.attr("data-vt-id");

            if (url) {
                $this.click(function () {
                    vt.versaTagTrack(url);
                });
            } 
        });

        $("input[data-ga-vpv]").each(function () {
            var $this = $(this);
            
            //make sure not to fire the same tracking code twice if both GA and VersaTag attributes are defined for the same element
            if(!$this.attr("data-vt-id")) {
                var url = $this.attr("data-ga-vpv");
                if (url) {
                    $this.click(function () {
                        vt.versaTagTrack(url);
                    });
                }
            }
        });


        //****************image map tracking******************

        $("area[data-vt-id]").each(function() {
            var $this = $(this),
        		url = $this.attr("data-vt-id");

            if (url) {
                $this.click(function () {
                    vt.versaTagTrack(url);
                });
            } 
        });

        //***********************Anchor Tracking***********************

        $("a[data-vt-id]").each(function () {
            var $this = $(this),
        		url = $this.attr("data-vt-id");

            if (url) {
                //click is not working for some reason
                $this.mousedown(function () {
                    vt.versaTagTrack(url);
                });
                $this.bind('touchstart', function() {
                    vt.versaTagTrack(url);
                });
            }
        });

        //***********************Div Tracking***********************
        $("div[data-vt-id]").click(function() {
            var $this = $(this),
        		url = $this.attr("data-vt-id");

            if (url) {
                vt.versaTagTrack(url);
            } 
        });



        //add in elements that have already been tagged for GA
        //note: decided to query the doc for these GA attributes separate from the VersaTag-specific attributes to achieve decoupling
        $("a[data-ga-vpv]").each(function () {
            var $this = $(this);
            
            //make sure not to fire the same tracking code twice if both GA and VersaTag attributes are defined for the same element
            if(!$this.attr("data-vt-id")) {
                var url = $this.attr("data-ga-vpv");
                if (url) {
                    $this.click(function () {
                        vt.versaTagTrack(url);
                    });
                }
            }

        });

        $("a[data-vt-hover-id]").each(function () {
            var $this = $(this),
        		url = $this.attr("data-vt-hover-id");

            if (url) {
                $this.mouseover(function () {
                    vt.versaTagTrack(url);
                });
            }
        });


    //***********************HTML5 Video Player Tracking***********************


    // HTML5 video player events for Google Analytics tracking
    var getVideoType = function (url) {
        var ext = 'unknown_html5';
        if (url != null) {
            var extMatch = /\.([a-z\d]+)(\?.*)?$/.exec(url.toLowerCase());
            if (extMatch != null) {
                ext = extMatch[1];
            }
        }
        return ext;
    };

    var trackVideoEvent = function (videoEvent, actionLabel) {

        //todo: need to get final spec for this
        var trackingId = $(videoEvent.target).attr('x-data-trackingid');
        var videoType = getVideoType(videoEvent.target.currentSrc);

        var tagValue = trackingId + "_" + actionLabel + "_" + videoType;

        vt.versaTagTrack(tagValue);

        //pageTracker._trackEvent("VideoPlayer", $(videoEvent.target).attr('x-data-trackingid'), actionLabel + "_" + gaGetVideoType(videoEvent.target.currentSrc));
    };

    $('video').each(function () {
        // 30seconds_reached
        $(this).bind('timeupdate', function (evt) {
            if (Math.floor(evt.target.currentTime.toFixed(3)) >= 30) {
                if ($(this).attr('x-data-past30seconds') === 'false') {
                    trackVideoEvent(evt, "30seconds_reached");
                    $(this).attr('x-data-past30seconds', 'true'); // Prevent duplicate events past the first 30 seconds
                }
            }
        }, false);

        // pausebutton_click
        $(this).bind('pause', function (evt) {
            // Log and track whether the 'Pause' button has been triggered.
            if (evt.target.currentTime != evt.target.duration) { // Prevent false pause event when video reaches end
                trackVideoEvent(evt, "pausebutton_click");
            }
        }, false);

        // playbutton_click
        $(this).bind('play', function (evt) {
            //pageTracker._trackPageview('/vpv/videoid/' + $(this).attr('x-data-trackingid'));
            trackVideoEvent(evt, "playbutton_click");
        }, false);

        // video_complete
        $(this).bind('ended', function (evt) {
            trackVideoEvent(evt, "video_complete");
        }, false);

    });

  }); 


} (jQuery, window, com.chevron.versaTag));
