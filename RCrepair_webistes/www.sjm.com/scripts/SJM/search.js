var txt_search = '';
var ResumewareSearchLink = "http://careers.sjm.com/sjm_rw/sjm_web/search.cfm?view=list&clearit=1";
var ResumewareSearchLinkAdvanced = "http://careers.sjm.com/sjm_rw/sjm_web/search.cfm";

// Using a relative link like /corporate/search.aspx was breaking the third party sites like Resumeware and TR
var SJMSearchLink = "http://www.sjm.com/corporate/search.aspx?c=sjmcorporate";

var SJMSearchLinkSjm = "http://www.sjm.com/corporate/search.aspx?c=sjmcorporate";

var $jq = jQuery.noConflict();
$jq(function () {
    // Search

    // Resumeware search
    $jq('a#ResumewareSearch').livequery(
		'click',
		function () {
		    //&job_description=";
		    var theKeywords = $jq('#TbResumewareSearch').val();

		    if (theKeywords == txt_search) {
		        theKeywords = "";
		    }

		    if (theKeywords.length > 0) {
		        var theURL = ResumewareSearchLink + "&job_description=" + theKeywords;

		        window.location = theURL;
		    }
		    else {
		        var theURL = ResumewareSearchLink;

		        window.location = theURL;
		    }
		}
    );

    $jq('a#ResumewareSearchAdvanced').livequery(
		'click',
		function () {
		    window.location = ResumewareSearchLinkAdvanced;
		}
    );

    // Enter Keywords in the text box
    $jq('input.search-txt').val(txt_search);
    $jq('input.search-txt').bind(
		    'focus',
		    function (e) {
		        if ($jq(this).val() == txt_search) {
		            $jq(this).val('');
		        }
		    }
	 );

    $jq('input.search-txt').bind(
		    'blur',
		    function (e) {
		        if ($jq(this).val() == '') {
		            $jq(this).val(txt_search);
		        }
		    }
	);

    $jq('input#TbSearch').val(txt_search);

    $jq('input.TbSearch').bind(
        'focus',
        function (e) {
            //alert('focus');
            if ($jq(this).val() == txt_search) {
                //alert('focus keys');
                $jq(this).val('');
            }
        }
    );

    $jq('input#TbSearch').bind(
        'blur',
        function (e) {
            //alert('blur');
            if ($jq(this).val() == '') {
                //alert('blur blank');
                $jq(this).val(txt_search);
            }
        }
    );

    // SJM Search on Third Party Sites like Thomson Reuters and Resumeware
    $jq('a#LbSearch').livequery(
	    'click',
	    function () {

	        var theKeywords = $jq('#TbSearch').val();
	        if (theKeywords.length > 0) {
	            var theURL = SJMSearchLink + "&q=" + theKeywords;

	            //alert('LbSearch #' + theURL + '#');

	            window.location = theURL;
	        }
	    }
    );


    /* 'change'*/
    $jq('#TbSearch').bind(
        'keypress',
        function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) { //Enter keycode 
                //alert('keypress');
                $jq("a#LbSearch").trigger('click');
                return false;
            }
        }
    );

    //Search on Sjm.com site
    $jq('input#TbSearchSjm').val(txt_search);

    $jq('input.TbSearchSjm').bind(
        'focus',
        function (e) {
            //alert('focus');
            if ($jq(this).val() == txt_search) {
                //alert('focus keys');
                $jq(this).val('');
            }
        }
    );

    $jq('input#TbSearchSjm').bind(
        'blur',
        function (e) {
            //alert('blur');
            if ($jq(this).val() == '') {
                //alert('blur blank');
                $jq(this).val(txt_search);
            }
        }
    );

    $jq('a#LbSearchSjm').livequery(
	    'click',
	    function () {

	        var theKeywords = $jq('#TbSearchSjm').val();
	        if (theKeywords.length > 0) {
	            var theURL = SJMSearchLinkSjm + "&q=" + theKeywords;

	            //alert('LbSearch #' + theURL + '#');

	            window.location = theURL;
	        }
	    }
    );


    /* 'change'*/
    $jq('#TbSearchSjm').bind(
        'keypress',
        function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) { //Enter keycode 
                //alert('keypress');
                $jq("a#LbSearchSjm").trigger('click');
                return false;
            }
        }
    );

});