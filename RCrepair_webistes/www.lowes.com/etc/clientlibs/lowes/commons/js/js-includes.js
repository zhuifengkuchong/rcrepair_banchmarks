if (configuration.akamaiEnabled) {
    var src = configuration.minifyEnabled ? configuration.cqJavaScriptMinifiedUrl : configuration.cqJavaScriptUrl;

    document.write('<script type="text/javascript" src="' + src + '"></script>');

    google.load('maps', '3', { other_params: 'sensor=false' });
}