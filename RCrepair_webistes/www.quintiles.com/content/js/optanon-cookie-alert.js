// Optanon Cookie Alert
// ============================================================

+function ($) {
    'use strict';

    // Check that Optanon in defined
    if (typeof Optanon !== "undefined" && !Optanon.IsAlertBoxClosed()) {
        var $alertBox = $('.optanon-alert-box-wrapper');

        $alertBox.find('.optanon-alert-box-close').click(function (e) {
            Optanon.SetAlertBoxClosed(true); // Implied consent mode
            $alertBox.fadeOut(400); // Default optanon behavior
        });

        // Show the alert box
        $alertBox.show();
    }

}(jQuery);