(function ($j, window, document) {
	//
	"use strict";
	
	// do not run in older versions of IE
	if ( $j.browser.msie ) {
			if( $j.browser.version < 9) {
				  return ;
			  }
	}
	
	//
	//
	//

	//
	//
	//
	var mobile_menu_icon,
			buildCatalogHelpMenu,
			search_icon,
			mobile_search_bar,
			mobile_menu_nav,
			util_language,
			util_location, 
			util_custsuppt,
			util_signin,
			util_signout,
			util_welcome,
			anonymous_user,
			util_signin_txt,
			search_txt_input,
			footer_top_mrkp,
			footer_top_trgt,
			//
			toggleMobileNavigation,
			toggleMobileSearch,
			reassignNavStyling,
			rebuildMobileNav,
			appendCartIconBadge,
			appendFooterLinks,
			buildMobileHomepageTabs,
			buildTopLevelLandingPage,
			buildSocialSharingControls,
			buildLocationLandingPage,
			buildLocationsBrowsePage,
			buildLocationsDetailPage,
			assignFooterPhoneInterface,
			vAlignMarqueeText,
			isResponsiveStylesLoaded,
			hideAddressBar,
			buildCampaignRegistrationPage,
			buildTrainingFinderPage,
			buildTrainingDetailPage,
			buildTrainingThankYouPage,
			buildAndRelocateSideNavigation,
			showMediaContact,
			buidlPressDetail,
			buildFaqsPage, current_faq_node,
			buildFaqsLandingPage,
			buildEventDetail,
			//
			sideBarInterfaceRefactor,
			pullEmptyList,
			redrawBioPage,
			//
			getHelpBox,
			bindHelpBox,
			bindSocialMedia,
			//
			sidebarSpecialityForm,
			//
			campaignRegistrationPage;
	//
	//
	toggleMobileNavigation = function () {
		//
		//console.log(Modernizr.touch);
		//
		if (mobile_menu_nav.height() === 0) {
			mobile_menu_nav.css({'height': '100%', 'display': 'block'});
			$j($j('#resp-toolbar li a')[0]).addClass('respToolbarPermBG');
		} else {
			mobile_menu_nav.css({'height': '0px', 'display': 'none'});
			$j($j('#resp-toolbar li a')[0]).removeClass('respToolbarPermBG');
		}
	};
	//
	toggleMobileSearch = function () {
		if (mobile_search_bar.css('display') === 'none') {
			mobile_search_bar.css({'display': 'block'});
			search_txt_input.focus();
			$j($j('#resp-toolbar li a')[2]).addClass('respToolbarPermBG');
		} else {
			mobile_search_bar.css({'display': 'none'});
			search_txt_input.blur();
			$j($j('#resp-toolbar li a')[2]).removeClass('respToolbarPermBG');
		}
	};
	//
	reassignNavStyling = function () {

		util_language.addClass('utilMobileNavItem');
		util_language.bind('click', function(evt){
			var current_url_path_str, curent_url_path_arr, new_location,
			setLinkText, switchLanguages;
			switchLanguages = function (lang) {
				var new_loc_str = lang;
				for (var i = 2; i < curent_url_path_arr.length; i++){
					new_loc_str += '/' + curent_url_path_arr[i];
				}
				window.location = new_loc_str;
			};
			evt.preventDefault();
			current_url_path_str = window.location.pathname;
			curent_url_path_arr = current_url_path_str.split( '/' );
			if (current_url_path_str === '/') {
				window.location = 'http://www.unitedrentals.com/fr/';
			}
			if(curent_url_path_arr.length === 3 && curent_url_path_arr[1] === 'fr' && curent_url_path_arr[2] === ''){
				window.location = '../../index.htm'/*tpa=http://www.unitedrentals.com/*/;
			}
			if (curent_url_path_arr[1] === 'en' &&  curent_url_path_arr[2] != '') {
				switchLanguages('/fr');
			}
			if (curent_url_path_arr[1] === 'fr' &&  curent_url_path_arr[2] != '') {
				switchLanguages('/en');
			}
		});

		util_location.addClass('utilMobileNavItem');
		util_custsuppt.addClass('utilMobileNavItem');
		util_signin.addClass('utilMobileNavItem');
		util_signout.addClass('utilMobileNavItem');
		util_welcome.addClass('utilMobileNavItem');
		//
		rebuildMobileNav();
	};
	//
	rebuildMobileNav = function () {
		if (!anonymous_user){
			mobile_menu_nav.prepend(util_signout);
			mobile_menu_nav.prepend(util_welcome);
		} else {
			mobile_menu_nav.prepend(util_signin);
		}
		mobile_menu_nav.prepend(util_custsuppt);
		mobile_menu_nav.prepend(util_location);
		mobile_menu_nav.prepend(util_language);
	};
	//
	appendCartIconBadge = function (markup) {
		var target_markup;
		//
		target_markup = $j('#resp-toolbar li a')[1];
		//
		markup.addClass('cartBadge');
		$j(target_markup).append(markup);
	};
	//
	appendFooterLinks = function () {
		var footer_tgt, terms_obj, privacy_obj, terms_cpy, privacy_cpy, terms_lnk, privacy_lnk;
		//
		if($j('#footer #footer-content .container .footer-menu').length < 3){
			$j('#footer #footer-content .container').addClass('campaignRegistrationRogueFooterLinks');
			//
			if ($j('body').hasClass('get-what-you-need-skid-steer-01')){
				$j('#footer #footer-content .container').addClass('forcePullRogueFooter');
				return;
			}
			//
		}
		//
		terms_cpy = $j($j('#legal .container a')[0]).html();
		privacy_cpy = $j($j('#legal .container a')[1]).html();
		terms_lnk = $j($j('#legal .container a')[0]).attr('href');
		privacy_lnk = $j($j('#legal .container a')[1]).attr('href');
		footer_tgt = $j('#footer #footer-content .container');
		terms_obj = document.createElement('div');
		privacy_obj = document.createElement('div');
		terms_obj.setAttribute('class', 'footer-menu');
		privacy_obj.setAttribute('class', 'footer-menu');
		//
		terms_obj.appendChild(document.createElement('h5'));
		privacy_obj.appendChild(document.createElement('h5'));
		//
		terms_obj.getElementsByTagName('h5')[0].appendChild(document.createElement('a')).setAttribute('href', terms_lnk);
		privacy_obj.getElementsByTagName('h5')[0].appendChild(document.createElement('a')).setAttribute('href', privacy_lnk);
		//
		terms_obj.getElementsByTagName('a')[0].appendChild(document.createTextNode(terms_cpy));
		privacy_obj.getElementsByTagName('a')[0].appendChild(document.createTextNode(privacy_cpy));
		//
		//
		$j(terms_obj).addClass('legalMobileFooterLink');
		$j(privacy_obj).addClass('legalMobileFooterLink');
		//
		footer_tgt.append(terms_obj);
		footer_tgt.append(privacy_obj);
		//
		//
	};
	//
	//
  buildAndRelocateSideNavigation = function(nav_obj, target_obj) {
  	// console.log('build and relocate side navigation');
  	var new_obj = nav_obj.clone();
  	new_obj.addClass('mobileSubnav');
  	target_obj.append(new_obj);
  };
  //
  //
  getHelpBox = function (mrkup, wrapped) {
		// console.log('getHelpBox');
		//
		var hlp_container, hlp_title_obj, hlp_title_content, hlp_text_obj;
		//
		hlp_container = document.createElement('div');
		hlp_container.setAttribute('class', 'mobileLandingHelpContainer');

		//
		hlp_title_obj = document.createElement('div');
		hlp_title_obj.setAttribute('class', 'mobileLandingHelpContainerTitle');

		if(wrapped !== "nowrap") {
			mrkup = mrkup.children('.can-we-help');
		}
		
		// menu title can be identified either as a link or the first span
		hlp_title_content = mrkup.children('h3').children('a');
		if (hlp_title_content.length === 0) {
			hlp_title_content = document.createElement('a');
			hlp_title_content.setAttribute('href','#');
			if(arguments[2] === 'campaign'){
				$j(hlp_title_content).append($j('#campaign-registration-actual-help .can-we-help h3 > span:first-child').text());
			} else {
				$j(hlp_title_content).append($j('.can-we-help h3 > span:first-child').text());				
			}
		}
		$j(hlp_title_obj).append(hlp_title_content);
		//
		// help menu may have optional text
		hlp_text_obj = mrkup.children('.body-text');
		// console.log(hlp_text_obj);
		//
		hlp_container.appendChild(hlp_title_obj);

		if (hlp_text_obj.length > 0 ) {
				$j(hlp_container).append(hlp_text_obj);
		}

		$j(hlp_container).append(mrkup.children('ul'));
		//

		return hlp_container;
	};
  //
  //
  bindHelpBox = function() {

		var help_aff;
		//
		help_aff = $j('.mobileLandingHelpContainerTitle');
		//
		help_aff.bind('click', function (evt) {
			evt.preventDefault();
			//
			var hlpbox,
				menuItemCount,
				menuItemHeight, 
				menuHeight,
				bodyTextHeight;

			bodyTextHeight = $j('.mobileLandingHelpContainer .body-text').outerHeight();
			if (bodyTextHeight === undefined){
				bodyTextHeight = 0;
			}

			hlpbox = $j(this).parent(); 
			menuItemCount=$j(".mobileLandingHelpContainer ul li").length;
			menuItemHeight = 34;
			//console.log(bodyTextHeight);
			menuHeight = ( menuItemHeight + 2 ) * (menuItemCount + 1) + bodyTextHeight;
			
			//
			//console.log($j(this).css(':before'));
			if (hlpbox.height() === menuItemHeight) {
				hlpbox.animate({'height': menuHeight +'px'});
				$j(this).removeClass('mobileLandingHelpContainerTitle');
				$j(this).addClass('mobileLandingHelpContainerTitleActive');
			} else {
				hlpbox.animate({'height': menuItemHeight + 'px'});
				$j(this).removeClass('mobileLandingHelpContainerTitleActive');
				$j(this).addClass('mobileLandingHelpContainerTitle');
			}
		});
  };
  //
  bindSocialMedia = function () {
		var tgt = $j('.mobileSocialControlShareAff');
		//
		tgt.bind('click', function () {
			var header = $j('#content-header');
			//
			if (header.height() > 40) {
				header.animate({'overflow': 'hidden', 'height': '40px'});
			} else {
				header.animate({'overflow': 'visible', 'height': '180px'});
			}
			//
		});
	};
	//
	//
	//
	//
	// ################################################## Home Page Page Srt ##################################################
	buildMobileHomepageTabs = function () {
		var orig_markup,
				target_div,
				tab_container,
				tab_box_count,
				tab_box_icons,
				i,
				//
				tabBoxGenerator;
		//
		tabBoxGenerator = function (ttl, cpy, lnk, aff, icn) {
			var box_container, box_link_overlay, box_title, box_icon, box_copy, box_aff;
			//
			box_container = document.createElement('div');
			box_container.setAttribute('class', 'mobileTabBox');
			//
			box_link_overlay = document.createElement('a');
			box_link_overlay.setAttribute('class', 'mobileTabBoxLink');
			box_link_overlay.setAttribute('href', lnk);
			box_link_overlay.setAttribute('target', '_self');
			//
			box_title = document.createElement('p');
			box_title.setAttribute('class', 'mobileTabBoxTitle');
			box_title.appendChild(document.createTextNode(ttl));
			//
			box_copy = document.createElement('p');
			box_copy.setAttribute('class', 'mobileTabBoxCopy');
			box_copy.appendChild(document.createTextNode(cpy));
			//
			box_icon = document.createElement('img');
			box_icon.setAttribute('class', 'mobileTabBoxIcn');
			box_icon.setAttribute('src', icn);
			//
			box_aff = document.createElement('img');
			box_aff.setAttribute('class', 'mobileTabBoxAff');
			box_aff.setAttribute('src', aff);
			box_aff.setAttribute('width', '16');
			box_aff.setAttribute('height', '16');
			//
			box_container.appendChild(box_icon);
			box_container.appendChild(box_title);
			box_container.appendChild(box_copy);
			box_container.appendChild(box_aff);
			box_container.appendChild(box_link_overlay);
			//
			return box_container;
		};
		//
		orig_markup = $j('#main .container.standalone .row.three-cols.has-dividers .box .generic-tout').clone();
		tab_box_count = orig_markup.length;
		tab_container = document.createElement('div');
		tab_container.setAttribute('class', 'mobileHomeTabContainer');
		tab_box_icons = ['Unknown_83_filename'/*tpa=http://www.unitedrentals.com/static/js/mobile-hp-icon-0.gif*/, 'Unknown_83_filename'/*tpa=http://www.unitedrentals.com/static/js/mobile-hp-icon-1.gif*/, 'Unknown_83_filename'/*tpa=http://www.unitedrentals.com/static/js/mobile-hp-icon-2.gif*/];
		//
		for (i = 0; i < tab_box_count; i++){
			tab_container.appendChild(tabBoxGenerator($j(orig_markup[i]).children('h2').text(),
																								$j(orig_markup[i]).children('p').text(),
																								$j(orig_markup[i]).children('a').attr('href'),
																								'../images/small_device/mobile-hp-tab-aff.gif'/*tpa=http://www.unitedrentals.com/static/images/small_device/mobile-hp-tab-aff.gif*/,
																								'/static/images/small_device/' + tab_box_icons[i]));
		}
		//
		target_div = $j('#content');
		target_div.prepend(tab_container);
	};
	// ################################################## Home Page Page End ##################################################
	//
	//
	//
	// ########################################## Top Level Landing Page Srt ##################################################
	buildTopLevelLandingPage = function (rebuildSideNav) {
		//console.log('buildTopLevelLandingPage');
		var linkbox_orig_markup,
				target_div,
				cat_container,
				cat_box_count,
				//
				helpbox_orig_markup,
				hlp_container,
				i,
				//
				linkBoxGereratorGeneric,
				linkBoxGereratorMultitopic,
				linkBoxGereratorTestimonial,
				linkBoxContainerGenerator,
				linkBoxGeneratorTraining,
				helpBoxGenerator,
				assignEnvents;
		//
		//
		// ############################## Generates a Linkbox Container ##############################
		linkBoxContainerGenerator = function (ttl) {
			//console.log("linkBoxContainerGenerator");
			var box_container, box_title_obj, box_title_aff, box_title_cpy, box_title_icn, title_link_bool;
			//
			if ($j(ttl).children('a').attr('href') === undefined) {
				title_link_bool = false;
			}
			//console.log($j(ttl).children('a').attr('href'));
			//
			box_container = document.createElement('div');
			box_container.setAttribute('class', 'mobileLandingBox');
			//
			box_title_obj = document.createElement('div');
			box_title_obj.setAttribute('class', 'mobileLandingBoxSecTitle');
			box_title_aff = document.createElement('div');
			box_title_aff.setAttribute('class', 'mobileLandingBoxSecTitleAffClosed mobileLandingBoxSecTitleAffHook');
			box_title_obj.appendChild(box_title_aff);
			box_title_cpy = document.createElement('div');
			box_title_cpy.setAttribute('class', 'mobileLandingBoxSecTitleCopy');
			box_title_obj.appendChild(box_title_cpy);
			if (title_link_bool !== false) {
				box_title_icn = document.createElement('div');
				box_title_icn.setAttribute('class', 'mobileLandingBoxSecTitleLinkIcon');
				box_title_obj.appendChild(box_title_icn);
			}
			//
			$j(box_title_cpy).html(ttl);
			box_container.appendChild(box_title_obj);
			//
			return box_container;
		};
		// ###########################################################################################
		//
		// ################################################## Generates a mobile version of a Generic Tout ##################################################
		linkBoxGereratorGeneric = function (ttl, cpy, lst, frm) {
			//console.log('linkBoxGereratorGeneric');
			var box_container, box_cpy_obj, box_list_obj, box_form_obj;
			//
			box_container = linkBoxContainerGenerator(ttl);
			
			//
			box_cpy_obj = document.createElement('div');
			box_cpy_obj.setAttribute('class', 'mobileLandingBoxSecCopy');
			//
			box_list_obj = document.createElement('div');
			box_list_obj.setAttribute('class', 'mobileLandingBoxSecLinks');
			//
			box_form_obj = document.createElement('div');
			box_form_obj.setAttribute('class', 'mobileLandingBoxForm');
			//
			if (cpy.text().length > 0) {
				$j(box_cpy_obj).html(cpy);
				box_container.appendChild(box_cpy_obj);
			}
			if (lst.length) {
				$j(box_list_obj).html(lst);
				box_container.appendChild(box_list_obj);
			}
			if (frm.length > 0) {
				$j(box_form_obj).html(frm);
				box_container.appendChild(box_form_obj);
			}
			//
			return box_container;
		};

		// ##########################################################################################
		//
		// ###### Generates a mobile version of a Training Calendar Tout ############################
		linkBoxGeneratorTraining = function (ttl, cpy) {
			//console.log('linkBoxGereratorGeneric');
			var box_container, box_cpy_obj;
			//
			box_container = linkBoxContainerGenerator(ttl);
			
			//
			box_cpy_obj = document.createElement('div');
			box_cpy_obj.setAttribute('class', 'mobileLandingBoxSecCopy');

			//
			if (cpy !== 'undefined') {
				if (cpy.text().length > 0) {
					$j(box_cpy_obj).html(cpy);
					box_container.appendChild(box_cpy_obj);
				}
			}

			//
			return box_container;
		};

		// ##################################################################################################################################################
		//
		// ################################################## Generates a mobile version of a Multitopic Tout ##################################################
		linkBoxGereratorMultitopic = function (ttl, topic_arr) {
			//console.log('linkBoxGereratorMultitopic');
			var box_container,
					i,
					//
					generateTopic;
			//
			box_container = linkBoxContainerGenerator(ttl);
			//
			generateTopic = function (topic) {
				var topic_container;
				topic_container = document.createElement('div');
				topic_container.setAttribute('class', 'mobileLandingMultipleTopicContainer');
				//
				if ($j(topic).children('h3').text().length < 1) {
					$j(topic).children('h3').addClass('mobileLandingMultipleTopicContainerNoH3');
					$j(topic).children('p').addClass('mobileLandingMultipleTopicContainerSoloP');
				}
				//
				//console.log($j(topic).children('ul').children('li').children('a').html());
				$j(topic).children('ul').children('li').children('a').removeClass('red-button');
				//
				$j(topic_container).append($j(topic).children('h3'));
				$j(topic_container).append($j(topic).children('p'));
				$j(topic_container).append($j(topic).children('ul'));
				//
				return topic_container;
			};
			//
			for (i = 0; i < topic_arr.length; i++) {
				$j(box_container).append(generateTopic(topic_arr[i]));
			}
			//
			return box_container;
		};
		// #####################################################################################################################################################
		//
		// ################################################## Generates a mobile version of a Testimonial Tout ##################################################
		linkBoxGereratorTestimonial = function (ttl, quote, attribution) {
			//console.log('linkBoxGereratorTestimonial');
			var box_container, box_quote_obj, box_attribution_obj;
			//
			box_container = linkBoxContainerGenerator(ttl);
			//
			box_quote_obj = document.createElement('div');
			box_quote_obj.setAttribute('class', 'mobileLandingBoxSecCopyTestimonialQuote');
			$j(box_quote_obj).append(quote[0]);
			//
			box_attribution_obj = document.createElement('div');
			box_attribution_obj.setAttribute('class', 'mobileLandingBoxSecCopyTestimonialAttribution');
			$j(box_attribution_obj).append(attribution[0]);
			//
			box_container.appendChild(box_quote_obj);
			box_container.appendChild(box_attribution_obj);
			//
			return box_container;
		};
		// ############################################################################################################################
		//
		// ################################################## Generates the Help Box ##################################################
		helpBoxGenerator = function (mrkup, wrapped) {

			var hlp_container = getHelpBox(mrkup,wrapped);
			return hlp_container;
		};
		// ############################################################################################################################
		//
		assignEnvents = function () {
			//console.log('assignEnvents');
			var title_aff;
			//
			title_aff = $j('.mobileLandingBoxSecTitleAffHook');
			//
			title_aff.bind('click', function () {
				var aff, link_box;
				//
				link_box = $j(this).parent().parent();
				aff = $j(this);
				//
				if(link_box.height() === 34) {
					link_box.css({'height': '100%'});
					aff.removeClass('mobileLandingBoxSecTitleAffClosed');
					aff.addClass('mobileLandingBoxSecTitleAffOpen');
				} else {
					link_box.css({'height': '34px'});
					aff.removeClass('mobileLandingBoxSecTitleAffOpen');
					aff.addClass('mobileLandingBoxSecTitleAffClosed');
				}
			});
			//
			bindHelpBox();
		};
		//
		linkbox_orig_markup = $j('#main .row.three-cols.has-dividers .box').children('div').clone();

		if(linkbox_orig_markup.length === 0) {
			linkbox_orig_markup = $j('.special-content-section').clone(); 
		}
		//
		//console.log(linkbox_orig_markup);
		//
		//
		cat_box_count = linkbox_orig_markup.length;
		cat_container = document.createElement('div');
		cat_container.setAttribute('class', 'mobileLandingBoxContainer');
		//
		//
		// ############################## Loop Looks for the three landing page touts and converts them to mobile ##############################
		for (i = 0; i < cat_box_count; i++){
			//
			if ($j(linkbox_orig_markup[i]).hasClass('generic-tout')) {
				//console.log('generic_' + i);
				cat_container.appendChild(linkBoxGereratorGeneric($j(linkbox_orig_markup[i]).children('h2'),
													$j(linkbox_orig_markup[i]).children('p'),
													$j(linkbox_orig_markup[i]).children('ul'),
													$j(linkbox_orig_markup[i]).children('.mini-form')));
			}
			//
			if ($j(linkbox_orig_markup[i]).hasClass('multitopic-tout')) {
				//console.log('multitopic_' + i);
				cat_container.appendChild(linkBoxGereratorMultitopic($j(linkbox_orig_markup[i]).children('h2'),
													$j(linkbox_orig_markup[i]).children('.topic')));
			}
			//
			if ($j(linkbox_orig_markup[i]).hasClass('testimonial-tout')) {
				//console.log('testimonial_' + i);
				cat_container.appendChild(linkBoxGereratorTestimonial($j(linkbox_orig_markup[i]).children('h2'),
													$j(linkbox_orig_markup[i]).children('.quote'),
													$j(linkbox_orig_markup[i]).children('.attribution')));
			}

			//
			if ($j(linkbox_orig_markup[i]).hasClass('special-content-section')) {
				//console.log('special-content-section' + i);
				//console.log (linkbox_orig_markup[i]);
				//console.log($j('.special-content-text',$j(linkbox_orig_markup[i])));
				cat_container.appendChild( linkBoxGeneratorTraining( $j('h2',$j(linkbox_orig_markup[i])),
						$j(linkbox_orig_markup[i])


					));
			}
			//
		}

		//

		// ##############################################################################################################################
		//
		//
		helpbox_orig_markup = $j('#main .can-we-help-menu').clone();
		if (helpbox_orig_markup.length === 0) {
			helpbox_orig_markup = $j('.can-we-help').clone();
			hlp_container = helpBoxGenerator(helpbox_orig_markup,"nowrap");
		}
		else
		{
			hlp_container = helpBoxGenerator(helpbox_orig_markup);
		}
			
		//
		//
		target_div = $j('#content');
		target_div.append(cat_container);

		// training calendar has navigation on the side that we want to move
		  if (rebuildSideNav === 'rebuildSideNav') {
		  	buildAndRelocateSideNavigation($j("#sidebar .subnav"), target_div);
		  }

		//
		//
		if ($j(hlp_container).children('ul').children('li').length > 0) {
			target_div.append(hlp_container);
		} else {
			$j(cat_container).css({'margin-bottom': '-74px'});
		}
		//
		assignEnvents();
	};
	// ################################################## Top Level Landing Page End ##################################################
	//
	//
	//
	// ################################################## Social Sharing Controls Srt ##################################################
	buildSocialSharingControls = function () {
		var trgt_mrkp, orig_mrkp,
				//
				socailConrolsGenerator, assignLinkAff,
				//
				route_check_str;
		//
		route_check_str = window.location.href.toString();
		//
		socailConrolsGenerator = function (mrkp) {
			if (mrkp.length < 1) {
				return;
			}
			var social_control, share_aff, social_links_container;
			//
			social_control = document.createElement('div');
			social_control.setAttribute('class', 'mobileSocialControlWrapper');
			//
			share_aff = document.createElement('div');
			share_aff.setAttribute('class', 'mobileSocialControlShareAff');
			social_control.appendChild(share_aff);
			//
			social_links_container = document.createElement('div');
			social_links_container.setAttribute('class', 'mobileSocialLinksContainer');
			social_links_container.appendChild(mrkp.children('.overlay').children('ul')[0]);
			$j(social_links_container).children('ul').children('li').removeClass('last');
			$j(social_links_container).children('ul').removeClass('menu');
			social_control.appendChild(social_links_container);
			//
			return social_control;
		};
		//
		/*
		assignLinkAff = function () {
			var tgt = $j('.mobileSocialControlShareAff');
			//
			tgt.bind('click', function () {
				var header = $j('#main #content-header');
				//
				if (header.height() > 40) {
					header.animate({'overflow': 'hidden', 'height': '40px'});
				} else {
					header.animate({'overflow': 'visible', 'height': '180px'});
				}
				//
			});
		};
		*/
		//
		if (!/catalog/i.test(route_check_str)) {
			trgt_mrkp = $j('#content #main-wide #content-header .row');
			orig_mrkp = $j('#content #main-wide #content-header .row .social-media-wrapper').clone();
			trgt_mrkp.append(socailConrolsGenerator(orig_mrkp));
			//assignLinkAff();
			bindSocialMedia();
		}
	};
	// ################################################## Social Sharing Controls End ##################################################
	//
	//
	//
	// ################################################## Location Landing Page Srt ##################################################
	buildLocationLandingPage = function () {
		var lang = "en";
		if (window.location.href.indexOf("/fr/") > 0) lang = "fr";

		//console.log('buildLocationLandingPage');
		var orig_markup, new_markup, markup_target;
		//
		markup_target = $j('#location-submission-form .box.span12.last .inner.search');
		//orig_markup = $j('#location-map-with-sidebar .browse-link').clone();
		orig_markup = $j('<div class="browse-link"><a class="ico-chevron" href="http://www.unitedrentals.com/%27 + lang + %27/equipment-rental-locations/browse?&searchType=newsearch">Browse rental locations by State / Province</a></div>');
		new_markup = document.createElement('div');
		new_markup.setAttribute('class', 'mobileBrowseForLocationsLink');
		
		new_markup.appendChild(orig_markup[0]);
		
		markup_target.append(new_markup);
	};
	// ################################################## Location Landing Page End ##################################################
	//
	//
	//
	// ################################################## Locations Browse Page Srt ##################################################
	buildLocationsBrowsePage = function () {
		var org_markup_usa,
				org_markup_can,
				new_markup_container,
				tgt_markup,
				//
				restyleLocationsCountt,
				genrtateLocationsList,
				fixCountryAnchor;
		//
		restyleLocationsCountt = function () {
			var mrkp;
			mrkp = $j('#wrapper #content #main-wide #main .row.no-vert-padding .box.span12.last .inner');
			mrkp.css({'padding': '10px'});
		};
		//
		genrtateLocationsList = function (mrkp) {
			var markp_container,
					list_head,
					list_a_elems,
					list_t_elems,
					i,
					//
					generateListItem;
			//
			generateListItem = function (a_elem, t_elem) {
				var itm, txt;
				itm = document.createElement('li');
				itm.setAttribute('class', 'mobileLocationsBrowseListItem');
				//
				txt = document.createElement('span');
				txt.setAttribute('class', 'mobileLocationsBrowseListItemNumber');
				$j(txt).append(t_elem);
				//
				$j(a_elem).append(txt);
				itm.appendChild(a_elem);
				//
				return itm;
			};
			//
			markp_container = document.createElement('ul');
			markp_container.setAttribute('class', 'mobileLocationsBrowseList');
			//
			if (mrkp.children('h2').attr('id') === 'canada') {
				mrkp.children('h2').attr('id', 'canada_mob_anchor');
			}
			//
			list_head = document.createElement('li');
			list_head.setAttribute('class', 'mobileLocationsBrowseListHead');
			list_head.appendChild(mrkp.children('h2')[0]);
			markp_container.appendChild(list_head);
			//
			list_a_elems = mrkp.children('table').children('tbody').children('tr').children('td').children('a');
			list_t_elems = mrkp.children('table').children('tbody').children('tr').children('td').children('br').andSelf().contents().filter(function() {return this.nodeType === 3;});
			for(i = 0; i < list_a_elems.length; i++){
				markp_container.appendChild(generateListItem(list_a_elems[i], $j(list_t_elems[i]).text()));
			}
			//
			return markp_container;
		};
		//
		fixCountryAnchor = function() {
			 $j('.canada-link').attr('href','#canada_mob_anchor');
		};
		
		new_markup_container = document.createElement('div');
		new_markup_container.setAttribute('class', 'mobileLocationsBrowseWrapper');
		//
		org_markup_usa = $j('#wrapper #content #main-wide #main #locs_list_usa').clone();
		org_markup_can = $j('#wrapper #content #main-wide #main #locs_list_can').clone();
		//
		tgt_markup = $j('#wrapper #content #main-wide #main');
		//
		restyleLocationsCountt();
		//
		new_markup_container.appendChild(genrtateLocationsList(org_markup_usa));
		new_markup_container.appendChild(document.createElement('br'));
		new_markup_container.appendChild(genrtateLocationsList(org_markup_can));
		//
		tgt_markup.append(new_markup_container);
		fixCountryAnchor();
	};
	// ################################################## Locations Browse Page End ##################################################
	//
	//
	// ################################################## Locations Detail Page Srt ##################################################
	buildLocationsDetailPage = function () {
		var orig_mrkup, target_mrkup,
				head_city_state_zip,
				head_address,
				head_aff_directions,
				head_distance,
				location_info,
				return_info,
				//
				setTimeoutContinue,
				bulidDetailInfo,
				forceMobilePhoneNumberInterface,
				setResponsiveTitle;
		//
		setTimeoutContinue = function () {
			var new_head_distance, map_side_copy;
			new_head_distance = $j(head_distance).clone();
			map_side_copy = document.createElement('div');
			map_side_copy.setAttribute('class', 'mobileLocationDetailMapSideCopy');
			//
			map_side_copy.appendChild(head_city_state_zip[0]);
			map_side_copy.appendChild(head_address[0]);
			head_aff_directions.removeClass('ico-chevron');
			map_side_copy.appendChild(head_aff_directions[0]);
			map_side_copy.appendChild(new_head_distance[0]);
			//
			target_mrkup.append(map_side_copy);
		};
		//
		bulidDetailInfo = function () {
			
			var map_bottom_copy, map_bottom_links, map_bottom_spc_top, map_bottom_spc_bot, bot_links,i;
			//
			map_bottom_copy = document.createElement('div');
			map_bottom_copy.setAttribute('class', 'mobileLocationDetailMapBottomCopy');
			//
			map_bottom_copy.appendChild(location_info[0]);
			map_bottom_copy.appendChild(return_info[0]);
			//
			map_bottom_links = document.createElement('div');
			map_bottom_links.setAttribute('class', 'mobileLocationDetailMapBottomLinks');
			
			bot_links = return_info.children('a');
			for (i = 0; i < bot_links.length; i++) {
				map_bottom_links.appendChild(bot_links[i]);
			}
			//
			map_bottom_copy.appendChild(map_bottom_links);
			//
			$j("#content").append(map_bottom_copy);
			//
			map_bottom_spc_top = document.createElement('div');
			map_bottom_spc_top.setAttribute('class', 'mobileLocationDetailMapTopSpacer');
			map_bottom_spc_bot = document.createElement('div');
			map_bottom_spc_bot.setAttribute('class', 'mobileLocationDetailMapBottomSpacer');
			//
			$j('.mobileLocationDetailMapBottomCopy .sidebar-block h3').after(map_bottom_spc_top);
			$j('.mobileLocationDetailMapBottomCopy .sidebar-block').first().append(map_bottom_spc_bot);
			//
			if ($j('.mobileLocationDetailMapBottomCopy .sidebar-block').length === 1) {
				$j('.mobileLocationDetailMapBottomCopy #location-details .sidebar-block').addClass('mobileLocationDetailMapBottomCopyShort');
			}
		};
		//
		forceMobilePhoneNumberInterface = function () {
			var phone_number_arr, number_val, number_lnk, number_txt, len, i;
			//
			phone_number_arr = $j('.mobileLocationDetailMapBottomCopy .loc-det-phone-num');
			len = phone_number_arr.length;
			
			for (i = 0; i < len; i++) {
			//
				number_val = $j(phone_number_arr[i]).text();
				number_txt = $j(phone_number_arr[i]).html();  //may include tracking span, thus different from text();
				
				number_lnk = document.createElement('a');
				number_lnk.setAttribute('href', 'tel:' + number_val);
				number_lnk.setAttribute('style', 'color:blue; text-decoration:underline;');
				number_lnk.innerHTML = number_txt;
								
			//console.log(number_lnk);
			//
				$j(phone_number_arr[i]).empty();
				$j(phone_number_arr[i]).append(number_lnk);
			}	
			//
			//console.log(number_str);
		};
		// shorten page title
		setResponsiveTitle = function() {
			var originalTitle, responsiveTitle, titleObj;
			titleObj = $j("#content-header h1");
			if (responsiveTitleForDetail != null) responsiveTitle = responsiveTitleForDetail;
			else responsiveTitle = "Location Details";
			if (titleObj !== null ) {
				originalTitle =  titleObj.text();
				titleObj.html('<span class="wideTemplate">' + originalTitle + '</span>' + '<span class="respTemplate">' + responsiveTitle + '</span>');
			}			
		};
		//
		setResponsiveTitle();
		
		//
		orig_mrkup = $j('#moblieLocationDetail .box.span4.sidebar .inner').clone();
		target_mrkup = $j('#moblieLocationDetail .box.span8.last .inner');
		//
		head_city_state_zip = orig_mrkup.children('.grey').children('#location-address').children('h2');
		head_city_state_zip.children('img').empty().remove();
		head_address = orig_mrkup.children('.grey').children('#location-address').children('address');
		head_aff_directions = orig_mrkup.children('.grey').children('#location-address').children('div').children('a');
		//
		location_info = orig_mrkup.children('.grey').children('#location-details');
		return_info = orig_mrkup.children('.return-search');
		//
		bulidDetailInfo();
		forceMobilePhoneNumberInterface();
		//
		window.setTimeout(function () {
			head_distance = $j('#moblieLocationDetail .box.span4.sidebar .inner').children('.grey').children('#location-address').children('div')[1];
			setTimeoutContinue();
		}, 2000);
	};
	//
	// ################################################## Locations Detail Page End #################################
	//
	//
	// ################################################## Vertical Align Marquee Text Start #########################
		// Determine if the responsive styles have been applied by checking visibilty of resp-logo element.
		// Alternatively we could check for the browser width.
		//
		isResponsiveStylesLoaded = function () {
			return ($j('#resp-logo').is(':visible')); 
		};
		//
		// Refine the top margin offset of the marquee text element to be a half of its height. 
		// This script assumes that that resposive styles have been applied where the marquee text
		// is absolutely positioned with top:50%;
			
		vAlignMarqueeText = function() {
			//
			var els, len, i, txtEl, txtHeight, homepageOffset = 0;
			//
			els = $j('.marquee-text');
			if (els === null) {
				return;
			}
			// slight vertical shift for the homepage marquee because its partially
			// below the fold
			if ($j('body').hasClass('homepage') ){
				homepageOffset = 6;
			}
			//
			len = els.length;
			for (i = 0; i < len; i++) {
				txtEl = $j(els[i]);
				txtHeight = txtEl.height();
				txtEl.css('margin-top','-' + ((txtHeight/2) + homepageOffset ) + 'px');
			}		
		};
	// ################################################## Vertical Align Marquee Text and Buttons End #########################
	//
	// ################################################## Footer Phone Interace Start #########################
	assignFooterPhoneInterface = function () {
		var phone_number, number_str, number_lnk, number_txt;
		//
		phone_number = $j('#footer_top_mobile .main-phone strong');
		//
		number_str = phone_number.text();
		number_txt = document.createTextNode(number_str);
		number_lnk = document.createElement('a');
		number_lnk.setAttribute('href', 'tel:1-800-877-3687');
		number_lnk.setAttribute('style', 'color:blue; text-decoration:underline;');
		number_lnk.appendChild(number_txt);
		//console.log(number_lnk);
		//
		phone_number.empty();
		phone_number.append(number_lnk);
		//
		//console.log(number_str);
	};
// ################################################## Footer Phone Interace End #########################		
//
//
// ################################################## Hide Address Bar on Iphone Safari Begin ############
	
	hideAddressBar = function() {
		// set timeout is required (re: http://davidwalsh.name/hide-address-bar)
		setTimeout(function() {
			// Hide the address bar!
			//window.scrollTo(0, 1);
			
			// Refine position of the marquee text - should be done only after the address bar is hidden
			// (iphone safari has a known issue where it includes address bar in the height() at the inital load)
			if (isResponsiveStylesLoaded()) {
				vAlignMarqueeText();
			}
		}, 0);
	};
// ################################################## Hide Address Bar on Iphone Safari End #####################
//
//	 
//################################################## Campaign Registration Page ##################################
	buildCampaignRegistrationPage = function () {
		
			var toggleExpand = function() {
				var obj = $j('.support-content');
				if (obj.hasClass("expand")) {
					obj.removeClass("expand");
				} else {
			  	obj.addClass("expand");
			  }
			};
			//mark paragraph with an image - makes it easier to style...
			var hasImage = $j("p:has(img)");
			if (hasImage.length > 0 ) {
				$j(hasImage[0]).addClass("hasImage");
			}

			//bind affordance
			$j(".mobileLinkAff").bind("click", toggleExpand);
	};
	// ################################################## Campaign Registration Page End ##############################
	//
	//
	// ################################################## Training Finder Page End ####################################
	buildTrainingFinderPage = function () {
		//console.log('buildTrainingFinderPage');
		// local vars and methods
		//
		var sidebar, target;
		var buildAndRelocateDaysFilter, 
				buildAndRelocateCourseSelection,
				buildAndRelocateStateSelection,
				buildAndRelocateResultCounter, 
				buildFilterAnchor,
				buildExpandCollapseAffordance;

		// filter by days - moving to side navingation
		buildAndRelocateDaysFilter = function() {
			var mobile_daysfilter, target, label;
			mobile_daysfilter = $j("select#days").clone();
			mobile_daysfilter.attr("id","daysMobile");
			mobile_daysfilter.attr("name","daysMobile");
			target = $j("#mobileDaysFilterContainter");
			target.prepend(mobile_daysfilter);
			label = document.createElement("span");
			label.innerHTML = "Show the next:";
			target.prepend($j(label));
		};

		// move selected course
		buildAndRelocateCourseSelection = function(){
				var e = $j(".course.desktopTemplate");
				if (e !== undefined) {
					e = e.clone();
					e.removeClass('desktopTemplate');
					$j('#mobileCourseTypeContainter').append(e);
				}
		};

		// move selected state
		 buildAndRelocateStateSelection = function() {
		 		var e = $j(".state.desktopTemplate");
				if (e !== undefined) {
					e = e.clone();
					e.removeClass('desktopTemplate');
					$j('#mobileStateContainter').append(e);
				}
		 };
		
		// result counter - moving below header
		buildAndRelocateResultCounter = function() {
			var e = $j("#content-header h1 em").clone();
			e.addClass('respTemplate');
			$j('.filterRow .box.span8 .inner').append(e);
		};

		// add link to jump to the filter section at the bottom of the page
		buildFilterAnchor = function() {
			var e = document.createElement("a");
			e.setAttribute("href","#filterSection");
			e.setAttribute("class","respTemplate iconFilter");
			$j('.mobileSubnav').attr("id","filterSection");
			$j('.filterRow .box.span1 .inner')[0].appendChild(e);
		};

		// add controls to expand/collapse filter options
		buildExpandCollapseAffordance = function() {
			var filters = $j(".filter-label.all");
			var len = filters.length;
			var i,e;
			for (i = 0 ; i < len; i++) {
				//console.log(filters[i]);
				var e = document.createElement("div");
				e.setAttribute("class","respTemplate filterAff");
				$j(filters[i]).prepend(e);
			}
		};

		//
		// rearrange markup
		//
		buildAndRelocateDaysFilter(); 
		buildAndRelocateCourseSelection();
		buildAndRelocateStateSelection();
		//side nav goes after content
	  	buildAndRelocateSideNavigation($j("#sidebar .subnav"), $j('#content')); 
	  	buildAndRelocateResultCounter();
	  	buildFilterAnchor();
	  	buildExpandCollapseAffordance();
	  	$j('ul.navigation').addClass('mobileCollapse');
	  	//
	  	// bind day selector to the mobile control
	  	//
		$j("select#daysMobile").bind('change', function() { 
		  	var mobileVal, desktopVal;
		  	mobileVal = $j(this).val();
		  	desktopVal = $j('select#days').val();
		  	if( mobileVal !== desktopVal) {
		  		  var o = $j('select#days');
		  			o.val($j(this).val());
		  			o.change();
		  	}
		  });
	  
		  //
		  // bind expand/collapse affordances
		  //
		  $j(".filterAff").bind('click', function() {
	  		var parentList;
	  		var parentHeight, thisHeight;

	  		parentList = $j(this).parent().parent();
	  		thisHeight = $j(this).height();  	  		

	  		parentHeight = $j(parentList).height();	
	  		if (!parentList.hasClass('mobileCollapse')) {
	  			parentList.addClass('mobileCollapse');
	  		}
	  		else {
	  			parentList.removeClass('mobileCollapse');
	  		}
		});
		bindSocialMedia();
	}
	// ################################################## Training Finder Page End ############################
  	//
  	//
  	// ################################################## Training Detail Page Begin ##########################
  buildTrainingDetailPage = function() {
  	//console.log('buildTrainingDetailPage');
  	var buildH1, 
  			hideSubheader,
  			createSection,
  			buildHelpBox,
  			replaceSectionHtml,
  			createSectionFromMarkup,
  			getCourseDetailMarkup,
  			haveValidationErrors,
  			expandForm;			

  	//responsive page has different h1 - so annoying!!!
  	buildH1 = function() {
  		var h1, origHtm, newHtm;
  		var h1 = $j('h1');
  		origHtm = h1[0].innerHTML;
  		newHtm =  '<span class="respTemplate">Class Detail &amp; Registration</span>'
  						+ '<span class="desktopTemplate">'+ origHtm + '</span>';				
  		h1[0].innerHTML = newHtm;					
  	};

  	// class "desktopTemplate" is hidden on all responsive pages
  	hideSubheader = function() {
  		var e = $j('.h1-sub');
  		if (e.length > 0) {
  			 $j(e[0]).addClass('desktopTemplate');
  		}
  	};
  	
  	// assign noData class to outer element of the empty section - will be hidden in css
  	replaceSectionHtml = function (mrkup,target) {
  		 if (mrkup !== undefined && mrkup.length > 0) {
  		 		target.innerHTML = mrkup;
  		 } else {
  		 		$j(target).parent().addClass("noData");
  		 }
  	};

  	// copy markup from a desktop section to a corresponding responsive section
  	createSection = function(src, target) {
  		var s,t, mrkup;
  		var s = $j(src);
  		var t = $j(target);
  		if (s.length > 0 && t.length > 0) {
  			mrkup = s[0].innerHTML;
  			replaceSectionHtml(mrkup,t[0]);
  		}
  	};

  	// create responsive section using markup provided as an argument
  	createSectionFromMarkup = function(newMarkup, target) {
  			var t = $j(target);
  			if (t.length > 0) {
  					replaceSectionHtml(newMarkup,t[0]);				
  			}
  	};

  	// concatenate a couple of sectons to get course detail information
  	getCourseDetailMarkup = function() {
  		var sources = [	".course-desc-more .inner", 
  									 	".session-desc .inner", 
  										".session-desc-more .inner" ];
  		var i, e, htm;
  		htm = "";
  		for (i = 0 ; i < sources.length; i++) {
  				e = $j(sources[i]);
  				if (e.length > 0) {
  					htm = htm + e[0].innerHTML;
  				}
  		}
  		return htm;
  	};

  	// build help box
  	buildHelpBox = function() {
  		 var helpbox_orig_markup, hlp_container;
  		 helpbox_orig_markup = $j('.can-we-help').clone();
			 hlp_container = getHelpBox(helpbox_orig_markup,"nowrap");
			 $j('#content').append(hlp_container);
  	};
  	//
  	expandForm = function() {
  		var o = $j("#registration");
 			o.removeClass("mobileCollapse");
  	};
  	//
  	haveValidationErrors = function() {
  		var e, i;
  		e = $j('.errors:visible');
  		if (e.length > 0 ) {
  			return true;			
  		}
  		return false;
  	};

  	//
  	//
  	// rearrange markup
  	buildH1();
  	hideSubheader();
  	buildHelpBox();
  	createSection(".dates .inner","#mDateTime .section-content");
  	createSection(".address-wrap","#mLocation .section-content");
  	createSection(".cost .info","#mCost .section-content");
  	createSection(".course-desc .inner","#mCourseOverview .section-content");
  	createSectionFromMarkup(getCourseDetailMarkup(),"#mCourseDetails .section-content");
  	
  	//
  	// expand regisration form, if url hash = "#registration"
  	// or if the form has validation errors 	
 		if (window.location.hash === '#registration'|| haveValidationErrors () ){
 				expandForm();
 		}

  	//
  	//
  	// bind collapse/expand functionality
  	$j(".mCollapseLink").bind('click', function() {
	  		var parentElement;
	  		parentElement = $j(this).parent();
	  		if (parentElement.hasClass('mobileCollapse')) {
	  			parentElement.removeClass('mobileCollapse');
	  		}
	  		else {
	  			parentElement.addClass('mobileCollapse');
	  		}
	  });
	  //
	  // expand registration form when clicking on the anchor
	  $j("#linkToRegisrationRow .button").bind ('click',expandForm);

	  bindHelpBox();
	  bindSocialMedia();
  };

  // ################################################## Training Detail Page End ###########################
  //
  //
  //
  // ################################################## Training Thank You Page Start ######################
  buildTrainingThankYouPage = function() {
  	var buildH1, buildHelpBox;
  	// this stupid page template doesn't have title in the right place
  	buildH1 = function() {
  		var h2, h1, htm;
  		h2 = $j('.introduction h2');
  		h1 = $j('h1');

  		if (h2.length > 0 && h1.length > 0) {
  			htm = h2[0].innerHTML;
  			h1[0].innerHTML = htm;
  			$j(h1[0]).addClass('respTemplate');
  		}
  	};

  	buildHelpBox = function() {
  		 var helpbox_orig_markup, hlp_container;
  		 helpbox_orig_markup = $j('.can-we-help').clone();
			 hlp_container = getHelpBox(helpbox_orig_markup,"nowrap");
			 //console.log('--------');
			 //console.log(hlp_container);
			 //console.log('--------');
			 $j('#content').append(hlp_container);
  	};

  	buildH1();
  	buildHelpBox();
  	//
  	bindSocialMedia();
  	bindHelpBox();
  };
  // ################################################## Training Thank You Page End #######################

	// ################################################## Sidebar Interface Refactor Start ######################
	sideBarInterfaceRefactor = function () {
		//console.log('sideBarInterfaceRefactor');
		var orig_markup, new_markup, markup_target,
		corp_contact_conatainer, corp_contact_aff, related_links_container, related_links_aff;
		//
		markup_target = $j('#main .sidebar.last');
		orig_markup = $j('#main .sidebar.last .inner').clone();
		new_markup = orig_markup;
		new_markup.attr('class', 'mobileSideBarInterface');
		markup_target.append(new_markup);
		//
		corp_contact_conatainer = new_markup.children('.can-we-help');
		corp_contact_aff = new_markup.children('.can-we-help').children('h3');
		corp_contact_aff.bind('click', function (){
			if ($j(this).parent().height() === 34) {
				$j(this).parent().css({'height':'100%'});
				$j(this).addClass('can-we-help-h3-open');
			} else {
				$j(this).parent().css({'height':'34px'});
				$j(this).removeClass('can-we-help-h3-open');
			}
		});
		//
		related_links_container = new_markup.children('.menu-tout');
		related_links_aff = new_markup.children('.menu-tout').children('h3');
		related_links_aff.bind('click', function (){
			if ($j(this).parent().height() === 34) {
				$j(this).parent().css({'height':'100%'});
				$j(this).addClass('menu-tout-h3-open');
			} else {
				$j(this).parent().css({'height':'34px'});
				$j(this).removeClass('menu-tout-h3-open');
			}
		});
		related_links_aff = new_markup.children('.menu-tout').children('ul').children('li').children('a').removeClass('ico-pdf');
		//
		$j('.mobileSideBarInterface .can-we-help').appendTo('.mobileSideBarInterface');
	};
	// ################################################## Sidebar Interface Refactor End ######################

	// ################################################## Pull Empty List Start ######################
	pullEmptyList = function () {
		var tgt_list, i;
		tgt_list = $j('.special-content-text .links-list');
		for(i = 0; i < tgt_list.length; i++) {
			if ($j(tgt_list[i]).children('li').text().length < 1) {
				$j(tgt_list[i]).children('li').hide();
			}
		}
	};
	// ################################################## Pull Empty List End ######################

	// ################################################## Redraw Bio Page Start ######################
	redrawBioPage = function () {
		var src_h1, src_h1_txt, tgt_h1;
		src_h1 = $j('#main h2');
		src_h1_txt = src_h1.text();
		tgt_h1 = $j('#content-header h1');
		tgt_h1.text(src_h1_txt);
		tgt_h1.css({'height': '28px'});
	};
	// ################################################## Redraw Bio Page End ######################

	// ################################################## ShowMediaContact Start ######################
	showMediaContact = function (hgt) {
		if ($j('.can-we-help').length < 1) {
			return;
		}
		if (hgt > 0) {
			hgt = 10;
		}
		var helpbox_orig_markup, hlp_container;
		helpbox_orig_markup = $j('.can-we-help').clone();
		hlp_container = getHelpBox(helpbox_orig_markup,"nowrap");
		//console.log(hlp_container);
		$j('#main').append($j(hlp_container).css({'margin-bottom': '0px', 'margin-top': hgt + 'px'}));
		bindHelpBox();
	};
	// ################################################### ShowMediaContact End #######################

	// ################################################### FAQs Landing Page Start #######################
	buildFaqsLandingPage = function () {
		var faq_groups, faq_blocks,
		buildFaqBlock, new_markup,
		i;
		//
		buildFaqBlock = function (head, list, back) {
			var output_markup, head_markup, list_markup, back_markup;
			//
			head_markup = document.createElement('h4');
			head_markup.appendChild(document.createTextNode(head));
			//
			list_markup = $j(list).clone();
			//
			// if (back.length > 2) {
			// 	back_markup = document.createElement('p');
			// 	back_markup.appendChild(document.createTextNode(back));
			// }
			//
			output_markup = document.createElement('div');
			output_markup.setAttribute('class', 'faqsLandingFaqBlock');
			//
			output_markup.appendChild(head_markup);
			$j(output_markup).append(list_markup);
			// if (back.length > 2) {
			// 	output_markup.appendChild(back_markup);
			// }
			//
			return output_markup;
		};
		//
		faq_groups = $j('#content #main #faq .row .box.span4 .inner');
		faq_blocks = [];
		for (i = 0; i< faq_groups.length; i++){
			faq_blocks[i] = buildFaqBlock($j(faq_groups[i]).children('h4').text(),
									   $j(faq_groups[i]).children('ul'),
									   $j(faq_groups[i]).children('.browse-all').text());
		}
		//
		new_markup  = document.createElement('div');
		new_markup.setAttribute('class', 'mobileFaqsLandingContent');
		//
		for (i = 0; i < faq_blocks.length; i++){
			new_markup.appendChild(faq_blocks[i]);
		}
		$j('#content #main #faq').prepend(new_markup);
		//
		$j('.faqsLandingFaqBlock h4').bind('click', function (evt) {
			if ($j(this).hasClass('mobileFaqsLandingH4over')) {
				$j(this).removeClass('mobileFaqsLandingH4over');
				$j(this).parent().css({'height': '34px'});
				return;
			}
			$j('.faqsLandingFaqBlock h4').removeClass('mobileFaqsLandingH4over');
			$j('.faqsLandingFaqBlock').css({'height': '34px'});
			$j(this).addClass('mobileFaqsLandingH4over');
			$j(this).parent().css({'height': 'auto'});
		});
	};
	// #################################################### FAQs Landing Page End ########################

	// ################################################### FAQs Page Start #######################
	buildFaqsPage = function () {
		var orig_markup, new_markup, target_markup, acc_markup,
			h3_arr, panel_arr, h3_newmarkup_arr, panel_newmarkup_arr,
			head_target, head_ttl, head_orig_ttl1, head_orig_ttl2, head_orig_ttl3,
			helpbox_orig_markup, hlp_container,
			i,
			//
			resetAllNodes,
			buildFAQsSelectBox;
		//
		buildFAQsSelectBox = function () {
			var orig_items, items_arr, select_box, select_box_holder, selected_val, n;
			//
			orig_items = $j('#sidebar .subnav .navigation li').clone();
			select_box = document.createElement('select');
			select_box.setAttribute('id', 'faq_select');
			select_box_holder = document.createElement('div');
			select_box_holder.setAttribute('id', 'faq_select_holder');
			items_arr = [];
			for (n = 0; n < orig_items.length; n++) {
				//
				if ($j(orig_items[n]).hasClass('active')) {
					selected_val = $j(orig_items[n]).children('a').attr('href')
					//console.log(selected_val);
				}
				//
				items_arr[n] = document.createElement('option');
				items_arr[n].setAttribute('value', $j(orig_items[n]).children('a').attr('href'));
				items_arr[n].appendChild(document.createTextNode($j(orig_items[n]).children('a').text()));
				select_box.appendChild(items_arr[n]);
			}
			$j(select_box_holder).prepend(select_box);
			$j('.faqsMobile').prepend(select_box_holder);
			$j("#faq_select").val(selected_val);
			//
			$j(select_box).bind('change', function () {
				window.location  = $j("#faq_select option:selected").attr('value');
			});
		};
		resetAllNodes = function  (evt) {
			var nodes_arr, panels_arr, n;
			nodes_arr = $j('#accordion .accNode');
			panels_arr = $j('#accordion .accPanel');
			for (n = 0; n < nodes_arr.length; n++){
				$j(nodes_arr[n]).css('background-image','url(/static/images/small_device/mobile-landing-title-aff-closed.gif)');
				$j(panels_arr[n]).css({'padding': '0px', 'height': '0px'});
			}
		};
		function activateNode (evt) {
			resetAllNodes();
			//console.log(current_faq_node);
			if ($j(this).attr('id').substring(9) === current_faq_node) {
				current_faq_node = undefined;
				return;
			}
			//console.log($j(this).attr('id').substring(9));
			$j(this).css('background-image','url(/static/images/small_device/mobile-landing-title-aff-open.gif)');
			$j('#accordion #acc_panel_' + $j(this).attr('id').substring(9)).css({'padding': '10px', 'height': 'auto'});
			current_faq_node = $j(this).attr('id').substring(9);
		}
		//
		orig_markup = $j('#faq-wrap .inner').clone();
		target_markup = $j('#faq-wrap');
		//
		acc_markup = document.createElement('div');
		acc_markup.setAttribute('id', 'accordion');
		//
		new_markup = document.createElement('div');
		new_markup.setAttribute('class', 'faqsMobile');
		//
		h3_arr = $j("#faq-wrap #faq-shortcuts p a");
		panel_arr = $j("#faq-wrap #faq-full .qa");
		//console.log(panel_arr);
		//
		h3_newmarkup_arr = [];
		panel_newmarkup_arr = [];
		for (i = 0; i < h3_arr.length; i++) {
			//
			h3_newmarkup_arr[i] = document.createElement('p');
			h3_newmarkup_arr[i].setAttribute('id', 'acc_node_' + i);
			h3_newmarkup_arr[i].setAttribute('class', 'accNode');
			h3_newmarkup_arr[i].appendChild(document.createTextNode($j(h3_arr[i]).text()));
			//
			panel_newmarkup_arr[i] = document.createElement('div');
			panel_newmarkup_arr[i].setAttribute('id', 'acc_panel_' + i);
			panel_newmarkup_arr[i].setAttribute('class', 'accPanel');
			$j(panel_newmarkup_arr[i]).append($j(panel_arr[i]).html());
		}
		//
		for (i = 0; i < h3_newmarkup_arr.length; i++) {
			$j(acc_markup).append(h3_newmarkup_arr[i]);
			$j(acc_markup).append(panel_newmarkup_arr[i]);
		}
		//
		//console.log(acc_markup)
		$j(new_markup).append(acc_markup);
		//
		target_markup.prepend(new_markup);
		//
		head_ttl = document.createElement('span');
		head_ttl.setAttribute('class', 'mobileFAQsHeadTitle');
		head_target = $j('#content-header h1');
		head_orig_ttl1 = $j('#breadcrumbs a').text();
		head_orig_ttl2 = $j('#sidebar .subnav ul li span').text();
		//
		if(!$j('body').hasClass('faq-landing')) {
			//head_ttl.appendChild(document.createTextNode(head_orig_ttl1 + ' – ' + head_orig_ttl2 + ' /'));
			head_ttl.appendChild(document.createTextNode(head_orig_ttl1 + ' /'));
		}
		//
		head_target.prepend(head_ttl);
		//
		if ($j('#sidebar .subnav .navigation li').length > 1) {
			buildFAQsSelectBox();
		}
		//
		$j('#accordion .accNode').bind('click', activateNode);
		//
		helpbox_orig_markup = $j('.can-we-help').clone();
		hlp_container = getHelpBox(helpbox_orig_markup,"nowrap");
		$j('#main').append($j(hlp_container).css({'margin-bottom': '0px', 'margin-top': '0px'}));
		bindHelpBox();
	};
	// #################################################### FAQs Page End ########################

	// #################################################### Press Detail Start ########################
	buidlPressDetail = function () {
		var orig_text, tgt_markup, new_text, new_head;
		//
		//
		orig_text = $j('#content-header h1');
		tgt_markup = $j('.pressrelease-item-wrap');
		new_text = document.createElement('p');
		new_text.setAttribute('class', 'pressReleaseDetailTitle');
		new_text.appendChild(document.createTextNode(orig_text.text()));
		tgt_markup.prepend(new_text);
		//
		new_head = orig_text.clone();
		new_head.addClass('pressReleaseDetailHead');
		new_head.text('PRESS RELEASE DETAIL');
		$j('#content-header .row').prepend(new_head);
	};
	// ##################################################### Press Detail End #########################

	// #################################################### Press Detail Start ########################
	buildEventDetail = function () {
		var orig_title, orig_subtitle, new_title, new_content_title;
		//
		orig_title = $j('#content-header .row h1').clone();
		orig_subtitle = $j('#content-header .row .h1-sub').clone();
		//
		new_title = document.createElement('h1');
		new_title.appendChild(document.createTextNode('EVENT DETAIL'));
		new_title.setAttribute('class', 'mobileEventDetailHead');
		$j('#content-header .row').prepend(new_title);
		//
		new_content_title =document.createElement('div');
		new_content_title.setAttribute('class', 'mobileEventDetailTitles');
		$j(new_content_title).prepend(orig_subtitle);
		$j(new_content_title).prepend(orig_title);
		$j('#main .content').prepend(new_content_title);
	};
	// ##################################################### Press Detail End #########################

	// #################################################### Campaign Registration Page Start ########################
	campaignRegistrationPage = function (is_detail) {
		var elem, tgt, h3, buildHelpBox;
		// //
		
		//
		buildHelpBox = function () {
			var helpbox_orig_markup, hlp_container;
			helpbox_orig_markup = $j('#campaign-registration-actual-help .can-we-help').clone();
			hlp_container = getHelpBox(helpbox_orig_markup,"nowrap", "campaign");
			//console.log(hlp_container);
			 if (is_detail) {
			 	$j('.campaignRegInner').append($j(hlp_container).css({'margin-bottom': '0px', 'margin-top': '10px'}));
			 } else{
			 	$j('#campaignReg .inner').append($j(hlp_container).css({'margin-bottom': '0px', 'margin-top': '10px'}));
			 }
			 bindHelpBox();
		};
		//
		buildHelpBox();
	};
	// ##################################################### Campaign Registration Page End #########################

	// #################################################### Sidebar Specialty Form Start ########################
	sidebarSpecialityForm = function () {
		//$j('#specialty-form-container').addClass('specialtyFormContainerOpen');
		var form_head, form_head_mob, form_contatiner, form_contatiner_mob, form_clone, clone_target;
		//
		form_head = $j('#specialty-form-container .specialtyFormTitle');
		form_contatiner = $j('#specialty-form-container');
		clone_target = $j('#main');
		//
		//alert('set');
		//
		//
		form_head.bind('click', function () {
			//alert('wha');
			if(form_contatiner.height() === 34){
				form_contatiner.addClass('specialtyFormContainerOpen');
			} else{
				form_contatiner.removeClass('specialtyFormContainerOpen');
			}
		});
		//
		//
		if(form_contatiner.children('form').children('dl').children('.form-row').children('div').children('dd').children('ul').length > 0){
			form_contatiner.addClass('specialtyFormContainerOpen');
		}
		//
		form_clone = form_contatiner.clone();
		form_clone.attr('id', 'specialty-form-container-mobile');
		clone_target.append(form_clone);
		//
		//
		form_contatiner_mob = $j('#specialty-form-container-mobile');
		form_head_mob = $j('#specialty-form-container-mobile .specialtyFormTitle');
		/*
		form_head_mob.bind('click', function () {
			if(form_contatiner_mob.height() === 34){
				form_contatiner_mob.addClass('specialtyFormContainerOpen');
			} else{
				form_contatiner_mob.removeClass('specialtyFormContainerOpen');
			}
			//alert('mobile click');
		});
		*/
	};
	// ##################################################### Sidebar Specialty Form End #########################

	buildCatalogHelpMenu = function () {
        var helpbox_orig_markup,
        hlp_container,
        i,
        assignEnvents,
        helpBoxGenerator;


        assignEnvents = function () {
                        //console.log('assignEnvents');
                        var title_aff;
                        //
                        title_aff = $j('.mobileLandingBoxSecTitleAffHook');
                        //
                        title_aff.bind('click', function () {
                                var aff, link_box;
                                //
                                link_box = $j(this).parent().parent();
                                aff = $j(this);
                                //
                                if(link_box.height() === 34) {
                                        link_box.css({'height': '100%'});
                                        aff.removeClass('mobileLandingBoxSecTitleAffClosed');
                                        aff.addClass('mobileLandingBoxSecTitleAffOpen');
                                } else {
                                        link_box.css({'height': '34px'});
                                        aff.removeClass('mobileLandingBoxSecTitleAffOpen');
                                        aff.addClass('mobileLandingBoxSecTitleAffClosed');
                                }
                        });
                        //
                        bindHelpBox();
                };
        helpBoxGenerator = function (mrkup, wrapped) {

                        var hlp_container = getHelpBox(mrkup,wrapped);
                        return hlp_container;
                };

        helpbox_orig_markup = $j('#main .can-we-help-menu').clone();
                if (helpbox_orig_markup.length === 0) {
                        helpbox_orig_markup = $j('.can-we-help').clone();
                        hlp_container = helpBoxGenerator(helpbox_orig_markup,"nowrap");
                }
                else
                {
                        hlp_container = helpBoxGenerator(helpbox_orig_markup);
                }

                //
                //
                var target_div = $j('div.can-we-help');
		//
                if ($j(hlp_container).children('ul').children('li').length > 0) {
                        //target_div.replaceWith("<div class='can-we-help'>" + hlp_container.outerHTML + "</div>");
                        target_div.replaceWith(hlp_container.outerHTML);
			//console.log(target_div);
			//$j("section").append(target_div);
                        $j(".mobileLandingHelpContainer").css({'margin-bottom': '10px'});
			//console.log($j("div.can-we-help"));
			if ($j("div.filters-block")) {
				//$j("div.filters-block").insertBefore(target_div);
				//target_div.appendTo($j("div.filters-block"));
				//$j("div.can-we-help").insertAfter($j("div.filters-block"));
				$j("div.mobileLandingHelpContainer").insertAfter($j("div.filters-block"));
			} else {
				target_div.appendTo($("section"));
				//$j("div.can-we-help").appendTo($("section"));
			}
			$j("div.can-we-help").remove();
                }
                assignEnvents();
};


	$j(document).ready(function(){
		//
		//alert('ready');
		//
		var lang = "en";
		if (window.location.href.indexOf("/fr/") > 0) lang = "fr";
		anonymous_user=true;
		mobile_menu_icon = $j('#resp-b-nav');
		search_icon = $j('#resp-b-search');
		mobile_menu_nav = $j('#mainnav');
		mobile_search_bar = $j('.site-search');
		util_language = $j('#utilnav .switch-language').clone();
		util_location = $j('#utilnav .find-location').clone();
		util_custsuppt = $j('#utilnav .customer-support').clone();
		util_signin = $j('#utilnav .access-urdata').clone();
		//
		util_welcome = $j('<li><a href="http://www.unitedrentals.com/%27 + lang + %27/catalog/customer/account/">My Account Dashboard</a></li>');
		util_signout = $j('<li><a href="http://www.unitedrentals.com/%27 + lang + %27/catalog/customer/account/logout">Sign Out</a></li>');
		//
		search_txt_input = $j('#catalog_search');
		footer_top_mrkp = $j('#footer-top').clone();
		footer_top_trgt = $j('#footer');
		//
		util_location.removeClass('find-location').children('a').removeClass('ico ico-pin ico-utils');
		util_custsuppt.removeClass('customer-support').children('a').removeClass('ico ico-person ico-utils');
		util_signin.removeClass('access-urdata');
		//
		//
		footer_top_mrkp.attr('id', 'footer_top_mobile');
		footer_top_trgt.append(footer_top_mrkp);
		//
		$j.ajax({
			cache: false,
			url: "/" + lang + "/catalog/shared-header.php?mode=urdata",
			success: function(data) {
				util_signin_txt = $j($j(data)[0]).removeClass('ico ico-key ico-utils');
				if ($j(data).length > 3) {
					anonymous_user = false;
				}
				util_signin.html(util_signin_txt);
				reassignNavStyling();
	 		}
		});
		//
		$j.ajax({
			cache: false,
			url: "/" + lang + "/catalog/shared-header.php?mode=cart",
			success: function(data) {
				var tgt, cnt;
				//
				tgt = $j($j(data)[0]).children('span').clone();
				cnt = Number(tgt.text());
				if (cnt > 0) {
					appendCartIconBadge(tgt);
				}
			}
		});
		//
		mobile_menu_icon.bind('click', function () {
			toggleMobileNavigation();
		});
		//
		search_icon.bind('click', function () {
			toggleMobileSearch();
		});
		//
		if (window.addEventListener) {
			window.addEventListener('orientationchange',hideAddressBar);
		}	
		if ($j('body').hasClass('homepage')){
			buildMobileHomepageTabs();
		}
		//
		if ($j('body').hasClass('solution-landing-page') || ($j('body').hasClass('campaign') && $j('body').hasClass('landing-page'))) {
			//console.log('VIEWING LANDING PAGE...');
			buildTopLevelLandingPage();
		}
		//
		if ($j('body').hasClass('training') && $j('body').hasClass('landing') ) {
			buildTopLevelLandingPage('rebuildSideNav');
		}
		//
		if ($j('body').hasClass('training') && $j('body').hasClass('trainingsession-list') ) {
			buildTrainingFinderPage();
		}
		//
		if ($j('body').hasClass('training') && $j('body').hasClass('thank-you') ) {
			buildTrainingThankYouPage();
		}
		//
		if ($j('body').hasClass('trainingsession-detail')) {
			buildTrainingDetailPage();
		}
		//
		if (!$j('body').hasClass('homepage')){
			buildSocialSharingControls();
		}
		//
		if ($j('body').hasClass('locations') && $j('body').hasClass('index')){
			if ($j("div[id*=moblieLocationDetail]").length === 0) {
				//console.log('LOCATIONS LANDING...');
				buildLocationLandingPage();
			} else {
				//console.log('LOCATIONS DETAIL...');
				buildLocationsDetailPage();
			}
		}
		//
		if ($j('body').hasClass('locations') && $j('body').hasClass('browse')){
			//console.log('LOCATIONS BROWSE...');
			buildLocationsBrowsePage();
		}
		//
		if ($j('body').hasClass('campaign-registration')){
			//console.log('CAMPAIGN REGISTRATION ...');
			buildCampaignRegistrationPage();
		}
		//
		if ($j('body').hasClass('our-company-page') || $j('body').hasClass('our-company-bio-page')){
			sideBarInterfaceRefactor();
		}
		//
		if ($j('body').hasClass('our-company-bio-page')){
			redrawBioPage();
		}
		//
		if ($j('body').hasClass('our-company-page') && $j('body').hasClass('college-graduates')){
			pullEmptyList();
		}
		//
		if ($j('body').hasClass('press-room') || $j('body').hasClass('in-the-news') || $j('body').hasClass('awards-recognition') || $j('body').hasClass('awards-recognition') || $j('body').hasClass('investor-relations')){
			showMediaContact();
		}
		if ($j('body').hasClass('safety-training-detail-page') || $j('body').hasClass('equipment-tools-detail-page')){
			showMediaContact(0);
		}
		//
		if ($j('body').hasClass('services-detail-page')){
			showMediaContact(0);
		}
		//
		if ($j('body').hasClass('specialty-solutions-page')){
			showMediaContact();
		}
		//
		if ($j('body').hasClass('solution-detail-page')){
			showMediaContact();
		}
		//
		if ($j('body').hasClass('events') && !$j('body').hasClass('investor-relations') ){
			showMediaContact();
		}
		//
		if ($j('body').hasClass('tdh-calculator-page') && $j('body').hasClass('tdh-calculator') ){
			showMediaContact();
		}
		//
		if ($j('body').hasClass('faq-landing') ){
			buildFaqsLandingPage();
		}
		//
		if ($j('body').hasClass('faqs') && !$j('body').hasClass('safety-training-detail-page')  ){
			buildFaqsPage();
		}
		//
		if ($j('body').hasClass('pressrelease-detail') ){
			if (!$j('body').hasClass('investor-relations') && !$j('body').hasClass('events') && !$j('body').hasClass('investor-events') && !$j('body').hasClass('pressrelease-detail') && !$j('body').hasClass('detail') ){
				buidlPressDetail();
			}
		}
		if ($j('body').hasClass('investor-relations') && $j('body').hasClass('press-releases') && $j('body').hasClass('pressrelease-detail') && $j('body').hasClass('detail') ){
			buidlPressDetail();
		}
		if ($j('body').hasClass('press-room') && $j('body').hasClass('pressrelease-detail') && $j('body').hasClass('detail') ){
			buidlPressDetail();
		}
		//
		if ($j('body').hasClass('event-detail') ){
			buildEventDetail();
		}
		if ($j('body').hasClass('investor-relations') && $j('body').hasClass('events') && $j('body').hasClass('investor-events') && $j('body').hasClass('pressrelease-detail') && $j('body').hasClass('detail') ){
			buildEventDetail();
		}
		//
		//
		// look for sidebar specialty form and apply event handling
		if( $j('#specialty-form-container').hasClass('specialtyFormContainer') ){
			sidebarSpecialityForm();
		}
		//
		//
		if ($j('body').hasClass('campaign-registration') ){
			if($j('body').hasClass('get-what-you-need-skid-steer-01')){
				appendFooterLinks();
				return;
			}
			if ($j('body').hasClass('product-category-detail')) {
				campaignRegistrationPage(true);
				if ($j(".campaign-detail-rates-title").length == 0) {
					$j(".campaign-detail-rates").hide();
				}
			} else{
				campaignRegistrationPage();
			}
		}

		//
		// add the new help menu
		//
		//'[class*="answerbox"]'
		// catalog-category-view
		if ($j('body').is('[class*="catalog-category-view"]') || $j('body').is('[class*="categorypath-equipment-tools"]') || $j('body').is('[class*="catalogsearch-result-index"]')){
		//if ($j('body').hasClass('category-equipment-tools')){
                if (navigator.userAgent.match(/Mobile/i)) {

                        buildCatalogHelpMenu();
                }
                }


		appendFooterLinks();
		assignFooterPhoneInterface();
		//
		hideAddressBar();
		//
	});
	//
}(jQuery,this,document));
