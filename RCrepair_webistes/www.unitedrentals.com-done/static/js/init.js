var $j = jQuery.noConflict();

var CURRENT_LANGUAGE = 'en'; // global variable indicating which language we're in; use for localizing js strings and for correct paths.  Default to 'en', set in language switcher code, below

/**
 * @method
 * @param {Object} $j       jQuery namespace (noConflict) object
 * @param {Object} window
 * @param {Object} document
 * @returns {undefined}
 */
(function($j, window, document){
	var 
	mobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase())),
	marqueePagerEventFlag = false,
	timeoutOverlays = 100,
	smallDevice = false; //initilized onload

        /**
         * Determine if the responsive styles have been applied by checking visibilty of resp-logo element.
         * @returns {Boolean}
         */
	function isHandheld() {		
		return ($j('#resp-logo').is(':visible')); 				
	}
	/*if($.browser.msie && $.browser.version == '7.0'){
		$('a[href$="pdf"]').append('<span class="after"><img src="../images/ico-pdf.png"/*tpa=http://www.unitedrentals.com/static/images/ico-pdf.png*/></span>');	
		$('a.nopdfico span.after').remove();
	}*/

        /**
         * Tabs on FAQs page
         * @returns {undefined}
         */
	function tabs(){
		if ( typeof($j.fn.tabs) == 'function') {
			$j(".tabbed-panel").tabs();
		}
	}

        /**
         * Test for IE7, adjust z-index values 
         * @returns {undefined}
         */
	function ieLayers(){
		engine = null;
		if (window.navigator.appName == "Microsoft Internet Explorer")
		{
		   if (document.documentMode) // IE8 or later
		      engine = document.documentMode;
		   else // IE 5-7
		   {
		      engine = 5; // Assume quirks mode unless proven otherwise
		      if (document.compatMode)
		      {
		         if (document.compatMode == "CSS1Compat")
		            engine = 7; // standards mode
		      }
		   }
		   if (engine === 7){
				var zIndexNumber = 220;
				$j('#header ul, #header div, .row').each(function() {
					$j(this).css('zIndex', zIndexNumber);
					zIndexNumber -= 10;
				});
			}	
		}
	}

        /**
         * Full-bleed marquee, resize image
         * @returns {undefined}
         */
	function marquee(){
		
		if ($j(".marquee-container.fullbleed").length > 0) {
			function sizeFullbleedMarquee() {

				$j(".fullbleed .marquee").each( function(i,e) {
					
					var isResponsive = ($j(window).width() < 700);
					
					if ($j("body").width() > 960 ) {						
						$j(e).width($j("body").width());
						ratio = $j("body").width()/$j(".marquee-image img",e).height();					
						$j(".marquee-image img",e).width($j("body").width());
						$j($j(".marquee-image img",e)).height($j("body").width()/ratio);
					}
					//
					if ($j("body").width() < 1150 && !isResponsive) {
						$j(".marquee-container .prev").hide();
					} else {
						$j(".marquee-container .prev").show();
					}
				});
				$j(".marquee-container.fullbleed").removeClass("loading");
			}
			$j(window).on("load", sizeFullbleedMarquee);
			$j(window).on("resize", sizeFullbleedMarquee);
		}
	}

        /**
         * Rotating marquees
         * @returns {undefined}
         */
	function marqueeRotate() { 
		if ( $j(".marquee-container").length > 0 && $j(".marquee").length > 1 && typeof($j.fn.iosSlider) == 'function') {
			
			//
			function markCurrentSlide(args) {
				$j(args.settings.navSlideSelector).removeClass('active');
				$j(args.settings.navSlideSelector + ":eq(" + args.currentSlideNumber + ")").addClass('active');
			}; 
			
			// 
			function stopAutoSlider() {		
				var args = $j('.marquee-container').data('iosslider');
				if (args.settings.autoSlide) {
				    args.settings.autoSlide=false;
				}
			};
			
			var auto = ($j(".marquee-container.autoslide").length > 0 );			
			// bind click events to turn off auto rotation after first click within the marquee area;		
			if (auto) {				
				$j(".marquee-container").click(stopAutoSlider);
				//bind these as well otherwise it jumps one extra slide before stopping
				$j(".marquee-nav li a").click(stopAutoSlider); 
				$j(".prev").click(stopAutoSlider); 
				$j(".next").click(stopAutoSlider);
			};
				
			$j(window).on("load", function() {		
				   var auto = ($j(".marquee-container.autoslide").length > 0 );	
				   $j(".marquee-container").iosSlider({
						snapToChildren: true,
						navPrevSelector: ".prev",
						navNextSelector: ".next",
						infiniteSlider: true,
						navSlideSelector: ".marquee-nav li a",
						onSliderLoaded: markCurrentSlide,
						onSlideChange: markCurrentSlide,
						autoSlide: auto,
						autoSlideTimer: 6000
					});
				   // by default slider stops when the user hovers over slider area - removing this functionality
				   if (auto)
					   $j(".marquee-container").unbind('mouseenter');
				   
			});
		}
		/*if ( $(".marquee-wrapper").length > 0 && $(".marquee").length > 1 && typeof($.fn.cycle) == 'function') {
			$(".marquee-wrapper")
				//.after('<div class="marquee-nav"></div>')
				.cycle({
					speed: 2000,
					fx: 'scrollHorz',
					fastOnEvent: true,
					containerResize: false,
					pager: '.marquee-nav',
					width: $("body").width(),
					pagerAnchorBuilder: function(idx, slide) { 
						// return selector string for existing anchor 
						return '.marquee-nav a:eq(' + idx + ')'; 
					},
					onPagerEvent: function(zeroBasedSlideIndex, slideElement) {
						marqueePagerEventFlag = true;
					},
					after: function(currSlideElement, nextSlideElement, options, forwardFlag) {
						if(marqueePagerEventFlag) {
							$(".marquee-wrapper").cycle('pause');
						}
					}
				});
		}*/
	}
	
        /**
         * @property {Object} vimeo_videos This object will hold all the loaded videos
         */
	vimeo_videos = {};
	
        /**
         * Callback for Vimeo
         * @param {type} video
         * @returns {undefined}
         */
	function embedVideo(video) {
		if (typeof(vimeo_videos['video_'+vimeo_id]) == 'undefined') {
			vimeo_videos['video_' + video.video_id] = video;
		}
		$j("#overlay_video").html(video.html);
	}
		
        /**
         * Overlay for marquee video
         * @returns {undefined}
         */
	function marqueeOverlay(){
		if ( typeof($j.fn.colorbox) == 'function') {
			var video_id;
			var video_url;
			var video_url_local;
			var video_width;
			var video_height;
			var triedLocal = false;
			
			$j(".play-video").colorbox({
				innerWidth: video_width,
				innerHeight: video_height,
				scrolling: false,
				html: function() {
					// Find the containing element of the video markup.
					// It's necessary to try some different searches depending on
					// what kind of module we're dealing with.
					if ($j(this).parents(".video-wrapper, .marquee, .detail-page-marquee, .box").find(".video-container").length > 0) {
						video = $j(this).parents(".video-wrapper, .marquee, .detail-page-marquee, .box").find(".video-container");
					} else {
						return false;
					}
					
					// Grab the data from the "A" element
					video_id = $j(".pimcore_video_flowplayer",video).attr("id");
					if (!isHandheld()) {
						// Desktop version
						video_url = $j(".pimcore_video_flowplayer",video).attr("href");
						video_url_local = $j(".pimcore_video_flowplayer",video).data("localhref");
						video_width = $j(".pimcore_video_flowplayer",video).width();
						video_height = $j(".pimcore_video_flowplayer",video).height();
					} else {
						// Mobile version
						
						video_url = $j(".pimcore_video_flowplayer",video).data("mobilehref");
						video_url_local = $j(".pimcore_video_flowplayer",video).data("mobilelocalhref");
						video_width = $j(".pimcore_video_flowplayer",video).data("mobilewidth");
						video_height = $j(".pimcore_video_flowplayer",video).data("mobileheight");
						
						
						// resize video overlay to fit the screen,
						// make it a bit larger for the landscape view
						
						if ($j(document).width() > 400 ) {
							video_width = 400;
							video_height = 225;
						}
						else
						{							
							video_width = 280;
							video_height = 160;
						}
						
						// this script was meant to scale video overlay responsively
						// works fine in emulator and android, but breaks the marquee on iphone....
						// thus replaced by the script above
						
						/*
						var ratio = video_width/video_height,
							doc_width = $j(document).width();
						

						if (doc_width > 400 ) {
							video_width = doc_width * 0.75;
						}
						else {
							video_width  = doc_width * 0.9;
						}
							
						video_height = video_width / ratio;
						*/
					}
					// Create the element that we'll inject the video HTML into
					return $j("<div></div>").attr("id","overlay_video").css({width: video_width, height: video_height});
				},
				onComplete: function() {
					// First we try using Vimeo's oEmbed API
					
					// Extract the video's ID from the URL
					a = document.createElement('a');
					a.href = video_url;
					vimeo_id = a.pathname.split('/').pop().split('.')[0];
					
					if (typeof(vimeo_videos['video_'+vimeo_id]) == 'undefined') {
						// oEmbed API endpoint
						endpoint = "https://vimeo.com/api/oembed.json?url=https%3A//vimeo.com/";
					
						// Settings for video player
						params = {
							maxheight: video_height,
							maxwidth: video_width,
							byline: false,
							title: false,
							portrait: false,
							color: "cc0000",
							autoplay: true,
							callback: "urinit.embedVideo"
						}
					
						// Create embed script if it doesn't already exist
						if ($j("#vimeo_" + vimeo_id).length == 0) {
							s = document.createElement('script');
							s.setAttribute('type','text/javascript');
							s.setAttribute('id', 'vimeo_' + vimeo_id);
							s.setAttribute('src', endpoint + vimeo_id + "?" + $j.param(params));
							document.getElementsByTagName('head').item(0).appendChild(s);
						}
					} else {
						urinit.embedVideo(vimeo_videos['video_'+vimeo_id]);
					}
					
					setTimeout(function() {
						if (typeof(vimeo_videos['video_'+vimeo_id]) == 'undefined') {
							// If oEmbed fails, we try Flowplayer with an external Vimeo URI
							flowplayer("overlay_video", { src: "../swf/flowplayer.commercial-3.2.14.swf"/*tpa=http://www.unitedrentals.com/static/swf/flowplayer.commercial-3.2.14.swf*/ },
							{
								key: '#$b1057c8a0d21633dbed',
								width: video_width,
								height: video_height,
								clip: {
									scaling: 'orig',
									autoPlay: true,
									autoBuffering: true,
									url: video_url
								},
								onError: function(errorCode, errorMessage) {
									// If we can't connect to Vimeo, use the local version
									if(!triedLocal) {
										triedLocal = true;
										$j("#overlay_video object").remove();
										flowplayer("overlay_video", { src: "../swf/flowplayer.commercial-3.2.14.swf"/*tpa=http://www.unitedrentals.com/static/swf/flowplayer.commercial-3.2.14.swf*/ },
										{
											key: '#$b1057c8a0d21633dbed',
											width: video_width,
											height: video_height,
											clip: {
												scaling: 'orig',
												autoPlay: true,
												autoBuffering: true,
												url: video_url_local
											},
											onError: function(errorCode, errorMessage) {
												$j(".play-video").colorbox.close();
												alert('The requested video will not load.');
											}
										}).ipad();
									} else {
										$j(".play-video").colorbox.close();
										alert('The requested video will not load.');
									}
								}
							}).ipad();
						}
					}, 1000);
				},
				onCleanup: function() {
					triedLocal = false;
					$j("#colorbox object").remove();
				}
				});
		}
	}
		
        /**
         * Hover for marquee video button
         * @returns {undefined}
         */
	function marqueeHover(){
		if ($j(".play-video").length > 0 && !( $j.browser.msie && parseInt($j.browser.version) < 9 ) ) {
			$j(".play-video .play-btn")
				.css("opacity", 0.7)
				.hover(
					function() {
						$j(this).fadeTo('fast',1);
					},
					function() {
						$j(this).fadeTo('fast',0.7);
					}
				);
		}
	}	

        /**
         * HTML5 placeholder attribute cross-browser fix
         * @returns {undefined}
         */
	function placeholder(){
		$j('input, textarea').placeholder();
	}
	
        /**
         * For row with dividers, even out heights
         * @returns {undefined}
         */
	function equalizeHeightsInRow() {
		// If a row has dividers between boxes, we need to even out the height (not in Edit Mode)
		 if ($j("#pimcore_body", parent.document).length == 0) {
		 	var divider_height = 0;
		 	$j(".row.has-dividers").each( function(n,row) {
		 		$j("> .box, > .col", row)
		 			.each( function(i,box) {
		 				if ( $j(box).height() > divider_height) {
		 					divider_height = $j(box).height();
		 				}
		 			})
		 			.each( function(i,box) {
		 				$j(box).height(divider_height);
		 				$j(box).prev("css3-container").find("shape").height(divider_height);
		 			});
		 		divider_height = 0;
		 	});
		 }
	}

        /**
         * How can we help you menu
         * @returns {undefined}
         */
	function helpMenu(){
		if ($j(".can-we-help-menu").length > 0) {
			if(!mobile) {
				$j(".can-we-help-menu").on("mouseover mouseout", function(e) {
					$j(this).toggleClass('hover');
					$j(".can-we-help-menu .open-arrow").text($j(".can-we-help-menu").hasClass('hover') ? "U" : "V");
				});
			} else {
				$j(".can-we-help-menu").bind("click", function(e) {
					$j(this).toggleClass('hover');
					$j(".open-arrow", this).text($j(this).hasClass('hover') ? "U" : "V");
					if (e.target = $j("h3 > a", this)) {
						e.preventDefault();
					}
					e.stopPropagation();
				});
				$j(".can-we-help-menu ul").bind('click', function(e) {
					e.stopPropagation();
				});
			}
		}
	}

        /**
         * function for overlay menus. takes one parameter: el=DOM element to target
         * @param {type} el
         * @returns {undefined}
         */
	function hoverMenu(el){

		// desktop devices - use hover/click events
		if(!mobile) {
			$j(el).hoverIntent({
				timeout: timeoutOverlays,
				over: function() {
					$j(this).addClass('hover');
				},
				out: function() {
					$j(this).removeClass('hover');
				}
			});
		} else {
			if ($j(el + " > a").length > 0 ) {
				$j(el + " > a").each( function(i, elem) {
					$j(elem).bind('click', function(e) { 
						$j(".hover").not($j(elem).parent()).removeClass('hover');
						$j(elem).parent().toggleClass('hover');
						e.preventDefault();
						e.stopPropagation();
					});
				});
			}
		}
	}
//	
	function navigationHover() {
		if (! (isHandheld() ) ) {
			hoverMenu('#utilnav .customer-support');
			hoverMenu('.social-media-wrapper');
			hoverMenu('#utilnav .access-urdata');
			hoverMenu('#utilnav .top-cart');
			hoverMenu('#utilnav .find-location');	
			hoverMenu('ul.navigation li.menu-item');
			hoverMenu('.subnav ul li.category');
			hoverMenu('.products-list li.item');
		}
	
	
		if (mobile) {
			/*
			$j("#wrapper").attr("onclick","void();").bind('click', function() {
				$j(".overlay").parents(".hover").removeClass("hover");
				
				if ($j(".can-we-help-menu").length > 0) {
					$j("#wrapper").bind('click', function() {
						$j(".can-we-help-menu").removeClass('hover');
						$j(".can-we-help-menu .open-arrow").text("V");
					});
				}
			});
			$j(".overlay").bind('click', function(e) {
				e.stopPropagation();
			});
			*/
		}
		
	}
        /**
         * Convert trade-mark code
         * @returns {undefined}
         */
	function addRegMark(){
        $j('a, h1, h2, h3, p, span').each( function(i,e) {
			html = "";
			if ($j(e).text().match(/\xAE/)) {
				html += $j(e).html($j(e).html().replace(/\xAE/, "<sup>&reg;</sup>"));
			}
			if ($j(e).text().match(/\u2122/)) {
				html += $j(e).html($j(e).html().replace(/\u2122/, "<sup>&trade;</sup>"));
			}
			return html;
        });
		if ($j.browser.msie && $j.browser.version < 8) {
			setTimeout('$j(".before+.before").remove()',100);
		}
	}
	
        /**
         * Function to launch BoldChat
         * @param {type} chatURL
         * @returns {undefined}
         */
	function launchChatWindow(chatURL) {
		specs = 'toolbar=0,scrollbars=1,location=0,status=0,menubar=0,resizable=1,width=470,height=400';
		if (typeof pageTracker == 'object') {
			window.open(pageTracker._getLinkerUrl(chatURL), 'Chat7519131872756685375', specs);
		} else {
			window.open(chatURL, 'Chat7519131872756685375', specs);
		}
	}
	
        /**
         * 
         * @returns {undefined}
         */
	function bindChatLinks() {
		chatURL = 'https://livechat.boldchat.com/aid/937829174591224572/bc.chat?resize=true&cwdid=7303295104565584067&url='+escape(document.location.href);
		$j('.live-chat').click(function(e) {
			launchChatWindow(chatURL);
			e.preventDefault();
		});
	}
	
	// 
	function setResponsiveImages() {

		for ( var i = 0, imgs = document.getElementsByTagName( "img" ), il = imgs.length; i < il; i++){
			var img	= imgs[i],
				full	= img.getAttribute( "data-fullsrc" ),
				fullwidth = img.getAttribute( "data-fullwidth" );
				fullheight = img.getAttribute( "data-fullheight" );

				if( full ){
					img.src = full;
				}
				if (fullwidth) {
					img.width = fullwidth;
				}
				if (fullheight) {
					img.height = fullheight;
				}
			}		
	}
			
	// our class constructor - empty for now
	function UrInit(){
	}

	// our public API
	UrInit.prototype = {
	    tabs 	     : tabs,
	    placeholder  : placeholder,
	    help         : helpMenu,
	    mar          : marquee,
	    marOver      : marqueeOverlay,
	    marRotate    : marqueeRotate,
	    marHover     : marqueeHover,
	    ieZ          : ieLayers,
	    regMark      : addRegMark,
		embedVideo	: embedVideo,
		bindChatLinks: bindChatLinks,
		equalizeHeightsInRow : equalizeHeightsInRow,
		navigationHover : navigationHover
	}

	// our namespace
	window.urinit = new UrInit;

	$j(document).ready(function(){	
		if (mobile)
			$j("body").addClass("mobile");
				
		urinit.navigationHover();
		urinit.tabs();
		urinit.placeholder(); 
		urinit.help();
		urinit.mar();
		urinit.marOver();
		urinit.marRotate();
		urinit.marHover();
		urinit.ieZ();
		urinit.regMark();
		urinit.bindChatLinks();
		if (! (isHandheld() ) ) {
			setTimeout('urinit.equalizeHeightsInRow()', 200);
		}	
	});

	/* #################### client side language switcher start #################### */
	function writeLangSwitcherLinkText (tgt) {
		var current_url_path_str, curent_url_path_arr;
		//
		current_url_path_str = window.location.pathname;
		curent_url_path_arr = current_url_path_str.split( '/' );
		//
		if (current_url_path_str === '/') {
			$j(tgt).text('En Français');
		}
		if(curent_url_path_arr.length === 3 && curent_url_path_arr[1] === 'fr' && curent_url_path_arr[2] === ''){
			$j(tgt).text('In English');
			CURRENT_LANGUAGE = "fr";  // global language variable
		}
		if (curent_url_path_arr[1] === 'en' &&  curent_url_path_arr[2] != '') {
			$j(tgt).text('En Français');
		}
		if (curent_url_path_arr[1] === 'fr' &&  curent_url_path_arr[2] != '') {
			$j(tgt).text('In English');
			CURRENT_LANGUAGE = "fr";
		}
	}
	var lang_switcher_elem = $j(".lang-switcher");
	writeLangSwitcherLinkText(lang_switcher_elem);
	lang_switcher_elem.bind('click', function(evt){
		var current_url_path_str, curent_url_path_arr, new_location,
		setLinkText, switchLanguages, current_url_query_string;
		switchLanguages = function (lang) {
			var new_loc_str = lang;
			for (var i = 2; i < curent_url_path_arr.length; i++){
				new_loc_str += '/' + curent_url_path_arr[i];
			}
			window.location = new_loc_str + current_url_query_string;
		};
		evt.preventDefault();
		current_url_path_str = window.location.pathname;
		current_url_query_string = window.location.search;
	//		console.log(current_url_query_string);
		curent_url_path_arr = current_url_path_str.split( '/' );
		if (current_url_path_str === '/') {
			window.location = 'http://www.unitedrentals.com/fr/';
		}
		if(curent_url_path_arr.length === 3 && curent_url_path_arr[1] === 'fr' && curent_url_path_arr[2] === ''){
			window.location = '../../index.htm'/*tpa=http://www.unitedrentals.com/*/;
		}
		if (curent_url_path_arr[1] === 'en' &&  curent_url_path_arr[2] != '') {
			switchLanguages('/fr');
		}
		if (curent_url_path_arr[1] === 'fr' &&  curent_url_path_arr[2] != '') {
			switchLanguages('/en');
		}
	});
	/* ##################### client side language switcher end ##################### */
	
}(jQuery,this,document));
