// ON-LOAD EVENTS
$(window).load(function () {
    adjustSecondaryMessagingWidthAndHeight()
});

function adjustSecondaryMessagingWidthAndHeight() {
    var largestHeight = 0;
    var largestWidth = 0;
    var imageHeight = 0;
    var largestContainerHeight = 0;
    $(".template-message-item").each(function () {
        if ($(this).find("img").height() > imageHeight) {
            if ($(this).find("img").height() > imageHeight) {
                imageHeight = $(this).find("img").height();
            }
        }
        $(this).find("div.template-message-item-text").each(function () {
            if ($(this).height() > largestHeight) {
                largestHeight = $(this).height();
            }

            if ($(this).width() > largestWidth) {
                largestWidth = $(this).width();
            }
        });
    });



    $(".template-message-item-text").each(function () {
        $(this).width(largestWidth);
        $(this).height(largestHeight);
    });
    $(".template-message-item").each(function () {
        if ($(this).height() > largestContainerHeight) {
            largestContainerHeight = $(this).height();
        }
    });

    $(".template-message-item").each(function () {
        $(this).width(largestWidth);
        $(this).height(largestContainerHeight);
        var item = $(this).find(".template-message-item-text");

        if ($(this).find("img").height() == null) {
            var height = imageHeight + largestHeight;
            if (item != null) {
                $(this).width(largestWidth);
                $(item).height(height);
            }
        }

    });


}