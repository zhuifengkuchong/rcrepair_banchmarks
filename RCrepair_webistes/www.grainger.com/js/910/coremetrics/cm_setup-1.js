// This is a helper method for pulling the page id from the browser url.
// withParams, if given, will append any parameters to the page id.
var getPagePath = function(withParams) {
	
	var path = document.location.pathname;
	
	if (withParams && document.location.search) {
		path += document.location.search;
	}
	
	return (path);
};
