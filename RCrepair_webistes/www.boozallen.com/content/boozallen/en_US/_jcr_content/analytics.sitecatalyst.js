
	        CQ_Analytics.registerAfterCallback(function(options) {
	            if(!options.compatibility && $CQ.inArray( options.componentPath, CQ_Analytics.Sitecatalyst.frameworkComponents) < 0 )
	                return false;    // component not in framework, skip SC callback
	            CQ_Analytics.Sitecatalyst.saveEvars();
	            CQ_Analytics.Sitecatalyst.updateEvars(options);
	            CQ_Analytics.Sitecatalyst.updateLinkTrackVars();
	            return false;
	        }, 10);
	
	        CQ_Analytics.registerAfterCallback(function(options) {
	            if(!options.compatibility && $CQ.inArray( options.componentPath, CQ_Analytics.Sitecatalyst.frameworkComponents) < 0 )
	                return false;    // component not in framework, skip SC callback
	            s = s_gi("bahcomdev");
	            if (s.linkTrackVars == "None") {
	                s.linkTrackVars = "events";
	            } else {
	                s.linkTrackVars = s.linkTrackVars + ",events";
	            }
	            CQ_Analytics.Sitecatalyst.trackLink(options);
	            return false;
	        }, 100);
	
	
	        CQ_Analytics.registerAfterCallback(function(options) {
	            if(!options.compatibility && $CQ.inArray( options.componentPath, CQ_Analytics.Sitecatalyst.frameworkComponents) < 0 )
	                return false;    // component not in framework, skip SC callback
	            CQ_Analytics.Sitecatalyst.restoreEvars();
	            return false;
	        }, 200);
	
	        CQ_Analytics.adhocLinkTracking = "true";
	        
	
	
	        var s_account = "bahcomdev";
	        var s = s_gi(s_account);
	        s.fpCookieDomainPeriods = "2";
	        s.currencyCode= 'USD';
        s.trackInlineStats= true;
        s.linkTrackVars= 'prop11,prop12';
        s.charSet= 'UTF-8';
        s.linkLeaveQueryString= false;
        s.linkExternalFilters= '';
        s.linkTrackEvents= 'event3';
        s.trackExternalLinks= true;
        s.linkDownloadFileTypes= 'exe,zip,wav,mp3,mov,mpg,avi,wmv,doc,pdf,xls';
        s.linkInternalFilters= 'javascript:,'+window.location.hostname;
        s.trackDownloadLinks= true;
        
        s.visitorNamespace = "boozallenhamilton";
        s.trackingServer = "http://www.boozallen.com/content/boozallen/en_US/_jcr_content/boozallenhamilton.122.2o7.net";
        s.trackingServerSecure = "http://www.boozallen.com/content/boozallen/en_US/_jcr_content/boozallenhamilton.122.2o7.net";
        
        
/* Plugin Config */
s.usePlugins=true;
function s_doPlugins(s) {
	/*Populate Page Name*/
    //if ((!window.s.pageType) && (!window.s.pageName || s.pageName==""))
    //s.pageName=s.getPageName();

	/*get campaign code */
	s.eVar1=s.getQueryParam('utm_campaign');
	s.eVar2=s.getQueryParam('utm_source'); 
	s.eVar3=s.getQueryParam('utm_medium'); 
	s.eVar4=s.getQueryParam('utm_content'); 
	s.eVar5=s.getQueryParam('utm_term'); 

	s.prop1 = s.getAndPersistValue(s.eVar1, 's_ev1_p', 0);// Copy all evars to prop for traffic variables
    s.prop2 = s.getAndPersistValue(s.eVar2, 's_ev2_p', 0);
    s.prop3 = s.getAndPersistValue(s.eVar3, 's_ev3_p', 0);
    s.prop4 = s.getAndPersistValue(s.eVar4, 's_ev4_p', 0);
	s.prop5 = s.getAndPersistValue(s.eVar5, 's_ev5_p', 0);

    s.prop7=s.getNewRepeat()
/*
	s.prop21=s.getQueryParam('sco')
	s.prop22=s.getQueryParam('scr') 
*/
    s.campaign=s.getQueryParam('cid');
    s.eVar6=s.getQueryParam('rcid');
    s.eVar7=s.getQueryParam('icid');


    /************* LINK TRACKING *************/
    /*s.hbx_lt = "auto"
    s.setupLinkTrack("prop10,prop11,prop12", "SC_LINKS");*/
}
s.doPlugins=s_doPlugins;
$('#Print a').click(function() {
s.linkTrackEvents='event3';
s.events='event3';
s.tl(this,'o','Print Button');

});