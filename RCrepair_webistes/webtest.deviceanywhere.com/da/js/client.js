/*	The options contains the default configuration for this object. They may be overridden in
	various ways, specified below.

	When the page unloads we save the current options into offline storage.
	When a page loads, we read the options from offline storage and apply them first.

	The following objects will be merged in order, so that any option specified in step 1
	will be overridden in step 2, which in turn is overridden in step 3:

	1)	Read options from offline storage and if found, merge into OptionManager.options

	2)	You can add some javascript to the page, immediately after the include for this file, i.e:

		<script src='client.js'/*tpa=http://webtest.deviceanywhere.com/da/js/client.js*/></script>
		<script>
			kdaWebtest.setOptions ({promptMcd: false, mcd: 123});
		</script>

	3)	As the query string param 'daConfig' on the url to the page containing this client.js file.
		The param value will be used to look up an object in the 'configs' object below:

		http://webtest.deviceanywhere.com/da/views/timbuk2/home.html?daConfig=dev

		This will merge the OptionManager.configs.dev object into the OptionManager.options object.

	4)	You can set the MCD specifically by using the daMcd query param. This will override any other
		MCD that was set from storage or wherever:

 		http://webtest.deviceanywhere.com/da/views/timbuk2/home.html?daMcd=1234

	See OptionManager.initialize below to see the code where this all happens.
*/

// The 'global' will be the window object in the browser, or the global object in Node.js

(function (global) {
	'use strict';

	// if client.js gets included more than once, just return
	if (global.kdaWebtest)
		return;

	/*******************************************************************************************************************
	 * OptionsManager object.
	 ******************************************************************************************************************/
	var OptionManager = function () {

		this.options	=	{	pauseOnBlur:	false,	// Stop requesting operations when page loses focus
								promptMcd: ('false'.toLowerCase() === 'true'),
								logger: {
									level:	'debug',
									target:	'console'
								},
								server: {
									host:		'http://webtest.deviceanywhere.com/da/js/webtest.deviceanywhere.com',
									port:		'80',
									sslPort:	'443'
								},
								useWebsockets:	true,
								mcd:			false
							},
		this.overrides	=	{},

		this.configs	=	{	qa: {
									pauseOnBlur:	false,
									promptMcd:		false,
									logger: {
										level:	'debug',
										target:	'server'
									},
									server:	{
										host:		'http://webtest.deviceanywhere.com/da/js/10.120.100.186',
										port:		80,
										sslPort:	443
									}
								},
								dev: {
									pauseOnBlur:	false,
									promptMcd:		true,
									mcd:			false,
									logger: {
										level:	'debug',
										target:	'console'
									},
									server:	{
										host:		'localhost',
										port:		31592,
										sslPort:	443
									}
								},
								remote: {
									pauseOnBlur:	false,
									promptMcd:		false,
									mcd:			false,
									logger: {
										level:	'debug',
										target:	'console'
									},
									server:	{
										host:		'http://webtest.deviceanywhere.com/da/js/swdev-amathison.win.keynote.com',
										port:		31592,
										sslPort:	443
									}
								}
							},
		this.queryParams=	false,
		this.mcdParam	=	false	// will only be set if the query param daMcd=XXX is passed in.
									// This will override any other mcd set in any other way.
	}

	OptionManager.prototype = {
		initialized: false,

		initialize: function () {
			if (this.initialized)
				return;
			this.initialized = true;

			logger.debug ('OptionManager.initialize(): Begin');

			// load overrides from storage using the global StorageManager
			var storage				= storageManager.read();

			var overridesConfig		= this.getOverridesFromConfig ();
			var overridesUrl		= this.getOverridesFromUrl ();

			if (storage && storage.overrides) {
				mixin (this.overrides, storage.overrides);
			}

			mixin (this.overrides,	overridesConfig);
			mixin (this.overrides,	overridesUrl);
			mixin (this.options,	this.overrides);

			if (storage.recording) recorder.recordOn();

			logger.debug ('OptionManager.initialize(): End');
		},
		setMcd: function (mcd) {
			logger.debug (function () {"OptionManager.setMcd(" + mcd +")"});

			this.options.mcd = this.overrides.mcd = mcd;
			saveOptions();
		},
		setOverrides: function (overrides) {
			logger.debug ("OptionManager.setOverrides(): Begin");

			if (overrides) {
				mixin (this.overrides, overrides);
				mixin (this.options, this.overrides);
//				logger.debug ("OptionManager.setOverrides(): this.options = " + stringify (this.options));
			}
			if (this.mcdParam) { // must do this last, as it overrides any of the options from the server.
				this.setMcd (this.mcdParam);
				logger.debug ("OptionManager.setOverrides(): MCD from query param: " + this.mcdParam);
			}
			if (!this.options.mcd && global.kdaWebtestMcd) { // still not set, look at window.kdaWebtestMcd
				this.setMcd (global.kdaWebtestMcd);
				logger.debug ("OptionManager.setOverrides(): MCD from window.kdaWebtestMcd:  " + global.kdaWebtestMcd);

			}
			logger.debug ("OptionManager.setOverrides(): End");
		},

		getOverridesFromUrl: function () { // private method
			var queryParam	= this.getQueryParams()['daScript'];
			var overrides	= {};

			if (queryParam) {
				try { overrides = global.JSON.parse (queryParam); } catch (ex) {}
			}
			queryParam	= this.getQueryParams()['daMcd'];
			if (queryParam) {
				try {
					this.mcdParam = overrides.mcd = parseInt (queryParam);
				} catch (ex) {}
			}
			return overrides;
		},

		getOverridesFromConfig: function () { // private method
			var queryParam = this.getQueryParams()['daConfig'];
			if (!queryParam) return {};

			return this.configs [queryParam] || {};
		},

		// split query params into associative array
		getQueryParams: function () { // private method
			if (this.queryParams) {
				return this.queryParams;
			}
			var url = window.location.href;
			this.queryParams = {};

			var halves = url.split("?");

			if (halves.length == 2) {
				var pairs = halves[1].split("&");
				for (var i = 0; i < pairs.length; i++) {
					var namevalue = pairs[i].split("=");
					this.queryParams[namevalue[0]] = namevalue[1];
				}
			}
			return this.queryParams;
		},
		useWebsockets:	function (newValue) {return this.getOrSetOption ('useWebsockets',	newValue)},

		getOrSetOption: function (optionName, newValue) {// returns previous value. Just acts as a getter if newValue is undefined
			var oldValue = this.options[optionName];
			if (newValue !== undefined) this.options[optionName] = newValue;
			return oldValue;
		}
	}

	/*******************************************************************************************************************
	 * Custom Event Dispatcher
	 ******************************************************************************************************************/
	var EventDispatcher = function () {}
	EventDispatcher.prototype = {
		event: {
			serverReady:	'serverReady',
			clientReady:	'clientReady',
			mcdSet:			'mcdSet'
		},
		fireServerReady: function (detail, node) {
			this.fire (this.createCustomEvent (this.event.serverReady, detail), node);
		},
		fireClientReady: function (detail, node) {
			this.fire (this.createCustomEvent (this.event.clientReady, detail), node);
		},

		fireClick: function (node) {
			var event = document.createEvent("MouseEvents");
			event.initMouseEvent ('click', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
			return this.fire (event, node);
		},
		fireChange: function (node) {
			this.fire (this.createEvent ('change', null), node);
		},
		fireInput: function (node) {
			this.fire (this.createEvent ('input', null), node);
		},
		fireFocus: function (node) {
			this.fire (this.createEvent ('focus', null), node);
		},
		fireKeyDown: function (node, key) {
			this.fireKeyEvent ("keydown", node, key);
		},
		fireKeyPress: function (node, key) {
			this.fireKeyEvent ("keypress", node, key);
		},
		fireKeyUp: function (node, key) {
			this.fireKeyEvent ("keyup", node, key);
		},
		fireKeyEvent: function ( eventName, node, key) {
			var event = document.createEvent('KeyboardEvent');
			var charCode = key.charCodeAt (0);

			event.initKeyboardEvent(eventName, true, true, document.defaultView, key, charCode);

			return this.fire (event, node);

		},
		fire: function (event, node) {
			(node || document.body).dispatchEvent (event);
		},
		createEvent: function (eventName, detail) {
			var event = document.createEvent("Event");
			event.initEvent (eventName, true, true);
			event.detail = detail;
			return event;
		},
		createMouseEvent: function (eventName, detail) {
			var event = document.createEvent("Mouse");
			event.initMouseEvent (eventName, true, true, window, 1);
			event.detail = detail;
			return event;
		},
		createCustomEvent: function (eventName, detail) {
			if (global.CustomEvent) {
				return new global.CustomEvent (eventName, {	detail:		detail,
					bubbles:	true,
					cancelable:	false
				});
			} else {
				return this.createEvent (eventName, detail);
			}
		}


	}

	/*******************************************************************************************************************
	 * Custom Events and EventHandlers Object
	 ******************************************************************************************************************/
	var EventHandlers = function () {
		if (global.document) {
			global.document.addEventListener ("blur",	curry (this.onBlur,		this),	true);
		}
		if (!global.addEventListener) // if root is not a browser window object...
			return;

		global.addEventListener		("load",			curry (this.onLoad,			this),	false);
		global.addEventListener		("unload",			curry (this.onUnload,		this),	false);

		global.addEventListener		("pageshow",		curry (this.onPageShow,		this),	false);
		global.addEventListener		("pagehide",		curry (this.onPageHide,		this),	false);

		global.addEventListener		("message", 		curry (this.onMessage,		this),	false);
		global.addEventListener		("focus",			curry (this.onFocus,		this),	true);

		global.addEventListener		(eventDispatcher.event.serverReady,	curry (this.onServerReady,	this),	true);
		global.addEventListener		(eventDispatcher.event.clientReady,	curry (this.onClientReady,	this),	true);

		global.addEventListener		("mousedown", function (e) {
											console.log ("mousedown: " + e.target.tagName);
										},	true);
		global.addEventListener		("mouseup", function (e) {
											console.log ("mouseup: " + e.target.tagName);
										},	true);
		global.addEventListener		("click", function (e) {
											console.log ("click: " + e.target.tagName);
										},	true);


	}

	EventHandlers.prototype = {
		loadHandled: false,

		onPageShow: function (e) {
			if (this.loadHandled) return;
			this.loadHandled = true;

			logger.debug ('EventHandlers.onPageShow()');

			if (e.persisted) {
				global.location.reload (true);
			} else {
				execute();
			}
		},
		onPageHide: function (e) {
			logger.debug ('EventHandlers.onPageShow()');
			this.loadHandled = false;
		},
		onLoad: function (e) {
			if (this.loadHandled) return;
			this.loadHandled = true;

			logger.debug ('EventHandlers.onLoad()');
			execute();
		},
		onUnload: function (e) {
			this.loadHandled = false;
		},

		onServerReady: function (e) {
			logger.debug ("EventHandlers.onServerReady()");

			optionManager.setOverrides	(e.detail.overrides);
			logger.setConfig			(optionManager.options.logger);

			// if no mcd is set, and we are configured to prompt the user for one...
			if (!optionManager.options.mcd && optionManager.options.promptMcd) {
				showMcdPopup (function (mcd){
					if (mcd) optionManager.setMcd (mcd);
					eventDispatcher.fireClientReady (e.detail); // pass the details to the next event
				});

			} else{
				logger.debug ("EventHandlers.onServerReady(): Before fireClientReady");
				eventDispatcher.fireClientReady (e.detail); // pass the details to the next event
			}
		},
		onClientReady: function (e) { // these calls must happen in this particular order
			logger.debug ("EventHandlers.onClientReady()");

			serverFacade.setReady		();
			recorder.setRecord			(e.detail.recording);
		},

		onFocus: function (e) {

			if (e.eventPhase == 1) { //capturing phase
				if (e.target) {
					elFocus = e.target;
				}

			} else if (e.eventPhase == 2) { // at-target phase
				if (optionManager.options.pauseOnBlur && runtimeExecutor.isModePause()) {
					 runtimeExecutor.setModeRun();
				}
			}
		},

		onBlur: function (e) {
			if (e.eventPhase == 1 ) {//capturing phase

				if (elFocus == e.target) {
					elFocus = null;
				}
			}
		},
		/**
		 * postMessage events from the iframe. This could be in IFrameManager, but is not.
		 */
		onMessage: function (event) {
			logger.debug ("EventHandlers.onMessage(): Begin");

			try {
				var json = global.JSON.parse (event.data);
				logger.debug ("EventHandlers.onMessage(): Message: " + json.message);

				switch (json.message) {

					case 'operations':
						operationManager.addOperations (json.data);
						break;

					case 'serverReady':
						eventDispatcher.fireServerReady({	overrides:	json.data.overrides,
															recording:	!!
json.data.recording	});
						break;

					case 'mcd':
						setMcd (json.data, false);
						break;
				}
				logger.debug ("EventHandlers.onMessage(): End")
			} catch (e) {
				logger.error ("EventHandlers.onMessage(): Exception thrown: " + e.toString());
			}
		}

	}

	/*******************************************************************************************************************
	 * StorageManager definition
	 ******************************************************************************************************************/
	var StorageManager = function (storageProvider, key) {
		this.key = key;

		this.storageProvider = (storageProvider && typeof storageProvider !== 'undefined' ? storageProvider : false);
	}

	StorageManager.prototype = {

		read: function (){
			return {}
		},
		write: function (storage) {
			if (!this.storageProvider)
				return;

			try {
				// Post it to the iframe to save in cross-domain safe way.
				serverFacade.postStorage (storage);

			} catch (ex){
				logger.debug ('StorageProvider.write error: ' + ex.toString());
			}

		},
		clear: function () {
			try {
				// this doesn't really belong here, but it's easy.

				if (this.storageProvider) {
					this.storageProvider.removeItem (this.key);
					this.storageProvider.clear();
				}
				serverFacade.postStorage ({});

			} catch (ex) {
				var dummy = ex.toString();
			}
		}
	}

	/*******************************************************************************************************************
	 * IFrameManager object. Creates the iframe for communicating with the server.
	 * We don't implement receiveMessage here, but rather in the EventHandlers object
	 * which then calls into this object. We want to have all event handlers in one place.
	 ******************************************************************************************************************/
	var IFrameManager = function () {
		this.elIFrame = null;
	}

	IFrameManager.prototype = {
		initialized: false,

		initialize: function () {
			if (this.initialized)
				return;
			this.initialized = true;

			logger.debug ('IFrameManager.initialize(): Begin');

			var serverOptions	= optionManager.options.server;
			var protocol		= (serverOptions.host=='localhost' || window.location.protocol=='file:' ? 'http:' :
window.location.protocol);

			var iframeUrl = protocol + '//'	+ serverOptions.host;

			if (protocol === 'https:' && (Number (serverOptions.sslPort) != 443)) {
				iframeUrl += (':' + serverOptions.sslPort);

			} else if (protocol === 'http:' && (Number (serverOptions.port) != 80) ) {
				iframeUrl += (':' + serverOptions.port);
			}

			iframeUrl += '/da/iframe' + "?d=" + new Date().getTime();

			logger.debug ('IFrameManager.initialize(): IFrameUrl==' + iframeUrl);

			this.elIFrame		= document.createElement ("iframe");

			this.elIFrame.setAttribute ("class",	'kdaWebtestTag');
			this.elIFrame.setAttribute ("src",		iframeUrl);
			this.elIFrame.setAttribute ("width",	"350px");
			this.elIFrame.setAttribute ("height",	"300px");

			this.elIFrame.style.display				= "none";
			this.elIFrame.style.backgroundColor		= 'white';
			this.elIFrame.style.zIndex				= 1000000;
			this.elIFrame.style.height				= '300px';
			this.elIFrame.style.overflowY			= 'auto';

			document.body.appendChild (this.elIFrame);
		},
		showIFrame: function (doShow) {
			this.elIFrame.style.display = (doShow ? "block" : "none");
		},
		deleteIFrame: function () {
			document.body.removeChild (this.elIFrame);
		},
		postMessage: function (message, data) {
			// do a null check on data, not a !data because 'false' is a valid message to send
			if (data == null)
				return null;

			var payload = '{"message":"' + message + '","data":' + stringify(data) + '}';

			if (this.elIFrame && this.elIFrame.contentWindow) {
				this.elIFrame.contentWindow.postMessage (payload, "*");
			}
			return payload;
		}

	}
	/*******************************************************************************************************************
	 * ServerFacade object. Creates the iframe for communicating with the server, and provide a facade for posting
	 * messages to the server iframe. We don't implement receiveMessage here, but rather in the EventHandlers object
	 * which then calls into this facade. We want to have all event handlers in one place.
	 ******************************************************************************************************************/
	var ServerFacade = function () {
		this.ready			= false;
	}

	ServerFacade.prototype = {

		// call this when the iframe has indicated it is in a ready state.
		setReady: function () {
			this.ready	= true;

			this.postMcd	(optionManager.options.mcd);
			runtimeExecutor.printMode();

			logger.info ("Status: Ready");
		},

		postStorage: function (storage) {
			this.postMessage ('storage', storage);
		},
		postLogger: function (options) {
			this.postMessage ('logger', options);
		},
		postMcd: function (mcd) {
			if (mcd) this.postMessage ("mcd", mcd);
		},
		postInspect: function (node) {
			if (node) this.postMessage ("inspect", '"' + domHelper.generateXPath (node) + '"');
		},
		postRecording: function (recording) {
			if (recording) this.postMessage ("record", recording);
		},
		postOperation: function (operation) {
			if (operation) this.postMessage ("operation", operation);
		},
		postPage: function () {
			this.postMessage ("page", {dom: domHelper.stringifyDom()}, false);
		},
		postDom: function () {
			logger.debug ('ServerFacade.postDom(): Begin');
			this.postMessage ("dom", {dom: domHelper.stringifyDom()}, false);
			logger.debug ('ServerFacade.postDom(): End');
		},
		postMessage: function (message, data) {
			iframeManager.postMessage (message, data);
		}
	}

	/*******************************************************************************************************************
	 * Logger object. Sends debug (logging) messages to the server if logging == true. Also will send the same message
	 * to the browsers' console window if it exists and if console == true
	 ******************************************************************************************************************/
	var Logger = function () {
		if (!global.console) global.console = {log:function(){}};
	}

	Logger.prototype = {
		enums:	{	level:	{	none:	0,
								error:	1,
								warn:	2,
								info:	3,
								debug:	4
						},
					target:	{	none:	0,
								console:1,
								iframe:	2,
								server:	3
						}
		},
		config:	{	level:	'debug',
					target:	'console'
		},

		setLevel: function (level) {
			this.config.level = (this.enums.level [level] ? level : this.enums.level.none);
			this.onOptionsChanged();
		},
		setTarget: function (target) {
			this.config.target = (this.enums.target [target] ? target : this.enums.target.none);
			this.onOptionsChanged();
		},
		setConfig: function (config) {
			mixin (this.config, config);
			this.onOptionsChanged();
		},
		onOptionsChanged: function () {
			iframeManager.showIFrame (this.enums.target [this.config.target] === this.enums.target.iframe);
			serverFacade.postLogger (this.config);
			optionManager.setOverrides ({'logger': this.config})
			saveOptions();
		},
		getLevelNumber:	function ()	{return this.enums.level [this.config.level];},

		error:	function (message) {this.log ('error',	message);},
		warn:	function (message) {this.log ('warn',	message);},
		info:	function (message) {this.log ('info',	message);},
		debug:	function (message) {this.log ('debug',	message);},

		log: function (level, message) {
			// TODO: remove this call to console. For debugging only
			this.console (this.toStringMessage (message));
//			return;

			var curLevel = this.getLevelNumber();
			var tstLevel = this.enums.level [level];

			if (this.getLevelNumber() < this.enums.level [level])
				return;

			serverFacade.postMessage (level, '"' + this.toStringMessage (message) + '"');
		},
		console: function (message) {
			if (console && console.log) console.log (this.toStringMessage(message));
		},
		toStringMessage: function (message) {
			var typeofMessage	= typeof message;
			var results;

			if (message == null || typeofMessage === 'undefined') {
				results = '';

			} else if (typeofMessage === 'function') {
				try {
					results = message();
				} catch (e) {
					results = e.toString();
				}
				if (results == null || typeof results !== 'string')
					results = '';

			} else if (typeofMessage === 'object') {
				results = stringify (message);

			} else if (typeofMessage === 'string') {
				results = message;

			} else {
				results = message.toString();
			}
			return results;
		}
	}
	/*******************************************************************************************************************
	 * The Inspector object. Create the Canvas overlay and handle all inspection tasks
	 ******************************************************************************************************************/
	var Inspector = function () {
		this.canvas			= null;
	}

	Inspector.prototype = {
		initialized: false,

		initialize: function () {
			if (this.initialized)
				return;

			this.initialized = true;

			logger.debug ('Inspector.initialize()');

			this.canvas				= document.createElement ("canvas");

			this.canvas.setAttribute ("class",	'kdaWebtestTag');

			var oStyle				= this.canvas.style;

			oStyle.position			= 'absolute';
			oStyle.display			= 'none';
			oStyle.backgroundColor	= 'white';
			oStyle.opacity			= 0.7;
			oStyle.top				= 0;
			oStyle.left				= 0;
			oStyle.zIndex			= 1000000;

			document.body.appendChild (this.canvas);

			this.canvas.addEventListener	("click", curry (this.onCanvasClick, this), false);
			this.canvas.addEventListener	("touchend", curry (this.onCanvasTouch, this), false);

		},

		inspectOn: function () {
//			if (runtimeExecutor.isModeInspect()) return;

			runtimeExecutor.pushMode();			// save the current mode for restoration in inspectOff
			runtimeExecutor.setModeInspect();

			this.canvas.width	= Math.max (document.documentElement.offsetWidth,
											document.documentElement.scrollWidth,
											document.body.offsetWidth,
											document.body.scrollWidth);

			this.canvas.height	= Math.max (document.documentElement.offsetHeight,
											document.documentElement.scrollHeight,
											document.body.offsetHeight,
											document.body.scrollHeight);

			this.canvas.style.display = 'block';
		},

		inspectOff: function () {
			this.canvas.style.display = 'none';

			if (runtimeExecutor.isModeInspect())
				runtimeExecutor.popMode (); 	// restore the previous mode, or set to RUN if no previous mode.
		},

		inspectNodes: function (nodes) {
			var visibleNodes =	 domHelper.winnowNodes (nodes, function (node) {
				return domHelper.isNodeVisible (node, true);
			});
			if (visibleNodes.length === 0)
				return false;

			this.inspectOn();

			var context		= this.getCanvasContext();
			var self		= this;
			var isFirst		= true;

			domHelper.visitNodes (nodes, function (node) {
				if (isFirst) // if this is the first one, scroll into view.
					self.scrollIntoView (node);

				self.inspectNode (node, context);
				isFirst = false;
			});
			return true;
		},

		inspectNode: function (node, context) {
			var rect = node.getBoundingClientRect();

			var top 	= rect.top	+ window.pageYOffset;
			var left	= rect.left	+ window.pageXOffset;

			context.strokeRect (left, top, node.offsetWidth, node.offsetHeight);
		},

		scrollIntoView: function (node) {
			//node.scrollIntoView();
			window.scrollTo (0, node.offsetTop-2);
		},
		elementFromPoint: function (x, y) {
			this.canvas.style.display = 'none';
			var node = document.elementFromPoint (x,y);
			this.canvas.style.display = 'block';
			return node;
		},

		getCanvasContext: function () {
			var context	= this.canvas.getContext('2d');

			context.clearRect (0, 0, this.canvas.width, this.canvas.height);
			context.strokeStyle ='red';
			context.lineWidth	= 2;

			return context;
		},

		onCanvasClick: function (ev) {
		    if(this.recordingTouch)
		        return;
		    this.recordingClick = true;
		    this.handleCanvasClick(ev);
		},
		onCanvasTouch: function (ev) {
		    if(this.recordingClick) {
		        return;
		    }
		    this.recordingTouch = true;
		    this.handleCanvasTouch(ev);
	    },
        handleCanvasClick: function(ev)
        {
             var node = this.elementFromPoint (ev.clientX, ev.clientY);
             if (!node) return;

             this.inspectNode (node, this.getCanvasContext());
             serverFacade.postInspect (node);
        },

        handleCanvasTouch: function(ev)
        {
//	     logger.debug("KDA Commander touch x " + ev.changedTouches[0]);
//	     logger.debug("KDA Commander touch x " + ev.changedTouches[0].pageX	+ " y " + ev.changedTouches[0].pageY);
             var node = this.elementFromPoint (ev.changedTouches[0].pageX, ev.changedTouches[0].pageY);

             if (!node) return;

             this.inspectNode (node, this.getCanvasContext());
             serverFacade.postInspect (node);
        }




	}

	/*******************************************************************************************************************
	 * The Recorder object.
	 ******************************************************************************************************************/
	var Recorder = function () {

		this.handlers = {
			onClick:	curry (this.onClick, this),
			onChange:	curry (this.onChange, this),
			onOpen:		curry (this.onOpen, this),
			onClose:	curry (this.onClose, this),
			onError:	curry (this.onError, this),
			onTouch:	curry (this.onTouch, this),
			onClickGlobal:	curry (this.onClickGlobal, this)
		};
		this.websocket		= false;
	}

	Recorder.prototype = {

		recordOn: function () {
			if (runtimeExecutor.isModeRecord()) return true; // we're already recording, just return

			if (!optionManager.useWebsockets()) {
				logger.info ("Websocket support turned off in OptionManager.");

			} else if (!this.supportsWebSocket()) { // if websockets are not supported print a message and fail
				logger.warn ("WebSockets are NOT supported. Falling back to PostMessage.");

			} else {
				logger.info ("WebSockets ARE supported.");
			}
			this.openWebSocket();
			// remove all document listeners first.
			try {
				document.removeEventListener	("click",	this.handlers.onClickGlobal,	true); // capturing phase
			} catch(error1)
			{

			}
			try {
				document.removeEventListener	("touchend",	this.handlers.onTouch,	true); // capturing phase
			} catch(error1)
			{

			}

			document.removeEventListener	("change",	this.handlers.onChange,	false); // bubbling phase

			try {
				document.addEventListener	("touchend", this.handlers.onTouch, true); // capture touch
			} catch(err) {
				console.log("KDA Commander, no touch end method, use click");
			}

			try {
				document.addEventListener	("click", this.handlers.onClickGlobal, true); // capturing phase
			} catch(err2)
			{
					console.log("KDA COmmander, no click either");
			}

			document.addEventListener	("change", this.handlers.onChange, false);
			runtimeExecutor.setModeRecord();
			saveOptions();
			return true;
		},
		recordOff: function () {
			if (!runtimeExecutor.isModeRecord()) return true;

			runtimeExecutor.setModeRun();

			try {
				document.removeEventListener	("click",	this.handlers.onClickGlobal,	true); // capturing phase
			} catch(error1)
			{

			}
			try {
				document.removeEventListener	("touchend",	this.handlers.onTouch,	true); // capturing phase
			} catch(error1)
			{

			}

			document.removeEventListener	("change",	this.handlers.onChange,	false); // bubbling phase

			this.closeWebSocket ();

			saveOptions();
		},
		setRecord: function (on) {
			on ? this.recordOn() : this.recordOff();
		},
		supportsWebSocket: function () {
			return ((!!global.WebSocket) && optionManager.useWebsockets());
		},
		isWebSocketOpen: function () {
			return this.websocket && this.websocket.readyState == WebSocket.OPEN;
		},
		closeWebSocket: function () {
			if (!this.websocket) return;

			this.websocket.close();
			this.websocket = false;
		},

		openWebSocket: function () {
			// if websockets not supported or a socket is already open, just return
			if (!this.supportsWebSocket() || this.isWebSocketOpen())
				return;

			var url = 	"ws://" + optionManager.options.server.host + ":" + optionManager.options.server.port +
						"/da/ws/record?mcd=" + optionManager.options.mcd;

			logger.info ("Websocket URL: " + url);

			var self		= this;
			var websocket	= new WebSocket (url);

			websocket.onopen	= function (event) {
				self.websocket = websocket;
				self.onOpen (event);
			};
			websocket.onclose	= this.handlers.onClose;
			websocket.onerror	= this.handlers.onError;
		},
		onOpen: function (event) {
			logger.info ("Websocket.onOpen called");
		},
		onClose: function (event) {
			logger.info ("Websocket.onClose called");
			this.websocket = false;
		},
		onError: function (event) {
			logger.info ("Websocket.onError called");
			this.onClose (event);
		},

		onChange: function (event) {
			if (!runtimeExecutor.isModeRecord()) return;

			logger.info ("Recorder.onChange called");

			var node		= event.target;
			var recording	= null;

			if (domHelper.isInputCheckbox (node)) {
				recording = this.createRecording (event.target, 'setCheck', {value: node.checked});

			} else if (node) {
				var values = [];

				domHelper.getNodeValue (node, values);

				if (values.length > 0) {
					recording = this.createRecording (event.target, 'setValue', {value: values.toString()});
				}
			}
			if (recording) this.sendRecording (recording);
		},
		onTouch: function (event) {
			if(!this.recordingClick) {
				this.recordingTouch = true;
				this.onClick(event);
			}
			this.recordingClick = false;
		},
		onClickGlobal: function (event) {
			if(!this.recordingTouch) {
				this.recordingClick = true;
				this.onClick(event);
			}
			this.recordingTouch = false;
		},
		onClick: function (event) {							// just return if:
			this.eventCalled = true;
			if ((!runtimeExecutor.isModeRecord())			||	// not recording or
				(domHelper.isInputCheckbox (event.target))	||	// it's a checkbox or
				(domHelper.isInputRadio (event.target))		||	// it's a radio button or
				(event.eventPhase != 1)	) 	// not in capturing phase
				return;

			if (this.isWebSocketOpen ()) {
				try {
					this.handleClick (event);

				} catch (e) {
					optionManager.useWebsockets (false);
					this.handleClickPostMessage (event, true);
				}
			} else {
				this.handleClickPostMessage (event, true);
			}

		},
		cancelDefaultAction :function(e) {
			 var evt = e ? e:window.event;
			 //if (evt.preventDefault) evt.preventDefault();
			 evt.returnValue = false;
			 return false;
		},

		// Call the cancelDefaultAction() function
		// to prevent the default browser response:

		handleEvent:function(e) {
			 var evt = e ? e:window.event;

		 // event handler code goes here

		 return cancelDefaultAction(evt);
		},

		handleClick: function (event) {
			logger.debug('KDACommander: Entering click, WebSocket');
			var recording = this.createRecording (event.target, 'click');
			this.sendRecording (recording);
			//event.stopPropagation();
			//event.preventDefault();
			return false;

		},


		handleClickPostMessage: function (event, postToServer ) {
			logger.debug('KDACommander: Entering click, Post Message');
			if (postToServer) {
			logger.debug('KDACommander: Entering click, Post Message');

				var recording = this.createRecording (event.target, 'click');

				serverFacade.postRecording (recording);

				//event.stopPropagation();
				//event.preventDefault();

				delay (this.handleClickPostMessage, 500, this, event, false);
				return false;

			} else {
				document.removeEventListener	("click", this.handlers.onClick, true); // capturing phase
				domHelper.dispatchClickEvent	(event.target);
				document.addEventListener		("click", this.handlers.onClick, true); // capturing phase
			}
		},

		sendRecording: function (recording) {
			if (!recording)
				return;

			if (this.isWebSocketOpen()) {
				try {
					logger.info ("http://webtest.deviceanywhere.com/da/js/Before websocket.send");
					this.websocket.send (stringify (recording));
					logger.info ("http://webtest.deviceanywhere.com/da/js/After websocket.send");
				} catch (ex) {
					logger.error (ex.message);
				}
			} else {
				serverFacade.postRecording (recording);
			}
		},
		createRecording: function (node, type, params) {
			if (!node) return;

			var recording	= {};
			recording.type	= type || 'click';
			recording.url	= window.location.href;
			recording.xpath	= domHelper.generateXPath	(node);
			recording.html	= domHelper.stringifyDom	();

			if (params)	recording.params = params;

			return recording;
		}

	}

	/************************************************************************************************************
	 * Dynamically load xpath.js if no native implementation exists.
	 ***********************************************************************************************************/
	var XPathLoader = function (document) {
		this.ready = false;
		this.document = document;
	}

	XPathLoader.prototype = {
		initialized: false,

		initialize: function () {

			if (this.initialized)
				return;
			this.initialized = true;

			logger.debug ('XPathLoader.initialize()');

			if (this.hasSupport()) {
				logger.debug ('XPathLoader.hasSupport == true');
				this.ready = true;

			} else {
				var self = this;
				var url = "https://" + optionManager.options.server.host + ":" + optionManager.options.server.port +
							"/da/js/xpath.js";

				logger.debug ('XPathLoader.hasSupport == false');
				logger.debug ('XPathLoader: load ' + url);

				new JSLoader().load (url, function () {
					self.ready = true;
					execute ();
				});
			}
		},
		isReady: function () {
			return this.ready;
		},
		hasSupport: function () {
			return !!(this.document.evaluate || this.document.createExpression);
		}

	}

	/************************************************************************************************************
	 * Dynamically load other javascript files.
	 ***********************************************************************************************************/
	var JSLoader = function () {
		this.items = {};
	}
	JSLoader.prototype = {
		status: {	loading:	1,
					loaded:		2,
					failed:		3
				},
		load: function (url, callback) {

			var script		= document.createElement('script');
			script.type		= 'text/javascript';
			script.src		= url;
//			script.async	= 'async';

			var self	= this;

			// bind the event to the callback function
		   // there are several events for cross browser compatibility
			script.onreadystatechange = script.onload = function () {

				if (self.isLoaded (url)) return;

				if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete" ) {
					self.items[url] = self.status.loaded;
					if (callback) callback ();
				}
			};
			this.items[url] = this.status.loading;

			// add to body to begin loading
			document.body.appendChild (script);
		},
		isLoading: function (url) {
			return this.items[url] === this.status.loading;
		},
		isLoaded: function (url) {
			return this.items[url] === this.status.loaded;
		},
		failed: function (url) {
			return this.items[url] === this.status.failed;
		},
		contains: function (url) {
			return this.items[url] !== undefined;
		}
	}

	/************************************************************************************************************
	 * DOMHelper object for performing generic DOM operations. None of the methods on this object
	 * should have any knowledge of the nodes they are working on other than perhaps inspecting tagNames
	 ***********************************************************************************************************/
	var DOMHelper = function (document) {
		this.document = document;
	}
	DOMHelper.prototype = {
		isDOMReady: function () {
			return (this.document && ( this.document.readyState == 'complete' || this.document.readyState == 'interactive'));
		},
		visitValues: function (values, callback) {
			var valueArray = (values ? values.split(',') :  new Array());
			for (var i=0, len = valueArray.length; i<len; i++) {
				callback (valueArray[i]);
			}
		},
		visitNodesValues: function (values, nodes, callback) {
			var self = this;

			this.visitValues (values, function (value) {
				self.visitNodes (nodes , function (node) {
					callback (value, node);
				});
			});
		},
		// If the callback returns true then cancel visitation. This is to handle the use-case where you are
		// searching for one particular node. Once found, there is no reason to continue iterating.
		visitNodes: function (nodes, callback) {
			if (!nodes) return false;

			var node = null;
			var len = nodes.length || 0;

			// It's an Array object
			if (isArray(nodes)) {
				for (var i=0, len = nodes.length; i<len; i++) {
					if (!!callback (nodes[i])) return true;
				}

			// A single node
			} else if (nodes.nodeName && nodes.parentNode) {
				callback (nodes);

			// if it is a NodeIterator;
			} else if (nodes.nextNode) {
				while ((node = nodes.nextNode()) != null) {
					if (!!callback (node)) return true;
				}

			// an XPathResult iterator
			} else if (nodes.iterateNext) {
				while ((node = nodes.iterateNext()) != null) {
					if (!!callback (node)) return true;
				}
			// If it is a NodeList
			} else if (nodes.item) {
				for (var i=0; i<len; i++) {
					if (!!callback (nodes.item(i))) return true;
				}
			// maybe it's an HTMLCollection
			} else {
				for (var i=0; i<len; i++) {
					if (!!callback (nodes[i])) return true;
				}
			}
			return false; //Visitation was *not* cancelled by one of the callbacks.
		},

		stringifyNodes: function (nodes) {
			var root = this.document.createElement ("elements"); //keep this name 'elements' to match scripting api

			this.visitNodes (nodes, function (node) {
				var cloned = node.cloneNode (true);
				root.appendChild (cloned);
			});
			return (root.hasChildNodes() ? toHtml (root) : null);
		},

		stringifyDom: function () {
			try {
				logger.debug ("DomHelper.stringifyDom. Before HtmlSerializer");
				var dom =  new HtmlSerializer (document.documentElement).toString();
				logger.debug ("DomHelper.stringifyDom. After HtmlSerializer");
				logger.debug("DomHelper, recording snippet " + dom);
				return dom;

			} catch (e) {
				logger.debug ("DomHelper exception thrown: " + e.toString());
				return "<html><head><title>Exception Thrown</title></head><body><h1>" + e.toString() + "</h1></body></html>";
			}
		},
		winnowNodes: function (nodes, callback) {
			var nodesNew = [];

			this.visitNodes (nodes, function (node) {
				if (callback (node)) {
					nodesNew.push (node);
				}
			});
			return nodesNew;
		},
		isNodeVisible: function (node, checkParent) {
			if (node === this.document) return true;

			var style = getComputedStyle (node);

			var visible = (	style.getPropertyValue('display') !== 'none' && style.getPropertyValue('visibility') !== 'hidden');

			if (!visible)
				return false;

			return (checkParent && !!node.parentNode ? this.isNodeVisible (node.parentNode, checkParent) : true);
		},

		// return false if this node can never be rendered on the page (ic. script, style, meta tags)
		isNodeRenderable: function (node) {
			var tagName = this.getTagName (node);
			return !!tagName && tagName !== 'script' && tagName !== 'meta' && tagName != 'link' && tagName !== 'style';
		},

		// return an array of  elements. This is handy for converting Text nodes to parent elements
		toElements: function (nodes) {
			var	array = [];
			var self = this;

			this.visitNodes (nodes, function (node) {
				var element = self.toElement (node);
				if (self.isNodeRenderable (element))
					array.push (element);
			});
			return array;
		},
		toElement: function (node) {
			return node && node.nodeType == Node.TEXT_NODE ? node.parentNode : node;
		},

		findFormNode: function (node) {
			var form = (this.hasTagName (node, 'form') ? node : (node.form ? node.form : false));
			if (form) return form;

			while (form = node.parentNode) {
				if (this.hasTagName (form, 'form')) {
					break;
				}
			}
			return form;
		},
		isInput: function (node) {
			return this.hasTagName(node, 'input');
		},
		isInputText: function (node) {
			var type = node.getAttribute ('type');
			if (!this.isInput(node) || !type)
				return false;
			return	type === 'text'	|| type === 'password'	|| type === 'date'	|| type === 'search' ||
					type === 'url'	|| type === 'number'	|| type === 'email' ;
		},
		isInputRadio: function (node) {
			return (this.isInput(node) && this.hasAttribute (node, 'type', 'radio'));
		},
		isInputCheckbox: function (node) {
			return (this.isInput(node) && this.hasAttribute (node, 'type', 'checkbox'));
		},
		hasTagName: function (node, tagName) {
			return this.getTagName (node) === tagName;
		},
		getTagName: function (node) {
			return node && node.tagName ? node.tagName.toLowerCase() : false;
		},
		hasAttribute: function (node, name, value) {
			var attr = node ? node.getAttribute(name) : false;
			if (!attr) return false;
			return (value ? attr == value : true);
		},
		getNodeValue: function (node, values) { // values is an array where we push all values

			var tagName	= this.getTagName (node);
			if (!tagName) return;

			switch (tagName) {
				case 'input':
					var type = node.getAttribute('type');
					switch (type) {
						case 'radio':
							if (node.checked && node.value) values.push (node.value);
							break;
						case 'checkbox':
							if (node.checked) {
								values.push (node.value || 'true');
							} else {
								values.push (node.value==='false' ? 'true' : 'false');
							}
							break;
						default:
							values.push (node.value);
					}
					break;

				case 'select':
					this.visitNodes (node.options , function (nodeToVisit) {
						if (nodeToVisit.selected) {
							if (nodeToVisit.value) values.push (nodeToVisit.value);
						}
					});
					break;
				case 'textarea':
				case 'button':
				case 'option':
					values.push (node.value || node.innerHtml);
					break;

				case 'form':
					values.push (node.action);
					break;

				case 'img':
					values.push (node.getAttribute('alt'));
					break;

				default:
					values.push (node.innerHTML);
			}
		},
		setNodeValue: function (node, value) {

			var tagName	= this.getTagName (node);
			if (!tagName) return false;

			if (value === null || value === undefined) value = "";

			switch (tagName) {

				case 'input':
					this.setInputValue (node, value);
					break;
				case 'select':
					this.setSelectValue (node, value);
					break;
				case 'button':
					this.setButtonValue (node, value);
					break;
				case 'textarea':
				case 'option':
					node.value = value;
					break;
				case 'form':
					node.action = value;
					break;
				case 'img':
					node.setAttribute('alt', value);
					break;
				default:
					if (node.nodeType == Node.TEXT_NODE)
							node.nodeValue = value;
					else 	node.innerHTML = value;
					break;
			}
			return true;
		},

		setInputValue: function (node, value) {

			if (this.isInputRadio (node) || this.isInputCheckbox(node)) {
				node.checked = false;
				this.visitValues (value, function (value) {
					if (node.value == value) node.checked = true;
				});
				if (!node.checked && node.value != 'false' && value == 'true') {
					node.checked = true;
				}

			} else if (this.isInputText (node)) {
				this.focusNode (node);
				this.clickNode (node);

				node.value = value;

				try {
					for (var i=0; i<value.length; i++) {
						eventDispatcher.fireKeyDown		(node, value.charAt(i));
						eventDispatcher.fireKeyPress	(node, value.charAt(i));
						eventDispatcher.fireKeyUp		(node, value.charAt(i));
					}
				} catch (e) {
				}
				eventDispatcher.fireInput  (node);

			} else {
				node.value = value;
			}
			eventDispatcher.fireChange (node);
			node.removeAttribute ('placeholder');
		},
		setSelectValue: function (node, values) {
			this.visitNodes (node.options, function (nodeToVisit) { // first unselect all nodes
				nodeToVisit.selected = false;
			});
			values = '' + values; // make sure it's a string
			this.visitNodesValues (values, node.options, function (value, nodeToVisit) {
				if (nodeToVisit.value == value) nodeToVisit.selected = true;
			});
			eventDispatcher.fireChange (node);
		},
		setButtonValue: function (node) {
			if (node.value)
				node.value = value;
			else
				node.innerHtml = value;
		},
		focusNode: function (node) {
			var tagName = this.getTagName (node);

			if (tagName==='input' || tagName==='select' || tagName==='textarea') {
				node.focus();
				eventDispatcher.fireFocus (node);
				return true;
			}
			return false;
		},
		clickNode: function (node) {
			var results = null;
			var eventDispatched = false;

			if (node.onclick && typeof node.onclick == 'function') {
				results = node.onclick();

			} else {
				results = eventDispatcher.fireClick (node);
				eventDispatched = true; // keep this around in case
			}
			if (results === false) // if a boolean 'false' was returned from the click event, it's okay, but don't continue.
				return true;

			var tagName = this.getTagName (node);
			var href	= node.getAttribute ('href');
			var type	= node.getAttribute ('type');

			if (tagName === 'a' && href) {
				if (href.indexOf ('javascript:') == -1) { // it's not javascript.
					window.location.href = href;

				} else if (href.indexOf ('.submit') != -1) { // maybe it's a form submission?
					this.submitNode (node);
				}
			} else if (tagName === 'input' && (type === 'submit' || type === 'image') && !eventDispatched) { // form submission
				results = eventDispatcher.fireClick (node);
			}
			return !!results;
		},
		submitNode: function (node) {
			if (!node) return false;

			var form = this.findFormNode (node);
			if (!form) return false;

			var doSubmit = true;
			if (form.onsubmit) {
				doSubmit = form.onsubmit();
			}
			if (!doSubmit) return true;

			var submitType = (form.submit ? (typeof form.submit).toLowerCase() : 'undefined');

			if (submitType == 'function') {
				form.submit();
				return true;

			} else if (submitType == 'object') {
				return eventDispatcher.fireClick (form.submit);
			}
			return false;
		},
		generateXPath: function (node) {
			var paths	= [];

			// Use nodeName (instead of localName) so namespace prefix is included (if any).
			for (; node && node.nodeType == 1; node = node.parentNode) {
				var index		= 0;
				var useIndex	= false;
				var sibling		= null;

				for (var sibling = node.previousSibling; sibling; sibling = sibling.previousSibling) {
					// Ignore document type declaration.
					if (sibling.nodeType == Node.DOCUMENT_TYPE_NODE)
						continue;

					if (sibling.nodeName == node.nodeName) {
						++index;
						useIndex = true;
					}
				}

				for (var sibling = node.nextSibling; sibling && !useIndex; sibling = sibling.nextSibling) {
					// Ignore document type declaration.
					if (sibling.nodeType == Node.DOCUMENT_TYPE_NODE)
						continue;

					if (sibling.nodeName == node.nodeName)
						useIndex = true;
				}

				var tagName = node.nodeName.toLowerCase();
				var pathIndex = (useIndex ? "[" + (index+1) + "]" : "");
				paths.splice(0, 0, tagName + pathIndex);
			}

			return paths.length ? "/" + paths.join("/") : "/";
		}

	}


	var HtmlSerializer = function (rootNode, useXMLSerializer) {
		this.rootNode			= rootNode;
		this.useXMLSerializer	= !!useXMLSerializer
	}

	HtmlSerializer.prototype = {

		toString: function () {
			if (!this.rootNode) return "";

			logger.debug ("HtmlSerializer.useXMLSerializer == " + this.useXMLSerializer);

			if (this.useXMLSerializer) {
				return this.toStringXMLSerializer ();
			} else {
				return this.rootNode.nodeType == Node.ELEMENT_NODE ?	this.doElement	(this.rootNode)	:

this.doTextNode	(this.rootNode);
			}
		},
		doElement: function (node) {
			if (!this.isAllowedTag (node)) // filter out
				return "";

			var self	= this;
			var string	= this.doBeginTag (node);

			domHelper.visitNodes (node.childNodes, function (childNode) {
				if (!!childNode) {
					if (childNode.nodeType == Node.ELEMENT_NODE) {
						string += self.doElement (childNode);
					} else {
						string += self.doTextNode (childNode);
					}
				}
			});
			return (string + this.doEndTag (node));
		},
		htmlEncode:function (value)
		{
			//create a in-memory div, set it's inner text(which jQuery automatically encodes)
			  //then grab the encoded contents back out.  The div never exists on the page.
			return value;
			//var test = document.createElement('div');
			//test.innerText = value;
                        //return test.innerHTML;
			//var yourString = new XMLSerializer().serializeToString(node);
			//logger.log("String after serializing " + yourString);
			//return yourString;

		},

		doTextNode: function (node) {
			if(node) {
			   if(node.nodeType == Node.COMMENT_NODE) return "";
			}
			var value = (node && node.nodeValue ? node.nodeValue.toString() : "");
			value = this.htmlEncode(node.nodeValue.toString());
			return value.trim ? value.trim() : value;
		},
		doBeginTag: function (node) {
			var string = "<" + domHelper.getTagName(node);

			var attributes	= node.attributes;
			var len			= attributes && attributes.length ? attributes.length : 0;

			for (var i=0; i<len; i++) {
				var attribute = attributes[i];
				string += (" " + attribute.name + "='" + attribute.value + "'");
			}
			return string + ">";
		},
		doEndTag: function (node) {
			return "</" + domHelper.getTagName (node) + ">";
		},
		isAllowedTag: function (node) {
			if (!node || !node.tagName || node.nodeType != Node.ELEMENT_NODE) return false;
			return (! (	(domHelper.hasTagName (node, "script")) ||
						(domHelper.hasTagName (node, "iframe") && domHelper.hasAttribute (node, "class", "kdaWebtestTag")) ||
						(domHelper.hasTagName (node, "canvas") && domHelper.hasAttribute (node, "class", "kdaWebtestTag"))));
		},

		toStringXMLSerializer: function () {

			var string = "";
			try {
				var serializer = new XMLSerializer();
				string = serializer.serializeToString (this.rootNode);
			} catch (ex) {
				logger.error ('No native XML Serializer found. Using innerHTML');
				string = "<" + this.rootNode.localName + ">" + this.rootNode.innerHTML +  "</" + this.rootNode.localName + ">";
			}
			return mungeHtml (string);
		}

	}
	/************************************************************************************************************
	 * SelectorExecutor object. Responsible for executing the proper Node Selector specified in the operation.
	 ***********************************************************************************************************/
	var SelectorExecutor = function () {}

	SelectorExecutor.prototype = {

		// Only returns elements, never text nodes. Any text nodes get converted to their parent element.
		// The passed in selector may either be a function, or a string containing the function name.
		execute: function (selector, params) {
			var fn = (typeof selector === 'function' ? selector : this[selector]);
			if (!fn) return false;

			return domHelper.toElements (fn.call (this, params));
		},
		elementId: function (params) {
			return (params && params.match ? document.getElementById (params.match) : false);
		},
		name: function (params) {
			return (params && params.match ? document.getElementsByName (params.match) : false);
		},
		tag: function (params) {
			return (params && params.match ? document.getElementsByTagName (params.match) : false);
		},
		css: function (params) {
			return (params && params.match ? document.querySelectorAll (params.match) : false);
		},
		form: function (params) {

			if (!params || !params.name || !params.value) return false;

			var selector		= this [params.name];
			var filter			= null;
			var selectorParams	= {};
			var filterParams	= {};

			// If there's a selector, use it, then filter by the 'form' tag
			if (selector) {
				selectorParams.match	= params.value;
				filter					= filterExecutor ['tag'];
				filterParams.match		= 'form';

			// else select by 'form' tag then apply the specified filter.
			} else {
				selector				= this.tag;
				selectorParams.match	= 'form';
				filter					= filterExecutor [params.name];
				filterParams.match		= params.value;
			}
			var nodes = this.execute (selector, selectorParams);

			if (filter) nodes = filter (nodes, filterParams);
			return (nodes.length > 0 ? nodes : false);
		},
		text: function (params) {

			if (!params || !params.match) return false;

			return document.createNodeIterator (document.body,
												NodeFilter.SHOW_TEXT,
												function (testNode) {
													return (testNode.data.indexOf (params.match) !=
-1 ?
															NodeFilter.FILTER_ACCEPT :
NodeFilter.FILTER_SKIP);
												},
												true);
		},
		xpath: function (params) {

			if (!params || !params.match) return false;

			if (document.evaluate) {
				logger.info ("Calling document.evaluate");
				return document.evaluate (params.match, document, null, XPathResult.UNORDERED_NODE_ITERATOR_TYPE, null);

			} else if (document.createExpression){
				logger.info ("Calling document.createExpression");
				var expression = document.createExpression (params.match, null);
				return expression.evaluate (document, XPathResult.UNORDERED_NODE_ITERATOR_TYPE, null);

			} else {
				logger.error ("Browser does not support XPath.");
			}
			return false;

		}

	}


	/************************************************************************************************************
	 * FilterExecutor object. Responsible for executing the proper Node Filter specified in the operation.
	 * Every method except execute takes 2 parameters: an array of nodes and a params object. Every method
	 * returns an array of nodes that is a subset of the original array.
	 ***********************************************************************************************************/
	var FilterExecutor = function () {}

	FilterExecutor.prototype = {

		execute: function (filter, nodes, params) {
			var fn = typeof filter === 'function' ? filter : this[filter];
			if (!fn) return false;

			return fn.call (this, nodes, params);
		},

		// the params object must have a 'match' member that is an integer specifying the index into the nodes array.
		index: function (nodes, params) {

			if (!nodes || !params || !params.match) return false;

			var index = (isNaN (params.match) ? parseInt (params.match) : params.match);

			if (index >= nodes.length) return false;

			return [nodes [index]];
		},
		attr: function (nodes, params) {
			if (!nodes || !params || !params.name || !params.value ) return false;

			var name	= params.name;
			var match	= params.value;

			return domHelper.winnowNodes (nodes, function (node) {
				var value = node.getAttribute ? node.getAttribute (name) : false;
				return (value && value.indexOf (match) > -1);
			});
		},
		text: function (nodes, params) {
			if (!nodes || !params || !params.match) return false;

			var match = params.match;

			return domHelper.winnowNodes (nodes, function (node) {
				var nodeIter	= document.createNodeIterator (
											node,
											NodeFilter.SHOW_TEXT,
											function (testNode) {
												return (testNode.data.indexOf (match) != -1 ?
														NodeFilter.FILTER_ACCEPT :
NodeFilter.FILTER_SKIP);
											},
											true);
				return (nodeIter.nextNode() != null);
			});
		},
		tag: function (nodes, params) {
			if (!nodes || !params || !params.match) return false;

			var match = params.match;

			return domHelper.winnowNodes (nodes, function (node) {
				return (node.localName == match);
			});
		},
		visible: function (nodes, params) {
			if (!nodes) return false;

			return domHelper.winnowNodes (nodes, function (node) {
				return domHelper.isNodeVisible (node, true);
			});
		}
	}

	/*******************************************************************************************************************
	 * ActionExecutor object. Responsible for executing the proper action specified in the operation.
	 * The function names in this object should exactly match the NodeActionTypeEnum java object on the server
	 ******************************************************************************************************************/
	var ActionExecutor = function () {
		this.fnWaitTest	=	{
				exists:		function (node)	{return true;},
				visible:	function (node)	{return  domHelper.isNodeVisible (node, true)},
				hidden:		function (node)	{return !domHelper.isNodeVisible (node, true)},
				focus:		function (node) {return elFocus == node},
				blur:		function (node) {return elFocus != node}
			}
	}
	ActionExecutor.prototype = {
		execute: function (name, nodes, params) {
			var fn = this[name];
			if (!fn) return false;

			return fn.call (this, nodes, params);
		},
		click: function (nodes, params) {
			if (!nodes || nodes.length == 0) return false;

			var node = nodes[0];

			// Do we really need to pause first?
			runtimeExecutor.setModePause();

			setTimeout (function() {
				domHelper.focusNode (node);
				domHelper.clickNode (node);

				runtimeExecutor.setModeRun();

			}, params.delay || defaultDelay);

			return true;
		},
		loggerConfig: function (nodes, params) { // nodes will be empty.
			if (!params || !params.level || !logger.enums.level [params.level] ||  !params.target || !logger.enums.target [params.target])
				return false;
			logger.setConfig (params);
			return true;
		},
		getValue: function (nodes, params) {	// operate on all nodes, return values as CSV
			if (!nodes || nodes.length == 0) return false;

			var values	= [];
			domHelper.visitNodes (nodes , function (node) {
				domHelper.getNodeValue (node, values);
			});

			return (values.length > 0 ? values.toString() : false);
		},
		setValue: function (nodes, params) {	// operate on all nodes
			if ( !nodes || nodes.length == 0) return false;

			var value = (params && params.value ? params.value : "");

			domHelper.visitNodes (nodes, function (node) {
				domHelper.setNodeValue (node, value);
			});
			return true;
		},
		setCheckOn: function (nodes) {
			return this.setCheck (nodes, {value: true});
		},
		setCheckOff: function (nodes) {
			return this.setCheck (nodes, {value: false});
		},
		setCheck: function (nodes, params) {	// operate on all nodes
			if ( !nodes || nodes.length == 0) return false;

			var value = (params && params.value ? params.value : false);

			domHelper.visitNodes (nodes, function (node) {
				if (domHelper.isInputCheckbox (node)) node.checked = value;
			});
			return true;
		},
		formSubmit: function (nodes, params) {		// operate on just the first node
			if (!nodes || nodes.length == 0) return false;

			setTimeout (function () {
				domHelper.submitNode (nodes[0]);
			}, params.delay || defaultDelay);

			return true;
		},
		formValues: function (nodes, params) {	// operate on just the first node, which should be a <form> tag
			if (!nodes || nodes.length==0 || !params) return false;

			domHelper.visitNodes (nodes[0].elements, function (node) {
				if (node.localName == 'fieldset')
					return;

				var name	= node.getAttribute ('name');
				var value	= (name ? params[name] : undefined);

				if (typeof value != 'undefined') {
					domHelper.setNodeValue (node, value);
				}
			});
			return true;
		},
		wait: function (nodes, params) {
			if (!nodes || nodes.length == 0 || !params) return false;

			var fnWaitTest = this.fnWaitTest [params.type || 'exists'];
			if (!fnWaitTest) return false;

			var success = true;
			domHelper.visitNodes (nodes, function (node) {
				if (! fnWaitTest (node)) { // returns false if not true
					success = false;
				}
			});
			return success;
		},
		inspectOff: function () {					// Node selection not required
			inspector.inspectOff ();
			return true;
		},

		inspectOn: function (nodes, params) {			// Operate on all, unless params.noSelect == true, which will be the
			if (! params ['noSelect']) {				// case if the user is just turning on inspect mode and not selecting anything.
			    inspector.inspectOn ();
				return inspector.inspectNodes (nodes);	// Fail if no visible nodes were selected.
			} else {
				inspector.inspectOn ();
				return true;
			}
		},
		recordOn: function () {						// Node selection not required
			return recorder.recordOn ();
		},
		recordOff: function () {					// Node selection not required
			recorder.recordOff ();
			return true;
		},
		getCount: function (nodes) {				// Count always returned with every operation.
			return true;
		},
		getDom: function () {						// Node selection not required
			logger.debug ('ActionExecutor.getDom(): Begin');
			delay (serverFacade.postDom, 10, serverFacade);
			logger.debug ('ActionExecutor.getDom(): End');
			return true;
		},
		getXPath: function (nodes) {
			var values	= [];
			domHelper.visitNodes (nodes , function (node) {
				values.push (domHelper.generateXPath (node));
			});
			return (values.length > 0 ? values.toString() : false);
		},
		pageSelectOn: function (nodes) {
			if (!nodes || nodes.length==0) {
				runtimeExecutor.setModePause();
			} else {
				runtimeExecutor.setModeRun();
			}
			return true;
		},
		pageSelectOff: function () {
			if (runtimeExecutor.isModePause())
				runtimeExecutor.setModeRun();
			return true;
		},
		pageRollcall: function () {
			delay (serverFacade.postPage, 10, serverFacade);
			return true;
		},
		clearCookies: function () {
			storageManager.clear();
		},
		scroll: function () {						// Node selection not required
			var clientHeight	= document.documentElement.clientHeight;
			var remaining		= document.body.scrollHeight - window.pageYOffset - clientHeight;

			if (remaining == 0) // Nothing left to scroll
				return 0;

			var scrollBy = (remaining < clientHeight ? remaining : clientHeight);
			window.scrollBy (0, scrollBy);
			return scrollBy;
		},
		getExtents: function () {
			var remaining = document.body.scrollHeight - window.pageYOffset - window.innerHeight;

			return	'{' +	'window.screenY:'		+ window.screenY							+ ', '	+
							'window.pageYOffset:'	+ window.pageYOffset						+ ', '	+
							'window.innerHeight:'	+ window.innerHeight						+ ', '	+
							'window.outerHeight:'	+ window.outerHeight						+ ', '	+

							'body.clientHeight:'	+ document.body.clientHeight				+ ', '	+
							'body.offsetHeight:'	+ document.body.offsetHeight				+ ', '	+
							'body.scrollHeight:'	+ document.body.scrollHeight				+ ', '	+

							'html.clientHeight:'	+ document.documentElement.clientHeight		+ ', '	+
							'html.offsetHeight:'	+ document.documentElement.offsetHeight		+ ', '	+
							'html.scrollHeight:'	+ document.documentElement.scrollHeight		+ ', '	+

							'screen.height:'		+ screen.availHeight						+
', '	+
							'body.scrollTop:'		+ document.body.scrollTop					+
', '	+
							'remaining:'			+ remaining
	+
					'}';

		}

	}

	/*******************************************************************************************************************
	 * OperationManager object. Responsible for executing the operation.
	 ******************************************************************************************************************/
	var OperationManager = function () {
		this.activeOperations	= {};
		this.completeOperations	= {};

		this.status				=	{	pending:	'pending',
										success:	'success',
										failure:	'failure',
										timeout:	'timeout'
								}
	}
	OperationManager.prototype = {

		isStatusPending:	function (operation) {return (!operation.status || operation.status === this.status.pending);},
		isActionNoHidden:	function (operation) {return this.hasActionParam (operation, 'noHidden')},
		isActionNoSelect:	function (operation) {return this.hasActionParam (operation, 'noSelect')},
		isActionNoRetry:	function (operation) {return this.hasActionParam (operation, 'noRetry')},
		isActionPostFirst:	function (operation) {return this.hasActionParam (operation, 'postFirst')},
		isActionRunAlways:	function (operation) {return this.hasActionParam (operation, 'runAlways')},
		isActionNoResponse:	function (operation) {return this.hasActionParam (operation, 'noResponse')},

		hasActionParam:		function (operation, name) {
			return (operation.action && operation.action.params && operation.action.params[name])
		},
		runOperations: function () {
			var active		= this.activeOperations;
			var complete	= this.completeOperations;

			var keepActive	= {};

			for (var id in active) {
				var operation = active [id];

				var success = this.runOperation (operation);

				if (success || this.isActionNoRetry (operation) ) {
					var status = success ? this.status.success : this.status.failure;

					this.finishOperation (operation, status, true); // we may have already posted out.

					// don't need to keep the whole operation around, just the ID to check later
					complete [id] = true;

				} else {
					keepActive [id] = operation;
				}
			}
			this.activeOperations = keepActive;
		},
		runOperation: function (operation) {

			if (! this.isStatusPending (operation)) // If the operation has already completed, just succeed
				return true;

			// If there is no action, or we're in paused mode, fail the operation
			if (!operation.action || (runtimeExecutor.isModePause() && !this.isActionRunAlways(operation)))
				return false;

			var actionType = operation.action.type;

			if (actionType != 'inspectOn')
				actionExecutor.inspectOff();

			// we've already performed the action,  so just return.
			if (actionType == 'inspectOff')
				return true;

			// Now run the selector and the filters
			var nodes	= this.filterNodes (operation, this.selectNodes (operation));
			var results	= this.initResults (operation, nodes);

			// Some actions need to post a success message *before* actually executing the action (like Click and SubmitForm)
			if (this.isActionPostFirst (operation)) {
				if (nodes.length == 0) return false;

				results = this.processResults (operation, true);

				this.finishOperation (operation, this.status.success, results);

				actionExecutor.execute (operation.action.type, nodes, operation.action.params);

			} else {
				// by the time we get here, we should have at least 1 node selected depending on the action.
				// Execute the action now
				results = actionExecutor.execute (operation.action.type, nodes, operation.action.params);
				results = this.processResults (operation, results);
			}
			return results;
		},
		// Return an array of Elements if any were selected, else return an empty array
		selectNodes: function (operation) {
			// Some actions (like getDom and inspectOff) don't require selecting nodes, so return an empty array
			if (!operation.selector || this.isActionNoSelect (operation)) return [];

			// For the ones that do require nodes, execute the NodeSelector (required)
			return selectorExecutor.execute (operation.selector.type, operation.selector.params);
		},

		// Iterate through the list of filters (if any) and execute them
		filterNodes: function (operation, nodes) {

			// Filter out hidden nodes for actions that only operate on visible nodes (like InspectOn)
			if (this.isActionNoHidden (operation)) {
				nodes = filterExecutor.visible (nodes);
			}
			// If there are no filters, or the nodes array is empty, return the nodes array now
			if (!operation.filters || operation.filters.length == 0 || nodes.length == 0)
				return nodes;

			// Iterate through the filters and run each one.
			for (var i=0, len = operation.filters.length; i < len; i++) {
				var filter = operation.filters [i];
				nodes = filterExecutor.execute (filter.type, nodes, filter.params);
			}
			return nodes;
		},
		initResults: function (operation, nodes) {
			operation.results = {};
			operation.results.nodeCount = nodes.length; // always return the node count.

			if ( nodes.length > 0) {		// return the selected nodes in an html snippet
				operation.results.nodes = domHelper.stringifyNodes (nodes);
			}

			return operation.results;
		},
		processResults: function (operation, results) {

			if (results == 0 || (results && typeof results !== 'boolean')) {
				operation.results.value = results.toString();	// return getValue in a string, scroll is a number
			}
			return !!results; // always return boolean
		},
		addOperations: function (operations) {

			if (operations.length) { // it's an array of operations
				for (var i=0, len=operations.length; i<len; i++) {
					this.addOperation (operations[i]);
				}
			} else if (operations.id) { // it's a single operation
				this.addOperation (operations);
			}
		},
		addOperation: function (operation) {
			// If the mode has been set to Pause and the action is not configured to always run, then just mark it complete.
			if (runtimeExecutor.isModePause() && ! this.isActionRunAlways(operation)) {
//				this.completeOperations [operation.id] = true;
				return;
			}
			var activeOperation		= this.activeOperations		[operation.id];
			var completeOperation	= this.completeOperations	[operation.id];

			if (!activeOperation && !completeOperation && operation.status === this.status.pending ) {
				this.activeOperations [operation.id] = operation;

				this.printOperation (operation);

				if (operation.timeout > 0) {
					operation.timeoutTimer = delay (this.timeoutOperation, operation.timeout, this, operation);
				}
			}
		},
		// Kill the timer, set the status, and optionally post the results to the server
		finishOperation: function (operation, status, doPost) {

			if (operation.timeoutTimer) {
				try {clearTimeout (operation.timeoutTimer) } catch (e) {}
				delete operation.timeoutTimer;
			}

			// If the status is pending set the status and init the results object
			// and optionally post the results to the server.
			if (this.isStatusPending (operation)) {

				if (!operation.results) operation.results = {};
				operation.results.status = 	operation.status = status;

				// always test to see if this action is a NO_RESPONSE action. If it is, don't post results. This allows
				// for broadcast type messages. It is up to the server to take them off the operations queue.
				if (doPost && !this.isActionNoResponse(operation)) serverFacade.postOperation (operation);

			}
		},
		// callback for timing out an operation. Only execute if status == pending
		timeoutOperation: function (operation) {

			if (this.isStatusPending	(operation)) {
				logger.error			('Operation ' + operation.id +' timed out');
				this.finishOperation	(operation, this.status.timeout, true);
			}
		},
		printOperation: function (operation) {
			logger.info (function (){
				var action = operation.action.type + (operation.action.params && operation.action.params.value ?
														' (' + operation.action.params.value +
')' : '');
				var message = 'Received: ' + operation.id + ' , ' + action;
				if (operation.selector) message += (' , ' + operation.selector.type + '=' +operation.selector.params.match);
				return message;
			});
		}

	}
	/*******************************************************************************************************************
	 * Runtime manager. Manages the mode (i.e. Run, Inspect, Record, Pause)
	 * Also is responsible for starting the
	 ******************************************************************************************************************/
	var RuntimeExecutor = function (checkInterval) {
		this.executing		= false;  // will only be true if the execute() loop is running.
		this.mode			= false;
		this.modeStack		= new Array();
		this.checkTimeout	= false;
		this.checkInterval	= checkInterval;
	}

	RuntimeExecutor.prototype = {
		execute: function () {

			if (!this.mode) {
				this.setModeRun();
			}
			if (this.delayIfNotReady ()) {
				return;
			}

			this.executing = true;

			try {
				operationManager.runOperations	();

			} catch (e) {
				logger.error ('runtimeExecutor.execute: ' + e.toString());
			}

			if (!!this.checkTimeout) clearTimeout (this.checkTimeout);

			this.checkTimeout = delay (this.execute, this.checkInterval, this);
		},
		delayIfNotReady: function (fn) {

			if (! (serverFacade.ready) ) {
				return delay (this.execute, 100, this);
			}
			return false;
		},
		isExecuting:	function () {return this.executing},
		isModeRun:		function ()	{return this.mode === 1},
		isModeInspect:	function ()	{return this.mode === 2},
		isModeRecord:	function ()	{return this.mode === 3},
		isModePause:	function ()	{return this.mode === 4},

		setModeRun:		function () {
			if (this.mode == 1) return;
			this.mode = 1;
			this.printMode();
		},
		setModeInspect:	function () {
			if (this.mode == 2) return;
			this.mode = 2;
			this.printMode();
		},
		setModeRecord:	function () {
			if (this.mode == 3) return;
			this.mode = 3;
			this.printMode();
		},
		setModePause:	function () {
			if (this.mode == 4) return;
			this.mode = 4;
			this.printMode();
		},
		pushMode:  function () {
			this.modeStack.push(this.mode);
		},
		popMode: function () {
			var lastMode = this.modeStack.pop ();
			this.mode = (lastMode || 1);
		},
		printMode: function () {
			var mode = this.mode;

			logger.info (function () {
				var msg = "Current Mode: ";
				switch (mode) {
					case 1: return msg + "RUN";
					case 2: return msg + "INSPECT";
					case 3: return msg + "RECORD";
					case 4: return msg + "PAUSE";
				}
				return msg + " UNKNOWN";
			});
		}
	}

	/*******************************************************************************************************************
	 * Global functions and variables.
	 ******************************************************************************************************************/

	var	version			= '6.0.SHAPSHOT',
		isBrowser		= !!(typeof window !== 'undefined' && navigator && document),
		elFocus			= null,
		defaultDelay	= 1000, //milliseconds

		// merge source properties into the target. modifies the target object, and also returns it.
		mixin		=	function (target, source) {
							if (!source) return target;

							var name;
							for (name in source) {
								var sourceVal	= source [name];
								var targetVal	= target [name];

								if (sourceVal !== null && sourceVal !== undefined) {
									var sourceType	= typeof (sourceVal.valueOf ? sourceVal.valueOf() : sourceVal);

									if (sourceType !== 'object' || !targetVal)  {
										target[name] = sourceVal;

									} else if (targetVal) {
										mixin (targetVal, sourceVal);
									}
								}

							}
							return target;
						},

		curry		=	function (fn, bindObj, args) {
							if (args && typeof args != 'array')	args = [args];
							return function() {return fn.apply(bindObj, args || arguments);};
						},
		delay		=	function (fn, delay, bindObj, args) {
							return setTimeout (curry (fn, bindObj, args), delay);
						},
		stringify	=	function (data) {
							var dataType = (data == null ? null : typeof data);
							return (dataType == null ? null : (dataType == "object" ? global.JSON.stringify (data) :
data.toString()));
						},
		setMcd		=	function (mcd, notifyServer) {
							logger.debug ("Global.setMcd()");

							optionManager.setMcd (mcd);
							if (notifyServer) serverFacade.postMcd (mcd);
						},
		toHtml		=	function (node) {
							return new HtmlSerializer (node).toString ();
						},
		mungeHtml	=	function (html) {
							return html.replace (/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi, '');
						},
		isArray		=	function (obj) {
							if (!obj) return false;

							var objType = typeof obj;
							if (objType !== 'object')
								return false;

							return (obj.length && obj.pop && obj.splice ? true : false);
						},

		//Global functions to be called from event handlers and the global object.
		initialized	=	false,
		initialize	=	function () {
							if (initialized) return;

							optionManager.initialize ();
							iframeManager.initialize()
							inspector.initialize();
							xpathLoader.initialize();

							initialized = true;

						},
		execute		=	function () { // we need to check the xpathLoader readiness here too.
							logger.debug ("Global.execute(): Begin");

							if (runtimeExecutor.isExecuting()) {
								logger.debug ("runtimeExecutor is already executing.");
								return;
							}
			 				// if initialization hasn't completed yet, try again in 100ms
							if (!initialized) {
								logger.debug ("initialized == false. Call execute() again...");
								setTimeout (function () {
									execute();
								}, 100);

							} else if (xpathLoader.isReady()) {
								logger.debug ("xpathLoader.isReady() == true");

								runtimeExecutor.execute();
							} else {
								logger.debug ("xpathLoader.isReady() == false. Ack.");
							}
							logger.debug ("Global.execute(): End");
						},

		saveOptions	=	function () {
							storageManager.write ({	'overrides': optionManager.overrides,
													'recording': runtimeExecutor.isModeRecord()});
						},
		formatString=	function () {
							var message = arguments[0];
							for (var i = 0, len=arguments.length-1; i < len; i++) {
								var regexp = new RegExp('\\{'+i+'\\}', 'gi');
								message = message.replace (regexp, stringify (arguments[i+1]));
							}
							return message;
						},
		getMinMax	=	function (value, min, max) {
			return (value > max ? max : (value < min ? min : value));
		},
		showMcdPopup = function (callback) {
			var mcd = null;

			var elWrapper	= document.createElement ("div");
			var elInput		= document.createElement ("input");
			var elButton	= document.createElement ("input");

			elWrapper.style.position		= 'absolute';
			elWrapper.style.top				= 0;
			elWrapper.style.left			= 0;
			elWrapper.style.width			= '100%';
			elWrapper.style.height			= '100%';
			elWrapper.style.zIndex			= 9999999999;
			elWrapper.style.backgroundColor	= 'white';

			elInput.setAttribute	("type", "text");
			elInput.setAttribute	("placeholder", "Enter MCD");
			elInput.style.width		= "180px";

			elButton.setAttribute	("type", "button");
			elButton.setAttribute	("value", "Set MCD");

			elWrapper.appendChild (elInput);
			elWrapper.appendChild (elButton);

			elButton.addEventListener	("click", function (e) {
				var mcd = Number (elInput.value);
				if (!isNaN (mcd)) {
					callback (mcd);
					document.body.removeChild (elWrapper);
				}
			});
			document.body.appendChild (elWrapper);
		},

		logger				= new Logger			(), // create Logger first so everyone else can use it.
		optionManager		= new OptionManager		(),
		storageManager		= new StorageManager	(global.localStorage, 'daClient:storage'),
		serverFacade		= new ServerFacade		(),
		iframeManager		= new IFrameManager		(),
		inspector			= new Inspector			(),
		recorder			= new Recorder			(),
		domHelper			= new DOMHelper			(global.document),
		xpathLoader			= new XPathLoader		(global.document),

		// These executors should only contain the methods defined in their respective json objects, plus one 'execute' method
		selectorExecutor	= new SelectorExecutor	(),
		filterExecutor		= new FilterExecutor	(),
		actionExecutor		= new ActionExecutor	(),

		operationManager	= new OperationManager	(),
		runtimeExecutor		= new RuntimeExecutor	(500), // check for new operations every 1000 ms

		eventDispatcher		= new EventDispatcher	(),
		eventHandlers		= new EventHandlers		();	// create EventHandlers last because it creates the onLoad handler
														// in its constructor.

	// Create the kdaWebtest object and add any of the vars we want to expose to the global namespace.
	var	kdaWebtest = {};

	kdaWebtest.operationManager	= operationManager;
	kdaWebtest.storageManager	= storageManager;
	kdaWebtest.optionManager	= optionManager;
	kdaWebtest.recorder			= recorder;
	kdaWebtest.logger			= logger;

	kdaWebtest.version			= version;
	kdaWebtest.isBrowser		= isBrowser;
	kdaWebtest.getQueryParam	= function (param) {return optionManager.getQueryParams()[param]};

	// some helper methods to be used directly from the page.
	kdaWebtest.isReady			= function ()		{return runtimeExecutor.isExecuting()};
	kdaWebtest.setOptions		= function (options){optionManager.setOverrides (options); saveOptions();},
	kdaWebtest.saveOptions		= function ()		{saveOptions()};
	kdaWebtest.setMcd			= function (mcd)	{setMcd (mcd)};

	kdaWebtest.pause			= function ()		{runtimeExecutor.setModePause()};
	kdaWebtest.run				= function ()		{runtimeExecutor.setModeRun()};

	kdaWebtest.curry			= curry;

	/**
	 * Now export the kdaWebtest object to the global namespace.
	 * Supports CommonJS module loading (i.e. Node.js) via the exports object
	 * Copied from underscore.js. export the kdsWebtest object for node.js if 'exports' object exists
	 **/
	if (typeof exports !== 'undefined') {
		if (typeof module !== 'undefined' && module.exports) {
			exports = module.exports = kdaWebtest;
		}
		exports.kdaWebtest = kdaWebtest;
	} else {
		global ['kdaWebtest'] = kdaWebtest;
	}

	// Start processing.
	// The dom is already loaded, this script may have been loaded dynamically, so we need to
	// call the functions normally called by the EventHandler.onDOMReady and onLoad methods.
	 if (domHelper.isDOMReady()) {
		initialize();
		execute();

	} else {
		if (global.document) {
			global.document.addEventListener ("DOMContentLoaded", function () {
				logger.debug ('onDomReady()');
				initialize();
			});
		}
	}

}) (this);


