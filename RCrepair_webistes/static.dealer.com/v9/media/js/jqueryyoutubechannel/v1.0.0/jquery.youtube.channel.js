/**
 *	Plugin which renders the YouTube channel videos list to the page
 *	@author:  H. Yankov (hristo.yankov at gmail dot com)
 *	@version: 1.0.0 (Nov/27/2009)
 *	http://yankov.us
 */

/*
 * Heavily modified from original version by ddcrandallm:
 *	- cleaned up source to eliminate globals
 *	- code passes JSLint
 *	- removed baked-in HTML widths for more flexibility
 *	- removed unnecessary (empty) table cells from YouTube markup service
 *  - added feature to change player URL
 *  - added optional callback method "afterLoad"
 */

(function () {
	 var window = this,
		$ = window.jQuery,
		__mainDiv,
		__preLoaderHTML,
		__opts,
		__jQueryYouTubeChannelReceiveData = function (data) {
			$.each(data.feed.entry, function(i,e) {
				if (e && e.content && e.content.$t) {
					__mainDiv.append(e.content.$t);
				}
			});
	
			// Mark the first div in the list
			$(__mainDiv).find("div:first").addClass("firstVideo");
	
			// Now hide objects depending on the settings and fix the img style
			$(__mainDiv).find("div").width('').each(function () {
				$('table', this).css({
					tableLayout: 'fixed',
					width: '100%'
				});
		
				// Do we need to remove borders from image?
				if (__opts.removeBordersFromImage) {
					$(this).find("table > tbody > tr:first > td:first > div").css("border", "0px");
					$(this).find("table > tbody > tr:first > td:first > div > a > img").css("border", "0px");
				}
		
				// Open in new tab?
				if (__opts.linksInNewWindow) {
					$(this).find("table > tbody > tr:first > td:first > div > a").attr("target", "_blank");
					$(this).find("table > tbody > tr:first > td:eq(1) > div > a").attr("target", "_blank");
				}
	
				// Hide the video length
				if (__opts.hideVideoLength) {
					$(this).find("table > tbody > tr:last > td:first").hide();
				}
		
				var additionalInfo = $(this).find("table > tbody > tr:first > td:last");
		
				if (__opts.hideFrom) {
					$(additionalInfo).find("div:eq(0)").hide();
				}
		
				if (__opts.hideViews) {
					$(additionalInfo).find("div:eq(1)").hide();
				}
		
				if (__opts.hideRating) {
					$(additionalInfo).find("div:eq(2)").hide();
				}
			
				if (__opts.hideNumberOfRatings) {
					$(additionalInfo).find("div:eq(3)").hide();
				}
			
				// Always hide the additional categories
				$(this).find("table > tbody > tr:last > td:last").hide();
		
				// remove widths set in HTML by YouTube
				$('*', this).removeAttr('width');
		
				// set colspan
				$('tbody > tr:first > td:eq(1), tbody > tr:eq(1) > td:eq(0)', this).attr('colspan', '2');
		
				// remove last cell?
				if (__opts.hideViews && __opts.hideRating && __opts.hideNumberOfRatings) {
					$('tr > td:eq(2)', this).remove();
				}
				
				// update links to optionally specified alternate URL
				// in videoLinkUrl setting
				if (__opts.videoLinkUrl) {
					$('a[href*="watch?v="]').each(function () {
						var href = __opts.videoLinkUrl,
							videoId = $(this).attr('href').toString().match(/v=([^&$]*)/),
							html = $(this).html(),
							style = $(this).attr('style'),
							linkString = 'a style="' + style + '"';
						
						videoId =
							(videoId && videoId.length && videoId[1]) ?
							videoId[1] :
							null;
						
						if (!videoId) {
							href = $(this).attr('href');
						}
						
						linkString += ' href="' + href.replace('|videoId|', videoId) + '"';
						
						if (__opts.videoLinkClasses) {
							linkString += ' class="' + __opts.videoLinkClasses + '"';
						}
						if (__opts.videoLinkAttributes) {
							linkString += ' ' + __opts.videoLinkAttributes;
						}
						
						linkString = '<' + linkString + '>' + html + '</a>';
						
						$(this).replaceWith(linkString);
					});
				}
			});
			// Remove the preloader and show the content
			$(__preLoaderHTML).remove();
			__mainDiv.show();
			
			// trigger callback
			if ($.isFunction(__opts.afterLoad)) {
				__opts.afterLoad();
			}
		};

	$.fn.youTubeChannel = function(options) {
		var videoDiv = $(this);

		$.fn.youTubeChannel.defaults = {
			userName: null,
			loadingText: "Loading...",
			linksInNewWindow: true,
			hideVideoLength: false,
			hideFrom: true,
			hideViews: true,
			hideRating: true,
			hideNumberOfRatings: true,
			removeBordersFromImage: true,
			videoLinkUrl: null,
			videoLinkClasses: null,
			videoLinkAttributes: null,
			afterLoad: null
		};
			
		__opts = $.extend({}, $.fn.youTubeChannel.defaults, options);
	
		return this.each(function() {
			if (__opts.userName) {
				videoDiv.append("<div class=\"channel_div\"></div>");
				__mainDiv = $(".channel_div", videoDiv);
				__mainDiv.hide();
			
				__preLoaderHTML = $("<p class=\"loader\">" + __opts.loadingText + "</p>");
				videoDiv.append(__preLoaderHTML);
			
				// TODO: Error handling!
				$.getScript(window.location.protocol + "//gdata.youtube.com/feeds/base/users/" + __opts.userName + "/uploads?alt=json-in-script&callback=__jQueryYouTubeChannelReceiveData");
			}
		});
	};
	
	window.__jQueryYouTubeChannelReceiveData = __jQueryYouTubeChannelReceiveData;
}());
