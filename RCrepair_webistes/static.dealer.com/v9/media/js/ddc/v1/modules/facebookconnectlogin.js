(function () {
	var window = this,
		$ = window.jQuery,
		DDC = (window.DDC = (window.DDC || {}));
	
	DDC.modules.facebookConnectLogin.refresh = function () {
		var FB = window.FB,
			login = function (response) {
				if (response && response.session && response.session.uid) {
					// login successful, hide login buttons and announce Facebook user id
					$(DDC.modules.facebookConnectLogin.selector).addClass('hide');
					$(document).trigger('facebookConnectLogin',
						[{uid: response.session.uid}]);
					$(document).trigger('dynamicUrls', [{
						facebookUid: response.session.uid
					}]);
				}
			},
			click = function (e) {
				FB.getLoginStatus(function (response) {
					if (response && (response.status !== 'connected')) {
						FB.login(login);
					}
				});

				e.preventDefault();
			};
		
		if (FB) {
			$(DDC.modules.facebookConnectLogin.selector).each(function () {
				var self = this;
				
				if (!$(this).data('facebook-connect-login-initialized')) {
					FB.getLoginStatus(function (response) {
						if (response && (response.status === 'connected')) {
							// user is logged in, do something
							login(response);
						} else {
							// user isn't logged in, expose button
							$(self).removeClass('hide');
						}
					});
					
					$(this).unbind('click.fbconnect')
						.bind('click.fbconnect', click)
						.data('facebook-connect-login-initialized', true);
				}
			});
		}
	};
}());
