(function () {
	var window = this,
		$ = window.jQuery,
		DDC = (window.DDC = (window.DDC || {}));
	
	DDC.modules.ui.refresh = function () {
		var formatNumber = function (value) {
				if (!isNaN(+value)) {
					value = (Math.round(+(value.toString()
						.replace(/(\d*\.?\d?\d?)(\d)*/g, '$1')) * 100) / 100)
						.toString();
					return value.replace(/(^|[^\w.])(\d{4,})/g,
						function ($0, $1, $2) {
							return $1 + $2.replace(/\d(?=(?:\d\d\d)+(?!\d))/g,
								'$&,');
						});
				}
			},
			sliderTemplate = '<div class="ui-slider">' +
				'<a class="ui-slider-handle ui-min"></a>' +
				'<a class="ui-slider-handle ui-max"></a>' +
				'</div>';

		// accordions
		$('.ui-accordion:not(.ui-manual)').accordion({
			autoHeight: false,
			header: '> .ui-accordion-header, fieldset > .ui-accordion-header',
			change: function () {
				// ensure reflow in IE
				if (!$.support.changeBubbles) {
					// Re-flows source elements in IE fixing DOM overlap issues
					$(this).parents('[class*=yui3-g]:eq(0)').addClass('reflow');
				}
			}
		});
		$('.ui-accordion-next').die('click.accordion-button')
			.live('click.accordion-button', function () {
				var index = $(this).parents('.ui-accordion')
					.find('.ui-accordion-content')
					.index($(this).parents('.ui-accordion-content'));

				$(this).parents('.ui-accordion')
					.accordion('activate', index + 1);

				return false;
			});
		$('.ui-accordion-previous').die('click.accordion-button')
			.live('click.accordion-button', function () {
				var index = $(this).parents('.ui-accordion')
					.find('.ui-accordion-content')
					.index($(this).parents('.ui-accordion-content'));

				$(this).parents('.ui-accordion')
					.accordion('activate', index - 1);

				return false;
			});
		// autocomplete
		$(':input[data-autocomplete-url]').each(function () {
			var settings,
				unique = function (a) {
					var i,
						b = a.slice(0);
					
					b.sort();
					
					for (i = 1; i < b.length;) {
						if(b[i-1] === b[i]){
							b.splice(i, 1);
						} else {
							i++;
						}
					}
					
					return b;
				};
			
			if (!$(this).data('autocomplete-initialized')) {
				settings = $.extend({
					minLength: 3,
					source: function (request, response) {
						if (settings.autocompleteUrl) {
							$.ajax({
								url: settings.autocompleteUrl.replace('|search|', request.term),
								dataType: 'html',
								success: function (data) {
									var result = unique($.map($(data).find('.item'), function (item) {
										return {
											label: $.trim($(item).text().replace(/\s+/g, ' ')),
											value: $.trim($(item).text().replace(/\s+/g, ' ')),
											href: $(item).attr('href')
										};
									}));
									
									response(result);
								}
							});
						}
					},
					select: function (event, ui) {
						var itemHref = $(ui.item).attr('href');
						
						if (itemHref && itemHref !== '#') {
							DDC.redirect(itemHref);
							return false;
						}
					}
				}, $.getDataAttributes(this));
				
				$(this).data('autocomplete-initialized', true).autocomplete(settings);
			}
		});
		// tabs
		$('.ui-tabs').each(function () {

			// Skip ui-tabs with lazy-load class applied.
			// Returning true in a jQuery each means 'continue'
			if ($(this).hasClass('lazy-load')) {
				return true;
			}

			var settings,
				selectedTabMarkup = "";

			$('ul:eq(0) a', this).each(function () {
				if ($(this).attr('data-href')) {
					$(this).attr('href', $(this).attr('data-href'));
					$(this).removeAttr('data-href');
				}
			});
			
			if (!$(this).data('tabs-initialized')) {
				settings = $.extend({
					cache: true,
					ajaxOptions: {
						beforeSend: function(xhr, opts) {
							// If the tab already has content, don't fetch it again
							return selectedTabMarkup === "";
						},
						success: function () {
							$(document).trigger('modulesRefresh');
						}
					},
					// Populate selectedTabMarkup on initially loaded default tab
					load: function (e, ui) {
						selectedTabMarkup = ui.panel.innerHTML;
					},
					// Populate selectedTabMarkup on subsequent manually selected tabs
					select: function (e, ui) {
						selectedTabMarkup = ui.panel.innerHTML;
					},
					show: function (e, data) {
						$(document).trigger('modulesRefresh');
						if (DDC.hasOwnProperty('Video')){
							// locate html5 or flash players in panels and stop them
							$(data.panel).parent().find('.jwplayer, .ddc-video-wrapper object').each(function () {
								var player;
								player = DDC.Video.getPlayer($(this).attr('id'));
								if (player) { player.pause(true); }
							});
						}

						// ensure any tabs specifying "empty panel on close"
						// have their panels emptied
						$(data.tab).parents('.ui-tabs').find('a').not(data.tab).each(function () {
							if ($.getDataAttributes(this).emptyPanelOnClose) {
								$($(this).attr('href')).empty();
							}
						});
						// ensure reflow in IE
						if (!$.support.changeBubbles) {
							// Re-flows source elements in IE fixing DOM overlap issues
							$(data.tab).parents('[class*=yui3-g]:eq(0)').addClass('reflow');
						}
					}
				}, $.getDataAttributes(this));

				$(this).data('tabs-initialized', true).tabs(settings);
			}
		});
		
		// sliders
		$('.range-slider:input').each(function () {
			var initData = 'slider-initialized',
				data = $.getDataAttributes(this),
				names = {
					min: 'min-' + $(this).attr('name'),
					max: 'max-' + $(this).attr('name')
				},
				classNames = {
					min: names.min.replace('.', '_'),
					max: names.max.replace('.', '_')
				},
				val = {
					min: (data.prefix || '') + (data.formatNumber ?
						formatNumber(data.min) :
						data.min) + (data.suffix || ''),
					max: (data.prefix || '') + (data.formatNumber ?
						formatNumber(data.max) :
						data.max) + (data.suffix || '')
				},
				change = function (e, ui) {
					var values = {};

					if (typeof ui.values[0] != "undefined") {
						values[names.min] = ui.values[0].toString();
					};

					if (typeof ui.values[1] != "undefined") {
						values[names.max] = ui.values[1].toString();
					};

					$(document).trigger('dynamicFields', [values]);
				},
				slide = function (e, ui) {
					var prefix = data.prefix || '',
						suffix = data.suffix || '',
						minValue = ui.values[0],
						maxValue = ui.values[1],
						dataMinValue = ui.values[0],
						dataMaxValue = ui.values[1]
					;

					if (data.formatNumber) {
						minValue = formatNumber(minValue);
						maxValue = formatNumber(maxValue);
					}

					$('.' + classNames.min).html(prefix + minValue + suffix);
					$('.' + classNames.max).html(prefix + maxValue + suffix);
					$('.' + classNames.min).attr('data-value', dataMinValue);
					$('.' + classNames.max).attr('data-value', dataMaxValue);

				},
				stop = function( e, ui) {
					var prefix = data.prefix || '',
						suffix = data.suffix || '',
						minValue = ui.values[0],
						maxValue = ui.values[1],
						dataMinValue = ui.values[0],
						dataMaxValue = ui.values[1]
					;

					if (data.formatNumber) {
						minValue = formatNumber(minValue);
						maxValue = formatNumber(maxValue);
					}

					$(this).trigger('sliderstop');
					$('.' + classNames.min).html(prefix + minValue + suffix);
					$('.' + classNames.max).html(prefix + maxValue + suffix);
					$('.' + classNames.min).attr('data-value', dataMinValue);
					$('.' + classNames.max).attr('data-value', dataMaxValue);
				};

				//generate a slider if the range values make sense
				if( (typeof data.min == "number") && (typeof data.max == "number") && data.max > data.min ) {
					data = $.extend({
						range: true,
						animate: true,
						values: [data.min, data.max],
						change: change,
						slide: slide,
						stop: stop
					}, data);

					if(typeof data.values == "string"){
						data.values = data.values.split(',');
						val.min = (data.values[0] !== undefined) ? data.prefix + data.values[0] + data.suffix : data.max;
						val.max = (data.values[1] !== undefined) ? data.prefix + data.values[1] + data.suffix : data.max;
					}

					if (!$(this).data(initData)) {
						var uiSliderValue = '<b class="ui-slider-min-value ' + classNames.min + '" data-value="' + data.min + '">' + val.min + '</b>' +
											'<span class="ui-slider-value-hyphen"> &ndash; </span>' +
											'<b class="ui-slider-max-value ' + classNames.max + '" data-value="' + data.max + '">' + val.max + '</b>';

						$(this)
							.siblings('.ui-slider-placeholder')
								.remove();
						$(this)
							.siblings('.ui-slider-value')
								.html(uiSliderValue);

						$(sliderTemplate)
							.slider(data)
							.insertAfter($(this)
								.addClass('hide')
								.data(initData, true));

						change(null, {values: data.values});
					}
				} else { // hide control
					$(this).hide();
					$(this).parent('label').hide();
				}
		});
	};
}());
