/*jslint regexp: true, browser: true, confusion: true, sloppy: true, white: true, forin: true, nomen: true, plusplus: true, maxerr: 50, indent: 4 */
(function () {
	var window = this,
		$ = window.jQuery,
		DDC = (window.DDC = (window.DDC || {}));
		
	DDC.modules.googlePlusOne.refresh = function () {
		var gapi = window.gapi;
		$(DDC.modules.googlePlusOne.selector).not('google-plus-one-initialized').each(function () {
			$(this).addClass('google-plus-one-initialized');
			var data = $.getDataAttributes(this)
			data.callback = 'googlePlusOne_callback';
			gapi.plusone.render(this, data);
		});
	};
}());

// Google PlusOne callback must be in the global namespace
// https://developers.google.com/+/plugins/+1button/
function googlePlusOne_callback(result){
	var state;
	
	if ( result.state == 'on' ) {
		state = 'plus one';
	} else if ( result.state == 'off' ) {
		state = 'minus one';
	}
	
	var data = {
		network: 'google',
		socialAction: state,
		opt_target: ''	// TODO:  allow for this to be a custom title, specified in the markup
	};
	data.opt_pagePath = document.location.pathname+document.location.search;

	if (state && state !== '') {
		$(document).trigger('trackSocialEvent', [data]);
	}
}
