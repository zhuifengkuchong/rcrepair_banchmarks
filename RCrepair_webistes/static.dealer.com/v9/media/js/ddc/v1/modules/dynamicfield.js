(function () {
	var window = this,
		$ = window.jQuery,
		DDC = (window.DDC = (window.DDC || {}));
	
	DDC.modules.dynamicField.refresh = function () {
		var dynamicFieldFormChange = function () {
				var data = DDC.getUrlParams('?' +
						$(this).serialize());
				
				/*
				 * Multiple selects don't serialize quite the way
				 * we want by default.  For the purposes of dynamic
				 * fields, we want a comma-separated list from a
				 * multiple select of all its selected options' values.
				 *
				 * This routine updates the serialized values of any
				 * multiple selects to be comma-separated lists
				 * instead of URL parameter strings (the
				 * not-so-useful default).
				 */

				var multipleCheckboxValues = [];
				$('input[type="checkbox"]', this).each(function () {
					if ($(this).is(":checked")) {
						multipleCheckboxValues.push($(this).val());
					}
					data[this.name] = multipleCheckboxValues.join(', ');
				});

				$('[multiple]', this).each(function () {
					var multipleValues = [],
						i;

					for (i = 0; i < this.options.length; i++) {
						if (this.options[i].selected) {
							multipleValues.push($(this.options[i]).val());
						}
					}
					
					data[this.name] = multipleValues.join(', ');
				});
				
				$(document).trigger('dynamicFields', [data]);
			};
		
		$(DDC.modules.dynamicField.selector).each(function () {
			var self = this;
			
			if (!$(this).data('dynamic-field-initialized')) {
				if ($(this).is('option')) {
					$(this)
						.attr('data-text', $(this).text())
						.text('');
				} else if (!$(this).attr('data-val')) {
					$(this)
						.attr('data-val', $(this).val())
						.val('');
				}
				$(this).data('dynamic-field-initialized', true);
			}
			if (!$(this).parents('form').data('dynamic-field-initialized')) {
				$(this).parents('form')
					.bind('change', dynamicFieldFormChange)
					.data('dynamic-field-initialized', true);
				
				setTimeout(function () {
					$(self).trigger('change');
				}, 250);
			}
		});
	};
}());
