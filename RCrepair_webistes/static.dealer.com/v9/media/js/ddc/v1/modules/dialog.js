(function () {
	var window = this,
		$ = window.jQuery,
		DDC = (window.DDC = (window.DDC || {}));

	DDC.modules.dialog.refresh = function () {
		var beforeClose = function () {
				var name = $(this).dialog('option', 'name'),
					href = $(this).dialog('option', 'href'),
					data = '',
					destroyOnClose = $(this).dialog('option', 'destroyOnClose');

				if (typeof href !== 'object') {
					data = href;
				}
				if (typeof name !== 'object') {
					data = name;
				}
				if (typeof destroyOnClose === 'object') {
					destroyOnClose = false;
				}

				$(document)
					.trigger('modulesRefresh')
					.trigger('trackEvent', [{
						widgetName: 'dialog',
						eventName: 'close',
						data: data,
						value: Math.round(((new Date()).getTime() -
							$(this).dialog('option', 'openTime')) /
								1000)
					}]);

				$('video').each(function () {
					$(this)[0].pause();
				});

				if (destroyOnClose) {
					// Destroy on close without a trace.
					// Useful for dialogs which would leave
					// dirty, running junk in the DOM (Flash, Iframe).
					DDC.dialogs[data] = null;
					$(this).dialog('destroy').remove();
				}
			},
			open = function () {
				var name = $(this).dialog('option', 'name'),
					href = $(this).dialog('option', 'href'),
					autoSize = $(this).dialog('option', 'autoSize'),
					closeAll = $(this).dialog('option', 'closeAll'),
					popup = $(this).dialog('option', 'popup'),
					data = '',
					noninteraction = false;

				if (typeof href !== 'object') {
					data = href;
				}
				if (typeof name !== 'object') {
					data = name;
				}

				if (typeof closeAll !== 'object') {
					$(document).trigger('dialogClose', [{
						except: data
					}]);
				}

				if (autoSize) {
					$(this).dialog('option', 'height', 'auto');
				}
				if (autoSize && $(this).height() > ($(window).height() - 40)) {
					$(this).dialog('option', 'height', $(window).height() - 40);
				}

				// prevents tracking on popup dialogs
				if(typeof popup !== 'object' && popup === true){
					nonInteraction = true;
				}
				$(document)
					.trigger('modulesRefresh')
					.trigger('trackEvent', [{
					widgetName: 'dialog',
					eventName: 'open',
					data: data,
					noninteraction: noninteraction
				}]);

				var dialog = $(this).dialog('option', 'openTime', (new Date()).getTime())
							.dialog('option', 'position', 'center'),
					dialogClasses = dialog.find('.mod, .ddc-content').context.className.split(/\s+/),
					trackingClass,
					trackingClassOverride;

				/**
				 * Tracks clicks on jQuery UI tabs as events
				 * for Google Analytics statistics.
				 */

				for (var i = dialogClasses.length - 1; i >= 0; i--){
					// If the current class is 'dialog', add 1
					// to the current iteration and set the trackingClass
					// var to that. The class after the dialog class is
					// typically what we want to track in analytics.
					if ( dialogClasses[i] === 'dialog' ) {
						trackingClass = dialogClasses[i+1];
					}

					// If the dialog class contains showroom
					// reset the class to 'showroom'
					if ( dialogClasses[i].indexOf('showroom') ) {
						trackingClassOverride = 'showroom';
					}
				}

				if (trackingClassOverride) {
					trackingClass = trackingClassOverride;
				}

				dialog.bind('tabsshow', function(event, ui) {
					if(ui.panel.className) {
						var target = $(event.target).find('a').eq(ui.index),
								label = $.trim(target.html().replace(/<\/?[^>]+>/gi, '').toLowerCase());

						$(document).trigger('trackEvent', {
							widgetName: trackingClass,
							eventName: 'sub-tab',
							data: label
						});
					}
				});
			};

		if (!$(document).data('dialog-initialized')) {
			$(document).data('dialog-initialized', true)
				.undelegate('.dialog[href], .dialog[data-href]', 'click.dialog')
				.delegate('.dialog[href], .dialog[data-href]', 'click.dialog', function () {
					var data = $.extend({
						resizable: false,
						modal: true,
						draggable: false,
						beforeClose: beforeClose,
						open: open,
						show: 'fade',
						hide: 'fade'
					}, $.getDataAttributes(this));

					// if regular href contains a hash, it's not intended for use
					// a hash can occur at an index other than 0 in IE
					data.href = ($(this).attr('href') && ($(this).attr('href').indexOf('#') === -1)) ?
						$(this).attr('href') :
						data.href;

					if (!data.title) {
						data.title = $(this).text();
					}

					$(document).trigger('dialog', [data]);
					return false;
				});
		}

		$('.dialog.mod, .dialog.ddc-content').each(function () {
			var data;
			if (!$(this).data('dialog-initialized')) {
				data = $.extend({
					el: this,
					resizable: true,
					draggable: true,
					beforeClose: beforeClose,
					open: open,
					show: 'fade',
					hide: 'fade'
				}, $.getDataAttributes(this));

				if (!data.title) {
					data.title = $('h1:eq(0)', this).text();
				}

				$(this).data('dialog-initialized', true);
				$(document).trigger('dialog', [data]);
			}
		});

		$('.dialog[data-popup]').each(function () {
			if ($(this).attr('data-popup') === 'true') {
				var data = $.extend({
					href: $(this).attr('data-href'),
					resizable: false,
					modal: true,
					draggable: false,
					beforeClose: beforeClose,
					open: open,
					show: 'fade',
					hide: 'fade'
				}, $.getDataAttributes(this));

				$(this).remove();
				$(document).trigger('dialog', [data]);
			}
		});
	};
}());

