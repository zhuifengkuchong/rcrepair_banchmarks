/**
*	Embedding code for JWPlayer to use Dealer.com playlists
*	This gives an API to embed the player with standard jwplayer options or uses data attributes of the target element
*	The resulting videos are registered in the DDC.Video global object
*	Copyright (c) 2014 Dealer.com.
*
*	Requires:
*	JQuery 1.5+
*	JQuery cookie plugin (only if using preroll functionality)
*
*	Created: 15.July.2013, Orson Bradford
*	Last modified: 3.February.2014, Orson Bradford
*/

/*! Allow CORS header support for ajax requests in IE
 * jQuery-ajaxTransport-XDomainRequest - v1.0.1 - 2013-10-17
 * https://github.com/MoonScript/jQuery-ajaxTransport-XDomainRequest
 * Copyright (c) 2013 Jason Moon (@JSONMOON)
 * Licensed MIT (/blob/master/LICENSE.txt)
 */
(function($){if(!$.support.cors&&$.ajaxTransport&&window.XDomainRequest){var n=/^https?:\/\//i;var o=/^get|post$/i;var p=new RegExp('^'+location.protocol,'i');var q=/text\/html/i;var r=/\/json/i;var s=/\/xml/i;$.ajaxTransport('* text html xml json',function(i,j,k){if(i.crossDomain&&i.async&&o.test(i.type)&&n.test(i.url)&&p.test(i.url)){var l=null;var m=(j.dataType||'').toLowerCase();return{send:function(f,g){l=new XDomainRequest();if(/^\d+$/.test(j.timeout)){l.timeout=j.timeout}l.ontimeout=function(){g(500,'timeout')};l.onload=function(){var a='Content-Length: '+l.responseText.length+'\r\nContent-Type: '+l.contentType;var b={code:200,message:'success'};var c={text:l.responseText};try{if(m==='html'||q.test(l.contentType)){c.html=l.responseText}else if(m==='json'||(m!=='text'&&r.test(l.contentType))){try{c.json=$.parseJSON(l.responseText)}catch(e){b.code=500;b.message='parseerror'}}else if(m==='xml'||(m!=='text'&&s.test(l.contentType))){var d=new ActiveXObject('Microsoft.XMLDOM');d.async=false;try{d.loadXML(l.responseText)}catch(e){d=undefined}if(!d||!d.documentElement||d.getElementsByTagName('parsererror').length){b.code=500;b.message='parseerror';throw'Invalid XML: '+l.responseText;}c.xml=d}}catch(parseMessage){throw parseMessage;}finally{g(b.code,b.message,c,a)}};l.onprogress=function(){};l.onerror=function(){g(500,'error',{text:l.responseText})};var h='';if(j.data){h=($.type(j.data)==='string')?j.data:$.param(j.data)}l.open(i.type,i.url);l.send(h)},abort:function(){if(l){l.abort()}}}}})}})(jQuery);

window.DDC = window.DDC || {};

// ************************************
// Register as Global Module
// ************************************

DDC.Video = (function ($) {
	"use strict";
	var version = 'http://static.dealer.com/global/media/js/ddc/apis/videoplayer/v2/2.14',
		// module object to hold externally accessible methods and properties
		module = {},
		supportsTouch = 'ontouchstart' in window || navigator.msMaxTouchPoints,
		ddcHostName = window.location.protocol+"//static.dealer.com/",
		jwPlayerkey = 'CuRkq2mEg9sCOY9v/r3gGjfwpRHO+3kZk713oA==',
		// Falling back to jwplayer 6.5 until the skin performance has been improved
		jwplayerPath = ddcHostName + 'global/media/js/ddc/apis/videoplayer/v2/jwplayer/v6.5/jwplayer.js',
		flashplayerPath = ddcHostName + 'global/media/js/ddc/apis/videoplayer/v2/jwplayer/v6.5/jwplayer.flash.swf',
		html5playerPath = ddcHostName + 'global/media/js/ddc/apis/videoplayer/v2/jwplayer/v6.5/jwplayer.html5.js',
		// Optional proxy for cross domain ajax requests using the DDC Request Proxy Portlet
		// (works for playlists and captions files but not skin xml files because jwplayer can't handle proxy escape chars)
		// proxyPath = 'http://static.dealer.com/proxy.htm?_minify=false&url=',
		proxyPath = null,
		prerollPathPattern = window.location.protocol+'//videos2.dealer.com/clients/%INIT%/%ID%/preroll.xml',
		postrollPathPattern = window.location.protocol+'//videos2.dealer.com/clients/%INIT%/%ID%/postroll.xml',
		skinsPath = ddcHostName + 'global/media/js/ddc/apis/videoplayer/v2/jwplayer/v6.5/skins/',
		localeSkins = {
			'glowddc' : {
				'en_US' : skinsPath + 'glowddc/glowddc_en.xml',
				'en_CA' : skinsPath + 'glowddc/glowddc_en.xml',
				'es_US' : skinsPath + 'glowddc/glowddc_es.xml',
				'fr_CA' : skinsPath + 'glowddc/glowddc_fr.xml'
			}
		},
		jwDefaults = {
			primary : (isIE() && isIE() < 10) ? 'flash' : 'html5',
			flashplayer : flashplayerPath,
			html5player : html5playerPath,
			width : 480,
			height : 270,
			stretching : 'uniform',
			skin : localeSkins.glowddc.en_US
		},
		// note these wrapper defaults will take priority over data attributes of target element, so leave undefined to override
		// keep all lowercase for normalization with element data attributes
		// see DDCVideoWrapper definition for more details
		defaults = {
			target: '.ddcPlayer-holder',
			targetid : undefined,
			playerid : undefined,
			wrapperelement: '<div class="ddc-video-wrapper" />',
			accountid : undefined,
			vehicleid : undefined,
			videourl : undefined,
			ddcplaylist : undefined,
			useprepostroll : undefined,
			prerollplaylist : undefined,
			postrollplaylist : undefined,
			locale : undefined, // if set, uses the appropriate version of the localeskin below, rather than from the jwsettings
			localeskin : 'glowddc',
			jwplaylist : [],
			jwsettings: {}
		},
		jwPlayerScriptPromise,
		trackerSettings = {
			ddc : {
				viewTypeVideo : 16
			},
			ga : {
				id : 'UA-9899509-3',
				category : 'Video'
			}
		},
		tracker = {
			ddc : undefined,
			ga : undefined
		},
		// use absolute paths for third party (CarSales) support
		// TODO - can use the ddcHostName var once that is static.dealer.com
		ddcTrackerPath = ddcHostName + 'common/tracker/tracker.min.js',
		ddcTrackerScriptPromise;

	// ************************************
	// DDC.Video exposed methods and properties
	// ************************************

	module.version = version;
	module.jwplayerPath = jwplayerPath;
	module.proxyPath = proxyPath;
	module.localeSkins = localeSkins;
	module.jwDefaults = jwDefaults;
	module.wrappers = [];

	// hoisted from function definitions below
	module.init = initEachWrapper;
	module.embed = embedEachWrapper;
	module.onReady = onReadyEachWrapper;
	module.getWrapper = getWrapper;
	module.getAllWrappers = getAllWrappers;
	module.getPlayer = getPlayer;
	module.getAllPlayers = getAllPlayers;
	module.getAllPlayerIds = getAllPlayerIds;

	/**
	* Remove all players cleanly when unloading page
	* Chrome has buffering errors JIRA-31593
	*/
	window.onbeforeunload = function () {
		var allplayers = [];
		try {
			allplayers = DDC.Video.getAllPlayers();

			for (var i = 0; i < allplayers.length; i++) {
				allplayers[i].remove();
			}
		} catch (e) {
			if (window.console && window.console.log) {
				console.log('DDC.Video unable to clean up players.');
			}
		}
	};

	/**
	* Simple IE check
	*/
	function isIE() {
		var userAgent = navigator.userAgent.toLowerCase();
		return (userAgent.indexOf('msie') != -1) ? parseInt(userAgent.split('msie')[1], 10) : false;
	}

	/**
	* Ajax Load JWPlayer Scripts
	*/
	function getJWPlayerScript() {
		var deferred = $.Deferred();

		// Reference for handlers which depend on jwplayer being available
		jwPlayerScriptPromise = deferred.promise();

		$.ajax({url: DDC.Video.jwplayerPath,
			dataType: 'script',
			cache: true
		}).fail(function (jqXHR, textStatus, errorThrown) {
			if (window.console && window.console.log) {
				console.log('Videoplayer unable to load jwplayer script');
			}
			deferred.reject();
		}).done(function (jqXHR, textStatus) {
			window.jwplayer.key = jwPlayerkey;
			deferred.resolve();
		});
	}

	/**
	* Ajax Load DDC Tracker
	*/
	function getDDCTracker() {
		var deferred = $.Deferred();

		// Reference for resolution handlers which depend on ddcTracker being available
		ddcTrackerScriptPromise = deferred.promise();

		$.ajax({url: ddcTrackerPath,
			dataType: 'script',
			cache: true
		}).fail(function (jqXHR, textStatus, errorThrown) {
			if (window.console && window.console.log) {
				console.log('Videoplayer DDC Tracker not found.');
			}
			deferred.reject();
		}).done(function (jqXHR, textStatus) {
			deferred.resolve();
		});
	}

	/**
	* Constructor for the wrapper object
	*
	* @param Object options An object literal containing key/value pairs used by the Dealer.com JWPlayer wrapper.
	* @option mixed target A jQuery selector or jQuery object reference of the target element (which will contain the video).
	* @option string targetid Id for the element which will contain the player ('target' above takes precedence).
	* @option string playerid id for the player
	* @option string wrapperelement HTML string to wrap video player with (since jwPlayer API gives no control over internal wrapper elements) 
	* @option string accountid DDC account id used to get pre/post roll paths
	* @option string vehicleid DDC vehicle id (currently unused, but included from legacy v8 player)
	* @option string ddcplaylist Path to a Dealer.com XML playlist file listing video assests for video player.
	* @option string useprepostroll Flag to indicate whether to try to load pre/post roll playlists.
	* @option string prerollplaylist Path to a Preroll XML playlist file listing video assests for video player.
	* @option string postrollplaylist Path to a Postroll XML playlist file listing video assests for video player.
	* @option string videourl direct URL for a video file; only used if a DDC playlist is not provided.
	* @option string locale language for text controls: en_US, es_US, en_CA, fr_CA
	* @option Object jwplaylist A jwPlayer playlist object which will get used in the jwsettings at embed time
	* @option Object jwsettings A jwPlayer options object (http://www.longtailvideo.com/support/jw-player/28839/embedding-the-player/)
	*/
	var DDCVideoWrapper = function (options) {
		this.options = options;
		this.playlistPromise = undefined;
		this.mainPlaylistPromise = undefined;
		this.prerollPlaylistPromise = undefined;
		this.postrollPlaylistPromise = undefined;
		this.hasPreroll  = undefined;
		this.jwPlayerInstance = undefined;
		this.playerState = 'created'; // change based on jwPlayer ready state. Either 'created', 'initializing', 'ready'
	};

	DDCVideoWrapper.prototype.init = init;
	DDCVideoWrapper.prototype.embed = embed;
	DDCVideoWrapper.prototype.onEmbed = onEmbed;
	DDCVideoWrapper.prototype.onReady = onReady;

	/**
	* Constructor for collection of wrappers created as a batch
	*
	* This maps the basic methods 'init', 'embed', 'onReady' to versions of these functions
	* which iterate over each wrapper in the collection. 'doFnEachWrapper' is a generic function which gets used to do this.
	*
	* @param Object options (See DDCVideoWrapper) shared in all wrappers in this collection
	*/
	var DDCVideoWrapperCollection = function (options) {
		this.wrappers = [];
		this.options = options;
	};

	DDCVideoWrapperCollection.prototype.addWrapper = addWrapper;
	DDCVideoWrapperCollection.prototype.getWrapper = getWrapper;
	DDCVideoWrapperCollection.prototype.init = initEachWrapper;
	DDCVideoWrapperCollection.prototype.embed = embedEachWrapper;
	DDCVideoWrapperCollection.prototype.onReady = onReadyEachWrapper;
	DDCVideoWrapperCollection.prototype.doFnEachWrapper = doFnEachWrapper;

	/**
	* Put a wrapper into the collection
	* (either global Video context or for given DDCVideoWrapperCollection)
	*
	* @param DDCVideoWrapper wrapperObj wrapper to add
	*/
	function addWrapper(wrapperObj) {
		var wrapperAdded;

		if (wrapperObj instanceof DDCVideoWrapper && !this.getWrapper(wrapperObj.options.targetid)) {
			this.wrappers.push(wrapperObj);
			wrapperAdded = wrapperObj;
		}

		return wrapperAdded;
	}

	/**
	* Get a wrapper from the collection by its target id, the player id or a jquery reference
	* (either in global DDC.Video context or for given DDCVideoWrapperCollection)
	*
	* @param mixed reference 
	*/
	function getWrapper(reference) {

		var refId,
			wrapper;

		if (typeof reference === 'string') {
			refId = reference;
		} else if (reference instanceof jQuery) {
			refId = reference.attr('id');
		}

		if (reference) {
			for (var i = this.wrappers.length - 1; i >= 0; i--) {
				if ((this.wrappers[i].options.targetid === refId) || (this.wrappers[i].options.playerid === refId)) {
					wrapper = this.wrappers[i];
					break;
				}
			}
		}

		return wrapper;
	}

	/**
	* Get array of all wrappers registered globally
	*
	*/
	function getAllWrappers() {

		return this.wrappers;
	}

	/**
	* Get a jwplayer instance from id
	* (either global Video context or for given DDCVideoWrapperCollection)
	*
	* @param mixed reference
	*/
	function getPlayer(reference) {
		var refId,
			wrapper,
			player;

		if (typeof reference === 'string') {
			refId = reference;
		} else if (reference instanceof jQuery) {
			refId = reference.attr('id');
		}

		wrapper = this.getWrapper(reference);
		if (wrapper) {
			player = wrapper.jwPlayerInstance;
		}

		// fallback to jwplayer built-in method
		if (!player && window.jwplayer) {
			player = window.jwplayer(refId);
		}

		return player;
	}

	/**
	* Get array of all jwplayer instances registered globally
	*
	*/
	function getAllPlayers() {
		var players = [];
		for (var i = this.wrappers.length - 1; i >= 0; i--) {
			if (this.wrappers[i].jwPlayerInstance) {
				players.push(this.wrappers[i].jwPlayerInstance);
			}
		}

		return players;
	}

	/**
	* Get array of all jwplayer ids registered globally
	*
	*/
	function getAllPlayerIds() {
		var playerIds = [];
		for (var i = this.wrappers.length - 1; i >= 0; i--) {
			playerIds.push(this.wrappers[i].options.playerid);
		}

		return playerIds;
	}

	/**
	* Wrapper function which routes init method through doFnEachWrapper 
	*
	* @param Object options (See DDCVideoWrapper)
	* @return DDCVideoWrapperCollection
	*/
	function initEachWrapper(options) {
		// ************************************
		// Init Each Wrapper
		// ************************************
		return doFnEachWrapper.call(this, init, options);
	}

	/**
	* Wrapper function which routes embed method through doFnEachWrapper 
	*
	* @param Object options (See DDCVideoWrapper)
	* @return DDCVideoWrapperCollection
	*/
	function embedEachWrapper(options) {
		// ************************************
		// Embed Each Wrapper
		// ************************************
		return doFnEachWrapper.call(this, embed, options);
	}

	/**
	* This is an abstraction to allow for some reuse between initEachWrapper() and embedEachWrapper()
	* If these two functions diverge, the stubs above could become distinct methods
	* 
	* @param Function fn A DDCVideoWrapper method which to call on each wrapper instance
	* @param Object options (See DDCVideoWrapper)
	* @return DDCVideoWrapperCollection current collection, or a new one if not set
	*/
	function doFnEachWrapper(fn, options) {
		var collection,
			$target;

		// Normalize fn to function reference, if passed as a string
		if (typeof fn === 'string') {
			if (DDCVideoWrapper.prototype[fn] && typeof DDCVideoWrapper.prototype[fn] === 'function') {
				fn = DDCVideoWrapper.prototype[fn];
			}
		}

		// Get jwPlayer script if not already loaded
		if (!window.jwplayer) {
			getJWPlayerScript();
		}

		// ************************************
		// Set up Tracking
		// ************************************

		if (!DDC.Tracker) {
			getDDCTracker();
		}

		if (!tracker.ga && !tracker.ddc) {
			tracker = configureTracker(tracker, trackerSettings);
		}

		// ************************************
		// Instantiate Collection Object
		// ************************************

		if (this instanceof DDCVideoWrapperCollection) {
			collection = this;
			collection.options = mergeOptions(collection.options, options);
		} else {
			// populate with defaults
			options = mergeOptions(defaults, options);
			collection = new DDCVideoWrapperCollection(options);
		}

		// ************************************
		// Acquire $target
		// ************************************

		$target = acquireTarget(collection.options);

		// ************************************
		// Delegate Fn calls per target
		// (init or embed)
		// ************************************

		if ($target && $target.length > 0) {
			$target.each(function (i, targetEl) {
				var itemWrapperInstance,
					itemOptions = $.extend(true, {}, collection.options),
					$itemTarget = $(targetEl),
					// 0 indexed counter for the player Ids: ddc-jwplayer-0, ...
					defaultPlayerId = ('ddc-jwplayer-' + DDC.Video.wrappers.length);

				// If not set, retrieve or generate unique targetid

				if (!itemOptions.targetid) {
					if ($itemTarget.attr('id')) {
						itemOptions.targetid = $itemTarget.attr('id');
					} else {
						itemOptions.targetid = defaultPlayerId + '-target';
						$itemTarget.attr('id', itemOptions.targetid);
					}
				}

				// Reset the existing 'target' to be this specific unique element - otherwise the generic selector will remain on these options and it will pick up multiple targets, in the individual video init calls
				itemOptions.target = $itemTarget;

				// Override an existing wrapper object, if there is one
				itemWrapperInstance = DDC.Video.getWrapper(itemOptions.targetid);

				// Or create a new one
				if (!itemWrapperInstance) {

					if (!itemOptions.playerid) {
						itemOptions.playerid = defaultPlayerId;
					}

					itemWrapperInstance = new DDCVideoWrapper(itemOptions);
					collection.addWrapper(itemWrapperInstance);
					// add to global collection as well. method isn't exposed on DDC.Video, so using call()
					addWrapper.call(DDC.Video, itemWrapperInstance);
				}

				// Try to call argument 'fn' (init or embed) on the wrapper instance
				if (typeof fn === "function") {
					try {
						fn.call(itemWrapperInstance, itemOptions);
					} catch (e) {
						if (window.console && window.console.log) {
							console.log('Not able to run function on wrapper');
						}
					}
				}
			});
		} else {
			// No targets. May not exist in DOM yet on init, and could exist later when call embed.
			// TODO - to be most efficient, playlist ajax calls should be made on init even without $target as long as playlist paths are defined in options (as opposed to in data attribs)
		}

		return collection;
	}

	/**
	* Unlike previous this is NOT a wrapper to doFnEachWrapper
	* It passes a function which is run on each wrapper's onReady callback 
	* So the callback function will repeat once per wrapper 
	*
	* @param Function fn A user-defined callback function
	* @return DDCVideoWrapperCollection
	*/
	function onReadyEachWrapper(fn) {
		var collection;

		// ************************************
		// Instantiate Collection Object
		// ************************************

		if (this instanceof DDCVideoWrapperCollection) {
			collection = this;
		} else {
			collection = new DDCVideoWrapperCollection();
			collection.wrappers = [];
			collection.init();
		}

		for (var i = this.wrappers.length - 1; i >= 0; i--) {
			if (collection.wrappers[i] instanceof DDCVideoWrapper) {
				collection.wrappers[i].onEmbedReadyCallback = fn;
			}
		}

		return collection;
	}

	/**
	* Extend "Base Options" (usually either collection level options or defaults) with more specific player options
	*
	* @param Object baseOptions (See DDCVideoWrapper)
	* @param Object options (See DDCVideoWrapper)
	* @return Object merged clone of options merged with base group options
	*/
	function mergeOptions(baseOptions, options) {
		var merged = {};

		if (baseOptions && typeof baseOptions === "object") {
			baseOptions = normalizeOptions(baseOptions);
		} else {
			baseOptions = {};
		}

		if (options && typeof options === "object") {
			options = normalizeOptions(options);
		} else {
			options = {};
		}

		// deep extend the remaining options, so as to properly merge nested jwsettings, if any
		$.extend(true, merged, baseOptions, options);

		return merged;
	}

	/**
	* Lowercase all option names to disambiguate and simplify merging
	* Also catch certain synonymous option names
	*
	* @param Object options (See DDCVideoWrapper)
	* @return Object normOptions normalized values
	*/
	function normalizeOptions(options) {
		var normOptions = {},
			normOption;

		for (var option in options) {
			if (options.hasOwnProperty(option)) {
				// normalize case - all options are lowercase
				normOption = option.toString().toLowerCase();

				// trap naming variants
				switch (normOption) {

				case 'autoplay':
					if (options[option] === true || options[option] === 'true') {
						normOptions.autostart = true;
					}
					break;

				case 'divname':
					normOptions.targetid = options[option];
					break;

				case 'height':
				case 'width':
					// specify without 'px' units, but '%' is allowed
					normOptions[normOption] = (options[option].toString().indexOf('%') > 0) ? parseInt(options[option], 10) + '%' : parseInt(options[option], 10).toString();
					break;

				case 'ddcplaylist':
				case 'ddcplaylisturl':
				case 'playlistxmlpath':
					normOptions.ddcplaylist = getProxiedPath(options[option]);
					break;

				case 'postrollxmlpath':
					normOptions.postrollplaylist = getProxiedPath(options[option]);
					break;

				case 'prerollxmlpath':
					normOptions.prerollplaylist = getProxiedPath(options[option]);
					break;

				case 'showcontrols':
					normOptions.controls = options[option];
					break;

				case 'allowfullscreen':
				case 'calltoactionpath':
				case 'overlaypath':
				case 'showplaylist':
				case 'swfid':
					//unused legacy options
					break;

				default:
					normOptions[normOption] = options[option];
					break;
				}
			}
		}

		return normOptions;
	}

	/**
	* Look up label translations
	*
	* @param String label
	* @return String translated
	*/
	var getLabel = function (label) {
		var i18n = (window.DDC.i18n || {}),
			translated = label;

		if (i18n.getLabel) {
			translated = DDC.i18n.getLabel(label);
		}

		if (!translated) {
			translated = label;
		}

		return translated;
	};

	/**
	* Tease out jwPlayer settings from wrapper options
	* With fallback to jwDefaults
	*
	* @param Object options (See DDCVideoWrapper)
	* @param Object jwDefaults a jwPlayer settings object
	* @return Object jwSettings a jwPlayer settings object
	*/
	function getJWSettings(options, jwDefaults) {
		var jwSettings = {},
			localeSkins;

		// Set the exact skin file for the locale, if available
		// Proxied skin files won't get handled properly by the jwplayer, so not attemptin to go through proxy
		if (options.locale && options.localeskin) {
			// check if the localeSkins have been overridden
			localeSkins = DDC.Video.localeSkins;
			if (localeSkins.hasOwnProperty(options.localeskin) && localeSkins[options.localeskin].hasOwnProperty(options.locale)) {
				options.jwsettings.skin = localeSkins[options.localeskin][options.locale];
			}
		}

		// Transfer any wrapper options which apply into the jwplayer settings
		for (var option in options) {
			if (option === 'jwsettings') {
				$.extend(true, jwSettings, options.jwsettings);
			} else if (option === 'jwplaylist') {
				jwSettings.playlist = options.jwplaylist;
			} else {
				// if option not defined in the defaults, assume we are dealing with a jwPlayer setting and grab it
				if (options.hasOwnProperty(option) && !defaults.hasOwnProperty(option)) {
					jwSettings[option] = options[option];
				}
			}
		}

		jwSettings = $.extend(true, {}, jwDefaults, jwSettings);

		return jwSettings;
	}

	/**
	* Find the DOM element to attach the video player to  
	*
	* @param Object options (See DDCVideoWrapper)
	* @param Object jwPlayerInstance jwPlayer object
	* @return jQuery $target
	*/
	function acquireTarget(options, wrapperInstance) {
		var $target = $(),
			target,
			targetid,
			alphaRegex = /[A-Za-z]/;

		if (typeof options == "object") {
			target = options.target;
			targetid = options.targetid;

			// if instance already exists, target it
			if (wrapperInstance && wrapperInstance.jwPlayerInstance) {
				if (wrapperInstance.options.target instanceof jQuery) {
					$target = wrapperInstance.options.target;
				} else {
					// can't target existing video player
				}
			} else if (target instanceof jQuery) {
				// param is already jQuery reference
				$target = target;
			} else if (targetid) {
				// jwPlayer html5 mode is strict about invalid ids, and will not embed if it's an invalid id
				if (alphaRegex.test(targetid[0])) {
					$target = $('#' + targetid);
				} else {
					// invalid element id
				}
			} else if (target) {
				// try to select element via jQuery
				$target = $(target);
			} else {
				if (window.console && window.console.log) {
					console.log('Videoplayer unable to find embed target.');
				}
			}
		}

		return $target;
	}

	/**
	* Take options and load playlist files to prepare video for embedding.
	*
	* @param Object options See DDCVideoWrapper
	* @return DDCVideoWrapper
	*/
	function init(options) {
		// return object is a wrapper instance (for chaining methods)
		var wrapperInstance,
			$target,
			simpleFormatObj = {'file' : undefined},
			prerollPlaylist,
			mainPlaylist,
			postrollPlaylist;

		// ************************************
		// Instantiate Wrapper Object
		// ************************************

		if (this instanceof DDCVideoWrapperCollection) {
			this.initAll(options);
			wrapperInstance = null;
		} else if (this instanceof DDCVideoWrapper) {
			wrapperInstance = this;
			wrapperInstance.options = mergeOptions(this.options, options);
		} else {
			// New instance, use the DDCVideoWrapper definition and register in the outer scope list
			wrapperInstance = new DDCVideoWrapper(options);
		}

		if (wrapperInstance) {
			// ************************************
			// Acquire $target
			// ************************************

			// Note that it is not critical that the target element be available at init, only at embed
			// But $target data attribute may hold the video paths extracted by buildPlaylist, so need to check for it
			$target = acquireTarget(wrapperInstance.options, wrapperInstance);

			// ************************************
			// Get Any Target Options from Data Attr
			// ************************************

			// sanity check, jwplayer can only have one target
			if ($target && $target.length == 1) {
				// nested mergeOptions statement to ensure proper overrides: manual options > data attributes > wrapper defaults
				wrapperInstance.options = mergeOptions(wrapperInstance.options, mergeOptions(parseTargetVideoOptions($target), options));
			}

			// ************************************
			// Get playlists
			// ************************************

			if (wrapperInstance.options.videourl) {

				simpleFormatObj.file = wrapperInstance.options.videourl;

				wrapperInstance.options.jwplaylist.push(simpleFormatObj);

			} else if (!wrapperInstance.playlistPromise) {
				// make sure we haven't already requested the playlists (in case of rapid repeat init calls)

				// MAIN PLAYLIST
				wrapperInstance.mainPlaylistPromise = loadPlaylist(wrapperInstance.options.ddcplaylist, wrapperInstance.options.locale);

				// PRE / POST ROLL PLAYLISTS
				if (wrapperInstance.options.useprepostroll || wrapperInstance.options.postrollplaylist || wrapperInstance.options.prerollplaylist) {

					if (!wrapperInstance.options.prerollplaylist) {
						prerollPlaylist = getProxiedPath(prerollPathPattern.replace('%INIT%', wrapperInstance.options.accountid.slice(0, 1)).replace('%ID%', wrapperInstance.options.accountid));
						// for example http://videos2.dealer.com/clients/d/ddcdemo/preroll.xml
					} else {
						prerollPlaylist = getProxiedPath(wrapperInstance.options.prerollplaylist);
					}

					if (!wrapperInstance.options.postrollplaylist) {
						postrollPlaylist = getProxiedPath(postrollPathPattern.replace('%INIT%', wrapperInstance.options.accountid.slice(0, 1)).replace('%ID%', wrapperInstance.options.accountid));
						// for example http://videos2.dealer.com/clients/d/ddcdemo/postroll.xml
					} else {
						postrollPlaylist = getProxiedPath(wrapperInstance.options.postrollplaylist);
					}

					// PREROLL PLAYLIST
					if ($.cookie && $.cookie('prerollViewed')) {
						wrapperInstance.prerollPlaylistPromise = $.Deferred().resolve();
					} else {
						wrapperInstance.prerollPlaylistPromise = loadPlaylist(prerollPlaylist, wrapperInstance.options.locale, simpleFormatObj);
					}

					// POSTROLL PLAYLIST
					wrapperInstance.postrollPlaylistPromise = loadPlaylist(postrollPlaylist, wrapperInstance.options.locale, simpleFormatObj);

					// When all the promises resolve
					wrapperInstance.playlistPromise = $.when(wrapperInstance.prerollPlaylistPromise, wrapperInstance.mainPlaylistPromise, wrapperInstance.postrollPlaylistPromise).done(function (preVideoList, mainVideoList, postVideoList) {
						var playlist = [];
						if (mainVideoList && mainVideoList.file) {
							if (preVideoList && preVideoList.file) {
								preVideoList.image = mainVideoList.image;
								wrapperInstance.hasPreroll = true;
								playlist.push(preVideoList);
							}

							playlist.push(mainVideoList);

							if (postVideoList && postVideoList.file) {
								playlist.push(postVideoList);
							}
						}
						wrapperInstance.options.jwplaylist = playlist;
					});

				} else {
					wrapperInstance.playlistPromise = $.when(wrapperInstance.mainPlaylistPromise).done(function (mainVideoList) {
						var playlist = [];
						if (mainVideoList && mainVideoList.file) {
							playlist.push(mainVideoList);
							wrapperInstance.options.jwplaylist = playlist;
						}
					});
				}
			} // end if playlistPromise defined
		} // end if wrapperInstance

		return wrapperInstance;
	}

	/**
	* Utility function to get the path using the proxyPath config (or not, if on same domain)
	*
	* @param String path path to transform
	* @return String proxiedPath
	*/
	function getProxiedPath(path) {
		var proxiedPath;

		if (typeof path == 'string') {
			if ((path.indexOf('http:') !== 0 && path.indexOf('https:') !== 0) || (path.indexOf(window.location.hostname.toString()) === 0) || !DDC.Video.proxyPath) {
				// no need to proxy if on same domain
				proxiedPath = path;
			} else {
				proxiedPath = DDC.Video.proxyPath + encodeURIComponent(path);
			}

			// videos.dealer.com CDN requires the %20 escape, rather than '+' separator
			proxiedPath = proxiedPath.replace(/\+/g, '%20');
		}

		return proxiedPath;
	}

	/**
	* Utility function to get the original path back out of a proxiedPath
	*
	* @param String path path to transform
	* @param String path proxyPath proxy path to remove from the main path arg 
	* @return String proxiedPath
	*/
	function getOriginalPath(path) {
		var originalPath;

		if (typeof path == 'string') {
			if (path.indexOf(DDC.Video.proxyPath) === 0) {
				originalPath = decodeURIComponent(path.substring(DDC.Video.proxyPath.length, path.length + 1));
			} else {
				originalPath = path;
			}

			// videos.dealer.com CDN requires the %20 escape, rather than '+' separator
			originalPath = originalPath.replace(/\+/g, '%20');
		}

		return originalPath;
	}

	/**
	* Given an element, try to get the video options out of its attributes
	* Normalize the options to account for naming variation across systems
	*
	* @param Object $target jQuery element
	* @return Object normOptions
	*/
	function parseTargetVideoOptions($target) {
		var attr,
			data = {},
			accountId,
			autoplay,
			ddcPlayerInfo,
			ddcPlayerInfoArray,
			normOptions = {},
			normOption;

		// ************************************
		// Raw Attributes
		// ************************************

		ddcPlayerInfo = $target.attr('ddcplayerinfo');

		// used to help parse the ddcplayerinfo dash separated values
		accountId = $target.attr('hyphenatedaccountId');

		// used to fix V8 auto play issue
		autoplay = $target.attr('autoplay');

		// ************************************
		// Data Attributes
		// ************************************

		data = $target.data();

		normOptions = normalizeOptions(data);

		accountId = accountId || normOptions.accountid;
		ddcPlayerInfo = ddcPlayerInfo || normOptions.ddcplayerinfo;
		normOptions.autoplay = autoplay || normOptions.autoplay;

		// data-ddcplayerinfo has an internal structure which needs to be parsed:
		// primary video information, of form: "[ddcplaylist]-[width]-[height]"
		if (ddcPlayerInfo) {
			if (accountId) {
				ddcPlayerInfo = ddcPlayerInfo.replace(accountId, "<siteId>");
			}

			ddcPlayerInfoArray = ddcPlayerInfo.split('-');

			if (ddcPlayerInfoArray[0]) {
				normOptions.ddcplaylist = (accountId) ? ddcPlayerInfoArray[0].replace("<siteId>", accountId) : ddcPlayerInfoArray[0];
				// ddc playlist info is URI Encoded
				normOptions.ddcplaylist = getProxiedPath(decodeURIComponent(normOptions.ddcplaylist));
			}

			normOptions.width = $target.get(0).style.width || ddcPlayerInfoArray[1];
			normOptions.height = $target.get(0).style.height || ddcPlayerInfoArray[2];
		}

		return normOptions;
	}

	/**
	* Make a call to the appropriate playlistUrl, load the xml file and convert it to the playlist format jwPlayer uses
	*
	* @param string playlistUrl path to the desired playlist xml
	* @param string locale which locale to use for UI translation 
	* @param Object formatObj a customized playlist format object to use instead of the default format
	* @return Promise playlistPromise jQuery deferred promise object
	*/
	function loadPlaylist(playlistUrl, locale, formatObj) {
		var deferred = $.Deferred(),
			playlistPromise = deferred.promise();

		if (playlistUrl) {
			// Ajax load the xml playlist & convert to jwPlayer playlist format
			// dataType 'text' because need to strip leading whitespace in xml file and custom parse to JSON
			// the default xml converters fail (due to whitespace issues?)
			$.ajax({
				url: ("http:" == playlistUrl.slice(0, 5) ? (window.location.protocol + playlistUrl.slice(5)) : playlistUrl),
				dataType: 'text'
				
			}).fail(function (jqXHR, textStatus, errorThrown) {
				if (window.console && window.console.log) {
					console.log('Videoplayer unable to load playlist ' + playlistUrl + ' (' + getOriginalPath(playlistUrl) + '): ' + errorThrown);
				}
			}).complete(function (jqXHR, textStatus) {
				var convertedPlaylist;
				if (textStatus == "success") {
					convertedPlaylist = convertDDCPlaylist(jqXHR.responseText, playlistUrl, locale, formatObj);
					deferred.resolve(convertedPlaylist);
				} else {
					// failed, but still allow the promise to resolve because we don't want a failed preroll or postroll to prevent the main video from playing
					deferred.resolve();
				}
			});
		}

		return playlistPromise;
	}

	/**
	* Take plain text return from playlist load and try to convert to XML and parse items
	*
	* @param string data Raw xml text from ajax request
	* @param string playlistUrl path used to determine the asset path and switch betwen DDC and Carsales playlist formats
	* @param Object formatObj a customized playlist format object to use instead of the default format
	* @return Object playlist for jwPlayer options
	*/
	function convertDDCPlaylist(data, playlistUrl, locale, formatObj) {
		var dataXML,
			assetPath,
			vidSeg,
			vidLoSource,
			vidHiSource,
			vidSource,
			vidThumb,
			vidCaptionSrt,
			labelHighQuality = (DDC.i18n) ? getLabel("VIDEOPLAYER_QUALITY_HIGH") : "High",
			labelLowQuality = (DDC.i18n) ? getLabel("VIDEOPLAYER_QUALITY_LOW") : "Low",
			labelCaptionOn = (DDC.i18n) ? getLabel("VIDEOPLAYER_CAPTIONS_ON") : "On",
			format = formatObj || {
				file: vidSource,
				image: vidThumb,
				sources: [{
					file: vidHiSource,
					label: labelHighQuality
				},
					{
					file: vidLoSource,
					label: labelLowQuality
				}],
				tracks: [{
					file: vidCaptionSrt,
					label: labelCaptionOn,
					kind: "captions"
				}]
			};

		// jwPlayer playlist to return
		var playlist = {};

		try {
			//strip top and bottom whitespace - don't use trim() need to support IE7/IE8
			dataXML = $.parseXML(data.replace(/^\s+|\s+$/g, ''));
		} catch (e) {
			if (window.console && window.console.log) {
				console.log('Videoplayer error reading XML file', e, data);
			}
			dataXML = null;
		}
		vidSeg = $(dataXML).find('segment');

		// ************************************
		// Parse XML Playlist
		// ************************************

		// The playlist XML structure is different, based on CarSales versus DDC
		if (playlistUrl.indexOf('http://static.dealer.com/global/media/js/ddc/apis/videoplayer/v2/vidauth.au') > -1) {
			// CarSales Playlist structure - simple de-entitize
			if (vidSeg.length > 0) {
				vidHiSource = vidSeg.find('h264HighSource').text().replace(/&amp;/g, '&');
				vidLoSource = vidSeg.find('h264LowSource').text().replace(/&amp;/g, '&');
				vidSource = vidHiSource;
				vidThumb = vidSeg.find('largeThumb').text().replace(/&amp;/g, '&');
				vidCaptionSrt = ''; // no captions for Carsales format playlists
			}
		} else {
			// DDC default Playlist structure
			// get the absolute path to serve the media, since paths not specified in some (virtualTour) playlists

			assetPath = getOriginalPath(playlistUrl);
			assetPath = assetPath.substring(0, assetPath.lastIndexOf("/") + 1);

			if (vidSeg.length > 0) {

				vidHiSource = vidSeg.attr('h264_high_src') || vidSeg.attr('flv_high_src') || '';
				if (vidHiSource && vidHiSource.indexOf('http:') !== 0 && vidHiSource.indexOf('https:') !== 0) { vidHiSource = assetPath + vidHiSource; }

				vidLoSource = vidSeg.attr('h264_low_src') || vidSeg.attr('flv_low_src') || '';
				if (vidLoSource && vidLoSource.indexOf('http:') !== 0 && vidLoSource.indexOf('https:') !== 0) { vidLoSource = assetPath + vidLoSource; }

				vidSource = vidHiSource || vidLoSource || undefined;

				vidThumb = vidSeg.attr('lrgThumb') || '';
				if (vidThumb && vidThumb.indexOf('http:') !== 0 && vidThumb.indexOf('https:') !== 0) { vidThumb = assetPath + vidThumb; }

				vidCaptionSrt = vidSeg.attr('srt') || '';
				// proxy required for captions file
				if (vidCaptionSrt) {
					if (vidCaptionSrt.indexOf('http:') !== 0 && vidCaptionSrt.indexOf('https:') !== 0) {
						vidCaptionSrt = getProxiedPath(assetPath + vidCaptionSrt);
					} else {
						vidCaptionSrt = getProxiedPath(vidCaptionSrt);
					}
				} else {
					// no captions found
				}
			}
		}

		// ************************************
		// Construct jwPlayer Playlist
		// Based on format object -- roughly
		// TODO Could be fine-tuned for custom formats
		// ************************************

		if (vidSource && format.hasOwnProperty('file')) {
			playlist.file = vidSource;
		}
		if (vidThumb && format.hasOwnProperty('image')) {
			playlist.image = vidThumb;
		}
		if (vidHiSource && vidLoSource && format.hasOwnProperty('sources')) {
			playlist.sources = [{
					file: vidHiSource,
					label: labelHighQuality
				},
					{
					file: vidLoSource,
					label: labelLowQuality
				}];
		}
		if (vidCaptionSrt && format.hasOwnProperty('tracks')) {
			playlist.tracks = [{
					file: vidCaptionSrt,
					label: labelCaptionOn,
					kind: "captions"
				}];
		}

		return playlist;
	}

	/**
	* embed function to instantiate the video player
	* (will call init automatically if needed)
	*
	* @param Object options See DDCVideoWrapper - will merge these extra options
	* @return DDCVideoWrapper
	*/
	function embed(options) {
		var wrapperInstance,
			target,
			$target,
			data,
			accountid;

		// ************************************
		// Instantiate Wrapper Object
		// ************************************

		if (this instanceof DDCVideoWrapperCollection) {
			this.embedAll(options);
			wrapperInstance = null;
		} else if (this instanceof DDCVideoWrapper) {
			wrapperInstance = this;
			// if we are overriding init options, re-run init
			if (options) {
				try {
					wrapperInstance.init(options);
				} catch (e) {
					if (window.console && window.console.log) {
						console.log('Videoplayer error initializing', e);
					}
				}
			}
		}

		if (wrapperInstance) {

			// ************************************
			// Acquire $target
			// ************************************

			$target = acquireTarget(wrapperInstance.options, wrapperInstance);

			// sanity check, jwplayer can only have one target
			if ($target && $target.length == 1) {

				// ************************************
				// Get Any Target Options from Markup
				// ************************************

				// nested mergeOptions statement to ensure proper overrides: manual options > data attributes > wrapper defaults
				wrapperInstance.options = mergeOptions(wrapperInstance.options, mergeOptions(parseTargetVideoOptions($target), options));

				// ************************************
				// Embed JWPlayer
				// ************************************

				// only run embed if playlist is configured and loaded
				// init takes care of this asynchronously with the playlist promise objects - make sure they are ready
				$.when(jwPlayerScriptPromise, wrapperInstance.playlistPromise).then(function () {
					wrapperInstance.onEmbed();
				});

			} else if ($target && $target.length > 1) {
				// if there are multiple targets at this point, something is wrong and must run through init again (TODO)
				if (window.console && window.console.log) {
					console.log('Videoplayer has multiple conflicting video embed target ids');
				}
				
			} else {
				if (window.console && window.console.log) {
					console.log('Videoplayer could not find embed target element', $target);
				}
			}
		} else {
			// no wrapper
		}

		return wrapperInstance;
	}

	/**
	* Jquery Success function for the successful return of the jwplayer script (which will now provide jwwrapper in global scope)
	*
	* jwPlayer automatically embeds when it loads - there is no control over this timing in the API
	* so we need to ajax load the script every time we want to embed a video (luckily it gets cached)
	*
	* @param Function fn a user-defined callback function
	*/
	function onEmbed() {
		var wrapperInstance,
			jwSettings,
			$placeholder,
			player,
			playerid,
			$target,
			playlistPath,
			called = {
				'play' : false,
				'0' : false,
				'25' : false,
				'50' : false,
				'75' : false,
				'100' : false
			};

		if (this instanceof DDCVideoWrapper && window.jwplayer && this.options.targetid && this.options.playerid) {

			wrapperInstance = this;

			$target = acquireTarget(wrapperInstance.options, wrapperInstance);
			playerid = wrapperInstance.options.playerid;

			// If this target already has a jwplayer video in it - remove that cleanly before embedding a new player over it
			if (wrapperInstance.jwPlayerInstance && wrapperInstance.jwPlayerInstance.hasOwnProperty('remove')) {
				try {
					wrapperInstance.jwPlayerInstance.remove();
				} catch (e) {
					// "Cannot call remove() before player is ready"
					// jwplayer will throw this error if the player is not fully instantiated:
					wrapperInstance.playerState = 'initializing';
				}
			}

			// ************************************
			// INSTANTIATE JWPLAYER
			// ************************************

			if (wrapperInstance.playerState != 'initializing') {

				wrapperInstance.playerState = 'initializing';

				// insert a temporary placeholder - which will be replaced by the jwplayer
				$placeholder = $('<div />').attr('id', playerid).addClass('jwplayer');
				$target.html($placeholder).addClass('jwplayer-loading');

				$placeholder.wrap(wrapperInstance.options.wrapperelement);

				jwSettings = getJWSettings(wrapperInstance.options, DDC.Video.jwDefaults);

				player = wrapperInstance.jwPlayerInstance = window.jwplayer(playerid).setup(jwSettings);

				player.onReady(function () {
					wrapperInstance.playerState = 'ready';
					wrapperInstance.playlistPromise = wrapperInstance.prerollPlaylistPromise = wrapperInstance.postrollPlaylistPromise = null;
					// jwplayer flash div wrapper doesn't have a .jwplayer class for some reason, so make it consistent with html version for styling
					if (this.getRenderingMode() == 'flash') {
						$(this.container).parent().addClass('jwplayer jwplayer-flash');
					} else {
						$(this.container).addClass('jwplayer jwplayer-html5');
					}
					$target.removeClass('jwplayer-loading');
					if (wrapperInstance.onEmbedReadyCallback && typeof wrapperInstance.onEmbedReadyCallback === 'function') {
						try {
							wrapperInstance.onEmbedReadyCallback.call(player);
						} catch (e) {
							if (window.console && window.console.log) {
								console.log('Videoplayer error in onReady callback', e);
							}
						}
					}
				});

				player.onSetupError(function (fallback, message) {
					wrapperInstance.playlistPromise = wrapperInstance.prerollPlaylistPromise = wrapperInstance.postrollPlaylistPromise = null;
					$('#' + playerid).addClass('jwplayer jwplayer-error');
					if (window.console && window.console.log) {
						console.log('Player setup error', fallback, message);
					}
				});

				// ************************************
				// Item playback fail handler
				// ************************************

				player.onError(function (message) {
					var myPlaylist,
						myIndex;

					try {
						myPlaylist = this.getPlaylist();
						myIndex = this.getPlaylistIndex();

						if (myPlaylist.length > 1) {
							// Remove bad playlist item and reload
							myPlaylist.splice(myIndex, 1);
							this.load(myPlaylist);
							this.play();
						}

					} catch (e) {
						if (window.console && window.console.log) {
							console.log('Player error', message);
						}
					}
				});

				// ************************************
				// Track Events
				// ************************************

				playlistPath = getOriginalPath(wrapperInstance.options.ddcplaylist);

				player.onPlay(function (e) {
					var currentPlaylistItem = this.getPlaylistItem(),
						currentPlaylistIndex = this.getPlaylistIndex(),
						trackingData = {};

					try {
						if (currentPlaylistIndex === 0 && wrapperInstance.hasPreroll && $.cookie) {
							$.cookie('prerollViewed', 'true', {expires: 1});
						}
						trackingData.eventName = 'play';
						trackingData.playlistPath = playlistPath;
						trackingData.videoPath = currentPlaylistItem.file;

						if (!called['play']) {
							called['play'] = true;
							if (tracker.hasOwnProperty('trackEvent')) {
								tracker.trackEvent(trackingData);
							}
						}
					} catch (e) {
						if (window.console && window.console.log) {
							console.log('tracking error, could not log play');
						}
					}
				});

				player.onTime(function (e) {
					var currentPlaylistItem,
						progress = (e.duration > 0) ? e.position / e.duration : 0,
						percentPlayed,
						trackingData = {};

					try {

						if (progress > 0 && !called['0']) {
							called['0'] = true;
							percentPlayed = '0';
						} else if (progress >= 0.25 && !called['25']) {
							called['25'] = true;
							percentPlayed = '25';
						} else if (progress >= 0.50 && !called['50']) {
							called['50'] = true;
							percentPlayed = '50';
						} else if (progress >= 0.75 && !called['75']) {
							called['75'] = true;
							percentPlayed = '75';
						} else if (progress >= 0.99 && !called['100']) {
							called['100'] = true;
							percentPlayed = '100';
						}

						if (percentPlayed != undefined) {
							currentPlaylistItem = this.getPlaylistItem();
							trackingData.eventName = 'progress';
							trackingData.playlistPath = playlistPath;
							trackingData.videoPath = currentPlaylistItem.file;
							trackingData.progress = percentPlayed;
							if (tracker.hasOwnProperty('trackEvent')) {
								tracker.trackEvent(trackingData);
							}
						}
					} catch (e) {
						if (window.console && window.console.log) {
							console.log('tracking error, could not log onProgress');
						}
					}
				});

			}

		} else {
			if (window.console && window.console.log) {
				console.log('Videoplayer could not embed into ' + this.options.targetid);
			}
		}
	}

	/**
	* A function which is run on each wrapper's onReady callback 
	*
	* @param Function fn A user-defined callback function
	*/
	function onReady(fn) {
		try {
			if (typeof fn === 'function' && this instanceof DDCVideoWrapper) {
				this.onEmbedReadyCallback = fn;
			}
		} catch (e) {
			if (window.console && window.console.log) {
				console.log('Videoplayer could not set up onReady callback', e);
			}
		}
	}

	/**
	* Set up video specific tracking methods for each type of tracker
	*
	* @param Object tracker global tracker object
	* @treturn Object tracker the configured tracker object, with tracking methods
	*/
	function configureTracker(tracker, trackerSettings) {

		tracker.trackEvent = function (data) {
			if (data.hasOwnProperty('eventName')) {
				switch (data.eventName) {

				case 'play':
					if (data.hasOwnProperty('videoPath') && data.hasOwnProperty('playlistPath')) {
						if (this.hasOwnProperty('ddc')) {
							this.ddcTrackPlayEvent(data.videoPath, data.playlistPath);
						}
						if (this.hasOwnProperty('ga')) {
							this.gaTrackPlayEvent(data.videoPath, data.playlistPath);
						}
					}
					break;

				case 'progress':
					if (data.hasOwnProperty('videoPath') && data.hasOwnProperty('playlistPath') && data.hasOwnProperty('progress')) {
						if (this.hasOwnProperty('ddc')) {
							this.ddcTrackProgressEvent(data.videoPath, data.playlistPath, data.progress);
						}
						if (this.hasOwnProperty('ga')) {
							this.gaTrackProgressEvent(data.videoPath, data.playlistPath, data.progress);
						}
					}
					break;

				default:
					break;
				}
			}
		};

		// ************************************
		// DDC Tracker
		// ************************************

		if (tracker.hasOwnProperty('ddc')) {
			if (DDC.Tracker) {
				configureDDCTracker(tracker, trackerSettings);
			} else {
				ddcTrackerScriptPromise.done(function () {
					configureDDCTracker(tracker, trackerSettings);
				}).fail(function () {
					if (window.console && window.console.log) {
						console.log('Videoplayer DDC Tracker not available');
					}
				});
			}
		}

		// ************************************
		// Google Analytics Tracker
		// ************************************

		if (tracker.hasOwnProperty('ga')) {
			if (DDC.Tracker && DDC.gaManager) {
				// DDC.Tracker events automatically tracked by gaManager
				delete(tracker.ga);
			} else {
				// otherwise use this custom GA track code:
				configureGATracker(tracker, trackerSettings);
			}
		}

		return tracker;
	}

	/**
	* Set up DDC tracker
	*
	* @param Object tracker global tracker object
	* @treturn Object tracker the configured tracker object, with DDC tracking methods
	*/
	function configureDDCTracker(tracker, trackerSettings) {
		var ddcTrackerCategory = trackerSettings.ddc.viewTypeVideo;
		tracker.ddc = DDC.Tracker;
		tracker.ddc.deploy();

		tracker.ddcTrackPlayEvent = function (videoPath, playlistPath) {
			this.ddcTrackProgressEvent(videoPath, playlistPath, '0');
		};

		tracker.ddcTrackProgressEvent = function (videoPath, playlistPath, progress) {
			var viewExtra = videoPath + '|' + playlistPath + '|0|' + progress;
			this.ddcTrack(viewExtra);
		};

		tracker.ddcTrack = function (viewExtra) {
			// DDC.Tracker reference
			tracker.ddc.track(ddcTrackerCategory, viewExtra);
		};

		return tracker;
	}

	/**
	* Set up google tracker
	*
	* @param Object tracker
	* @treturn Object tracker the configured tracker object, with google tracking methods
	*/
	function configureGATracker(tracker, settings) {
		var ddcGoogleId = settings.ga.id,
			gaCategory = settings.ga.category;

		tracker.ga = window._gaq = window._gaq || [];

		tracker.gaTrackPlayEvent = function (videoPath, playlistPath) {
			this.gaTrack(gaCategory, 'play', playlistPath);
		};

		tracker.gaTrackProgressEvent = function (videoPath, playlistPath, progress) {
			this.gaTrack(gaCategory, 'viewed ' + progress + '%', playlistPath);
		};

		tracker.gaTrack = function (category, action, opt_label, opt_value, opt_noninteraction) {
			tracker.ga.push(function() {
				_gat._getTracker(ddcGoogleId)._trackEvent(category, action, opt_label, opt_value, opt_noninteraction);
			});
		};
	}

	// ************************************
	// Return Public Module Methods and Props
	// ************************************
	return module;

})(jQuery);
