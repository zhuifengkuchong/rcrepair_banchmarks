/*
	Project/Purpose:
		WCS7 HeroSlideShows

	Description:
		Singleton Object to control the Hero Slide Shows.

	Version: 1.1

	ChangeLog:
		1.1 - 06/06/2011
			* Updated the private method names to start with an underscore
			  to better sync with other programming standards & visually 
			  signify that the method is private.
			* Altered the code to better mirror "real" programming language methodologies.
			* Cleaned up the code
			* Added usage comments.
		1.0 - Initial release

	Notes:
		- See Pro JavaScript Design Patterns for singleton pattern details
		- http://www.apress.com/web-development/javascript/9781590599082

	Last Updated for Release:
		WCS7 v1

	$Last Updated: 06-Jun-2011 12:06:01PM $

*/
/*
	How To:
		Basic Usage: Simply include this file and it will automatically start rotating the hero.

		Expectations:
			Global variables set by DCTM:
			htmlPath -  Path to the HTML for the slides to be included via AJAX.
			imageArray[index] - Delimited string providing the imgage path & the title for the thumbnail creation.

		Advanced Usage:
			heroSlideShow.init() - (Automatically Called)
				- Sets up variables & thumbnails, pulls in the ajax, starts the rotation.

			heroSlideShow.$heroWrapper
				- cached jQuery object for ".hero_wrapper". It is suggested that this object be left alone, but was exposed
				  incase it was needed for anything.

			heroSlideShow.getSlideCount()
				- Returns total slides.

			heroSlideShow.jumpToSlide(slideNumber)
				- Pauses the rotation and jumps to the slide passed.

			heroSlideShow.togglePlay()
				- Toggles between Rotating and Paused states.

			heroSlideShow.pause()
				- Pauses the rotation

			heroSlideShow.start()
				- Unpauses the rotation and automatically moves to the next slide in the rotation

			heroSlideShow.next()
				- Pauses rotation and moves to the next slide in the rotation

			heroSlideShow.previous()
				- Pauses rotation and moves to the previous slide in the rotation

			heroSlideShow.debug()
				- console.log()'s a number of details related to the slideshow. FIREBUG is required.

			heroSlideShow.importAjax() (Automatically called by init() on window load)
				- SHOULD NOT BE CALLED MANUALLY EVER!
				- This gets deleted after the first call.
		
		Understandings:
			You can only use the methods detailed above. Any method prefixed with an underscore means it is inaccessible
			outside the functions mentioned above.

*/
heroSlideShow = (function(){

	// Private Attributes
	var slideCount=0,
	slideDuration=10000,
	nextSlide=1, currentSlide, prevSlide,
	$myParent, $mySlides, $myThumbs,
	myTimerID,currentState, ajaxImported;

	// Private Methods

	// Moves to next slide in rotation order, adjusts slide numbers,
	// sets timeout for next rotation.
	// On first call, via init(), sets up all numbers as needed
	function _rotateSlides() {
		
		_internalDebug('Rotating.');

		if (!currentState) {

			_internalDebug('First run.');
			
			currentSlide = 1;
			nextSlide = 1;
			previousSlide = slideCount;
			
			$mySlides = $('.hero_slide',this.$heroWrapper);
			$mySlides.css({"opacity":0});
			$('.dogear',$mySlides).css({"opacity":0});
			$mySlides.eq(0).css({"opacity":1});

			$myThumbs = $('.heroSlideThumbs li',this.$heroWrapper);

		} else {
			_hideSlide(currentSlide);
		}
		currentState = 'Rotating';
		_showSlide(nextSlide);
		myTimerID = setTimeout(function(){ _rotateSlides(); }, slideDuration);
		_setUpSlideNumbers();
	}

	// Fades in passed slide
	function _showSlide(slide) {
		
		var $thisSlide = $mySlides.eq((slide-1));
		var $thisThumb = $myThumbs.eq((slide-1));

		_internalDebug('Showing: ', slide);

		// Show the slide
		$thisSlide.addClass('active').animate({opacity : 1}, 1000);
		// Show the Dog ears, staggered.
		$('.dogear:first',$thisSlide).delay(400).animate({opacity : 1}, 500);
		$('.dogear:last',$thisSlide).delay(600).animate({opacity : 1}, 600);
		currentSlide = (slide*1);
		$thisThumb.addClass('active');
	}

	// Fades out passed slide
	function _hideSlide(slide) {

		var $thisSlide = $mySlides.eq((slide-1));
		var $thisThumb = $myThumbs.eq((slide-1));
		
		_internalDebug('Hiding: ', slide);

		// Hide Dog Ears
		$('.dogear:first',$thisSlide).animate({opacity : 0},0);
		$('.dogear:last',$thisSlide).animate({opacity : 0}, 0);

		// Hide Slide
		$thisSlide.animate({opacity : 0}, 500).delay(500).removeClass('active');
		$thisThumb.removeClass('active');
	}

	// Hides current slide, jumps to passed slide number, pauses the slide show.
	function _jumpToSlide(slide) {
		_internalDebug("Jumping to Slide :",slide);
		_pause();
		if (slide != currentSlide) {
			_hideSlide(currentSlide);
			_showSlide(slide);
			_setUpSlideNumbers();
		}
	}

	// Unpauses the slide show
	function _start() {
		if (currentState == 'Paused') {
			_internalDebug('Start Called.');
			_rotateSlides(); 
		} else {
			_internalDebug('Already Rotating.');
		}
	}

	// pauses the slide show
	function _pause() {
		if (currentState != 'Paused') {
			_internalDebug('Pause Called.');
			currentState = 'Paused';
			clearTimeout(myTimerID);
			myTimerID=null;
		} else {
			_internalDebug('Already Paused.');
		}
	}

	// jumpsTo next slide in rotation order
	function _next() {
		_internalDebug('Next Called.');
		_jumpToSlide(nextSlide);
	}

	// jumpsTo previous slide in rotation order
	function _previous() {
		_internalDebug('Previous Called.');
		_jumpToSlide(prevSlide);
	}

	// Toggles rotate and paused states
	function _togglePlay(){
		_internalDebug('Toggling Play.');
		if (currentState == 'Paused') { _start(); }
		else { _pause(); }
	}

	// Set up the thumbnails & their click events
	function _makeThumbs() {
		
		_internalDebug('Making thumbnails.');

		if ($('.heroSlideThumbs').length == 0) {
			var myHtml = '<ul class="clearfix">';
			$.each(imageArray, function(i){
				if (imageArray[i]) {
					imgInfo = imageArray[i].split('^^^');
					myHtml += '<li rel="'+i+'"><div class="heroThumbArrow" rel="'+i+'"></div><img src="/hdus/en_US'+imgInfo[0]+'" width="116px" height="52px" class="thumbnail" /><p class="large b">'+imgInfo[1]+'</p></li>';
				}
			});
			myHtml += '</ul>';
			this.$heroWrapper.append('<div class="row"><div class="heroSlideThumbs clearfix">'+myHtml+'</div></div>');

			// Attach the click event
			$('.heroSlideThumbs li',this.$heroWrapper).click(function(){
				heroSlideShow.jumpToSlide($(this).attr('rel'));
			});
		} else {
			_internalDebug('Thumbnails already made.');
		}
	}

	// Sets up next and previous slide numbers
	function _setUpSlideNumbers() {
		prevSlide = ((currentSlide-1)==0)?slideCount:(currentSlide-1);
		nextSlide = ((currentSlide+1)>slideCount)?1:((currentSlide*1)+1);
		_internalDebug("Slides: \nPrevious:\t", prevSlide, "\nCurrent:\t", currentSlide, "\nNext:\t\t",nextSlide);
	
	}

	// Used for internal debug messages that get displayed when
	// GLOBAL_FED_DEBUG OR HEROSLIDESHOW_DEBUG are true.
	// Uses console.log so firebug is required.
	function _internalDebug() {
		try {
			if (window.GLOBAL_FED_DEBUG || window.HEROSLIDESHOW_DEBUG) {
				var args = Array.prototype.slice.call(arguments);
				console.log.apply(this, args)
			}
		} catch(e) {
			try {
				/* This is for IE. It is not perfect, but it helps */
				if (window.console && window.console.log) {
					console.log(args);
				}
			} catch(nm){
			}
		}
	}

	// Object Return. All functions in here can be used publicly
	return {

		// Simple Container
		$heroWrapper:null,
		// Intializes everything.
		// Starts the auto rotating.
		init : function(){
			if (!currentState) {
				_internalDebug('Init Called.');
				this.$heroWrapper = $('.hero_wrapper');
				slideCount = this.$heroWrapper.attr('rel');
				if (this.getSlideCount()>1) {
					//$myParent = this.$heroWrapper.parent();
					_makeThumbs.call(this);
					heroSlideShow.importAjax();
				}
			} else {
				_internalDebug('Init Already Called.');
			}
		},

		// Returns the number of slides
		getSlideCount : function(){ return slideCount*1; },

		// Allows for jumping to a specified slide.
		// Stops the auto rotating.
		jumpToSlide : function(slide){
			_jumpToSlide(slide);
		},

		// Pauses the auto rotating
		pause : function() { _pause(); },

		// Restarts the auto rotating from the current slide,
		// calling this instantly moves to the next slide.
		start : function(){ _start(); },

		// Jumps to the next slide & stops auto rotate.
		next : function(){ _next(); },

		// Jumps to the previous slide and stops the auto rotating.
		previous : function(){ _previous(); },

		// Toggles between Rotating and Paused states
		togglePlay : function(){ _togglePlay(); },

		// Dumps some debugging information.
		debug : function(){
			try {
				console.log('---heroSlideShow Debug---\n',
				"this: ", this, "\n",
				'$heroWrapper: ', this.$heroWrapper,"\n",
				'$mySlides: ', $mySlides,"\n",
				"Duration: ", (slideDuration/1000), " seconds.\n",
				"myTimerID: ", myTimerID, "\n",
				"Currently: ", currentState, "\n",
				"Slides: \n",
				"Previous:\t", prevSlide, "\nCurrent:\t", currentSlide, "\nNext:\t\t",nextSlide,"\n",
				'\n---/heroSlideShow Debug ---');
			} catch (e) {}
		},

		// Imports the ajax slides.
		// This is only public so it can be called on window.load().
		// This method kills itself so it can not be called a second time.
		importAjax : function(){
			if (!ajaxImported) {
				_internalDebug('importAjax Called.');
				$('<div class="heroAjaxIncluded"></div>').insertAfter(this.$heroWrapper.find('.hero_slide'));

				this.$heroWrapper.find('.heroAjaxIncluded').load('/hdus/en_US'+htmlPath+'.htm', function(){
					attachOverlays('.heroAjaxIncluded');
					_rotateSlides()
					});
				$('.pod').css('height','auto');
				fixPodHeights();
				ajaxImported = true;

				// Kill yourself
				delete this.importAjax;
			} else {
				// We should NEVER get here.
				_internalDebug('importAjax Called Again. :(');
			}

		}

	} // end object return

})();

// Initialize the slideshow
heroSlideShow.init();